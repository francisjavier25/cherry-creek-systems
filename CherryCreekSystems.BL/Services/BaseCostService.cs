﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using System;
using System.Linq;

namespace CherryCreekSystems.BL
{
    public class BaseCostService : GenericService<BaseCost>
    {
        public BaseCostService(DataContext context)
            : base(context)
        {
        }

        public BaseCostService()
        {
        }

        public double GetBaseCost(int boomTypeId, double xValue)
        {
            var baseValues = Get().Where(bc => bc.BoomSystemId == boomTypeId).ToList();

            double[] x_Values = baseValues.Select(p => p.XValue).ToArray();
            double[] y_Values = baseValues.Select(p => p.YValue).ToArray();

            double forecast = 0f;
            double b = 0f;
            double a = 0f;
            double X = xValue; // Forecast

            double tempTop = 0f;
            double tempBottom = 0f;

            // X
            double x_Avg = x_Values.Sum();
            x_Avg /= x_Values.Length;

            //for (int i = 0; i < x_Values.Length; i++)
            //{
            //    x_Avg += x_Values[i];
            //}
            //x_Avg /= x_Values.Length;

            // Y
            double y_Avg = y_Values.Sum();
            y_Avg /= y_Values.Length;

            //for (int i = 0; i < y_Values.Length; i++)
            //{
            //    y_Avg += y_Values[i];
            //}
            //y_Avg /= y_Values.Length;

            for (int i = 0; i < y_Values.Length; i++)
            {
                tempTop += (x_Values[i] - x_Avg) * (y_Values[i] - y_Avg);
                tempBottom += Math.Pow(((x_Values[i] - x_Avg)), 2f);
            }

            b = tempTop / tempBottom;
            a = y_Avg - b * x_Avg;

            forecast = a + b * X;

            return Math.Round(forecast, 2);
        }
    }
}