﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using log4net;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace CherryCreekSystems.BL
{
    public class QuoteRequestBoomService : GenericService<QuoteRequestBoom>
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public async Task DeleteRecordsForQuote(int quoteId)
        {
            try
            {
                await Context.QuoteRequestBooms.Where(b => b.QuoteRequestId == quoteId).DeleteAsync();
            }
            catch (Exception ex)
            {
                log.Error("Add", ex);
                throw;
            }
        }
    }
}