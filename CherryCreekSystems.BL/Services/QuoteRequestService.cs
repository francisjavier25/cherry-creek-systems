﻿using AutoMapper;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace CherryCreekSystems.BL
{
    public class QuoteRequestService : GenericService<QuoteRequest>
    {
        public QuoteRequestService()
        {
        }

        public QuoteRequestService(DataContext context) : base(context)
        {
        }

        public IQueryable<QuoteRequest> GetByBoomType(int boomTypeId)
        {
            //Context.Database.Log = message => Trace.Write(message);
            var entities = Context.QuoteRequests.Join(Context.QuoteRequestBooms, q => q.Id, b => b.QuoteRequestId,
                           (q, b) => new { Quote = q, Boom = b })
                           .Where(b => b.Boom.BoomTypeId == boomTypeId)
                           .GroupBy(s => s.Quote.Id) //A way to select distinct
                           .Select(b => b.FirstOrDefault().Quote);
            return entities;
        }

        public Task FillChildObjects(bool customer = false, bool salesRep = false, bool distributor = true,
                                     bool quoteStatus = false, bool greenHouse = false, bool customItem = false,
                                     params QuoteRequest[] quoteRequests)
        {
            var task = Task.Run(async () =>
            {
                //Context.Database.Log = message => Trace.Write(message);
                var queries = new List<IEnumerable>();

                if (customer)
                {
                    var customerIds = quoteRequests.Select(c => c.CustomerId).Distinct();
                    var customers = await Context.Customers.Where(c => customerIds.Contains(c.Id))
                                                           .Include(c => c.ShipAddresses) //Using .Select on state generates a full join between ship addresses and states
                                                           .ToListAsync();                //Without filtering on the specific addresses for customers first
                                                                                          //Better to use separate query for states

                    var stateIds = customers.SelectMany(s => s.ShipAddresses).Select(a => a.StateId).Distinct();
                    var states = Context.States.Where(s => stateIds.Contains(s.Id)).Future();
                    queries.Add(states);
                }
                if (salesRep)
                {
                    var salesRepIds = quoteRequests.Select(c => c.SalesRepId).Distinct();
                    var salesReps = Context.SalesReps.Where(s => salesRepIds.Contains(s.Id)).Future();
                    queries.Add(salesReps);
                }
                if (distributor)
                {
                    var distributorIds = quoteRequests.Select(c => c.DistributorId).Distinct();
                    var distributors = Context.Distributors.Where(d => distributorIds.Contains(d.Id)).Future();
                    queries.Add(distributors);
                }
                if (quoteStatus)
                {
                    var quoteStatusIds = quoteRequests.Select(c => c.QuoteStatusId).Distinct();
                    var quoteStatuses = Context.QuoteStatuses.Where(s => quoteStatusIds.Contains(s.Id)).Future();
                    queries.Add(quoteStatuses);
                }
                if (greenHouse)
                {
                    var greenHouseTypeIds = quoteRequests.SelectMany(c => c.QuoteBoomSystemsData.QuoteBoomSystems)
                                                         .Select(g => g.Boom.GreenHouseTypeId).Distinct();
                    var greenHouses = Context.GreenHouseTypes.Where(g => greenHouseTypeIds.Contains(g.Id)).Future();
                    queries.Add(greenHouses);
                }
                if (customItem)
                {
                    var quoteRequestIds = quoteRequests.Select(c => c.Id).Distinct();
                    var customItems = Context.CustomeQuoteItems.Where(c => quoteRequestIds.Contains(c.QuoteRequest_Id)).Future();
                    queries.Add(customItems);
                }

                var query = queries.First();
                foreach (var n in query)
                {
                    break;
                    //Force first query to run, which forces all to be executed
                }

                if (greenHouse)
                {
                    //The greenhouse type objects in the system/booms objects do not get filled in automatically
                    var systems = quoteRequests.SelectMany(c => c.QuoteBoomSystemsData.QuoteBoomSystems);
                    foreach (var s in systems)
                    {
                        var greenHouseType = Context.GreenHouseTypes.FirstOrDefault(g => g.Id == s.Boom.GreenHouseTypeId);
                        s.Boom.GreenHouseType = greenHouseType;
                    }
                }
            });

            return task;
        }

        public async Task<int> Copy(QuoteRequest quoteRequest)
        {
            Mapper.CreateMap<QuoteRequest, QuoteRequest>();
            Mapper.CreateMap<QuoteBoomSystemData, QuoteBoomSystemData>();

            var copyQuoteRequest = new QuoteRequest();
            Mapper.Map(quoteRequest, copyQuoteRequest);

            copyQuoteRequest.Customer = null;
            copyQuoteRequest.Distributor = null;
            copyQuoteRequest.ShipAddress = null;
            copyQuoteRequest.SalesRep = null;
            copyQuoteRequest.QuoteStatus = null;
            copyQuoteRequest.Id = 0;
            copyQuoteRequest.Revision = 0;

            foreach (var quoteBoomSystem in copyQuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems)
            {
                quoteBoomSystem.GenerateID();
            }

            Add(copyQuoteRequest);
            await SaveChangesAsync();
            copyQuoteRequest.QuoteNumber = quoteRequest.QuoteNumber;

            await SaveChangesAsync();

            var sugarCRMClient = new SugarCRMClient();
            await sugarCRMClient.ExportQuoteToSugarCRM(copyQuoteRequest.Id);

            return copyQuoteRequest.Id;
        }

        public async Task<string> CreateQuoteNumber(QuoteRequest quoteRequest)
        {
            await FillChildObjects(quoteRequests: quoteRequest, customer: true);

            var companyNameFirstWord = quoteRequest.Customer.CompanyName.FirstWord();
            var date = DateTime.Now.ToString("MMddyy");
            var uniqeQuoteNumber = 30000 + quoteRequest.Id;
            var quoteNumber = $"{companyNameFirstWord}-{date}-QUOTE-{uniqeQuoteNumber}";
            return quoteNumber;
        }

        public async Task<IEnumerable<ProgressViewModel>> GetProgressViewModel(IQueryable<QuoteRequest> quoteRequestsQuery, string searchTerm = "", int? quoteStatusId = null)
        {
            searchTerm = (searchTerm ?? string.Empty).Trim().ToLower();

            var quoteRequests = await quoteRequestsQuery.Where(s => (!quoteStatusId.HasValue || s.QuoteStatusId == quoteStatusId.Value)
                                                && (string.IsNullOrEmpty(searchTerm) || (s.Customer.CompanyName.ToLower().Contains(searchTerm)))
                                           ).ToListAsync();

            var quotesWithSystems = quoteRequests.Where(q => q.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive().Any()).ToArray();
            await FillChildObjects(customer: true, quoteRequests: quotesWithSystems);

            var quoteSystems = quotesWithSystems.SelectMany(q => q.QuoteBoomSystemsData.QuoteBoomSystems
                                                .Select(f => new { QuoteRequest = q, QuoteSystem = f }));

            var viewModel = quoteSystems.Select((qu, s) => new ProgressViewModel
            {
                CustomerName = qu.QuoteRequest.Customer.CompanyName,
                QuoteRequestId = qu.QuoteRequest.Id,
                //BoomId = qu.QuoteSystem.Boom.BoomTypeId,
                SystemId = qu.QuoteSystem.Id_,
                BoomType = (BoomType)qu.QuoteSystem.Boom.BoomTypeId,
                SystemName = qu.QuoteSystem.DetailCalculationResult + " " + qu.QuoteSystem.Name,
                Quantity = qu.QuoteSystem.Quantity,

                MachiningThreshold = qu.QuoteSystem.Boom.MachiningThreshold,
                WeldingThreshold = qu.QuoteSystem.Boom.WeldingThreshold,
                ProductionThreshold = qu.QuoteSystem.Boom.ProductionThreshold,
                ShippingPickListThreshold = qu.QuoteSystem.Boom.ShippingPickListThreshold,
                SawAndSheerThreshold = qu.QuoteSystem.Boom.SawAndSheerThreshold,
                AssemblyPickListThreshold = qu.QuoteSystem.Boom.AssemblyPickListThreshold,
                PunchAndBreakThreshold = qu.QuoteSystem.Boom.PunchAndBreakThreshold,
                AssemblyThreshold = qu.QuoteSystem.Boom.AssemblyThreshold,
                SprayBarApprovalThreshold = qu.QuoteSystem.Boom.SprayBarApprovalThreshold,
                GalganizerThreshold = qu.QuoteSystem.Boom.GalganizerThreshold,
                ShippingThreshold = qu.QuoteSystem.Boom.ShippingThreshold,
                InfoAndDesignThreshold = qu.QuoteSystem.Boom.InfoAndDesignThreshold,

                MachiningStatus = qu.QuoteSystem.Boom.MachiningStatus,
                WeldingStatus = qu.QuoteSystem.Boom.WeldingStatus,
                ProductionStatus = qu.QuoteSystem.Boom.ProductionStatus,
                ShippingPickListStatus = qu.QuoteSystem.Boom.ShippingPickListStatus,
                SawAndSheerStatus = qu.QuoteSystem.Boom.SawAndSheerStatus,
                AssemblyPickListStatus = qu.QuoteSystem.Boom.AssemblyPickListStatus,
                PunchAndBreakStatus = qu.QuoteSystem.Boom.PunchAndBreakStatus,
                AssemblyStatus = qu.QuoteSystem.Boom.AssemblyStatus,
                SprayBarApprovalStatus = qu.QuoteSystem.Boom.SprayBarApprovalStatus,
                GalganizerStatus = qu.QuoteSystem.Boom.GalganizerStatus,
                ShippingStatus = qu.QuoteSystem.Boom.ShippingStatus,
                InfoAndDesignStatus = qu.QuoteSystem.Boom.InfoAndDesignStatus,
                OrderDate = qu.QuoteRequest.OrderDate,
                DueDate = qu.QuoteRequest.DueDate,
                StatusId = qu.QuoteRequest.QuoteStatusId
            }).ToList();
            return viewModel;
        }
    }
}