﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL.Services
{
    public class UserCreator
    {
        private readonly DataContext _dataContext;
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;

        public UserCreator(DataContext dataContext)
        {
            _dataContext = new DataContext();
            _userManager = new ApplicationUserManager(new ApplicationUserStore(_dataContext) { AutoSaveChanges = false });
            _roleManager = new ApplicationRoleManager(new ApplicationRoleStore(_dataContext));
        }

        public async Task<ApplicationUser> CreateDistributorUser(Distributor dist)
        {
            using (var transaction = _dataContext.Database.BeginTransaction())
            {
                ApplicationUser user;
                var role = await _roleManager.FindByNameAsync("distributor");
                try
                {
                    _dataContext.Distributors.Add(dist);
                    await _dataContext.SaveChangesAsync();
                    user = await CreateUser(dist.Name);
                    await _dataContext.SaveChangesAsync();
                    CreateRole(user.Id, dist.Id, role.Id);
                    await _dataContext.SaveChangesAsync();
                    transaction.Commit();
                    return user;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    user = null;
                }

                return user;
            }
        }

        public async Task<ApplicationUser> CreateCustomerUser(Customer cust)
        {
            using (var transaction = _dataContext.Database.BeginTransaction())
            {
                ApplicationUser user;
                var role = await _roleManager.FindByNameAsync("customer");
                try
                {
                    _dataContext.Customers.Add(cust);
                    await _dataContext.SaveChangesAsync();
                    user = await CreateUser(cust.CompanyName);
                    await _dataContext.SaveChangesAsync();
                    CreateRole(user.Id, cust.Id, role.Id);
                    await _dataContext.SaveChangesAsync();
                    transaction.Commit();
                    return user;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    user = null;
                }

                return user;
            }
        }

        public async Task<ApplicationUser> CreateSalesRepUser(SalesRep rep)
        {
            using (var transaction = _dataContext.Database.BeginTransaction())
            {
                ApplicationUser user;
                var role = await _roleManager.FindByNameAsync("salesrep");
                try
                {
                    _dataContext.SalesReps.Add(rep);
                    await _dataContext.SaveChangesAsync();
                    user = await CreateUser(rep.Name);
                    await _dataContext.SaveChangesAsync();
                    CreateRole(user.Id, rep.Id, role.Id);
                    await _dataContext.SaveChangesAsync();
                    transaction.Commit();
                    return user;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    user = null;
                }

                return user;
            }
        }

        public async Task<ApplicationUser> CreateAdminUser(string name)
        {
            using (var transaction = _dataContext.Database.BeginTransaction())
            {
                ApplicationUser user;
                var userName = await GetUserName(name);
                var role = await _roleManager.FindByNameAsync("administrator");
                try
                {
                    user = new ApplicationUser {UserName = userName};
                    _userManager.Create(user, "@dmin123");
                    await _dataContext.SaveChangesAsync();

                    var adminUserRole = new ApplicationUserRole
                    {
                        UserId = user.Id,
                        ApplicationUserId = null,
                        RoleId = role.Id
                    };
                    _dataContext.ApplicationUserRoles.Add(adminUserRole);
                    await _dataContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    user = null;
                }
                return user;
            }
        }

        private async Task<ApplicationUser> CreateUser(string name)
        {
            var userName = await GetUserName(name);
            var user = new ApplicationUser { UserName = userName };
            await _userManager.CreateAsync(user, "password123");
            return user;
        }

        private void CreateRole(string userId, int applicationUserId, string roleId)
        {
            var userRole = new ApplicationUserRole
            {
                UserId = userId,
                ApplicationUserId = applicationUserId,
                RoleId = roleId
            };
            _dataContext.ApplicationUserRoles.Add(userRole);
        }

        private async Task<string> GetUserName(string name)
        {
            var userName = Regex.Replace(name, "[^0-9a-zA-Z]+", "").Replace(" ", "");
            var userExists = await _userManager.FindByNameAsync(userName) != null;

            if (userExists)
            {
                var originalUserName = userName;
                foreach (var i in Enumerable.Range(2, 10000))
                {
                    userName = originalUserName + i;
                    userExists = await _userManager.FindByNameAsync(userName) != null;
                    if (!userExists)
                    {
                        break;
                    }
                }
            }

            return userName;
        }
    }
}