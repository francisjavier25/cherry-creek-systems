﻿using CherryCreekSystems.DAL;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SendGrid.Helpers.Mail;

namespace CherryCreekSystems.BL
{
    public class UserEmails
    {
        public SpecService specService;
        private readonly DataContext _context;

        public UserEmails()
        {
            _context = new DataContext();
            specService = new SpecService();
        }

        public async Task<bool> SendQuoteFileNotifyWebsite(int customerId, string customerEmail, string fileName, int quoteId, string quoteNumber, string emailTemplate)
        {
            var customer = await _context.GetCustomerUsers().FirstOrDefaultAsync(c => c.ApplicationUserId == customerId);
            if (customer == null) return false;

            #region get file data

            byte[] fileBytes;
            using (var file = File.OpenRead(fileName))
            {
                fileBytes = new byte[file.Length];
                await file.ReadAsync(fileBytes, 0, (int)file.Length);
            }

            var emailAttachment = new EmailClient.EmailAttachment
            {
                Name = fileName,
                Type = "pdf",
                Bytes = fileBytes
            };

            #endregion get file data

            #region create email body

            var systemsLink = $"http://ccsquoting.azurewebsites.net/Customer/Quotes/Systems/{quoteId}";
            var systemsHref = new TagBuilder("a") { InnerHtml = "here" };
            systemsHref.MergeAttribute("href", systemsLink);

            var sBuilder = new StringBuilder(emailTemplate);
            sBuilder.Replace("{Company Name}", customer.CompanyName);
            sBuilder.Replace("{Quote Number}", quoteNumber);
            sBuilder.Replace("{Systems Link}", systemsHref.ToString());
            sBuilder.Replace("{User Name}", customer.UserName);

            #endregion

            return await EmailClient.SendEmail(customerEmail, "WDrummond@cherrycreeksystems.com", null, "WDrummond@cherrycreeksystems.com", sBuilder.ToString(),
                "View Your Quote Information", new List<EmailClient.EmailAttachment> { emailAttachment });
        }
    }
}