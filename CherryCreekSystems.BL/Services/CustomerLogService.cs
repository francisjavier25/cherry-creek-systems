﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Domain.SugarCRMLogs;

namespace CherryCreekSystems.BL
{
    public class CustomerLogService : GenericService<CustomerLog>
    {
        public CustomerLogService(DataContext context)
            : base(context)
        {
        }

        public CustomerLogService()
        {

        }
    }
}