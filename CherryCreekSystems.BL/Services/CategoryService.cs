﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain.BoomComponents;

namespace CherryCreekSystems.BL
{
    public class CategoryService : GenericService<Category>
    {
        public CategoryService(DataContext context)
            : base(context)
        {
        }
    }
}