﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain.BoomComponents;

namespace CherryCreekSystems.BL
{
    public class MinorComponentDetailService : GenericService<MinorComponentDetail>
    {
        public MinorComponentDetailService(DataContext context)
            : base(context)
        {
        }
    }
}