﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class ShipAddressService : GenericService<ShipAddress>
    {
        public ShipAddressService() { }

        public ShipAddressService(DataContext context)
            : base(context)
        {
        }
    }
}