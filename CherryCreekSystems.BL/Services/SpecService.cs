﻿using CherryCreekSystems.Domain;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL
{
    public class SpecService : GenericService<Spec>
    {
        public Task<SpecLookup> GetCustomerQuoteEmail()
        {
            //Context.Database.Log = message => Trace.Write(message);
            var specIds = Context.Specs.Where(s => s.Name.Equals("Customer Quote Email")).Select(d => d.Id);
            var lookups = Context.SpecLookups.Where(l => specIds.Contains(l.SpecId));

            return lookups.FirstOrDefaultAsync();
        }
    }
}