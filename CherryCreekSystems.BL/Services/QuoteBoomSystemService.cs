﻿using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL
{
    public class QuoteBoomSystemService : GenericService<QuoteBoomSystem>
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private CalculationEngine Engine;
        private List<string> CalculationErrors;

        private QuoteRequestBoomService quoteRequestBoomService;

        public QuoteBoomSystemService()
        {
        }

        public QuoteBoomSystemService(DataContext context)
        {
            Context = context;
        }

        public async Task RemoveQuoteBoomSystem(QuoteRequest quoteRequest, string systemId)
        {
            var quoteBoomSystem = quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.First(q => q.Id_ == systemId);
            quoteBoomSystem.Active = false;
            await Context.SaveChangesAsync();

            await RecreateQuoteRequestBoomRecords(quoteRequest);
        }

        public async Task<QuoteSystemResult> CreateQuoteBoomSystem(QuoteRequest quoteRequest, Boom boom)
        {
            var result = new QuoteSystemResult();
            try
            {
                var quoteBoomSystem = await CreateDetailQuote(quoteRequest, boom);
                result.CalculationErrors = PerformDetailQuoteCalculations(quoteBoomSystem, quoteRequest);

                if (result.CalculationErrors.Count > 0)
                {
                    if (quoteRequest != null && quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems != null)
                        quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.Remove(quoteBoomSystem);

                    return result;
                }

                await Context.SaveChangesAsync();

                await RecreateQuoteRequestBoomRecords(quoteRequest);

                result.System = quoteBoomSystem;
                return result;
            }
            catch (Exception ex)
            {
                log.Error("CreateQuoteBoomSystem", ex);
                throw;
            }
        }

        private async Task RecreateQuoteRequestBoomRecords(QuoteRequest quoteRequest)
        {
            quoteRequestBoomService = new QuoteRequestBoomService();

            //Delete all
            await quoteRequestBoomService.DeleteRecordsForQuote(quoteRequest.Id);

            //Re-create all
            var quoteRequestBoomRecords = quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive().Select(s => new QuoteRequestBoom
            {
                QuoteRequestId = quoteRequest.Id,
                BoomTypeId = s.Boom.BoomTypeId
            });

            quoteRequestBoomService.AddRange(quoteRequestBoomRecords);

            await quoteRequestBoomService.SaveChangesAsync();
        }

        private async Task<QuoteBoomSystem> CreateDetailQuote(QuoteRequest quoteRequest, Boom boom)
        {
            try
            {
                var boomSystemService = new BoomSystemService();
                var statusThresholdService = new StatusThresholdService();

                var boomSystemTask = boomSystemService.GetAllLoaded();

                var statusThreshold = await statusThresholdService.Get().ById(boom.BoomTypeId);

                var boomSystem = (await boomSystemTask).ById(boom.BoomTypeId).Ordered();

                CopyThresholdValues(boom, statusThreshold);

                //Start by calculating system details
                var systemComponent = new QuoteBoomSystem
                {
                    Order = quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.Count + 1,
                    Name = boomSystem.Name,
                    Quantity = boom.QuantityOfSystems,
                    PriceCalculation = boomSystem.PriceCalculation
                };
                systemComponent.GenerateID();

                CopyCalculationDetails(StartingComponent: boomSystem, CalculatedComponent: systemComponent);
                CopyDescriptionDetails(StartingComponent: boomSystem, CalculatedComponent: systemComponent);
                CopyPriceConditions(StartingComponent: boomSystem, CalculatedComponent: systemComponent);
                SetIncludeInBuildDocument(systemComponent);

                var quoteCategories = new List<QuoteCategory>();
                foreach (var category in boomSystem.Categories.Where(c => c.IsActive))
                {
                    var quoteCategory = new QuoteCategory();
                    quoteCategory.GenerateID();

                    CopyNameAndOrderValue(StartingComponent: category, CalculatedComponent: quoteCategory);
                    quoteCategories.Add(quoteCategory);

                    var quoteMajorComponents = new List<QuoteMajorComponent>();
                    if (category.MajorComponents != null)
                    {
                        foreach (var majorComponent in category.MajorComponents.Where(c => c.IsActive))
                        {
                            var quoteMajorComponent = new QuoteMajorComponent();
                            quoteMajorComponent.GenerateID();

                            MakeVisible(quoteMajorComponent);
                            CopyNameAndOrderValue(StartingComponent: majorComponent, CalculatedComponent: quoteMajorComponent);
                            CopyCalculationDetails(StartingComponent: majorComponent, CalculatedComponent: quoteMajorComponent);
                            CopyDescriptionDetails(StartingComponent: majorComponent, CalculatedComponent: quoteMajorComponent);
                            CopyPriceConditions(StartingComponent: majorComponent, CalculatedComponent: quoteMajorComponent);
                            CopyActivationConditions(StartingComponent: majorComponent, CalculatedComponent: quoteMajorComponent);
                            SetIncludeInBuildDocument(quoteMajorComponent);
                            quoteMajorComponents.Add(quoteMajorComponent);

                            var quoteMajorComponentDetails = new List<QuoteMajorComponentDetail>();
                            if (majorComponent.MajorComponentDetails != null)
                            {
                                foreach (var majorComponentDetail in majorComponent.MajorComponentDetails.Where(c => c.IsActive))
                                {
                                    var quoteMajorComponentDetail = new QuoteMajorComponentDetail();
                                    quoteMajorComponentDetail.GenerateID();

                                    MakeVisible(quoteMajorComponentDetail);
                                    CopyNameAndOrderValue(StartingComponent: majorComponentDetail, CalculatedComponent: quoteMajorComponentDetail);
                                    CopyDescriptionDetails(StartingComponent: majorComponentDetail, CalculatedComponent: quoteMajorComponentDetail);
                                    CopyNameAndOrderValue(StartingComponent: majorComponentDetail, CalculatedComponent: quoteMajorComponentDetail);
                                    CopyCalculationDetails(StartingComponent: majorComponentDetail, CalculatedComponent: quoteMajorComponentDetail);
                                    CopyDescriptionDetails(StartingComponent: majorComponentDetail, CalculatedComponent: quoteMajorComponentDetail);
                                    SetIncludeInBuildDocument(quoteMajorComponentDetail);
                                    quoteMajorComponentDetails.Add(quoteMajorComponentDetail);
                                }
                            }

                            //Assign the major component details
                            quoteMajorComponent.QuoteMajorComponentDetails = quoteMajorComponentDetails;

                            var quoteMinorComponents = new List<QuoteMinorComponent>();
                            if (majorComponent.MinorComponents != null)
                            {
                                foreach (var minorComponent in majorComponent.MinorComponents.Where(c => c.IsActive))
                                {
                                    var quoteMinorComponent = new QuoteMinorComponent();
                                    quoteMinorComponent.GenerateID();

                                    MakeVisible(quoteMinorComponent);
                                    CopyNameAndOrderValue(StartingComponent: minorComponent, CalculatedComponent: quoteMinorComponent);
                                    CopyCalculationDetails(StartingComponent: minorComponent, CalculatedComponent: quoteMinorComponent);
                                    CopyDescriptionDetails(StartingComponent: minorComponent, CalculatedComponent: quoteMinorComponent);
                                    CopyPriceConditions(StartingComponent: minorComponent, CalculatedComponent: quoteMinorComponent);
                                    CopyActivationConditions(StartingComponent: minorComponent, CalculatedComponent: quoteMinorComponent);
                                    SetIncludeInBuildDocument(quoteMinorComponent);
                                    quoteMinorComponents.Add(quoteMinorComponent);

                                    var quoteMinorComponentDetails = new List<QuoteMinorComponentDetail>();
                                    if (minorComponent.MinorComponentDetails != null)
                                    {
                                        foreach (var minorComponentDetail in minorComponent.MinorComponentDetails.Where(c => c.IsActive))
                                        {
                                            var quoteMinorComponentDetail = new QuoteMinorComponentDetail();
                                            quoteMinorComponentDetail.GenerateID();

                                            MakeVisible(quoteMinorComponentDetail);
                                            CopyNameAndOrderValue(StartingComponent: minorComponentDetail, CalculatedComponent: quoteMinorComponentDetail);
                                            CopyDescriptionDetails(StartingComponent: minorComponentDetail, CalculatedComponent: quoteMinorComponentDetail);
                                            CopyNameAndOrderValue(StartingComponent: minorComponentDetail, CalculatedComponent: quoteMinorComponentDetail);
                                            CopyCalculationDetails(StartingComponent: minorComponentDetail, CalculatedComponent: quoteMinorComponentDetail);
                                            CopyDescriptionDetails(StartingComponent: minorComponentDetail, CalculatedComponent: quoteMinorComponentDetail);
                                            SetIncludeInBuildDocument(quoteMinorComponentDetail);
                                            quoteMinorComponentDetails.Add(quoteMinorComponentDetail);
                                        }
                                    }

                                    //Assign the minor component details
                                    quoteMinorComponent.QuoteMinorComponentDetails = quoteMinorComponentDetails;
                                }
                            }

                            //Assign the minor components
                            quoteMajorComponent.QuoteMinorComponents = quoteMinorComponents;
                        }
                    }

                    //Assign the major components
                    quoteCategory.QuoteMajorComponents = quoteMajorComponents;
                }

                //Assign the categories
                systemComponent.QuoteCategories = quoteCategories;

                quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.Add(systemComponent);

                systemComponent.Active = true;
                systemComponent.Boom = boom;
                return systemComponent;
            }
            catch (Exception ex)
            {
                log.Error("CreateDetailQuote", ex);
                throw;
            }
        }

        private static void CopyThresholdValues(Boom boom, StatusThreshold statusThreshold)
        {
            boom.InfoAndDesignThreshold = statusThreshold.InfoAndDesignThreshold;
            boom.SprayBarApprovalThreshold = statusThreshold.SprayBarApprovalThreshold;
            boom.AssemblyPickListThreshold = statusThreshold.AssemblyPickListThreshold;
            boom.ShippingPickListThreshold = statusThreshold.ShippingPickListThreshold;
            boom.ProductionThreshold = statusThreshold.ProductionThreshold;
            boom.SawAndSheerThreshold = statusThreshold.SawAndSheerThreshold;
            boom.PunchAndBreakThreshold = statusThreshold.PunchAndBreakThreshold;
            boom.MachiningThreshold = statusThreshold.MachiningThreshold;
            boom.WeldingThreshold = statusThreshold.WeldingThreshold;
            boom.GalganizerThreshold = statusThreshold.GalganizerThreshold;
            boom.AssemblyThreshold = statusThreshold.AssemblyThreshold;
            boom.ShippingThreshold = statusThreshold.ShippingThreshold;
        }

        public List<string> PerformDetailQuoteCalculations(QuoteBoomSystem quoteBoomSystem, QuoteRequest quoteRequest)
        {
            try
            {
                CalculationErrors = new List<string>();

                var calculatedPriceLevel = (1 + (quoteRequest.PriceLevel / 100));

                Engine = new CalculationEngine
                {
                    QuoteRequest = quoteRequest,
                    Boom = quoteBoomSystem.Boom,
                    BoomBaseValue = calculateBaseCost(quoteBoomSystem.Boom.BoomTypeId, quoteBoomSystem.Boom)
                };
                Engine.RegisterQuoteAndBoomValues();

                foreach (var quoteCategory in quoteBoomSystem.QuoteCategories)
                {
                    if (quoteCategory.QuoteMajorComponents != null)
                    {
                        foreach (var quoteMajorComponent in quoteCategory.QuoteMajorComponents)
                        {
                            PerformDetailCalculation(CalculatedComponent: quoteMajorComponent);
                            PerformComponentPriceCalculation(CalculatedComponent: quoteMajorComponent, priceLevel: calculatedPriceLevel);
                            PerformAdditionalPriceAssignment(CalculatedComponent: quoteMajorComponent);
                            PerformActivationCalculation(CalculatedComponent: quoteMajorComponent);

                            if (quoteMajorComponent.QuoteMinorComponents != null)
                            {
                                foreach (var quoteMinorComponent in quoteMajorComponent.QuoteMinorComponents)
                                {
                                    PerformDetailCalculation(CalculatedComponent: quoteMinorComponent);
                                    PerformComponentPriceCalculation(CalculatedComponent: quoteMinorComponent, priceLevel: calculatedPriceLevel);
                                    PerformAdditionalPriceAssignment(CalculatedComponent: quoteMinorComponent);
                                    PerformActivationCalculation(CalculatedComponent: quoteMinorComponent);
                                }
                            }
                        }
                    }
                }

                //Perform calculations
                PerformDescriptionCalculation(CalculatedComponent: quoteBoomSystem);
                PerformDetailCalculation(CalculatedComponent: quoteBoomSystem);
                PerformComponentPriceCalculation(CalculatedComponent: quoteBoomSystem, priceLevel: calculatedPriceLevel);
                PerformAdditionalPriceAssignment(CalculatedComponent: quoteBoomSystem);

                //Calling this after the value calculations are finished because only then the Engine will have the values we need to insert into the component names
                foreach (var quoteCategory in quoteBoomSystem.QuoteCategories)
                {
                    if (quoteCategory.QuoteMajorComponents != null)
                    {
                        foreach (var quoteMajorComponent in quoteCategory.QuoteMajorComponents)
                        {
                            PerformDescriptionCalculation(CalculatedComponent: quoteMajorComponent);

                            if (quoteMajorComponent.QuoteMinorComponents != null)
                            {
                                foreach (var quoteMinorComponent in quoteMajorComponent.QuoteMinorComponents)
                                {
                                    PerformDescriptionCalculation(CalculatedComponent: quoteMinorComponent);
                                }
                            }
                        }
                    }
                }

                return CalculationErrors;
            }
            catch (Exception ex)
            {
                log.Error("PerformDetailQuoteCalculations", ex);
                throw ex;
            }
        }

        private static void MakeVisible(IVisible Component)
        {
            Component.IsVisible = true;
        }

        private static void SetIncludeInBuildDocument(IDescriptionCalculatedComponent CalculatedComponent)
        {
            CalculatedComponent.IncludeInBuildDocument = true;
        }

        private static void CopyNameAndOrderValue(INamedAndOrderedComponent StartingComponent, INamedAndOrderedComponent CalculatedComponent)
        {
            CalculatedComponent.Name = StartingComponent.Name;
            CalculatedComponent.Order = StartingComponent.Order;
        }

        private static void CopyCalculationDetails(IDetailedComponent StartingComponent, IDetailCalculatedComponent CalculatedComponent)
        {
            CalculatedComponent.DetailCalculation = StartingComponent.DetailCalculation;
            CalculatedComponent.DetailCalculationID = StartingComponent.DetailCalculationID;
        }

        private static void CopyActivationConditions(IActivatedComponent StartingComponent, IActivatedCalculatedComponent CalculatedComponent)
        {
            CalculatedComponent.ActivateComponentCondition = StartingComponent.ActivateComponentCondition;
        }

        private static void CopyPriceConditions(IPricedComponent StartingComponent, IPriceCalculatedComponent CalculatedComponent)
        {
            CalculatedComponent.PriceCalculation = StartingComponent.PriceCalculation;
            CalculatedComponent.AddPriceCondition = StartingComponent.AddPriceCondition;
            CalculatedComponent.PriceCalculationID = StartingComponent.PriceCalculationID;
        }

        private static void CopyDescriptionDetails(IDescriptionComponent StartingComponent, IDescriptionCalculatedComponent CalculatedComponent)
        {
            CalculatedComponent.DescriptionCalculation = StartingComponent.DescriptionCalculation;
            CalculatedComponent.DescriptionCalculationID = StartingComponent.DescriptionCalculationID;
        }

        private void PerformDetailCalculation(IDetailCalculatedComponent CalculatedComponent)
        {
            try
            {
                if (!string.IsNullOrEmpty(CalculatedComponent.DetailCalculation))
                {
                    var result = Engine.PerformCalculation(CalculatedComponent.DetailCalculation, CalculatedComponent.DetailCalculationID, CalculatedComponent.Name);
                    //Always need to run the engine calculation in case other components need the data
                    //But if the component data is overriden then just don't assign the result
                    if (!CalculatedComponent.OverrideComponentData)
                        CalculatedComponent.DetailCalculationResult = result;
                }
            }
            catch (Exception ex)
            {
                log.Error("PerformDetailCalculation - " + CalculatedComponent.DetailCalculation, ex);
                CalculationErrors.Add(ex.Message);
            }
        }

        private void PerformDescriptionCalculation(IDescriptionCalculatedComponent CalculatedComponent)
        {
            try
            {
                if (!string.IsNullOrEmpty(CalculatedComponent.DescriptionCalculation))
                {
                    var result = Engine.PerformCalculation(CalculatedComponent.DescriptionCalculation, CalculatedComponent.DescriptionCalculationID, CalculatedComponent.Name);
                    if (!CalculatedComponent.OverrideComponentData)
                        CalculatedComponent.DescriptionCalculationResult = result;
                }
            }
            catch (Exception ex)
            {
                log.Error("PerformDetailCalculation - " + CalculatedComponent.DescriptionCalculation, ex);
                CalculationErrors.Add(ex.Message);
            }
        }

        private void PerformAdditionalPriceAssignment(IPriceCalculatedComponent CalculatedComponent)
        {
            try
            {
                if (!string.IsNullOrEmpty(CalculatedComponent.AddPriceCondition))
                {
                    var result = Engine.PerformCalculation(CalculatedComponent.AddPriceCondition, componentName: CalculatedComponent.Name);

                    if (!CalculatedComponent.OverrideComponentData)
                    {
                        var priceCalculationConditionResult = Convert.ToBoolean(result);

                        if (priceCalculationConditionResult == true)
                        {
                            CalculatedComponent.AssignedPrice = CalculatedComponent.CalculatedPrice;
                        }
                        else
                        {
                            CalculatedComponent.AssignedPrice = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("PerformAdditionalPriceCalculation - " + CalculatedComponent.AddPriceCondition, ex);
                CalculationErrors.Add(ex.Message);
            }
        }

        private void PerformActivationCalculation(IActivatedCalculatedComponent CalculatedComponent)
        {
            try
            {
                if (!string.IsNullOrEmpty(CalculatedComponent.ActivateComponentCondition))
                {
                    var result = Engine.PerformCalculation(CalculatedComponent.ActivateComponentCondition, componentName: CalculatedComponent.Name);
                    CalculatedComponent.IsActive = Convert.ToBoolean(result);
                }
            }
            catch (Exception ex)
            {
                log.Error("PerformActivationCalculation - " + CalculatedComponent.ActivateComponentCondition, ex);
                CalculationErrors.Add(ex.Message);
            }
        }

        private void PerformComponentPriceCalculation(IPriceCalculatedComponent CalculatedComponent, double priceLevel)
        {
            try
            {
                if (!string.IsNullOrEmpty(CalculatedComponent.PriceCalculation))
                {
                    var result = Engine.PerformCalculation(CalculatedComponent.PriceCalculation, CalculatedComponent.PriceCalculationID, CalculatedComponent.Name);
                    var calculatedPrice = Convert.ToDouble(result);

                    CalculatedComponent.CalculatedPrice = Math.Round(calculatedPrice, 2) * priceLevel; //Added price level discount/markup
                }
            }
            catch (Exception ex)
            {
                log.Error("PerformSystemPriceCalculation", ex);
                CalculationErrors.Add(ex.Message);
            }
        }

        private double calculateBaseCost(int boomTypeId, Boom boom)
        {
            try
            {
                var baseCostService = new BaseCostService();

                var xValue = 0.0;
                switch (boomTypeId)
                {
                    case (int)BoomType.Echo:
                        xValue = boom.BayLengthInFeet;
                        break;

                    case (int)BoomType.SingleRail:
                        xValue = boom.BayWidthInFeet;
                        break;

                    case (int)BoomType.DoubleRail:
                        xValue = boom.BayWidthInFeet;
                        break;

                    case (int)BoomType.CWFDoubleRail:
                        xValue = boom.BayWidthInFeet;
                        break;

                    case (int)BoomType.Tower:
                        xValue = boom.BayWidthInFeet;
                        break;

                    case (int)BoomType.Navigator:
                        xValue = boom.BayWidthInFeet;
                        break;
                }

                var baseCost = baseCostService.GetBaseCost(boomTypeId, xValue);
                return baseCost;
            }
            catch (Exception ex)
            {
                log.Error("calculateBaseCost", ex);
                throw;
            }
        }
    }
}