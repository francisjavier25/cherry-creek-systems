﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class DistributorService : GenericService<Distributor>
    {
        public DistributorService()
        {
        }

        public DistributorService(DataContext context) : base(context)
        {
        }
    }
}