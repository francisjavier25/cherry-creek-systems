﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class GreenhouseTypeService : GenericService<GreenHouseType>
    {
        public GreenhouseTypeService()
        {
        }

        public GreenhouseTypeService(DataContext context) : base(context)
        {
        }
    }
}