﻿using CherryCreekSystems.Domain;
using System;
using System.Collections.Generic;

namespace CherryCreekSystems.BL
{
    public class ProgressStatusCalculator
    {
        private DateTime _cleanTodayDate;

        public ProgressStatusCalculator()
        {
            _cleanTodayDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        public IEnumerable<StatusViewModel> GetStatusViewModel(IEnumerable<ProgressViewModel> progressData)
        {
            var statusList = new List<StatusViewModel>();
            foreach (var s in progressData)
            {
                var status = new StatusViewModel
                {
                    CustomerName = s.CustomerName,
                    BoomId = s.BoomId,
                    SystemName = s.SystemName,
                    Quantity = s.Quantity,
                    ControlType = s.ControlType,
                    OrderDate = s.OrderDate,
                    DueDate = s.DueDate
                };

                var cleanOrderDate = new DateTime(s.OrderDate.Year, s.OrderDate.Month, s.OrderDate.Day);
                var cleanDueDate = new DateTime(s.DueDate.Year, s.DueDate.Month, s.DueDate.Day);
                var daysToDue = cleanDueDate.Subtract(cleanOrderDate).Days;

                status.InfoAndDesignStatus = CalculateStatus(s.InfoAndDesignStatus, s.InfoAndDesignThreshold, cleanOrderDate, daysToDue);
                status.SprayBarApprovalStatus = CalculateStatus(s.SprayBarApprovalStatus, s.SprayBarApprovalThreshold, cleanOrderDate, daysToDue);
                status.AssemblyPickListStatus = CalculateStatus(s.AssemblyPickListStatus, s.AssemblyPickListThreshold, cleanOrderDate, daysToDue);
                status.ShippingPickListStatus = CalculateStatus(s.ShippingPickListStatus, s.ShippingPickListThreshold, cleanOrderDate, daysToDue);
                status.ProductionStatus = CalculateStatus(s.ProductionStatus, s.ProductionThreshold, cleanOrderDate, daysToDue);
                status.SawAndSheerStatus = CalculateStatus(s.SawAndSheerStatus, s.SawAndSheerThreshold, cleanOrderDate, daysToDue);
                status.PunchAndBreakStatus = CalculateStatus(s.PunchAndBreakStatus, s.PunchAndBreakThreshold, cleanOrderDate, daysToDue);
                status.MachiningStatus = CalculateStatus(s.MachiningStatus, s.MachiningThreshold, cleanOrderDate, daysToDue);
                status.WeldingStatus = CalculateStatus(s.WeldingStatus, s.WeldingThreshold, cleanOrderDate, daysToDue);
                status.GalganizerStatus = CalculateStatus(s.GalganizerStatus, s.GalganizerThreshold, cleanOrderDate, daysToDue);
                status.AssemblyStatus = CalculateStatus(s.AssemblyStatus, s.AssemblyThreshold, cleanOrderDate, daysToDue);
                status.ShippingStatus = CalculateStatus(s.ShippingStatus, s.ShippingThreshold, cleanOrderDate, daysToDue);

                statusList.Add(status);
            }

            return statusList;
        }

        private double CalculateStatus(int status, double threshold, DateTime orderDate, int daysToDue)
        {
            if (status == 100)
            {
                return 0; //Green completed
            }

            var earlyDate = orderDate.AddDays(daysToDue * threshold);
            if (earlyDate >= _cleanTodayDate)
            {
                return 1; //White, too early
            }

            var currentDateToEarlyDateDays = _cleanTodayDate.Subtract(earlyDate).Days;
            if (currentDateToEarlyDateDays > 5)
            {
                return 0.5; //Red, super late
            }

            var calc = 0.95 - 0.1 * currentDateToEarlyDateDays;
            var roundedCalc = Math.Round(calc + 0.05, 2);
            return roundedCalc; //Shade of red, getting late
        }
    }
}