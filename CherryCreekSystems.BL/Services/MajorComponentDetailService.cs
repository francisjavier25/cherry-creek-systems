﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain.BoomComponents;

namespace CherryCreekSystems.BL
{
    public class MajorComponentDetailService : GenericService<MajorComponentDetail>
    {
        public MajorComponentDetailService(DataContext context)
            : base(context)
        {
        }
    }
}