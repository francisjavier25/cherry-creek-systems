﻿using CherryCreekSystems.DAL;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL
{
    public class GenericService<TEntity> where TEntity : class
    {
        protected DataContext Context;

        public GenericService()
        {
            Context = new DataContext();
            //Context.Database.Log = message => Trace.Write(message);
        }

        public GenericService(DataContext context)
        {
            Context = context;
        }

        public IQueryable<TEntity> Get()
        {
            var entities = Context.Set<TEntity>();
            return entities;
        }

        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void Add(TEntity entity)
        {
            try
            {
                Context.Set<TEntity>().Add(entity);
            }
            catch (Exception ex)
            {
                log.Error("Add", ex);
                throw;
            }
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            try
            {
                Context.Set<TEntity>().AddRange(entities);
            }
            catch (Exception ex)
            {
                log.Error("AddRange", ex);
                throw;
            }
        }

        public void Remove(TEntity entity)
        {
            try
            {
                Context.Set<TEntity>().Remove(entity);
            }
            catch (Exception ex)
            {
                log.Error("Remove", ex);
                throw;
            }
        }

        public Task SaveChangesAsync()
        {
            //Context.Database.Log = message => Trace.Write(message);
            //var transaction = Context.Database.CurrentTransaction;
            return Context.SaveChangesAsync();
        }

        public int SaveChanges()
        {
            try
            {
                //Context.Database.Log = message => Trace.Write(message);
                return Context.SaveChanges();
            }
            catch
            {
                return 0;
            }
        }
    }
}