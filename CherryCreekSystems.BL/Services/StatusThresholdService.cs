﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class StatusThresholdService : GenericService<StatusThreshold>
    {
        public StatusThresholdService(DataContext context)
            : base(context)
        {
        }
        public StatusThresholdService() { }
    }
}