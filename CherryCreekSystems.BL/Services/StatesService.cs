﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class StatesService : GenericService<State>
    {
        public StatesService(DataContext context) : base(context)
        {

        }

        public StatesService()
        {
        }
    }
}