﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class CustomerChecklistService : GenericService<CustomerCheckList>
    {
        public CustomerChecklistService(DataContext context)
            : base(context)
        {
        }
    }
}