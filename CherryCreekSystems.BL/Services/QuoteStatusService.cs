﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class QuoteStatusService : GenericService<QuoteStatus>
    {
        public QuoteStatusService()
        {
        }

        public QuoteStatusService(DataContext context) : base(context)
        {
        }
    }
}