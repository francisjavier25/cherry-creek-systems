﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class SteelDataService : GenericService<SteelData>
    {
        public SteelDataService(DataContext context)
            : base(context)
        {
        }

        public SteelDataService() { }
    }
}