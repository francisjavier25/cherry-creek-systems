﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class BoomService : GenericService<Boom>
    {
        public BoomService()
        {
        }

        public BoomService(DataContext context) : base(context)
        {
        }
    }
}