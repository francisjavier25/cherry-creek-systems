﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain.BoomComponents;

namespace CherryCreekSystems.BL
{
    public class MajorComponentService : GenericService<MajorComponent>
    {
        public MajorComponentService(DataContext context)
            : base(context)
        {
        }
    }
}