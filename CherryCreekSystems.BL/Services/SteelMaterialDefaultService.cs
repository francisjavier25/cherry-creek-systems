﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL
{
    public class SteelMaterialDefaultService : GenericService<SteelMaterialDefault>
    {
        public SteelMaterialDefaultService()
        {
        }
    }
}
