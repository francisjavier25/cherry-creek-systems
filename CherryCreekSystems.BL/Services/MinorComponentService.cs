﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain.BoomComponents;

namespace CherryCreekSystems.BL
{
    public class MinorComponentService : GenericService<MinorComponent>
    {
        public MinorComponentService(DataContext context)
            : base(context)
        {
        }
    }
}