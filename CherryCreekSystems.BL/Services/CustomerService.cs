﻿using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace CherryCreekSystems.BL
{
    public class CustomerService : GenericService<Customer>
    {
        public CustomerService()
        {
        }

        public CustomerService(DataContext context) : base(context)
        {
        }

        public IQueryable<Customer> GetCustomersForDistributor(int distributorId)
        {
            //Context.Database.Log = message => Trace.Write(message);
            var customerIds = Context.QuoteRequests.Where(r => r.DistributorId == distributorId).Select(c => c.CustomerId).Distinct();
            var customers = Context.Customers.Where(c => customerIds.Contains(c.Id)).OrderedByCompanyName();
            return customers;
        }

        public IQueryable<Customer> GetCustomersForSalesRep(int salesRepId)
        {
            //Context.Database.Log = message => Trace.Write(message);
            var customerIds = Context.QuoteRequests.Where(r => r.SalesRepId == salesRepId).Select(c => c.CustomerId).Distinct();
            var customers = Context.Customers.Where(c => customerIds.Contains(c.Id)).OrderedByCompanyName();
            return customers;
        }
        public Task FillChildObjects(bool shipAddress = false,
                                    params Customer[] customers)
        {
            var task = Task.Run(() =>
            {
                //Context.Database.Log = message => Trace.Write(message);
                var queries = new List<IEnumerable>();
                if (shipAddress)
                {
                    var customerIds = customers.Select(c => c.Id);
                    var shipAddresses = Context.ShipAddresses.Where(s => customerIds.Contains(s.CustomerId))
                                                             .IncludeStates()
                                                             .Future();

                    queries.Add(shipAddresses);
                }

                var query = queries.First();
                foreach (var n in query)
                {
                    break;
                    //Force first query to run, which forces all to be executed
                }
            });

            return task;
        }
    }
}