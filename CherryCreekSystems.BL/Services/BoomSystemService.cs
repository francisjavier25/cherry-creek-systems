﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain.BoomComponents;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace CherryCreekSystems.BL
{
    public class BoomSystemService : GenericService<BoomSystem>
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public BoomSystemService()
        {
        }

        public BoomSystemService(DataContext context)
            : base(context)
        {
        }

        public async Task<List<BoomSystem>> GetAllLoaded()
        {
            try
            {
                var systems = Task.Run(() =>
                {
                    var load = Context.BoomSystems.Future();
                    Context.Categories.Where(c => c.IsActive).Future();
                    Context.MajorComponents.Where(c => c.IsActive).Future();
                    Context.MinorComponents.Where(c => c.IsActive).Future();
                    Context.MajorComponentDetails.Where(c => c.IsActive).Future();
                    Context.MinorComponentDetails.Where(c => c.IsActive).Future();
                    return load.ToList();
                });
                var boomSystems = await systems;

                return boomSystems;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}