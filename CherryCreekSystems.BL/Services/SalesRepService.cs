﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using Z.EntityFramework.Plus;

namespace CherryCreekSystems.BL
{
    public class SalesRepService : GenericService<SalesRep>
    {
        public SalesRepService()
        {
        }

        public SalesRepService(DataContext context) : base(context)
        {
        }
    }
}