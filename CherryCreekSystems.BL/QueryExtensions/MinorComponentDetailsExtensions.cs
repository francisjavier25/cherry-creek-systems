﻿using CherryCreekSystems.Domain.BoomComponents;
using System.Collections.Generic;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class MinorComponentDetailsExtensions
    {
        public static List<MinorComponentDetail> OrderAsc(this List<MinorComponentDetail> query)
        {
            return query.OrderBy(c => c.Order).ToList();
        }

        public static List<MinorComponentDetail> IsActive(this List<MinorComponentDetail> query)
        {
            return query.Where(c => c.IsActive).ToList();
        }
    }
}