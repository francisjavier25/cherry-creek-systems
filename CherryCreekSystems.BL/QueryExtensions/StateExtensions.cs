﻿using CherryCreekSystems.Domain;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class StateExtensions
    {
        public static Task<State> ByName(this IQueryable<State> query, string name)
        {
            return query.FirstOrDefaultAsync(s => s.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }
    }
}