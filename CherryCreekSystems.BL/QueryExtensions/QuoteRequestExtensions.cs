﻿using CherryCreekSystems.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class QuoteRequestExtensions
    {
        public static IQueryable<QuoteRequest> OrderedByDateDescending(this IQueryable<QuoteRequest> query)
        {
            return query.OrderByDescending(q => q.OrderDate);
        }

        public static IQueryable<QuoteRequest> OrderedByIdDescending(this IQueryable<QuoteRequest> query)
        {
            return query.OrderByDescending(q => q.Id);
        }

        public static IQueryable<QuoteRequest> ByDistributor(this IQueryable<QuoteRequest> query, int? distributorId)
        {
            return query.Where(q => !distributorId.HasValue || q.DistributorId == distributorId);
        }

        public static IQueryable<QuoteRequest> ByCustomer(this IQueryable<QuoteRequest> query, int? customerId)
        {
            return query.Where(q => !customerId.HasValue || q.CustomerId == customerId);
        }

        public static IQueryable<QuoteRequest> BySalesRep(this IQueryable<QuoteRequest> query, int? salesRepId)
        {
            return query.Where(q => !salesRepId.HasValue || q.SalesRepId == salesRepId);
        }

        public static IQueryable<QuoteRequest> ByQuoteStatus(this IQueryable<QuoteRequest> query, int? quoteStatusId)
        {
            return query.Where(q => !quoteStatusId.HasValue || q.QuoteStatusId == quoteStatusId);
        }

        public static IQueryable<QuoteRequest> ByStatusDate(this IQueryable<QuoteRequest> query, DateTime startDate, DateTime endDate)
        {
            return query.Where(q => q.StatusDate >= startDate && q.StatusDate <= endDate);
        }

        public static IQueryable<QuoteRequest> ByQuoteNumber(this IQueryable<QuoteRequest> query, string quoteNumber)
        {
            return query.Where(q => string.IsNullOrEmpty(quoteNumber) || q.QuoteNumber == quoteNumber);
        }

        public static Task<QuoteRequest> ByQuoteId(this IQueryable<QuoteRequest> query, int id)
        {
            return query.FirstOrDefaultAsync(q => q.Id == id);
        }

        public static IQueryable<QuoteRequest> WhereActive(this IQueryable<QuoteRequest> query)
        {
            return query.Where(q => q.Active);
        }

        public static async Task<double> LatestPriceLevelAssigned(this IQueryable<QuoteRequest> query, int customerId)
        {
            var quote = await query.ByCustomer(customerId)
                                   .OrderedByIdDescending()
                                   .LatestQuoteWithPriceLevel();
            if (quote != null)
                return quote.PriceLevel;

            return 0;
        }

        public static Task<QuoteRequest> LatestQuoteWithPriceLevel(this IQueryable<QuoteRequest> query)
        {
            return query.FirstOrDefaultAsync(q => q.PriceLevel != 0);
        }

        public static IEnumerable<string> GetBoomTypes(this QuoteRequest quoteRequest)
        {
            return quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive().Select(b => EnumExtensions.GetDescription((BoomType)b.Boom.BoomTypeId)).Distinct();
        }

        public static double GetTotalCost(this QuoteRequest quoteRequest)
        {
            var totalCost = 0.0;
            if (quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems != null)
            {
                foreach (var sys in quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive())
                {
                    var systemCount = sys.Quantity;
                    var systemPrice = sys.GetTotalCost();
                    var extendedPrice = systemPrice * systemCount;
                    totalCost += extendedPrice;
                }
            }

            totalCost += quoteRequest.FreightEstimateValue;
            totalCost += quoteRequest.InstallationEstimateValue;
            totalCost += quoteRequest.SteelEstimateValue;
            totalCost += quoteRequest.DiscountValue;
            return Math.Round(totalCost, 2);
        }
    }
}