﻿using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.Domain;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public class SpecContainer
    {
        private const string DRUM_SIZE = "Drum Size";
        private const string ECHO_CONTROLLER_TYPE = "Echo Controller Type";
        private const string NUMBER_OF_LAYERS = "Number Of Layers";
        private const string PULLEY_BRACKET_SPACING = "Pulley Bracket Spacing";
        private const string FEED = "Feed";
        private const string DRB_CONTROLLER_TYPE = "DRB Controller Type";
        private const string SINGLE_DUAL_WATER = "Single Dual Water";
        private const string EV_VPD_GAUGE_WIRES = "EV VPD Gauge Wires";
        private const string INJECTOR_TYPE = "Injector Type";
        private const string CARRY_STEEL = "Carry Steel";
        private Dictionary<string, IEnumerable<string>> container;

        public SpecContainer() { container = new Dictionary<string, IEnumerable<string>>(); }

        public IEnumerable<string> DrumSize
        {
            get
            {
                if (container.ContainsKey(DRUM_SIZE))
                    return container[DRUM_SIZE];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(DRUM_SIZE))
                    container.Add(DRUM_SIZE, value);
                else
                    container[DRUM_SIZE] = value;
            }
        }

        public IEnumerable<string> EchoControllerType
        {
            get
            {
                if (container.ContainsKey(ECHO_CONTROLLER_TYPE))
                    return container[ECHO_CONTROLLER_TYPE];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(ECHO_CONTROLLER_TYPE))
                    container.Add(ECHO_CONTROLLER_TYPE, value);
                else
                    container[ECHO_CONTROLLER_TYPE] = value;
            }
        }

        public IEnumerable<string> NumberOfLayers
        {
            get
            {
                if (container.ContainsKey(NUMBER_OF_LAYERS))
                    return container[NUMBER_OF_LAYERS];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(NUMBER_OF_LAYERS))
                    container.Add(NUMBER_OF_LAYERS, value);
                else
                    container[NUMBER_OF_LAYERS] = value;
            }
        }

        public IEnumerable<string> PulleyBracketSpacing
        {
            get
            {
                if (container.ContainsKey(PULLEY_BRACKET_SPACING))
                    return container[PULLEY_BRACKET_SPACING];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(PULLEY_BRACKET_SPACING))
                    container.Add(PULLEY_BRACKET_SPACING, value);
                else
                    container[PULLEY_BRACKET_SPACING] = value;
            }
        }

        public IEnumerable<string> Feed
        {
            get
            {
                if (container.ContainsKey(FEED))
                    return container[FEED];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(FEED))
                    container.Add(FEED, value);
                else
                    container[FEED] = value;
            }
        }

        public IEnumerable<string> DRBControllerType
        {
            get
            {
                if (container.ContainsKey(DRB_CONTROLLER_TYPE))
                    return container[DRB_CONTROLLER_TYPE];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(DRB_CONTROLLER_TYPE))
                    container.Add(DRB_CONTROLLER_TYPE, value);
                else
                    container[DRB_CONTROLLER_TYPE] = value;
            }
        }

        public IEnumerable<string> SingleDualWater
        {
            get
            {
                if (container.ContainsKey(SINGLE_DUAL_WATER))
                    return container[SINGLE_DUAL_WATER];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(SINGLE_DUAL_WATER))
                    container.Add(SINGLE_DUAL_WATER, value);
                else
                    container[SINGLE_DUAL_WATER] = value;
            }
        }

        public IEnumerable<string> EvVpdGaugeWires
        {
            get
            {
                if (container.ContainsKey(EV_VPD_GAUGE_WIRES))
                    return container[EV_VPD_GAUGE_WIRES];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(EV_VPD_GAUGE_WIRES))
                    container.Add(EV_VPD_GAUGE_WIRES, value);
                else
                    container[EV_VPD_GAUGE_WIRES] = value;
            }
        }

        public IEnumerable<string> InjectorType
        {
            get
            {
                if (container.ContainsKey(INJECTOR_TYPE))
                    return container[INJECTOR_TYPE];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(INJECTOR_TYPE))
                    container.Add(INJECTOR_TYPE, value);
                else
                    container[INJECTOR_TYPE] = value;
            }
        }

        public IEnumerable<string> CarrySteel
        {
            get
            {
                if (container.ContainsKey(CARRY_STEEL))
                    return container[CARRY_STEEL];

                return new string[0];
            }
            set
            {
                if (!container.ContainsKey(CARRY_STEEL))
                    container.Add(CARRY_STEEL, value);
                else
                    container[CARRY_STEEL] = value;
            }
        }

    }

    public static class SpecExtensions
    {
        public static IQueryable<Spec> WithSpecLookups(this IQueryable<Spec> query)
        {
            return query.Include(c => c.SpecLookups);
        }

        public static Task<Spec> CustomerQuoteEmailTemplate(this IQueryable<Spec> query)
        {
            return query.FirstOrDefaultAsync(s => s.Name.Equals("Customer Quote Email"));
        }

        public static SpecContainer GetSpecContainer(this List<Spec> specsList)
        {
            var container = new SpecContainer();

            //Echo specs
            container.DrumSize = specsList.GetValues("Drum Size");
            container.EchoControllerType = specsList.GetValues("Echo Controller Type");
            container.NumberOfLayers = specsList.GetValues("Number Of Layers");
            container.PulleyBracketSpacing = specsList.GetValues("Pulley Bracket Spacing");
            container.Feed = specsList.GetValues("Feed");

            //SRB specs
            container.DRBControllerType = specsList.GetValues("DRB Controller Type");
            container.SingleDualWater = specsList.GetValues("Single Dual Water");
            container.EvVpdGaugeWires = specsList.GetValues("EV VPD Gauge Wires");
            container.InjectorType = specsList.GetValues("Injector Type").OrderBy(s => s);

            //DRB specs

            //CWF-DRB specs

            //TB specs
            container.CarrySteel = specsList.GetValues("Carry Steel");

            //Navigator specs

            //Ground Runner specs

            //Sky Rail specs

            return container;
        }
    }
}