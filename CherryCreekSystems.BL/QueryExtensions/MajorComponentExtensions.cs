﻿using CherryCreekSystems.Domain.BoomComponents;
using System.Collections.Generic;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class MajorComponentExtensions
    {
        public static List<MajorComponent> OrderAsc(this List<MajorComponent> query)
        {
            return query.OrderBy(c => c.Order).ToList();
        }

        public static List<MajorComponent> IsActive(this List<MajorComponent> query)
        {
            return query.Where(c => c.IsActive).ToList();
        }
    }
}