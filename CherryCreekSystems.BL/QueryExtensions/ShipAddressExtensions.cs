﻿using System.Data.Entity;
using CherryCreekSystems.Domain;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class ShipAddressExtensions
    {
        public static IQueryable<ShipAddress> ByCustomerId(this IQueryable<ShipAddress> query, int customerId)
        {
            return query.Where(s => s.CustomerId == customerId);
        }

        public static IQueryable<ShipAddress> IncludeStates(this IQueryable<ShipAddress> query)
        {
            return query.Include(c => c.State);
        }
    }
}