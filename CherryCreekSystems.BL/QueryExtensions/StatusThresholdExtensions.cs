﻿using CherryCreekSystems.Domain;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class StatusThresholdExtensions
    {
        public static Task<StatusThreshold> ById(this IQueryable<StatusThreshold> query, int boomTypeId)
        {
            return query.FirstOrDefaultAsync(b => b.BoomTypeId == boomTypeId);
        }
    }
}