﻿using CherryCreekSystems.Domain;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class GreenHouseExtensions
    {
        public static Task<GreenHouseType> ById(this IQueryable<GreenHouseType> query, int id)
        {
            return query.FirstOrDefaultAsync(g => g.Id == id);
        }

        public static IQueryable<GreenHouseType> OrderedByName(this IQueryable<GreenHouseType> query)
        {
            return query.OrderBy(n => n.Name);
        }
    }
}