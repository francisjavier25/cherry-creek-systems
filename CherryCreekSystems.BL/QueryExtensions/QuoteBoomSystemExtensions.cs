﻿using CherryCreekSystems.Domain.BoomComponents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class QuoteBoomSystemExtensions
    {
        public static QuoteBoomSystem FirstOrDefaultActive(this List<QuoteBoomSystem> query)
        {
            return query.FirstOrDefault(q => q.Active);
        }

        public static QuoteBoomSystem FirstOrDefaultActiveById(this IEnumerable<QuoteBoomSystem> query, string id)
        {
            return query.FirstOrDefault(q => q.Active && q.Id_.Equals(id, StringComparison.OrdinalIgnoreCase));
        }

        public static IEnumerable<QuoteBoomSystem> WhereActive(this List<QuoteBoomSystem> query)
        {
            return query.Where(q => q.Active);
        }

        public static void CalculateSystemsProgress(this IEnumerable<QuoteBoomSystem> list)
        {
            foreach (var system in list)
            {
                var boom = system.Boom;
                if (boom == null) continue;

                var informationAndDesignAverage = boom.InfoAndDesignStatus;
                var engineeringAverage = boom.SprayBarApprovalStatus;
                var productionAverage = (boom.AssemblyPickListStatus + boom.ShippingPickListStatus
                                         + boom.ProductionStatus + boom.SawAndSheerStatus + boom.PunchAndBreakStatus
                                         + boom.MachiningStatus
                                         + boom.WeldingStatus) / 7;
                var galvanizerAverage = boom.GalganizerStatus;
                var assemblyAverage = boom.AssemblyStatus;
                var shippingAverage = boom.ShippingStatus;

                var progressData = new List<ProgressData>
                {
                    new ProgressData {value = informationAndDesignAverage.ToString(), userColor = "#d3d3d3"},
                    new ProgressData {value = engineeringAverage.ToString(), userColor = "#add8e6"},
                    new ProgressData {value = productionAverage.ToString(), userColor = "#ffff00"},
                    new ProgressData {value = galvanizerAverage.ToString(), userColor = "#ff69b4"},
                    new ProgressData {value = assemblyAverage.ToString(), userColor = "#ff0000"},
                    new ProgressData {value = shippingAverage.ToString(), userColor = "#00ff00"}
                };

                system.StatusData = JsonConvert.SerializeObject(progressData);
            }
        }

        public static double GetTotalCost(this QuoteBoomSystem system)
        {
            var totalCost = system.AssignedPrice;

            foreach (var quoteCategory in system.QuoteCategories)
            {
                if (quoteCategory.QuoteMajorComponents != null)
                    totalCost += quoteCategory.QuoteMajorComponents.WhereActive().Sum(c => c.AssignedPrice);
            }

            return totalCost;
        }

        /// <summary>
        /// Will order sub-collections by using the Order field
        /// </summary>
        /// <param name="quoteBoomSystem"></param>
        /// <returns></returns>
        public static QuoteBoomSystem Ordered(this QuoteBoomSystem quoteBoomSystem)
        {
            if (quoteBoomSystem.QuoteCategories != null)
            {
                quoteBoomSystem.QuoteCategories = quoteBoomSystem.QuoteCategories.OrderBy(c => c.Order).ToList();
                foreach (var quoteCategory in quoteBoomSystem.QuoteCategories)
                {
                    if (quoteCategory.QuoteMajorComponents != null)
                    {
                        quoteCategory.QuoteMajorComponents = quoteCategory.QuoteMajorComponents.OrderBy(c => c.Order).ToList();
                        foreach (var quoteMajorComponent in quoteCategory.QuoteMajorComponents)
                        {
                            if (quoteMajorComponent.QuoteMajorComponentDetails != null)
                                quoteMajorComponent.QuoteMajorComponentDetails = quoteMajorComponent.QuoteMajorComponentDetails.OrderBy(c => c.Order).ToList();

                            if (quoteMajorComponent.QuoteMinorComponents != null)
                            {
                                quoteMajorComponent.QuoteMinorComponents = quoteMajorComponent.QuoteMinorComponents.OrderBy(c => c.Order).ToList();
                                foreach (var quoteMinorComponent in quoteMajorComponent.QuoteMinorComponents)
                                {
                                    if (quoteMinorComponent.QuoteMinorComponentDetails != null)
                                        quoteMinorComponent.QuoteMinorComponentDetails = quoteMinorComponent.QuoteMinorComponentDetails.OrderBy(c => c.Order).ToList();
                                }
                            }
                        }
                    }
                }
            }

            return quoteBoomSystem;
        }
    }

    public class ProgressData
    {
        public string value { get; set; }
        public string userColor { get; set; }
    }
}