﻿using CherryCreekSystems.Domain.BoomComponents;
using System.Collections.Generic;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class QuoteMajorComponentExtensions
    {
        public static IEnumerable<QuoteMajorComponent> WhereActive(this IEnumerable<QuoteMajorComponent> query)
        {
            return query.Where(c => c.IsActive && c.IsVisible);
        }

        public static List<QuoteMajorComponent> WhereActive(this List<QuoteMajorComponent> query)
        {
            return query.Where(c => c.IsActive && c.IsVisible).ToList();
        }
    }
}