﻿using System.Collections.Generic;
using CherryCreekSystems.Domain.BoomComponents;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class CategoryExtensions
    {
        public static List<Category> OrderAsc(this List<Category> query)
        {
            return query.OrderBy(c => c.Order).ToList();
        }

        public static List<Category> IsActive(this List<Category> query)
        {
            return query.Where(c => c.IsActive).ToList();
        }
    }
}