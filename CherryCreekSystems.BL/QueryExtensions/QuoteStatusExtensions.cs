﻿using System.Linq;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class QuoteStatusExtensions
    {
        public static IQueryable<QuoteStatus> OrderedByName(this IQueryable<QuoteStatus> query)
        {
            return query.OrderBy(s => s.Name);
        }

        public static IQueryable<QuoteStatus> OrderedById(this IQueryable<QuoteStatus> query)
        {
            return query.OrderBy(s => s.Id);
        }
    }
}
