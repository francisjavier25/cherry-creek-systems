﻿using CherryCreekSystems.Domain.BoomComponents;
using System.Collections.Generic;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class MinorComponentExtensions
    {
        public static List<MinorComponent> OrderAsc(this List<MinorComponent> query)
        {
            return query.OrderBy(c => c.Order).ToList();
        }

        public static List<MinorComponent> IsActive(this List<MinorComponent> query)
        {
            return query.Where(c => c.IsActive).ToList();
        }
    }
}