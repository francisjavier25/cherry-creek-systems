﻿using CherryCreekSystems.Domain.BoomComponents;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class BoomSystemExtensions
    {
        public static Task<BoomSystem> ById(this IQueryable<BoomSystem> query, int boomTypeId)
        {
            return query.FirstOrDefaultAsync(b => b.BoomTypeId == boomTypeId);
        }

        public static BoomSystem ById(this List<BoomSystem> query, int boomTypeId)
        {
            return query.FirstOrDefault(b => b.BoomTypeId == boomTypeId);
        }
    }
}