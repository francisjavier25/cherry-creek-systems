﻿using CherryCreekSystems.Domain.BoomComponents;
using System.Collections.Generic;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class MajorComponentDetailsExtensions
    {
        public static List<MajorComponentDetail> OrderAsc(this List<MajorComponentDetail> query)
        {
            return query.OrderBy(c => c.Order).ToList();
        }

        public static List<MajorComponentDetail> IsActive(this List<MajorComponentDetail> query)
        {
            return query.Where(c => c.IsActive).ToList();
        }
    }
}