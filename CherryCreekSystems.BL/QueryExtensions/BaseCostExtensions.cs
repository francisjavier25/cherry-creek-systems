﻿using CherryCreekSystems.Domain;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class BaseCostExtensions
    {
        public static IQueryable<BaseCost> ByBoomSystemId(this IQueryable<BaseCost> query, int boomSystemId)
        {
            return query.Where(c => c.BoomSystemId == boomSystemId);
        }
    }
}