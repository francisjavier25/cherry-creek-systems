﻿using System.Data.Entity;
using System.Linq;
using CherryCreekSystems.Domain;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using CherryCreekSystems.Domain.ViewModels;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class CustomerExtensions
    {
        public static Task<Customer> ById(this IQueryable<Customer> query, int id)
        {
            return query.FirstOrDefaultAsync(c => c.Id == id);
        }

        public static async Task<Customer> BySugarCRMId(this IQueryable<Customer> query, string sugarCRMId)
        {
            return await query.FirstOrDefaultAsync(c => c.SugarCRMId.Equals(sugarCRMId, StringComparison.OrdinalIgnoreCase));
        }

        public static IQueryable<Customer> OrderedByCompanyName(this IQueryable<Customer> query)
        {
            return query.OrderBy(c => c.CompanyName);
        }

        public static IQueryable<Customer> ByDistributorIds(this IQueryable<Customer> query, params int?[] distributorIds)
        {
            return query.Where(c => c.QuoteRequests.Any(q => distributorIds.Contains(q.DistributorId)));
        }

        public static IQueryable<Customer> BySalesRepIds(this IQueryable<Customer> query, params int?[] salesRepIds)
        {
            return query.Where(c => c.QuoteRequests.Any(q => salesRepIds.Contains(q.SalesRepId)));
        }

        public static IQueryable<CustomerUser> BySearch(this IQueryable<CustomerUser> query, string search)
        {
            search = (search ?? string.Empty).ToLower().Trim();
            return query.Where(d => string.IsNullOrEmpty(search) || d.CompanyName.ToLower().Contains(search)
                                                                 || d.ContactName.ToLower().Contains(search)
                                                                 || d.Email.ToLower().Contains(search)
                                                                 || d.Fax.ToLower().Contains(search)
                                                                 || d.Phone.ToLower().Contains(search)
                                                                 || d.UserName.ToLower().Contains(search)
                                                                 );
        }

        public static IQueryable<CustomerUser> GetOrdered(this IQueryable<CustomerUser> query)
        {
            return query.OrderBy(d => d.CompanyName);
        }

        public static IEnumerable<CustomerViewModel> ToViewModel(this IEnumerable<Customer> list)
        {
            return list.Select(c => new CustomerViewModel
            {
                Id = c.Id,
                CompanyName = c.CompanyName
            });
        }

        public static IEnumerable<CustomerViewModel> WithFirstEmptyValue(this IEnumerable<CustomerViewModel> list)
        {
            yield return new CustomerViewModel { Id = null, CompanyName = string.Empty };

            foreach (var c in list)
            {
                yield return c;
            }
        }
    }
}