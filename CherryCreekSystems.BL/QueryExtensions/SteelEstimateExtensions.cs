﻿using System.Collections.Generic;
using CherryCreekSystems.Domain;
using System.Linq;

namespace CherryCreekSystems.BL.QueryExtensions
{
    public static class SteelEstimateExtensions
    {
        public static List<SteelEstimate> ByBoomId(this List<SteelEstimate> query, int boomTypeId)
        {
            return query.Where(c => c.BoomTypeId == boomTypeId).ToList();
        }

        public static bool AnyActive(this List<SteelMaterial> query)
        {
            return query.Any(e => e.Active);
        }

        public static List<SteelMaterial> GetActive(this List<SteelMaterial> query)
        {
            return query.Where(e => e.Active).ToList();
        }

        public static IQueryable<SteelMaterialDefault> OrderById(this IQueryable<SteelMaterialDefault> query)
        {
            return query.OrderBy(s => s.Id);
        }

        public static IQueryable<string> SelectMaterial(this IQueryable<SteelMaterialDefault> query)
        {
            return query.Select(s => s.Material);
        }
    }
}