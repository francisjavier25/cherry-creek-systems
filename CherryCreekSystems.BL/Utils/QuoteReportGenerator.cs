﻿using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Resource;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Fonts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using CherryCreekSystems.BL.QueryExtensions;

namespace CherryCreekSystems.BL
{
    internal class QuoteReportGenerator
    {
        #region private fonts/sizes/characters/spacing/etc

        //Fonts
        private const string Font = "Consolas";

        private const char SpaceCharacter = '\xA0';

        private static readonly Unit SquareWidth = new Unit(0.6, UnitType.Centimeter);

        private static readonly Unit BorderWidth = new Unit(0.06, UnitType.Centimeter);
        private static readonly Unit BottomBorderWidth = new Unit(0.03, UnitType.Centimeter);

        private static readonly Font RedFont = new Font { Color = Color.Parse("red") };

        private static readonly Unit LeftMargin = new Unit(1.2, UnitType.Centimeter);
        private static readonly Unit TopMargin = new Unit(0.3, UnitType.Centimeter);
        private static readonly Unit BottomMargin = new Unit(0.1, UnitType.Centimeter);

        private static readonly Unit LeftIndent = new Unit(2, UnitType.Centimeter);

        private static readonly Unit TermsFontSize = new Unit(0.24, UnitType.Centimeter);

        private static readonly Unit DocsForShippingSize = new Unit(0.30, UnitType.Centimeter);
        private static readonly Unit MajorComponentFontSize = new Unit(0.26, UnitType.Centimeter);

        private static readonly Unit MinorComponentFontSize = new Unit(0.24, UnitType.Centimeter);
        private static readonly Unit ComponentDetailFontSize = new Unit(0.22, UnitType.Centimeter);

        private static readonly Unit FooterInfoFontSize = new Unit(0.19, UnitType.Centimeter);

        private static readonly Unit BorderSize = new Unit(0.1, UnitType.Millimeter);

        private static readonly Unit Spacing = new Unit(0.4, UnitType.Centimeter);
        private static readonly Unit TermsSpacing = new Unit(0.25, UnitType.Centimeter);

        private static readonly Unit LineSpacing = new Unit(0.5, UnitType.Centimeter);

        private readonly string _logoPath;

        private static readonly Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]");

        private static NumberFormatInfo _numberFormat;

        #endregion private fonts/sizes/characters/spacing/etc

        static QuoteReportGenerator()
        {
            GlobalFontSettings.FontResolver = new FontResolver();
        }

        public QuoteReportGenerator()
        {
            _logoPath = Path.GetTempPath() + "CCSLogo.png";
            Resources.CCSLogo.Save(_logoPath);

            var curCulture = Thread.CurrentThread.CurrentCulture.ToString();
            _numberFormat = new CultureInfo(curCulture).NumberFormat;
            _numberFormat.CurrencyNegativePattern = 1;
        }

        #region sales order form

        public static string GenerateSalesOrderForm(QuoteRequest quoteRequest, QuoteBoomSystem system)
        {
            // Create a renderer for the MigraDoc document.

            var pdfRenderer = new PdfDocumentRenderer(false);
            var document = new Document();
            document.DefaultPageSetup.TopMargin = TopMargin * 2;
            document.DefaultPageSetup.LeftMargin = LeftMargin;
            document.DefaultPageSetup.BottomMargin = BottomMargin;

            var style = document.Styles["Normal"];
            style.Font.Name = Font;

            var page = document.AddSection();
            AddPageNumber(page);
            CreateSalesOrderForm(page, quoteRequest, system);

            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;
            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            // Save the document...
            var timeStamp = DateTime.Now.Ticks % 1000;
            var cleanQuoteNumber = illegalInFileName.Replace(quoteRequest.QuoteNumber, "");
            var filename = $"{cleanQuoteNumber}_Sales_Form_{timeStamp}.pdf";
            pdfRenderer.PdfDocument.Save(filename);
            // ...and start a viewer.
            return filename;
        }

        private static void CreateSalesOrderForm(Section page, QuoteRequest quoteRequest, QuoteBoomSystem system)
        {
            var headerParagraph = page.AddParagraph();
            headerParagraph.Format.Font.Bold = true;
            headerParagraph.Format.SpaceAfter = Spacing;
            headerParagraph.AddText("Cherry Creek Systems ‐ Sales Order Check‐Off Sheet");

            var tableForm = page.AddTable();
            tableForm.Borders.Visible = true;
            tableForm.Borders.Width = BorderWidth;
            tableForm.Format.SpaceBefore = Spacing;
            var formFirstColumn = tableForm.AddColumn();
            var formSecondColumn = tableForm.AddColumn();
            var formThirdColumn = tableForm.AddColumn();
            var formSpaceColumn = tableForm.AddColumn();
            var formFourthColumn = tableForm.AddColumn();

            formFirstColumn.Format.Alignment = ParagraphAlignment.Right;
            formFirstColumn.Width = new Unit(8, UnitType.Centimeter);

            formSecondColumn.Format.Alignment = ParagraphAlignment.Left;
            formSecondColumn.Width = new Unit(4, UnitType.Centimeter);

            formThirdColumn.Format.Alignment = ParagraphAlignment.Left;
            formThirdColumn.Width = new Unit(4, UnitType.Centimeter);

            formSpaceColumn.Format.Alignment = ParagraphAlignment.Left;
            formSpaceColumn.Width = new Unit(0.8, UnitType.Centimeter);

            formSpaceColumn.Borders.Top.Width = 0;
            formSpaceColumn.Borders.Left.Width = 0;
            formSpaceColumn.Borders.Right.Width = 0;
            formSpaceColumn.Borders.Bottom.Width = 0;

            formFourthColumn.Format.Alignment = ParagraphAlignment.Left;
            formFourthColumn.Width = new Unit(2, UnitType.Centimeter);

            //Customer Name
            var formFirstRow = tableForm.AddRow();

            var formFirstRowFirstCell = formFirstRow[0];
            formFirstRowFirstCell.Borders.Bottom.Width = 0;
            formFirstRowFirstCell.Borders.Right.Width = 0;
            formFirstRowFirstCell.AddParagraph("Customer Name:");

            var formFirstRowSecondCell = formFirstRow[1];
            formFirstRowSecondCell.Borders.Left.Width = 0;
            formFirstRowSecondCell.MergeRight = 3;
            formFirstRowSecondCell.AddParagraph(quoteRequest.Customer.CompanyName);

            //Customer Phone
            var formSecondRow = tableForm.AddRow();

            var formSecondRowFirstCell = formSecondRow[0];
            formSecondRowFirstCell.Borders.Top.Width = 0;
            formSecondRowFirstCell.Borders.Bottom.Width = 0;
            formSecondRowFirstCell.Borders.Right.Width = 0;
            formSecondRowFirstCell.AddParagraph("Customer Phone #:");

            var formSecondRowSecondCell = formSecondRow[1];
            formSecondRowSecondCell.Borders.Left.Width = 0;
            formSecondRowSecondCell.MergeRight = 3;
            formSecondRowSecondCell.AddParagraph(quoteRequest.Phone);

            //Distributor
            var formThirdRow = tableForm.AddRow();

            var formThirdRowFirstCell = formThirdRow[0];
            formThirdRowFirstCell.Borders.Top.Width = 0;
            formThirdRowFirstCell.Borders.Bottom.Width = 0;
            formThirdRowFirstCell.Borders.Right.Width = 0;
            formThirdRowFirstCell.AddParagraph("Distributor:");

            var formThirdRowThirdCell = formThirdRow[1];
            formThirdRowThirdCell.Borders.Left.Width = 0;
            formThirdRowThirdCell.MergeRight = 3;
            formThirdRowThirdCell.AddParagraph(quoteRequest.Distributor.Name);

            //System type
            var formFourthRow = tableForm.AddRow();

            var formFourthRowFirstCell = formFourthRow[0];
            formFourthRowFirstCell.Borders.Top.Width = 0;
            formFourthRowFirstCell.Borders.Bottom.Width = 0;
            formFourthRowFirstCell.Borders.Right.Width = 0;
            formFourthRowFirstCell.AddParagraph("System Type:");

            var formFourthRowFourthCell = formFourthRow[1];
            formFourthRowFourthCell.Borders.Left.Width = 0;
            formFourthRowFourthCell.MergeRight = 3;
            formFourthRowFourthCell.AddParagraph(system.SystemName);

            //Quantity Ordered
            var formFifthRow = tableForm.AddRow();

            var formFifthRowFirstCell = formFifthRow[0];
            formFifthRowFirstCell.Borders.Top.Width = 0;
            formFifthRowFirstCell.Borders.Bottom.Width = 0;
            formFifthRowFirstCell.Borders.Right.Width = 0;
            formFifthRowFirstCell.AddParagraph("Quantity Ordered:");

            var formFifthRowSecondCell = formFifthRow[1];
            formFifthRowSecondCell.Borders.Left.Width = 0;
            formFifthRowSecondCell.Borders.Right.Width = 0;
            formFifthRowSecondCell.AddParagraph(system.Quantity.ToString());

            var formFifthRowThirdCell = formFifthRow[2];
            formFifthRowThirdCell.Format.Alignment = ParagraphAlignment.Center;
            formFifthRowThirdCell.Borders.Left.Width = 0;
            formFifthRowThirdCell.Borders.Bottom.Width = 0;
            formFifthRowThirdCell.MergeRight = 2;
            formFifthRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formFifthRowThirdCell.AddParagraph("Manual & Docs For Shipping:");

            //Confirmed Order Date
            var formSixthRow = tableForm.AddRow();

            var formSixthRowFirstCell = formSixthRow[0];
            formSixthRowFirstCell.Borders.Top.Width = 0;
            formSixthRowFirstCell.Borders.Bottom.Width = 0;
            formSixthRowFirstCell.Borders.Right.Width = 0;
            formSixthRowFirstCell.AddParagraph("Confirmed Order Date:");

            var formSixthRowSecondCell = formSixthRow[1];
            formSixthRowSecondCell.Borders.Left.Width = 0;
            formSixthRowSecondCell.Borders.Right.Width = 0;
            formSixthRowSecondCell.AddParagraph(quoteRequest.OrderDate.ToString("MM/dd/yyyy"));

            var formSixthRowThirdCell = formSixthRow[2];
            formSixthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formSixthRowThirdCell.Borders.Top.Width = 0;
            formSixthRowThirdCell.Borders.Left.Width = 0;
            formSixthRowThirdCell.Borders.Right.Width = 0;
            formSixthRowThirdCell.Borders.Bottom.Width = 0;
            formSixthRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formSixthRowThirdCell.AddParagraph("Installation - Main");

            var formSixthRowFourthCell = formSixthRow[4];
            formSixthRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formSixthRowFourthCell.Borders.Top.Width = 0;
            formSixthRowFourthCell.Borders.Left.Width = 0;
            formSixthRowFourthCell.Borders.Bottom.Width = 0;

            var squareFrame = formSixthRowFourthCell.AddTextFrame();
            squareFrame.Width = SquareWidth;
            squareFrame.Height = SquareWidth;
            squareFrame.LineFormat.Width = BorderWidth;

            //Expected Shipment Date
            var formSeventhRow = tableForm.AddRow();

            var formSeventhRowFirstCell = formSeventhRow[0];
            formSeventhRowFirstCell.Borders.Top.Width = 0;
            formSeventhRowFirstCell.Borders.Bottom.Width = 0;
            formSeventhRowFirstCell.Borders.Right.Width = 0;
            formSeventhRowFirstCell.AddParagraph("Expected Shipment Date:");

            var formSeventhRowSecondCell = formSeventhRow[1];
            formSeventhRowSecondCell.Borders.Left.Width = 0;
            formSeventhRowSecondCell.Borders.Right.Width = 0;
            formSeventhRowSecondCell.AddParagraph(quoteRequest.DueDate.ToString("MM/dd/yyyy"));

            var formSeventhRowThirdCell = formSeventhRow[2];
            formSeventhRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formSeventhRowThirdCell.Borders.Top.Width = 0;
            formSeventhRowThirdCell.Borders.Left.Width = 0;
            formSeventhRowThirdCell.Borders.Right.Width = 0;
            formSeventhRowThirdCell.Borders.Bottom.Width = 0;
            formSeventhRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formSeventhRowThirdCell.AddParagraph("Installation - CWF");

            var formSeventhRowFourthCell = formSeventhRow[4];
            formSeventhRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formSeventhRowFourthCell.Borders.Top.Width = 0;
            formSeventhRowFourthCell.Borders.Left.Width = 0;
            formSeventhRowFourthCell.Borders.Bottom.Width = 0;

            formSeventhRowFourthCell.Add(squareFrame.Clone());

            //Signed Order Received date
            var formEightRow = tableForm.AddRow();

            var formEightRowFirstCell = formEightRow[0];
            formEightRowFirstCell.Borders.Top.Width = 0;
            formEightRowFirstCell.Borders.Bottom.Width = 0;
            formEightRowFirstCell.Borders.Right.Width = 0;
            formEightRowFirstCell.AddParagraph("Down Payment Received (Date):");

            var formEightRowSecondCell = formEightRow[1];
            formEightRowSecondCell.Borders.Left.Width = 0;
            formEightRowSecondCell.Borders.Right.Width = 0;

            var formEightRowThirdCell = formEightRow[2];
            formEightRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formEightRowThirdCell.Borders.Top.Width = 0;
            formEightRowThirdCell.Borders.Left.Width = 0;
            formEightRowThirdCell.Borders.Right.Width = 0;
            formEightRowThirdCell.Borders.Bottom.Width = 0;
            formEightRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formEightRowThirdCell.AddParagraph("Install - Transfer");

            var formEightRowFourthCell = formEightRow[4];
            formEightRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formEightRowFourthCell.Borders.Top.Width = 0;
            formEightRowFourthCell.Borders.Left.Width = 0;
            formEightRowFourthCell.Borders.Bottom.Width = 0;

            formEightRowFourthCell.Add(squareFrame.Clone());

            //Signed Order Received date
            var formNinthRow = tableForm.AddRow();

            var formNinthRowFirstCell = formNinthRow[0];
            formNinthRowFirstCell.Borders.Top.Width = 0;
            formNinthRowFirstCell.Borders.Bottom.Width = 0;
            formNinthRowFirstCell.Borders.Right.Width = 0;
            formNinthRowFirstCell.AddParagraph("Signed Order Received (Date):");

            var formNinthRowSecondCell = formNinthRow[1];
            formNinthRowSecondCell.Borders.Left.Width = 0;
            formNinthRowSecondCell.Borders.Right.Width = 0;

            var formNinthRowThirdCell = formNinthRow[2];
            formNinthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formNinthRowThirdCell.Borders.Top.Width = 0;
            formNinthRowThirdCell.Borders.Left.Width = 0;
            formNinthRowThirdCell.Borders.Right.Width = 0;
            formNinthRowThirdCell.Borders.Bottom.Width = 0;
            formNinthRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formNinthRowThirdCell.AddParagraph("Controller - Main");

            var formNinthRowFourthCell = formNinthRow[4];
            formNinthRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formNinthRowFourthCell.Borders.Top.Width = 0;
            formNinthRowFourthCell.Borders.Left.Width = 0;
            formNinthRowFourthCell.Borders.Bottom.Width = 0;

            formNinthRowFourthCell.Add(squareFrame.Clone());

            //Engineering
            var formTenthRow = tableForm.AddRow();

            var formTenthRowFirstCell = formTenthRow[0];
            formTenthRowFirstCell.Format.SpaceBefore = new Unit(0.2, UnitType.Centimeter);
            formTenthRowFirstCell.Format.Alignment = ParagraphAlignment.Center;
            formTenthRowFirstCell.Format.Font.Bold = true;
            formTenthRowFirstCell.Borders.Top.Width = BorderWidth;
            formTenthRowFirstCell.Borders.Bottom.Width = BorderWidth;
            formTenthRowFirstCell.Borders.Right.Width = BorderWidth;
            formTenthRowFirstCell.AddParagraph("Engineering");

            var formTenthRowSecondCell = formTenthRow[1];
            formTenthRowSecondCell.Borders.Left.Width = 0;
            formTenthRowSecondCell.Borders.Right.Width = 0;
            formTenthRowSecondCell.Borders.Bottom.Width = 0;

            var formTenthRowThirdCell = formTenthRow[2];
            formTenthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formTenthRowThirdCell.Borders.Top.Width = 0;
            formTenthRowThirdCell.Borders.Left.Width = 0;
            formTenthRowThirdCell.Borders.Right.Width = 0;
            formTenthRowThirdCell.Borders.Bottom.Width = 0;
            formTenthRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formTenthRowThirdCell.AddParagraph("Controller - Appendix");

            var formTenthRowFourthCell = formTenthRow[4];
            formTenthRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formTenthRowFourthCell.Borders.Top.Width = 0;
            formTenthRowFourthCell.Borders.Left.Width = 0;
            formTenthRowFourthCell.Borders.Bottom.Width = 0;

            formTenthRowFourthCell.Add(squareFrame.Clone());

            //File Review date
            var formEleventhRow = tableForm.AddRow();

            var formEleventhRowFirstCell = formEleventhRow[0];
            formEleventhRowFirstCell.Borders.Top.Width = 0;
            formEleventhRowFirstCell.Borders.Bottom.Width = 0;
            formEleventhRowFirstCell.Borders.Right.Width = 0;
            formEleventhRowFirstCell.AddParagraph("File Review Date:");

            var formEleventhRowSecondCell = formEleventhRow[1];
            formEleventhRowSecondCell.Borders.Left.Width = 0;
            formEleventhRowSecondCell.Borders.Right.Width = 0;
            formEleventhRowSecondCell.Borders.Top.Width = 0;

            var formEleventhRowThirdCell = formEleventhRow[2];
            formEleventhRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formEleventhRowThirdCell.Borders.Top.Width = 0;
            formEleventhRowThirdCell.Borders.Left.Width = 0;
            formEleventhRowThirdCell.Borders.Right.Width = 0;
            formEleventhRowThirdCell.Borders.Bottom.Width = 0;
            formEleventhRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formEleventhRowThirdCell.AddParagraph("Customer Pick-List");

            var formEleventhRowFourthCell = formEleventhRow[4];
            formEleventhRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formEleventhRowFourthCell.Borders.Top.Width = 0;
            formEleventhRowFourthCell.Borders.Left.Width = 0;
            formEleventhRowFourthCell.Borders.Bottom.Width = 0;

            formEleventhRowFourthCell.Add(squareFrame.Clone());

            //Engineering Checklist Start Date
            var formTwelfthRow = tableForm.AddRow();

            var formTwelfthRowFirstCell = formTwelfthRow[0];
            formTwelfthRowFirstCell.Borders.Top.Width = 0;
            formTwelfthRowFirstCell.Borders.Bottom.Width = 0;
            formTwelfthRowFirstCell.Borders.Right.Width = 0;
            formTwelfthRowFirstCell.AddParagraph("Engineering Checklist Start Date:");

            var formTwelfthRowSecondCell = formTwelfthRow[1];
            formTwelfthRowSecondCell.Borders.Left.Width = 0;
            formTwelfthRowSecondCell.Borders.Right.Width = 0;

            var formTwelfthRowThirdCell = formTwelfthRow[2];
            formTwelfthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formTwelfthRowThirdCell.Borders.Top.Width = 0;
            formTwelfthRowThirdCell.Borders.Left.Width = 0;
            formTwelfthRowThirdCell.Borders.Right.Width = 0;
            formTwelfthRowThirdCell.Borders.Bottom.Width = 0;
            formTwelfthRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formTwelfthRowThirdCell.AddParagraph("Dosatron Handout");

            var formTwelfthRowFourthCell = formTwelfthRow[4];
            formTwelfthRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formTwelfthRowFourthCell.Borders.Top.Width = 0;
            formTwelfthRowFourthCell.Borders.Left.Width = 0;
            formTwelfthRowFourthCell.Borders.Bottom.Width = 0;

            formTwelfthRowFourthCell.Add(squareFrame.Clone());

            //Checklist Complete Date
            var formThirtheenthRow = tableForm.AddRow();

            var formThirtheenthRowFirstCell = formThirtheenthRow[0];
            formThirtheenthRowFirstCell.Borders.Top.Width = 0;
            formThirtheenthRowFirstCell.Borders.Bottom.Width = 0;
            formThirtheenthRowFirstCell.Borders.Right.Width = 0;
            formThirtheenthRowFirstCell.AddParagraph("Checklist Complete Date:");

            var formThirtheenthRowSecondCell = formThirtheenthRow[1];
            formThirtheenthRowSecondCell.Borders.Left.Width = 0;
            formThirtheenthRowSecondCell.Borders.Right.Width = 0;

            var formThirtheenthRowThirdCell = formThirtheenthRow[2];
            formThirtheenthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formThirtheenthRowThirdCell.Borders.Top.Width = 0;
            formThirtheenthRowThirdCell.Borders.Left.Width = 0;
            formThirtheenthRowThirdCell.Borders.Right.Width = 0;
            formThirtheenthRowThirdCell.Borders.Bottom.Width = 0;
            formThirtheenthRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formThirtheenthRowThirdCell.AddParagraph("Strip-It Handout");

            var formThirtheenthRowFourthCell = formThirtheenthRow[4];
            formThirtheenthRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formThirtheenthRowFourthCell.Borders.Top.Width = 0;
            formThirtheenthRowFourthCell.Borders.Left.Width = 0;
            formThirtheenthRowFourthCell.Borders.Bottom.Width = 0;

            formThirtheenthRowFourthCell.Add(squareFrame.Clone());

            //Spraybar Prints Sent to Customer (Date):
            var formFourtheenthRow = tableForm.AddRow();

            var formFourtheenthRowFirstCell = formFourtheenthRow[0];
            formFourtheenthRowFirstCell.Borders.Top.Width = 0;
            formFourtheenthRowFirstCell.Borders.Bottom.Width = 0;
            formFourtheenthRowFirstCell.Borders.Right.Width = 0;
            formFourtheenthRowFirstCell.AddParagraph("Spraybar Prints Sent to Customer (Date):");

            var formFourtheenthRowSecondCell = formFourtheenthRow[1];
            formFourtheenthRowSecondCell.Borders.Left.Width = 0;
            formFourtheenthRowSecondCell.Borders.Right.Width = 0;

            var formFourtheenthRowThirdCell = formFourtheenthRow[2];
            formFourtheenthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formFourtheenthRowThirdCell.Borders.Top.Width = 0;
            formFourtheenthRowThirdCell.Borders.Left.Width = 0;
            formFourtheenthRowThirdCell.Borders.Right.Width = 0;
            formFourtheenthRowThirdCell.Borders.Bottom.Width = 0;
            formFourtheenthRowThirdCell.Format.Font.Size = DocsForShippingSize;
            formFourtheenthRowThirdCell.AddParagraph("Promotional Item");

            var formFourtheenthRowFourthCell = formFourtheenthRow[4];
            formFourtheenthRowFourthCell.VerticalAlignment = VerticalAlignment.Bottom;
            formFourtheenthRowFourthCell.Borders.Top.Width = 0;
            formFourtheenthRowFourthCell.Borders.Left.Width = 0;
            formFourtheenthRowFourthCell.Borders.Bottom.Width = 0;

            formFourtheenthRowFourthCell.Add(squareFrame.Clone());

            //Approval & Sign‐off Date:
            var formFiftheenthRow = tableForm.AddRow();

            var formFiftheenthRowFirstCell = formFiftheenthRow[0];
            formFiftheenthRowFirstCell.Borders.Top.Width = 0;
            formFiftheenthRowFirstCell.Borders.Bottom.Width = 0;
            formFiftheenthRowFirstCell.Borders.Right.Width = 0;
            formFiftheenthRowFirstCell.AddParagraph("Approval & Sign‐off Date:");

            var formFiftheenthRowSecondCell = formFiftheenthRow[1];
            formFiftheenthRowSecondCell.Borders.Left.Width = 0;
            formFiftheenthRowSecondCell.Borders.Right.Width = 0;

            var formFiftheenthRowThirdCell = formFiftheenthRow[2];
            formFiftheenthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formFiftheenthRowThirdCell.Borders.Top.Width = 0;
            formFiftheenthRowThirdCell.Borders.Left.Width = 0;
            formFiftheenthRowThirdCell.Borders.Right.Width = 0;
            formFiftheenthRowThirdCell.Borders.Bottom.Width = 0;
            formFiftheenthRowThirdCell.Format.Font.Size = DocsForShippingSize;

            var formFiftheenthRowFourthCell = formFiftheenthRow[4];
            formFiftheenthRowFourthCell.Borders.Top.Width = 0;
            formFiftheenthRowFourthCell.Borders.Left.Width = 0;
            formFiftheenthRowFourthCell.Borders.Bottom.Width = 0;

            //Production
            var formSixteenthRow = tableForm.AddRow();

            var formSixteenthRowFirstCell = formSixteenthRow[0];
            formSixteenthRowFirstCell.Format.SpaceBefore = new Unit(0.2, UnitType.Centimeter);
            formSixteenthRowFirstCell.Format.Alignment = ParagraphAlignment.Center;
            formSixteenthRowFirstCell.Format.Font.Bold = true;
            formSixteenthRowFirstCell.Borders.Top.Width = BorderWidth;
            formSixteenthRowFirstCell.Borders.Bottom.Width = BorderWidth;
            formSixteenthRowFirstCell.Borders.Right.Width = BorderWidth;
            formSixteenthRowFirstCell.AddParagraph("Production");

            var formSixteenthRowSecondCell = formSixteenthRow[1];
            formSixteenthRowSecondCell.Borders.Left.Width = 0;
            formSixteenthRowSecondCell.Borders.Right.Width = 0;
            formSixteenthRowSecondCell.Borders.Bottom.Width = 0;

            var formSixteenthRowThirdCell = formSixteenthRow[2];
            formSixteenthRowThirdCell.Borders.Top.Width = 0;
            formSixteenthRowThirdCell.Borders.Left.Width = 0;
            formSixteenthRowThirdCell.Borders.Right.Width = 0;
            formSixteenthRowThirdCell.Borders.Bottom.Width = 0;

            var formSixteenthRowFourthCell = formSixteenthRow[4];
            formSixteenthRowFourthCell.Borders.Top.Width = 0;
            formSixteenthRowFourthCell.Borders.Left.Width = 0;
            formSixteenthRowFourthCell.Borders.Bottom.Width = 0;

            //File Reviewed Date:
            var formSeventeenthRow = tableForm.AddRow();

            var formSeventeenthRowFirstCell = formSeventeenthRow[0];
            formSeventeenthRowFirstCell.Borders.Top.Width = 0;
            formSeventeenthRowFirstCell.Borders.Bottom.Width = 0;
            formSeventeenthRowFirstCell.Borders.Right.Width = 0;
            formSeventeenthRowFirstCell.AddParagraph("File Reviewed Date:");

            var formSeventeenthRowSecondCell = formSeventeenthRow[1];
            formSeventeenthRowSecondCell.Borders.Top.Width = 0;
            formSeventeenthRowSecondCell.Borders.Left.Width = 0;
            formSeventeenthRowSecondCell.Borders.Right.Width = 0;

            var formSeventeenthRowThirdCell = formSeventeenthRow[2];
            formSeventeenthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formSeventeenthRowThirdCell.Borders.Top.Width = 0;
            formSeventeenthRowThirdCell.Borders.Left.Width = 0;
            formSeventeenthRowThirdCell.Borders.Right.Width = 0;
            formSeventeenthRowThirdCell.Borders.Bottom.Width = 0;
            formSeventeenthRowThirdCell.Format.Font.Size = DocsForShippingSize;

            var formSeventeenthRowFourthCell = formSeventeenthRow[4];
            formSeventeenthRowFourthCell.Borders.Top.Width = 0;
            formSeventeenthRowFourthCell.Borders.Left.Width = 0;
            formSeventeenthRowFourthCell.Borders.Bottom.Width = 0;

            //Production Checklist Start Date:
            var formEightteenthRow = tableForm.AddRow();

            var formEightteenthRowFirstCell = formEightteenthRow[0];
            formEightteenthRowFirstCell.Borders.Top.Width = 0;
            formEightteenthRowFirstCell.Borders.Bottom.Width = 0;
            formEightteenthRowFirstCell.Borders.Right.Width = 0;
            formEightteenthRowFirstCell.AddParagraph("Production Checklist Start Date:");

            var formEightteenthRowSecondCell = formEightteenthRow[1];
            formEightteenthRowSecondCell.Borders.Left.Width = 0;
            formEightteenthRowSecondCell.Borders.Right.Width = 0;

            var formEightteenthRowThirdCell = formEightteenthRow[2];
            formEightteenthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formEightteenthRowThirdCell.Borders.Top.Width = 0;
            formEightteenthRowThirdCell.Borders.Left.Width = 0;
            formEightteenthRowThirdCell.Borders.Right.Width = 0;
            formEightteenthRowThirdCell.Borders.Bottom.Width = 0;
            formEightteenthRowThirdCell.Format.Font.Size = DocsForShippingSize;

            var formEightteenthRowFourthCell = formEightteenthRow[4];
            formEightteenthRowFourthCell.Borders.Top.Width = 0;
            formEightteenthRowFourthCell.Borders.Left.Width = 0;
            formEightteenthRowFourthCell.Borders.Bottom.Width = 0;

            //Checklist Complete Date:
            var formNineteenthRow = tableForm.AddRow();

            var formNineteenthRowFirstCell = formNineteenthRow[0];
            formNineteenthRowFirstCell.Borders.Top.Width = 0;
            formNineteenthRowFirstCell.Borders.Bottom.Width = 0;
            formNineteenthRowFirstCell.Borders.Right.Width = 0;
            formNineteenthRowFirstCell.AddParagraph("Checklist Complete Date:");

            var formNineteenthRowSecondCell = formNineteenthRow[1];
            formNineteenthRowSecondCell.Borders.Left.Width = 0;
            formNineteenthRowSecondCell.Borders.Right.Width = 0;

            var formNineteenthRowThirdCell = formNineteenthRow[2];
            formNineteenthRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formNineteenthRowThirdCell.Borders.Top.Width = 0;
            formNineteenthRowThirdCell.Borders.Left.Width = 0;
            formNineteenthRowThirdCell.Borders.Right.Width = 0;
            formNineteenthRowThirdCell.Borders.Bottom.Width = 0;
            formNineteenthRowThirdCell.Format.Font.Size = DocsForShippingSize;

            var formNineteenthRowFourthCell = formNineteenthRow[4];
            formNineteenthRowFourthCell.Borders.Top.Width = 0;
            formNineteenthRowFourthCell.Borders.Left.Width = 0;
            formNineteenthRowFourthCell.Borders.Bottom.Width = 0;

            //Released to Floor (Date):
            var formTwentiethRow = tableForm.AddRow();

            var formTwentiethRowFirstCell = formTwentiethRow[0];
            formTwentiethRowFirstCell.Borders.Top.Width = 0;
            formTwentiethRowFirstCell.Borders.Bottom.Width = 0;
            formTwentiethRowFirstCell.Borders.Right.Width = 0;
            formTwentiethRowFirstCell.AddParagraph("Released to Floor (Date):");

            var formTwentiethRowSecondCell = formTwentiethRow[1];
            formTwentiethRowSecondCell.Borders.Left.Width = 0;
            formTwentiethRowSecondCell.Borders.Right.Width = 0;

            var formTwentiethRowThirdCell = formTwentiethRow[2];
            formTwentiethRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formTwentiethRowThirdCell.Borders.Top.Width = 0;
            formTwentiethRowThirdCell.Borders.Left.Width = 0;
            formTwentiethRowThirdCell.Borders.Right.Width = 0;
            formTwentiethRowThirdCell.Borders.Bottom.Width = 0;
            formTwentiethRowThirdCell.Format.Font.Size = DocsForShippingSize;

            var formTwentiethRowFourthCell = formTwentiethRow[4];
            formTwentiethRowFourthCell.Borders.Top.Width = 0;
            formTwentiethRowFourthCell.Borders.Left.Width = 0;
            formTwentiethRowFourthCell.Borders.Bottom.Width = 0;

            //Pick‐List Released to Shipping (Date):
            var formTwentyOneRow = tableForm.AddRow();

            var formTwentyOneRowFirstCell = formTwentyOneRow[0];
            formTwentyOneRowFirstCell.Borders.Top.Width = 0;
            formTwentyOneRowFirstCell.Borders.Bottom.Width = 0;
            formTwentyOneRowFirstCell.Borders.Right.Width = 0;
            formTwentyOneRowFirstCell.AddParagraph("Pick‐List Released to Shipping (Date):");

            var formTwentyOneRowSecondCell = formTwentyOneRow[1];
            formTwentyOneRowSecondCell.Borders.Left.Width = 0;
            formTwentyOneRowSecondCell.Borders.Right.Width = 0;

            var formTwentyOneRowThirdCell = formTwentyOneRow[2];
            formTwentyOneRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formTwentyOneRowThirdCell.Borders.Top.Width = 0;
            formTwentyOneRowThirdCell.Borders.Left.Width = 0;
            formTwentyOneRowThirdCell.Borders.Right.Width = 0;
            formTwentyOneRowThirdCell.Borders.Bottom.Width = 0;
            formTwentyOneRowThirdCell.Format.Font.Size = DocsForShippingSize;

            var formTwentyOneRowFourthCell = formTwentyOneRow[4];
            formTwentyOneRowFourthCell.Borders.Top.Width = 0;
            formTwentyOneRowFourthCell.Borders.Left.Width = 0;
            formTwentyOneRowFourthCell.Borders.Bottom.Width = 0;

            //Actual Shipment Date:
            var formTwentyTwoRow = tableForm.AddRow();

            var formTwentyTwoRowFirstCell = formTwentyTwoRow[0];
            formTwentyTwoRowFirstCell.Borders.Top.Width = 0;
            formTwentyTwoRowFirstCell.Borders.Bottom.Width = 0;
            formTwentyTwoRowFirstCell.Borders.Right.Width = 0;
            formTwentyTwoRowFirstCell.AddParagraph("Actual Shipment Date:");

            var formTwentyTwoRowSecondCell = formTwentyTwoRow[1];
            formTwentyTwoRowSecondCell.Borders.Left.Width = 0;
            formTwentyTwoRowSecondCell.Borders.Right.Width = 0;

            var formTwentyTwoRowThirdCell = formTwentyTwoRow[2];
            formTwentyTwoRowThirdCell.Format.Alignment = ParagraphAlignment.Right;
            formTwentyTwoRowThirdCell.Borders.Top.Width = 0;
            formTwentyTwoRowThirdCell.Borders.Left.Width = 0;
            formTwentyTwoRowThirdCell.Borders.Right.Width = 0;
            formTwentyTwoRowThirdCell.Borders.Bottom.Width = 0;
            formTwentyTwoRowThirdCell.Format.Font.Size = DocsForShippingSize;

            var formTwentyTwoRowFourthCell = formTwentyTwoRow[4];
            formTwentyTwoRowFourthCell.Borders.Top.Width = 0;
            formTwentyTwoRowFourthCell.Borders.Left.Width = 0;
            formTwentyTwoRowFourthCell.Borders.Bottom.Width = 0;

            //Special Notes
            var formTwentyThreeRow = tableForm.AddRow();

            var formTwentyThreeRowFirstCell = formTwentyThreeRow[0];
            formTwentyThreeRowFirstCell.Format.Alignment = ParagraphAlignment.Left;
            formTwentyThreeRowFirstCell.Format.Font.Bold = true;
            formTwentyThreeRowFirstCell.Borders.Top.Width = 0;
            formTwentyThreeRowFirstCell.MergeRight = 4;
            formTwentyThreeRowFirstCell.AddParagraph("Special Notes:");

            //Special Notes line 1
            var formTwentyFourRow = tableForm.AddRow();

            var formTwentyFourRowFirstCell = formTwentyFourRow[0];
            formTwentyFourRowFirstCell.Format.Alignment = ParagraphAlignment.Left;
            formTwentyFourRowFirstCell.Format.Font.Bold = true;
            formTwentyFourRowFirstCell.Borders.Top.Width = 0;
            formTwentyFourRowFirstCell.Borders.Bottom.Width = BottomBorderWidth;
            formTwentyFourRowFirstCell.MergeRight = 4;

            //Special Notes line 2
            var formTwentyFiveRow = tableForm.AddRow();

            var formTwentyFiveRowFirstCell = formTwentyFiveRow[0];
            formTwentyFiveRowFirstCell.Format.Alignment = ParagraphAlignment.Left;
            formTwentyFiveRowFirstCell.Format.Font.Bold = true;
            formTwentyFiveRowFirstCell.Borders.Top.Width = 0;
            formTwentyFiveRowFirstCell.Borders.Bottom.Width = BottomBorderWidth;
            formTwentyFiveRowFirstCell.MergeRight = 4;

            //Special Notes line 3
            var formTwentySixRow = tableForm.AddRow();

            var formTwentySixRowFirstCell = formTwentySixRow[0];
            formTwentySixRowFirstCell.Format.Alignment = ParagraphAlignment.Left;
            formTwentySixRowFirstCell.Format.Font.Bold = true;
            formTwentySixRowFirstCell.Borders.Top.Width = 0;
            formTwentySixRowFirstCell.Borders.Bottom.Width = BottomBorderWidth;
            formTwentySixRowFirstCell.MergeRight = 4;

            //Special Notes line 4
            var formTwentySevenRow = tableForm.AddRow();

            var formTwentySevenRowFirstCell = formTwentySevenRow[0];
            formTwentySevenRowFirstCell.Format.Alignment = ParagraphAlignment.Left;
            formTwentySevenRowFirstCell.Format.Font.Bold = true;
            formTwentySevenRowFirstCell.Borders.Top.Width = 0;
            formTwentySevenRowFirstCell.Borders.Bottom.Width = BorderWidth;
            formTwentySevenRowFirstCell.MergeRight = 4;
        }

        #endregion sales order form

        #region detailed quote report

        public string GenerateDetailQuoteReport(QuoteBoomSystem quoteBoomSystem, QuoteRequest quoteRequest)
        {
            var report = GetDetailReport(quoteBoomSystem, quoteRequest);
            // Save the document...
            var fileName = CreateDetailQuoteReportFileName(quoteRequest.QuoteNumber, quoteBoomSystem.SystemName);
            report.PdfDocument.Save(fileName);

            return fileName;
        }

        public Stream GenerateDetailQuoteStream(QuoteBoomSystem quoteBoomSystem, QuoteRequest quoteRequest)
        {
            var stream = new MemoryStream();
            var report = GetDetailReport(quoteBoomSystem, quoteRequest);
            report.PdfDocument.Save(stream, closeStream: false);
            return stream;
        }

        public string CreateDetailQuoteReportFileName(string quoteNumber, string systemName)
        {
            var timeStamp = DateTime.Now.Ticks % 1000;
            var cleanQuoteNumber = illegalInFileName.Replace(quoteNumber, "");
            var cleanSystemName = illegalInFileName.Replace(systemName, "");
            var filename = $"{cleanQuoteNumber}_{cleanSystemName}_{timeStamp}.pdf";

            return filename;
        }

        private PdfDocumentRenderer GetDetailReport(QuoteBoomSystem quoteBoomSystem, QuoteRequest quoteRequest)
        {
            //PdfFontEmbedding embedding = PdfFontEmbedding.Always;

            // Create a renderer for the MigraDoc document.

            var pdfRenderer = new PdfDocumentRenderer(false);
            var document = new Document();
            document.DefaultPageSetup.TopMargin = TopMargin;
            document.DefaultPageSetup.LeftMargin = LeftMargin;
            document.DefaultPageSetup.BottomMargin = BottomMargin;

            var style = document.Styles["Normal"];
            style.Font.Name = Font;

            var detailPage = document.AddSection();
            AddPageNumber(detailPage);

            PopulateDetailPage(detailPage, quoteBoomSystem, quoteRequest, quoteBoomSystem.Boom, fromGeneralReport: false);

            if (!string.IsNullOrWhiteSpace(quoteBoomSystem.Boom.AdditionalInformation))
            {
                var additionalInformationPage = document.AddSection();
                AddPageNumber(additionalInformationPage);
                CreateAdditionalInformationPage(additionalInformationPage, quoteBoomSystem.Boom);
            }

            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;
            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            return pdfRenderer;
        }

        private static void AddPageNumber(Section detailPage)
        {
            var pageNumberFrame = detailPage.AddTextFrame();
            pageNumberFrame.RelativeHorizontal = RelativeHorizontal.Page;
            pageNumberFrame.RelativeVertical = RelativeVertical.Page;
            pageNumberFrame.WrapFormat.DistanceLeft = new Unit(20, UnitType.Centimeter);
            pageNumberFrame.WrapFormat.DistanceTop = new Unit(29, UnitType.Centimeter);
            var pageNumberParagraph = pageNumberFrame.AddParagraph();
            pageNumberParagraph.Format.Font.Size = DocsForShippingSize;
            pageNumberParagraph.AddPageField();
        }

        private void CreateAdditionalInformationPage(Section page, Boom boom)
        {
            CreateHeader(page, _logoPath);
            CreateAdditionalInformationParagraph(page, boom);
        }

        private static void CreateAdditionalInformationParagraph(Section page, Boom boom)
        {
            var additionalInformationParagraphTitle = page.AddParagraph();
            additionalInformationParagraphTitle.Format.LeftIndent = LeftIndent;
            additionalInformationParagraphTitle.Format.Alignment = ParagraphAlignment.Center;
            additionalInformationParagraphTitle.Format.SpaceBefore = Spacing * 2;
            additionalInformationParagraphTitle.Format.Font.Bold = true;
            additionalInformationParagraphTitle.Format.Font.Underline = Underline.Single;
            additionalInformationParagraphTitle.Format.Font.Size = MajorComponentFontSize;
            additionalInformationParagraphTitle.AddText("Additional Information");

            var additionalInformationParagraph = page.AddParagraph();
            additionalInformationParagraph.Format.LineSpacingRule = LineSpacingRule.Exactly;
            additionalInformationParagraph.Format.LineSpacing = LineSpacing;
            additionalInformationParagraph.Format.LeftIndent = LeftIndent;
            additionalInformationParagraph.Format.SpaceBefore = Spacing * 2;
            additionalInformationParagraph.Format.Font.Size = MajorComponentFontSize;
            var additionalInformation = boom.AdditionalInformation ?? string.Empty;
            additionalInformationParagraph.AddText(additionalInformation);
        }

        private void PopulateDetailPage(Section page, QuoteBoomSystem quoteBoomSystem, QuoteRequest quoteRequest, Boom boom, bool fromGeneralReport)
        {
            CreateHeader(page, _logoPath);
            if (fromGeneralReport)
                CreateQuoteNumberLine(page, quoteRequest);
            else
                CreateCustomerInfoDetailedQuote(page, quoteRequest, boom);

            CreateSystemNameParagraph(page, quoteBoomSystem);
            CreateGreenhouseInfo(page, boom);
            CreateRegularComponentsTable(page, quoteBoomSystem);
            CreateOptionalComponentsTable(page, quoteBoomSystem);
            CreateTotalCostParagraph(page, quoteBoomSystem);
        }

        private static void CreateQuoteNumberLine(Section page, QuoteRequest quoteRequest)
        {
            var quoteNumberTable = page.AddTable();
            quoteNumberTable.Format.SpaceBefore = Spacing;
            quoteNumberTable.Format.Font.Size = MajorComponentFontSize;
            quoteNumberTable.Format.Font.Bold = true;

            var column = quoteNumberTable.AddColumn();

            column.Format.Alignment = ParagraphAlignment.Right;
            column.Width = new Unit(18, UnitType.Centimeter);

            var row = quoteNumberTable.AddRow();

            var quoteNumberparagraph = row[0].AddParagraph();
            quoteNumberparagraph.AddTextAndLineBreak("Quote #:".PadRight(10, SpaceCharacter) + quoteRequest.QuoteNumberWithRevision);
        }

        private static void CreateTotalCostParagraph(Section page, QuoteBoomSystem quoteBoomSystem)
        {
            var systemTotalCostTable = page.AddTable();

            var totalCostTitleColumn = systemTotalCostTable.AddColumn();
            var totalCostColumn = systemTotalCostTable.AddColumn();

            totalCostTitleColumn.Format.Alignment = ParagraphAlignment.Left;
            totalCostTitleColumn.Width = new Unit(14.8, UnitType.Centimeter);

            totalCostColumn.Format.Alignment = ParagraphAlignment.Right;
            totalCostColumn.Width = new Unit(3.5, UnitType.Centimeter);

            var totalCostComponentRow = systemTotalCostTable.AddRow();
            totalCostComponentRow.Format.SpaceBefore = Spacing;

            var totalCostTitleParagraph = totalCostComponentRow.Cells[0].AddParagraph();
            totalCostTitleParagraph.Format.LeftIndent = new Unit(3, UnitType.Centimeter);
            totalCostTitleParagraph.Format.Font.Bold = true;
            totalCostTitleParagraph.Format.Font.Size = MajorComponentFontSize;
            totalCostTitleParagraph.AddText("Total Price Per System");

            var totalCostParagraph = totalCostComponentRow.Cells[1].AddParagraph();
            totalCostParagraph.Format.Font.Bold = true;
            totalCostParagraph.Format.Font.Size = MajorComponentFontSize;
            totalCostParagraph.AddText(quoteBoomSystem.GetTotalCost().ToString("C2"));
        }

        private static void CreateOptionalComponentsTable(Section page, QuoteBoomSystem quoteBoomSystem, bool includeWithoutValue = true, bool forBuildDocument = false, bool includePricing = true)
        {
            var spaces = new string(SpaceCharacter, 5);

            var optionsCategory = quoteBoomSystem.QuoteCategories.FirstOrDefault(c => c.Name.ToLower() == "options and upgrades");
            if (optionsCategory?.QuoteMajorComponents == null) return;
            var optionalComponentsTable = page.AddTable();
            var detailColumn = optionalComponentsTable.AddColumn();
            var nameOptionalColumn = optionalComponentsTable.AddColumn();
            var optionPriceOptionalColumn = optionalComponentsTable.AddColumn();
            var addedCostOptionalColumn = optionalComponentsTable.AddColumn();

            detailColumn.Format.Alignment = ParagraphAlignment.Right;
            detailColumn.Width = new Unit(0.8, UnitType.Centimeter);

            nameOptionalColumn.Format.Alignment = ParagraphAlignment.Left;
            nameOptionalColumn.Width = new Unit(12.5, UnitType.Centimeter);

            optionPriceOptionalColumn.Format.Alignment = ParagraphAlignment.Right;
            optionPriceOptionalColumn.Width = new Unit(2.5, UnitType.Centimeter);

            addedCostOptionalColumn.Format.Alignment = ParagraphAlignment.Right;
            addedCostOptionalColumn.Width = new Unit(2.5, UnitType.Centimeter);

            var optionsCategoryHeaderRow = optionalComponentsTable.AddRow();
            optionsCategoryHeaderRow.Format.SpaceBefore = new Unit(0.1, UnitType.Centimeter);

            optionsCategoryHeaderRow.Cells[0].AddParagraph();

            var optionsCategorySecondColumn = optionsCategoryHeaderRow.Cells[1].AddParagraph();
            optionsCategorySecondColumn.Format.Font.Bold = true;
            optionsCategorySecondColumn.Format.Font.Size = MajorComponentFontSize;
            optionsCategorySecondColumn.AddText("Options and Upgrades");

            if (includePricing)
            {
                var optionsCategoryThirdColumn = optionsCategoryHeaderRow.Cells[2].AddParagraph();
                optionsCategoryThirdColumn.Format.Font.Bold = true;
                optionsCategoryThirdColumn.Format.Font.Size = MajorComponentFontSize;
                optionsCategoryThirdColumn.AddText("Option Price:");

                var optionsCategoryFourthColumn = optionsCategoryHeaderRow.Cells[3].AddParagraph();
                optionsCategoryFourthColumn.Format.Font.Bold = true;
                optionsCategoryFourthColumn.Format.Font.Size = MajorComponentFontSize;
                optionsCategoryFourthColumn.AddText("Added Cost:");
            }

            if (optionsCategory.QuoteMajorComponents != null)
            {
                foreach (var majorComponent in optionsCategory.QuoteMajorComponents.Where(m => m.IsVisible && m.IsActive && (m.AssignedPrice != 0.0 || includeWithoutValue) && (!forBuildDocument || m.IncludeInBuildDocument)))
                {
                    var majorComponentRow = optionalComponentsTable.AddRow();

                    var majorComponentDetailResultParagraph = majorComponentRow.Cells[0].AddParagraph();
                    var majorComponentNameParagraph = majorComponentRow.Cells[1].AddParagraph();
                    var majorComponentOptionPriceParagraph = majorComponentRow.Cells[2].AddParagraph();
                    var majorComponentAddedCostParagraph = majorComponentRow.Cells[3].AddParagraph();

                    majorComponentDetailResultParagraph.Format.Font.Size = MajorComponentFontSize;
                    majorComponentNameParagraph.Format.Font.Size = MajorComponentFontSize;
                    majorComponentOptionPriceParagraph.Format.Font.Size = MajorComponentFontSize;
                    majorComponentAddedCostParagraph.Format.Font.Size = MajorComponentFontSize;

                    var majorComponentDetailResultValue = majorComponent.DetailCalculationResult ?? string.Empty;
                    var majorComponentNameValue = majorComponent.DescriptionCalculationResult ?? string.Empty;
                    majorComponentNameValue = string.Concat(majorComponentNameValue, spaces);
                    majorComponentNameValue = includePricing
                                            ? majorComponentNameValue.PadRight(73, '.') + spaces
                                            : majorComponentNameValue.Trim(); //Added Trim because of weird "WidthAndHeightCannotBeNegative" bug

                    majorComponentDetailResultParagraph.AddText(majorComponentDetailResultValue);
                    majorComponentNameParagraph.AddText(majorComponentNameValue);

                    if (includePricing)
                    {
                        majorComponentOptionPriceParagraph.AddText(majorComponent.CalculatedPrice.ToString("C2", _numberFormat));
                        majorComponentAddedCostParagraph.AddText(majorComponent.AssignedPrice.ToString("C2", _numberFormat));
                    }

                    if (majorComponent.QuoteMajorComponentDetails != null)
                    {
                        foreach (var majorComponentDetail in majorComponent.QuoteMajorComponentDetails.Where(m => m.IsVisible && (!forBuildDocument || m.IncludeInBuildDocument)))
                        {
                            var majorComponentDetailRow = optionalComponentsTable.AddRow();

                            var majorComponentDetailParagraph = majorComponentDetailRow.Cells[1].AddParagraph();
                            majorComponentDetailParagraph.Format.LeftIndent = new Unit(0.3, UnitType.Centimeter);
                            majorComponentDetailParagraph.Format.Font.Italic = true;
                            majorComponentDetailParagraph.Format.Font.Size = ComponentDetailFontSize;

                            var majorComponentDetailNameValue = majorComponentDetail.Name ?? string.Empty;
                            majorComponentDetailParagraph.AddText(majorComponentDetailNameValue);
                        }
                    }

                    if (majorComponent.QuoteMinorComponents != null)
                    {
                        foreach (var minorComponent in majorComponent.QuoteMinorComponents.Where(m => m.IsVisible && m.IsActive && (!forBuildDocument || m.IncludeInBuildDocument)))
                        {
                            var minorComponentRow = optionalComponentsTable.AddRow();

                            var minorComponentNameParagraph = minorComponentRow.Cells[1].AddParagraph();
                            minorComponentNameParagraph.Format.LeftIndent = new Unit(0.6, UnitType.Centimeter);
                            minorComponentNameParagraph.Format.Font.Size = MinorComponentFontSize;

                            var minorComponentNameValue = minorComponent.DescriptionCalculationResult ?? string.Empty;
                            minorComponentNameParagraph.AddText(minorComponentNameValue);

                            if (minorComponent.QuoteMinorComponentDetails != null)
                            {
                                foreach (var minorComponentDetail in minorComponent.QuoteMinorComponentDetails.Where(m => m.IsVisible && (!forBuildDocument || m.IncludeInBuildDocument)))
                                {
                                    var minorComponentDetailRow = optionalComponentsTable.AddRow();

                                    var minorComponentDetailParagraph = minorComponentDetailRow.Cells[1].AddParagraph();
                                    minorComponentDetailParagraph.Format.LeftIndent = new Unit(0.85, UnitType.Centimeter);
                                    minorComponentDetailParagraph.Format.Font.Italic = true;
                                    minorComponentDetailParagraph.Format.Font.Size = ComponentDetailFontSize;

                                    var minorComponentDetailNameValue = minorComponentDetail.Name ?? string.Empty;
                                    minorComponentDetailParagraph.AddText(minorComponentDetailNameValue);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void CreateRegularComponentsTable(Section page, QuoteBoomSystem quoteBoomSystem, bool forBuildDocument = false, bool includePricing = true)
        {
            var regularCategories = quoteBoomSystem.QuoteCategories.Where(c => c.Name.ToLower() != "options and upgrades").ToList();
            if (!regularCategories.Any()) return;

            var componentsTable = page.AddTable();
            var detailColumn = componentsTable.AddColumn();
            var nameColumn = componentsTable.AddColumn();
            var addedCostOptionalColumn = componentsTable.AddColumn();

            detailColumn.Format.Alignment = ParagraphAlignment.Right;
            detailColumn.Width = new Unit(0.8, UnitType.Centimeter);

            nameColumn.Format.Alignment = ParagraphAlignment.Left;
            nameColumn.Width = new Unit(15, UnitType.Centimeter);

            addedCostOptionalColumn.Format.Alignment = ParagraphAlignment.Right;
            addedCostOptionalColumn.Width = new Unit(2.5, UnitType.Centimeter);

            var headerRow = componentsTable.AddRow();
            headerRow.Format.SpaceBefore = new Unit(0.05, UnitType.Centimeter);

            headerRow.Cells[0].AddParagraph();
            headerRow.Cells[1].AddParagraph();

            var optionsCategoryFourthColumn = headerRow.Cells[2].AddParagraph();
            optionsCategoryFourthColumn.Format.Font.Bold = true;
            optionsCategoryFourthColumn.Format.Font.Size = MajorComponentFontSize;

            var quoteMajorComponents = regularCategories
                .Where(c => c.QuoteMajorComponents != null)
                .SelectMany(c => c.QuoteMajorComponents)
                .Where(c => c.AssignedPrice != 0 && c.IsActive && c.IsVisible);

            if (quoteMajorComponents.Any() && includePricing)
            {
                optionsCategoryFourthColumn.AddText("Cost:");
            }

            foreach (var category in regularCategories)
            {
                var categoryRow = componentsTable.AddRow();
                categoryRow.Format.SpaceBefore = new Unit(0.05, UnitType.Centimeter);

                var categoryParagraph = categoryRow.Cells[1].AddParagraph();
                categoryParagraph.Format.Font.Bold = true;
                categoryParagraph.Format.Font.Size = MajorComponentFontSize;

                var categoryNameValue = category.Name ?? string.Empty;
                categoryParagraph.AddText(categoryNameValue);

                if (category.QuoteMajorComponents != null)
                {
                    foreach (var majorComponent in category.QuoteMajorComponents.Where(m => m.IsVisible && m.IsActive && (!forBuildDocument || m.IncludeInBuildDocument)))
                    {
                        var majorComponentRow = componentsTable.AddRow();

                        var majorComponentDetailResultParagraph = majorComponentRow.Cells[0].AddParagraph();
                        var majorComponentNameParagraph = majorComponentRow.Cells[1].AddParagraph();
                        var majorComponentAddedCostParagraph = majorComponentRow.Cells[2].AddParagraph();

                        majorComponentDetailResultParagraph.Format.Font.Size = MajorComponentFontSize;
                        majorComponentNameParagraph.Format.Font.Size = MajorComponentFontSize;
                        majorComponentAddedCostParagraph.Format.Font.Size = MajorComponentFontSize;

                        var majorComponentDetailResultValue = majorComponent.DetailCalculationResult ?? string.Empty;
                        var majorComponentNameValue = majorComponent.DescriptionCalculationResult ?? string.Empty;
                        majorComponentDetailResultParagraph.AddText(majorComponentDetailResultValue);
                        majorComponentNameParagraph.AddText(majorComponentNameValue);

                        //Only show cost if its different than 0
                        if (majorComponent.AssignedPrice != 0 && includePricing)
                        {
                            majorComponentAddedCostParagraph.AddText(majorComponent.AssignedPrice.ToString("C2", _numberFormat));
                        }

                        if (majorComponent.QuoteMajorComponentDetails != null)
                        {
                            foreach (var majorComponentDetail in majorComponent.QuoteMajorComponentDetails.Where(m => m.IsVisible && (!forBuildDocument || m.IncludeInBuildDocument)))
                            {
                                var majorComponentDetailRow = componentsTable.AddRow();

                                var majorComponentDetailParagraph = majorComponentDetailRow.Cells[1].AddParagraph();
                                majorComponentDetailParagraph.Format.LeftIndent = new Unit(0.3, UnitType.Centimeter);
                                majorComponentDetailParagraph.Format.Font.Italic = true;
                                majorComponentDetailParagraph.Format.Font.Size = ComponentDetailFontSize;

                                var majorComponentDetailNameValue = majorComponentDetail.Name ?? string.Empty;
                                majorComponentDetailParagraph.AddText(majorComponentDetailNameValue);
                            }
                        }

                        if (majorComponent.QuoteMinorComponents != null)
                        {
                            foreach (var minorComponent in majorComponent.QuoteMinorComponents.Where(m => m.IsVisible && m.IsActive && (!forBuildDocument || m.IncludeInBuildDocument)))
                            {
                                var minorComponentRow = componentsTable.AddRow();

                                var minorComponentDetailResultParagraph = minorComponentRow.Cells[0].AddParagraph();
                                minorComponentDetailResultParagraph.Format.Font.Size = MinorComponentFontSize;

                                var minorComponentNameParagraph = minorComponentRow.Cells[1].AddParagraph();
                                minorComponentNameParagraph.Format.LeftIndent = new Unit(0.3, UnitType.Centimeter);
                                minorComponentNameParagraph.Format.Font.Size = MinorComponentFontSize;

                                var minorComponentDetailResultValue = minorComponent.DetailCalculationResult ?? string.Empty;
                                var minorComponentNameValue = minorComponent.DescriptionCalculationResult ?? string.Empty;
                                minorComponentDetailResultParagraph.AddText(minorComponentDetailResultValue);
                                minorComponentNameParagraph.AddText(minorComponentNameValue);

                                if (minorComponent.QuoteMinorComponentDetails != null)
                                {
                                    foreach (var minorComponentDetail in minorComponent.QuoteMinorComponentDetails.Where(m => m.IsVisible && (!forBuildDocument || m.IncludeInBuildDocument)))
                                    {
                                        var minorComponentDetailRow = componentsTable.AddRow();

                                        var minorComponentDetailParagraph = minorComponentDetailRow.Cells[1].AddParagraph();
                                        minorComponentDetailParagraph.Format.LeftIndent = new Unit(0.8, UnitType.Centimeter);
                                        minorComponentDetailParagraph.Format.Font.Italic = true;
                                        minorComponentDetailParagraph.Format.Font.Size = ComponentDetailFontSize;

                                        var minorComponentDetailNameValue = minorComponentDetail.Name ?? string.Empty;
                                        minorComponentDetailParagraph.AddText(minorComponentDetailNameValue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void CreateSystemNameParagraph(Section page, QuoteBoomSystem quoteBoomSystem, bool forBuildDocument = false)
        {
            var systemNameParagraph = page.AddParagraph();
            systemNameParagraph.Format.LeftIndent = LeftIndent;
            systemNameParagraph.Format.Alignment = ParagraphAlignment.Center;
            systemNameParagraph.Format.SpaceBefore = Spacing;
            systemNameParagraph.Format.Font.Size = MajorComponentFontSize;
            systemNameParagraph.Format.Font.Bold = true;
            var systemNameAndCost = string.Concat(quoteBoomSystem.DetailCalculationResult, quoteBoomSystem.DescriptionCalculationResult, " Quotation");
            systemNameAndCost = forBuildDocument
                                ? systemNameAndCost
                                : string.Concat(systemNameAndCost.PadRight(systemNameAndCost.Length + 5, '.'), quoteBoomSystem.AssignedPrice.ToString("C2", _numberFormat));

            systemNameParagraph.AddText(systemNameAndCost);
        }

        private static void CreateCustomerInfoDetailedQuote(Section page, QuoteRequest quoteRequest, Boom boom, bool includeShippingInfo = false, bool includeSystemInformation = true)
        {
            //Customer table
            var customerTable = page.AddTable();
            customerTable.Format.Font.Size = MajorComponentFontSize;
            var customerFirstColumn = customerTable.AddColumn();
            var customerSecondColumn = customerTable.AddColumn();

            customerFirstColumn.Format.Alignment = ParagraphAlignment.Left;
            customerFirstColumn.Width = new Unit(13, UnitType.Centimeter);

            customerSecondColumn.Format.Alignment = ParagraphAlignment.Left;
            customerSecondColumn.Width = new Unit(9, UnitType.Centimeter);

            var customerFirstRow = customerTable.AddRow();
            customerFirstRow.Format.SpaceBefore = Spacing;

            var customerLeftParagraph = customerFirstRow.Cells[0].AddParagraph();
            customerLeftParagraph.AddTextAndLineBreak("Quote #:".PadRight(19, SpaceCharacter) + quoteRequest.QuoteNumberWithRevision);
            customerLeftParagraph.AddTextAndLineBreak("Purchase Order:".PadRight(19, SpaceCharacter) + quoteRequest.PurchaseOrder);
            customerLeftParagraph.AddTextAndLineBreak("Customer:".PadRight(19, SpaceCharacter) + quoteRequest.Customer.CompanyName);
            customerLeftParagraph.AddTextAndLineBreak("Distributor:".PadRight(19, SpaceCharacter) + quoteRequest.Distributor.Name);
            customerLeftParagraph.AddTextAndLineBreak("Attn:".PadRight(19, SpaceCharacter) + quoteRequest.ContactName);
            customerLeftParagraph.AddTextAndLineBreak("Phone:".PadRight(19, SpaceCharacter) + quoteRequest.Phone);
            customerLeftParagraph.AddTextAndLineBreak("E-mail:".PadRight(19, SpaceCharacter) + quoteRequest.Email);

            var customerRightParagraph = customerFirstRow.Cells[1].AddParagraph();
            customerRightParagraph.AddTextAndLineBreak("Date:".PadRight(22, SpaceCharacter) + DateTime.Now.ToShortDateString());

            if (includeSystemInformation)
            {
                customerRightParagraph.AddTextAndLineBreak("System Width (Feet):".PadRight(22, SpaceCharacter) +
                                                           boom.BayWidthInFeet.ToString("N2"));
                customerRightParagraph.AddTextAndLineBreak("System Length (Feet):".PadRight(22, SpaceCharacter) +
                                                           boom.BayLengthInFeet.ToString("N2"));
                customerRightParagraph.AddTextAndLineBreak("Number of Bays:".PadRight(22, SpaceCharacter) +
                                                           boom.NumberOfBays);
            }

            if (includeShippingInfo)
            {
                var shipInfo = quoteRequest.FullAddress;
                if (!string.IsNullOrEmpty(shipInfo))
                {
                    var customerShipInfoParagraph = customerFirstRow.Cells[0].AddParagraph();
                    customerShipInfoParagraph.AddTextAndLineBreak("Ship Address:".PadRight(15, SpaceCharacter) +
                                                                  shipInfo);
                }
                //if (shipInfo != null)
                //{
                //    var customerShipInfoParagraph = customerFirstRow.Cells[0].AddParagraph();
                //    customerShipInfoParagraph.AddTextAndLineBreak("Ship Address:".PadRight(15, SpaceCharacter) +
                //                                                  shipInfo.FullAddress);
                //}
            }
        }

        private static void CreateHeader(Section section, string path)
        {
            //Header table
            var headerTable = section.AddTable();
            headerTable.Format.Font.Size = MajorComponentFontSize;
            var headerFirstColumn = headerTable.AddColumn();
            var headerSecondColumn = headerTable.AddColumn();

            headerFirstColumn.Format.Alignment = ParagraphAlignment.Left;
            headerFirstColumn.Width = new Unit(11.5, UnitType.Centimeter);

            headerSecondColumn.Format.Alignment = ParagraphAlignment.Right;
            headerSecondColumn.Width = new Unit(8, UnitType.Centimeter);

            //Logo
            var headerRow = headerTable.AddRow();
            headerRow.Cells[0].AddImage(path);

            //Company Info
            var companyParagraph = headerRow.Cells[1].AddParagraph();
            companyParagraph.AddTextAndLineBreak("Cherry Creek Systems, Inc.");
            companyParagraph.AddTextAndLineBreak("3025 N Hancock Ave");
            companyParagraph.AddTextAndLineBreak("Colorado Springs, CO 80907");
            companyParagraph.AddTextAndLineBreak("www.cherrycreeksystems.com");
            companyParagraph.AddTextAndLineBreak("Phone: (877) 558-3246");
            companyParagraph.AddTextAndLineBreak("Fax: (719) 380-5343");
        }

        #endregion detailed quote report

        #region general quote report

        public string GenerateQuoteReport(QuoteRequest quoteRequest, List<SteelEstimate> steelEstimates, List<SteelData> steelData)
        {
            var report = GetReport(quoteRequest, steelEstimates, steelData);
            // Save the document...
            var fileName = CreateQuoteReportFileName(quoteRequest.QuoteNumber);
            report.PdfDocument.Save(fileName);

            return fileName;
        }

        public Stream GenerateQuoteReportStream(QuoteRequest quoteRequest, List<SteelEstimate> steelEstimates, List<SteelData> steelData)
        {
            var stream = new MemoryStream();
            var report = GetReport(quoteRequest, steelEstimates, steelData);
            report.PdfDocument.Save(stream, closeStream: false);
            return stream;
        }

        public string CreateQuoteReportFileName(string quoteNumber)
        {
            var timeStamp = DateTime.Now.Ticks % 1000;
            var cleanQuoteNumber = illegalInFileName.Replace(quoteNumber, "");
            var filename = $"{cleanQuoteNumber}_{timeStamp}.pdf";
            return filename;
        }

        private PdfDocumentRenderer GetReport(QuoteRequest quoteRequest, List<SteelEstimate> steelEstimates, List<SteelData> steelData)
        {
            var document = new Document();
            document.DefaultPageSetup.TopMargin = TopMargin;
            document.DefaultPageSetup.LeftMargin = LeftMargin;
            document.DefaultPageSetup.BottomMargin = BottomMargin;

            var style = document.Styles["Normal"];
            style.Font.Name = Font;

            var page = document.AddSection();
            AddPageNumber(page);

            CreateHeader(page, _logoPath);
            CreateCustomerInfo(page, quoteRequest);
            CreateQuotationSummary(page, quoteRequest);
            CreateFooter(page);

            foreach (var system in quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems)
            {
                var systemDetailPage = document.AddSection();
                AddPageNumber(systemDetailPage);
                PopulateDetailPage(systemDetailPage, system, quoteRequest, system.Boom, fromGeneralReport: true);

                if (!string.IsNullOrWhiteSpace(system.Boom.AdditionalInformation))
                {
                    var additionalInformationPage = document.AddSection();
                    AddPageNumber(additionalInformationPage);
                    CreateAdditionalInformationPage(additionalInformationPage, system.Boom);
                }
            }

            var steelEstimatesPage = document.AddSection();
            AddPageNumber(steelEstimatesPage);
            CreateSteelQuantitiesPage(steelEstimatesPage, quoteRequest, steelEstimates, steelData);

            var termsAndConditionsPage = document.AddSection();
            AddPageNumber(termsAndConditionsPage);
            CreateTermsAndConditionsPage(termsAndConditionsPage);

            // Create a renderer for the MigraDoc document.
            var pdfRenderer = new PdfDocumentRenderer(false);
            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;
            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            return pdfRenderer;
        }

        private void CreateTermsAndConditionsPage(Section page)
        {
            CreateHeader(page, _logoPath);

            var priceHeaderParagraph = page.AddParagraph();
            priceHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            priceHeaderParagraph.Format.SpaceBefore = TermsSpacing + 0.2;
            priceHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            priceHeaderParagraph.Format.Font.Bold = true;
            priceHeaderParagraph.Format.Font.Underline = Underline.Single;
            priceHeaderParagraph.AddText("PRICES:");

            var priceParagraph = page.AddParagraph();
            priceParagraph.Format.Alignment = ParagraphAlignment.Left;
            priceParagraph.Format.SpaceBefore = TermsSpacing;
            priceParagraph.Format.Font.Size = TermsFontSize;
            priceParagraph.Format.Font.Bold = true;
            priceParagraph.AddText("Unless otherwise specified in our proposal, prices are firm for thirty (30) days only when confirmed in writing by “Cherry Creek Systems, Inc.” . Service time, including travel and living expenses of a “Factory Service Technician” is not  included unless indicated.");

            var paymentHeaderParagraph = page.AddParagraph();
            paymentHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            paymentHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            paymentHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            paymentHeaderParagraph.Format.Font.Bold = true;
            paymentHeaderParagraph.Format.Font.Underline = Underline.Single;
            paymentHeaderParagraph.AddText("CREDIT AND PAYMENT:");

            var paymentParagraph = page.AddParagraph();
            paymentParagraph.Format.Alignment = ParagraphAlignment.Left;
            paymentParagraph.Format.SpaceBefore = TermsSpacing;
            paymentParagraph.Format.Font.Size = TermsFontSize;
            paymentParagraph.Format.Font.Bold = true;
            paymentParagraph.AddText("Unless otherwise specified in our proposal, payment terms are 50% with the “Purchase Order”, balance due plus shipping and net thirty  (30) days from shipment. Overdue payments are subject to a surcharge of 1-1/2% per month. This is equivalent to 18% per year on the outstanding balance over thirty (30) days. Any collection fees including attorney costs which may be incurred will be added to this amount. Upon “Cherry Creek Systems, Inc.”  failure to receive payment as provided herein, in addition to any other remedies which “Cherry Creek Systems, Inc.” may have, “Cherry Creek Systems, Inc.”  shall have the right and the “Buyer” will permit “Cherry Creek Systems, Inc.”  to enter the premises where the equipment is installed and repossess equipment or products as to which full payment has not been received by “Cherry Creek Systems, Inc.”.");

            var taxesHeaderParagraph = page.AddParagraph();
            taxesHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            taxesHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            taxesHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            taxesHeaderParagraph.Format.Font.Bold = true;
            taxesHeaderParagraph.Format.Font.Underline = Underline.Single;
            taxesHeaderParagraph.AddText("TAXES:");

            var taxesParagraph = page.AddParagraph();
            taxesParagraph.Format.Alignment = ParagraphAlignment.Left;
            taxesParagraph.Format.SpaceBefore = TermsSpacing;
            taxesParagraph.Format.Font.Size = TermsFontSize;
            taxesParagraph.Format.Font.Bold = true;
            taxesParagraph.AddText("Sales tax, personal property tax, use tax, excise tax or any other taxes imposed by federal, state or municipal authority and incurred by “Cherry Creek Systems, Inc.”  shall be for the “Buyer” account and are in addition to the price quoted in the proposal.Unless otherwise specified in our proposal, payment terms are 50% with the “Purchase Order”, balance due plus shipping and net thirty  (30) days from shipment. Overdue payments are subject to a surcharge of 1-1/2% per month. This is equivalent to 18% per year on the outstanding balance over thirty (30) days. Any collection fees including attorney costs which may be incurred will be added to this amount. Upon “Cherry Creek Systems, Inc.”  failure to receive payment as provided herein, in addition to any other remedies which “Cherry Creek Systems, Inc.” may have, “Cherry Creek Systems, Inc.”  shall have the right and the “Buyer” will permit “Cherry Creek Systems, Inc.”  to enter the premises where the equipment is installed and repossess equipment or products as to which full payment has not been received by “Cherry Creek Systems, Inc.”.");

            var titleHeaderParagraph = page.AddParagraph();
            titleHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            titleHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            titleHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            titleHeaderParagraph.Format.Font.Bold = true;
            titleHeaderParagraph.Format.Font.Underline = Underline.Single;
            titleHeaderParagraph.AddText("TITLE:");

            var titleParagraph = page.AddParagraph();
            titleParagraph.Format.Alignment = ParagraphAlignment.Left;
            titleParagraph.Format.SpaceBefore = TermsSpacing;
            titleParagraph.Format.Font.Size = TermsFontSize;
            titleParagraph.Format.Font.Bold = true;
            titleParagraph.AddText("Title to all equipment shall pass to buyer at F.O.B. point of shipment and risk of loss will therefore be borne by “Buyer”.");

            var maejureHeaderParagraph = page.AddParagraph();
            maejureHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            maejureHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            maejureHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            maejureHeaderParagraph.Format.Font.Bold = true;
            maejureHeaderParagraph.Format.Font.Underline = Underline.Single;
            maejureHeaderParagraph.AddText("FORCE MAJEURE:");

            var maejureParagraph = page.AddParagraph();
            maejureParagraph.Format.Alignment = ParagraphAlignment.Left;
            maejureParagraph.Format.SpaceBefore = TermsSpacing;
            maejureParagraph.Format.Font.Size = TermsFontSize;
            maejureParagraph.Format.Font.Bold = true;
            maejureParagraph.AddText("“Cherry Creek Systems, Inc.” shall not be liable for any loss damage arising out of  delay in shipment or delivery, or failure to manufacture or failure of the equipment to operate, due to causes beyond reasonable control, such as but not limited to acts of God, acts of “Buyer”, acts of civil or military  authority, priorities, fires, strikes, floods, epidemics, quarantines restrictions, war, riot., delays of shipper, and “Cherry Creek Systems, Inc.’s” ability to obtain necessary labor, materials, or manufacturing facilities. In the event of such delay, the date of  delivery shall be extended for a period of time equal to the time lost by the delay.");

            var storageHeaderParagraph = page.AddParagraph();
            storageHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            storageHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            storageHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            storageHeaderParagraph.Format.Font.Bold = true;
            storageHeaderParagraph.Format.Font.Underline = Underline.Single;
            storageHeaderParagraph.AddText("STORAGE:");

            var storageParagraph = page.AddParagraph();
            storageParagraph.Format.Alignment = ParagraphAlignment.Left;
            storageParagraph.Format.SpaceBefore = TermsSpacing;
            storageParagraph.Format.Font.Size = TermsFontSize;
            storageParagraph.Format.Font.Bold = true;
            storageParagraph.AddText("If the “Buyer” declines or is unable to take delivery at the time (s) specified in the proposal or contract “Cherry Creek Systems, Inc.”  will have equipment stored at the “Buyers” risk, expense and account. The materials shall be considered shipped.");

            var fobHeaderParagraph = page.AddParagraph();
            fobHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            fobHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            fobHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            fobHeaderParagraph.Format.Font.Bold = true;
            fobHeaderParagraph.Format.Font.Underline = Underline.Single;
            fobHeaderParagraph.AddText("FOB POINT:");

            var fobParagraph = page.AddParagraph();
            fobParagraph.Format.Alignment = ParagraphAlignment.Left;
            fobParagraph.Format.SpaceBefore = TermsSpacing;
            fobParagraph.Format.Font.Size = TermsFontSize;
            fobParagraph.Format.Font.Bold = true;
            fobParagraph.AddText("Unless otherwise negotiated FOB point is “Cherry Creek Systems, Inc.”  3025 N. Hancock Ave, Colorado Springs, Colorado, 80907, USA.");

            var deliveryHeaderParagraph = page.AddParagraph();
            deliveryHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            deliveryHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            deliveryHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            deliveryHeaderParagraph.Format.Font.Bold = true;
            deliveryHeaderParagraph.Format.Font.Underline = Underline.Single;
            deliveryHeaderParagraph.AddText("DELIVERIES:");

            var deliveryParagraph = page.AddParagraph();
            deliveryParagraph.Format.Alignment = ParagraphAlignment.Left;
            deliveryParagraph.Format.SpaceBefore = TermsSpacing;
            deliveryParagraph.Format.Font.Size = TermsFontSize;
            deliveryParagraph.Format.Font.Bold = true;
            deliveryParagraph.AddText("Deliveries quoted are for shipment from “Cherry Creek Systems, Inc.”  plant. Delivery times are computed from date of our acceptance of purchase order.");

            var cancelationHeaderParagraph = page.AddParagraph();
            cancelationHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            cancelationHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            cancelationHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            cancelationHeaderParagraph.Format.Font.Bold = true;
            cancelationHeaderParagraph.Format.Font.Underline = Underline.Single;
            cancelationHeaderParagraph.AddText("CANCELLATION and RETURNS of EQUIPMENT: ");

            var cancelationParagraph = page.AddParagraph();
            cancelationParagraph.Format.Alignment = ParagraphAlignment.Left;
            cancelationParagraph.Format.SpaceBefore = TermsSpacing;
            cancelationParagraph.Format.Font.Size = TermsFontSize;
            cancelationParagraph.Format.Font.Bold = true;
            cancelationParagraph.AddText("Orders which are cancelled must be verified in writing by both parties and depending upon the circumstances, a reasonable and proper cancellation charge, including factory costs and expenses incurred by “Cherry Creek Systems, Inc.” in carrying forward the order to date of agreement to terminate, will be issued. Goods may be returned only when specifically authorized. The “Buyer” will be charged for the reconditioning of goods returned to a saleable condition, any sales expenses then incurred by “Cherry Creek Systems, Inc.”  plus a restocking charge and any outgoing and incoming transportation costs which “Cherry Creek Systems, Inc.”  pays.");

            var contractHeaderParagraph = page.AddParagraph();
            contractHeaderParagraph.Format.Alignment = ParagraphAlignment.Left;
            contractHeaderParagraph.Format.SpaceBefore = TermsSpacing;
            contractHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            contractHeaderParagraph.Format.Font.Bold = true;
            contractHeaderParagraph.Format.Font.Underline = Underline.Single;
            contractHeaderParagraph.AddText("ENTIRE CONTRACT:");

            var contractParagraph = page.AddParagraph();
            contractParagraph.Format.Alignment = ParagraphAlignment.Left;
            contractParagraph.Format.SpaceBefore = TermsSpacing;
            contractParagraph.Format.Font.Size = TermsFontSize;
            contractParagraph.Format.Font.Bold = true;
            contractParagraph.AddText("No acceptance by “Cherry Creek Systems, Inc.” of any order shall be deemed to be an acceptance of any provision contained in the buyer purchase order form. This writing constitutes the entire agreement and understanding between “Buyer” and “Cherry Creek Systems, Inc.” as of the date of acceptance by “Cherry Creek Systems, Inc.”  and shall not be modified thereafter in anyway except in writing and executed by a person  duly authorized by “Cherry Creek Systems, Inc.”  to execute same.");
        }

        private void CreateSteelQuantitiesPage(Section page, QuoteRequest quoteRequest, List<SteelEstimate> steelEstimates, List<SteelData> steelData)
        {
            CreateHeader(page, _logoPath);

            var titleParagraph = page.AddParagraph();
            titleParagraph.Format.LeftIndent = LeftIndent;
            titleParagraph.Format.Alignment = ParagraphAlignment.Center;
            titleParagraph.Format.SpaceBefore = Spacing;
            titleParagraph.Format.Font.Size = MajorComponentFontSize;
            titleParagraph.Format.Font.Bold = true;
            titleParagraph.Format.Font.Underline = Underline.Single;
            titleParagraph.AddText("Steel Quantities");

            CreateCustomerInfo(page, quoteRequest);

            var steelDisclaimerParagraph = page.AddParagraph();
            steelDisclaimerParagraph.Format.LeftIndent = LeftIndent;
            steelDisclaimerParagraph.Format.Alignment = ParagraphAlignment.Center;
            steelDisclaimerParagraph.Format.SpaceBefore = Spacing;
            steelDisclaimerParagraph.Format.SpaceAfter = Spacing;
            steelDisclaimerParagraph.Format.Font.Color = Color.Parse("Gray");
            steelDisclaimerParagraph.Format.Font.Size = TermsFontSize;
            steelDisclaimerParagraph.AddText("Steel Tubing or Pipe is necessary for all of Cherry Creeks equipment, but NOT provided by CCS unless requested by the Customer. In some cases, it is cheaper to buy the steel locally.");

            var steelEstimatesTable = page.AddTable();
            steelEstimatesTable.Format.SpaceBefore = TermsSpacing;
            steelEstimatesTable.Format.SpaceAfter = TermsSpacing;

            var steelEstimatesFirstColumn = steelEstimatesTable.AddColumn();
            steelEstimatesFirstColumn.Borders.Top.Width = BorderSize;
            steelEstimatesFirstColumn.Borders.Bottom.Width = BorderSize;
            steelEstimatesFirstColumn.Borders.Left.Width = BorderSize;
            steelEstimatesFirstColumn.Format.Alignment = ParagraphAlignment.Left;
            steelEstimatesFirstColumn.Format.Font.Size = MajorComponentFontSize;
            steelEstimatesFirstColumn.Width = new Unit(7, UnitType.Centimeter);

            var steelEstimatesSecondColumn = steelEstimatesTable.AddColumn();
            steelEstimatesSecondColumn.Borders.Top.Width = BorderSize;
            steelEstimatesSecondColumn.Borders.Bottom.Width = BorderSize;
            steelEstimatesSecondColumn.Format.Alignment = ParagraphAlignment.Center;
            steelEstimatesSecondColumn.Format.Font.Size = MajorComponentFontSize;
            steelEstimatesSecondColumn.Width = new Unit(5.2, UnitType.Centimeter);

            var steelEstimatesThirdColumn = steelEstimatesTable.AddColumn();
            steelEstimatesThirdColumn.Borders.Top.Width = BorderSize;
            steelEstimatesThirdColumn.Borders.Bottom.Width = BorderSize;
            steelEstimatesThirdColumn.Borders.Right.Width = BorderSize;
            steelEstimatesThirdColumn.Format.Alignment = ParagraphAlignment.Left;
            steelEstimatesThirdColumn.Format.Font.Size = MajorComponentFontSize;
            steelEstimatesThirdColumn.Width = new Unit(7, UnitType.Centimeter);

            var headerRow = steelEstimatesTable.AddRow();

            headerRow.VerticalAlignment = VerticalAlignment.Center;
            headerRow.Format.Font.Bold = true;
            headerRow[0].AddParagraph("Steel Quantity Estimate");
            headerRow[2].AddParagraph("(Steel prices are NOT included in quotation pricing)");

            foreach (var system in quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems)
            {
                //If any manual steel materials have been entered, used those instead
                if (system.SteelMaterials.AnyActive())
                {
                    foreach (var estimate in system.SteelMaterials.GetActive())
                    {
                        var estimateRow = steelEstimatesTable.AddRow();
                        estimateRow[0].AddParagraph(estimate.Material);
                        estimateRow[1].AddParagraph(estimate.Quantity);
                        estimateRow[2].AddParagraph(system.SystemName);
                    }
                }
                else
                {
                    var estimates = steelEstimates.Where(e => e.BoomTypeId == system.Boom.BoomTypeId);

                    foreach (var estimate in estimates)
                    {
                        var estimateRow = steelEstimatesTable.AddRow();
                        estimateRow[0].AddParagraph(estimate.Material);
                        estimateRow[1].AddParagraph(estimate.QuantityCalculationResult);
                        estimateRow[2].AddParagraph(system.SystemName);
                    }
                }
            }

            var steelSecondDisclaimerParagraph = page.AddParagraph();
            steelSecondDisclaimerParagraph.Format.LeftIndent = LeftIndent;
            steelSecondDisclaimerParagraph.Format.Alignment = ParagraphAlignment.Center;
            steelSecondDisclaimerParagraph.Format.SpaceBefore = Spacing;
            steelSecondDisclaimerParagraph.Format.SpaceAfter = Spacing;
            steelSecondDisclaimerParagraph.Format.Font.Color = Color.Parse("Gray");
            steelSecondDisclaimerParagraph.Format.Font.Size = TermsFontSize;
            steelSecondDisclaimerParagraph.AddText("*** Please Contact CCS for current pricing and availability of all steel; as it can fluctuate throughout the year ***");

            var steelDataTable = page.AddTable();
            steelDataTable.Format.LeftIndent = LeftIndent;
            steelDataTable.Format.SpaceBefore = TermsSpacing;
            steelDataTable.Format.SpaceAfter = TermsSpacing;

            var steelDataFirstColumn = steelDataTable.AddColumn();
            steelDataFirstColumn.Borders.Width = BorderSize;
            steelDataFirstColumn.Format.Alignment = ParagraphAlignment.Left;
            steelDataFirstColumn.Format.Font.Size = MajorComponentFontSize;
            steelDataFirstColumn.Width = new Unit(4.9, UnitType.Centimeter);

            var steelDataSecondtColumn = steelDataTable.AddColumn();
            steelDataSecondtColumn.Borders.Width = BorderSize;
            steelDataSecondtColumn.Format.Alignment = ParagraphAlignment.Left;
            steelDataSecondtColumn.Format.Font.Size = MajorComponentFontSize;
            steelDataSecondtColumn.Width = new Unit(4.8, UnitType.Centimeter);

            var steelDataThirdColumn = steelDataTable.AddColumn();
            steelDataThirdColumn.Borders.Width = BorderSize;
            steelDataThirdColumn.Format.Alignment = ParagraphAlignment.Left;
            steelDataThirdColumn.Format.Font.Size = MajorComponentFontSize;
            steelDataThirdColumn.Width = new Unit(4.8, UnitType.Centimeter);

            var steelDataFourthColumn = steelDataTable.AddColumn();
            steelDataFourthColumn.Borders.Width = BorderSize;
            steelDataFourthColumn.Format.Alignment = ParagraphAlignment.Left;
            steelDataFourthColumn.Format.Font.Size = MajorComponentFontSize;
            steelDataFourthColumn.Width = new Unit(4.7, UnitType.Centimeter);

            var steelDataHeaderRow = steelDataTable.AddRow();
            steelDataHeaderRow[0].MergeRight = 3;

            var steelDataHeaderParagraph = steelDataHeaderRow[0].AddParagraph();
            steelDataHeaderParagraph.Format.Borders.Right.Width = 0;
            steelDataHeaderParagraph.Format.Alignment = ParagraphAlignment.Center;
            steelDataHeaderParagraph.Format.SpaceBefore = Spacing;
            steelDataHeaderParagraph.Format.SpaceAfter = Spacing;
            steelDataHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            steelDataHeaderParagraph.Format.Font.Bold = true;
            steelDataHeaderParagraph.AddText("Round Pipe Sizes / Dimensions / Weights");

            var steelDataColumnsHeaderRow = steelDataTable.AddRow();

            var steelDataColumnsHeaderParagraph1 = steelDataColumnsHeaderRow[0].AddParagraph();
            steelDataColumnsHeaderParagraph1.Format.Alignment = ParagraphAlignment.Center;
            steelDataColumnsHeaderParagraph1.Format.Font.Size = MajorComponentFontSize;
            steelDataColumnsHeaderParagraph1.Format.Font.Bold = true;
            steelDataColumnsHeaderParagraph1.AddText("Size - Inches (mm.)");

            var steelDataColumnsHeaderParagraph2 = steelDataColumnsHeaderRow[1].AddParagraph();
            steelDataColumnsHeaderParagraph2.Format.Alignment = ParagraphAlignment.Center;
            steelDataColumnsHeaderParagraph2.Format.Font.Size = MajorComponentFontSize;
            steelDataColumnsHeaderParagraph2.Format.Font.Bold = true;
            steelDataColumnsHeaderParagraph2.AddText("Outer Diameter - Inches (mm.)");

            var steelDataColumnsHeaderParagraph3 = steelDataColumnsHeaderRow[2].AddParagraph();
            steelDataColumnsHeaderParagraph3.Format.Alignment = ParagraphAlignment.Center;
            steelDataColumnsHeaderParagraph3.Format.Font.Size = MajorComponentFontSize;
            steelDataColumnsHeaderParagraph3.Format.Font.Bold = true;
            steelDataColumnsHeaderParagraph3.AddText("Pounds per Foot");

            var steelDataColumnsHeaderParagraph4 = steelDataColumnsHeaderRow[3].AddParagraph();
            steelDataColumnsHeaderParagraph4.Format.Alignment = ParagraphAlignment.Center;
            steelDataColumnsHeaderParagraph4.Format.Font.Size = MajorComponentFontSize;
            steelDataColumnsHeaderParagraph4.Format.Font.Bold = true;
            steelDataColumnsHeaderParagraph4.AddText("Schedule");

            foreach (var sd in steelData)
            {
                var steelDataRow = steelDataTable.AddRow();

                var steelDataColumn1 = steelDataRow[0].AddParagraph();
                steelDataColumn1.Format.Alignment = ParagraphAlignment.Center;
                steelDataColumn1.Format.Font.Size = MajorComponentFontSize;
                steelDataColumn1.AddText(sd.Size);

                var steelDataColumn2 = steelDataRow[1].AddParagraph();
                steelDataColumn2.Format.Alignment = ParagraphAlignment.Center;
                steelDataColumn2.Format.Font.Size = MajorComponentFontSize;
                steelDataColumn2.AddText(sd.OuterDiameter);

                var steelDataColumn3 = steelDataRow[2].AddParagraph();
                steelDataColumn3.Format.Alignment = ParagraphAlignment.Center;
                steelDataColumn3.Format.Font.Size = MajorComponentFontSize;
                steelDataColumn3.AddText(sd.PoundsPerFoot);

                var steelDataColumn4 = steelDataRow[3].AddParagraph();
                steelDataColumn4.Format.Alignment = ParagraphAlignment.Center;
                steelDataColumn4.Format.Font.Size = MajorComponentFontSize;
                steelDataColumn4.AddText(sd.Schedule);
            }
        }

        private static void CreateFooter(Section page)
        {
            var footerHeaderParagraph = page.AddParagraph();
            footerHeaderParagraph.Format.LeftIndent = LeftIndent;
            footerHeaderParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerHeaderParagraph.Format.SpaceBefore = Spacing;
            footerHeaderParagraph.Format.Font.Size = MajorComponentFontSize;
            footerHeaderParagraph.Format.Font.Bold = true;
            footerHeaderParagraph.AddText("Purchase Agreement, Terms & Conditions");

            var footerFirstParagraph = page.AddParagraph();
            footerFirstParagraph.Format.LeftIndent = LeftIndent;
            footerFirstParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerFirstParagraph.Format.Font.Size = FooterInfoFontSize;
            footerFirstParagraph.Format.SpaceBefore = Spacing;
            footerFirstParagraph.AddFormattedText("50% down payment ", RedFont);
            footerFirstParagraph.AddText("due upon order date. ");
            footerFirstParagraph.AddFormattedText("Remaining 50% plus shipping cost, ", RedFont);
            footerFirstParagraph.AddText("due net 30 days after equipment ship date.");

            var footerSecondParagraph = page.AddParagraph();
            footerSecondParagraph.Format.LeftIndent = LeftIndent;
            footerSecondParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerSecondParagraph.Format.Font.Size = FooterInfoFontSize;
            footerSecondParagraph.Format.SpaceBefore = Spacing;
            footerSecondParagraph.AddText("All late payments and unpaid balances will be charged at 1.5% interest per month.");

            var footerThirdParagraph = page.AddParagraph();
            footerThirdParagraph.Format.LeftIndent = LeftIndent;
            footerThirdParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerThirdParagraph.Format.Font.Size = FooterInfoFontSize;
            footerThirdParagraph.Format.SpaceBefore = Spacing;
            footerThirdParagraph.AddText("All quotes and pricing are good for 30 days from date stated above.");

            var footerFourthParagraph = page.AddParagraph();
            footerFourthParagraph.Format.LeftIndent = LeftIndent;
            footerFourthParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerFourthParagraph.Format.Font.Size = FooterInfoFontSize;
            footerFourthParagraph.Format.SpaceBefore = Spacing;
            footerFourthParagraph.AddFormattedText("Acceptance of Agreement: ", new Font { Bold = true, Underline = Underline.Single });
            footerFourthParagraph.AddText("The equipment description, price and terms given above, plus Cherry Creek Systems Inc.'s standard terms and conditions as outlined on the attached T&C page are satisfactory and hereby accepted. Payments will be made as outlined above. Cherry Creek Systems is authorized to ship the equipment as specified.");

            var footerFifthParagraph = page.AddParagraph();
            footerFifthParagraph.Format.LeftIndent = LeftIndent;
            footerFifthParagraph.Format.Borders.Width = new Unit(0.025, UnitType.Centimeter);
            footerFifthParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerFifthParagraph.Format.Font.Bold = true;
            footerFifthParagraph.Format.SpaceBefore = Spacing;
            footerFifthParagraph.AddText("Please sign and fax back to us at (719) 380-5343 or (719) 380-8695");

            var footerSixthParagraph = page.AddParagraph();
            footerSixthParagraph.Format.LeftIndent = LeftIndent;
            footerSixthParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerSixthParagraph.Format.Font.Size = new Unit(0.245, UnitType.Centimeter);
            footerSixthParagraph.Format.Font.Bold = true;
            footerSixthParagraph.Format.SpaceBefore = Spacing;
            footerSixthParagraph.AddText("* Signature gives Cherry Creek Systems the authorization to ship & charge for the equipment as specified above *");

            var footerSeventhParagraph = page.AddParagraph();
            footerSeventhParagraph.Format.LeftIndent = LeftIndent;
            footerSeventhParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerSeventhParagraph.Format.Font.Size = FooterInfoFontSize;
            footerSeventhParagraph.Format.SpaceBefore = Spacing * 1.5;
            footerSeventhParagraph.AddText(new string('_', 75));

            var footerEightParagraph = page.AddParagraph();
            footerEightParagraph.Format.LeftIndent = LeftIndent;
            footerEightParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerEightParagraph.Format.Font.Size = MajorComponentFontSize;
            footerEightParagraph.AddText("(Signature)".PadRight(35, SpaceCharacter) + "(Date)");

            var footerNinthParagraph = page.AddParagraph();
            footerNinthParagraph.Format.LeftIndent = LeftIndent;
            footerNinthParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerNinthParagraph.Format.Font.Size = FooterInfoFontSize;
            footerNinthParagraph.Format.SpaceBefore = Spacing * 1.5;
            footerNinthParagraph.AddText(new string('_', 75));

            var footerTenthParagraph = page.AddParagraph();
            footerTenthParagraph.Format.Font.Size = MajorComponentFontSize;
            footerTenthParagraph.Format.LeftIndent = LeftIndent;
            footerTenthParagraph.Format.Alignment = ParagraphAlignment.Center;
            footerTenthParagraph.AddText("(Printed Name & Title)");
        }

        private static void CreateQuotationSummary(Section page, QuoteRequest quoteRequest)
        {
            var titleParagraph = page.AddParagraph();
            titleParagraph.Format.LeftIndent = LeftIndent;
            titleParagraph.Format.Alignment = ParagraphAlignment.Center;
            titleParagraph.Format.SpaceBefore = Spacing;
            titleParagraph.Format.Font.Size = MajorComponentFontSize;
            titleParagraph.Format.Font.Bold = true;
            titleParagraph.Format.Font.Underline = Underline.Single;
            titleParagraph.AddText("Quotation Summary");

            var quotationSummaryTable = page.AddTable();
            quotationSummaryTable.Format.Font.Size = MajorComponentFontSize;
            var systemColumn = quotationSummaryTable.AddColumn();
            var quantityColumn = quotationSummaryTable.AddColumn();
            var priceColumn = quotationSummaryTable.AddColumn();
            var extendedPriceColumn = quotationSummaryTable.AddColumn();

            systemColumn.Format.Alignment = ParagraphAlignment.Left;
            systemColumn.Width = new Unit(11.5, UnitType.Centimeter);

            quantityColumn.Format.Alignment = ParagraphAlignment.Center;
            quantityColumn.Width = new Unit(0.2, UnitType.Centimeter);

            priceColumn.Format.Alignment = ParagraphAlignment.Right;
            priceColumn.Width = new Unit(4, UnitType.Centimeter);

            extendedPriceColumn.Format.Alignment = ParagraphAlignment.Right;
            extendedPriceColumn.Width = new Unit(3.8, UnitType.Centimeter);

            var headerRow = quotationSummaryTable.AddRow();
            headerRow.Format.SpaceBefore = Spacing;
            headerRow.Format.SpaceAfter = Spacing;

            var headerFirstParagraph = headerRow.Cells[0].AddParagraph();
            headerFirstParagraph.Format.Font.Size = MajorComponentFontSize;
            headerFirstParagraph.Format.Font.Bold = true;
            headerFirstParagraph.AddText("System Description");

            var headerSecondParagraph = headerRow.Cells[1].AddParagraph();
            headerSecondParagraph.Format.Font.Size = MajorComponentFontSize;
            headerSecondParagraph.Format.Font.Bold = true;
            headerSecondParagraph.AddText("Quantity");

            var headerThirdParagraph = headerRow.Cells[2].AddParagraph();
            headerThirdParagraph.Format.Font.Size = MajorComponentFontSize;
            headerThirdParagraph.Format.Font.Bold = true;
            headerThirdParagraph.AddText("Unit Price");

            var headerFourthParagraph = headerRow.Cells[3].AddParagraph();
            headerFourthParagraph.Format.Font.Size = MajorComponentFontSize;
            headerFourthParagraph.Format.Font.Bold = true;
            headerFourthParagraph.AddText("Extended Price");

            var systems = quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems;
            var subTotalPrice = 0.0;
            foreach (var sys in systems)
            {
                var systemName = sys.DetailCalculationResult + " " + sys.Name;
                var systemCount = sys.Quantity;
                var systemPrice = sys.GetTotalCost();
                var extendedPrice = systemPrice * systemCount;
                subTotalPrice += extendedPrice;

                var systemRow = quotationSummaryTable.AddRow();

                var nameParagraph = systemRow.Cells[0].AddParagraph();
                nameParagraph.AddText(systemName);

                var quantityParagraph = systemRow.Cells[1].AddParagraph();
                quantityParagraph.AddText(systemCount.ToString());

                var priceParagraph = systemRow.Cells[2].AddParagraph();
                priceParagraph.AddText(systemPrice.ToString("C2"));

                var extendedPriceParagraph = systemRow.Cells[3].AddParagraph();
                extendedPriceParagraph.AddText(extendedPrice.ToString("C2"));
            }

            var customItems = quoteRequest.CustomQuoteItems;
            foreach (var custom in customItems.Where(c => c.Active))
            {
                var name = custom.Description;
                var count = custom.Quantity;
                var price = custom.Price;
                var extendedPrice = count * price;
                subTotalPrice += extendedPrice;

                var systemRow = quotationSummaryTable.AddRow();

                var nameParagraph = systemRow.Cells[0].AddParagraph();
                nameParagraph.AddText(name);

                var quantityParagraph = systemRow.Cells[1].AddParagraph();
                quantityParagraph.AddText(count.ToString());

                var priceParagraph = systemRow.Cells[2].AddParagraph();
                priceParagraph.AddText(price.ToString("C2"));

                var extendedPriceParagraph = systemRow.Cells[3].AddParagraph();
                extendedPriceParagraph.AddText(extendedPrice.ToString("C2"));
            }

            var subTotalRow = quotationSummaryTable.AddRow();
            subTotalRow.Format.SpaceBefore = Spacing;

            var subtotalLabelParagraph = subTotalRow.Cells[2].AddParagraph();
            subtotalLabelParagraph.Format.Font.Bold = true;
            subtotalLabelParagraph.AddText("Sub-Total:");

            var subtotalParagraph = subTotalRow.Cells[3].AddParagraph();
            subtotalParagraph.Format.Font.Bold = true;
            subtotalParagraph.AddText(subTotalPrice.ToString("C2"));

            var steelEstimateRow = quotationSummaryTable.AddRow();

            var steelEstimateLabelParagraph = steelEstimateRow.Cells[2].AddParagraph();
            steelEstimateLabelParagraph.Format.Font.Bold = true;
            steelEstimateLabelParagraph.AddText("Steel Estimate:");

            var steelEstimateDisplay = quoteRequest.SteelEstimateValue != 0.00 ?
                             quoteRequest.SteelEstimateValue.ToString("C2", _numberFormat) :
                             quoteRequest.SteelEstimate ?? string.Empty;

            var steelEstimateParagraph = steelEstimateRow.Cells[3].AddParagraph();
            steelEstimateParagraph.Format.Font.Bold = true;
            steelEstimateParagraph.AddText(steelEstimateDisplay);

            var installationEstimateRow = quotationSummaryTable.AddRow();

            var installationEstimateLabelParagraph = installationEstimateRow.Cells[2].AddParagraph();
            installationEstimateLabelParagraph.Format.Font.Bold = true;
            installationEstimateLabelParagraph.AddText("Installation Estimate:");

            var installationEstimateDisplay = quoteRequest.InstallationEstimateValue != 0.00 ?
                             quoteRequest.InstallationEstimateValue.ToString("C2", _numberFormat) :
                             quoteRequest.InstallationEstimate ?? string.Empty;

            var installationEstimateParagraph = installationEstimateRow.Cells[3].AddParagraph();
            installationEstimateParagraph.Format.Font.Bold = true;
            installationEstimateParagraph.AddText(installationEstimateDisplay);

            var freightEstimateRow = quotationSummaryTable.AddRow();

            var freightEstimateLabelParagraph = freightEstimateRow.Cells[2].AddParagraph();
            freightEstimateLabelParagraph.Format.Font.Bold = true;
            freightEstimateLabelParagraph.AddText("Freight Estimate:");

            var freightEstimateDisplay = quoteRequest.FreightEstimateValue != 0.00 ?
                             quoteRequest.FreightEstimateValue.ToString("C2", _numberFormat) :
                             quoteRequest.FreightEstimate ?? string.Empty;

            var freightEstimateParagraph = freightEstimateRow.Cells[3].AddParagraph();
            freightEstimateParagraph.Format.Font.Bold = true;
            freightEstimateParagraph.AddText(freightEstimateDisplay);

            var discountRow = quotationSummaryTable.AddRow();
            //Force the discount value to always be negative
            var discountValue = quoteRequest.DiscountValue;

            var discountLabelParagraph = discountRow.Cells[2].AddParagraph();
            discountLabelParagraph.Format.Font.Bold = true;
            discountLabelParagraph.AddText("Discount:");

            var discountDisplay = discountValue != 0.00 ?
                             discountValue.ToString("C2", _numberFormat) :
                             quoteRequest.Discount ?? string.Empty;

            var discountParagraph = discountRow.Cells[3].AddParagraph();
            discountParagraph.Format.Font.Bold = true;
            discountParagraph.AddText(discountDisplay);

            var totalPriceRow = quotationSummaryTable.AddRow();

            var totalPriceLabelParagraph = totalPriceRow.Cells[2].AddParagraph();
            totalPriceLabelParagraph.Format.Font.Bold = true;
            totalPriceLabelParagraph.AddText("Total:");

            var totalPrice = subTotalPrice
                + quoteRequest.SteelEstimateValue
                + quoteRequest.FreightEstimateValue
                + quoteRequest.InstallationEstimateValue
                + discountValue;

            var totalPriceParagraph = totalPriceRow.Cells[3].AddParagraph();
            totalPriceParagraph.Format.Font.Bold = true;
            totalPriceParagraph.AddText(totalPrice.ToString("C2"));
        }

        private static void CreateGreenhouseInfo(Section page, Boom boom)
        {
            var titleParagraph = page.AddParagraph();
            titleParagraph.Format.LeftIndent = LeftIndent;
            titleParagraph.Format.Alignment = ParagraphAlignment.Center;
            titleParagraph.Format.SpaceBefore = Spacing;
            titleParagraph.Format.Font.Size = MajorComponentFontSize; ;
            titleParagraph.Format.Font.Bold = true;
            titleParagraph.Format.Font.Underline = Underline.Single;
            titleParagraph.AddText("Greenhouse Specifications");

            //Greenhouse table
            var greenhouseTable = page.AddTable();
            greenhouseTable.Format.Font.Size = MajorComponentFontSize;
            var greenhouseFirstColumn = greenhouseTable.AddColumn();
            var greenhouseSecondColumn = greenhouseTable.AddColumn();
            var greenhouseThirdColumn = greenhouseTable.AddColumn();

            greenhouseFirstColumn.Format.Alignment = ParagraphAlignment.Left;
            greenhouseFirstColumn.Width = new Unit(7, UnitType.Centimeter);

            greenhouseSecondColumn.Format.Alignment = ParagraphAlignment.Left;
            greenhouseSecondColumn.Width = new Unit(7, UnitType.Centimeter);

            greenhouseThirdColumn.Format.Alignment = ParagraphAlignment.Left;
            greenhouseThirdColumn.Width = new Unit(7, UnitType.Centimeter);

            var customerFirstRow = greenhouseTable.AddRow();
            customerFirstRow.Format.SpaceBefore = Spacing;

            var customerFirstParagraph = customerFirstRow.Cells[0].AddParagraph();
            customerFirstParagraph.AddTextAndLineBreak("Type of House:".PadRight(27, SpaceCharacter) + boom.GreenHouseType.Name);
            customerFirstParagraph.AddTextAndLineBreak("Bay Length (ft):".PadRight(27, SpaceCharacter) + boom.BayLengthInFeet);
            customerFirstParagraph.AddTextAndLineBreak("Bay Width (ft):".PadRight(27, SpaceCharacter) + boom.BayWidthInFeet);
            customerFirstParagraph.AddTextAndLineBreak("Number of Bays:".PadRight(27, SpaceCharacter) + boom.NumberOfBays);
            customerFirstParagraph.AddTextAndLineBreak("Number of Bay Sidewalls:".PadRight(27, SpaceCharacter) + boom.NumberOfBaySideWalls);
            customerFirstParagraph.AddTextAndLineBreak("Walkway Width (in):".PadRight(27, SpaceCharacter) + boom.WalkwayWidthInInches);

            var customerSecondParagraph = customerFirstRow.Cells[1].AddParagraph();
            customerSecondParagraph.AddTextAndLineBreak("Bottom Cord Height (ft):".PadRight(30, SpaceCharacter) + boom.BottomCordHeightInFeet);
            customerSecondParagraph.AddTextAndLineBreak("Bottom Cord Type (Sq/Rd):".PadRight(30, SpaceCharacter) + boom.BottomCordType);
            customerSecondParagraph.AddTextAndLineBreak("Bottom Cord Dimensions:".PadRight(30, SpaceCharacter) + boom.BottomCordSize);
            customerSecondParagraph.AddTextAndLineBreak("Truss Spacing (ft):".PadRight(30, SpaceCharacter) + boom.TrussSpacingInFeet);
            customerSecondParagraph.AddTextAndLineBreak("Post Spacing (ft):".PadRight(30, SpaceCharacter) + boom.PostSpacingInFeet);
            customerSecondParagraph.AddTextAndLineBreak("Post Dimensions (in x in):".PadRight(30, SpaceCharacter) + boom.PostDimensionsInInchesSquared);

            var customerThirdParagraph = customerFirstRow.Cells[2].AddParagraph();
            customerThirdParagraph.AddTextAndLineBreak("Growing Surface Height (in):".PadRight(30, SpaceCharacter) + boom.GrowingSurfaceHeightInInches);
            customerThirdParagraph.AddTextAndLineBreak("Location:".PadRight(12, SpaceCharacter) + boom.Location);
        }

        private static void CreateCustomerInfo(Section page, QuoteRequest quoteRequest)
        {
            //Customer table
            var customerTable = page.AddTable();
            customerTable.Format.Font.Size = MajorComponentFontSize;
            var customerFirstColumn = customerTable.AddColumn();
            var customerSecondColumn = customerTable.AddColumn();

            customerFirstColumn.Format.Alignment = ParagraphAlignment.Left;
            customerFirstColumn.Width = new Unit(13, UnitType.Centimeter);

            customerSecondColumn.Format.Alignment = ParagraphAlignment.Left;
            customerSecondColumn.Width = new Unit(9, UnitType.Centimeter);

            var customerFirstRow = customerTable.AddRow();
            customerFirstRow.Format.SpaceBefore = Spacing;

            var customerLeftParagraph = customerFirstRow.Cells[0].AddParagraph();
            customerLeftParagraph.AddTextAndLineBreak("Quote #:".PadRight(18, SpaceCharacter) + quoteRequest.QuoteNumberWithRevision);
            customerLeftParagraph.AddTextAndLineBreak("Purchase Order:".PadRight(18, SpaceCharacter) + quoteRequest.PurchaseOrder);
            customerLeftParagraph.AddTextAndLineBreak("Customer:".PadRight(18, SpaceCharacter) + quoteRequest.Customer.CompanyName);
            customerLeftParagraph.AddTextAndLineBreak("Distributor:".PadRight(18, SpaceCharacter) + quoteRequest.Distributor.Name);
            customerLeftParagraph.AddTextAndLineBreak("Attn:".PadRight(18, SpaceCharacter) + quoteRequest.ContactName);
            customerLeftParagraph.AddTextAndLineBreak("Phone:".PadRight(18, SpaceCharacter) + quoteRequest.Phone);
            customerLeftParagraph.AddTextAndLineBreak("E-mail:".PadRight(18, SpaceCharacter) + quoteRequest.Email);

            var customerRightParagraph = customerFirstRow.Cells[1].AddParagraph();
            customerRightParagraph.AddTextAndLineBreak("Date:".PadRight(22, SpaceCharacter) + DateTime.Now.ToShortDateString());
            customerRightParagraph.AddTextAndLineBreak("Lead Time (Weeks):".PadRight(22, SpaceCharacter) + quoteRequest.LeadTime);
            customerRightParagraph.AddTextAndLineBreak("PO #:".PadRight(22, SpaceCharacter) + "________");
        }

        #endregion general quote report

        public string GenerateBuildDocument(QuoteBoomSystem quoteBoomSystem, QuoteRequest quoteRequest)
        {
            var pdfRenderer = new PdfDocumentRenderer(false);
            var document = new Document();
            document.DefaultPageSetup.TopMargin = TopMargin;
            document.DefaultPageSetup.LeftMargin = LeftMargin;
            document.DefaultPageSetup.BottomMargin = BottomMargin;

            var style = document.Styles["Normal"];
            style.Font.Name = Font;

            var customerPage = document.AddSection();
            AddPageNumber(customerPage);
            PopulateCustomerBuildPage(customerPage, quoteRequest, quoteBoomSystem);

            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;
            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            // Save the document...
            var timeStamp = DateTime.Now.Ticks % 1000;
            var cleanQuoteNumber = illegalInFileName.Replace(quoteRequest.QuoteNumber, "");
            var cleanSystemName = illegalInFileName.Replace(quoteBoomSystem.Name, "");
            var filename = $"{cleanQuoteNumber}_{cleanSystemName}_Build_Document_{timeStamp}.pdf";
            pdfRenderer.PdfDocument.Save(filename);
            // ...and start a viewer.
            return filename;
        }

        private void PopulateCustomerBuildPage(Section page, QuoteRequest quoteRequest, QuoteBoomSystem quoteBoomSystem)
        {
            CreateHeader(page, _logoPath);
            CreateCustomerInfoDetailedQuote(page, quoteRequest, quoteBoomSystem.Boom, includeShippingInfo: true, includeSystemInformation: false);
            CreateGreenhouseInfo(page, quoteBoomSystem.Boom);
            CreateSystemNameParagraph(page, quoteBoomSystem, forBuildDocument: true);
            CreateRegularComponentsTable(page, quoteBoomSystem, forBuildDocument: true, includePricing: false);
            CreateOptionalComponentsTable(page, quoteBoomSystem, forBuildDocument: true, includeWithoutValue: false, includePricing: false);
            CreateAdditionalInformationParagraph(page, quoteBoomSystem.Boom);
        }

        //public string GenerateBoomChecklistForm()
        //{
        //    var path = AppDomain.CurrentDomain.BaseDirectory + @"\Boom_Checklist_2015_distributed.pdf";
        //    using (var stream = new FileOutputStream())
        //    {
        //        iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(path);
        //        PdfStamper stamper =
        //            new PdfStamper(reader, new FileOutputStream(dest), '\0', true);
        //        stamper.Close();
        //    }
        //    // Create a renderer for the MigraDoc document.
        //    //PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false);
        //    //pdfRenderer.PdfDocument.
        //    // Associate the MigraDoc document with a renderer
        //    //pdfRenderer.Document = document;
        //    // Layout and render document to PDF
        //    //pdfRenderer.RenderDocument();

        //    //var fileName = CreateQuoteReportFileName(quoteRequest.QuoteNumber);
        //    //pdfRenderer.PdfDocument.Save("test.pdf");

        //    return "test.pdf";
        //}
    }
}