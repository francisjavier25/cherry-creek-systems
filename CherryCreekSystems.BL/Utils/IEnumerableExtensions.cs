﻿using System.Collections.Generic;

namespace CherryCreekSystems.BL.Utils
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<string> WithFirstEmptyValue(this IEnumerable<string> specValues)
        {
            yield return string.Empty;
            foreach (var s in specValues)
            {
                yield return s;
            }
        }
    }
}