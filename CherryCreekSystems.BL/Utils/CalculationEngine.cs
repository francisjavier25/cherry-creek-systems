﻿using CherryCreekSystems.Domain;
using ciloci.FormulaEngine;
using log4net;
using System;
using System.Linq;
using System.Reflection;

namespace CherryCreekSystems.BL.Utils
{
    public class CalculationEngine
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly FormulaEngine Engine;

        public QuoteRequest QuoteRequest { get; set; }

        public Boom Boom { get; set; }

        public double BoomBaseValue { get; set; }

        public CalculationEngine()
        {
            Engine = new FormulaEngine();
        }

        public void RegisterIndividualValue(string name, double value)
        {
            INamedReference formulaReference = Engine.ReferenceFactory.Named(name);
            Formula formula = Engine.CreateFormula($"={value}");
            Engine.AddFormula(formula, formulaReference);
        }

        public void RegisterQuoteAndBoomValues()
        {
            RegisterValues(QuoteRequest);
            RegisterValues(Boom);

            INamedReference formulaReference = Engine.ReferenceFactory.Named("BaseValue");
            Formula formula = Engine.CreateFormula($"={BoomBaseValue}");
            Engine.AddFormula(formula, formulaReference);
        }

        private void RegisterValues(object source)
        {

            var name = string.Empty;
            try
            {
                var properties = source.GetType().GetProperties();//.Where(p => p.IsDefined(typeof(CalculationEnabled), true));
                //Since entity framework returns dynamic proxies, we might need to check for properties on the base type (original class)
                if (!properties.Any())
                    properties = source.GetType().BaseType.GetProperties();//.Where(p => p.IsDefined(typeof(CalculationEnabled), true));

                foreach (var p in properties)
                {
                     name = p.Name;
                    var value = p.GetValue(source);

                    INamedReference formulaReference = Engine.ReferenceFactory.Named($"{p.Name}");
                    if (Engine.HasFormulaAt(formulaReference)) continue;

                    Formula formula = null;
                    Type type = p.PropertyType;
                    if (type == typeof(string))
                        formula = Engine.CreateFormula($@"=""{value}""");
                    if (type == typeof(int) || type == typeof(double) || type == typeof(decimal) || type == typeof(bool))
                        formula = Engine.CreateFormula($"={value}");

                    //formula could be null if the property found is a complex type
                    if (formula != null)
                        Engine.AddFormula(formula, formulaReference);
                }
            }
            catch (Exception ex)
            {
                log.Error("RegisterValues", ex);
                throw;
            }
        }

        public string PerformCalculation(string calculation, string calculationId = "", string componentName = "")
        {
            if (!string.IsNullOrEmpty(calculation))
            {
                if (!calculation.StartsWith("="))
                {
                    calculation = calculation.Insert(0, "=");
                }

                try
                {
                    var result = GetFormulaCalculationResult(calculation, calculationId, componentName);

                    return result;
                }
                catch (Exception ex)
                {
                    //Calculation could not parse
                    throw;
                }
            }

            return string.Empty;
        }

        private string GetFormulaCalculationResult(string calculation, string calculationId = "", string componentName = "")
        {
            var formulaWithValues = string.Empty;
            try
            {
                formulaWithValues = GetNameCalculationResult(calculation);
                calculation = calculation.Replace("\r\n", "");
                Formula formula = Engine.CreateFormula(calculation);
                var result = formula.Evaluate();

                var resultValue = result.ToString();
                if (resultValue == "#NAME?" || resultValue == "#DIV/0!" || resultValue == "#VALUE!")
                    throw new Exception("Result is " + resultValue + " for formula " + calculationId + ": " + calculation);

                if (!string.IsNullOrEmpty(calculationId))
                {
                    INamedReference formulaReference = Engine.ReferenceFactory.Named(calculationId);
                    Engine.AddFormula(formula, formulaReference);
                }

                return resultValue;
            }
            catch (Exception ex)
            {
                log.Error("GetFormulaCalculationResult", ex);
                var formulaError = $"{componentName}: {calculation}";
                formulaError += $"{Environment.NewLine}{componentName}: {formulaWithValues}";
                Exception e = new Exception(formulaError);
                throw e;
            }
        }

        private string GetNameCalculationResult(string name)
        {
            var references = Engine.GetNamedReferences();
            foreach (INamedReference r in references)
            {
                var referenceName = r.Name;
                if (name.Contains(referenceName))
                    name = name.Replace(referenceName, r.Result.ToString());
            }

            return name;
        }
    }
}