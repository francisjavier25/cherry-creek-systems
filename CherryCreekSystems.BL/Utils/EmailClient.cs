﻿using System;
using System.Collections.Generic;
using System.Linq;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL
{
    public class EmailClient
    {
        public class EmailAttachment
        {
            public string Name;
            public string Type;
            public byte[] Bytes;
        }

        public static async Task<bool> SendEmail(string to, string from, string bcc, string cc, string body, string subject, List<EmailAttachment> attachments)
        {
            var client = new SendGrid.SendGridAPIClient("SG.vHFmRwulRXSYRppYA9yvHA.EAA_vRym81JaF0EChGKYWtRA4xvy74QB9YkirAGgY10");

            var email = new Mail(new Email(from), subject, new Email(to), new Content("text/html", body));

            if (!string.IsNullOrEmpty(bcc))
            {
                email.Personalization[0].Bccs = new List<Email>
                {
                    new Email(bcc)
                };
            };

            if (!string.IsNullOrEmpty(cc))
            {
                email.Personalization[0].Ccs = new List<Email>
                {
                    new Email(cc)
                };
            }

            if (attachments != null)
            {
                email.Attachments = attachments.Select(a => new Attachment
                {
                    Filename = a.Name,
                    Type = a.Type,
                    Content = Convert.ToBase64String(a.Bytes)
                }).ToList();
            }

            var emailRequest = email.Get();
            var response = await client.client.mail.send.post(requestBody: emailRequest);

            return response.StatusCode.ToString().ToLower() == "accepted";
        }

        //public static Task<bool> SendExceptionEmail(Exception ex, string jsonData)
        //{
        //    return SendEmail(Config.GetExceptionNotifyEmail(), Config.GetFromEmail(), null, null,
        //          jsonData + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, "EyeRep Admin Exception");
        //}
    }
}