﻿using CherryCreekSystems.Domain;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CherryCreekSystems.BL.Utils
{
    public static class GenericExtensions
    {
        public static string ToJson<T>(this List<T> list)
        {
            return JsonConvert.SerializeObject(list);
        }

        public static IEnumerable<TViewModel> ToViewModel<TViewModel>(this IEnumerable<ISimpleEntity> list)
            where TViewModel : ISimpleEntityViewModel, new()
        {
            return list.Select(c => new TViewModel
            {
                Id = c.Id,
                Name = c.Name
            });
        }

        public static IEnumerable<T> WithFirstEmptyValue<T>(this IEnumerable<T> query) where T : ISimpleEntityViewModel, new()
        {
            yield return new T { Id = null, Name = string.Empty };

            foreach (var b in query)
            {
                yield return b;
            }
        }
    }
}