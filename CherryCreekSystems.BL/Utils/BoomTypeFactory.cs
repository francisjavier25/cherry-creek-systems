﻿using CherryCreekSystems.Domain;

namespace CherryCreekSystems.BL.Utils
{
    public static class BoomTypeFactory
    {
        public static Boom CreateBoomType(BoomType boomTypeId)
        {
            Boom boom;

            switch (boomTypeId)
            {
                case BoomType.Echo:
                    boom = new Echo();
                    break;

                case BoomType.SingleRail:
                    boom = new SingleRail();
                    break;

                case BoomType.DoubleRail:
                    boom = new DoubleRail();
                    break;

                case BoomType.CWFDoubleRail:
                    boom = new CenterWaterFeedDoubleRail();
                    break;

                case BoomType.Tower:
                    boom = new Tower();
                    break;

                case BoomType.Navigator:
                    boom = new Navigator();
                    break;

                case BoomType.GroundRunner:
                    boom = new GroundRunner();
                    break;

                case BoomType.SkyRail:
                    boom = new SkyRail();
                    break;

                default:
                    boom = null;
                    break;
            }

            return boom;
        }
    }
}