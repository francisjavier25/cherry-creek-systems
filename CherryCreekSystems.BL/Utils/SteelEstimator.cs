﻿using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using System.Collections.Generic;
using System.Linq;

namespace CherryCreekSystems.BL.Utils
{
    public class SteelEstimator
    {
        private List<SteelEstimate> steelEstimates;
        private CalculationEngine calculationEngine;

        public SteelEstimator(List<SteelEstimate> estimates)
        {
            steelEstimates = estimates;
            calculationEngine = new CalculationEngine();
        }

        public List<SteelEstimate> EstimateSteelQuantities(QuoteRequest quoteRequest, QuoteBoomSystem system)
        {
            var estimatesForBoom = steelEstimates;

            calculationEngine.QuoteRequest = quoteRequest;
            calculationEngine.Boom = system.Boom;
            calculationEngine.RegisterQuoteAndBoomValues();
            calculationEngine.RegisterIndividualValue("SystemCount", system.Quantity);

            foreach (var estimate in estimatesForBoom.ToList())
            {
                bool activation;
                bool.TryParse(calculationEngine.PerformCalculation(estimate.ActivateEstimateCondition), out activation);

                if (activation)
                {
                    var calculationResult = calculationEngine.PerformCalculation(estimate.QuantityCalculation);
                    estimate.QuantityCalculationResult = calculationResult;
                }
                else
                {
                    estimatesForBoom.Remove(estimate);
                }
            }

            return estimatesForBoom;
        }
    }
}