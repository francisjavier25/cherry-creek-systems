﻿using Microsoft.Azure;
using System;
using System.Configuration;

namespace CherryCreekSystems.BL.Utils
{
    public static class Configuration
    {
        public static bool EnableTestData
        {
            get
            {
                var enableTestData = false;

                try
                {
                    enableTestData = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableTestData"]);
                }
                catch
                {
                    enableTestData = false;
                }

                return enableTestData;
            }
        }

        public static string StorageAccountConnectionString
        {
            get
            {
                string storageAccountConnectionString;

                try
                {
                    storageAccountConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];
                }
                catch
                {
                    storageAccountConnectionString = string.Empty;
                }

                return storageAccountConnectionString;
            }
        }

        public static string QuotesBridgeErrorNotificationEmails
        {
            get
            {
                string quotesBridgeErrorNotificationEmails = "";

                try
                { 
                    quotesBridgeErrorNotificationEmails = CloudConfigurationManager.GetSetting("QuotesBridgeErrorNotificationEmail");
                }
                catch
                {
                    quotesBridgeErrorNotificationEmails = "fjavier_5@hotmail.com";
                }

                return quotesBridgeErrorNotificationEmails;
            }
        }
    }
}