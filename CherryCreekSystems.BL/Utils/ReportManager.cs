﻿using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.Domain;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL.Utils
{
    public class ReportManager
    {
        private QuoteRequest QuoteRequest { get; set; }

        private List<SteelEstimate> SteelEstimates { get; set; }

        private List<SteelData> SteelData { get; set; }

        /// <summary>
        /// Generate main report with all detail quotes
        /// </summary>
        /// <returns>Returns file name of report generated</returns>
        public async Task<string> GenerateMainQuoteReportFile(int quoteId)
        {
            await InitializeMainReportData(quoteId);

            QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
            var reportName = reportGenerator.GenerateQuoteReport(QuoteRequest, SteelEstimates, SteelData);

            return reportName;
        }

        /// <summary>
        /// Generate main report with all detail quotes
        /// </summary>
        /// <returns>Returns stream of report generated</returns>
        public async Task<Stream> GenerateMainQuoteReportStream(int quoteId)
        {
            await InitializeMainReportData(quoteId);

            QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
            var stream = reportGenerator.GenerateQuoteReportStream(QuoteRequest, SteelEstimates, SteelData);

            return stream;
        }

        /// <summary>
        /// Generate a report file name for streams
        /// </summary>
        /// <param name="quoteNumber"></param>
        /// <returns></returns>
        public string CreateMainReportFileName(string quoteNumber)
        {
            QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
            var fileName = reportGenerator.CreateQuoteReportFileName(quoteNumber);

            return fileName;
        }

        /// <summary>
        /// Generate a dedail report file name for streams
        /// </summary>
        /// <param name="quoteNumber"></param>
        /// <returns></returns>
        public string CreateDetailReportFileName(string quoteNumber, string systemName)
        {
            QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
            var fileName = reportGenerator.CreateDetailQuoteReportFileName(quoteNumber, systemName);

            return fileName;
        }

        private async Task InitializeMainReportData(int quoteId)
        {
            var steelDataTask = PopulateSteelDatas();

            var quoteRequestService = new QuoteRequestService();
            QuoteRequest = await quoteRequestService.Get().ByQuoteId(quoteId);
            await quoteRequestService.FillChildObjects(customer: true, greenHouse: true, distributor: true,
                                           customItem: true,
                                           quoteRequests: QuoteRequest);

            var steelEstimatesTask = PopulateSteelEstimates();

            foreach (var s in QuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive())
            {
                s.Ordered();
            }
            
            //Force the systems object to have only active systems before being passed to the report generator
            QuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems =
                QuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive().ToList();

            await Task.WhenAll(steelDataTask, steelEstimatesTask);
        }

        /// <summary>
        /// Generate individual system report
        /// </summary>
        /// <returns>Returns file name of report generated</returns>
        public async Task<string> GenerateDetailQuoteReportFile(int quoteId, string quoteBoomSystemId)
        {
            Domain.BoomComponents.QuoteBoomSystem system = await InitializeDetailReportData(quoteId, quoteBoomSystemId);

            QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
            var reportName = reportGenerator.GenerateDetailQuoteReport(system, QuoteRequest);

            return reportName;
        }

        /// <summary>
        /// Generate individual system report
        /// </summary>
        /// <returns>Returns stream of report generated</returns>
        public async Task<Stream> GenerateDetailQuoteReportStream(int quoteId, string quoteBoomSystemId)
        {
            Domain.BoomComponents.QuoteBoomSystem system = await InitializeDetailReportData(quoteId, quoteBoomSystemId);

            QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
            var stream = reportGenerator.GenerateDetailQuoteStream(system, QuoteRequest);

            return stream;
        }

        private async Task<Domain.BoomComponents.QuoteBoomSystem> InitializeDetailReportData(int quoteId, string quoteBoomSystemId)
        {
            var quoteRequestService = new QuoteRequestService();
            QuoteRequest = await quoteRequestService.Get().ByQuoteId(quoteId);
            await quoteRequestService.FillChildObjects(customer: true, greenHouse: true, distributor: true,
                                           customItem: true,
                                           quoteRequests: QuoteRequest);

            var system =
                QuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.FirstOrDefault(s => s.Id_ == quoteBoomSystemId).Ordered();
            return system;
        }

        /// <summary>
        /// Generate individual build report
        /// </summary>
        /// <returns>Returns file name of report generated</returns>
        public async Task<string> GenerateDetailBuildReport(int quoteId, string quoteBoomSystemId)
        {
            var quoteRequestService = new QuoteRequestService();
            QuoteRequest = await quoteRequestService.Get().ByQuoteId(quoteId);
            await quoteRequestService.FillChildObjects(customer: true, greenHouse: true, distributor: true,
                               customItem: true,
                               quoteRequests: QuoteRequest);

            var system =
                QuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.FirstOrDefault(s => s.Id_ == quoteBoomSystemId);

            QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
            var reportName = reportGenerator.GenerateBuildDocument(system, QuoteRequest);

            return reportName;
        }

        /// <summary>
        /// Generate sales order form
        /// </summary>
        /// <returns>Returns file name of report generated</returns>
        public async Task<string> GenerateSalesOrderForm(int quoteId, string quoteBoomSystemId)
        {
            var quoteRequestService = new QuoteRequestService();
            QuoteRequest = await quoteRequestService.Get().ByQuoteId(quoteId);
            await quoteRequestService.FillChildObjects(customer: true, distributor: true,
                                                       quoteRequests: QuoteRequest);

            var system =
                QuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.FirstOrDefault(s => s.Id_ == quoteBoomSystemId);

            var reportName = QuoteReportGenerator.GenerateSalesOrderForm(QuoteRequest, system);

            return reportName;
        }

        private async Task PopulateSteelEstimates()
        {
            var steelEstimateService = new SteelEstimateService();
            var allEstimates = await steelEstimateService.Get().ToListAsync();
            SteelEstimates = new List<SteelEstimate>();

            var booms = new Dictionary<int, bool>();
            foreach (var system in QuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems)
            {
                //If any manual steel estimates have been entered, don't try to calcualte anything
                if (system.SteelMaterials.AnyActive())
                    continue;

                bool exists;
                if (booms.TryGetValue(system.Boom.BoomTypeId, out exists)) //If the boom steel estimates are already in the list
                    continue;

                var applicableEstimates = allEstimates.ByBoomId(system.Boom.BoomTypeId);
                var steelEstimator = new SteelEstimator(applicableEstimates);
                var calculatedEstimates = steelEstimator.EstimateSteelQuantities(QuoteRequest, system);
                SteelEstimates.AddRange(calculatedEstimates);

                booms.Add(system.Boom.BoomTypeId, true);
            }
        }

        private async Task PopulateSteelDatas()
        {
            var steelDataService = new SteelDataService();
            SteelData = await steelDataService.Get().ToListAsync();
        }
    }
}