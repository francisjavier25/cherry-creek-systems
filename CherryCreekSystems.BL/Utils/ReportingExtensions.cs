﻿using MigraDoc.DocumentObjectModel;

namespace CherryCreekSystems.BL.Utils
{
    public static class ReportingExtensions
    {
        public static void AddTextAndLineBreak(this Paragraph paragraph, string text)
        {
            paragraph.AddText(text);
            paragraph.AddLineBreak();
        }
    }
}