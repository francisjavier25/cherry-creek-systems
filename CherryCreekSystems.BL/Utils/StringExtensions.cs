﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace CherryCreekSystems.BL.Utils
{
    public static class ValueExtensions
    {
        /// <summary>
        /// Converts a string that contains 'yes' to its boolean representation, any other value will be false
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ToBoolean(this string value)
        {
            return !string.IsNullOrWhiteSpace(value) && value.ToUpper() == "YES";
        }

        public static string ToStringAnswer(this bool value)
        {
            return value ? "Yes" : "No";
        }

        public static string FirstWord(this string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty; ;

            var words = value.Split(' ');
            return words.Any() ? words[0] : string.Empty;
        }

        public static string ToSafeString(this string value)
        {
            return value != null ? value : string.Empty;
        }

        public static string ToSafeString(this object value)
        {
            return value != null ? value.ToString() : string.Empty;
        }

        public static bool IsNotEmpty(this string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }

        public static double ToSafeDouble(this double? value)
        {
            return value.HasValue ? value.Value : 0;
        }

        public static int ToSafeInt(this int? value)
        {
            return value.HasValue ? value.Value : 0;
        }

        public static DateTime ToSafeDate(this string value)
        {
            DateTime.TryParse(value, out DateTime date);
            return date;
        }
    }
}