﻿using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using CherryCreekSystems.BL.QueryExtensions;
using Newtonsoft.Json;

namespace CherryCreekSystems.BL.Utils
{
    public static class QueryExtensions
    {
        /// <summary>
        /// Will order sub-collections by using the Order field
        /// </summary>
        /// <param name="boomSystem"></param>
        /// <returns></returns>
        public static BoomSystem Ordered(this BoomSystem boomSystem)
        {
            if (boomSystem.Categories != null)
            {
                boomSystem.Categories = boomSystem.Categories.OrderBy(c => c.Order).ToList();
                foreach (var category in boomSystem.Categories)
                {
                    if (category.MajorComponents != null)
                    {
                        category.MajorComponents = category.MajorComponents.OrderBy(c => c.Order).ToList();
                        foreach (var majorComponent in category.MajorComponents)
                        {
                            if (majorComponent.MajorComponentDetails != null)
                            {
                                majorComponent.MajorComponentDetails = majorComponent.MajorComponentDetails.OrderBy(c => c.Order).ToList();
                            }

                            if (majorComponent.MinorComponents != null)
                            {
                                majorComponent.MinorComponents = majorComponent.MinorComponents.OrderBy(c => c.Order).ToList();
                                foreach (var minorComponent in majorComponent.MinorComponents)
                                {
                                    if (minorComponent.MinorComponentDetails != null)
                                    {
                                        minorComponent.MinorComponentDetails =
                                            minorComponent.MinorComponentDetails.OrderBy(c => c.Order).ToList();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return boomSystem;
        }

        //public static string Serialized(this QuoteBoomSystem quoteBoomSystem)
        //{
        //    return quoteBoomSystem != null ? JsonConvert.SerializeObject(quoteBoomSystem) : string.Empty;
        //}

        /// <summary>
        /// Used to return the lookup values for a specific spec, if it has multiple possible values
        /// </summary>
        /// <param name="specs"></param>
        /// <param name="SpecName"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetValues(this IEnumerable<Spec> specs, string SpecName)
        {
            var specName = SpecName.ToLower();
            var spec = specs.FirstOrDefault(s => s.Name.ToLower() == specName);
            if (spec?.SpecLookups != null && spec.SpecLookups.Count > 0)
            {
                return spec.SpecLookups
                           .Select(v => v.Value);
                           //.ToList();
            }
            return new List<string>();
        }
    }
}