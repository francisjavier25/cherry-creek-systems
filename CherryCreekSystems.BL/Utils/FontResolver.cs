﻿using PdfSharp.Fonts;
using System;
using System.IO;

namespace CherryCreekSystems.BL.Utils
{
    public class FontResolver : IFontResolver
    {
        public FontResolverInfo ResolveTypeface(string familyName, bool isBold, bool isItalic)
        {
            return new FontResolverInfo("Consolas#");
        }

        /// <summary>
        /// Return the font data for the fonts.
        /// </summary>
        public byte[] GetFont(string faceName)
        {
            var fontFileName = AppDomain.CurrentDomain.BaseDirectory + "Consolas.ttf";
            return File.ReadAllBytes(fontFileName);
        }
    }
}