﻿using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.SugarCRM;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CherryCreekSystems.BL
{
    public class SugarCRMClient
    {
        private const string QUOTES_MODULE = "AOS_Quotes";
        private const string QUOTE_LINE_ITEMS_MODULE = "AOS_Products_Quotes";
        private string _sessionId;
        private sugarsoap _client;

        public SugarCRMClient()
        {
            string UserName = "fjavier";
            string Password = "Mizaelj17";
            string URL = "http://52.161.19.164/service/v4/soap.php";

            //SugarCRM is a web reference added that points to http://{site_url}/service/v4/soap.php?wsdl
            _client = new sugarsoap();
            _client.Timeout = 900000;
            _client.Url = URL;

            //Create authentication object
            user_auth UserAuth = new user_auth();

            //Populate credentials
            UserAuth.user_name = UserName;
            UserAuth.password = getMD5(Password);

            //Try to authenticate
            name_value[] LoginList = new name_value[0];
            entry_value LoginResult = _client.login(UserAuth, "", LoginList);

            //get session id
            _sessionId = LoginResult.id;
        }

        public async Task ExportQuoteToSugarCRM(int quoteId)
        {
            await Task.FromResult(0);

            //CloudQueue queue = InitializeQuotesQueue();

            //var quoteService = new QuoteRequestService();
            //var quote = await quoteService.Get().ByQuoteId(quoteId);
            //var activeSystems = quote.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive();

            //foreach (var system in activeSystems)
            //{
            //    CloudQueueMessage message = new CloudQueueMessage(JsonConvert.SerializeObject(
            //        new QuoteMessage
            //        {
            //            QuoteId = quoteId,
            //            SystemId = system.Id_,
            //            QuoteNumber = quote.QuoteNumberWithRevision,
            //            SystemName = system.SystemName
            //        }
            //    ));
            //    await queue.AddMessageAsync(message);
            //}
        }

        private static CloudQueue InitializeQuotesQueue()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Utils.Configuration.StorageAccountConnectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue queue = queueClient.GetQueueReference("quotes");
            queue.CreateIfNotExists();
            return queue;
        }

        public async Task<bool> MigrateQuoteToSuiteCRM(int quoteId, string systemId)
        {
            var quoteService = new QuoteRequestService();
            var quote = await quoteService.Get().ByQuoteId(quoteId);
            await quoteService.FillChildObjects(customer: true, greenHouse: true, quoteRequests: quote);

            var system = quote.QuoteBoomSystemsData.QuoteBoomSystems.FirstOrDefaultActiveById(systemId);

            //Maybe send email because the queued system was not found or not active?
            if (system == null) return true; //The quote was inserted into queue when active, but at migration time it could be NOT active

            system.SugarCRMId = "";

            name_value[] fieldList = GetQuoteMainFields(quote, system);

            new_set_entry_result result = _client.set_entry(_sessionId, QUOTES_MODULE, fieldList);

            if (string.IsNullOrEmpty(system.SugarCRMId))
            {
                system.SugarCRMId = result.id;
                quoteService.SaveChanges();
            }

            #region grab existing line items from quote in SugarCRM

            var quoteLineItemReferences = _client.get_entry_list(_sessionId, QUOTE_LINE_ITEMS_MODULE, $"{QUOTE_LINE_ITEMS_MODULE}.parent_id='{system.SugarCRMId}'", "", 0, null, null, 99999, deleted: 0, favorites: false);
            var quoteLineItemIds = quoteLineItemReferences.entry_list.Select(r => r.id).ToArray();
            var quoteLineItems = _client.get_entries(_sessionId, QUOTE_LINE_ITEMS_MODULE, quoteLineItemIds, null, null, false);

            #endregion grab existing line items from quote in SugarCRM

            var line = 1;
            bool isSugarRecordModified;
            var orderedSystem = system.Ordered();
            var majorComponents = orderedSystem.QuoteCategories.SelectMany(q => q.QuoteMajorComponents);

            foreach (var ma in majorComponents)
            {
                var maIsVisible = ma.IsActive && ma.IsVisible;

                isSugarRecordModified = CheckIfSugarRecordModified(quoteLineItems, ma.SugarCRMId, ma.DescriptionCalculationResult);

                if (!isSugarRecordModified)
                {
                    name_value[] maLineItemFieldList =
                    PopulateLineItem(ma.SugarCRMId, system.SugarCRMId, ma.DescriptionCalculationResult, ma.AssignedPrice, maIsVisible, line, "Major Component", "", ma.Order);

                    var maResult = _client.set_entry(_sessionId, "AOS_Products_Quotes", maLineItemFieldList);
                    ma.SugarCRMId = maResult.id;
                    line++;
                }

                foreach (var mad in ma.QuoteMajorComponentDetails)
                {
                    isSugarRecordModified = CheckIfSugarRecordModified(quoteLineItems, mad.SugarCRMId, mad.ComponentName);

                    if (!isSugarRecordModified)
                    {
                        name_value[] madLineItemFieldList =
               PopulateLineItem(mad.SugarCRMId, system.SugarCRMId, $"{mad.Name}", 0, mad.IsVisible && maIsVisible, line, "Major Component Detail", ma.SugarCRMId, mad.Order);

                        var madResult = _client.set_entry(_sessionId, "AOS_Products_Quotes", madLineItemFieldList);
                        mad.SugarCRMId = madResult.id;
                        line++;
                    }
                }

                foreach (var mi in ma.QuoteMinorComponents)
                {
                    var miIsVisible = mi.IsActive && mi.IsVisible;

                    isSugarRecordModified = CheckIfSugarRecordModified(quoteLineItems, mi.SugarCRMId, mi.ComponentName);

                    if (!isSugarRecordModified)
                    {
                        name_value[] miLineItemFieldList =
               PopulateLineItem(mi.SugarCRMId, system.SugarCRMId, $"{mi.DescriptionCalculationResult}", mi.AssignedPrice, miIsVisible && maIsVisible, line, "Minor Component", ma.SugarCRMId, mi.Order);

                        var miResult = _client.set_entry(_sessionId, "AOS_Products_Quotes", miLineItemFieldList);
                        mi.SugarCRMId = miResult.id;
                        line++;
                    }

                    foreach (var mid in mi.QuoteMinorComponentDetails)
                    {
                        isSugarRecordModified = CheckIfSugarRecordModified(quoteLineItems, mid.SugarCRMId, mid.ComponentName);

                        if (!isSugarRecordModified)
                        {
                            name_value[] midLineItemFieldList =
                   PopulateLineItem(mid.SugarCRMId, system.SugarCRMId, $"{mid.Name}", 0, mid.IsVisible && miIsVisible && maIsVisible, line, "Minor Component Detail", mi.SugarCRMId, mid.Order);

                            var midResult = _client.set_entry(_sessionId, "AOS_Products_Quotes", midLineItemFieldList);
                            mid.SugarCRMId = midResult.id;
                            line++;
                        }
                    }
                }

                quoteService.SaveChanges();
            }

            return result.id.IsNotEmpty(); //If id is not empty, quote was migrated successfully
        }

        //Actually checking if component modified
        private static bool CheckIfSugarRecordModified(get_entry_result_version2 quoteLineItems, string sugarCRMId, string componentText)
        {
            //return false;
            var sugarMA = quoteLineItems.entry_list.FirstOrDefault(l => l.id.Equals(sugarCRMId, StringComparison.OrdinalIgnoreCase));
            if (sugarMA != null)
            {
                var field = sugarMA.name_value_list.FirstOrDefault(n => n.name == "name");
                if (field != null)
                {
                    return !field.value.Equals(componentText, StringComparison.OrdinalIgnoreCase);
                }
            }

            return false;
        }

        private name_value[] PopulateLineItem(string sugarCRMId, string systemSugarCRMId, string itemText, double price, bool isActive, int row, string componentType, string parentId, int order)
        {
            name_value[] lineItemFieldList = new name_value[20];
            lineItemFieldList[0] = new name_value();
            lineItemFieldList[0].name = "name";
            lineItemFieldList[0].value = itemText;

            sugarCRMId = sugarCRMId?.Trim();
            //if (!string.IsNullOrEmpty(sugarCRMId))
            //{
            //lineItemFieldList[1] = new name_value();
            //lineItemFieldList[1].name = "id";
            //lineItemFieldList[1].value = sugarCRMId;//"bdbb6c10-b930-79e5-cc60-59992caf5c0c";
            //}

            lineItemFieldList[2] = new name_value();
            lineItemFieldList[2].name = "parent_id";
            lineItemFieldList[2].value = systemSugarCRMId;

            lineItemFieldList[3] = new name_value();
            lineItemFieldList[3].name = "deleted";
            lineItemFieldList[3].value = isActive ? "0" : "1";

            //Price fields
            lineItemFieldList[4] = new name_value();
            lineItemFieldList[4].name = "product_list_price";
            lineItemFieldList[4].value = price.ToString("C2");
            lineItemFieldList[5] = new name_value();
            lineItemFieldList[5].name = "product_unit_price";
            lineItemFieldList[5].value = price.ToString("C2");
            lineItemFieldList[6] = new name_value();
            lineItemFieldList[6].name = "product_total_price";
            lineItemFieldList[6].value = price.ToString("C2");

            //Constant values
            lineItemFieldList[7] = new name_value();
            lineItemFieldList[7].name = "vat_amt";
            lineItemFieldList[7].value = "0";
            lineItemFieldList[8] = new name_value();
            lineItemFieldList[8].name = "parent_type";
            lineItemFieldList[8].value = "AOS_Quotes";

            lineItemFieldList[9] = new name_value();
            lineItemFieldList[9].name = "number";
            lineItemFieldList[9].value = row.ToString();
            //

            lineItemFieldList[10] = new name_value();
            lineItemFieldList[10].name = "component_type_c";
            lineItemFieldList[10].value = componentType;

            lineItemFieldList[11] = new name_value();
            lineItemFieldList[11].name = "component_parent_id_c";
            lineItemFieldList[11].value = parentId;

            lineItemFieldList[12] = new name_value();
            lineItemFieldList[12].name = "component_order_c";
            lineItemFieldList[12].value = order.ToString();

            return lineItemFieldList;
        }

        public async Task OneTimeMigrateQuotes()
        {
            var quoteService = new QuoteRequestService();
            var quotes = await quoteService.Get().WhereActive().ToListAsync();

            foreach (var cq in quotes)
            {
                await ExportQuoteToSugarCRM(cq.Id);
            }
        }

        private static name_value[] GetQuoteMainFields(QuoteRequest quote, QuoteBoomSystem system)
        {
            NameValueCollection fieldListCollection = new NameValueCollection();
            //to update a record, you will need to pass in a record id
            if (!string.IsNullOrEmpty(system.SugarCRMId))
                fieldListCollection.Add("id", system.SugarCRMId);

            fieldListCollection.Add("name", quote.QuoteNumberWithRevision);
            fieldListCollection.Add("billing_account_id", quote.Customer?.SugarCRMId ?? string.Empty);
            fieldListCollection.Add("total_amount", system.GetTotalCost().ToString("C2"));
            fieldListCollection.Add("email1", quote.Email);
            fieldListCollection.Add("system_name_c", system.SystemName);
            fieldListCollection.Add("quantity_c", system.Boom.QuantityOfSystems.ToString());
            fieldListCollection.Add("boom_type_c", ((BoomType)system.Boom.BoomTypeId).GetDescription());

            //Common boom fields
            fieldListCollection.Add("number_of_bays_c", system.Boom.NumberOfBays.ToString());
            fieldListCollection.Add("bay_length_c", system.Boom.BayLengthInFeet.ToString());
            fieldListCollection.Add("bay_width_c", system.Boom.BayWidthInFeet.ToString());
            fieldListCollection.Add("hose_size_c", system.Boom.HoseSize.ToString());
            fieldListCollection.Add("growing_surface_height_c", system.Boom.GrowingSurfaceHeightInInches.ToString());
            fieldListCollection.Add("post_spacing_c", system.Boom.PostSpacingInFeet.ToString());
            fieldListCollection.Add("post_dimensions_c", system.Boom.PostDimensionsInInchesSquared);
            fieldListCollection.Add("order_date_c", quote.OrderDate.ToString("MM/dd/yyyy"));
            fieldListCollection.Add("due_date_c", quote.DueDate.ToString("MM/dd/yyyy"));
            fieldListCollection.Add("spraybar_size_c", system.Boom.SprayBarSize.ToString());
            fieldListCollection.Add("number_of_bay_sidewalls_c", system.Boom.NumberOfBaySideWalls.ToString());
            fieldListCollection.Add("bottom_cord_type_c", system.Boom.BottomCordType);
            fieldListCollection.Add("bottom_cord_size_c", system.Boom.BottomCordSize);
            fieldListCollection.Add("bottom_cord_height_c", system.Boom.BottomCordHeightInFeet.ToString());
            fieldListCollection.Add("location_c", system.Boom.Location);
            fieldListCollection.Add("truss_spacing_c", system.Boom.TrussSpacingInFeet.ToString());
            fieldListCollection.Add("walkway_width_c", system.Boom.WalkwayWidthInInches.ToString());
            fieldListCollection.Add("clearance_width_c", system.Boom.ClearanceWidthInInches.ToString());
            fieldListCollection.Add("additional_information_c", system.Boom.AdditionalInformation);
            fieldListCollection.Add("customer_notes_c", system.Boom.CustomerNote);

            //Echo fields
            fieldListCollection.Add("drum_size_c", system.Boom.DrumSize.ToString());
            fieldListCollection.Add("basket_spacing_c", system.Boom.BasketSpacing.ToString());
            fieldListCollection.Add("controller_type_c", system.Boom.ControllerType);
            fieldListCollection.Add("number_of_layers_c", system.Boom.NumberOfLayers.ToString());
            fieldListCollection.Add("center_bay_water_station_c", system.Boom.CenterBayWaterStationRequested.ToStringAnswer());
            //3/4"
            fieldListCollection.Add("th_f_amiad_filder_assembly_c", system.Boom.AMIADFilterAssemblyRequested.ToStringAnswer());
            fieldListCollection.Add("lockline_c", system.Boom.LocklineRequested.ToStringAnswer());
            fieldListCollection.Add("extension_hanger_length_c", system.Boom.ExtensionHangerLengthInInches.ToString());
            fieldListCollection.Add("pulley_bracket_spacing_c", system.Boom.PulleyBracketSpacingInFeet.ToString());
            fieldListCollection.Add("remote_pull_chain_switch_c", system.Boom.RemotePullChainSwitchRequested.ToStringAnswer());
            fieldListCollection.Add("dramm_redhead_breaker_upgrad_c", system.Boom.DRAMMRedheadBreakerUpgradeRequested.ToStringAnswer());
            fieldListCollection.Add("water_solenoid_control_c", system.Boom.WaterSolenoidControlRequested.ToStringAnswer());
            fieldListCollection.Add("ev_third_party_control_c", system.Boom.EvVpd.ToStringAnswer());
            fieldListCollection.Add("feed_c", system.Boom.Feed);

            //Single rail and the rest
            fieldListCollection.Add("single_dual_water_c", system.Boom.SingleOrDualWater);
            fieldListCollection.Add("number_of_rows_c", system.Boom.NumberOfRows.ToString());
            fieldListCollection.Add("spraybody_spacing_c", system.Boom.SprayBodySpacingInInches.ToString());
            fieldListCollection.Add("hose_loop_height_c", system.Boom.HoseLoopHeightInInches.ToString());
            fieldListCollection.Add("step_lockline_upgrade_c", system.Boom.TripleStepLocklineUpgradeRequested.ToStringAnswer());
            fieldListCollection.Add("pressure_regulator_c", system.Boom.PressureRegulatorRequested.ToStringAnswer());
            fieldListCollection.Add("auto_injector_c", system.Boom.AutoInjectorRequested.ToStringAnswer());
            fieldListCollection.Add("injector_type_c", system.Boom.InjectorType);
            fieldListCollection.Add("sweep_ninety_degree_c", system.Boom.Sweep90Requested.ToStringAnswer());
            fieldListCollection.Add("triple_turret_body_c", system.Boom.TripleTurretBodyRequested.ToStringAnswer());
            fieldListCollection.Add("tip_one_c", system.Boom.Tip1);
            fieldListCollection.Add("tip_two_c", system.Boom.Tip2);
            fieldListCollection.Add("tip_three_c", system.Boom.Tip3);
            //1"
            fieldListCollection.Add("one_inch_amiad_assy_c", system.Boom.AMIADFilterASSYRequested.ToStringAnswer());
            fieldListCollection.Add("rail_dropdown_assy_c", system.Boom.RailDropDownASSYInInches.ToString());
            fieldListCollection.Add("one_fourth_hose_upgrade_c", system.Boom.HoseUpgradeRequested.ToStringAnswer());
            fieldListCollection.Add("wireless_walk_switch_c", system.Boom.WirelessWalkSwitchRequested.ToStringAnswer());
            fieldListCollection.Add("center_water_feed_system_c", system.Boom.CenterWaterFeedSystemRequested.ToStringAnswer());
            fieldListCollection.Add("diamond_sweep_c", system.Boom.DiamondSweepRequested.ToStringAnswer());
            fieldListCollection.Add("carry_steel_c", system.Boom.CarrySteel);
            fieldListCollection.Add("dual_motor_no_axle_c", system.Boom.DualMotorNoAxleRequested.ToStringAnswer());
            fieldListCollection.Add("twenty_five_gallon_tank_c", system.Boom.Gallon25ReservoirTankRequested.ToStringAnswer());
            fieldListCollection.Add("conveyor_c", system.Boom.ConveyorRequested.ToStringAnswer());
            fieldListCollection.Add("mounted_injector_c", system.Boom.MountedInjectorRequested.ToStringAnswer());

            fieldListCollection.Add("freight_estimate_c", quote.FreightEstimate);
            fieldListCollection.Add("steel_estimate_c", quote.SteelEstimate);
            fieldListCollection.Add("installation_estimate_c", quote.InstallationEstimate);
            fieldListCollection.Add("ev_wire_gauge_c", system.Boom.EVWireGauge);

            fieldListCollection.Add("assigned_user_id", "33ceb6c8-2cf3-d0b7-e1a8-593096e8e2e0"); //Walter's id
                                                                                                 //fieldListCollection.Add("assigned_user_id", "d6aa03f7-5473-d3b7-3247-593199715113"); //My id

            //this is just a trick to avoid having to manually specify index values for name_value[]
            name_value[] fieldList = new name_value[fieldListCollection.Count];

            int count = 0;
            foreach (string name in fieldListCollection)
            {
                var values = fieldListCollection.GetValues(name);
                if (values != null && values.Any())
                {
                    var value = values.FirstOrDefault();
                    if (value == null)
                        continue;

                    name_value field = new name_value();
                    field.name = name; field.value = value;
                    fieldList[count] = field;
                    count++;
                }
            }

            return fieldList;
        }

        //public void MigrateAccounts()
        //{
        //    var customerService = new CustomerService();
        //    var customers = customerService.Get().Where(c => string.IsNullOrEmpty(c.SugarCRMId)).ToList();
        //    customerService.FillChildObjects(shipAddress: true, customers: customers.ToArray()).Wait();

        //    foreach (var c in customers)
        //    {
        //        NameValueCollection fieldListCollection = new NameValueCollection();
        //        //to update a record, you will need to pass in a record id as commented below
        //        if (!string.IsNullOrEmpty(c.SugarCRMId))
        //            fieldListCollection.Add("id", c.SugarCRMId);

        //        fieldListCollection.Add("name", c.CompanyName);
        //        fieldListCollection.Add("phone_fax", c.Fax ?? string.Empty);
        //        fieldListCollection.Add("phone_office", c.Phone ?? string.Empty);
        //        fieldListCollection.Add("email1", c.Email ?? string.Empty);

        //        if (c.ShipAddresses != null && c.ShipAddresses.Any())
        //        {
        //            var shipAddress = c.ShipAddresses.First();

        //            fieldListCollection.Add("shipping_address_street", shipAddress.Street1);
        //            fieldListCollection.Add("shipping_address_street_2", shipAddress.Street2 ?? "");
        //            fieldListCollection.Add("shipping_address_city", shipAddress.City);
        //            fieldListCollection.Add("shipping_address_state", shipAddress.State?.Name ?? "");
        //            fieldListCollection.Add("shippping_address_postalcode", shipAddress.Zip);
        //            fieldListCollection.Add("shipping_address_country", "USA");
        //        }

        //        fieldListCollection.Add("assigned_user_id", "33ceb6c8-2cf3-d0b7-e1a8-593096e8e2e0"); //Walter's id
        //        //fieldListCollection.Add("assigned_user_id", "d6aa03f7-5473-d3b7-3247-593199715113"); //My id

        //        //this is just a trick to avoid having to manually specify index values for name_value[]
        //        SugarCRM.name_value[] fieldList = new SugarCRM.name_value[fieldListCollection.Count];

        //        int count = 0;
        //        foreach (string name in fieldListCollection)
        //        {
        //            foreach (string value in fieldListCollection.GetValues(name))
        //            {
        //                SugarCRM.name_value field = new SugarCRM.name_value();
        //                field.name = name; field.value = value;
        //                fieldList[count] = field;
        //            }
        //            count++;
        //        }

        //        try
        //        {
        //            SugarCRM.new_set_entry_result result = _client.set_entry(_sessionId, "Accounts", fieldList);
        //            //SugarCRM.new_set_entry_result result = _client.set_entry(_sessionId, "AOS_Quotes", fieldList);

        //            string RecordID = result.id;
        //            c.SugarCRMId = RecordID;
        //            customerService.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //    }
        //}

        static private string getMD5(string TextString)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBuffer = Encoding.ASCII.GetBytes(TextString);
            byte[] outputBuffer = md5.ComputeHash(inputBuffer);

            StringBuilder Builder = new StringBuilder(outputBuffer.Length);
            for (int i = 0; i < outputBuffer.Length; i++)
            {
                Builder.Append(outputBuffer[i].ToString("X2"));
            }

            return Builder.ToString();
        }
    }

    public class QuoteMessage
    {
        public int QuoteId { get; set; }
        public string SystemId { get; set; }
        public string QuoteNumber { get; set; }
        public string SystemName { get; set; }
    }
}