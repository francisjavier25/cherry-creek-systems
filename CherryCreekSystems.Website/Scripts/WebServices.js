﻿var axios = require('axios');

class WebServices {

  static getQuoteViewModel(quoteId) {
    return axios({
      method: 'post',
      url: '/administrator/quote/GetQuoteViewModel',
      data: { quoteId: quoteId }
    })
      .then((response) => {
        return response.data;
      })
      .catch(function (error) {
        //vm.answer = 'Error! Could not reach the API. ' + error
      });
  }

  static getCustomer(customerId) {
    return axios({
      method: 'post',
      url: '/administrator/customer/GetCustomerWithAddresses/SelectedCustomerId',
      data:
      {
        customerId: customerId
      }
    })
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        //vm.answer = 'Error! Could not reach the API. ' + error
      });
  }
}

module.exports = WebServices;