﻿var VeeValidate = require('vee-validate');
var Vue = require('vue');
var VueMask = require('v-mask');
var _ = require('underscore');
var WebServices = require('./WebServices');

var loadingMessage = "Loading System...";

var dict = {
  en: {
    messages: {
      required: 'Required',
      between: 'Enter 1 or more',
      decimal: 'Enter a valid decimal',
      numeric: 'Enter a valid integer',
      date_format: 'Enter a valid date',
      date_between: 'Enter a valid date'
    }
    //custom: {
    //    LeadTime: {
    //        between: 'Enter a valid number'//
    //    }
    //}
  }
};

$(document).ready(function () {
  VeeValidate.Validator.localize(dict);
  Vue.use(VeeValidate);
  //Vue.use(VueNumeric.default);
  Vue.use(VueMask.VueMaskPlugin);

  var app = new Vue({
    el: '#app',
    data:
    {
      showLoading: false,

      //Register viewmodel fields
      vm: JSON.parse($('#QuoteViewModel').val()),
      validation:
      {
        //Vee-validate default validations
        required: { required: true },
        requiredInteger: { required: true, numeric: true, between: [1, 99999999999] },
        requiredDecimal: { required: true, decimal: 2 },
        optionalInteger: { numeric: true },
        optionalDecimal: { decimal: 2 },
        requiredDate: { required: true, date_format: 'MM/DD/YYYY', date_between: ['01/01/1970', '12/12/3000'] },
      }
    },
    methods: {
      //validateBeforeSubmit() {
      //    this.$validator.validateAll().then((result) => {
      //        if (result) {
      //            alert('From Submitted!');
      //            return;
      //        }

      //        alert('Correct them errors!');
      //    });
      //},
      submit: function (e) {
        e.preventDefault();

        this.$validator.validateAll().then((result) => {
          if (result) {
            axios({
              method: 'post',
              url: '/administrator/quote/save',
              data: $("#quoteForm").serialize()
            })
              .then(function (response) {
                app.vm.CalculationErrors = response.data.CalculationErrors;
              })
              .catch(function (error) {
                //vm.answer = 'Error! Could not reach the API. ' + error
              });
          }
        });
      }
    },
    //Initialize page with view model from server
    created: function () {

      this.showLoading = true;

      WebServices.getQuoteViewModel(1).then(function (data) {
        app.vm = data;
        app.showLoading = false;
      });
    },
    watch:
    {
      'vm.SelectedCustomerId': function (val) {
        if (val > 0) {
          WebServices.getCustomer(val).then(function (data) {
            app.vm.Customer = data;
          });
        }
      },
      showLoading: function (val) {
        if (val)
          waitingDialog.show(loadingMessage);
        else
          waitingDialog.hide();
      }
    },
    computed: {
      ExistingQuote: function () {
        return this.vm.QuoteId > 0;
      },
      FullAddress: function () {
        if (this.vm.SelectedShipAddressId) {
          var shipAddress = _.findWhere(this.vm.Customer.ShipAddresses, { Id: this.vm.SelectedShipAddressId });
          if (shipAddress && shipAddress.FullAddress) {
            return shipAddress.FullAddress;
          }
        }
        return "";
      },
      CustomerSelected: function () {
        return this.vm.SelectedCustomerId > 0;
      },
      CustomerHasShipAddresses: function () {
        var customer = this.vm.Customer;
        return customer && customer.ShipAddresses && customer.ShipAddresses.length;
      },
      NoBoomSelected: function () {
        return this.vm.SelectedBoomType == 0;
      },
      EchoField: function () {
        return this.vm.SelectedBoomType == 1;
      },
      CalculationErrorsAvailable: function () {
        return this.vm.CalculationErrors && this.vm.CalculationErrors.length > 0;
      },

      //Custom validation
      customValidation_PostSpacing: function () {
        return this.vm.SelectedBoomType == 6 ? this.validation.requiredDecimal : this.validation.optionalDecimal;
      }
    }
  });
});


var waitingDialog = waitingDialog || (function ($) {
  'use strict';

  // Creating modal dialog's DOM
  var $dialog = $(
    '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
    '<div class="modal-dialog modal-m">' +
    '<div class="modal-content">' +
    '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
    '<div class="modal-body">' +
    '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
    '</div>' +
    '</div></div></div>');

  return {
    show: function (message, options) {
      // Assigning defaults
      if (typeof options === 'undefined') {
        options = {};
      }
      if (typeof message === 'undefined') {
        message = 'Loading';
      }
      var settings = $.extend({
        dialogSize: 'm',
        progressType: '',
        onHide: null // This callback runs after the dialog was hidden
      }, options);

      // Configuring dialog
      $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
      $dialog.find('.progress-bar').attr('class', 'progress-bar');
      if (settings.progressType) {
        $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
      }
      $dialog.find('h3').text(message);
      // Adding callbacks
      if (typeof settings.onHide === 'function') {
        $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
          settings.onHide.call($dialog);
        });
      }
      // Opening dialog
      $dialog.modal();
    },
    /**
     * Closes dialog
     */
    hide: function () {
      $dialog.modal('hide');
    }
  };

})(jQuery);