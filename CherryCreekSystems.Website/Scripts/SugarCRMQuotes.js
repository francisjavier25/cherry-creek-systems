﻿
$(document).ready(function () {
  var url = window.location.href;

  var isDetailView = url.indexOf('DetailView') > -1;
  var isEditView = $('form[name="EditView"]');

  //Detail View Code
  if (isDetailView) {
    console.log('Detail View Customizations Activated');

    var detailViewRowItwm = $('.detail-view-row-item');
    var twentyFileGallonTankField = $('div[field="twenty_five_gallon_tank_c"]');
    var conveyorField = $('div[field="conveyor_c"]');

    detailViewRowItwm.find('.detail-view-field.col-sm-8')
      .removeClass('col-sm-8')
      .addClass('col-sm-5')
      .addClass('col-sm-push-3');

    detailViewRowItwm.find('.detail-view-field.col-sm-10')
      .removeClass('col-sm-10')
      .addClass('col-sm-8')
      .addClass('col-sm-push-2')
      .css('margin-left', '-50px')
      .css('width', '825px');

    //REFACTOR
    twentyFileGallonTankField.prev()
      .addClass('col-sm-push-12');

    twentyFileGallonTankField.removeClass('col-sm-push-3')
      .addClass('col-sm-push-12')
      .css('margin-left', '190px')

    conveyorField.prev()
      .addClass('col-sm-push-12');

    conveyorField.removeClass('col-sm-push-3')
      .addClass('col-sm-push-12')
      .css('margin-left', '190px')
  }
  //Edit View Code
  else if (isEditView.length) {
    console.log('Edit View Customizations Activated');
  }
});