﻿$(document).ready(function () {
    $(".AjaxSubmitForm").change(function () {
        var selectElement = $(this);
        var form = selectElement.closest("form");
        $(form).submit();
    });
});

function toggleLoading() {
    var overlayFrame = $("#pnlOverlayFrame");

    if (overlayFrame)
        overlayFrame.toggle();
}