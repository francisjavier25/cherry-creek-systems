﻿using CherryCreekSystems.Domain;
using PagedList;

namespace CherryCreekSystems.Website.Models
{
    public class SalesRepsViewModel
    {
        public SalesRepsViewModel()
        {
            Page = 1;
        }

        public int Page { get; set; }

        public string Search { get; set; }

        public IPagedList<SalesRepUser> SalesReps { get; set; }
    }
}