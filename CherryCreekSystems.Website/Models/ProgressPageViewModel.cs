﻿using CherryCreekSystems.Domain;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CherryCreekSystems.Website.Models
{
    public class ProgressPageViewModel
    {
        public string Search { get; set; }
        public int? QuoteStatusId { get; set; }
        public IEnumerable<QuoteStatus> QuoteStatuses { get; set; }
        public IEnumerable<ProgressViewModel> ProgressModels { get; set; }
    }
}