﻿using CherryCreekSystems.Domain;
using PagedList;

namespace CherryCreekSystems.Website.Models
{
    public class CustomersViewModel
    {
        public CustomersViewModel()
        {
            Page = 1;
        }

        public int Page { get; set; }

        public string Search { get; set; }

        public IPagedList<CustomerUser> Customers { get; set; }
    }
}