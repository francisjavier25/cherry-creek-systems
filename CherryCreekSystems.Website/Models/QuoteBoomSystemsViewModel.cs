﻿using System;
using CherryCreekSystems.Domain.BoomComponents;
using System.Collections.Generic;

namespace CherryCreekSystems.Website.Models
{
    public class QuoteBoomSystemsViewModel
    {
        #region previous page link
        public int Page { get; set; }
        public int? DistributorId { get; set; }
        public int? CustomerId { get; set; }
        public int? SalesRepId { get; set; }
        public int? QuoteStatusId { get; set; }
        public int? QuoteId { get; set; }
        public int? BoomTypeId { get; set; }
        #endregion

        public string QuoteNumber { get; set; }
        public string ShippingDate { get; set; }

        public IEnumerable<QuoteBoomSystem> QuoteBoomSystems { get; set; }
    }
}