﻿using CherryCreekSystems.Domain;
using PagedList;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CherryCreekSystems.Website.Models
{
    public class QuotesViewModel
    {
        public QuotesViewModel()
        {
            Page = 1;
            QuoteStatusId = 1;
        }

        public int? QuoteId { get; set; }
        public int Page { get; set; }

        public int? DistributorId { get; set; }
        public IList<SelectListItem> Distributors { get; set; }

        public int? CustomerId { get; set; }
        public IList<SelectListItem> Customers { get; set; }

        public int? SalesRepId { get; set; }
        public IList<SelectListItem> SalesReps { get; set; }

        public int? QuoteStatusId { get; set; }
        public IList<SelectListItem> QuoteStatuses { get; set; }

        public int? BoomTypeId { get; set; }
        public IList<SelectListItem> BoomTypes { get; set; }

        public IPagedList<QuoteViewModel> QuoteRequests { get; set; }
    }

    public class QuoteViewModel
    {
        public QuoteRequest QuoteRequest { get; set; }

        public IEnumerable<string> BoomTypes { get; set; }
    }
}