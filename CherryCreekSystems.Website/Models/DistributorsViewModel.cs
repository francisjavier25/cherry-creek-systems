﻿using CherryCreekSystems.Domain;
using PagedList;

namespace CherryCreekSystems.Website.Models
{
    public class DistributorsViewModel
    {
        public DistributorsViewModel()
        {
            Page = 1;
        }

        public int Page { get; set; }

        public string Search { get; set; }

        public IPagedList<DistributorUser> Distributors { get; set; }
    }
}