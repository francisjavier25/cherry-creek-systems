﻿using System;
using System.Collections.Generic;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Website.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.Domain;
using Newtonsoft.Json;
using PagedList;

namespace CherryCreekSystems.Website.Areas.Customers.Controllers
{
    [Authorize(Roles = "customer")]
    public class QuotesController : Website.Controllers.QuotesController
    {
        public async Task<ActionResult> Index(QuotesViewModel viewModel)
        {
            var quoteStatusService = new QuoteStatusService();
            var getQuoteStatusesTask = quoteStatusService.Get().OrderedByName().ToListAsync();

            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            viewModel.CustomerId = (await user.CustomerIds()).First();

            if (viewModel.QuoteId.HasValue)
            {
                QuoteRequestService quoteRequestService = new QuoteRequestService();
                var singleQuote = await quoteRequestService.Get().ByQuoteId(viewModel.QuoteId.Value);
                var singleQuoteViewModel = new QuoteViewModel
                {
                    QuoteRequest = singleQuote,
                    BoomTypes = singleQuote.GetBoomTypes()
                };
                var allowedToSeeInfo = await SecurityCheck(user, singleQuote);
                if (!allowedToSeeInfo)
                {
                    return HttpNotFound();
                }
                viewModel.QuoteRequests = new PagedList<QuoteViewModel>(new List<QuoteViewModel> { singleQuoteViewModel }, 1, 1);
            }
            else
            {
                viewModel.QuoteRequests = await GetQuoteRequestsPage(
                        viewModel.Page,
                        customerId: viewModel.CustomerId,
                        quoteStatusId: viewModel.QuoteStatusId,
                        boomTypeId: viewModel.BoomTypeId);
            }

            var boomTypeIds = BoomTypeStructure.GetList();
            var quoteStatuses = await getQuoteStatusesTask;

            viewModel.QuoteStatuses = quoteStatuses.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.Name
            }).ToList();

            viewModel.QuoteStatuses.Insert(0, new SelectListItem { Value = "", Text = "All" });

            viewModel.BoomTypes = boomTypeIds.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.Name
            }).ToList();

            viewModel.BoomTypes.Insert(0, new SelectListItem { Value = "", Text = "All" });

            return View(viewModel);
        }
    }
}