﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CherryCreekSystems.Website.Areas.Administrator.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Administrator/ShipAddress
        public async Task<ContentResult> GetCustomerWithAddresses(int customerId)
        {
            var customerService = new CustomerService();
            var customer = await customerService.Get().ById(customerId);
            await customerService.FillChildObjects(shipAddress: true, customers: new[] { customer });

            var customerViewModel = new CustomerViewModel
            {
                Id = customer.Id,
                ContactName = customer.ContactName,
                Email = customer.Email,
                Fax = customer.Fax,
                Phone = customer.Phone,
                ShipAddresses = customer?.ShipAddresses?.Select(s => new ShipAddressViewModel
                {
                    Id = s.Id,
                    FullAddress = s.FullAddress
                })
            };

            var customerJson = JsonConvert.SerializeObject(customerViewModel);
            return Content(customerJson);
        }
    }
}