﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CherryCreekSystems.Website.Areas.Administrator.Controllers
{
    public class QuoteController : Controller
    {
        //FOR WEB SERVICE TO SUITE CRM
        public async Task<JsonResult> Quote()
        {
            var quote = await (new QuoteRequestService().Get().ByQuoteId(1));

            var quoteViewModel = new QuoteViewModel()
            {
                Systems = quote.QuoteBoomSystemsData.QuoteBoomSystems,
                SelectedBoomType = 1,
                QuoteId = 0,
                QuoteNumber = "111111111",
                QuoteTotal = 100.55,
                SelectedQuoteStatusId = 1,
                SelectedCustomerId = 1,
                SelectedDistributorId = 5,
                SelectedSalesRepId = 4,
                ShipAddress = "a test address",
                LeadTime = 7,
                PurchaseOrder = "an order",
                Discount = "1,333",
                PriceLevel = 22.7,
                SelectedGreenhouseTypeId = 5,
                NumberOfBays = 6,
                BayLength = 200,
                BayWidth = 100,
                HoseSize = 12.34,
                GrowingSurfaceHeight = 33.55,
                PostSpacing = 77,
                PostDimensions = "300 X 400",
                FreightEstimate = "111",
                InstallationEstimate = "222",
                SteelEstimate = "333",
                OrderDate = DateTime.Now.ToString("MM/dd/yyyy"),
                DueDate = DateTime.Now.AddDays(30).ToString("MM/dd/yyyy"),
                SprayBarSize = 88,
                NumberOfBaySidewalls = 5,
                BottomCordType = "RD",
                BottomCordHeight = 5.5,
                BottomCordSize = "7 X 8",
                Location = "my house",
                AdditionalInformation = "some additional info",
                CustomerNotes = "another notes",
                TrussSpacing = 1.2,
                WalkwayWidth = 3.4,
                ClearanceWidth = 4.5,

                QuantityOfSystems = 4,
                SelectedDrumSize = 20,
                BasketSpacing = 1.2,
                SelectedControllerType = "Standard Enhanced",
                SelectedNumberOfLayers = 3,
                CenterBayWaterStation = false,
                EvVpdThirdPartyControl = false,
                ThreeFourthAMIADFilterAssembly = false,
                LockLine = false,
                ExtensionHangerLength = 5.5,
                SelectedPulleyBracketSpacing = 5,
                RemotePullChainSwitch = false,
                DRAMMRedheadBreakerUpgrade = false,
                WaterSolenoidControl = false,
                SelectedFeed = "Center Feed",
                Customer = new CustomerViewModel { CompanyName = "Test Company", ContactName = "Francisco Ramos" }
            };

            return Json(quoteViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            var quoteCreationViewModel = new QuoteCreationViewModel();
            return View(quoteCreationViewModel);
        }

        public async Task<ContentResult> Save(QuoteCreationViewModel viewModel)
        {
            var quoteRequest = new QuoteRequest
            {
                ContactName = viewModel.Customer.ContactName,
                Phone = viewModel.Customer.Phone,
                Fax = viewModel.Customer.Fax,
                Email = viewModel.Customer.Email,
                FullAddress = viewModel.FullAddress,
                CustomerId = viewModel.SelectedCustomerId,
                ShipAddressId = viewModel.SelectedShipAddressId,
                SalesRepId = viewModel.SelectedSalesRepId,
                DistributorId = viewModel.SelectedDistributorId,
                LeadTime = viewModel.LeadTime.ToSafeString(),
                SteelEstimate = viewModel.SteelEstimate,
                FreightEstimate = viewModel.FreightEstimate,
                InstallationEstimate = viewModel.InstallationEstimate,
                Discount = viewModel.Discount,
                PriceLevel = viewModel.PriceLevel.ToSafeDouble(),
                PurchaseOrder = viewModel.PurchaseOrder,
                OrderDate = viewModel.OrderDate.ToSafeDate(),
                DueDate = viewModel.DueDate.ToSafeDate(),
                Active = true
            };

            //Check if status changed
            if (viewModel.SelectedQuoteStatusId != quoteRequest.QuoteStatusId)
            {
                quoteRequest.StatusDate = DateTime.Now;
            }

            var containsRevisionNumber = viewModel.QuoteNumber.ToSafeString().EndsWith("-" + quoteRequest.Revision);
            var quoteNumber = viewModel.QuoteNumber;
            if (containsRevisionNumber)
            {
                var dashIndex = quoteNumber.LastIndexOf("-");
                quoteNumber = quoteNumber.Substring(0, dashIndex);
            }

            //Update to new status
            quoteRequest.QuoteStatusId = viewModel.SelectedQuoteStatusId;
            quoteRequest.Revision++;

            quoteRequest.QuoteNumber = viewModel.QuoteNumber;

            var boom = new Boom
            {
                //Greenhouse values
                AdditionalInformation = viewModel.AdditionalInformation,
                CustomerNote = viewModel.CustomerNotes,
                GreenHouseTypeId = viewModel.SelectedGreenhouseTypeId,
                NumberOfBays = viewModel.NumberOfBays.ToSafeInt(),
                BayLengthInFeet = viewModel.BayLength.ToSafeDouble(),
                BayWidthInFeet = viewModel.BayWidth.ToSafeDouble(),
                NumberOfBaySideWalls = viewModel.NumberOfBaySidewalls.ToSafeInt(),
                WalkwayWidthInInches = viewModel.WalkwayWidth.ToSafeDouble(),
                ClearanceWidthInInches = viewModel.ClearanceWidth.ToSafeDouble(),
                BottomCordHeightInFeet = viewModel.BottomCordHeight.ToSafeDouble(),
                TrussSpacingInFeet = viewModel.TrussSpacing.ToSafeDouble(),
                BottomCordType = viewModel.BottomCordType,
                BottomCordSize = viewModel.BottomCordSize,
                GrowingSurfaceHeightInInches = viewModel.GrowingSurfaceHeight.ToSafeDouble(),
                PostSpacingInFeet = viewModel.PostSpacing.ToSafeDouble(),
                PostDimensionsInInchesSquared = viewModel.PostDimensions,
                SprayBarSize = viewModel.SprayBarSize.ToSafeDouble(),
                HoseSize = viewModel.HoseSize.ToSafeDouble(),
                Location = viewModel.Location
            };

            //Echo System values
            boom.BoomTypeId = viewModel.SelectedBoomType;
            boom.QuantityOfSystems = viewModel.QuantityOfSystems.ToSafeInt();
            boom.DrumSize = viewModel.SelectedDrumSize.ToSafeInt();
            boom.BasketSpacing = viewModel.BasketSpacing.ToSafeDouble();
            boom.ControllerType = viewModel.SelectedControllerType;
            boom.NumberOfLayers = viewModel.SelectedNumberOfLayers.ToSafeInt();
            boom.Feed = viewModel.SelectedFeed;

            boom.EvVpd = viewModel.EvVpdThirdPartyControl;
            boom.CenterBayWaterStationRequested = viewModel.CenterBayWaterStation;
            boom.AMIADFilterAssemblyRequested = viewModel.ThreeFourthAMIADFilterAssembly;
            boom.LocklineRequested = viewModel.LockLine;

            boom.ExtensionHangerLengthInInches = viewModel.ExtensionHangerLength.ToSafeDouble();
            boom.PulleyBracketSpacingInFeet = viewModel.SelectedPulleyBracketSpacing.ToSafeDouble();
            boom.RemotePullChainSwitchRequested = viewModel.RemotePullChainSwitch;
            boom.DRAMMRedheadBreakerUpgradeRequested = viewModel.DRAMMRedheadBreakerUpgrade;
            boom.WaterSolenoidControlRequested = viewModel.WaterSolenoidControl;

            var context = new DataContext();
            var quoteRequestService = new QuoteRequestService(context);
            var quoteBoomSystemService = new QuoteBoomSystemService(context);

            quoteRequestService.Add(quoteRequest);
            await context.SaveChangesAsync();

            var quoteSystemResult = await quoteBoomSystemService.CreateQuoteBoomSystem(quoteRequest, boom);
            quoteRequest.QuoteNumber = await quoteRequestService.CreateQuoteNumber(quoteRequest);
            await context.SaveChangesAsync();

            var responseViewModel = new QuoteCreationResponseViewModel();
            responseViewModel.QuoteNumber = quoteRequest.QuoteNumber;
            if (quoteSystemResult.CalculationErrors.Any())
            {
                responseViewModel.CalculationErrors = quoteSystemResult.CalculationErrors;
            }

            var jsonResponse = JsonConvert.SerializeObject(responseViewModel);
            return Content(jsonResponse);
        }

        public async Task<ContentResult> GetQuoteViewModel(int? quoteId)
        {
            var specsService = new SpecService();
            var statusService = new QuoteStatusService();
            var distributorsService = new DistributorService();
            var customersService = new CustomerService();
            var salesRepService = new SalesRepService();
            var greenhouseService = new GreenhouseTypeService();

            var specsQuery = specsService.Get().WithSpecLookups().ToListAsync();
            var statusesQuery = statusService.Get().OrderedById().ToListAsync();
            var distributorsQuery = distributorsService.Get().OrderedByName().ToListAsync();
            var salesRepsQuery = salesRepService.Get().OrderedByName().ToListAsync();
            var greenhouseQuery = greenhouseService.Get().OrderedByName().ToListAsync();
            var customersQuery = customersService.Get()
                    .Where(c => c.CompanyName.Contains("test")).OrderedByCompanyName().Take(100).ToListAsync();

            var quoteViewModel = new QuoteCreationViewModel
            {
                SelectedBoomType = 0,
                QuoteId = 0,
                QuoteNumber = "111111111",
                QuoteTotal = 100.55,
                SelectedQuoteStatusId = 1,
                SelectedCustomerId = 1,
                SelectedDistributorId = 5,
                SelectedSalesRepId = 4,
                ShipAddress = "a test address",
                LeadTime = 7,
                PurchaseOrder = "an order",
                Discount = "1,333",
                PriceLevel = 22.7,
                SelectedGreenhouseTypeId = 5,
                NumberOfBays = 6,
                BayLength = 200,
                BayWidth = 100,
                HoseSize = 12.34,
                GrowingSurfaceHeight = 33.55,
                PostSpacing = 77,
                PostDimensions = "300 X 400",
                FreightEstimate = "111",
                InstallationEstimate = "222",
                SteelEstimate = "333",
                OrderDate = DateTime.Now.ToString("MM/dd/yyyy"),
                DueDate = DateTime.Now.AddDays(30).ToString("MM/dd/yyyy"),
                SprayBarSize = 88,
                NumberOfBaySidewalls = 5,
                BottomCordType = "RD",
                BottomCordHeight = 5.5,
                BottomCordSize = "7 X 8",
                Location = "my house",
                AdditionalInformation = "some additional info",
                CustomerNotes = "another notes",
                TrussSpacing = 1.2,
                WalkwayWidth = 3.4,
                ClearanceWidth = 4.5,

                QuantityOfSystems = 4,
                SelectedDrumSize = 20,
                BasketSpacing = 1.2,
                SelectedControllerType = "Standard Enhanced",
                SelectedNumberOfLayers = 3,
                CenterBayWaterStation = false,
                EvVpdThirdPartyControl = false,
                ThreeFourthAMIADFilterAssembly = false,
                LockLine = false,
                ExtensionHangerLength = 5.5,
                SelectedPulleyBracketSpacing = 5,
                RemotePullChainSwitch = false,
                DRAMMRedheadBreakerUpgrade = false,
                WaterSolenoidControl = false,
                SelectedFeed = "Center Feed"
            };

            quoteViewModel.BoomTypes = BoomTypeStructure.GetList().ToList();
            quoteViewModel.Statuses = (await statusesQuery).ToViewModel<QuoteStatusViewModel>().ToList();
            quoteViewModel.Distributors = (await distributorsQuery).ToViewModel<DistributorViewModel>().ToList();
            quoteViewModel.Customers = (await customersQuery).ToViewModel().ToList();
            quoteViewModel.SalesReps = (await salesRepsQuery).ToViewModel<SalesRepViewModel>().ToList();
            quoteViewModel.GreenhouseTypes = (await greenhouseQuery).ToViewModel<GreenhouseViewModel>().ToList();

            var specs = await specsQuery;
            var specsContainer = specs.GetSpecContainer();

            quoteViewModel.DrumSizes = specsContainer.DrumSize.ToList();
            quoteViewModel.EchoSelectedControllerTypes = specsContainer.EchoControllerType.ToList();
            quoteViewModel.NumberOfLayers = specsContainer.NumberOfLayers.ToList();
            quoteViewModel.PulleyBracketSpacings = specsContainer.PulleyBracketSpacing.ToList();
            quoteViewModel.Feeds = specsContainer.Feed.ToList();

            quoteViewModel.CalculationErrors = new List<string>();

            var quoteViewModelJson = JsonConvert.SerializeObject(quoteViewModel);

            return Content(quoteViewModelJson);
        }
    }

    public class QuoteCreationResponseViewModel
    {
        public string QuoteNumber { get; set; }
        public List<string> CalculationErrors { get; set; }
    }

    public class QuoteCreationViewModel
    {
        public QuoteCreationViewModel()
        {
            Customer = new CustomerViewModel { ShipAddresses = new List<ShipAddressViewModel>() };
            Statuses = new List<QuoteStatusViewModel>();
            Distributors = new List<DistributorViewModel>();
            Customers = new List<CustomerViewModel>();
            SalesReps = new List<SalesRepViewModel>();
            GreenhouseTypes = new List<GreenhouseViewModel>();
            DrumSizes = new List<string>();
            EchoSelectedControllerTypes = new List<string>();
            CalculationErrors = new List<string>();
        }

        public int QuoteId { get; set; }
        public int SelectedQuoteStatusId { get; set; }
        public int SelectedCustomerId { get; set; }
        public int SelectedDistributorId { get; set; }
        public int SelectedBoomType { get; set; }
        public int SelectedSalesRepId { get; set; }
        public int SelectedGreenhouseTypeId { get; set; }
        public int? SelectedShipAddressId { get; set; }
        public string SelectedControllerType { get; set; }
        public int? SelectedNumberOfLayers { get; set; }
        public double? SelectedPulleyBracketSpacing { get; set; }
        public string SelectedFeed { get; set; }

        public List<BoomTypeStructure> BoomTypes { get; set; }
        public List<QuoteStatusViewModel> Statuses { get; set; }
        public List<DistributorViewModel> Distributors { get; set; }
        public List<CustomerViewModel> Customers { get; set; }
        public List<SalesRepViewModel> SalesReps { get; set; }
        public List<GreenhouseViewModel> GreenhouseTypes { get; set; }
        public List<string> DrumSizes { get; set; }
        public List<string> EchoSelectedControllerTypes { get; set; }
        public List<string> NumberOfLayers { get; set; }
        public List<string> PulleyBracketSpacings { get; set; }
        public List<string> Feeds { get; set; }

        public string QuoteNumber { get; set; }
        public double QuoteTotal { get; set; }
        public string ShipAddress { get; set; }
        public int? LeadTime { get; set; }
        public string PurchaseOrder { get; set; }
        public string Discount { get; set; }
        public double? PriceLevel { get; set; }

        public int? NumberOfBays { get; set; }
        public double? BayLength { get; set; }
        public double? BayWidth { get; set; }
        public double? HoseSize { get; set; }
        public double? GrowingSurfaceHeight { get; set; }
        public double? PostSpacing { get; set; }
        public string PostDimensions { get; set; }

        public string FreightEstimate { get; set; }
        public string InstallationEstimate { get; set; }
        public string SteelEstimate { get; set; }

        public string OrderDate { get; set; }
        public string DueDate { get; set; }
        public double? SprayBarSize { get; set; }
        public int? NumberOfBaySidewalls { get; set; }
        public string BottomCordType { get; set; }
        public string BottomCordSize { get; set; }
        public double? BottomCordHeight { get; set; }
        public string Location { get; set; }
        public string AdditionalInformation { get; set; }
        public string CustomerNotes { get; set; }

        public double? TrussSpacing { get; set; }
        public double? WalkwayWidth { get; set; }
        public double? ClearanceWidth { get; set; }

        public int? QuantityOfSystems { get; set; }
        public int? SelectedDrumSize { get; set; }
        public double? BasketSpacing { get; set; }
        public bool CenterBayWaterStation { get; set; }
        public bool EvVpdThirdPartyControl { get; set; }
        public bool ThreeFourthAMIADFilterAssembly { get; set; }
        public bool LockLine { get; set; }
        public double? ExtensionHangerLength { get; set; }
        public bool RemotePullChainSwitch { get; set; }
        public bool DRAMMRedheadBreakerUpgrade { get; set; }
        public bool WaterSolenoidControl { get; set; }

        public CustomerViewModel Customer { get; set; }
        public string FullAddress { get; set; }

        public List<string> CalculationErrors { get; set; }
    }
}