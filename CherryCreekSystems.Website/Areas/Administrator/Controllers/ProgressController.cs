﻿using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.Website.Models;

namespace CherryCreekSystems.Website.Areas.Administrator.Controllers
{
    [Authorize(Roles = "administrator")]
    public class ProgressController : Controller
    {
        public async Task<ActionResult> Index()
        {
            var viewModel = await GetProgressPageViewModel();
            return View(viewModel);
        }

        public async Task<ActionResult> ProgressPage(ProgressPageViewModel inputViewModel)
        {
            var viewModel = await GetProgressPageViewModel(inputViewModel.Search, inputViewModel.QuoteStatusId);
            return PartialView("_Progress", viewModel);
        }

        private async Task<ProgressPageViewModel> GetProgressPageViewModel(string search = "", int? quoteStatusId = null)
        {
            var getProgressTask = GetProgressViewModel(search, quoteStatusId);

            var quoteStatusService = new QuoteStatusService();
            var quoteStatusesTask = quoteStatusService.Get().OrderedById().ToListAsync();

            var quoteStatuses = await quoteStatusesTask;
            var progressViewModels = await getProgressTask;

            var viewModel = new ProgressPageViewModel
            {
                QuoteStatuses = quoteStatuses,
                ProgressModels = progressViewModels
            };

            return viewModel;
        }

        private static async Task<IEnumerable<ProgressViewModel>> GetProgressViewModel(string search = "", int? quoteStatusId = null)
        {
            var quoteRequestService = new QuoteRequestService();
            var quoteRequests = quoteRequestService.Get().WhereActive();

            var viewModel = await quoteRequestService.GetProgressViewModel(quoteRequests, search, quoteStatusId);
            return viewModel;

        }

        //public async Task<ActionResult> Status()
        //{
        //    using (var context = new DataContext())
        //    {
        //        var progressData = await context.QuoteBoomSystems.GetProgressViewModel(quoteStatusId: 2); //In Progress
        //        var progressCalculator = new ProgressStatusCalculator();
        //        var viewModel = progressCalculator.GetStatusViewModel(progressData);
        //        return View(viewModel);
        //    }
        //}

        //public async Task<ActionResult> StatusUpdate(ProgressViewModel viewModel)
        //{
        //    try
        //    {
        //        using (var context = new DataContext())
        //        {
        //            var updatedBoom = BoomTypeFactory.CreateBoomType(viewModel.BoomType);
        //            SetValuesToForceUpdates(updatedBoom);
        //            updatedBoom.Id = viewModel.BoomId;
        //            context.Booms.Attach(updatedBoom);
        //            updatedBoom.InfoAndDesignStatus = viewModel.InfoAndDesignStatus;
        //            updatedBoom.SprayBarApprovalStatus = viewModel.SprayBarApprovalStatus;
        //            updatedBoom.AssemblyPickListStatus = viewModel.AssemblyPickListStatus;
        //            updatedBoom.ShippingPickListStatus = viewModel.ShippingPickListStatus;
        //            updatedBoom.ProductionStatus = viewModel.ProductionStatus;
        //            updatedBoom.SawAndSheerStatus = viewModel.SawAndSheerStatus;
        //            updatedBoom.PunchAndBreakStatus = viewModel.PunchAndBreakStatus;
        //            updatedBoom.MachiningStatus = viewModel.MachiningStatus;
        //            updatedBoom.WeldingStatus = viewModel.WeldingStatus;
        //            updatedBoom.GalganizerStatus = viewModel.GalganizerStatus;
        //            updatedBoom.AssemblyStatus = viewModel.AssemblyStatus;
        //            updatedBoom.ShippingStatus = viewModel.ShippingStatus;

        //            var updatedQuoteRequest = new QuoteRequest { Id = viewModel.QuoteRequestId };
        //            context.QuoteRequests.Attach(updatedQuoteRequest);
        //            updatedQuoteRequest.QuoteStatusId = viewModel.StatusId;
        //            updatedQuoteRequest.OrderDate = viewModel.OrderDate;
        //            updatedQuoteRequest.DueDate = viewModel.DueDate;

        //            await context.SaveChangesAsync();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Content("0");
        //    }

        //    return Content("1");
        //}

        /// <summary>
        /// Set all of the values to -1, that way EF will always recognize that a change comes from the client
        /// and updates the data
        /// </summary>
        /// <param name="boom"></param>
        private void SetValuesToForceUpdates(Boom boom)
        {
            boom.InfoAndDesignStatus = -1;
            boom.SprayBarApprovalStatus = -1;
            boom.AssemblyPickListStatus = -1;
            boom.ShippingPickListStatus = -1;
            boom.ProductionStatus = -1;
            boom.SawAndSheerStatus = -1;
            boom.PunchAndBreakStatus = -1;
            boom.MachiningStatus = -1;
            boom.WeldingStatus = -1;
            boom.GalganizerStatus = -1;
            boom.AssemblyStatus = -1;
            boom.ShippingStatus = -1;
        }
    }
}