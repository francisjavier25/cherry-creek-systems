﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Website.Models;
using PagedList;
using PagedList.EntityFramework;
using System.Threading.Tasks;
using System.Web.Mvc;
using CherryCreekSystems.BL.QueryExtensions;

namespace CherryCreekSystems.Website.Areas.Administrator.Controllers
{
    [Authorize(Roles = "administrator")]
    public class SalesRepsController : Controller
    {
        public async Task<ActionResult> Index(SalesRepsViewModel viewModel)
        {
            viewModel.SalesReps = await GetSalesRepsPage(viewModel.Page, viewModel.Search);

            return View(viewModel);
        }

        public async Task<ActionResult> SalesRepsPage(SalesRepsViewModel viewModel)
        {
            viewModel.SalesReps = await GetSalesRepsPage(viewModel.Page, viewModel.Search);

            return PartialView("_SalesReps", viewModel);
        }

        public static async Task<IPagedList<SalesRepUser>> GetSalesRepsPage(
                  int page,
                  string searchTerm)
        {
            using (var context = new DataContext())
            {
                var salesReps = context.GetSalesRepUsers()
                                          .BySearch(searchTerm)
                                          .GetOrdered();

                return await salesReps.ToPagedListAsync(page, 15);
            }
        }
    }
}