﻿using System;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Website.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.Domain;
using Newtonsoft.Json;

namespace CherryCreekSystems.Website.Areas.Administrator.Controllers
{
    [Authorize(Roles = "administrator")]
    public class QuotesController : Website.Controllers.QuotesController
    {
        public async Task<ActionResult> Index(QuotesViewModel viewModel)
        {
            var distributorService = new DistributorService();
            var customerService = new CustomerService();
            var salesRepService = new SalesRepService();
            var quoteStatusService = new QuoteStatusService();

            var getDistributorsTask = distributorService.Get().OrderedByName().ToListAsync();
            var getCustomersTask = customerService.Get().OrderedByCompanyName().ToListAsync();
            var getSalesRepsTask = salesRepService.Get().OrderedByName().ToListAsync();
            var getQuoteStatusesTask = quoteStatusService.Get().OrderedByName().ToListAsync();

            viewModel.QuoteRequests = await GetQuoteRequestsPage(
                viewModel.Page,
                distributorId: viewModel.DistributorId,
                customerId: viewModel.CustomerId,
                salesRepId: viewModel.SalesRepId,
                quoteStatusId: viewModel.QuoteStatusId,
                boomTypeId: viewModel.BoomTypeId);

            var boomTypeIds = BoomTypeStructure.GetList();
            var distributors = await getDistributorsTask;
            var customers = await getCustomersTask;
            var salesReps = await getSalesRepsTask;
            var quoteStatuses = await getQuoteStatusesTask;

            viewModel.Distributors = distributors.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.Name
            }).ToList();

            viewModel.Distributors.Insert(0, new SelectListItem { Value = "", Text = "All" });

            viewModel.Customers = customers.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.CompanyName
            }).ToList();

            viewModel.Customers.Insert(0, new SelectListItem { Value = "", Text = "All" });

            viewModel.SalesReps = salesReps.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.Name
            }).ToList();

            viewModel.SalesReps.Insert(0, new SelectListItem { Value = "", Text = "All" });

            viewModel.QuoteStatuses = quoteStatuses.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.Name
            }).ToList();

            viewModel.QuoteStatuses.Insert(0, new SelectListItem { Value = "", Text = "All" });

            viewModel.BoomTypes = boomTypeIds.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.Name
            }).ToList();

            viewModel.BoomTypes.Insert(0, new SelectListItem { Value = "", Text = "All" });

            return View(viewModel);
        }
    }
}