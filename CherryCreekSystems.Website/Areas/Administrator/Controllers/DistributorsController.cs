﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Website.Models;
using PagedList;
using PagedList.EntityFramework;
using System.Threading.Tasks;
using System.Web.Mvc;
using CherryCreekSystems.BL.QueryExtensions;

namespace CherryCreekSystems.Website.Areas.Administrator.Controllers
{
    [Authorize(Roles = "administrator")]
    public class DistributorsController : Controller
    {
        // GET: Administrator/Distributors
        public async Task<ActionResult> Index(DistributorsViewModel viewModel)
        {
            viewModel.Distributors = await GetDistributorsPage(viewModel.Page, viewModel.Search);

            return View(viewModel);
        }

        public async Task<ActionResult> DistributorsPage(DistributorsViewModel viewModel)
        {
            viewModel.Distributors = await GetDistributorsPage(viewModel.Page, viewModel.Search);

            return PartialView("_Distributors", viewModel);
        }

        public static async Task<IPagedList<DistributorUser>> GetDistributorsPage(
                  int page,
                  string searchTerm)
        {
            using (var context = new DataContext())
            {
                var distributors = context.GetDistributorUsers()
                                          .BySearch(searchTerm)
                                          .GetOrdered();

                return await distributors.ToPagedListAsync(page, 15);
            }
        }
    }
}