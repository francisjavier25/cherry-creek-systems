﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Website.Models;
using PagedList;
using PagedList.EntityFramework;
using System.Threading.Tasks;
using System.Web.Mvc;
using CherryCreekSystems.BL.QueryExtensions;

namespace CherryCreekSystems.Website.Areas.Administrator.Controllers
{
    [Authorize(Roles = "administrator")]
    public class CustomersController : Controller
    {
        // GET: Administrator/Distributors
        public async Task<ActionResult> Index(CustomersViewModel viewModel)
        {
            viewModel.Customers = await GetCustomersPage(viewModel.Page, viewModel.Search);

            return View(viewModel);
        }

        public async Task<ActionResult> Customerspage(CustomersViewModel viewModel)
        {
            viewModel.Customers = await GetCustomersPage(viewModel.Page, viewModel.Search);

            return PartialView("_Customers", viewModel);
        }

        public static async Task<IPagedList<CustomerUser>> GetCustomersPage(
                  int page,
                  string searchTerm)
        {
            using (var context = new DataContext())
            {
                var customers = context.GetCustomerUsers()
                                       .BySearch(searchTerm)
                                       .GetOrdered();

                return await customers.ToPagedListAsync(page, 15);
            }
        }
    }
}