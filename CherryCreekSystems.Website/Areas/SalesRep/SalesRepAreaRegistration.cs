﻿using System.Web.Mvc;

namespace CherryCreekSystems.Website.Areas.SalesReps
{
    public class SalesRepAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SalesRep";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SalesRep_default",
                "SalesRep/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CherryCreekSystems.Website.Areas.SalesReps.Controllers" }
            );
        }
    }
}