﻿using System.Web.Mvc;

namespace CherryCreekSystems.Website.Areas.Distributors
{
    public class DistributorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Distributor";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Distributor_default",
                "Distributor/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CherryCreekSystems.Website.Areas.Distributors.Controllers" }
            );
        }
    }
}