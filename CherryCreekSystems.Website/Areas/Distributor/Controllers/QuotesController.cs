﻿using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Website.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;

namespace CherryCreekSystems.Website.Areas.Distributors.Controllers
{
    [Authorize(Roles = "distributor")]
    public class QuotesController : Website.Controllers.QuotesController
    {
        // GET: Administrator/Quotes
        public async Task<ActionResult> Index(QuotesViewModel viewModel)
        {
            var customerService = new CustomerService();
            var quoteStatusService = new QuoteStatusService();

            var getQuoteStatusesTask = quoteStatusService.Get().OrderedByName().ToListAsync();

            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            viewModel.DistributorId = (await user.DistributorIds()).First();

            var getCustomersTask = customerService.GetCustomersForDistributor(viewModel.DistributorId.Value).ToListAsync();

            viewModel.QuoteRequests = await GetQuoteRequestsPage(
                viewModel.Page,
                distributorId: viewModel.DistributorId,
                customerId: viewModel.CustomerId,
                quoteStatusId: viewModel.QuoteStatusId,
                boomTypeId: viewModel.BoomTypeId);

            var boomTypeIds = BoomTypeStructure.GetList();
            var quoteStatuses = await getQuoteStatusesTask;
            var customers = await getCustomersTask;

            viewModel.Customers = customers.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.CompanyName
            }).ToList();

            viewModel.Customers.Insert(0, new SelectListItem { Value = "", Text = "All" });

            viewModel.QuoteStatuses = quoteStatuses.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.Name
            }).ToList();

            viewModel.QuoteStatuses.Insert(0, new SelectListItem { Value = "", Text = "All" });

            viewModel.BoomTypes = boomTypeIds.Select(d => new SelectListItem
            {
                Value = d.Id.ToString(),
                Text = d.Name
            }).ToList();

            viewModel.BoomTypes.Insert(0, new SelectListItem { Value = "", Text = "All" });

            return View(viewModel);
        }
    }
}