﻿
$(document)
    .ready(function () {
        $(document).on("keypress", ".numeric", function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }

            return true;
        });

        $(document).on("keyup", ".numeric", function () {
            if ($(this).val() == "") {
                $(this).val("0");
            }

            var parentRow = $(this).closest("tr");
            var saveButton = parentRow.find(".save");
            saveButton.css("visibility", "hidden");

            parentRow.find(".numeric")
                .each(function () {
                    var element = $(this);
                    var originalValue = element.data("original");
                    var currentValue = element.val();

                    if (originalValue != currentValue) {
                        saveButton.css("visibility", "visible");
                        return false;
                    }
                });
        });

        $(document).on("keydown", ".numeric", function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 9) {
                if ($(this).val() == "0") {
                    $(this).val("");
                }
            }
        });

        $(document).on("change", ".statusId", function (evt) {
            var parentRow = $(this).closest("tr");
            var saveButton = parentRow.find(".save");
            saveButton.css("visibility", "visible");
        });

        $(document).on("click", ".save", function () {
            var saveButton = $(this);
            var parentRow = saveButton.closest("tr");
            var saveCompleted = parentRow.find(".saveCompleted");

            var boomId = parentRow.data("id");
            var boomType = parentRow.data("boomtype");
            var quoteRequestId = parentRow.data("quote-id");
            var orderDate = parentRow.find(".dpd1").val();
            var dueDate = parentRow.find(".dpd2").val();

            var statusIdElement = parentRow.find(".statusId");
            var statusId = statusIdElement.val();
            var originalStatusId = statusIdElement.data("original");

            var infoAndDesign = parentRow.find("#item_InfoAndDesignStatus").val();
            var sprayBarApproval = parentRow.find("#item_SprayBarApprovalStatus").val();
            var assemblyPickList = parentRow.find("#item_AssemblyPickListStatus").val();
            var shippingPickList = parentRow.find("#item_ShippingPickListStatus").val();
            var production = parentRow.find("#item_ProductionStatus").val();
            var sawAndSheer = parentRow.find("#item_SawAndSheerStatus").val();
            var punchAndBreak = parentRow.find("#item_PunchAndBreakStatus").val();
            var machining = parentRow.find("#item_MachiningStatus").val();
            var welding = parentRow.find("#item_WeldingStatus").val();
            var galvanizer = parentRow.find("#item_GalganizerStatus").val();
            var assembly = parentRow.find("#item_AssemblyStatus").val();
            var shipping = parentRow.find("#item_ShippingStatus").val();

            var data =
            {
                QuoteRequestId: quoteRequestId,
                BoomId: boomId,
                BoomType: boomType,
                OrderDate: orderDate,
                DueDate: dueDate,
                StatusId: statusId,
                InfoAndDesignStatus: infoAndDesign,
                SprayBarApprovalStatus: sprayBarApproval,
                AssemblyPickListStatus: assemblyPickList,
                ShippingPickListStatus: shippingPickList,
                ProductionStatus: production,
                SawAndSheerStatus: sawAndSheer,
                PunchAndBreakStatus: punchAndBreak,
                MachiningStatus: machining,
                WeldingStatus: welding,
                GalganizerStatus: galvanizer,
                AssemblyStatus: assembly,
                ShippingStatus: shipping
            };

            $.post("/Administrator/Progress/StatusUpdate",
                data,
                function (result) {
                    if (result == "1") {
                        saveButton.css("visibility", "hidden");
                        saveButton.toggle();
                        saveCompleted.toggle({
                            complete: function () {
                                saveCompleted.fadeToggle(2000,
                                    function () {
                                        saveButton.toggle();
                                    }
                                );

                                //If the status changed for this system, that means that it changed for other systems, so eliminate those rows
                                if (statusId != originalStatusId) {
                                    $('tr[data-quote-id="' + quoteRequestId + '"]').remove();
                                }
                            }
                        });
                    } else {
                        alert("There was error saving the data");
                    }
                });
        });
    });
