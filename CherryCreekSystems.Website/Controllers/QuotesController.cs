﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Website.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using PagedList.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CherryCreekSystems.Website.Controllers
{
    public class QuotesController : Controller
    {
        private ApplicationUserManager _userManager;

        protected ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async Task<ActionResult> QuotesPage(QuotesViewModel viewModel)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (await user.IsDistributor())
            {
                viewModel.DistributorId = (await user.DistributorIds()).First();
            }

            if (await user.IsCustomer())
            {
                viewModel.CustomerId = (await user.CustomerIds()).First();
            }

            if (await user.IsSalesRep())
            {
                viewModel.SalesRepId = (await user.SalesRepIds()).First();
            }

            var quoteRequests = await GetQuoteRequestsPage(viewModel.Page, viewModel.DistributorId, viewModel.CustomerId, viewModel.SalesRepId, viewModel.QuoteStatusId, viewModel.BoomTypeId);
            viewModel.QuoteRequests = quoteRequests;

            return PartialView("_Quotes", viewModel);
        }

        public async Task<ActionResult> Download(int id)
        {
            #region security check

            var quoteRequestService = new QuoteRequestService();
            var getQuoteRequestTask = quoteRequestService.Get().ByQuoteId(id);

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var quoteRequest = await getQuoteRequestTask;

            var allowedToSeeInfo = await SecurityCheck(user, quoteRequest);
            if (!allowedToSeeInfo)
            {
                return HttpNotFound();
            }

            #endregion security check

            var reportManager = new ReportManager();
            var fileName = reportManager.CreateMainReportFileName(quoteRequest.QuoteNumber);
            var stream = await reportManager.GenerateMainQuoteReportStream(id);

            stream.Position = 0;
            return File(stream, "application/pdf", fileName);
        }

        public async Task<ActionResult> DownloadSystem(string id, int quoteId)
        {
            #region security check

            var quoteRequestService = new QuoteRequestService();
            var getQuoteRequestTask = quoteRequestService.Get().ByQuoteId(quoteId);

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var quoteRequest = await getQuoteRequestTask;

            var allowedToSeeInfo = await SecurityCheck(user, quoteRequest);
            if (!allowedToSeeInfo)
            {
                return HttpNotFound();
            }

            #endregion security check

            var system = quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.First(c => c.Id_ == id);

            var reportManager = new ReportManager();
            var fileName = reportManager.CreateDetailReportFileName(quoteRequest.QuoteNumber, system.SystemName);
            var stream = await reportManager.GenerateDetailQuoteReportStream(quoteId, id);

            stream.Position = 0;
            return File(stream, "application/pdf", fileName);
        }

        public async Task<ActionResult> Systems(int id, string systemId, QuotesViewModel previousPageData)
        {
            var quoteRequestService = new QuoteRequestService();
            var getQuoteRequestTask = quoteRequestService.Get().ByQuoteId(id);

            #region security check

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var quoteRequest = await getQuoteRequestTask;

            var allowedToSeeInfo = await SecurityCheck(user, quoteRequest);
            if (!allowedToSeeInfo)
            {
                return HttpNotFound();
            }

            #endregion security check

            var boomSystems = quoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive();
            boomSystems.CalculateSystemsProgress();

            var viewModel = new QuoteBoomSystemsViewModel
            {
                QuoteNumber = quoteRequest.QuoteNumber,
                ShippingDate = quoteRequest.DueDate.ToString("MM/dd/yyyy"),
                QuoteBoomSystems = boomSystems,
                Page = previousPageData.Page,
                DistributorId = previousPageData.DistributorId,
                CustomerId = previousPageData.CustomerId,
                SalesRepId = previousPageData.SalesRepId,
                QuoteStatusId = previousPageData.QuoteStatusId,
                QuoteId = previousPageData.QuoteId
            };

            ViewBag.SystemId = systemId;

            return View(viewModel);
        }

        protected async Task<bool> SecurityCheck(ApplicationUser user, QuoteRequest quoteRequest)
        {
            if (await user.IsDistributor())
            {
                var allowedToSeeInfo = quoteRequest.DistributorId == (await user.DistributorIds()).First();
                if (!allowedToSeeInfo)
                    return false;
            }

            if (await user.IsCustomer())
            {
                var allowedToSeeInfo = quoteRequest.CustomerId == (await user.CustomerIds()).First();
                if (!allowedToSeeInfo)
                    return false;
            }

            if (await user.IsSalesRep())
            {
                var allowedToSeeInfo = quoteRequest.SalesRepId == (await user.SalesRepIds()).First();
                if (!allowedToSeeInfo)
                    return false;
            }

            return true;
        }

        protected static async Task<IPagedList<QuoteViewModel>> GetQuoteRequestsPage(
           int page,
           int? distributorId = null,
           int? customerId = null,
           int? salesRepId = null,
           int? quoteStatusId = null,
           int? boomTypeId = null)
        {
            var quoteRequestService = new QuoteRequestService();
            IQueryable<QuoteRequest> quoteRequests;

            if (boomTypeId.HasValue)
            {
                quoteRequests = quoteRequestService.GetByBoomType(boomTypeId.Value);
            }
            else
            {
                quoteRequests = quoteRequestService.Get();
            }
            quoteRequests = quoteRequests
                .ByDistributor(distributorId)
                .ByCustomer(customerId)
                .BySalesRep(salesRepId)
                .ByQuoteStatus(quoteStatusId)
                .OrderedByDateDescending()
                .WhereActive();

            var quoteRequestsResult = await quoteRequests.ToPagedListAsync(page, 15);

            await quoteRequestService.FillChildObjects(quoteRequests: quoteRequestsResult.ToArray(),
    customer: true, distributor: true, quoteStatus: true);

            var viewModel = quoteRequestsResult.Select(q =>
            new QuoteViewModel
            {
                QuoteRequest = q,
                BoomTypes = q.GetBoomTypes()
            });

            var pagedViewModel = new StaticPagedList<QuoteViewModel>(viewModel, quoteRequestsResult.GetMetaData());

            return pagedViewModel;
        }

        //public async Task<ActionResult> Checklist(int boomId, bool saved = false)
        //{
        //    var customerCheckList = new CustomerCheckList();
        //    using (var context = new DataContext())
        //    {
        //        var boom = await context.Booms
        //                                .Include(b => b.CustomerCheckList)
        //                                .FirstOrDefaultAsync(b => b.Id == boomId);
        //        if (boom?.CustomerCheckList != null)
        //        {
        //            customerCheckList = boom.CustomerCheckList;
        //        }
        //        else
        //        {
        //            customerCheckList.Date = DateTime.Now;
        //            customerCheckList = new CustomerCheckList
        //            {
        //                Id = 0,
        //                Customer = "Test",
        //                //Name = "Customer",
        //                AdditionalCustomRequests = "Some other needed stuff",
        //                Email = "test@test.net",
        //                NumberOfRows = "5",
        //                WalkwayLocation = "some location",
        //                SpraybarSize = "10",
        //                CropRowWidth = "1 foot",
        //                PostDimensions = "5 x 6",
        //                HoseSize = "5",
        //                BottomCordOrientation = "portrait",
        //                VFDOrEnvironmenalControls = "VFD",
        //                TrussHoopDimensionAndType = "4 foot round",
        //                BayLength = "100",
        //                BayWidth = "200",
        //                DistanceFromCenterToCenterOfRail = "50",
        //                WalkwayWidth = "25",
        //                QuoteNumber = "QT 8878",
        //                IsWalkwayClearanceRequired = true,
        //                IsTherePreexistingRails = true,
        //                RequestedBoom = "Sky-Rail Boom",
        //                TypeAndSizeOfRail = "long 10 meters",
        //                BenchHeight = "99",
        //                CropHeight = "88",
        //                TrussSpacing = "77",
        //                TrussHeight = "66",
        //                CenteredOrOffset = "centered",
        //                FaxNumber = "11-222-333",
        //                OnsiteContact = "Contact 1",
        //                PhoneNumber = "999-999-9999",
        //                RailTypeAndSize = "a new awesome rail",
        //                RequestedBoomQuantity = "5",
        //                ShipToAddressCity = "CS",
        //                ShipToAddressName = "warehouse 1",
        //                ShipToAddressState = "CO",
        //                ShipToAddressStreet = "street 1",
        //                ShipToAddressZipCode = "80923",
        //                WalkwayClearanceRequirements = "needs to be tall"
        //            };
        //        }

        //        ViewBag.BoomId = boom.Id;
        //        ViewBag.ChecklistId = customerCheckList.Id;
        //    }

        //    ViewBag.Saved = saved;
        //    return View(customerCheckList);
        //}

        [HttpPost]
        //public async Task<ActionResult> Checklist(int boomId, HttpPostedFileBase BoomSketchUpload, HttpPostedFileBase TrussSketchUpload)
        //{
        //    using (var context = new DataContext())
        //    {
        //        var boom = await context.Booms
        //                                .Include(b => b.CustomerCheckList)
        //                                .FirstOrDefaultAsync(b => b.Id == boomId);

        //        var customerCheckList = boom.CustomerCheckList ?? new CustomerCheckList();
        //        UpdateModel(customerCheckList);
        //        customerCheckList.Date = DateTime.Now;

        //        if (BoomSketchUpload != null && BoomSketchUpload.ContentLength > 0)
        //            await SaveBoomSketchImage(BoomSketchUpload, customerCheckList);

        //        if (TrussSketchUpload != null && TrussSketchUpload.ContentLength > 0)
        //            await SaveTrussSketchImage(TrussSketchUpload, customerCheckList);

        //        if (boom.CustomerCheckList == null)
        //        {
        //            context.CustomerCheckLists.Add(customerCheckList);
        //            boom.CustomerCheckListId = customerCheckList.Id;
        //        }

        //        await context.SaveChangesAsync();
        //    }

        //    return RedirectToAction("Checklist", new { boomId, saved = true, });
        //}

        #region save images

        private static async Task SaveBoomSketchImage(HttpPostedFileBase boomSketchUpload, CustomerCheckList checkList)
        {
            var boomSketchBytes = new byte[boomSketchUpload.InputStream.Length];
            await boomSketchUpload.InputStream.ReadAsync(boomSketchBytes, 0, boomSketchBytes.Length);

            checkList.BoomSketchImageName = boomSketchUpload.FileName;
            checkList.BoomSketchImage = boomSketchBytes;
        }

        private static async Task SaveTrussSketchImage(HttpPostedFileBase trussSketchUpload, CustomerCheckList checkList)
        {
            var trussSketchBytes = new byte[trussSketchUpload.InputStream.Length];
            await trussSketchUpload.InputStream.ReadAsync(trussSketchBytes, 0, trussSketchBytes.Length);

            checkList.TrussSketchImageName = trussSketchUpload.FileName;
            checkList.TrussSketchImage = trussSketchBytes;
        }

        #endregion save images
    }
}