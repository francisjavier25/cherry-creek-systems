﻿using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Website.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CherryCreekSystems.Website.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async Task<ActionResult> Index(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return await RedirectToLocal(returnUrl, User.Identity.Name);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return await RedirectToLocal(returnUrl, model.UserName);

                //case SignInStatus.LockedOut:
                //    return View("Lockout");

                //case SignInStatus.RequiresVerification:
                //    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View("Index", model);
            }
        }

        private async Task<ActionResult> RedirectToLocal(string returnUrl, string userName = "")
        {
            if (!string.IsNullOrEmpty(returnUrl))
            {
                if (Url.IsLocalUrl(returnUrl))
                {
                    return Redirect(returnUrl);
                }
            }

            ApplicationUser user = UserManager.FindByName(userName);

            if (await user.IsAdmin())
            {
                return RedirectToAction("Index", "Quotes", new { area = "Administrator" });
            }

            if (await user.IsDistributor())
            {
                return RedirectToAction("Index", "Quotes", new { area = "Distributor" });
            }

            if (await user.IsSalesRep())
            {
                return RedirectToAction("Index", "Quotes", new { area = "SalesRep" });
            }

            return RedirectToAction("Index", "Quotes", new { area = "Customer" });
        }
    }
}