﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Services;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.SugarCRMLogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace CherryCreekSystems.Website.Controllers
{
    [RoutePrefix("api/Customers")]
    public class CustomersController : ApiController
    {
        [Route("CreateCustomer")]
        [HttpPost]
        public async Task<IHttpActionResult> Get()
        {
            var rawCustomer = await Request.Content.ReadAsStringAsync();
            var customerFields = HttpUtility.ParseQueryString(rawCustomer);

            DataContext context = new DataContext();
            var customerLogService = new CustomerLogService(context);
            var customerService = new CustomerService(context);

            #region save raw customer log (always create a new entry)
            var customerLog = new CustomerLog
            {
                CustomerData = rawCustomer,
                EntryDate = DateTime.Now,
                Name = customerFields["name"],
                Fax = customerFields["phone_fax"],
                Phone = customerFields["phone_office"],
                Email = customerFields["email1"],
                SugarCRMId = customerFields["id"]
            };
            customerLogService.Add(customerLog);
            #endregion

            #region obtain address information

            var addressField = customerFields["shipping_address_street"];
            var cityField = customerFields["shipping_address_city"];
            var zipField = customerFields["shipping_address_postalcode"];
            var stateField = customerFields["shipping_address_state"];

            #endregion

            #region create customer if necessary 

            var customerInQuotationSystem = await customerService.Get().BySugarCRMId(customerLog.SugarCRMId);
            if (customerInQuotationSystem == null)
            {
                customerLog.Action = "New";

                ShipAddress shipAddress = await CreateShipAddressIfAvailable(context, addressField, cityField, zipField, stateField);

                var newCustomer = new Customer
                {
                    CompanyName = customerLog.Name,
                    Fax = customerLog.Fax,
                    Phone = customerLog.Phone,
                    Email = customerLog.Email,
                    SugarCRMId = customerLog.SugarCRMId,
                    ShipAddresses = shipAddress == null ? null : new List<ShipAddress> { shipAddress }
                };

                var userCreator = new UserCreator(context);
                await userCreator.CreateCustomerUser(newCustomer);
            }
            else //or update with information from SugarCRM
            {
                await customerService.FillChildObjects(shipAddress: true, customers: customerInQuotationSystem);

                customerLog.Action = "Update";
                customerInQuotationSystem.CompanyName = customerLog.Name;
                customerInQuotationSystem.Fax = customerLog.Fax;
                customerInQuotationSystem.Phone = customerLog.Phone;
                customerInQuotationSystem.Email = customerLog.Email;

                var sugarCRMAddress = customerInQuotationSystem.ShipAddresses?.FirstOrDefault(a => a.IsSugarCRMAddress);
                if (sugarCRMAddress != null)
                {
                    sugarCRMAddress.Street1 = addressField;
                    sugarCRMAddress.City = cityField;
                    sugarCRMAddress.Zip = zipField;

                    await UpdateStateIfAvailable(context, stateField, sugarCRMAddress);
                }
                else
                {
                    ShipAddress shipAddress = await CreateShipAddressIfAvailable(context, addressField, cityField, zipField, stateField);

                    if (shipAddress != null)
                    {
                        customerInQuotationSystem.ShipAddresses = new List<ShipAddress> { shipAddress };
                    }
                }
            }
            #endregion

            await context.SaveChangesAsync();

            return Ok();
        }

        private static async Task<ShipAddress> CreateShipAddressIfAvailable(DataContext context, string addressField, string cityField, string zipField, string stateField)
        {
            ShipAddress shipAddress = null;
            if (!string.IsNullOrEmpty(addressField))
            {
                shipAddress = new ShipAddress
                {
                    Street1 = addressField,
                    City = cityField,
                    Zip = zipField,
                    IsSugarCRMAddress = true
                };

                await UpdateStateIfAvailable(context, stateField, shipAddress);
            }

            return shipAddress;
        }

        private static async Task UpdateStateIfAvailable(DataContext context, string stateField, ShipAddress shipAddress)
        {
            if (!string.IsNullOrEmpty(stateField))
            {
                //Check for a state name match
                var statesService = new StatesService(context);
                var state = await statesService.Get().ByName(stateField);
                if (state != null)
                {
                    shipAddress.StateId = state.Id;
                }
            }
        }
    }
}
