﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CherryCreekSystems.Website.Startup))]
namespace CherryCreekSystems.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
