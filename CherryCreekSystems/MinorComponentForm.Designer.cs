﻿namespace CherryCreekSystems
{
    partial class MinorComponentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label33 = new System.Windows.Forms.Label();
            this.lstBoomSpecs = new System.Windows.Forms.ListBox();
            this.txtDetailCalculation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ddlOrder = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtMinorComponentName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.emptyTextErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblId = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDetailId = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDescriptionId = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescriptionCalculation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtActivateCondition = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.emptyTextErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(1185, 31);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(128, 24);
            this.label33.TabIndex = 91;
            this.label33.Text = "Boom Specs";
            // 
            // lstBoomSpecs
            // 
            this.lstBoomSpecs.FormattingEnabled = true;
            this.lstBoomSpecs.ItemHeight = 16;
            this.lstBoomSpecs.Location = new System.Drawing.Point(1074, 67);
            this.lstBoomSpecs.Name = "lstBoomSpecs";
            this.lstBoomSpecs.Size = new System.Drawing.Size(345, 644);
            this.lstBoomSpecs.TabIndex = 89;
            this.lstBoomSpecs.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstBoomSpecs_MouseDown);
            // 
            // txtDetailCalculation
            // 
            this.txtDetailCalculation.AllowDrop = true;
            this.txtDetailCalculation.Location = new System.Drawing.Point(221, 344);
            this.txtDetailCalculation.Multiline = true;
            this.txtDetailCalculation.Name = "txtDetailCalculation";
            this.txtDetailCalculation.Size = new System.Drawing.Size(788, 133);
            this.txtDetailCalculation.TabIndex = 86;
            this.txtDetailCalculation.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtDetailCalculation.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtDetailCalculation.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 338);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 84;
            this.label2.Text = "Detail Calculation:";
            this.label2.UseMnemonic = false;
            // 
            // ddlOrder
            // 
            this.ddlOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlOrder.FormattingEnabled = true;
            this.ddlOrder.Location = new System.Drawing.Point(220, 702);
            this.ddlOrder.Name = "ddlOrder";
            this.ddlOrder.Size = new System.Drawing.Size(788, 24);
            this.ddlOrder.TabIndex = 83;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 705);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 82;
            this.label1.Text = "Order:";
            this.label1.UseMnemonic = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(684, 769);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(113, 52);
            this.btnClose.TabIndex = 81;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(419, 769);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(113, 52);
            this.btnUpdate.TabIndex = 80;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(419, 769);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(113, 52);
            this.btnAdd.TabIndex = 79;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtMinorComponentName
            // 
            this.txtMinorComponentName.Location = new System.Drawing.Point(221, 67);
            this.txtMinorComponentName.Name = "txtMinorComponentName";
            this.txtMinorComponentName.Size = new System.Drawing.Size(788, 22);
            this.txtMinorComponentName.TabIndex = 78;
            this.txtMinorComponentName.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateTextboxHasValue);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(142, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 77;
            this.label3.Text = "Name:";
            this.label3.UseMnemonic = false;
            // 
            // emptyTextErrorProvider
            // 
            this.emptyTextErrorProvider.BlinkRate = 0;
            this.emptyTextErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.emptyTextErrorProvider.ContainerControl = this;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(552, 769);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(113, 52);
            this.btnDelete.TabIndex = 93;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(218, 31);
            this.lblId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(0, 17);
            this.lblId.TabIndex = 95;
            this.lblId.UseMnemonic = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(166, 31);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 17);
            this.label8.TabIndex = 94;
            this.label8.Text = "ID:";
            this.label8.UseMnemonic = false;
            // 
            // lblDetailId
            // 
            this.lblDetailId.AutoSize = true;
            this.lblDetailId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailId.Location = new System.Drawing.Point(218, 302);
            this.lblDetailId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDetailId.Name = "lblDetailId";
            this.lblDetailId.Size = new System.Drawing.Size(0, 17);
            this.lblDetailId.TabIndex = 97;
            this.lblDetailId.UseMnemonic = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(126, 302);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 17);
            this.label4.TabIndex = 96;
            this.label4.Text = "Detail ID:";
            this.label4.UseMnemonic = false;
            // 
            // lblDescriptionId
            // 
            this.lblDescriptionId.AutoSize = true;
            this.lblDescriptionId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionId.Location = new System.Drawing.Point(219, 121);
            this.lblDescriptionId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescriptionId.Name = "lblDescriptionId";
            this.lblDescriptionId.Size = new System.Drawing.Size(0, 17);
            this.lblDescriptionId.TabIndex = 108;
            this.lblDescriptionId.UseMnemonic = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(92, 121);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 107;
            this.label5.Text = "Description ID:";
            this.label5.UseMnemonic = false;
            // 
            // txtDescriptionCalculation
            // 
            this.txtDescriptionCalculation.AllowDrop = true;
            this.txtDescriptionCalculation.Location = new System.Drawing.Point(221, 155);
            this.txtDescriptionCalculation.Multiline = true;
            this.txtDescriptionCalculation.Name = "txtDescriptionCalculation";
            this.txtDescriptionCalculation.Size = new System.Drawing.Size(788, 133);
            this.txtDescriptionCalculation.TabIndex = 106;
            this.txtDescriptionCalculation.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtDescriptionCalculation.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtDescriptionCalculation.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 192);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 17);
            this.label7.TabIndex = 105;
            this.label7.Text = "Description Calculation:";
            this.label7.UseMnemonic = false;
            // 
            // txtActivateCondition
            // 
            this.txtActivateCondition.AllowDrop = true;
            this.txtActivateCondition.Location = new System.Drawing.Point(220, 523);
            this.txtActivateCondition.Multiline = true;
            this.txtActivateCondition.Name = "txtActivateCondition";
            this.txtActivateCondition.Size = new System.Drawing.Size(788, 133);
            this.txtActivateCondition.TabIndex = 110;
            this.txtActivateCondition.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtActivateCondition.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtActivateCondition.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(78, 523);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 17);
            this.label9.TabIndex = 109;
            this.label9.Text = "Activate Condition:";
            this.label9.UseMnemonic = false;
            // 
            // MinorComponentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1448, 882);
            this.Controls.Add(this.txtActivateCondition);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblDescriptionId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDescriptionCalculation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblDetailId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lstBoomSpecs);
            this.Controls.Add(this.txtDetailCalculation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ddlOrder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtMinorComponentName);
            this.Controls.Add(this.label3);
            this.Name = "MinorComponentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Quotation System";
            ((System.ComponentModel.ISupportInitialize)(this.emptyTextErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ListBox lstBoomSpecs;
        private System.Windows.Forms.TextBox txtDetailCalculation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ddlOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtMinorComponentName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ErrorProvider emptyTextErrorProvider;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblDetailId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblDescriptionId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDescriptionCalculation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtActivateCondition;
        private System.Windows.Forms.Label label9;
    }
}