﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Windows.Forms;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Services;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class DistributorsForm : Form
    {
        public delegate void SaveFinished();

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly DistributorService distributorService;

        private Distributor _CurrentDistributor;

        public Distributor CurrentDistributor
        {
            get { return _CurrentDistributor; }
            set
            {
                _CurrentDistributor = value;
                if (_CurrentDistributor != null)
                {
                    txtName.Text = _CurrentDistributor.Name;
                    txtPriceLevel.Text = _CurrentDistributor.PriceLevel.ToString();

                    btnUpdate.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    btnUpdate.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public DistributorsForm()
        {
            InitializeComponent();

            _context = new DataContext();

            distributorService = new DistributorService(_context);
        }

        public async Task Initialize()
        {
            var distributorList = await distributorService.Get().OrderedByName().ToListAsync();

            ddlDistributors.DataSource = distributorList;
            ddlDistributors.DisplayMember = "Name";
            ddlDistributors.SelectedIndex = -1;
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            Distributor distributor = new Distributor();
            bool isValidDistributor = ValidateAndPopulateDistributor(distributor);
            if (isValidDistributor)
            {
                var userCreator = new UserCreator(_context);
                var user = await userCreator.CreateDistributorUser(distributor);

                if (user != null)
                {
                    await Initialize();
                    ddlDistributors.SelectedItem = distributor;
                    OnSaveFinished();
                    MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("There was an error creating the user", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                }
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidDistributor = ValidateAndPopulateDistributor(CurrentDistributor);
            if (isValidDistributor)
            {
                await _context.SaveChangesAsync();

                var currentRepIndex = ddlDistributors.SelectedIndex;
                await Initialize();
                ddlDistributors.SelectedIndex = currentRepIndex;
                OnSaveFinished();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private bool ValidateAndPopulateDistributor(Distributor distributor)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(distributor);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(Distributor distributor)
        {
            distributor.Name = txtName.Text.Trim();
            distributor.PriceLevel = Convert.ToDouble(txtPriceLevel.Text);
        }

        private void mnNewDistributor_Click(object sender, EventArgs e)
        {
            CurrentDistributor = null;
        }

        private void ddlDistributors_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedDistributor = ddlDistributors.SelectedValue as Distributor;
            CurrentDistributor = selectedDistributor;
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DistributorsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _context.Dispose();
        }
    }
}