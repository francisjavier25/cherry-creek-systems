﻿namespace CherryCreekSystems
{
    partial class DetailQuoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnCustomerInfo = new System.Windows.Forms.Panel();
            this.lblSystemLength = new System.Windows.Forms.Label();
            this.lblNumberOfBays = new System.Windows.Forms.Label();
            this.lblSystemWidth = new System.Windows.Forms.Label();
            this.lblQuoteNumber = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.pnComponents = new System.Windows.Forms.Panel();
            this.btnGenerateDetailedReport = new System.Windows.Forms.Button();
            this.txtSystemName = new System.Windows.Forms.TextBox();
            this.txtSystemDetailCalculationResult = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtSystemCost = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.chkOverrideSystemComponent = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnGenerateBuildDocument = new System.Windows.Forms.Button();
            this.pnCustomerInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSystemCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantityOfSystems)).BeginInit();
            this.SuspendLayout();
            // 
            // pnCustomerInfo
            // 
            this.pnCustomerInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnCustomerInfo.Controls.Add(this.lblSystemLength);
            this.pnCustomerInfo.Controls.Add(this.lblNumberOfBays);
            this.pnCustomerInfo.Controls.Add(this.lblSystemWidth);
            this.pnCustomerInfo.Controls.Add(this.lblQuoteNumber);
            this.pnCustomerInfo.Controls.Add(this.label12);
            this.pnCustomerInfo.Controls.Add(this.label13);
            this.pnCustomerInfo.Controls.Add(this.label14);
            this.pnCustomerInfo.Controls.Add(this.label15);
            this.pnCustomerInfo.Location = new System.Drawing.Point(34, 16);
            this.pnCustomerInfo.Margin = new System.Windows.Forms.Padding(4);
            this.pnCustomerInfo.Name = "pnCustomerInfo";
            this.pnCustomerInfo.Size = new System.Drawing.Size(1177, 53);
            this.pnCustomerInfo.TabIndex = 1;
            // 
            // lblSystemLength
            // 
            this.lblSystemLength.AutoSize = true;
            this.lblSystemLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSystemLength.Location = new System.Drawing.Point(692, 21);
            this.lblSystemLength.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSystemLength.Name = "lblSystemLength";
            this.lblSystemLength.Size = new System.Drawing.Size(0, 13);
            this.lblSystemLength.TabIndex = 21;
            // 
            // lblNumberOfBays
            // 
            this.lblNumberOfBays.AutoSize = true;
            this.lblNumberOfBays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberOfBays.Location = new System.Drawing.Point(458, 21);
            this.lblNumberOfBays.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfBays.Name = "lblNumberOfBays";
            this.lblNumberOfBays.Size = new System.Drawing.Size(0, 13);
            this.lblNumberOfBays.TabIndex = 18;
            // 
            // lblSystemWidth
            // 
            this.lblSystemWidth.AutoSize = true;
            this.lblSystemWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSystemWidth.Location = new System.Drawing.Point(943, 21);
            this.lblSystemWidth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSystemWidth.Name = "lblSystemWidth";
            this.lblSystemWidth.Size = new System.Drawing.Size(0, 13);
            this.lblSystemWidth.TabIndex = 17;
            // 
            // lblQuoteNumber
            // 
            this.lblQuoteNumber.AutoSize = true;
            this.lblQuoteNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuoteNumber.Location = new System.Drawing.Point(99, 21);
            this.lblQuoteNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuoteNumber.Name = "lblQuoteNumber";
            this.lblQuoteNumber.Size = new System.Drawing.Size(0, 13);
            this.lblQuoteNumber.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(330, 21);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Number of Bays:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(787, 21);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(125, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "System Width (Feet):";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(536, 21);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "System Length (Feet):";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(38, 21);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "Quote #:";
            // 
            // pnComponents
            // 
            this.pnComponents.AutoScroll = true;
            this.pnComponents.Location = new System.Drawing.Point(34, 138);
            this.pnComponents.Margin = new System.Windows.Forms.Padding(4);
            this.pnComponents.Name = "pnComponents";
            this.pnComponents.Size = new System.Drawing.Size(1177, 621);
            this.pnComponents.TabIndex = 2;
            // 
            // btnGenerateDetailedReport
            // 
            this.btnGenerateDetailedReport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerateDetailedReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateDetailedReport.Location = new System.Drawing.Point(350, 800);
            this.btnGenerateDetailedReport.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateDetailedReport.Name = "btnGenerateDetailedReport";
            this.btnGenerateDetailedReport.Size = new System.Drawing.Size(228, 60);
            this.btnGenerateDetailedReport.TabIndex = 3;
            this.btnGenerateDetailedReport.Text = "Generate Report";
            this.btnGenerateDetailedReport.UseVisualStyleBackColor = true;
            this.btnGenerateDetailedReport.Click += new System.EventHandler(this.btnGenerateDetailedReport_Click);
            // 
            // txtSystemName
            // 
            this.txtSystemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSystemName.Location = new System.Drawing.Point(350, 101);
            this.txtSystemName.Margin = new System.Windows.Forms.Padding(4);
            this.txtSystemName.Name = "txtSystemName";
            this.txtSystemName.Size = new System.Drawing.Size(426, 29);
            this.txtSystemName.TabIndex = 4;
            // 
            // txtSystemDetailCalculationResult
            // 
            this.txtSystemDetailCalculationResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSystemDetailCalculationResult.Location = new System.Drawing.Point(266, 102);
            this.txtSystemDetailCalculationResult.Margin = new System.Windows.Forms.Padding(4);
            this.txtSystemDetailCalculationResult.Name = "txtSystemDetailCalculationResult";
            this.txtSystemDetailCalculationResult.Size = new System.Drawing.Size(61, 29);
            this.txtSystemDetailCalculationResult.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(34, 800);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(228, 60);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(982, 800);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(228, 60);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtSystemCost
            // 
            this.txtSystemCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.txtSystemCost.Location = new System.Drawing.Point(859, 101);
            this.txtSystemCost.Margin = new System.Windows.Forms.Padding(4);
            this.txtSystemCost.Mask = "N2";
            this.txtSystemCost.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSystemCost.Name = "txtSystemCost";
            this.txtSystemCost.Size = new System.Drawing.Size(133, 27);
            this.txtSystemCost.TabIndex = 76;
            this.txtSystemCost.TabStop = false;
            this.txtSystemCost.Text = "0.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(831, 103);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 24);
            this.label6.TabIndex = 22;
            this.label6.Text = "$";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1000, 102);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 24);
            this.label7.TabIndex = 77;
            this.label7.Text = "Quantity:";
            // 
            // txtQuantityOfSystems
            // 
            this.txtQuantityOfSystems.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.txtQuantityOfSystems.Location = new System.Drawing.Point(1091, 101);
            this.txtQuantityOfSystems.Margin = new System.Windows.Forms.Padding(4);
            this.txtQuantityOfSystems.Mask = "G";
            this.txtQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtQuantityOfSystems.Name = "txtQuantityOfSystems";
            this.txtQuantityOfSystems.Size = new System.Drawing.Size(72, 27);
            this.txtQuantityOfSystems.TabIndex = 78;
            this.txtQuantityOfSystems.TabStop = false;
            this.txtQuantityOfSystems.Text = "0";
            // 
            // chkOverrideSystemComponent
            // 
            this.chkOverrideSystemComponent.Location = new System.Drawing.Point(64, 112);
            this.chkOverrideSystemComponent.Name = "chkOverrideSystemComponent";
            this.chkOverrideSystemComponent.Size = new System.Drawing.Size(21, 17);
            this.chkOverrideSystemComponent.TabIndex = 79;
            this.chkOverrideSystemComponent.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(41, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 32);
            this.label8.TabIndex = 80;
            this.label8.Text = "Override Component?";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(117, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 32);
            this.label9.TabIndex = 82;
            this.label9.Text = "Include in Build Document?";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnGenerateBuildDocument
            // 
            this.btnGenerateBuildDocument.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerateBuildDocument.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateBuildDocument.Location = new System.Drawing.Point(666, 800);
            this.btnGenerateBuildDocument.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateBuildDocument.Name = "btnGenerateBuildDocument";
            this.btnGenerateBuildDocument.Size = new System.Drawing.Size(228, 60);
            this.btnGenerateBuildDocument.TabIndex = 83;
            this.btnGenerateBuildDocument.Text = "Generate Build Document";
            this.btnGenerateBuildDocument.UseVisualStyleBackColor = true;
            this.btnGenerateBuildDocument.Click += new System.EventHandler(this.btnGenerateBuildDocument_Click);
            // 
            // DetailQuoteForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1250, 873);
            this.Controls.Add(this.btnGenerateBuildDocument);
            this.Controls.Add(this.pnCustomerInfo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.chkOverrideSystemComponent);
            this.Controls.Add(this.txtQuantityOfSystems);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtSystemCost);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtSystemDetailCalculationResult);
            this.Controls.Add(this.txtSystemName);
            this.Controls.Add(this.btnGenerateDetailedReport);
            this.Controls.Add(this.pnComponents);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DetailQuoteForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quotation System";
            this.Load += new System.EventHandler(this.DetailQuoteForm_Load);
            this.pnCustomerInfo.ResumeLayout(false);
            this.pnCustomerInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSystemCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantityOfSystems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnCustomerInfo;
        private System.Windows.Forms.Label lblNumberOfBays;
        private System.Windows.Forms.Label lblSystemWidth;
        private System.Windows.Forms.Label lblQuoteNumber;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblSystemLength;
        private System.Windows.Forms.Panel pnComponents;
        private System.Windows.Forms.Button btnGenerateDetailedReport;
        private System.Windows.Forms.TextBox txtSystemName;
        private System.Windows.Forms.TextBox txtSystemDetailCalculationResult;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSystemCost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadMaskedEditBox txtQuantityOfSystems;
        private System.Windows.Forms.CheckBox chkOverrideSystemComponent;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnGenerateBuildDocument;
    }
}