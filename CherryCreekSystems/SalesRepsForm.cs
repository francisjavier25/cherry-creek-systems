﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.Services;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CherryCreekSystems.BL.QueryExtensions;

namespace CherryCreekSystems
{
    public partial class SalesRepsForm : Form
    {
        public delegate void SaveFinished();

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly SalesRepService salesRepService;

        private SalesRep _currentSalesRep;

        public SalesRep CurrentSalesRep
        {
            get { return _currentSalesRep; }
            set
            {
                _currentSalesRep = value;
                if (_currentSalesRep != null)
                {
                    txtName.Text = _currentSalesRep.Name;
                    txtPhone.Text = _currentSalesRep.Phone;

                    btnUpdate.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    btnUpdate.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public SalesRepsForm()
        {
            InitializeComponent();

            _context = new DataContext();

            salesRepService = new SalesRepService(_context);
        }

        public async Task Initialize()
        {
            var salesRepList = await salesRepService.Get().OrderedByName().ToListAsync();

            ddlSalesReps.DataSource = salesRepList;
            ddlSalesReps.DisplayMember = "Name";
            ddlSalesReps.SelectedIndex = -1;
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            SalesRep salesRep = new SalesRep();
            bool isValidSalesRep = ValidateAndPopulateSalesRep(salesRep);
            if (isValidSalesRep)
            {
                var userCreator = new UserCreator(_context);
                var user = await userCreator.CreateSalesRepUser(salesRep);

                if (user != null)
                {
                    await Initialize();
                    ddlSalesReps.SelectedItem = salesRep;
                    OnSaveFinished();
                    MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("There was an error creating the user", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                }
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidSalesRep = ValidateAndPopulateSalesRep(CurrentSalesRep);
            if (isValidSalesRep)
            {
                await _context.SaveChangesAsync();

                var currentRepIndex = ddlSalesReps.SelectedIndex;
                await Initialize();
                ddlSalesReps.SelectedIndex = currentRepIndex;
                OnSaveFinished();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private bool ValidateAndPopulateSalesRep(SalesRep salesRep)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(salesRep);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(SalesRep salesRep)
        {
            salesRep.Name = txtName.Text.Trim();
            salesRep.Phone = txtPhone.Text.Trim();
        }

        private void mnNewSalesRep_Click(object sender, EventArgs e)
        {
            CurrentSalesRep = null;
        }

        private void ddlSalesReps_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedSalesRep = ddlSalesReps.SelectedValue as SalesRep;
            CurrentSalesRep = selectedSalesRep;
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SalesRepsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _context.Dispose();
        }
    }
}