﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class MajorComponentDetailForm : Form
    {
        public delegate void SaveFinished(bool isDelete);

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly MajorComponentDetailService majorComponentDetailService;

        //private readonly int BoomTypeId;
        private readonly MajorComponent MajorComponent;

        private readonly int MajorComponentDetailsCount;
        private readonly int NewMajorComponentDetailOrder;

        private MajorComponentDetail _currentMajorComponentDetail;

        public MajorComponentDetail CurrentMajorComponentDetail
        {
            get { return _currentMajorComponentDetail; }
            set
            {
                _currentMajorComponentDetail = value;
                if (_currentMajorComponentDetail != null)
                {
                    txtMajorComponentDetailName.Text = _currentMajorComponentDetail.Name;
                    //txtDetailCalculation.Text = _currentMajorComponentDetail.DetailCalculation;
                    //txtAddPriceCondition.Text = _currentMajorComponentDetail.AddPriceCondition;
                    //txtAdditionalPrice.Text = _currentMajorComponentDetail.Price.ToString();
                    ddlOrder.DataSource = Enumerable.Range(1, MajorComponentDetailsCount).ToList();
                    ddlOrder.SelectedItem = _currentMajorComponentDetail.Order;

                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    ddlOrder.DataSource = Enumerable.Range(1, NewMajorComponentDetailOrder).ToList();
                    ddlOrder.SelectedItem = NewMajorComponentDetailOrder;

                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public MajorComponentDetailForm(DataContext context, MajorComponentDetail majorComponentDetail, MajorComponent majorComponent, int majorComponentDetailsCount)
        {
            InitializeComponent();

            _context = context;
            MajorComponent = majorComponent;
            MajorComponentDetailsCount = majorComponentDetailsCount;
            NewMajorComponentDetailOrder = majorComponentDetailsCount + 1;
            CurrentMajorComponentDetail = majorComponentDetail;

            majorComponentDetailService = new MajorComponentDetailService(_context);
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            MajorComponentDetail majorComponentDetail = new MajorComponentDetail();
            bool isValidMajorComponentDetail = ValidateAndPopulateMajorComponentDetail(majorComponentDetail);
            if (isValidMajorComponentDetail)
            {
                majorComponentDetailService.Add(majorComponentDetail);
                await _context.SaveChangesAsync();

                majorComponentDetail.ComponentID = "MAD" + majorComponentDetail.Id;
                majorComponentDetail.DetailCalculationID = "MADD" + majorComponentDetail.Id;
                majorComponentDetail.DescriptionCalculationID = "MADT" + majorComponentDetail.Id;
                majorComponentDetail.PriceCalculationID = "MADP" + majorComponentDetail.Id;
                await _context.SaveChangesAsync();

                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidMajorComponentDetail = ValidateAndPopulateMajorComponentDetail(CurrentMajorComponentDetail);
            if (isValidMajorComponentDetail)
            {
                await _context.SaveChangesAsync();
                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            //majorComponentDetailService.Remove(CurrentMajorComponentDetail);
            CurrentMajorComponentDetail.IsActive = false;
            await _context.SaveChangesAsync();
            OnSaveFinished(isDelete: true);
            MessageBox.Show("Major Component Detail has been removed", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            Close();
        }

        private bool ValidateAndPopulateMajorComponentDetail(MajorComponentDetail majorComponentDetail)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(majorComponentDetail);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(MajorComponentDetail majorComponentDetail)
        {
            majorComponentDetail.Name = txtMajorComponentDetailName.Text.Trim();
            majorComponentDetail.Order = Convert.ToInt32(ddlOrder.SelectedItem);
            majorComponentDetail.MajorComponentId = MajorComponent.Id;
            majorComponentDetail.IsActive = true;
            //majorComponentDetail.Price = Convert.ToDouble(txtAdditionalPrice.Text);
        }

        private void ValidateTextboxHasValue(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}