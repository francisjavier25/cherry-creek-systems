﻿using System.Windows.Forms;

namespace CherryCreekSystems
{
    public partial class LoadingForm : Form
    {
        public string Text
        {
            set
            {
                lblText.Text = value;
            }
        }

        public LoadingForm()
        {
            InitializeComponent();
        }

        public new void Show(string text)
        {
            Text = text;
            base.Show();
        }
    }
}