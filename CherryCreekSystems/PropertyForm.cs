﻿using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace CherryCreekSystems
{
    public partial class PropertyForm : Form
    {
        public delegate void ValueEntered(object value, string targetObject, string targetField);

        public ValueEntered OnValueEntered;

        private Type DataType;
        private string TargetObject;
        private string TargetField;
        private object CurrentValue;

        public PropertyForm(Type propertyType, string targetObject, string targetField, object currentValue = null)
        {
            InitializeComponent();

            DataType = propertyType;
            TargetObject = targetObject;
            TargetField = targetField;
            CurrentValue = currentValue;

            displayProperControlAndValue();
        }

        private void displayProperControlAndValue()
        {
            if (DataType == typeof(double))
            {
                txtDoubleValue.Visible = true;
                if (CurrentValue != null)
                {
                    txtDoubleValue.Text = Convert.ToDouble(CurrentValue).ToString();
                }
            }
            else if (DataType == typeof(int))
            {
                txtIntValue.Visible = true;
                if (CurrentValue != null)
                {
                    txtIntValue.Text = Convert.ToInt32(CurrentValue).ToString();
                }
            }
            else if (DataType == typeof(bool))
            {
                pnlValue.Visible = true;
                if (CurrentValue != null)
                {
                    var radioButton = Controls.Find(string.Format("rd{0}", CurrentValue), true)
                                                      .First() as RadioButton;
                    radioButton.Checked = true;
                }
            }
            else if (DataType == typeof(string))
            {
                txtStringValue.Visible = true;
                if (CurrentValue != null)
                {
                    txtStringValue.Text = CurrentValue.ToString();
                }
            }
        }

        private void btnSubmitValue_Click(object sender, EventArgs e)
        {
            var isValid = ValidateChildren();
            if (isValid)
            {
                if (DataType == typeof(double))
                {
                    var doubleValue = Convert.ToDouble(txtDoubleValue.Text);
                    OnValueEntered(doubleValue, TargetObject, TargetField);
                }
                else if (DataType == typeof(int))
                {
                    var intValue = Convert.ToInt32(txtIntValue.Text);
                    OnValueEntered(intValue, TargetObject, TargetField);
                }
                else if (DataType == typeof(bool))
                {
                    var boolValue = ControlUtilities.GetSelectedRadioButtonValueInPanel(pnlValue);
                    OnValueEntered(boolValue.ToBoolean(), TargetObject, TargetField);
                }
                else if (DataType == typeof(string))
                {
                    var stringValue = txtStringValue.Text;
                    OnValueEntered(stringValue, TargetObject, TargetField);
                }
            }

            this.Close();
        }

        private void ValidateGreaterThanZero(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var textBox = sender as RadMaskedEditBox;
            if (!textBox.Visible)
            {
                greaterThanZeroErrorProvider.SetError(textBox, "");
            }
            else
            {
                ValidationUtilities.ValidateGreaterThanZero(sender, greaterThanZeroErrorProvider, e);
            }
        }

        private void ValidateRadioOptionSelected(object sender, CancelEventArgs e)
        {
            var radioButton = sender as RadioButton;
            if (!radioButton.Visible)
            {
                radioButtonsErrorProvider.SetError(radioButton, "");
            }
            else
            {
                ValidationUtilities.ValidateRadioOptionSelected(radioButton, radioButtonsErrorProvider, e);
            }
        }
    }
}