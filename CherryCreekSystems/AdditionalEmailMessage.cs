﻿using System;
using System.Windows.Forms;

namespace CherryCreekSystems
{
    public partial class AdditionalEmailMessageForm : Form
    {
        public string AdditionalMessage { get; set; }

        public AdditionalEmailMessageForm()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            AdditionalMessage = txtAdditionalMessage.Text;
            Close();
        }
    }
}