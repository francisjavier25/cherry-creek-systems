﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class GreenHousesForm : Form
    {
        public delegate void SaveFinished();

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly GreenhouseTypeService greenHouseTypeService;

        private GreenHouseType _CurrentGreenHouseType;

        public GreenHouseType CurrentGreenHouseType
        {
            get { return _CurrentGreenHouseType; }
            set
            {
                _CurrentGreenHouseType = value;
                if (_CurrentGreenHouseType != null)
                {
                    txtName.Text = _CurrentGreenHouseType.Name;

                    btnUpdate.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    btnUpdate.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public GreenHousesForm()
        {
            InitializeComponent();

            _context = new DataContext();

            greenHouseTypeService = new GreenhouseTypeService(_context);
        }

        public async Task Initialize()
        {
            var greenHouseTypeList = await greenHouseTypeService.Get().OrderedByName().ToListAsync();
            ddlGreenHouseTypes.DataSource = greenHouseTypeList;
            ddlGreenHouseTypes.DisplayMember = "Name";
            ddlGreenHouseTypes.SelectedIndex = -1;
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            var greenHouse = new GreenHouseType();
            bool isValidDistributor = ValidateAndPopulateGreenHouseType(greenHouse);
            if (isValidDistributor)
            {
                greenHouseTypeService.Add(greenHouse);
                await _context.SaveChangesAsync();

                await Initialize();
                ddlGreenHouseTypes.SelectedItem = greenHouse;
                OnSaveFinished();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidDistributor = ValidateAndPopulateGreenHouseType(CurrentGreenHouseType);
            if (isValidDistributor)
            {
                await _context.SaveChangesAsync();

                var currentRepIndex = ddlGreenHouseTypes.SelectedIndex;
                await Initialize();
                ddlGreenHouseTypes.SelectedIndex = currentRepIndex;
                OnSaveFinished();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private bool ValidateAndPopulateGreenHouseType(GreenHouseType greenHouse)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(greenHouse);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(GreenHouseType greenHouse)
        {
            greenHouse.Name = txtName.Text.Trim();
        }

        private void mnNewGreenHouse_Click(object sender, EventArgs e)
        {
            CurrentGreenHouseType = null;
        }

        private void ddlGreenHouses_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedGreenHouse = ddlGreenHouseTypes.SelectedValue as GreenHouseType;
            CurrentGreenHouseType = selectedGreenHouse;
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GreenHouseTypesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _context.Dispose();
        }
    }
}