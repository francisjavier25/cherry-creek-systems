﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using JR.Utils.GUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CherryCreekSystems
{
    public partial class SearchQuoteForm : Form
    {
        private readonly DataContext _context;

        private readonly CustomerService customerService;
        private readonly SalesRepService salesRepService;
        private readonly QuoteStatusService quoteStatusService;

        private string CurrentQuoteNumber;
        private bool _isPerformingOperation;

        private LoadingForm LoadingForm;

        public SearchQuoteForm(string currentQuoteNumber)
        {
            InitializeComponent();

            _context = new DataContext();

            CurrentQuoteNumber = currentQuoteNumber;

            customerService = new CustomerService();
            salesRepService = new SalesRepService();
            quoteStatusService = new QuoteStatusService();

            setStartAndEndDate();

            LoadingForm = new LoadingForm();
        }

        // This event handler manually raises the CellValueChanged event
        // by calling the CommitEdit method.
        private void gvQuotes_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (gvQuotes.IsCurrentCellDirty)
            {
                // This fires the cell value changed handler below
                gvQuotes.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private async void gvQuotes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var quoteRequestService = new QuoteRequestService();

            // My combobox column is the second one so I hard coded a 1, flavor to taste
            if (e.RowIndex < 0) return;

            //Save a change in the status dropdown
            var cb = (DataGridViewComboBoxCell)gvQuotes.Rows[e.RowIndex].Cells[4];
            if (cb.Value != null)
            {
                var currentQuote = getSelectedQuoteViewModel();
                var quoteFromDatabase = await quoteRequestService.Get().ByQuoteId(currentQuote.Id);
                quoteFromDatabase.QuoteStatusId = (int)cb.Value;
                await quoteRequestService.SaveChangesAsync();

                BeginInvoke((MethodInvoker)delegate
                {
                    FlexibleMessageBox.Show("Quote Status has been changed", "Quotation System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                });

                gvQuotes.Invalidate();
            }
        }

        #region make dropdowns open on first click

        private void gvQuotes_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //ComboBox ctl = e.Control as ComboBox;
            //if (ctl == null) return;
            //ctl.Enter -= ctl_Enter;

            //if (gvQuotes.CurrentCell.RowIndex > 0)
            //{
            //    ctl.Enter += ctl_Enter;
            //}
        }

        //void ctl_Enter(object sender, EventArgs e)
        //{
        //    var dropDown = sender as ComboBox;
        //    if (dropDown == null) return;

        //    dropDown.DroppedDown = true;
        //}

        #endregion make dropdowns open on first click

        #region set up search fields

        private void setStartAndEndDate()
        {
            dtStart.Value = DateTime.Now.AddDays(-30);
            dtEnd.Value = DateTime.Now;
        }

        public async Task Initialize()
        {
            var initializeSalesRepList = bindSalesRepList();
            var initializeCustomersList = bindCustomerList();
            var initializeQuoteStatusesList = BindQuoteStatusListAndColumn();

            bindBoomTypesList();
            await initializeSalesRepList;
            await initializeCustomersList;
            await initializeQuoteStatusesList;
        }

        private async Task bindSalesRepList()
        {
            var salesRepList = await salesRepService.Get().OrderedByName().ToListAsync();
            salesRepList.Insert(0, new SalesRep { Name = "Select All" });

            ddlSalesReps.DisplayMember = "Name";
            ddlSalesReps.DataSource = salesRepList;
            ddlSalesReps.SelectedIndex = 0;
        }

        private async Task bindCustomerList()
        {
            var customerList = await customerService.Get().OrderedByCompanyName().ToListAsync();
            customerList.Insert(0, new Customer { CompanyName = "Select All" });

            ddlCustomers.DisplayMember = "CompanyName";
            ddlCustomers.DataSource = customerList;
            ddlCustomers.SelectedIndex = 0;
        }

        private void bindBoomTypesList()
        {
            var boomTypeList = BoomTypeStructure.GetList().ToList();
            boomTypeList.Insert(0, new BoomTypeStructure { Name = "Select All" });

            ddlBoomTypes.DisplayMember = "Name";
            ddlBoomTypes.DataSource = boomTypeList;
            ddlBoomTypes.SelectedIndex = 0;
        }

        private async Task BindQuoteStatusListAndColumn()
        {
            var quoteStatusList = await quoteStatusService.Get().ToListAsync();
            var quoteStatusColumn = quoteStatusList.ToList();
            quoteStatusList.Insert(0, new QuoteStatus { Name = "Select All" });

            ddlQuoteStatus.DisplayMember = "Name";
            ddlQuoteStatus.DataSource = quoteStatusList;
            ddlQuoteStatus.SelectedIndex = 0;

            var statusColumn = (DataGridViewComboBoxColumn)gvQuotes.Columns[4];
            statusColumn.ValueMember = "Id";
            statusColumn.DisplayMember = "Name";
            statusColumn.FlatStyle = FlatStyle.Flat;
            statusColumn.DataSource = quoteStatusColumn;
        }

        #endregion set up search fields

        private async void PerformSearch(object sender, EventArgs args)
        {
            _isPerformingOperation = true;
            await SearchQuotes(showLoadingForm: true);
            _isPerformingOperation = false;
        }

        private async Task SearchQuotes(bool showLoadingForm = false, string loadingFormMessage = "Searching Quotes...")
        {
            var quoteRequestService = new QuoteRequestService();

            var customer = ddlCustomers.SelectedValue as Customer;
            var salesRep = ddlSalesReps.SelectedValue as SalesRep;
            var status = ddlQuoteStatus.SelectedValue as QuoteStatus;
            var boomType = ddlBoomTypes.SelectedValue as BoomTypeStructure;

            var quoteNumber = txtQuoteNumber.Text.Replace("_", "");
            var customerId = customer != null && customer.CompanyName.ToLower() != "select all" ? customer.Id : (int?)null;
            var salesRepId = salesRep != null && salesRep.Name.ToLower() != "select all" ? salesRep.Id : (int?)null;
            var statusId = status != null && status.Name.ToLower() != "select all" ? status.Id : (int?)null;
            var boomTypeId = boomType != null && boomType.Name.ToLower() != "select all" ? boomType.Id : (int?)null;

            var date1 = dtStart.Value;
            var date2 = dtEnd.Value;

            var startDate = new DateTime(date1.Year, date1.Month, date1.Day, 0, 0, 0);
            var endDate = new DateTime(date2.Year, date2.Month, date2.Day, 23, 59, 59);

            if (showLoadingForm)
            {
                LoadingForm.Show(loadingFormMessage);
            }

            IQueryable<QuoteRequest> query;
            if (boomTypeId.HasValue)
            {
                query = quoteRequestService.GetByBoomType(boomTypeId.Value);
            }
            else 
            {
                query = quoteRequestService.Get();
            }

            var results = await query.Where(c => c.QuoteNumber != CurrentQuoteNumber)
                                     .ByQuoteNumber(quoteNumber)
                                     .BySalesRep(salesRepId)
                                     .ByCustomer(customerId)
                                     .ByQuoteStatus(statusId)
                                     .ByStatusDate(startDate, endDate)
                                     .WhereActive()
                                     .ToListAsync();

            await quoteRequestService.FillChildObjects(quoteRequests: results.ToArray(), customer: true, salesRep: true);

            var datasource = results.Select
                (r => new QuoteRequestViewModel
                {
                    Id = r.Id,
                    QuoteNumber = r.QuoteNumberWithRevision,
                    CustomerName = r.Customer.CompanyName,
                    SalesRepName = r.SalesRep.Name,
                    DateCreated = r.StatusDate.ToShortDateString(),
                    QuoteStatus = r.QuoteStatusId,
                    TotalCost = r.GetTotalCost()
                }).ToList();

            var sortedDataSource = new SortableBindingList<QuoteRequestViewModel>(datasource);

            gvQuotes.AutoGenerateColumns = false;
            gvQuotes.DataSource = sortedDataSource;

            if (showLoadingForm)
            {
                LoadingForm.Hide();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gvQuotes_DoubleCellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex <= -1) return;

            var quote = getSelectedQuoteViewModel();
            openQuoteRequestForm(quote.Id);
        }

        private async void gvQuotes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 6) return;

            var quoteRequestService = new QuoteRequestService();

            _isPerformingOperation = true;
            if (e.ColumnIndex == 6)
            {
                try
                {
                    var quote = getSelectedQuoteViewModel();
                    LoadingForm.Show("Copying Quote...");
                    btnSearch.Enabled = false;
                    btnClose.Enabled = false;
                    gvQuotes.Enabled = false;

                    var fullQuoteForCopy = await quoteRequestService.Get().ByQuoteId(quote.Id);
                    var copyQuoteId = await quoteRequestService.Copy(fullQuoteForCopy);

                    LoadingForm.Invoke((MethodInvoker)delegate
                    {
                        LoadingForm.Hide();
                    });

                    await SearchQuotes(showLoadingForm: true, loadingFormMessage: "Reloading Quotes...");
                    btnSearch.Enabled = true;
                    btnClose.Enabled = true;
                    gvQuotes.Enabled = true;
                    openQuoteRequestForm(copyQuoteId);
                }
                catch (Exception ex)
                {
                }
            }
            else if (e.ColumnIndex == 7)
            {
                try
                {
                    var quote = getSelectedQuoteViewModel();
                    LoadingForm.Show("Removing Quote...");
                    btnSearch.Enabled = false;
                    btnClose.Enabled = false;
                    gvQuotes.Enabled = false;

                    var fullQuote = await quoteRequestService.Get().ByQuoteId(quote.Id);
                    fullQuote.Active = false;
                    await quoteRequestService.SaveChangesAsync();

                    LoadingForm.Invoke((MethodInvoker)delegate
                    {
                        LoadingForm.Hide();
                    });
                    await SearchQuotes(showLoadingForm: true, loadingFormMessage: "Reloading Quotes...");
                    btnSearch.Enabled = true;
                    btnClose.Enabled = true;
                    gvQuotes.Enabled = true;
                }
                catch (Exception ex)
                {
                }
            }
            _isPerformingOperation = false;
        }

        private void openQuoteRequestForm(int quoteId)
        {
            var quoteRequestForm = new QuoteRequestForm(quoteId: quoteId, isMainScreen: false);
            quoteRequestForm.OnSaveFinished += OnSaveFinished;
            quoteRequestForm.ShowDialog();
        }

        private async void OnSaveFinished(QuoteRequest quote, bool showLoadingForm, string loadingFormMessage)
        {
            var quoteRequestService = new QuoteRequestService();

            var dataSource = gvQuotes.DataSource as BindingList<QuoteRequestViewModel>;
            var quoteObject = dataSource.FirstOrDefault(q => q.Id == quote.Id);
            if (quoteObject != null)
            {
                var indexOf = dataSource.IndexOf(quoteObject);
                dataSource.RemoveAt(indexOf);

                var updatedQuote = await quoteRequestService.Get().ByQuoteId(quote.Id);
                await quoteRequestService.FillChildObjects(quoteRequests: updatedQuote,
                    customer: true,
                    salesRep: true,
                    distributor: true);

                var quoteRequestViewModel = new QuoteRequestViewModel
                {
                    Id = updatedQuote.Id,
                    QuoteNumber = updatedQuote.QuoteNumberWithRevision,
                    CustomerName = updatedQuote.Customer.CompanyName,
                    SalesRepName = updatedQuote.SalesRep.Name,
                    DateCreated = updatedQuote.StatusDate.ToShortDateString(),
                    QuoteStatus = updatedQuote.QuoteStatusId,
                    TotalCost = updatedQuote.GetTotalCost()
                };
                dataSource.Insert(indexOf, quoteRequestViewModel);
                gvQuotes.ClearSelection();
                gvQuotes.Rows[indexOf].Selected = true;
            }
        }

        private QuoteRequestViewModel getSelectedQuoteViewModel()
        {
            var row = gvQuotes.CurrentRow;
            var quote = row.DataBoundItem as QuoteRequestViewModel;
            return quote;
        }

        private class QuoteRequestViewModel
        {
            public int Id { get; set; }

            public string QuoteNumber { get; set; }

            public string CustomerName { get; set; }

            public string SalesRepName { get; set; }

            public string DateCreated { get; set; }

            public int QuoteStatus { get; set; }

            public double TotalCost { get; set; }
        }

        private void SearchQuoteForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Prevent disposing the object context immediately when closing the form if its being used
            if (!_isPerformingOperation)
                _context.Dispose();
        }
    }

    /// <summary>
    /// Provides a generic collection that supports data binding and additionally supports sorting.
    /// See http://msdn.microsoft.com/en-us/library/ms993236.aspx
    /// If the elements are IComparable it uses that; otherwise compares the ToString()
    /// </summary>
    /// <typeparam name="T">The type of elements in the list.</typeparam>
    public class SortableBindingList<T> : BindingList<T> where T : class
    {
        private bool _isSorted;
        private ListSortDirection _sortDirection = ListSortDirection.Ascending;
        private PropertyDescriptor _sortProperty;

        /// <summary>
        /// Initializes a new instance of the <see cref="SortableBindingList{T}"/> class.
        /// </summary>
        /// <param name="list">An <see cref="T:System.Collections.Generic.IList`1" /> of items to be contained in the <see cref="T:System.ComponentModel.BindingList`1" />.</param>
        public SortableBindingList(IList<T> list)
            : base(list)
        {
        }

        /// <summary>
        /// Gets a value indicating whether the list supports sorting.
        /// </summary>
        protected override bool SupportsSortingCore
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether the list is sorted.
        /// </summary>
        protected override bool IsSortedCore
        {
            get { return _isSorted; }
        }

        /// <summary>
        /// Gets the direction the list is sorted.
        /// </summary>
        protected override ListSortDirection SortDirectionCore
        {
            get { return _sortDirection; }
        }

        /// <summary>
        /// Gets the property descriptor that is used for sorting the list if sorting is implemented in a derived class; otherwise, returns null
        /// </summary>
        protected override PropertyDescriptor SortPropertyCore
        {
            get { return _sortProperty; }
        }

        /// <summary>
        /// Removes any sort applied with ApplySortCore if sorting is implemented
        /// </summary>
        protected override void RemoveSortCore()
        {
            _sortDirection = ListSortDirection.Ascending;
            _sortProperty = null;
            _isSorted = false; //thanks Luca
        }

        /// <summary>
        /// Sorts the items if overridden in a derived class
        /// </summary>
        /// <param name="prop"></param>
        /// <param name="direction"></param>
        protected override void ApplySortCore(PropertyDescriptor prop, ListSortDirection direction)
        {
            _sortProperty = prop;
            _sortDirection = direction;

            var list = Items as List<T>;
            if (list == null) return;

            list.Sort(Compare);

            _isSorted = true;
            //fire an event that the list has been changed.
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }

        private int Compare(T lhs, T rhs)
        {
            var result = OnComparison(lhs, rhs);
            //invert if descending
            if (_sortDirection == ListSortDirection.Descending)
                result = -result;
            return result;
        }

        private int OnComparison(T lhs, T rhs)
        {
            var lhsValue = lhs == null ? null : _sortProperty.GetValue(lhs);
            var rhsValue = rhs == null ? null : _sortProperty.GetValue(rhs);
            if (lhsValue == null)
            {
                return (rhsValue == null) ? 0 : -1; //nulls are equal
            }
            if (rhsValue == null)
            {
                return 1; //first has value, second doesn't
            }
            if (lhsValue is IComparable)
            {
                return ((IComparable)lhsValue).CompareTo(rhsValue);
            }
            if (lhsValue.Equals(rhsValue))
            {
                return 0; //both are the same
            }
            //not comparable, compare ToString
            return lhsValue.ToString().CompareTo(rhsValue.ToString());
        }
    }
}