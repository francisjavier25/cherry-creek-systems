﻿namespace CherryCreekSystems
{
    partial class QuoteRequestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuoteRequestForm));
            this.ddlCustomers = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ddlSalesReps = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ddlGreenhouseType = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtAdditionalInformation = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbBooms = new System.Windows.Forms.TabControl();
            this.tpEcho = new System.Windows.Forms.TabPage();
            this.pnlEchoWaterSolenoidControl = new System.Windows.Forms.Panel();
            this.rdEchoWaterSolenoidControlNo = new System.Windows.Forms.RadioButton();
            this.rdEchoWaterSolenoidControlYes = new System.Windows.Forms.RadioButton();
            this.label143 = new System.Windows.Forms.Label();
            this.ddlEchoFeed = new System.Windows.Forms.ComboBox();
            this.label135 = new System.Windows.Forms.Label();
            this.pnlEchoEV = new System.Windows.Forms.Panel();
            this.rdEchoEVNo = new System.Windows.Forms.RadioButton();
            this.rdEchoEVYes = new System.Windows.Forms.RadioButton();
            this.label127 = new System.Windows.Forms.Label();
            this.txtEchoQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlEchoRedheadBreakerUpgrade = new System.Windows.Forms.Panel();
            this.rdEchoRedheadBreakerUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdEchoRedheadBreakerUpgradeYes = new System.Windows.Forms.RadioButton();
            this.ddlEchoPulleyBracketSpacing = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.pnlEchoRemotePullChainSwitch = new System.Windows.Forms.Panel();
            this.rdEchoRemotePullChainSwitchNo = new System.Windows.Forms.RadioButton();
            this.rdEchoRemotePullChainSwitchYes = new System.Windows.Forms.RadioButton();
            this.label36 = new System.Windows.Forms.Label();
            this.txtEchoExtensionHangerLength = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.pnlEchoLockline = new System.Windows.Forms.Panel();
            this.rdEchoLocklineNo = new System.Windows.Forms.RadioButton();
            this.rdEchoLocklineYes = new System.Windows.Forms.RadioButton();
            this.label32 = new System.Windows.Forms.Label();
            this.pnlEchoAMIADFilterAssembly = new System.Windows.Forms.Panel();
            this.rdEchoAMIADFilterAssemblyNo = new System.Windows.Forms.RadioButton();
            this.rdEchoAMIADFilterAssemblyYes = new System.Windows.Forms.RadioButton();
            this.label31 = new System.Windows.Forms.Label();
            this.pnlEchoCenterBayWaterStation = new System.Windows.Forms.Panel();
            this.rdEchoCenterBayWaterStationNo = new System.Windows.Forms.RadioButton();
            this.rdEchoCenterBayWaterStationYes = new System.Windows.Forms.RadioButton();
            this.label30 = new System.Windows.Forms.Label();
            this.ddlEchoNumberOfLayers = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.ddlEchoControllerType = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtEchoBasketSpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.ddlEchoDrumSize = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tpSingleRailBoom = new System.Windows.Forms.TabPage();
            this.ddlSRBEVWireGauge = new System.Windows.Forms.ComboBox();
            this.label213 = new System.Windows.Forms.Label();
            this.ddlSRBInjectorType = new System.Windows.Forms.ComboBox();
            this.label151 = new System.Windows.Forms.Label();
            this.pnlSRBAutoInjector = new System.Windows.Forms.Panel();
            this.rdSRBAutoInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdSRBAutoInjectorYes = new System.Windows.Forms.RadioButton();
            this.label152 = new System.Windows.Forms.Label();
            this.pnlSRBWaterSolenoidControl = new System.Windows.Forms.Panel();
            this.rdSRBWaterSolenoidControlNo = new System.Windows.Forms.RadioButton();
            this.rdSRBWaterSolenoidControlYes = new System.Windows.Forms.RadioButton();
            this.label144 = new System.Windows.Forms.Label();
            this.ddlSRBFeed = new System.Windows.Forms.ComboBox();
            this.label136 = new System.Windows.Forms.Label();
            this.pnlSRBEV = new System.Windows.Forms.Panel();
            this.rdSRBEVNo = new System.Windows.Forms.RadioButton();
            this.rdSRBEVYes = new System.Windows.Forms.RadioButton();
            this.label128 = new System.Windows.Forms.Label();
            this.txtSRBTip3 = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.txtSRBTip2 = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.txtSRBTip1 = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.txtSRBNumberOfRows = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label96 = new System.Windows.Forms.Label();
            this.pnlSRBAMIADFilderAssy = new System.Windows.Forms.Panel();
            this.rdSRBAMIADFilderAssyNo = new System.Windows.Forms.RadioButton();
            this.rdSRBAMIADFilderAssyYes = new System.Windows.Forms.RadioButton();
            this.label42 = new System.Windows.Forms.Label();
            this.pnlSRBTripleTurretBody = new System.Windows.Forms.Panel();
            this.rdSRBTripleTurretBodyNo = new System.Windows.Forms.RadioButton();
            this.rdSRBTripleTurretBodyYes = new System.Windows.Forms.RadioButton();
            this.label53 = new System.Windows.Forms.Label();
            this.txtSRBQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlSRBMountedInjector = new System.Windows.Forms.Panel();
            this.rdSRBMountedInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdSRBMountedInjectorYes = new System.Windows.Forms.RadioButton();
            this.label41 = new System.Windows.Forms.Label();
            this.pnlSRBPressureRegulator = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.rdSRBPressureRegulatorNo = new System.Windows.Forms.RadioButton();
            this.rdSRBPressureRegulatorYes = new System.Windows.Forms.RadioButton();
            this.label43 = new System.Windows.Forms.Label();
            this.txtSRBSpraybodySpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label44 = new System.Windows.Forms.Label();
            this.pnlSRBSweep90Degree = new System.Windows.Forms.Panel();
            this.rdSRBSweep90DegreeNo = new System.Windows.Forms.RadioButton();
            this.rdSRBSweep90DegreeYes = new System.Windows.Forms.RadioButton();
            this.label45 = new System.Windows.Forms.Label();
            this.pnlSRBTripleWaterLocklineUpgrade = new System.Windows.Forms.Panel();
            this.rdSRBTripleWaterLocklineUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdSRBTripleWaterLocklineUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label47 = new System.Windows.Forms.Label();
            this.ddlSRBControllerType = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtSRBHoseLoopHeight = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label50 = new System.Windows.Forms.Label();
            this.ddlSRBSingleDualWater = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.tpDoubleRailBoom = new System.Windows.Forms.TabPage();
            this.ddlDRBEVWireGauge = new System.Windows.Forms.ComboBox();
            this.label214 = new System.Windows.Forms.Label();
            this.ddlDRBInjectorType = new System.Windows.Forms.ComboBox();
            this.label150 = new System.Windows.Forms.Label();
            this.pnlDRBAutoInjector = new System.Windows.Forms.Panel();
            this.rdDRBAutoInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdDRBAutoInjectorYes = new System.Windows.Forms.RadioButton();
            this.label149 = new System.Windows.Forms.Label();
            this.pnlDRBWaterSolenoidControl = new System.Windows.Forms.Panel();
            this.rdDRBWaterSolenoidControlNo = new System.Windows.Forms.RadioButton();
            this.rdDRBWaterSolenoidControlYes = new System.Windows.Forms.RadioButton();
            this.label145 = new System.Windows.Forms.Label();
            this.pnlDRBEV = new System.Windows.Forms.Panel();
            this.rdDRBEVNo = new System.Windows.Forms.RadioButton();
            this.rdDRBEVYes = new System.Windows.Forms.RadioButton();
            this.label129 = new System.Windows.Forms.Label();
            this.txtDRBTip3 = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.txtDRBTip2 = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.txtDRBTip1 = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.txtDRBNumberOfRows = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label101 = new System.Windows.Forms.Label();
            this.pnlDRBWirelessWalkSwitch = new System.Windows.Forms.Panel();
            this.rdDRBWirelessWalkSwitchNo = new System.Windows.Forms.RadioButton();
            this.rdDRBWirelessWalkSwitchYes = new System.Windows.Forms.RadioButton();
            this.label64 = new System.Windows.Forms.Label();
            this.txtDRBRailDropDownAssy = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label63 = new System.Windows.Forms.Label();
            this.pnlDRB1_1_4thHoseUpgrade = new System.Windows.Forms.Panel();
            this.rdDRB1_1_4thHoseUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdDRB1_1_4thHoseUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlDRBTripleTurretBody = new System.Windows.Forms.Panel();
            this.rdDRBTripleTurretBodyNo = new System.Windows.Forms.RadioButton();
            this.rdDRBTripleTurretBodyYes = new System.Windows.Forms.RadioButton();
            this.label48 = new System.Windows.Forms.Label();
            this.txtDRBQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlDRBMountedInjector = new System.Windows.Forms.Panel();
            this.rdDRBMountedInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdDRBMountedInjectorYes = new System.Windows.Forms.RadioButton();
            this.label54 = new System.Windows.Forms.Label();
            this.pnlDRBPressureRegulator = new System.Windows.Forms.Panel();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.rdDRBPressureRegulatorNo = new System.Windows.Forms.RadioButton();
            this.rdDRBPressureRegulatorYes = new System.Windows.Forms.RadioButton();
            this.label55 = new System.Windows.Forms.Label();
            this.txtDRBSpraybodySpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label56 = new System.Windows.Forms.Label();
            this.pnlDRBSweep90 = new System.Windows.Forms.Panel();
            this.rdDRBSweep90No = new System.Windows.Forms.RadioButton();
            this.rdDRBSweep90Yes = new System.Windows.Forms.RadioButton();
            this.label57 = new System.Windows.Forms.Label();
            this.pnlDRBTripleWaterLocklineUpgrade = new System.Windows.Forms.Panel();
            this.rdDRBTripleWaterLocklineUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdDRBTripleWaterLocklineUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label58 = new System.Windows.Forms.Label();
            this.ddlDRBControllerType = new System.Windows.Forms.ComboBox();
            this.label59 = new System.Windows.Forms.Label();
            this.txtDRBHoseLoopHeight = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label60 = new System.Windows.Forms.Label();
            this.ddlDRBSingleDualWater = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.tpCWFDoubleRailBoom = new System.Windows.Forms.TabPage();
            this.ddlCWFDRBEVWireGauge = new System.Windows.Forms.ComboBox();
            this.label215 = new System.Windows.Forms.Label();
            this.pnlCWFDRBWaterFeedSystem = new System.Windows.Forms.Panel();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBWaterFeedSystemNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBWaterFeedSystemYes = new System.Windows.Forms.RadioButton();
            this.label160 = new System.Windows.Forms.Label();
            this.pnlCWFDRB1_1_4thHoseUpgrade = new System.Windows.Forms.Panel();
            this.rdCWFDRB1_1_4thHoseUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRB1_1_4thHoseUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label159 = new System.Windows.Forms.Label();
            this.ddlCWFDRBInjectorType = new System.Windows.Forms.ComboBox();
            this.label153 = new System.Windows.Forms.Label();
            this.pnlCWFDRBAutoInjector = new System.Windows.Forms.Panel();
            this.rdCWFDRBAutoInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBAutoInjectorYes = new System.Windows.Forms.RadioButton();
            this.label154 = new System.Windows.Forms.Label();
            this.pnlCWFDRBWaterSolenoidControl = new System.Windows.Forms.Panel();
            this.rdCWFDRBWaterSolenoidControlNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBWaterSolenoidControlYes = new System.Windows.Forms.RadioButton();
            this.label146 = new System.Windows.Forms.Label();
            this.ddlCWFDRBFeed = new System.Windows.Forms.ComboBox();
            this.label137 = new System.Windows.Forms.Label();
            this.pnlCWFDRBEV = new System.Windows.Forms.Panel();
            this.rdCWFDRBEVNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBEVYes = new System.Windows.Forms.RadioButton();
            this.label130 = new System.Windows.Forms.Label();
            this.txtCWFDRBTip3 = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.txtCWFDRBTip2 = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.txtCWFDRBTip1 = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.txtCWFDRBNumberOfRows = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label107 = new System.Windows.Forms.Label();
            this.pnlCWFDRBWirelessWalkSwitch = new System.Windows.Forms.Panel();
            this.rdCWFDRBWirelessWalkSwitchNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBWirelessWalkSwitchYes = new System.Windows.Forms.RadioButton();
            this.label65 = new System.Windows.Forms.Label();
            this.txtCWFDRBRailDropDownAssy = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label66 = new System.Windows.Forms.Label();
            this.pnlCWFDRBTripleTurretBody = new System.Windows.Forms.Panel();
            this.rdCWFDRBTripleTurretBodyNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBTripleTurretBodyYes = new System.Windows.Forms.RadioButton();
            this.label68 = new System.Windows.Forms.Label();
            this.txtCWFDRBQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlCWFDRBMountedInjector = new System.Windows.Forms.Panel();
            this.rdCWFDRBMountedInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBMountedInjectorYes = new System.Windows.Forms.RadioButton();
            this.label69 = new System.Windows.Forms.Label();
            this.pnlCWFDRBPressureRegulator = new System.Windows.Forms.Panel();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBPressureRegulatorNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBPressureRegulatorYes = new System.Windows.Forms.RadioButton();
            this.label70 = new System.Windows.Forms.Label();
            this.txtCWFDRBSpraybodySpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label71 = new System.Windows.Forms.Label();
            this.pnlCWFDRBTripleWaterLocklineUpgrade = new System.Windows.Forms.Panel();
            this.rdCWFDRBTripleWaterLocklineUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdCWFDRBTripleWaterLocklineUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label73 = new System.Windows.Forms.Label();
            this.ddlCWFDRBControllerType = new System.Windows.Forms.ComboBox();
            this.label74 = new System.Windows.Forms.Label();
            this.ddlCWFDRBSingleDualWater = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.tpControlTowerBoom = new System.Windows.Forms.TabPage();
            this.ddlCTEVWireGauge = new System.Windows.Forms.ComboBox();
            this.label216 = new System.Windows.Forms.Label();
            this.ddlCTInjectorType = new System.Windows.Forms.ComboBox();
            this.label155 = new System.Windows.Forms.Label();
            this.pnlCTAutoInjector = new System.Windows.Forms.Panel();
            this.rdCTAutoInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdCTAutoInjectorYes = new System.Windows.Forms.RadioButton();
            this.label156 = new System.Windows.Forms.Label();
            this.pnlCTWaterSolenoidControl = new System.Windows.Forms.Panel();
            this.rdCTWaterSolenoidControlNo = new System.Windows.Forms.RadioButton();
            this.rdCTWaterSolenoidControlYes = new System.Windows.Forms.RadioButton();
            this.label147 = new System.Windows.Forms.Label();
            this.ddlCTFeed = new System.Windows.Forms.ComboBox();
            this.label138 = new System.Windows.Forms.Label();
            this.ddlCTCarrySteel = new System.Windows.Forms.ComboBox();
            this.label72 = new System.Windows.Forms.Label();
            this.pnlCTEV = new System.Windows.Forms.Panel();
            this.rdCTEVNo = new System.Windows.Forms.RadioButton();
            this.rdCTEVYes = new System.Windows.Forms.RadioButton();
            this.label131 = new System.Windows.Forms.Label();
            this.txtCTTip3 = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.txtCTTip2 = new System.Windows.Forms.TextBox();
            this.label122 = new System.Windows.Forms.Label();
            this.txtCTTip1 = new System.Windows.Forms.TextBox();
            this.label123 = new System.Windows.Forms.Label();
            this.txtCTNumberOfRows = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label110 = new System.Windows.Forms.Label();
            this.pnlCTWaterFeedSystem = new System.Windows.Forms.Panel();
            this.radioButton29 = new System.Windows.Forms.RadioButton();
            this.rdCTWaterFeedSystemNo = new System.Windows.Forms.RadioButton();
            this.rdCTWaterFeedSystemYes = new System.Windows.Forms.RadioButton();
            this.label92 = new System.Windows.Forms.Label();
            this.pnlCTGallonTank = new System.Windows.Forms.Panel();
            this.rdCTGallonTankNo = new System.Windows.Forms.RadioButton();
            this.rdCTGallonTankYes = new System.Windows.Forms.RadioButton();
            this.pnlCTConveyor = new System.Windows.Forms.Panel();
            this.rdCTConveyorNo = new System.Windows.Forms.RadioButton();
            this.rdCTConveyorYes = new System.Windows.Forms.RadioButton();
            this.label89 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.pnlCTDualMotorNoAxle = new System.Windows.Forms.Panel();
            this.radioButton24 = new System.Windows.Forms.RadioButton();
            this.rdCTDualMotorNoAxleNo = new System.Windows.Forms.RadioButton();
            this.rdCTDualMotorNoAxleYes = new System.Windows.Forms.RadioButton();
            this.label90 = new System.Windows.Forms.Label();
            this.pnlCTDiamondSweep = new System.Windows.Forms.Panel();
            this.radioButton19 = new System.Windows.Forms.RadioButton();
            this.rdCTDiamondSweepNo = new System.Windows.Forms.RadioButton();
            this.rdCTDiamondSweepYes = new System.Windows.Forms.RadioButton();
            this.label88 = new System.Windows.Forms.Label();
            this.pnlCT1_1_4thHoseUpgrade = new System.Windows.Forms.Panel();
            this.rdCT1_1_4thHoseUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdCT1_1_4thHoseUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label86 = new System.Windows.Forms.Label();
            this.txtCTHoseLoopHeight = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label85 = new System.Windows.Forms.Label();
            this.pnlCTWirelessWalkSwitch = new System.Windows.Forms.Panel();
            this.rdCTWirelessWalkSwitchNo = new System.Windows.Forms.RadioButton();
            this.rdCTWirelessWalkSwitchYes = new System.Windows.Forms.RadioButton();
            this.label67 = new System.Windows.Forms.Label();
            this.pnlCTTripleTurretBody = new System.Windows.Forms.Panel();
            this.rdCTTripleTurretBodyNo = new System.Windows.Forms.RadioButton();
            this.rdCTTripleTurretBodyYes = new System.Windows.Forms.RadioButton();
            this.label75 = new System.Windows.Forms.Label();
            this.txtCTQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlCTMountedInjector = new System.Windows.Forms.Panel();
            this.rdCTMountedInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdCTMountedInjectorYes = new System.Windows.Forms.RadioButton();
            this.label78 = new System.Windows.Forms.Label();
            this.pnlCTPressureRegulator = new System.Windows.Forms.Panel();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.rdCTPressureRegulatorNo = new System.Windows.Forms.RadioButton();
            this.rdCTPressureRegulatorYes = new System.Windows.Forms.RadioButton();
            this.label79 = new System.Windows.Forms.Label();
            this.txtCTSpraybodySpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label80 = new System.Windows.Forms.Label();
            this.pnlCTTripleWaterLocklineUpgrade = new System.Windows.Forms.Panel();
            this.rdCTTripleWaterLocklineUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdCTTripleWaterLocklineUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label81 = new System.Windows.Forms.Label();
            this.ddlCTControllerType = new System.Windows.Forms.ComboBox();
            this.label82 = new System.Windows.Forms.Label();
            this.ddlCTSingleDualWater = new System.Windows.Forms.ComboBox();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.tpNavigatorBoom = new System.Windows.Forms.TabPage();
            this.ddlNavEVWireGauge = new System.Windows.Forms.ComboBox();
            this.label217 = new System.Windows.Forms.Label();
            this.ddlNavInjectorType = new System.Windows.Forms.ComboBox();
            this.label157 = new System.Windows.Forms.Label();
            this.pnlNavAutoInjector = new System.Windows.Forms.Panel();
            this.rdNavAutoInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdNavAutoInjectorYes = new System.Windows.Forms.RadioButton();
            this.label158 = new System.Windows.Forms.Label();
            this.pnlNavWaterSolenoidControl = new System.Windows.Forms.Panel();
            this.rdNavWaterSolenoidControlNo = new System.Windows.Forms.RadioButton();
            this.rdNavWaterSolenoidControlYes = new System.Windows.Forms.RadioButton();
            this.label148 = new System.Windows.Forms.Label();
            this.ddlNavFeed = new System.Windows.Forms.ComboBox();
            this.label139 = new System.Windows.Forms.Label();
            this.ddlNavCarrySteel = new System.Windows.Forms.ComboBox();
            this.label133 = new System.Windows.Forms.Label();
            this.pnlNavEV = new System.Windows.Forms.Panel();
            this.rdNavEVNo = new System.Windows.Forms.RadioButton();
            this.rdNavEVYes = new System.Windows.Forms.RadioButton();
            this.label132 = new System.Windows.Forms.Label();
            this.txtNavTip3 = new System.Windows.Forms.TextBox();
            this.label124 = new System.Windows.Forms.Label();
            this.txtNavTip2 = new System.Windows.Forms.TextBox();
            this.label125 = new System.Windows.Forms.Label();
            this.txtNavTip1 = new System.Windows.Forms.TextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.txtNavNumberOfRows = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label111 = new System.Windows.Forms.Label();
            this.ddlNavControllerType = new System.Windows.Forms.ComboBox();
            this.label94 = new System.Windows.Forms.Label();
            this.pnlNavWaterFeedSystem = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.rdNavWaterFeedSystemNo = new System.Windows.Forms.RadioButton();
            this.rdNavWaterFeedSystemYes = new System.Windows.Forms.RadioButton();
            this.label91 = new System.Windows.Forms.Label();
            this.pnlNavGallonTank = new System.Windows.Forms.Panel();
            this.rdNavGallonTankNo = new System.Windows.Forms.RadioButton();
            this.rdNavGallonTankYes = new System.Windows.Forms.RadioButton();
            this.label93 = new System.Windows.Forms.Label();
            this.pnlNavSweep90 = new System.Windows.Forms.Panel();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.rdNavSweep90No = new System.Windows.Forms.RadioButton();
            this.rdNavSweep90Yes = new System.Windows.Forms.RadioButton();
            this.label95 = new System.Windows.Forms.Label();
            this.pnlNav1_1_4thHoseUpgrade = new System.Windows.Forms.Panel();
            this.rdNav1_1_4thHoseUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdNav1_1_4thHoseUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label97 = new System.Windows.Forms.Label();
            this.txtNavHoseLoopHeight = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label98 = new System.Windows.Forms.Label();
            this.pnlNavWirelessWalkSwitch = new System.Windows.Forms.Panel();
            this.rdNavWirelessWalkSwitchNo = new System.Windows.Forms.RadioButton();
            this.rdNavWirelessWalkSwitchYes = new System.Windows.Forms.RadioButton();
            this.label99 = new System.Windows.Forms.Label();
            this.pnlNavTripleTurretBody = new System.Windows.Forms.Panel();
            this.rdNavTripleTurretBodyNo = new System.Windows.Forms.RadioButton();
            this.rdNavTripleTurretBodyYes = new System.Windows.Forms.RadioButton();
            this.label102 = new System.Windows.Forms.Label();
            this.txtNavQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlNavMountedInjector = new System.Windows.Forms.Panel();
            this.rdNavMountedInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdNavMountedInjectorYes = new System.Windows.Forms.RadioButton();
            this.label103 = new System.Windows.Forms.Label();
            this.pnlNavPressureRegulator = new System.Windows.Forms.Panel();
            this.radioButton28 = new System.Windows.Forms.RadioButton();
            this.rdNavPressureRegulatorNo = new System.Windows.Forms.RadioButton();
            this.rdNavPressureRegulatorYes = new System.Windows.Forms.RadioButton();
            this.label104 = new System.Windows.Forms.Label();
            this.txtNavSpraybodySpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label105 = new System.Windows.Forms.Label();
            this.pnlNavTripleWaterLocklineUpgrade = new System.Windows.Forms.Panel();
            this.rdNavTripleWaterLocklineUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdNavTripleWaterLocklineUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label106 = new System.Windows.Forms.Label();
            this.ddlNavSingleDualWater = new System.Windows.Forms.ComboBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.tpGroundRunnerBoom = new System.Windows.Forms.TabPage();
            this.ddlGREVWireGauge = new System.Windows.Forms.ComboBox();
            this.label218 = new System.Windows.Forms.Label();
            this.pnlGRWaterFeedSystem = new System.Windows.Forms.Panel();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.rdGRWaterFeedSystemNo = new System.Windows.Forms.RadioButton();
            this.rdGRWaterFeedSystemYes = new System.Windows.Forms.RadioButton();
            this.label209 = new System.Windows.Forms.Label();
            this.ddlGRInjectorType = new System.Windows.Forms.ComboBox();
            this.label162 = new System.Windows.Forms.Label();
            this.pnlGRAutoInjector = new System.Windows.Forms.Panel();
            this.rdGRAutoInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdGRAutoInjectorYes = new System.Windows.Forms.RadioButton();
            this.label163 = new System.Windows.Forms.Label();
            this.pnlGRWaterSolenoidControl = new System.Windows.Forms.Panel();
            this.rdGRWaterSolenoidControlNo = new System.Windows.Forms.RadioButton();
            this.rdGRWaterSolenoidControlYes = new System.Windows.Forms.RadioButton();
            this.label164 = new System.Windows.Forms.Label();
            this.pnlGREV = new System.Windows.Forms.Panel();
            this.rdGREVNo = new System.Windows.Forms.RadioButton();
            this.rdGREVYes = new System.Windows.Forms.RadioButton();
            this.label165 = new System.Windows.Forms.Label();
            this.txtGRTip3 = new System.Windows.Forms.TextBox();
            this.label166 = new System.Windows.Forms.Label();
            this.txtGRTip2 = new System.Windows.Forms.TextBox();
            this.label167 = new System.Windows.Forms.Label();
            this.txtGRTip1 = new System.Windows.Forms.TextBox();
            this.label168 = new System.Windows.Forms.Label();
            this.txtGRNumberOfRows = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label169 = new System.Windows.Forms.Label();
            this.pnlGRWirelessWalkSwitch = new System.Windows.Forms.Panel();
            this.rdGRWirelessWalkSwitchNo = new System.Windows.Forms.RadioButton();
            this.rdGRWirelessWalkSwitchYes = new System.Windows.Forms.RadioButton();
            this.label170 = new System.Windows.Forms.Label();
            this.txtGRRailDropDownAssy = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label171 = new System.Windows.Forms.Label();
            this.pnlGR1_1_4thHoseUpgrade = new System.Windows.Forms.Panel();
            this.rdGR1_1_4thHoseUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdGR1_1_4thHoseUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label172 = new System.Windows.Forms.Label();
            this.pnlGRTripleTurretBody = new System.Windows.Forms.Panel();
            this.rdGRTripleTurretBodyNo = new System.Windows.Forms.RadioButton();
            this.rdGRTripleTurretBodyYes = new System.Windows.Forms.RadioButton();
            this.label173 = new System.Windows.Forms.Label();
            this.txtGRQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlGRMountedInjector = new System.Windows.Forms.Panel();
            this.rdGRMountedInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdGRMountedInjectorYes = new System.Windows.Forms.RadioButton();
            this.label174 = new System.Windows.Forms.Label();
            this.pnlGRPressureRegulator = new System.Windows.Forms.Panel();
            this.radioButton23 = new System.Windows.Forms.RadioButton();
            this.rdGRPressureRegulatorNo = new System.Windows.Forms.RadioButton();
            this.rdGRPressureRegulatorYes = new System.Windows.Forms.RadioButton();
            this.label175 = new System.Windows.Forms.Label();
            this.txtGRSpraybodySpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label176 = new System.Windows.Forms.Label();
            this.pnlGRSweep90 = new System.Windows.Forms.Panel();
            this.rdGRSweep90No = new System.Windows.Forms.RadioButton();
            this.rdGRSweep90Yes = new System.Windows.Forms.RadioButton();
            this.label177 = new System.Windows.Forms.Label();
            this.pnlGRTripleWaterLocklineUpgrade = new System.Windows.Forms.Panel();
            this.rdGRTripleWaterLocklineUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdGRTripleWaterLocklineUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label178 = new System.Windows.Forms.Label();
            this.ddlGRControllerType = new System.Windows.Forms.ComboBox();
            this.label179 = new System.Windows.Forms.Label();
            this.txtGRHoseLoopHeight = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label180 = new System.Windows.Forms.Label();
            this.ddlGRSingleDualWater = new System.Windows.Forms.ComboBox();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.tpSkyRailBoom = new System.Windows.Forms.TabPage();
            this.ddlSREVWireGauge = new System.Windows.Forms.ComboBox();
            this.label219 = new System.Windows.Forms.Label();
            this.pnlSRWaterFeedSystem = new System.Windows.Forms.Panel();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.rdSRWaterFeedSystemNo = new System.Windows.Forms.RadioButton();
            this.rdSRWaterFeedSystemYes = new System.Windows.Forms.RadioButton();
            this.label210 = new System.Windows.Forms.Label();
            this.ddlSRInjectorType = new System.Windows.Forms.ComboBox();
            this.label183 = new System.Windows.Forms.Label();
            this.pnlSRAutoInjector = new System.Windows.Forms.Panel();
            this.rdSRAutoInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdSRAutoInjectorYes = new System.Windows.Forms.RadioButton();
            this.label184 = new System.Windows.Forms.Label();
            this.pnlSRWaterSolenoidControl = new System.Windows.Forms.Panel();
            this.rdSRWaterSolenoidControlNo = new System.Windows.Forms.RadioButton();
            this.rdSRWaterSolenoidControlYes = new System.Windows.Forms.RadioButton();
            this.label185 = new System.Windows.Forms.Label();
            this.pnlSREV = new System.Windows.Forms.Panel();
            this.rdSREVNo = new System.Windows.Forms.RadioButton();
            this.rdSREVYes = new System.Windows.Forms.RadioButton();
            this.label186 = new System.Windows.Forms.Label();
            this.txtSRTip3 = new System.Windows.Forms.TextBox();
            this.label187 = new System.Windows.Forms.Label();
            this.txtSRTip2 = new System.Windows.Forms.TextBox();
            this.label188 = new System.Windows.Forms.Label();
            this.txtSRTip1 = new System.Windows.Forms.TextBox();
            this.label189 = new System.Windows.Forms.Label();
            this.txtSRNumberOfRows = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label190 = new System.Windows.Forms.Label();
            this.pnlSRWirelessWalkSwitch = new System.Windows.Forms.Panel();
            this.rdSRWirelessWalkSwitchNo = new System.Windows.Forms.RadioButton();
            this.rdSRWirelessWalkSwitchYes = new System.Windows.Forms.RadioButton();
            this.label191 = new System.Windows.Forms.Label();
            this.txtSRRailDropDownAssy = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label192 = new System.Windows.Forms.Label();
            this.pnlSR1_1_4thHoseUpgrade = new System.Windows.Forms.Panel();
            this.rdSR1_1_4thHoseUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdSR1_1_4thHoseUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label193 = new System.Windows.Forms.Label();
            this.pnlSRTripleTurretBody = new System.Windows.Forms.Panel();
            this.rdSRTripleTurretBodyNo = new System.Windows.Forms.RadioButton();
            this.rdSRTripleTurretBodyYes = new System.Windows.Forms.RadioButton();
            this.label194 = new System.Windows.Forms.Label();
            this.txtSRQuantityOfSystems = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlSRMountedInjector = new System.Windows.Forms.Panel();
            this.rdSRMountedInjectorNo = new System.Windows.Forms.RadioButton();
            this.rdSRMountedInjectorYes = new System.Windows.Forms.RadioButton();
            this.label195 = new System.Windows.Forms.Label();
            this.pnlSRPressureRegulator = new System.Windows.Forms.Panel();
            this.radioButton25 = new System.Windows.Forms.RadioButton();
            this.rdSRPressureRegulatorNo = new System.Windows.Forms.RadioButton();
            this.rdSRPressureRegulatorYes = new System.Windows.Forms.RadioButton();
            this.label196 = new System.Windows.Forms.Label();
            this.txtSRSpraybodySpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label197 = new System.Windows.Forms.Label();
            this.pnlSRSweep90 = new System.Windows.Forms.Panel();
            this.rdSRSweep90No = new System.Windows.Forms.RadioButton();
            this.rdSRSweep90Yes = new System.Windows.Forms.RadioButton();
            this.label198 = new System.Windows.Forms.Label();
            this.pnlSRTripleWaterLocklineUpgrade = new System.Windows.Forms.Panel();
            this.rdSRTripleWaterLocklineUpgradeNo = new System.Windows.Forms.RadioButton();
            this.rdSRTripleWaterLocklineUpgradeYes = new System.Windows.Forms.RadioButton();
            this.label199 = new System.Windows.Forms.Label();
            this.ddlSRControllerType = new System.Windows.Forms.ComboBox();
            this.label200 = new System.Windows.Forms.Label();
            this.txtSRHoseLoopHeight = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label201 = new System.Windows.Forms.Label();
            this.ddlSRSingleDualWater = new System.Windows.Forms.ComboBox();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.txtNumberOfBays = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtBayLength = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtNumberOfBaySidewalls = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtWalkwayWidth = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtBayWidth = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtBottomCordHeight = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtTrussSpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtGrowingSurfaceHeight = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtPostSpacing = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtSpraybarSize = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtHoseSize = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.rdSQCordType = new System.Windows.Forms.RadioButton();
            this.rdRDCordType = new System.Windows.Forms.RadioButton();
            this.pnlBottomCordType = new System.Windows.Forms.Panel();
            this.greaterThanZeroErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.dropdownValueErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.radioButtonsErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnCreateQuote = new System.Windows.Forms.Button();
            this.emptyTextErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lstDetailQuotes = new System.Windows.Forms.ListBox();
            this.label37 = new System.Windows.Forms.Label();
            this.ddlShipAddresses = new System.Windows.Forms.ComboBox();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.mnNewQuote = new System.Windows.Forms.ToolStripMenuItem();
            this.mnRefreshLookups = new System.Windows.Forms.ToolStripMenuItem();
            this.mnSearchQuotes = new System.Windows.Forms.ToolStripMenuItem();
            this.mnManageLookups = new System.Windows.Forms.ToolStripMenuItem();
            this.mnCustomers = new System.Windows.Forms.ToolStripMenuItem();
            this.mnSalesReps = new System.Windows.Forms.ToolStripMenuItem();
            this.mnBaseCosts = new System.Windows.Forms.ToolStripMenuItem();
            this.mnDistributors = new System.Windows.Forms.ToolStripMenuItem();
            this.mnGreenHouses = new System.Windows.Forms.ToolStripMenuItem();
            this.mnManageComponents = new System.Windows.Forms.ToolStripMenuItem();
            this.mnManageEmails = new System.Windows.Forms.ToolStripMenuItem();
            this.mnManageCustomerQuoteEmail = new System.Windows.Forms.ToolStripMenuItem();
            this.label100 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.ddlQuoteStatus = new System.Windows.Forms.ComboBox();
            this.btnAddSystem = new System.Windows.Forms.Button();
            this.btnUpdateQuote = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.ddlDistributors = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtBottomCordSize = new System.Windows.Forms.TextBox();
            this.btnGenerateQuoteReport = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.txtFreightEstimate = new System.Windows.Forms.TextBox();
            this.txtQuoteNumber = new System.Windows.Forms.TextBox();
            this.btnRemoveSystem = new System.Windows.Forms.Button();
            this.txtPostDimensions = new System.Windows.Forms.TextBox();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.btnSendEmail = new System.Windows.Forms.Button();
            this.btnGenerateSalesOrderForm = new System.Windows.Forms.Button();
            this.label140 = new System.Windows.Forms.Label();
            this.dtOrderConfirmed = new System.Windows.Forms.DateTimePicker();
            this.label141 = new System.Windows.Forms.Label();
            this.txtClearanceWidth = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label142 = new System.Windows.Forms.Label();
            this.txtInstallationEstimate = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.txtPurchaseOrder = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.txtCustomerNote = new System.Windows.Forms.TextBox();
            this.label206 = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.label207 = new System.Windows.Forms.Label();
            this.txtSteelEstimate = new System.Windows.Forms.TextBox();
            this.label208 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.txtDueDate = new System.Windows.Forms.DateTimePicker();
            this.txtContactName = new System.Windows.Forms.TextBox();
            this.txtContactPhone = new System.Windows.Forms.TextBox();
            this.txtContactFax = new System.Windows.Forms.TextBox();
            this.txtContactEmail = new System.Windows.Forms.TextBox();
            this.txtShipAddress = new System.Windows.Forms.TextBox();
            this.label211 = new System.Windows.Forms.Label();
            this.btnAddCustomItems = new System.Windows.Forms.Button();
            this.btnSteelMaterials = new System.Windows.Forms.Button();
            this.txtLeadTime = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label212 = new System.Windows.Forms.Label();
            this.txtQuoteTotal = new System.Windows.Forms.TextBox();
            this.txtPriceLevel = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label220 = new System.Windows.Forms.Label();
            this.tbBooms.SuspendLayout();
            this.tpEcho.SuspendLayout();
            this.pnlEchoWaterSolenoidControl.SuspendLayout();
            this.pnlEchoEV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEchoQuantityOfSystems)).BeginInit();
            this.pnlEchoRedheadBreakerUpgrade.SuspendLayout();
            this.pnlEchoRemotePullChainSwitch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEchoExtensionHangerLength)).BeginInit();
            this.pnlEchoLockline.SuspendLayout();
            this.pnlEchoAMIADFilterAssembly.SuspendLayout();
            this.pnlEchoCenterBayWaterStation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEchoBasketSpacing)).BeginInit();
            this.tpSingleRailBoom.SuspendLayout();
            this.pnlSRBAutoInjector.SuspendLayout();
            this.pnlSRBWaterSolenoidControl.SuspendLayout();
            this.pnlSRBEV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRBNumberOfRows)).BeginInit();
            this.pnlSRBAMIADFilderAssy.SuspendLayout();
            this.pnlSRBTripleTurretBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRBQuantityOfSystems)).BeginInit();
            this.pnlSRBMountedInjector.SuspendLayout();
            this.pnlSRBPressureRegulator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRBSpraybodySpacing)).BeginInit();
            this.pnlSRBSweep90Degree.SuspendLayout();
            this.pnlSRBTripleWaterLocklineUpgrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRBHoseLoopHeight)).BeginInit();
            this.tpDoubleRailBoom.SuspendLayout();
            this.pnlDRBAutoInjector.SuspendLayout();
            this.pnlDRBWaterSolenoidControl.SuspendLayout();
            this.pnlDRBEV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBNumberOfRows)).BeginInit();
            this.pnlDRBWirelessWalkSwitch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBRailDropDownAssy)).BeginInit();
            this.pnlDRB1_1_4thHoseUpgrade.SuspendLayout();
            this.pnlDRBTripleTurretBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBQuantityOfSystems)).BeginInit();
            this.pnlDRBMountedInjector.SuspendLayout();
            this.pnlDRBPressureRegulator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBSpraybodySpacing)).BeginInit();
            this.pnlDRBSweep90.SuspendLayout();
            this.pnlDRBTripleWaterLocklineUpgrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBHoseLoopHeight)).BeginInit();
            this.tpCWFDoubleRailBoom.SuspendLayout();
            this.pnlCWFDRBWaterFeedSystem.SuspendLayout();
            this.pnlCWFDRB1_1_4thHoseUpgrade.SuspendLayout();
            this.pnlCWFDRBAutoInjector.SuspendLayout();
            this.pnlCWFDRBWaterSolenoidControl.SuspendLayout();
            this.pnlCWFDRBEV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCWFDRBNumberOfRows)).BeginInit();
            this.pnlCWFDRBWirelessWalkSwitch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCWFDRBRailDropDownAssy)).BeginInit();
            this.pnlCWFDRBTripleTurretBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCWFDRBQuantityOfSystems)).BeginInit();
            this.pnlCWFDRBMountedInjector.SuspendLayout();
            this.pnlCWFDRBPressureRegulator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCWFDRBSpraybodySpacing)).BeginInit();
            this.pnlCWFDRBTripleWaterLocklineUpgrade.SuspendLayout();
            this.tpControlTowerBoom.SuspendLayout();
            this.pnlCTAutoInjector.SuspendLayout();
            this.pnlCTWaterSolenoidControl.SuspendLayout();
            this.pnlCTEV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTNumberOfRows)).BeginInit();
            this.pnlCTWaterFeedSystem.SuspendLayout();
            this.pnlCTGallonTank.SuspendLayout();
            this.pnlCTConveyor.SuspendLayout();
            this.pnlCTDualMotorNoAxle.SuspendLayout();
            this.pnlCTDiamondSweep.SuspendLayout();
            this.pnlCT1_1_4thHoseUpgrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTHoseLoopHeight)).BeginInit();
            this.pnlCTWirelessWalkSwitch.SuspendLayout();
            this.pnlCTTripleTurretBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTQuantityOfSystems)).BeginInit();
            this.pnlCTMountedInjector.SuspendLayout();
            this.pnlCTPressureRegulator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTSpraybodySpacing)).BeginInit();
            this.pnlCTTripleWaterLocklineUpgrade.SuspendLayout();
            this.tpNavigatorBoom.SuspendLayout();
            this.pnlNavAutoInjector.SuspendLayout();
            this.pnlNavWaterSolenoidControl.SuspendLayout();
            this.pnlNavEV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNavNumberOfRows)).BeginInit();
            this.pnlNavWaterFeedSystem.SuspendLayout();
            this.pnlNavGallonTank.SuspendLayout();
            this.pnlNavSweep90.SuspendLayout();
            this.pnlNav1_1_4thHoseUpgrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNavHoseLoopHeight)).BeginInit();
            this.pnlNavWirelessWalkSwitch.SuspendLayout();
            this.pnlNavTripleTurretBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNavQuantityOfSystems)).BeginInit();
            this.pnlNavMountedInjector.SuspendLayout();
            this.pnlNavPressureRegulator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNavSpraybodySpacing)).BeginInit();
            this.pnlNavTripleWaterLocklineUpgrade.SuspendLayout();
            this.tpGroundRunnerBoom.SuspendLayout();
            this.pnlGRWaterFeedSystem.SuspendLayout();
            this.pnlGRAutoInjector.SuspendLayout();
            this.pnlGRWaterSolenoidControl.SuspendLayout();
            this.pnlGREV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRNumberOfRows)).BeginInit();
            this.pnlGRWirelessWalkSwitch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRRailDropDownAssy)).BeginInit();
            this.pnlGR1_1_4thHoseUpgrade.SuspendLayout();
            this.pnlGRTripleTurretBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRQuantityOfSystems)).BeginInit();
            this.pnlGRMountedInjector.SuspendLayout();
            this.pnlGRPressureRegulator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRSpraybodySpacing)).BeginInit();
            this.pnlGRSweep90.SuspendLayout();
            this.pnlGRTripleWaterLocklineUpgrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRHoseLoopHeight)).BeginInit();
            this.tpSkyRailBoom.SuspendLayout();
            this.pnlSRWaterFeedSystem.SuspendLayout();
            this.pnlSRAutoInjector.SuspendLayout();
            this.pnlSRWaterSolenoidControl.SuspendLayout();
            this.pnlSREV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRNumberOfRows)).BeginInit();
            this.pnlSRWirelessWalkSwitch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRRailDropDownAssy)).BeginInit();
            this.pnlSR1_1_4thHoseUpgrade.SuspendLayout();
            this.pnlSRTripleTurretBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRQuantityOfSystems)).BeginInit();
            this.pnlSRMountedInjector.SuspendLayout();
            this.pnlSRPressureRegulator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRSpraybodySpacing)).BeginInit();
            this.pnlSRSweep90.SuspendLayout();
            this.pnlSRTripleWaterLocklineUpgrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRHoseLoopHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberOfBays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBayLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberOfBaySidewalls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWalkwayWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBayWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBottomCordHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrussSpacing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrowingSurfaceHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostSpacing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpraybarSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoseSize)).BeginInit();
            this.pnlBottomCordType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.greaterThanZeroErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropdownValueErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtonsErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptyTextErrorProvider)).BeginInit();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtClearanceWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeadTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // ddlCustomers
            // 
            this.ddlCustomers.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ddlCustomers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCustomers.FormattingEnabled = true;
            this.ddlCustomers.Location = new System.Drawing.Point(137, 164);
            this.ddlCustomers.Name = "ddlCustomers";
            this.ddlCustomers.Size = new System.Drawing.Size(181, 21);
            this.ddlCustomers.TabIndex = 7;
            this.ddlCustomers.Tag = "Echo,SingleRail,DoubleRail,CWFDoubleRail,Tower,Navigator,GroundRunner,SkyRail";
            this.ddlCustomers.SelectionChangeCommitted += new System.EventHandler(this.ddlCustomers_SelectionChangeCommitted);
            this.ddlCustomers.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Customer:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(92, 258);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Fax:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(84, 288);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Email:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(78, 228);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Phone:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(72, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Contact:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(60, 383);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Sales Rep:";
            // 
            // ddlSalesReps
            // 
            this.ddlSalesReps.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSalesReps.FormattingEnabled = true;
            this.ddlSalesReps.Location = new System.Drawing.Point(137, 381);
            this.ddlSalesReps.Name = "ddlSalesReps";
            this.ddlSalesReps.Size = new System.Drawing.Size(181, 21);
            this.ddlSalesReps.TabIndex = 19;
            this.ddlSalesReps.Tag = "Echo,SingleRail,DoubleRail,CWFDoubleRail,Tower,Navigator,GroundRunner,SkyRail";
            this.ddlSalesReps.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 418);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Lead Time (Weeks):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(344, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Select Greenhouse Type:";
            // 
            // ddlGreenhouseType
            // 
            this.ddlGreenhouseType.DisplayMember = "GreenHouseTypeId";
            this.ddlGreenhouseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlGreenhouseType.FormattingEnabled = true;
            this.ddlGreenhouseType.Location = new System.Drawing.Point(487, 42);
            this.ddlGreenhouseType.Name = "ddlGreenhouseType";
            this.ddlGreenhouseType.Size = new System.Drawing.Size(110, 21);
            this.ddlGreenhouseType.TabIndex = 25;
            this.ddlGreenhouseType.Tag = "Echo,SingleRail,DoubleRail,CWFDoubleRail,Tower,Navigator,GroundRunner,SkyRail";
            this.ddlGreenhouseType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(372, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 35);
            this.label10.TabIndex = 26;
            this.label10.Text = "Number of Bays:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(372, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 35);
            this.label11.TabIndex = 28;
            this.label11.Text = "Bay Length (FT):";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(372, 134);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 35);
            this.label12.TabIndex = 30;
            this.label12.Text = "Bay Width (FT):";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(570, 133);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 35);
            this.label13.TabIndex = 32;
            this.label13.Text = "Number of Bay Sidewalls:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(841, 70);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 35);
            this.label14.TabIndex = 44;
            this.label14.Text = "Walkway Width (IN):";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(828, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 35);
            this.label15.TabIndex = 42;
            this.label15.Text = "Truss Spacing (FT):";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(571, 232);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 35);
            this.label16.TabIndex = 54;
            this.label16.Text = "Bottom Cord Height (FT):";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(372, 168);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 35);
            this.label18.TabIndex = 48;
            this.label18.Text = "Hose Size (IN):";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(598, 101);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 35);
            this.label19.TabIndex = 46;
            this.label19.Text = "Spraybar Size (IN):";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(333, 268);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(139, 35);
            this.label20.TabIndex = 38;
            this.label20.Text = "Post Dimensions (IN X IN):";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(372, 236);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 35);
            this.label21.TabIndex = 36;
            this.label21.Text = "Post Spacing (FT):";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(330, 203);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(142, 35);
            this.label22.TabIndex = 34;
            this.label22.Text = "Growing Surface Height (IN):";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(598, 168);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 35);
            this.label17.TabIndex = 50;
            this.label17.Text = "Bottom Cord Type:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAdditionalInformation
            // 
            this.txtAdditionalInformation.Location = new System.Drawing.Point(707, 307);
            this.txtAdditionalInformation.Multiline = true;
            this.txtAdditionalInformation.Name = "txtAdditionalInformation";
            this.txtAdditionalInformation.Size = new System.Drawing.Size(309, 80);
            this.txtAdditionalInformation.TabIndex = 41;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(587, 310);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(111, 13);
            this.label23.TabIndex = 40;
            this.label23.Text = "Additional Information:";
            // 
            // tbBooms
            // 
            this.tbBooms.Controls.Add(this.tpEcho);
            this.tbBooms.Controls.Add(this.tpSingleRailBoom);
            this.tbBooms.Controls.Add(this.tpDoubleRailBoom);
            this.tbBooms.Controls.Add(this.tpCWFDoubleRailBoom);
            this.tbBooms.Controls.Add(this.tpControlTowerBoom);
            this.tbBooms.Controls.Add(this.tpNavigatorBoom);
            this.tbBooms.Controls.Add(this.tpGroundRunnerBoom);
            this.tbBooms.Controls.Add(this.tpSkyRailBoom);
            this.tbBooms.Location = new System.Drawing.Point(26, 520);
            this.tbBooms.Name = "tbBooms";
            this.tbBooms.SelectedIndex = 0;
            this.tbBooms.Size = new System.Drawing.Size(1376, 293);
            this.tbBooms.TabIndex = 57;
            this.tbBooms.TabStop = false;
            this.tbBooms.Tag = "";
            // 
            // tpEcho
            // 
            this.tpEcho.Controls.Add(this.pnlEchoWaterSolenoidControl);
            this.tpEcho.Controls.Add(this.label143);
            this.tpEcho.Controls.Add(this.ddlEchoFeed);
            this.tpEcho.Controls.Add(this.label135);
            this.tpEcho.Controls.Add(this.pnlEchoEV);
            this.tpEcho.Controls.Add(this.label127);
            this.tpEcho.Controls.Add(this.txtEchoQuantityOfSystems);
            this.tpEcho.Controls.Add(this.pnlEchoRedheadBreakerUpgrade);
            this.tpEcho.Controls.Add(this.ddlEchoPulleyBracketSpacing);
            this.tpEcho.Controls.Add(this.label35);
            this.tpEcho.Controls.Add(this.label34);
            this.tpEcho.Controls.Add(this.pnlEchoRemotePullChainSwitch);
            this.tpEcho.Controls.Add(this.label36);
            this.tpEcho.Controls.Add(this.txtEchoExtensionHangerLength);
            this.tpEcho.Controls.Add(this.label33);
            this.tpEcho.Controls.Add(this.pnlEchoLockline);
            this.tpEcho.Controls.Add(this.label32);
            this.tpEcho.Controls.Add(this.pnlEchoAMIADFilterAssembly);
            this.tpEcho.Controls.Add(this.label31);
            this.tpEcho.Controls.Add(this.pnlEchoCenterBayWaterStation);
            this.tpEcho.Controls.Add(this.label30);
            this.tpEcho.Controls.Add(this.ddlEchoNumberOfLayers);
            this.tpEcho.Controls.Add(this.label29);
            this.tpEcho.Controls.Add(this.ddlEchoControllerType);
            this.tpEcho.Controls.Add(this.label28);
            this.tpEcho.Controls.Add(this.txtEchoBasketSpacing);
            this.tpEcho.Controls.Add(this.label27);
            this.tpEcho.Controls.Add(this.ddlEchoDrumSize);
            this.tpEcho.Controls.Add(this.label26);
            this.tpEcho.Controls.Add(this.label25);
            this.tpEcho.Location = new System.Drawing.Point(4, 22);
            this.tpEcho.Name = "tpEcho";
            this.tpEcho.Padding = new System.Windows.Forms.Padding(3);
            this.tpEcho.Size = new System.Drawing.Size(1368, 267);
            this.tpEcho.TabIndex = 0;
            this.tpEcho.Tag = "Echo";
            this.tpEcho.Text = "Echo";
            this.tpEcho.UseVisualStyleBackColor = true;
            // 
            // pnlEchoWaterSolenoidControl
            // 
            this.pnlEchoWaterSolenoidControl.Controls.Add(this.rdEchoWaterSolenoidControlNo);
            this.pnlEchoWaterSolenoidControl.Controls.Add(this.rdEchoWaterSolenoidControlYes);
            this.pnlEchoWaterSolenoidControl.Location = new System.Drawing.Point(922, 11);
            this.pnlEchoWaterSolenoidControl.Name = "pnlEchoWaterSolenoidControl";
            this.pnlEchoWaterSolenoidControl.Size = new System.Drawing.Size(160, 31);
            this.pnlEchoWaterSolenoidControl.TabIndex = 27;
            // 
            // rdEchoWaterSolenoidControlNo
            // 
            this.rdEchoWaterSolenoidControlNo.AutoSize = true;
            this.rdEchoWaterSolenoidControlNo.Location = new System.Drawing.Point(87, 6);
            this.rdEchoWaterSolenoidControlNo.Name = "rdEchoWaterSolenoidControlNo";
            this.rdEchoWaterSolenoidControlNo.Size = new System.Drawing.Size(41, 17);
            this.rdEchoWaterSolenoidControlNo.TabIndex = 1;
            this.rdEchoWaterSolenoidControlNo.TabStop = true;
            this.rdEchoWaterSolenoidControlNo.Tag = "Echo";
            this.rdEchoWaterSolenoidControlNo.Text = "NO";
            this.rdEchoWaterSolenoidControlNo.UseVisualStyleBackColor = true;
            this.rdEchoWaterSolenoidControlNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdEchoWaterSolenoidControlNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdEchoWaterSolenoidControlYes
            // 
            this.rdEchoWaterSolenoidControlYes.AutoSize = true;
            this.rdEchoWaterSolenoidControlYes.Location = new System.Drawing.Point(26, 6);
            this.rdEchoWaterSolenoidControlYes.Name = "rdEchoWaterSolenoidControlYes";
            this.rdEchoWaterSolenoidControlYes.Size = new System.Drawing.Size(46, 17);
            this.rdEchoWaterSolenoidControlYes.TabIndex = 0;
            this.rdEchoWaterSolenoidControlYes.TabStop = true;
            this.rdEchoWaterSolenoidControlYes.Tag = "Echo";
            this.rdEchoWaterSolenoidControlYes.Text = "YES";
            this.rdEchoWaterSolenoidControlYes.UseVisualStyleBackColor = true;
            this.rdEchoWaterSolenoidControlYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(798, 20);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(119, 13);
            this.label143.TabIndex = 26;
            this.label143.Text = "Water Solenoid Control:";
            // 
            // ddlEchoFeed
            // 
            this.ddlEchoFeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEchoFeed.FormattingEnabled = true;
            this.ddlEchoFeed.Location = new System.Drawing.Point(581, 234);
            this.ddlEchoFeed.Name = "ddlEchoFeed";
            this.ddlEchoFeed.Size = new System.Drawing.Size(160, 21);
            this.ddlEchoFeed.TabIndex = 25;
            this.ddlEchoFeed.Tag = "Echo";
            this.ddlEchoFeed.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(536, 236);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(34, 13);
            this.label135.TabIndex = 24;
            this.label135.Text = "Feed:";
            // 
            // pnlEchoEV
            // 
            this.pnlEchoEV.Controls.Add(this.rdEchoEVNo);
            this.pnlEchoEV.Controls.Add(this.rdEchoEVYes);
            this.pnlEchoEV.Location = new System.Drawing.Point(189, 226);
            this.pnlEchoEV.Name = "pnlEchoEV";
            this.pnlEchoEV.Size = new System.Drawing.Size(160, 31);
            this.pnlEchoEV.TabIndex = 13;
            // 
            // rdEchoEVNo
            // 
            this.rdEchoEVNo.AutoSize = true;
            this.rdEchoEVNo.Location = new System.Drawing.Point(98, 7);
            this.rdEchoEVNo.Name = "rdEchoEVNo";
            this.rdEchoEVNo.Size = new System.Drawing.Size(41, 17);
            this.rdEchoEVNo.TabIndex = 1;
            this.rdEchoEVNo.TabStop = true;
            this.rdEchoEVNo.Tag = "Echo";
            this.rdEchoEVNo.Text = "NO";
            this.rdEchoEVNo.UseVisualStyleBackColor = true;
            this.rdEchoEVNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdEchoEVNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdEchoEVYes
            // 
            this.rdEchoEVYes.AutoSize = true;
            this.rdEchoEVYes.Location = new System.Drawing.Point(26, 7);
            this.rdEchoEVYes.Name = "rdEchoEVYes";
            this.rdEchoEVYes.Size = new System.Drawing.Size(46, 17);
            this.rdEchoEVYes.TabIndex = 0;
            this.rdEchoEVYes.TabStop = true;
            this.rdEchoEVYes.Tag = "Echo";
            this.rdEchoEVYes.Text = "YES";
            this.rdEchoEVYes.UseVisualStyleBackColor = true;
            this.rdEchoEVYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(32, 235);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(145, 13);
            this.label127.TabIndex = 12;
            this.label127.Text = "EV, VPD, Third Party Control:";
            // 
            // txtEchoQuantityOfSystems
            // 
            this.txtEchoQuantityOfSystems.Location = new System.Drawing.Point(190, 20);
            this.txtEchoQuantityOfSystems.Mask = "G";
            this.txtEchoQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtEchoQuantityOfSystems.Name = "txtEchoQuantityOfSystems";
            this.txtEchoQuantityOfSystems.Size = new System.Drawing.Size(160, 20);
            this.txtEchoQuantityOfSystems.TabIndex = 1;
            this.txtEchoQuantityOfSystems.TabStop = false;
            this.txtEchoQuantityOfSystems.Tag = "Echo";
            this.txtEchoQuantityOfSystems.Text = "0";
            this.txtEchoQuantityOfSystems.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // pnlEchoRedheadBreakerUpgrade
            // 
            this.pnlEchoRedheadBreakerUpgrade.Controls.Add(this.rdEchoRedheadBreakerUpgradeNo);
            this.pnlEchoRedheadBreakerUpgrade.Controls.Add(this.rdEchoRedheadBreakerUpgradeYes);
            this.pnlEchoRedheadBreakerUpgrade.Location = new System.Drawing.Point(582, 192);
            this.pnlEchoRedheadBreakerUpgrade.Name = "pnlEchoRedheadBreakerUpgrade";
            this.pnlEchoRedheadBreakerUpgrade.Size = new System.Drawing.Size(160, 31);
            this.pnlEchoRedheadBreakerUpgrade.TabIndex = 23;
            // 
            // rdEchoRedheadBreakerUpgradeNo
            // 
            this.rdEchoRedheadBreakerUpgradeNo.AutoSize = true;
            this.rdEchoRedheadBreakerUpgradeNo.Location = new System.Drawing.Point(87, 7);
            this.rdEchoRedheadBreakerUpgradeNo.Name = "rdEchoRedheadBreakerUpgradeNo";
            this.rdEchoRedheadBreakerUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdEchoRedheadBreakerUpgradeNo.TabIndex = 1;
            this.rdEchoRedheadBreakerUpgradeNo.TabStop = true;
            this.rdEchoRedheadBreakerUpgradeNo.Tag = "Echo";
            this.rdEchoRedheadBreakerUpgradeNo.Text = "NO";
            this.rdEchoRedheadBreakerUpgradeNo.UseVisualStyleBackColor = true;
            this.rdEchoRedheadBreakerUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdEchoRedheadBreakerUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdEchoRedheadBreakerUpgradeYes
            // 
            this.rdEchoRedheadBreakerUpgradeYes.AutoSize = true;
            this.rdEchoRedheadBreakerUpgradeYes.Location = new System.Drawing.Point(20, 7);
            this.rdEchoRedheadBreakerUpgradeYes.Name = "rdEchoRedheadBreakerUpgradeYes";
            this.rdEchoRedheadBreakerUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdEchoRedheadBreakerUpgradeYes.TabIndex = 0;
            this.rdEchoRedheadBreakerUpgradeYes.TabStop = true;
            this.rdEchoRedheadBreakerUpgradeYes.Tag = "Echo";
            this.rdEchoRedheadBreakerUpgradeYes.Text = "YES";
            this.rdEchoRedheadBreakerUpgradeYes.UseVisualStyleBackColor = true;
            this.rdEchoRedheadBreakerUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // ddlEchoPulleyBracketSpacing
            // 
            this.ddlEchoPulleyBracketSpacing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEchoPulleyBracketSpacing.FormattingEnabled = true;
            this.ddlEchoPulleyBracketSpacing.Location = new System.Drawing.Point(581, 125);
            this.ddlEchoPulleyBracketSpacing.Name = "ddlEchoPulleyBracketSpacing";
            this.ddlEchoPulleyBracketSpacing.Size = new System.Drawing.Size(160, 21);
            this.ddlEchoPulleyBracketSpacing.TabIndex = 19;
            this.ddlEchoPulleyBracketSpacing.Tag = "Echo";
            this.ddlEchoPulleyBracketSpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(390, 201);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(182, 13);
            this.label35.TabIndex = 22;
            this.label35.Text = "DRAMM Redhead Breaker Upgrade:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(451, 129);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(120, 13);
            this.label34.TabIndex = 18;
            this.label34.Text = "Pulley Bracket Spacing:";
            // 
            // pnlEchoRemotePullChainSwitch
            // 
            this.pnlEchoRemotePullChainSwitch.Controls.Add(this.rdEchoRemotePullChainSwitchNo);
            this.pnlEchoRemotePullChainSwitch.Controls.Add(this.rdEchoRemotePullChainSwitchYes);
            this.pnlEchoRemotePullChainSwitch.Location = new System.Drawing.Point(582, 155);
            this.pnlEchoRemotePullChainSwitch.Name = "pnlEchoRemotePullChainSwitch";
            this.pnlEchoRemotePullChainSwitch.Size = new System.Drawing.Size(160, 31);
            this.pnlEchoRemotePullChainSwitch.TabIndex = 21;
            // 
            // rdEchoRemotePullChainSwitchNo
            // 
            this.rdEchoRemotePullChainSwitchNo.AutoSize = true;
            this.rdEchoRemotePullChainSwitchNo.Location = new System.Drawing.Point(87, 7);
            this.rdEchoRemotePullChainSwitchNo.Name = "rdEchoRemotePullChainSwitchNo";
            this.rdEchoRemotePullChainSwitchNo.Size = new System.Drawing.Size(41, 17);
            this.rdEchoRemotePullChainSwitchNo.TabIndex = 1;
            this.rdEchoRemotePullChainSwitchNo.TabStop = true;
            this.rdEchoRemotePullChainSwitchNo.Tag = "Echo";
            this.rdEchoRemotePullChainSwitchNo.Text = "NO";
            this.rdEchoRemotePullChainSwitchNo.UseVisualStyleBackColor = true;
            this.rdEchoRemotePullChainSwitchNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdEchoRemotePullChainSwitchNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdEchoRemotePullChainSwitchYes
            // 
            this.rdEchoRemotePullChainSwitchYes.AutoSize = true;
            this.rdEchoRemotePullChainSwitchYes.Location = new System.Drawing.Point(20, 7);
            this.rdEchoRemotePullChainSwitchYes.Name = "rdEchoRemotePullChainSwitchYes";
            this.rdEchoRemotePullChainSwitchYes.Size = new System.Drawing.Size(46, 17);
            this.rdEchoRemotePullChainSwitchYes.TabIndex = 0;
            this.rdEchoRemotePullChainSwitchYes.TabStop = true;
            this.rdEchoRemotePullChainSwitchYes.Tag = "Echo";
            this.rdEchoRemotePullChainSwitchYes.Text = "YES";
            this.rdEchoRemotePullChainSwitchYes.UseVisualStyleBackColor = true;
            this.rdEchoRemotePullChainSwitchYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(440, 166);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(132, 13);
            this.label36.TabIndex = 20;
            this.label36.Text = "Remote Pull Chain Switch:";
            // 
            // txtEchoExtensionHangerLength
            // 
            this.txtEchoExtensionHangerLength.Location = new System.Drawing.Point(582, 91);
            this.txtEchoExtensionHangerLength.Mask = "F2";
            this.txtEchoExtensionHangerLength.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtEchoExtensionHangerLength.Name = "txtEchoExtensionHangerLength";
            this.txtEchoExtensionHangerLength.Size = new System.Drawing.Size(160, 20);
            this.txtEchoExtensionHangerLength.TabIndex = 17;
            this.txtEchoExtensionHangerLength.TabStop = false;
            this.txtEchoExtensionHangerLength.Tag = "";
            this.txtEchoExtensionHangerLength.Text = "0.00";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(419, 94);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(150, 13);
            this.label33.TabIndex = 16;
            this.label33.Text = "Extension Hanger Length (IN):";
            // 
            // pnlEchoLockline
            // 
            this.pnlEchoLockline.Controls.Add(this.rdEchoLocklineNo);
            this.pnlEchoLockline.Controls.Add(this.rdEchoLocklineYes);
            this.pnlEchoLockline.Location = new System.Drawing.Point(581, 48);
            this.pnlEchoLockline.Name = "pnlEchoLockline";
            this.pnlEchoLockline.Size = new System.Drawing.Size(160, 31);
            this.pnlEchoLockline.TabIndex = 15;
            // 
            // rdEchoLocklineNo
            // 
            this.rdEchoLocklineNo.AutoSize = true;
            this.rdEchoLocklineNo.Location = new System.Drawing.Point(87, 7);
            this.rdEchoLocklineNo.Name = "rdEchoLocklineNo";
            this.rdEchoLocklineNo.Size = new System.Drawing.Size(41, 17);
            this.rdEchoLocklineNo.TabIndex = 1;
            this.rdEchoLocklineNo.TabStop = true;
            this.rdEchoLocklineNo.Tag = "Echo";
            this.rdEchoLocklineNo.Text = "NO";
            this.rdEchoLocklineNo.UseVisualStyleBackColor = true;
            this.rdEchoLocklineNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdEchoLocklineNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdEchoLocklineYes
            // 
            this.rdEchoLocklineYes.AutoSize = true;
            this.rdEchoLocklineYes.Location = new System.Drawing.Point(26, 6);
            this.rdEchoLocklineYes.Name = "rdEchoLocklineYes";
            this.rdEchoLocklineYes.Size = new System.Drawing.Size(46, 17);
            this.rdEchoLocklineYes.TabIndex = 0;
            this.rdEchoLocklineYes.TabStop = true;
            this.rdEchoLocklineYes.Tag = "Echo";
            this.rdEchoLocklineYes.Text = "YES";
            this.rdEchoLocklineYes.UseVisualStyleBackColor = true;
            this.rdEchoLocklineYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(520, 54);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 13);
            this.label32.TabIndex = 14;
            this.label32.Text = "Lockline:";
            // 
            // pnlEchoAMIADFilterAssembly
            // 
            this.pnlEchoAMIADFilterAssembly.Controls.Add(this.rdEchoAMIADFilterAssemblyNo);
            this.pnlEchoAMIADFilterAssembly.Controls.Add(this.rdEchoAMIADFilterAssemblyYes);
            this.pnlEchoAMIADFilterAssembly.Location = new System.Drawing.Point(581, 11);
            this.pnlEchoAMIADFilterAssembly.Name = "pnlEchoAMIADFilterAssembly";
            this.pnlEchoAMIADFilterAssembly.Size = new System.Drawing.Size(160, 31);
            this.pnlEchoAMIADFilterAssembly.TabIndex = 13;
            // 
            // rdEchoAMIADFilterAssemblyNo
            // 
            this.rdEchoAMIADFilterAssemblyNo.AutoSize = true;
            this.rdEchoAMIADFilterAssemblyNo.Location = new System.Drawing.Point(87, 7);
            this.rdEchoAMIADFilterAssemblyNo.Name = "rdEchoAMIADFilterAssemblyNo";
            this.rdEchoAMIADFilterAssemblyNo.Size = new System.Drawing.Size(41, 17);
            this.rdEchoAMIADFilterAssemblyNo.TabIndex = 1;
            this.rdEchoAMIADFilterAssemblyNo.TabStop = true;
            this.rdEchoAMIADFilterAssemblyNo.Tag = "Echo";
            this.rdEchoAMIADFilterAssemblyNo.Text = "NO";
            this.rdEchoAMIADFilterAssemblyNo.UseVisualStyleBackColor = true;
            this.rdEchoAMIADFilterAssemblyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdEchoAMIADFilterAssemblyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdEchoAMIADFilterAssemblyYes
            // 
            this.rdEchoAMIADFilterAssemblyYes.AutoSize = true;
            this.rdEchoAMIADFilterAssemblyYes.Location = new System.Drawing.Point(26, 7);
            this.rdEchoAMIADFilterAssemblyYes.Name = "rdEchoAMIADFilterAssemblyYes";
            this.rdEchoAMIADFilterAssemblyYes.Size = new System.Drawing.Size(46, 17);
            this.rdEchoAMIADFilterAssemblyYes.TabIndex = 0;
            this.rdEchoAMIADFilterAssemblyYes.TabStop = true;
            this.rdEchoAMIADFilterAssemblyYes.Tag = "Echo";
            this.rdEchoAMIADFilterAssemblyYes.Text = "YES";
            this.rdEchoAMIADFilterAssemblyYes.UseVisualStyleBackColor = true;
            this.rdEchoAMIADFilterAssemblyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(432, 21);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(141, 13);
            this.label31.TabIndex = 12;
            this.label31.Text = "3/4\" AMIAD Filter Assembly:";
            // 
            // pnlEchoCenterBayWaterStation
            // 
            this.pnlEchoCenterBayWaterStation.Controls.Add(this.rdEchoCenterBayWaterStationNo);
            this.pnlEchoCenterBayWaterStation.Controls.Add(this.rdEchoCenterBayWaterStationYes);
            this.pnlEchoCenterBayWaterStation.Location = new System.Drawing.Point(190, 188);
            this.pnlEchoCenterBayWaterStation.Name = "pnlEchoCenterBayWaterStation";
            this.pnlEchoCenterBayWaterStation.Size = new System.Drawing.Size(160, 31);
            this.pnlEchoCenterBayWaterStation.TabIndex = 11;
            // 
            // rdEchoCenterBayWaterStationNo
            // 
            this.rdEchoCenterBayWaterStationNo.AutoSize = true;
            this.rdEchoCenterBayWaterStationNo.Location = new System.Drawing.Point(98, 7);
            this.rdEchoCenterBayWaterStationNo.Name = "rdEchoCenterBayWaterStationNo";
            this.rdEchoCenterBayWaterStationNo.Size = new System.Drawing.Size(41, 17);
            this.rdEchoCenterBayWaterStationNo.TabIndex = 1;
            this.rdEchoCenterBayWaterStationNo.TabStop = true;
            this.rdEchoCenterBayWaterStationNo.Tag = "Echo";
            this.rdEchoCenterBayWaterStationNo.Text = "NO";
            this.rdEchoCenterBayWaterStationNo.UseVisualStyleBackColor = true;
            this.rdEchoCenterBayWaterStationNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdEchoCenterBayWaterStationNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdEchoCenterBayWaterStationYes
            // 
            this.rdEchoCenterBayWaterStationYes.AutoSize = true;
            this.rdEchoCenterBayWaterStationYes.Location = new System.Drawing.Point(26, 7);
            this.rdEchoCenterBayWaterStationYes.Name = "rdEchoCenterBayWaterStationYes";
            this.rdEchoCenterBayWaterStationYes.Size = new System.Drawing.Size(46, 17);
            this.rdEchoCenterBayWaterStationYes.TabIndex = 0;
            this.rdEchoCenterBayWaterStationYes.TabStop = true;
            this.rdEchoCenterBayWaterStationYes.Tag = "Echo";
            this.rdEchoCenterBayWaterStationYes.Text = "YES";
            this.rdEchoCenterBayWaterStationYes.UseVisualStyleBackColor = true;
            this.rdEchoCenterBayWaterStationYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(50, 197);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(130, 13);
            this.label30.TabIndex = 10;
            this.label30.Text = "Center Bay Water Station:";
            // 
            // ddlEchoNumberOfLayers
            // 
            this.ddlEchoNumberOfLayers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEchoNumberOfLayers.FormattingEnabled = true;
            this.ddlEchoNumberOfLayers.Location = new System.Drawing.Point(190, 162);
            this.ddlEchoNumberOfLayers.Name = "ddlEchoNumberOfLayers";
            this.ddlEchoNumberOfLayers.Size = new System.Drawing.Size(160, 21);
            this.ddlEchoNumberOfLayers.TabIndex = 9;
            this.ddlEchoNumberOfLayers.Tag = "Echo";
            this.ddlEchoNumberOfLayers.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(85, 165);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(95, 13);
            this.label29.TabIndex = 8;
            this.label29.Text = "Number Of Layers:";
            // 
            // ddlEchoControllerType
            // 
            this.ddlEchoControllerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEchoControllerType.FormattingEnabled = true;
            this.ddlEchoControllerType.Location = new System.Drawing.Point(190, 127);
            this.ddlEchoControllerType.Name = "ddlEchoControllerType";
            this.ddlEchoControllerType.Size = new System.Drawing.Size(160, 21);
            this.ddlEchoControllerType.TabIndex = 7;
            this.ddlEchoControllerType.Tag = "Echo";
            this.ddlEchoControllerType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(98, 129);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(81, 13);
            this.label28.TabIndex = 6;
            this.label28.Text = "Controller Type:";
            // 
            // txtEchoBasketSpacing
            // 
            this.txtEchoBasketSpacing.Location = new System.Drawing.Point(190, 93);
            this.txtEchoBasketSpacing.Mask = "F2";
            this.txtEchoBasketSpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtEchoBasketSpacing.Name = "txtEchoBasketSpacing";
            this.txtEchoBasketSpacing.Size = new System.Drawing.Size(160, 20);
            this.txtEchoBasketSpacing.TabIndex = 5;
            this.txtEchoBasketSpacing.TabStop = false;
            this.txtEchoBasketSpacing.Tag = "Echo";
            this.txtEchoBasketSpacing.Text = "0.00";
            this.txtEchoBasketSpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(74, 94);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(105, 13);
            this.label27.TabIndex = 4;
            this.label27.Text = "Basket Spacing (IN):";
            // 
            // ddlEchoDrumSize
            // 
            this.ddlEchoDrumSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEchoDrumSize.FormattingEnabled = true;
            this.ddlEchoDrumSize.Location = new System.Drawing.Point(190, 54);
            this.ddlEchoDrumSize.Name = "ddlEchoDrumSize";
            this.ddlEchoDrumSize.Size = new System.Drawing.Size(160, 21);
            this.ddlEchoDrumSize.TabIndex = 3;
            this.ddlEchoDrumSize.Tag = "Echo";
            this.ddlEchoDrumSize.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(104, 54);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(78, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "Drum Size (IN):";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(77, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(105, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Quantity Of Systems:";
            // 
            // tpSingleRailBoom
            // 
            this.tpSingleRailBoom.Controls.Add(this.ddlSRBEVWireGauge);
            this.tpSingleRailBoom.Controls.Add(this.label213);
            this.tpSingleRailBoom.Controls.Add(this.ddlSRBInjectorType);
            this.tpSingleRailBoom.Controls.Add(this.label151);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBAutoInjector);
            this.tpSingleRailBoom.Controls.Add(this.label152);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBWaterSolenoidControl);
            this.tpSingleRailBoom.Controls.Add(this.label144);
            this.tpSingleRailBoom.Controls.Add(this.ddlSRBFeed);
            this.tpSingleRailBoom.Controls.Add(this.label136);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBEV);
            this.tpSingleRailBoom.Controls.Add(this.label128);
            this.tpSingleRailBoom.Controls.Add(this.txtSRBTip3);
            this.tpSingleRailBoom.Controls.Add(this.label114);
            this.tpSingleRailBoom.Controls.Add(this.txtSRBTip2);
            this.tpSingleRailBoom.Controls.Add(this.label113);
            this.tpSingleRailBoom.Controls.Add(this.txtSRBTip1);
            this.tpSingleRailBoom.Controls.Add(this.label112);
            this.tpSingleRailBoom.Controls.Add(this.txtSRBNumberOfRows);
            this.tpSingleRailBoom.Controls.Add(this.label96);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBAMIADFilderAssy);
            this.tpSingleRailBoom.Controls.Add(this.label42);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBTripleTurretBody);
            this.tpSingleRailBoom.Controls.Add(this.label53);
            this.tpSingleRailBoom.Controls.Add(this.txtSRBQuantityOfSystems);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBMountedInjector);
            this.tpSingleRailBoom.Controls.Add(this.label41);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBPressureRegulator);
            this.tpSingleRailBoom.Controls.Add(this.label43);
            this.tpSingleRailBoom.Controls.Add(this.txtSRBSpraybodySpacing);
            this.tpSingleRailBoom.Controls.Add(this.label44);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBSweep90Degree);
            this.tpSingleRailBoom.Controls.Add(this.label45);
            this.tpSingleRailBoom.Controls.Add(this.pnlSRBTripleWaterLocklineUpgrade);
            this.tpSingleRailBoom.Controls.Add(this.label47);
            this.tpSingleRailBoom.Controls.Add(this.ddlSRBControllerType);
            this.tpSingleRailBoom.Controls.Add(this.label49);
            this.tpSingleRailBoom.Controls.Add(this.txtSRBHoseLoopHeight);
            this.tpSingleRailBoom.Controls.Add(this.label50);
            this.tpSingleRailBoom.Controls.Add(this.ddlSRBSingleDualWater);
            this.tpSingleRailBoom.Controls.Add(this.label51);
            this.tpSingleRailBoom.Controls.Add(this.label52);
            this.tpSingleRailBoom.Location = new System.Drawing.Point(4, 22);
            this.tpSingleRailBoom.Name = "tpSingleRailBoom";
            this.tpSingleRailBoom.Padding = new System.Windows.Forms.Padding(3);
            this.tpSingleRailBoom.Size = new System.Drawing.Size(1368, 267);
            this.tpSingleRailBoom.TabIndex = 1;
            this.tpSingleRailBoom.Tag = "SingleRail";
            this.tpSingleRailBoom.Text = "Single Rail";
            this.tpSingleRailBoom.UseVisualStyleBackColor = true;
            // 
            // ddlSRBEVWireGauge
            // 
            this.ddlSRBEVWireGauge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSRBEVWireGauge.FormattingEnabled = true;
            this.ddlSRBEVWireGauge.Location = new System.Drawing.Point(486, 224);
            this.ddlSRBEVWireGauge.Name = "ddlSRBEVWireGauge";
            this.ddlSRBEVWireGauge.Size = new System.Drawing.Size(164, 21);
            this.ddlSRBEVWireGauge.TabIndex = 49;
            this.ddlSRBEVWireGauge.Tag = "SingleRail";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(391, 227);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(86, 13);
            this.label213.TabIndex = 48;
            this.label213.Text = "EV Wire/Gauge:";
            // 
            // ddlSRBInjectorType
            // 
            this.ddlSRBInjectorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSRBInjectorType.FormattingEnabled = true;
            this.ddlSRBInjectorType.Location = new System.Drawing.Point(487, 102);
            this.ddlSRBInjectorType.Name = "ddlSRBInjectorType";
            this.ddlSRBInjectorType.Size = new System.Drawing.Size(164, 21);
            this.ddlSRBInjectorType.TabIndex = 47;
            this.ddlSRBInjectorType.Tag = "SingleRail";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(405, 105);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(72, 13);
            this.label151.TabIndex = 46;
            this.label151.Text = "Injector Type:";
            // 
            // pnlSRBAutoInjector
            // 
            this.pnlSRBAutoInjector.Controls.Add(this.rdSRBAutoInjectorNo);
            this.pnlSRBAutoInjector.Controls.Add(this.rdSRBAutoInjectorYes);
            this.pnlSRBAutoInjector.Location = new System.Drawing.Point(488, 133);
            this.pnlSRBAutoInjector.Name = "pnlSRBAutoInjector";
            this.pnlSRBAutoInjector.Size = new System.Drawing.Size(163, 31);
            this.pnlSRBAutoInjector.TabIndex = 45;
            // 
            // rdSRBAutoInjectorNo
            // 
            this.rdSRBAutoInjectorNo.AutoSize = true;
            this.rdSRBAutoInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRBAutoInjectorNo.Name = "rdSRBAutoInjectorNo";
            this.rdSRBAutoInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBAutoInjectorNo.TabIndex = 1;
            this.rdSRBAutoInjectorNo.TabStop = true;
            this.rdSRBAutoInjectorNo.Tag = "SingleRail";
            this.rdSRBAutoInjectorNo.Text = "NO";
            this.rdSRBAutoInjectorNo.UseVisualStyleBackColor = true;
            this.rdSRBAutoInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBAutoInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBAutoInjectorYes
            // 
            this.rdSRBAutoInjectorYes.AutoSize = true;
            this.rdSRBAutoInjectorYes.Location = new System.Drawing.Point(25, 7);
            this.rdSRBAutoInjectorYes.Name = "rdSRBAutoInjectorYes";
            this.rdSRBAutoInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBAutoInjectorYes.TabIndex = 0;
            this.rdSRBAutoInjectorYes.TabStop = true;
            this.rdSRBAutoInjectorYes.Tag = "SingleRail";
            this.rdSRBAutoInjectorYes.Text = "YES";
            this.rdSRBAutoInjectorYes.UseVisualStyleBackColor = true;
            this.rdSRBAutoInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(408, 142);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(70, 13);
            this.label152.TabIndex = 44;
            this.label152.Text = "Auto Injector:";
            // 
            // pnlSRBWaterSolenoidControl
            // 
            this.pnlSRBWaterSolenoidControl.Controls.Add(this.rdSRBWaterSolenoidControlNo);
            this.pnlSRBWaterSolenoidControl.Controls.Add(this.rdSRBWaterSolenoidControlYes);
            this.pnlSRBWaterSolenoidControl.Location = new System.Drawing.Point(1130, 20);
            this.pnlSRBWaterSolenoidControl.Name = "pnlSRBWaterSolenoidControl";
            this.pnlSRBWaterSolenoidControl.Size = new System.Drawing.Size(160, 31);
            this.pnlSRBWaterSolenoidControl.TabIndex = 35;
            // 
            // rdSRBWaterSolenoidControlNo
            // 
            this.rdSRBWaterSolenoidControlNo.AutoSize = true;
            this.rdSRBWaterSolenoidControlNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRBWaterSolenoidControlNo.Name = "rdSRBWaterSolenoidControlNo";
            this.rdSRBWaterSolenoidControlNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBWaterSolenoidControlNo.TabIndex = 1;
            this.rdSRBWaterSolenoidControlNo.TabStop = true;
            this.rdSRBWaterSolenoidControlNo.Tag = "SingleRail";
            this.rdSRBWaterSolenoidControlNo.Text = "NO";
            this.rdSRBWaterSolenoidControlNo.UseVisualStyleBackColor = true;
            this.rdSRBWaterSolenoidControlNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBWaterSolenoidControlNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBWaterSolenoidControlYes
            // 
            this.rdSRBWaterSolenoidControlYes.AutoSize = true;
            this.rdSRBWaterSolenoidControlYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRBWaterSolenoidControlYes.Name = "rdSRBWaterSolenoidControlYes";
            this.rdSRBWaterSolenoidControlYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBWaterSolenoidControlYes.TabIndex = 0;
            this.rdSRBWaterSolenoidControlYes.TabStop = true;
            this.rdSRBWaterSolenoidControlYes.Tag = "SingleRail";
            this.rdSRBWaterSolenoidControlYes.Text = "YES";
            this.rdSRBWaterSolenoidControlYes.UseVisualStyleBackColor = true;
            this.rdSRBWaterSolenoidControlYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(1005, 29);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(119, 13);
            this.label144.TabIndex = 34;
            this.label144.Text = "Water Solenoid Control:";
            // 
            // ddlSRBFeed
            // 
            this.ddlSRBFeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSRBFeed.FormattingEnabled = true;
            this.ddlSRBFeed.Location = new System.Drawing.Point(812, 193);
            this.ddlSRBFeed.Name = "ddlSRBFeed";
            this.ddlSRBFeed.Size = new System.Drawing.Size(160, 21);
            this.ddlSRBFeed.TabIndex = 33;
            this.ddlSRBFeed.Tag = "SingleRail";
            this.ddlSRBFeed.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(772, 195);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(34, 13);
            this.label136.TabIndex = 32;
            this.label136.Text = "Feed:";
            // 
            // pnlSRBEV
            // 
            this.pnlSRBEV.Controls.Add(this.rdSRBEVNo);
            this.pnlSRBEV.Controls.Add(this.rdSRBEVYes);
            this.pnlSRBEV.Location = new System.Drawing.Point(487, 176);
            this.pnlSRBEV.Name = "pnlSRBEV";
            this.pnlSRBEV.Size = new System.Drawing.Size(164, 31);
            this.pnlSRBEV.TabIndex = 31;
            // 
            // rdSRBEVNo
            // 
            this.rdSRBEVNo.AutoSize = true;
            this.rdSRBEVNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRBEVNo.Name = "rdSRBEVNo";
            this.rdSRBEVNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBEVNo.TabIndex = 1;
            this.rdSRBEVNo.TabStop = true;
            this.rdSRBEVNo.Tag = "SingleRail";
            this.rdSRBEVNo.Text = "NO";
            this.rdSRBEVNo.UseVisualStyleBackColor = true;
            this.rdSRBEVNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBEVNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBEVYes
            // 
            this.rdSRBEVYes.AutoSize = true;
            this.rdSRBEVYes.Location = new System.Drawing.Point(26, 7);
            this.rdSRBEVYes.Name = "rdSRBEVYes";
            this.rdSRBEVYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBEVYes.TabIndex = 0;
            this.rdSRBEVYes.TabStop = true;
            this.rdSRBEVYes.Tag = "SingleRail";
            this.rdSRBEVYes.Text = "YES";
            this.rdSRBEVYes.UseVisualStyleBackColor = true;
            this.rdSRBEVYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label128
            // 
            this.label128.Location = new System.Drawing.Point(379, 176);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(98, 32);
            this.label128.TabIndex = 30;
            this.label128.Text = "EV, VPD, Third Party Control:";
            this.label128.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSRBTip3
            // 
            this.txtSRBTip3.Location = new System.Drawing.Point(812, 158);
            this.txtSRBTip3.Name = "txtSRBTip3";
            this.txtSRBTip3.Size = new System.Drawing.Size(162, 20);
            this.txtSRBTip3.TabIndex = 29;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(771, 161);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(34, 13);
            this.label114.TabIndex = 28;
            this.label114.Text = "Tip 3:";
            // 
            // txtSRBTip2
            // 
            this.txtSRBTip2.Location = new System.Drawing.Point(812, 133);
            this.txtSRBTip2.Name = "txtSRBTip2";
            this.txtSRBTip2.Size = new System.Drawing.Size(162, 20);
            this.txtSRBTip2.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(771, 136);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(34, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "Tip 2:";
            // 
            // txtSRBTip1
            // 
            this.txtSRBTip1.Location = new System.Drawing.Point(812, 108);
            this.txtSRBTip1.Name = "txtSRBTip1";
            this.txtSRBTip1.Size = new System.Drawing.Size(162, 20);
            this.txtSRBTip1.TabIndex = 25;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(771, 110);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(34, 13);
            this.label112.TabIndex = 24;
            this.label112.Text = "Tip 1:";
            // 
            // txtSRBNumberOfRows
            // 
            this.txtSRBNumberOfRows.Location = new System.Drawing.Point(190, 95);
            this.txtSRBNumberOfRows.Mask = "G";
            this.txtSRBNumberOfRows.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRBNumberOfRows.Name = "txtSRBNumberOfRows";
            this.txtSRBNumberOfRows.Size = new System.Drawing.Size(160, 20);
            this.txtSRBNumberOfRows.TabIndex = 23;
            this.txtSRBNumberOfRows.TabStop = false;
            this.txtSRBNumberOfRows.Tag = "SingleRail";
            this.txtSRBNumberOfRows.Text = "0";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(89, 101);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(91, 13);
            this.label96.TabIndex = 22;
            this.label96.Text = "Number Of Rows:";
            // 
            // pnlSRBAMIADFilderAssy
            // 
            this.pnlSRBAMIADFilderAssy.Controls.Add(this.rdSRBAMIADFilderAssyNo);
            this.pnlSRBAMIADFilderAssy.Controls.Add(this.rdSRBAMIADFilderAssyYes);
            this.pnlSRBAMIADFilderAssy.Location = new System.Drawing.Point(813, 221);
            this.pnlSRBAMIADFilderAssy.Name = "pnlSRBAMIADFilderAssy";
            this.pnlSRBAMIADFilderAssy.Size = new System.Drawing.Size(160, 31);
            this.pnlSRBAMIADFilderAssy.TabIndex = 21;
            // 
            // rdSRBAMIADFilderAssyNo
            // 
            this.rdSRBAMIADFilderAssyNo.AutoSize = true;
            this.rdSRBAMIADFilderAssyNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRBAMIADFilderAssyNo.Name = "rdSRBAMIADFilderAssyNo";
            this.rdSRBAMIADFilderAssyNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBAMIADFilderAssyNo.TabIndex = 1;
            this.rdSRBAMIADFilderAssyNo.TabStop = true;
            this.rdSRBAMIADFilderAssyNo.Tag = "SingleRail";
            this.rdSRBAMIADFilderAssyNo.Text = "NO";
            this.rdSRBAMIADFilderAssyNo.UseVisualStyleBackColor = true;
            this.rdSRBAMIADFilderAssyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBAMIADFilderAssyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBAMIADFilderAssyYes
            // 
            this.rdSRBAMIADFilderAssyYes.AutoSize = true;
            this.rdSRBAMIADFilderAssyYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRBAMIADFilderAssyYes.Name = "rdSRBAMIADFilderAssyYes";
            this.rdSRBAMIADFilderAssyYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBAMIADFilderAssyYes.TabIndex = 0;
            this.rdSRBAMIADFilderAssyYes.TabStop = true;
            this.rdSRBAMIADFilderAssyYes.Tag = "SingleRail";
            this.rdSRBAMIADFilderAssyYes.Text = "YES";
            this.rdSRBAMIADFilderAssyYes.UseVisualStyleBackColor = true;
            this.rdSRBAMIADFilderAssyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(700, 230);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(108, 13);
            this.label42.TabIndex = 20;
            this.label42.Text = "1\" AMIAD Filter Assy:";
            // 
            // pnlSRBTripleTurretBody
            // 
            this.pnlSRBTripleTurretBody.Controls.Add(this.rdSRBTripleTurretBodyNo);
            this.pnlSRBTripleTurretBody.Controls.Add(this.rdSRBTripleTurretBodyYes);
            this.pnlSRBTripleTurretBody.Location = new System.Drawing.Point(813, 63);
            this.pnlSRBTripleTurretBody.Name = "pnlSRBTripleTurretBody";
            this.pnlSRBTripleTurretBody.Size = new System.Drawing.Size(160, 31);
            this.pnlSRBTripleTurretBody.TabIndex = 9;
            // 
            // rdSRBTripleTurretBodyNo
            // 
            this.rdSRBTripleTurretBodyNo.AutoSize = true;
            this.rdSRBTripleTurretBodyNo.Location = new System.Drawing.Point(86, 7);
            this.rdSRBTripleTurretBodyNo.Name = "rdSRBTripleTurretBodyNo";
            this.rdSRBTripleTurretBodyNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBTripleTurretBodyNo.TabIndex = 1;
            this.rdSRBTripleTurretBodyNo.TabStop = true;
            this.rdSRBTripleTurretBodyNo.Tag = "SingleRail";
            this.rdSRBTripleTurretBodyNo.Text = "NO";
            this.rdSRBTripleTurretBodyNo.UseVisualStyleBackColor = true;
            this.rdSRBTripleTurretBodyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBTripleTurretBodyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBTripleTurretBodyYes
            // 
            this.rdSRBTripleTurretBodyYes.AutoSize = true;
            this.rdSRBTripleTurretBodyYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRBTripleTurretBodyYes.Name = "rdSRBTripleTurretBodyYes";
            this.rdSRBTripleTurretBodyYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBTripleTurretBodyYes.TabIndex = 0;
            this.rdSRBTripleTurretBodyYes.TabStop = true;
            this.rdSRBTripleTurretBodyYes.Tag = "SingleRail";
            this.rdSRBTripleTurretBodyYes.Text = "YES";
            this.rdSRBTripleTurretBodyYes.UseVisualStyleBackColor = true;
            this.rdSRBTripleTurretBodyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(710, 67);
            this.label53.MaximumSize = new System.Drawing.Size(150, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(94, 13);
            this.label53.TabIndex = 8;
            this.label53.Text = "Triple Turret Body:";
            // 
            // txtSRBQuantityOfSystems
            // 
            this.txtSRBQuantityOfSystems.Location = new System.Drawing.Point(190, 20);
            this.txtSRBQuantityOfSystems.Mask = "G";
            this.txtSRBQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRBQuantityOfSystems.Name = "txtSRBQuantityOfSystems";
            this.txtSRBQuantityOfSystems.Size = new System.Drawing.Size(160, 20);
            this.txtSRBQuantityOfSystems.TabIndex = 1;
            this.txtSRBQuantityOfSystems.TabStop = false;
            this.txtSRBQuantityOfSystems.Tag = "SingleRail";
            this.txtSRBQuantityOfSystems.Text = "0";
            this.txtSRBQuantityOfSystems.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // pnlSRBMountedInjector
            // 
            this.pnlSRBMountedInjector.Controls.Add(this.rdSRBMountedInjectorNo);
            this.pnlSRBMountedInjector.Controls.Add(this.rdSRBMountedInjectorYes);
            this.pnlSRBMountedInjector.Location = new System.Drawing.Point(489, 58);
            this.pnlSRBMountedInjector.Name = "pnlSRBMountedInjector";
            this.pnlSRBMountedInjector.Size = new System.Drawing.Size(161, 31);
            this.pnlSRBMountedInjector.TabIndex = 17;
            // 
            // rdSRBMountedInjectorNo
            // 
            this.rdSRBMountedInjectorNo.AutoSize = true;
            this.rdSRBMountedInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRBMountedInjectorNo.Name = "rdSRBMountedInjectorNo";
            this.rdSRBMountedInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBMountedInjectorNo.TabIndex = 1;
            this.rdSRBMountedInjectorNo.TabStop = true;
            this.rdSRBMountedInjectorNo.Tag = "SingleRail";
            this.rdSRBMountedInjectorNo.Text = "NO";
            this.rdSRBMountedInjectorNo.UseVisualStyleBackColor = true;
            this.rdSRBMountedInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBMountedInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBMountedInjectorYes
            // 
            this.rdSRBMountedInjectorYes.AutoSize = true;
            this.rdSRBMountedInjectorYes.Location = new System.Drawing.Point(23, 7);
            this.rdSRBMountedInjectorYes.Name = "rdSRBMountedInjectorYes";
            this.rdSRBMountedInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBMountedInjectorYes.TabIndex = 0;
            this.rdSRBMountedInjectorYes.TabStop = true;
            this.rdSRBMountedInjectorYes.Tag = "SingleRail";
            this.rdSRBMountedInjectorYes.Text = "YES";
            this.rdSRBMountedInjectorYes.UseVisualStyleBackColor = true;
            this.rdSRBMountedInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(433, 67);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(45, 13);
            this.label41.TabIndex = 16;
            this.label41.Text = "Injector:";
            // 
            // pnlSRBPressureRegulator
            // 
            this.pnlSRBPressureRegulator.Controls.Add(this.radioButton1);
            this.pnlSRBPressureRegulator.Controls.Add(this.rdSRBPressureRegulatorNo);
            this.pnlSRBPressureRegulator.Controls.Add(this.rdSRBPressureRegulatorYes);
            this.pnlSRBPressureRegulator.Location = new System.Drawing.Point(489, 20);
            this.pnlSRBPressureRegulator.Name = "pnlSRBPressureRegulator";
            this.pnlSRBPressureRegulator.Size = new System.Drawing.Size(161, 31);
            this.pnlSRBPressureRegulator.TabIndex = 15;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(-290, 41);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(41, 17);
            this.radioButton1.TabIndex = 46;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "Echo";
            this.radioButton1.Text = "NO";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // rdSRBPressureRegulatorNo
            // 
            this.rdSRBPressureRegulatorNo.AutoSize = true;
            this.rdSRBPressureRegulatorNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRBPressureRegulatorNo.Name = "rdSRBPressureRegulatorNo";
            this.rdSRBPressureRegulatorNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBPressureRegulatorNo.TabIndex = 1;
            this.rdSRBPressureRegulatorNo.TabStop = true;
            this.rdSRBPressureRegulatorNo.Tag = "SingleRail";
            this.rdSRBPressureRegulatorNo.Text = "NO";
            this.rdSRBPressureRegulatorNo.UseVisualStyleBackColor = true;
            this.rdSRBPressureRegulatorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBPressureRegulatorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBPressureRegulatorYes
            // 
            this.rdSRBPressureRegulatorYes.AutoSize = true;
            this.rdSRBPressureRegulatorYes.Location = new System.Drawing.Point(23, 7);
            this.rdSRBPressureRegulatorYes.Name = "rdSRBPressureRegulatorYes";
            this.rdSRBPressureRegulatorYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBPressureRegulatorYes.TabIndex = 0;
            this.rdSRBPressureRegulatorYes.TabStop = true;
            this.rdSRBPressureRegulatorYes.Tag = "SingleRail";
            this.rdSRBPressureRegulatorYes.Text = "YES";
            this.rdSRBPressureRegulatorYes.UseVisualStyleBackColor = true;
            this.rdSRBPressureRegulatorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(380, 27);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(100, 13);
            this.label43.TabIndex = 14;
            this.label43.Text = "Pressure Regulator:";
            // 
            // txtSRBSpraybodySpacing
            // 
            this.txtSRBSpraybodySpacing.Location = new System.Drawing.Point(189, 157);
            this.txtSRBSpraybodySpacing.Mask = "F2";
            this.txtSRBSpraybodySpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRBSpraybodySpacing.Name = "txtSRBSpraybodySpacing";
            this.txtSRBSpraybodySpacing.Size = new System.Drawing.Size(161, 20);
            this.txtSRBSpraybodySpacing.TabIndex = 19;
            this.txtSRBSpraybodySpacing.TabStop = false;
            this.txtSRBSpraybodySpacing.Tag = "SingleRail";
            this.txtSRBSpraybodySpacing.Text = "0.00";
            this.txtSRBSpraybodySpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(56, 158);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(122, 13);
            this.label44.TabIndex = 18;
            this.label44.Text = "Spraybody Spacing (IN):";
            // 
            // pnlSRBSweep90Degree
            // 
            this.pnlSRBSweep90Degree.Controls.Add(this.rdSRBSweep90DegreeNo);
            this.pnlSRBSweep90Degree.Controls.Add(this.rdSRBSweep90DegreeYes);
            this.pnlSRBSweep90Degree.Location = new System.Drawing.Point(812, 22);
            this.pnlSRBSweep90Degree.Name = "pnlSRBSweep90Degree";
            this.pnlSRBSweep90Degree.Size = new System.Drawing.Size(164, 31);
            this.pnlSRBSweep90Degree.TabIndex = 13;
            // 
            // rdSRBSweep90DegreeNo
            // 
            this.rdSRBSweep90DegreeNo.AutoSize = true;
            this.rdSRBSweep90DegreeNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRBSweep90DegreeNo.Name = "rdSRBSweep90DegreeNo";
            this.rdSRBSweep90DegreeNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBSweep90DegreeNo.TabIndex = 1;
            this.rdSRBSweep90DegreeNo.TabStop = true;
            this.rdSRBSweep90DegreeNo.Tag = "SingleRail";
            this.rdSRBSweep90DegreeNo.Text = "NO";
            this.rdSRBSweep90DegreeNo.UseVisualStyleBackColor = true;
            this.rdSRBSweep90DegreeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBSweep90DegreeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBSweep90DegreeYes
            // 
            this.rdSRBSweep90DegreeYes.AutoSize = true;
            this.rdSRBSweep90DegreeYes.Location = new System.Drawing.Point(26, 6);
            this.rdSRBSweep90DegreeYes.Name = "rdSRBSweep90DegreeYes";
            this.rdSRBSweep90DegreeYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBSweep90DegreeYes.TabIndex = 0;
            this.rdSRBSweep90DegreeYes.TabStop = true;
            this.rdSRBSweep90DegreeYes.Tag = "SingleRail";
            this.rdSRBSweep90DegreeYes.Text = "YES";
            this.rdSRBSweep90DegreeYes.UseVisualStyleBackColor = true;
            this.rdSRBSweep90DegreeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(704, 30);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(102, 13);
            this.label45.TabIndex = 12;
            this.label45.Text = "Sweep - 90 Degree:";
            // 
            // pnlSRBTripleWaterLocklineUpgrade
            // 
            this.pnlSRBTripleWaterLocklineUpgrade.Controls.Add(this.rdSRBTripleWaterLocklineUpgradeNo);
            this.pnlSRBTripleWaterLocklineUpgrade.Controls.Add(this.rdSRBTripleWaterLocklineUpgradeYes);
            this.pnlSRBTripleWaterLocklineUpgrade.Location = new System.Drawing.Point(190, 214);
            this.pnlSRBTripleWaterLocklineUpgrade.Name = "pnlSRBTripleWaterLocklineUpgrade";
            this.pnlSRBTripleWaterLocklineUpgrade.Size = new System.Drawing.Size(160, 31);
            this.pnlSRBTripleWaterLocklineUpgrade.TabIndex = 7;
            // 
            // rdSRBTripleWaterLocklineUpgradeNo
            // 
            this.rdSRBTripleWaterLocklineUpgradeNo.AutoSize = true;
            this.rdSRBTripleWaterLocklineUpgradeNo.Location = new System.Drawing.Point(98, 7);
            this.rdSRBTripleWaterLocklineUpgradeNo.Name = "rdSRBTripleWaterLocklineUpgradeNo";
            this.rdSRBTripleWaterLocklineUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRBTripleWaterLocklineUpgradeNo.TabIndex = 1;
            this.rdSRBTripleWaterLocklineUpgradeNo.TabStop = true;
            this.rdSRBTripleWaterLocklineUpgradeNo.Tag = "SingleRail";
            this.rdSRBTripleWaterLocklineUpgradeNo.Text = "NO";
            this.rdSRBTripleWaterLocklineUpgradeNo.UseVisualStyleBackColor = true;
            this.rdSRBTripleWaterLocklineUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRBTripleWaterLocklineUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRBTripleWaterLocklineUpgradeYes
            // 
            this.rdSRBTripleWaterLocklineUpgradeYes.AutoSize = true;
            this.rdSRBTripleWaterLocklineUpgradeYes.Location = new System.Drawing.Point(26, 7);
            this.rdSRBTripleWaterLocklineUpgradeYes.Name = "rdSRBTripleWaterLocklineUpgradeYes";
            this.rdSRBTripleWaterLocklineUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRBTripleWaterLocklineUpgradeYes.TabIndex = 0;
            this.rdSRBTripleWaterLocklineUpgradeYes.TabStop = true;
            this.rdSRBTripleWaterLocklineUpgradeYes.Tag = "SingleRail";
            this.rdSRBTripleWaterLocklineUpgradeYes.Text = "YES";
            this.rdSRBTripleWaterLocklineUpgradeYes.UseVisualStyleBackColor = true;
            this.rdSRBTripleWaterLocklineUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(46, 217);
            this.label47.MaximumSize = new System.Drawing.Size(150, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(136, 26);
            this.label47.TabIndex = 6;
            this.label47.Text = "Step/Stop/Water Lockline Upgrade:";
            // 
            // ddlSRBControllerType
            // 
            this.ddlSRBControllerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSRBControllerType.FormattingEnabled = true;
            this.ddlSRBControllerType.Location = new System.Drawing.Point(189, 128);
            this.ddlSRBControllerType.Name = "ddlSRBControllerType";
            this.ddlSRBControllerType.Size = new System.Drawing.Size(162, 21);
            this.ddlSRBControllerType.TabIndex = 11;
            this.ddlSRBControllerType.Tag = "SingleRail";
            this.ddlSRBControllerType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(97, 130);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(81, 13);
            this.label49.TabIndex = 10;
            this.label49.Text = "Controller Type:";
            // 
            // txtSRBHoseLoopHeight
            // 
            this.txtSRBHoseLoopHeight.Location = new System.Drawing.Point(190, 188);
            this.txtSRBHoseLoopHeight.Mask = "F2";
            this.txtSRBHoseLoopHeight.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRBHoseLoopHeight.Name = "txtSRBHoseLoopHeight";
            this.txtSRBHoseLoopHeight.Size = new System.Drawing.Size(160, 20);
            this.txtSRBHoseLoopHeight.TabIndex = 3;
            this.txtSRBHoseLoopHeight.TabStop = false;
            this.txtSRBHoseLoopHeight.Tag = "SingleRail";
            this.txtSRBHoseLoopHeight.Text = "0.00";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(60, 188);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(116, 13);
            this.label50.TabIndex = 2;
            this.label50.Text = "Hose Loop Height (IN):";
            // 
            // ddlSRBSingleDualWater
            // 
            this.ddlSRBSingleDualWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSRBSingleDualWater.FormattingEnabled = true;
            this.ddlSRBSingleDualWater.Location = new System.Drawing.Point(190, 58);
            this.ddlSRBSingleDualWater.Name = "ddlSRBSingleDualWater";
            this.ddlSRBSingleDualWater.Size = new System.Drawing.Size(160, 21);
            this.ddlSRBSingleDualWater.TabIndex = 5;
            this.ddlSRBSingleDualWater.Tag = "SingleRail";
            this.ddlSRBSingleDualWater.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(89, 63);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(98, 13);
            this.label51.TabIndex = 4;
            this.label51.Text = "Single/Dual Water:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(77, 22);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(105, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Quantity Of Systems:";
            // 
            // tpDoubleRailBoom
            // 
            this.tpDoubleRailBoom.Controls.Add(this.ddlDRBEVWireGauge);
            this.tpDoubleRailBoom.Controls.Add(this.label214);
            this.tpDoubleRailBoom.Controls.Add(this.ddlDRBInjectorType);
            this.tpDoubleRailBoom.Controls.Add(this.label150);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBAutoInjector);
            this.tpDoubleRailBoom.Controls.Add(this.label149);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBWaterSolenoidControl);
            this.tpDoubleRailBoom.Controls.Add(this.label145);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBEV);
            this.tpDoubleRailBoom.Controls.Add(this.label129);
            this.tpDoubleRailBoom.Controls.Add(this.txtDRBTip3);
            this.tpDoubleRailBoom.Controls.Add(this.label115);
            this.tpDoubleRailBoom.Controls.Add(this.txtDRBTip2);
            this.tpDoubleRailBoom.Controls.Add(this.label116);
            this.tpDoubleRailBoom.Controls.Add(this.txtDRBTip1);
            this.tpDoubleRailBoom.Controls.Add(this.label117);
            this.tpDoubleRailBoom.Controls.Add(this.txtDRBNumberOfRows);
            this.tpDoubleRailBoom.Controls.Add(this.label101);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBWirelessWalkSwitch);
            this.tpDoubleRailBoom.Controls.Add(this.label64);
            this.tpDoubleRailBoom.Controls.Add(this.txtDRBRailDropDownAssy);
            this.tpDoubleRailBoom.Controls.Add(this.label63);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRB1_1_4thHoseUpgrade);
            this.tpDoubleRailBoom.Controls.Add(this.label3);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBTripleTurretBody);
            this.tpDoubleRailBoom.Controls.Add(this.label48);
            this.tpDoubleRailBoom.Controls.Add(this.txtDRBQuantityOfSystems);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBMountedInjector);
            this.tpDoubleRailBoom.Controls.Add(this.label54);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBPressureRegulator);
            this.tpDoubleRailBoom.Controls.Add(this.label55);
            this.tpDoubleRailBoom.Controls.Add(this.txtDRBSpraybodySpacing);
            this.tpDoubleRailBoom.Controls.Add(this.label56);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBSweep90);
            this.tpDoubleRailBoom.Controls.Add(this.label57);
            this.tpDoubleRailBoom.Controls.Add(this.pnlDRBTripleWaterLocklineUpgrade);
            this.tpDoubleRailBoom.Controls.Add(this.label58);
            this.tpDoubleRailBoom.Controls.Add(this.ddlDRBControllerType);
            this.tpDoubleRailBoom.Controls.Add(this.label59);
            this.tpDoubleRailBoom.Controls.Add(this.txtDRBHoseLoopHeight);
            this.tpDoubleRailBoom.Controls.Add(this.label60);
            this.tpDoubleRailBoom.Controls.Add(this.ddlDRBSingleDualWater);
            this.tpDoubleRailBoom.Controls.Add(this.label61);
            this.tpDoubleRailBoom.Controls.Add(this.label62);
            this.tpDoubleRailBoom.Location = new System.Drawing.Point(4, 22);
            this.tpDoubleRailBoom.Margin = new System.Windows.Forms.Padding(2);
            this.tpDoubleRailBoom.Name = "tpDoubleRailBoom";
            this.tpDoubleRailBoom.Padding = new System.Windows.Forms.Padding(2);
            this.tpDoubleRailBoom.Size = new System.Drawing.Size(1368, 267);
            this.tpDoubleRailBoom.TabIndex = 2;
            this.tpDoubleRailBoom.Tag = "DoubleRail";
            this.tpDoubleRailBoom.Text = "Double Rail";
            this.tpDoubleRailBoom.UseVisualStyleBackColor = true;
            // 
            // ddlDRBEVWireGauge
            // 
            this.ddlDRBEVWireGauge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlDRBEVWireGauge.FormattingEnabled = true;
            this.ddlDRBEVWireGauge.Location = new System.Drawing.Point(476, 221);
            this.ddlDRBEVWireGauge.Name = "ddlDRBEVWireGauge";
            this.ddlDRBEVWireGauge.Size = new System.Drawing.Size(164, 21);
            this.ddlDRBEVWireGauge.TabIndex = 51;
            this.ddlDRBEVWireGauge.Tag = "SingleRail";
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(381, 224);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(86, 13);
            this.label214.TabIndex = 50;
            this.label214.Text = "EV Wire/Gauge:";
            // 
            // ddlDRBInjectorType
            // 
            this.ddlDRBInjectorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlDRBInjectorType.FormattingEnabled = true;
            this.ddlDRBInjectorType.Location = new System.Drawing.Point(476, 101);
            this.ddlDRBInjectorType.Name = "ddlDRBInjectorType";
            this.ddlDRBInjectorType.Size = new System.Drawing.Size(160, 21);
            this.ddlDRBInjectorType.TabIndex = 43;
            this.ddlDRBInjectorType.Tag = "DoubleRail";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(394, 103);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(72, 13);
            this.label150.TabIndex = 42;
            this.label150.Text = "Injector Type:";
            // 
            // pnlDRBAutoInjector
            // 
            this.pnlDRBAutoInjector.Controls.Add(this.rdDRBAutoInjectorNo);
            this.pnlDRBAutoInjector.Controls.Add(this.rdDRBAutoInjectorYes);
            this.pnlDRBAutoInjector.Location = new System.Drawing.Point(477, 132);
            this.pnlDRBAutoInjector.Name = "pnlDRBAutoInjector";
            this.pnlDRBAutoInjector.Size = new System.Drawing.Size(160, 31);
            this.pnlDRBAutoInjector.TabIndex = 41;
            // 
            // rdDRBAutoInjectorNo
            // 
            this.rdDRBAutoInjectorNo.AutoSize = true;
            this.rdDRBAutoInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdDRBAutoInjectorNo.Name = "rdDRBAutoInjectorNo";
            this.rdDRBAutoInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRBAutoInjectorNo.TabIndex = 1;
            this.rdDRBAutoInjectorNo.TabStop = true;
            this.rdDRBAutoInjectorNo.Tag = "DoubleRail";
            this.rdDRBAutoInjectorNo.Text = "NO";
            this.rdDRBAutoInjectorNo.UseVisualStyleBackColor = true;
            this.rdDRBAutoInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBAutoInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBAutoInjectorYes
            // 
            this.rdDRBAutoInjectorYes.AutoSize = true;
            this.rdDRBAutoInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdDRBAutoInjectorYes.Name = "rdDRBAutoInjectorYes";
            this.rdDRBAutoInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBAutoInjectorYes.TabIndex = 0;
            this.rdDRBAutoInjectorYes.TabStop = true;
            this.rdDRBAutoInjectorYes.Tag = "DoubleRail";
            this.rdDRBAutoInjectorYes.Text = "YES";
            this.rdDRBAutoInjectorYes.UseVisualStyleBackColor = true;
            this.rdDRBAutoInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(397, 141);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(70, 13);
            this.label149.TabIndex = 40;
            this.label149.Text = "Auto Injector:";
            // 
            // pnlDRBWaterSolenoidControl
            // 
            this.pnlDRBWaterSolenoidControl.Controls.Add(this.rdDRBWaterSolenoidControlNo);
            this.pnlDRBWaterSolenoidControl.Controls.Add(this.rdDRBWaterSolenoidControlYes);
            this.pnlDRBWaterSolenoidControl.Location = new System.Drawing.Point(1128, 12);
            this.pnlDRBWaterSolenoidControl.Name = "pnlDRBWaterSolenoidControl";
            this.pnlDRBWaterSolenoidControl.Size = new System.Drawing.Size(155, 31);
            this.pnlDRBWaterSolenoidControl.TabIndex = 39;
            // 
            // rdDRBWaterSolenoidControlNo
            // 
            this.rdDRBWaterSolenoidControlNo.AutoSize = true;
            this.rdDRBWaterSolenoidControlNo.Location = new System.Drawing.Point(80, 7);
            this.rdDRBWaterSolenoidControlNo.Name = "rdDRBWaterSolenoidControlNo";
            this.rdDRBWaterSolenoidControlNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRBWaterSolenoidControlNo.TabIndex = 1;
            this.rdDRBWaterSolenoidControlNo.TabStop = true;
            this.rdDRBWaterSolenoidControlNo.Tag = "DoubleRail";
            this.rdDRBWaterSolenoidControlNo.Text = "NO";
            this.rdDRBWaterSolenoidControlNo.UseVisualStyleBackColor = true;
            this.rdDRBWaterSolenoidControlNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBWaterSolenoidControlNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBWaterSolenoidControlYes
            // 
            this.rdDRBWaterSolenoidControlYes.AutoSize = true;
            this.rdDRBWaterSolenoidControlYes.Location = new System.Drawing.Point(19, 7);
            this.rdDRBWaterSolenoidControlYes.Name = "rdDRBWaterSolenoidControlYes";
            this.rdDRBWaterSolenoidControlYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBWaterSolenoidControlYes.TabIndex = 0;
            this.rdDRBWaterSolenoidControlYes.TabStop = true;
            this.rdDRBWaterSolenoidControlYes.Tag = "DoubleRail";
            this.rdDRBWaterSolenoidControlYes.Text = "YES";
            this.rdDRBWaterSolenoidControlYes.UseVisualStyleBackColor = true;
            this.rdDRBWaterSolenoidControlYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(1001, 18);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(119, 13);
            this.label145.TabIndex = 38;
            this.label145.Text = "Water Solenoid Control:";
            // 
            // pnlDRBEV
            // 
            this.pnlDRBEV.Controls.Add(this.rdDRBEVNo);
            this.pnlDRBEV.Controls.Add(this.rdDRBEVYes);
            this.pnlDRBEV.Location = new System.Drawing.Point(477, 177);
            this.pnlDRBEV.Name = "pnlDRBEV";
            this.pnlDRBEV.Size = new System.Drawing.Size(160, 31);
            this.pnlDRBEV.TabIndex = 37;
            // 
            // rdDRBEVNo
            // 
            this.rdDRBEVNo.AutoSize = true;
            this.rdDRBEVNo.Location = new System.Drawing.Point(87, 7);
            this.rdDRBEVNo.Name = "rdDRBEVNo";
            this.rdDRBEVNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRBEVNo.TabIndex = 1;
            this.rdDRBEVNo.TabStop = true;
            this.rdDRBEVNo.Tag = "DoubleRail";
            this.rdDRBEVNo.Text = "NO";
            this.rdDRBEVNo.UseVisualStyleBackColor = true;
            this.rdDRBEVNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBEVNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBEVYes
            // 
            this.rdDRBEVYes.AutoSize = true;
            this.rdDRBEVYes.Location = new System.Drawing.Point(19, 7);
            this.rdDRBEVYes.Name = "rdDRBEVYes";
            this.rdDRBEVYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBEVYes.TabIndex = 0;
            this.rdDRBEVYes.TabStop = true;
            this.rdDRBEVYes.Tag = "DoubleRail";
            this.rdDRBEVYes.Text = "YES";
            this.rdDRBEVYes.UseVisualStyleBackColor = true;
            this.rdDRBEVYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label129
            // 
            this.label129.Location = new System.Drawing.Point(387, 175);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(84, 37);
            this.label129.TabIndex = 36;
            this.label129.Text = "EV, VPD, Third Party Control:";
            // 
            // txtDRBTip3
            // 
            this.txtDRBTip3.Location = new System.Drawing.Point(789, 141);
            this.txtDRBTip3.Name = "txtDRBTip3";
            this.txtDRBTip3.Size = new System.Drawing.Size(156, 20);
            this.txtDRBTip3.TabIndex = 35;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(748, 143);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(34, 13);
            this.label115.TabIndex = 34;
            this.label115.Text = "Tip 3:";
            // 
            // txtDRBTip2
            // 
            this.txtDRBTip2.Location = new System.Drawing.Point(789, 116);
            this.txtDRBTip2.Name = "txtDRBTip2";
            this.txtDRBTip2.Size = new System.Drawing.Size(156, 20);
            this.txtDRBTip2.TabIndex = 33;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(748, 118);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(34, 13);
            this.label116.TabIndex = 32;
            this.label116.Text = "Tip 2:";
            // 
            // txtDRBTip1
            // 
            this.txtDRBTip1.Location = new System.Drawing.Point(789, 90);
            this.txtDRBTip1.Name = "txtDRBTip1";
            this.txtDRBTip1.Size = new System.Drawing.Size(156, 20);
            this.txtDRBTip1.TabIndex = 31;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(748, 93);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(34, 13);
            this.label117.TabIndex = 30;
            this.label117.Text = "Tip 1:";
            // 
            // txtDRBNumberOfRows
            // 
            this.txtDRBNumberOfRows.Location = new System.Drawing.Point(185, 75);
            this.txtDRBNumberOfRows.Mask = "G";
            this.txtDRBNumberOfRows.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtDRBNumberOfRows.Name = "txtDRBNumberOfRows";
            this.txtDRBNumberOfRows.Size = new System.Drawing.Size(158, 20);
            this.txtDRBNumberOfRows.TabIndex = 27;
            this.txtDRBNumberOfRows.TabStop = false;
            this.txtDRBNumberOfRows.Tag = "SingleRail";
            this.txtDRBNumberOfRows.Text = "0";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(86, 78);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(91, 13);
            this.label101.TabIndex = 26;
            this.label101.Text = "Number Of Rows:";
            // 
            // pnlDRBWirelessWalkSwitch
            // 
            this.pnlDRBWirelessWalkSwitch.Controls.Add(this.rdDRBWirelessWalkSwitchNo);
            this.pnlDRBWirelessWalkSwitch.Controls.Add(this.rdDRBWirelessWalkSwitchYes);
            this.pnlDRBWirelessWalkSwitch.Location = new System.Drawing.Point(789, 217);
            this.pnlDRBWirelessWalkSwitch.Name = "pnlDRBWirelessWalkSwitch";
            this.pnlDRBWirelessWalkSwitch.Size = new System.Drawing.Size(155, 31);
            this.pnlDRBWirelessWalkSwitch.TabIndex = 25;
            // 
            // rdDRBWirelessWalkSwitchNo
            // 
            this.rdDRBWirelessWalkSwitchNo.AutoSize = true;
            this.rdDRBWirelessWalkSwitchNo.Location = new System.Drawing.Point(80, 7);
            this.rdDRBWirelessWalkSwitchNo.Name = "rdDRBWirelessWalkSwitchNo";
            this.rdDRBWirelessWalkSwitchNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRBWirelessWalkSwitchNo.TabIndex = 1;
            this.rdDRBWirelessWalkSwitchNo.TabStop = true;
            this.rdDRBWirelessWalkSwitchNo.Tag = "DoubleRail";
            this.rdDRBWirelessWalkSwitchNo.Text = "NO";
            this.rdDRBWirelessWalkSwitchNo.UseVisualStyleBackColor = true;
            this.rdDRBWirelessWalkSwitchNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBWirelessWalkSwitchNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBWirelessWalkSwitchYes
            // 
            this.rdDRBWirelessWalkSwitchYes.AutoSize = true;
            this.rdDRBWirelessWalkSwitchYes.Location = new System.Drawing.Point(20, 7);
            this.rdDRBWirelessWalkSwitchYes.Name = "rdDRBWirelessWalkSwitchYes";
            this.rdDRBWirelessWalkSwitchYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBWirelessWalkSwitchYes.TabIndex = 0;
            this.rdDRBWirelessWalkSwitchYes.TabStop = true;
            this.rdDRBWirelessWalkSwitchYes.Tag = "DoubleRail";
            this.rdDRBWirelessWalkSwitchYes.Text = "YES";
            this.rdDRBWirelessWalkSwitchYes.UseVisualStyleBackColor = true;
            this.rdDRBWirelessWalkSwitchYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label64
            // 
            this.label64.Location = new System.Drawing.Point(685, 216);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(96, 28);
            this.label64.TabIndex = 24;
            this.label64.Text = "Wireless Walk Switch:";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDRBRailDropDownAssy
            // 
            this.txtDRBRailDropDownAssy.Location = new System.Drawing.Point(184, 231);
            this.txtDRBRailDropDownAssy.Mask = "F2";
            this.txtDRBRailDropDownAssy.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtDRBRailDropDownAssy.Name = "txtDRBRailDropDownAssy";
            this.txtDRBRailDropDownAssy.Size = new System.Drawing.Size(160, 20);
            this.txtDRBRailDropDownAssy.TabIndex = 19;
            this.txtDRBRailDropDownAssy.TabStop = false;
            this.txtDRBRailDropDownAssy.Tag = "DoubleRail";
            this.txtDRBRailDropDownAssy.Text = "0.00";
            // 
            // label63
            // 
            this.label63.Location = new System.Drawing.Point(44, 234);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(132, 16);
            this.label63.TabIndex = 18;
            this.label63.Text = "Rail Drop Down Assy. (IN):";
            // 
            // pnlDRB1_1_4thHoseUpgrade
            // 
            this.pnlDRB1_1_4thHoseUpgrade.Controls.Add(this.rdDRB1_1_4thHoseUpgradeNo);
            this.pnlDRB1_1_4thHoseUpgrade.Controls.Add(this.rdDRB1_1_4thHoseUpgradeYes);
            this.pnlDRB1_1_4thHoseUpgrade.Location = new System.Drawing.Point(789, 12);
            this.pnlDRB1_1_4thHoseUpgrade.Name = "pnlDRB1_1_4thHoseUpgrade";
            this.pnlDRB1_1_4thHoseUpgrade.Size = new System.Drawing.Size(160, 31);
            this.pnlDRB1_1_4thHoseUpgrade.TabIndex = 21;
            // 
            // rdDRB1_1_4thHoseUpgradeNo
            // 
            this.rdDRB1_1_4thHoseUpgradeNo.AutoSize = true;
            this.rdDRB1_1_4thHoseUpgradeNo.Location = new System.Drawing.Point(87, 7);
            this.rdDRB1_1_4thHoseUpgradeNo.Name = "rdDRB1_1_4thHoseUpgradeNo";
            this.rdDRB1_1_4thHoseUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRB1_1_4thHoseUpgradeNo.TabIndex = 1;
            this.rdDRB1_1_4thHoseUpgradeNo.TabStop = true;
            this.rdDRB1_1_4thHoseUpgradeNo.Tag = "DoubleRail";
            this.rdDRB1_1_4thHoseUpgradeNo.Text = "NO";
            this.rdDRB1_1_4thHoseUpgradeNo.UseVisualStyleBackColor = true;
            this.rdDRB1_1_4thHoseUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRB1_1_4thHoseUpgradeNo.Click += new System.EventHandler(this.HoseUpgradeNo_Click);
            this.rdDRB1_1_4thHoseUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRB1_1_4thHoseUpgradeYes
            // 
            this.rdDRB1_1_4thHoseUpgradeYes.AutoSize = true;
            this.rdDRB1_1_4thHoseUpgradeYes.Location = new System.Drawing.Point(20, 7);
            this.rdDRB1_1_4thHoseUpgradeYes.Name = "rdDRB1_1_4thHoseUpgradeYes";
            this.rdDRB1_1_4thHoseUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRB1_1_4thHoseUpgradeYes.TabIndex = 0;
            this.rdDRB1_1_4thHoseUpgradeYes.TabStop = true;
            this.rdDRB1_1_4thHoseUpgradeYes.Tag = "DoubleRail";
            this.rdDRB1_1_4thHoseUpgradeYes.Text = "YES";
            this.rdDRB1_1_4thHoseUpgradeYes.UseVisualStyleBackColor = true;
            this.rdDRB1_1_4thHoseUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRB1_1_4thHoseUpgradeYes.Click += new System.EventHandler(this.HoseUpgradeYes_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(680, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 28);
            this.label3.TabIndex = 20;
            this.label3.Text = "1 1/4\" Hose Upgrade:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlDRBTripleTurretBody
            // 
            this.pnlDRBTripleTurretBody.Controls.Add(this.rdDRBTripleTurretBodyNo);
            this.pnlDRBTripleTurretBody.Controls.Add(this.rdDRBTripleTurretBodyYes);
            this.pnlDRBTripleTurretBody.Location = new System.Drawing.Point(789, 49);
            this.pnlDRBTripleTurretBody.Name = "pnlDRBTripleTurretBody";
            this.pnlDRBTripleTurretBody.Size = new System.Drawing.Size(155, 31);
            this.pnlDRBTripleTurretBody.TabIndex = 9;
            // 
            // rdDRBTripleTurretBodyNo
            // 
            this.rdDRBTripleTurretBodyNo.AutoSize = true;
            this.rdDRBTripleTurretBodyNo.Location = new System.Drawing.Point(87, 7);
            this.rdDRBTripleTurretBodyNo.Name = "rdDRBTripleTurretBodyNo";
            this.rdDRBTripleTurretBodyNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRBTripleTurretBodyNo.TabIndex = 1;
            this.rdDRBTripleTurretBodyNo.TabStop = true;
            this.rdDRBTripleTurretBodyNo.Tag = "DoubleRail";
            this.rdDRBTripleTurretBodyNo.Text = "NO";
            this.rdDRBTripleTurretBodyNo.UseVisualStyleBackColor = true;
            this.rdDRBTripleTurretBodyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBTripleTurretBodyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBTripleTurretBodyYes
            // 
            this.rdDRBTripleTurretBodyYes.AutoSize = true;
            this.rdDRBTripleTurretBodyYes.Location = new System.Drawing.Point(20, 7);
            this.rdDRBTripleTurretBodyYes.Name = "rdDRBTripleTurretBodyYes";
            this.rdDRBTripleTurretBodyYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBTripleTurretBodyYes.TabIndex = 0;
            this.rdDRBTripleTurretBodyYes.TabStop = true;
            this.rdDRBTripleTurretBodyYes.Tag = "DoubleRail";
            this.rdDRBTripleTurretBodyYes.Text = "YES";
            this.rdDRBTripleTurretBodyYes.UseVisualStyleBackColor = true;
            this.rdDRBTripleTurretBodyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(685, 56);
            this.label48.MaximumSize = new System.Drawing.Size(150, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(94, 13);
            this.label48.TabIndex = 8;
            this.label48.Text = "Triple Turret Body:";
            // 
            // txtDRBQuantityOfSystems
            // 
            this.txtDRBQuantityOfSystems.Location = new System.Drawing.Point(183, 14);
            this.txtDRBQuantityOfSystems.Mask = "G";
            this.txtDRBQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtDRBQuantityOfSystems.Name = "txtDRBQuantityOfSystems";
            this.txtDRBQuantityOfSystems.Size = new System.Drawing.Size(160, 20);
            this.txtDRBQuantityOfSystems.TabIndex = 1;
            this.txtDRBQuantityOfSystems.TabStop = false;
            this.txtDRBQuantityOfSystems.Tag = "DoubleRail";
            this.txtDRBQuantityOfSystems.Text = "0";
            this.txtDRBQuantityOfSystems.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // pnlDRBMountedInjector
            // 
            this.pnlDRBMountedInjector.Controls.Add(this.rdDRBMountedInjectorNo);
            this.pnlDRBMountedInjector.Controls.Add(this.rdDRBMountedInjectorYes);
            this.pnlDRBMountedInjector.Location = new System.Drawing.Point(477, 54);
            this.pnlDRBMountedInjector.Name = "pnlDRBMountedInjector";
            this.pnlDRBMountedInjector.Size = new System.Drawing.Size(160, 31);
            this.pnlDRBMountedInjector.TabIndex = 17;
            // 
            // rdDRBMountedInjectorNo
            // 
            this.rdDRBMountedInjectorNo.AutoSize = true;
            this.rdDRBMountedInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdDRBMountedInjectorNo.Name = "rdDRBMountedInjectorNo";
            this.rdDRBMountedInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRBMountedInjectorNo.TabIndex = 1;
            this.rdDRBMountedInjectorNo.TabStop = true;
            this.rdDRBMountedInjectorNo.Tag = "DoubleRail";
            this.rdDRBMountedInjectorNo.Text = "NO";
            this.rdDRBMountedInjectorNo.UseVisualStyleBackColor = true;
            this.rdDRBMountedInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBMountedInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBMountedInjectorYes
            // 
            this.rdDRBMountedInjectorYes.AutoSize = true;
            this.rdDRBMountedInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdDRBMountedInjectorYes.Name = "rdDRBMountedInjectorYes";
            this.rdDRBMountedInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBMountedInjectorYes.TabIndex = 0;
            this.rdDRBMountedInjectorYes.TabStop = true;
            this.rdDRBMountedInjectorYes.Tag = "DoubleRail";
            this.rdDRBMountedInjectorYes.Text = "YES";
            this.rdDRBMountedInjectorYes.UseVisualStyleBackColor = true;
            this.rdDRBMountedInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(377, 65);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(90, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "Mounted Injector:";
            // 
            // pnlDRBPressureRegulator
            // 
            this.pnlDRBPressureRegulator.Controls.Add(this.radioButton8);
            this.pnlDRBPressureRegulator.Controls.Add(this.rdDRBPressureRegulatorNo);
            this.pnlDRBPressureRegulator.Controls.Add(this.rdDRBPressureRegulatorYes);
            this.pnlDRBPressureRegulator.Location = new System.Drawing.Point(477, 15);
            this.pnlDRBPressureRegulator.Name = "pnlDRBPressureRegulator";
            this.pnlDRBPressureRegulator.Size = new System.Drawing.Size(160, 31);
            this.pnlDRBPressureRegulator.TabIndex = 15;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(-290, 41);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(41, 17);
            this.radioButton8.TabIndex = 46;
            this.radioButton8.TabStop = true;
            this.radioButton8.Tag = "Echo";
            this.radioButton8.Text = "NO";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // rdDRBPressureRegulatorNo
            // 
            this.rdDRBPressureRegulatorNo.AutoSize = true;
            this.rdDRBPressureRegulatorNo.Location = new System.Drawing.Point(87, 7);
            this.rdDRBPressureRegulatorNo.Name = "rdDRBPressureRegulatorNo";
            this.rdDRBPressureRegulatorNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRBPressureRegulatorNo.TabIndex = 1;
            this.rdDRBPressureRegulatorNo.TabStop = true;
            this.rdDRBPressureRegulatorNo.Tag = "DoubleRail";
            this.rdDRBPressureRegulatorNo.Text = "NO";
            this.rdDRBPressureRegulatorNo.UseVisualStyleBackColor = true;
            this.rdDRBPressureRegulatorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBPressureRegulatorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBPressureRegulatorYes
            // 
            this.rdDRBPressureRegulatorYes.AutoSize = true;
            this.rdDRBPressureRegulatorYes.Location = new System.Drawing.Point(20, 7);
            this.rdDRBPressureRegulatorYes.Name = "rdDRBPressureRegulatorYes";
            this.rdDRBPressureRegulatorYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBPressureRegulatorYes.TabIndex = 0;
            this.rdDRBPressureRegulatorYes.TabStop = true;
            this.rdDRBPressureRegulatorYes.Tag = "DoubleRail";
            this.rdDRBPressureRegulatorYes.Text = "YES";
            this.rdDRBPressureRegulatorYes.UseVisualStyleBackColor = true;
            this.rdDRBPressureRegulatorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(367, 23);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(100, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "Pressure Regulator:";
            // 
            // txtDRBSpraybodySpacing
            // 
            this.txtDRBSpraybodySpacing.Location = new System.Drawing.Point(184, 134);
            this.txtDRBSpraybodySpacing.Mask = "F2";
            this.txtDRBSpraybodySpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtDRBSpraybodySpacing.Name = "txtDRBSpraybodySpacing";
            this.txtDRBSpraybodySpacing.Size = new System.Drawing.Size(158, 20);
            this.txtDRBSpraybodySpacing.TabIndex = 23;
            this.txtDRBSpraybodySpacing.TabStop = false;
            this.txtDRBSpraybodySpacing.Tag = "DoubleRail";
            this.txtDRBSpraybodySpacing.Text = "0.00";
            this.txtDRBSpraybodySpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(75, 133);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(100, 28);
            this.label56.TabIndex = 22;
            this.label56.Text = "Spraybody Spacing (IN):";
            // 
            // pnlDRBSweep90
            // 
            this.pnlDRBSweep90.Controls.Add(this.rdDRBSweep90No);
            this.pnlDRBSweep90.Controls.Add(this.rdDRBSweep90Yes);
            this.pnlDRBSweep90.Location = new System.Drawing.Point(789, 174);
            this.pnlDRBSweep90.Name = "pnlDRBSweep90";
            this.pnlDRBSweep90.Size = new System.Drawing.Size(155, 31);
            this.pnlDRBSweep90.TabIndex = 13;
            // 
            // rdDRBSweep90No
            // 
            this.rdDRBSweep90No.AutoSize = true;
            this.rdDRBSweep90No.Location = new System.Drawing.Point(80, 6);
            this.rdDRBSweep90No.Name = "rdDRBSweep90No";
            this.rdDRBSweep90No.Size = new System.Drawing.Size(41, 17);
            this.rdDRBSweep90No.TabIndex = 1;
            this.rdDRBSweep90No.TabStop = true;
            this.rdDRBSweep90No.Tag = "DoubleRail";
            this.rdDRBSweep90No.Text = "NO";
            this.rdDRBSweep90No.UseVisualStyleBackColor = true;
            this.rdDRBSweep90No.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBSweep90No.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBSweep90Yes
            // 
            this.rdDRBSweep90Yes.AutoSize = true;
            this.rdDRBSweep90Yes.Location = new System.Drawing.Point(19, 6);
            this.rdDRBSweep90Yes.Name = "rdDRBSweep90Yes";
            this.rdDRBSweep90Yes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBSweep90Yes.TabIndex = 0;
            this.rdDRBSweep90Yes.TabStop = true;
            this.rdDRBSweep90Yes.Tag = "DoubleRail";
            this.rdDRBSweep90Yes.Text = "YES";
            this.rdDRBSweep90Yes.UseVisualStyleBackColor = true;
            this.rdDRBSweep90Yes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(725, 189);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(58, 13);
            this.label57.TabIndex = 12;
            this.label57.Text = "Sweep 90:";
            // 
            // pnlDRBTripleWaterLocklineUpgrade
            // 
            this.pnlDRBTripleWaterLocklineUpgrade.Controls.Add(this.rdDRBTripleWaterLocklineUpgradeNo);
            this.pnlDRBTripleWaterLocklineUpgrade.Controls.Add(this.rdDRBTripleWaterLocklineUpgradeYes);
            this.pnlDRBTripleWaterLocklineUpgrade.Location = new System.Drawing.Point(183, 193);
            this.pnlDRBTripleWaterLocklineUpgrade.Name = "pnlDRBTripleWaterLocklineUpgrade";
            this.pnlDRBTripleWaterLocklineUpgrade.Size = new System.Drawing.Size(160, 31);
            this.pnlDRBTripleWaterLocklineUpgrade.TabIndex = 7;
            // 
            // rdDRBTripleWaterLocklineUpgradeNo
            // 
            this.rdDRBTripleWaterLocklineUpgradeNo.AutoSize = true;
            this.rdDRBTripleWaterLocklineUpgradeNo.Location = new System.Drawing.Point(98, 7);
            this.rdDRBTripleWaterLocklineUpgradeNo.Name = "rdDRBTripleWaterLocklineUpgradeNo";
            this.rdDRBTripleWaterLocklineUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdDRBTripleWaterLocklineUpgradeNo.TabIndex = 1;
            this.rdDRBTripleWaterLocklineUpgradeNo.TabStop = true;
            this.rdDRBTripleWaterLocklineUpgradeNo.Tag = "DoubleRail";
            this.rdDRBTripleWaterLocklineUpgradeNo.Text = "NO";
            this.rdDRBTripleWaterLocklineUpgradeNo.UseVisualStyleBackColor = true;
            this.rdDRBTripleWaterLocklineUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdDRBTripleWaterLocklineUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdDRBTripleWaterLocklineUpgradeYes
            // 
            this.rdDRBTripleWaterLocklineUpgradeYes.AutoSize = true;
            this.rdDRBTripleWaterLocklineUpgradeYes.Location = new System.Drawing.Point(26, 7);
            this.rdDRBTripleWaterLocklineUpgradeYes.Name = "rdDRBTripleWaterLocklineUpgradeYes";
            this.rdDRBTripleWaterLocklineUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdDRBTripleWaterLocklineUpgradeYes.TabIndex = 0;
            this.rdDRBTripleWaterLocklineUpgradeYes.TabStop = true;
            this.rdDRBTripleWaterLocklineUpgradeYes.Tag = "DoubleRail";
            this.rdDRBTripleWaterLocklineUpgradeYes.Text = "YES";
            this.rdDRBTripleWaterLocklineUpgradeYes.UseVisualStyleBackColor = true;
            this.rdDRBTripleWaterLocklineUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(45, 199);
            this.label58.MaximumSize = new System.Drawing.Size(150, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(136, 26);
            this.label58.TabIndex = 6;
            this.label58.Text = "Step/Stop/Water Lockline Upgrade:";
            // 
            // ddlDRBControllerType
            // 
            this.ddlDRBControllerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlDRBControllerType.FormattingEnabled = true;
            this.ddlDRBControllerType.Location = new System.Drawing.Point(183, 105);
            this.ddlDRBControllerType.Name = "ddlDRBControllerType";
            this.ddlDRBControllerType.Size = new System.Drawing.Size(160, 21);
            this.ddlDRBControllerType.TabIndex = 11;
            this.ddlDRBControllerType.Tag = "DoubleRail";
            this.ddlDRBControllerType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(93, 107);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(81, 13);
            this.label59.TabIndex = 10;
            this.label59.Text = "Controller Type:";
            // 
            // txtDRBHoseLoopHeight
            // 
            this.txtDRBHoseLoopHeight.Location = new System.Drawing.Point(183, 164);
            this.txtDRBHoseLoopHeight.Mask = "F2";
            this.txtDRBHoseLoopHeight.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtDRBHoseLoopHeight.Name = "txtDRBHoseLoopHeight";
            this.txtDRBHoseLoopHeight.Size = new System.Drawing.Size(160, 20);
            this.txtDRBHoseLoopHeight.TabIndex = 3;
            this.txtDRBHoseLoopHeight.TabStop = false;
            this.txtDRBHoseLoopHeight.Tag = "DoubleRail";
            this.txtDRBHoseLoopHeight.Text = "0.00";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(61, 167);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(116, 13);
            this.label60.TabIndex = 2;
            this.label60.Text = "Hose Loop Height (IN):";
            // 
            // ddlDRBSingleDualWater
            // 
            this.ddlDRBSingleDualWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlDRBSingleDualWater.FormattingEnabled = true;
            this.ddlDRBSingleDualWater.Location = new System.Drawing.Point(183, 45);
            this.ddlDRBSingleDualWater.Name = "ddlDRBSingleDualWater";
            this.ddlDRBSingleDualWater.Size = new System.Drawing.Size(160, 21);
            this.ddlDRBSingleDualWater.TabIndex = 5;
            this.ddlDRBSingleDualWater.Tag = "DoubleRail";
            this.ddlDRBSingleDualWater.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(83, 49);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(98, 13);
            this.label61.TabIndex = 4;
            this.label61.Text = "Single/Dual Water:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(71, 16);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(105, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "Quantity Of Systems:";
            // 
            // tpCWFDoubleRailBoom
            // 
            this.tpCWFDoubleRailBoom.Controls.Add(this.ddlCWFDRBEVWireGauge);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label215);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBWaterFeedSystem);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label160);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRB1_1_4thHoseUpgrade);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label159);
            this.tpCWFDoubleRailBoom.Controls.Add(this.ddlCWFDRBInjectorType);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label153);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBAutoInjector);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label154);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBWaterSolenoidControl);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label146);
            this.tpCWFDoubleRailBoom.Controls.Add(this.ddlCWFDRBFeed);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label137);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBEV);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label130);
            this.tpCWFDoubleRailBoom.Controls.Add(this.txtCWFDRBTip3);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label118);
            this.tpCWFDoubleRailBoom.Controls.Add(this.txtCWFDRBTip2);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label119);
            this.tpCWFDoubleRailBoom.Controls.Add(this.txtCWFDRBTip1);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label120);
            this.tpCWFDoubleRailBoom.Controls.Add(this.txtCWFDRBNumberOfRows);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label107);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBWirelessWalkSwitch);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label65);
            this.tpCWFDoubleRailBoom.Controls.Add(this.txtCWFDRBRailDropDownAssy);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label66);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBTripleTurretBody);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label68);
            this.tpCWFDoubleRailBoom.Controls.Add(this.txtCWFDRBQuantityOfSystems);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBMountedInjector);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label69);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBPressureRegulator);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label70);
            this.tpCWFDoubleRailBoom.Controls.Add(this.txtCWFDRBSpraybodySpacing);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label71);
            this.tpCWFDoubleRailBoom.Controls.Add(this.pnlCWFDRBTripleWaterLocklineUpgrade);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label73);
            this.tpCWFDoubleRailBoom.Controls.Add(this.ddlCWFDRBControllerType);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label74);
            this.tpCWFDoubleRailBoom.Controls.Add(this.ddlCWFDRBSingleDualWater);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label76);
            this.tpCWFDoubleRailBoom.Controls.Add(this.label77);
            this.tpCWFDoubleRailBoom.Location = new System.Drawing.Point(4, 22);
            this.tpCWFDoubleRailBoom.Margin = new System.Windows.Forms.Padding(2);
            this.tpCWFDoubleRailBoom.Name = "tpCWFDoubleRailBoom";
            this.tpCWFDoubleRailBoom.Padding = new System.Windows.Forms.Padding(2);
            this.tpCWFDoubleRailBoom.Size = new System.Drawing.Size(1368, 267);
            this.tpCWFDoubleRailBoom.TabIndex = 3;
            this.tpCWFDoubleRailBoom.Tag = "CWFDoubleRail";
            this.tpCWFDoubleRailBoom.Text = "CWF Double Rail";
            this.tpCWFDoubleRailBoom.UseVisualStyleBackColor = true;
            // 
            // ddlCWFDRBEVWireGauge
            // 
            this.ddlCWFDRBEVWireGauge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCWFDRBEVWireGauge.FormattingEnabled = true;
            this.ddlCWFDRBEVWireGauge.Location = new System.Drawing.Point(473, 222);
            this.ddlCWFDRBEVWireGauge.Name = "ddlCWFDRBEVWireGauge";
            this.ddlCWFDRBEVWireGauge.Size = new System.Drawing.Size(164, 21);
            this.ddlCWFDRBEVWireGauge.TabIndex = 57;
            this.ddlCWFDRBEVWireGauge.Tag = "SingleRail";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(378, 225);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(86, 13);
            this.label215.TabIndex = 56;
            this.label215.Text = "EV Wire/Gauge:";
            // 
            // pnlCWFDRBWaterFeedSystem
            // 
            this.pnlCWFDRBWaterFeedSystem.Controls.Add(this.radioButton5);
            this.pnlCWFDRBWaterFeedSystem.Controls.Add(this.rdCWFDRBWaterFeedSystemNo);
            this.pnlCWFDRBWaterFeedSystem.Controls.Add(this.rdCWFDRBWaterFeedSystemYes);
            this.pnlCWFDRBWaterFeedSystem.Location = new System.Drawing.Point(1072, 61);
            this.pnlCWFDRBWaterFeedSystem.Name = "pnlCWFDRBWaterFeedSystem";
            this.pnlCWFDRBWaterFeedSystem.Size = new System.Drawing.Size(139, 31);
            this.pnlCWFDRBWaterFeedSystem.TabIndex = 55;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(-290, 41);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(41, 17);
            this.radioButton5.TabIndex = 46;
            this.radioButton5.TabStop = true;
            this.radioButton5.Tag = "Echo";
            this.radioButton5.Text = "NO";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // rdCWFDRBWaterFeedSystemNo
            // 
            this.rdCWFDRBWaterFeedSystemNo.AutoSize = true;
            this.rdCWFDRBWaterFeedSystemNo.Location = new System.Drawing.Point(87, 7);
            this.rdCWFDRBWaterFeedSystemNo.Name = "rdCWFDRBWaterFeedSystemNo";
            this.rdCWFDRBWaterFeedSystemNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBWaterFeedSystemNo.TabIndex = 1;
            this.rdCWFDRBWaterFeedSystemNo.TabStop = true;
            this.rdCWFDRBWaterFeedSystemNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBWaterFeedSystemNo.Text = "NO";
            this.rdCWFDRBWaterFeedSystemNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBWaterFeedSystemNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBWaterFeedSystemNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBWaterFeedSystemYes
            // 
            this.rdCWFDRBWaterFeedSystemYes.AutoSize = true;
            this.rdCWFDRBWaterFeedSystemYes.Location = new System.Drawing.Point(20, 7);
            this.rdCWFDRBWaterFeedSystemYes.Name = "rdCWFDRBWaterFeedSystemYes";
            this.rdCWFDRBWaterFeedSystemYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBWaterFeedSystemYes.TabIndex = 0;
            this.rdCWFDRBWaterFeedSystemYes.TabStop = true;
            this.rdCWFDRBWaterFeedSystemYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBWaterFeedSystemYes.Text = "YES";
            this.rdCWFDRBWaterFeedSystemYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBWaterFeedSystemYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label160
            // 
            this.label160.Location = new System.Drawing.Point(991, 61);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(75, 29);
            this.label160.TabIndex = 54;
            this.label160.Text = "Center Water Feed System:";
            this.label160.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlCWFDRB1_1_4thHoseUpgrade
            // 
            this.pnlCWFDRB1_1_4thHoseUpgrade.Controls.Add(this.rdCWFDRB1_1_4thHoseUpgradeNo);
            this.pnlCWFDRB1_1_4thHoseUpgrade.Controls.Add(this.rdCWFDRB1_1_4thHoseUpgradeYes);
            this.pnlCWFDRB1_1_4thHoseUpgrade.Location = new System.Drawing.Point(775, 18);
            this.pnlCWFDRB1_1_4thHoseUpgrade.Name = "pnlCWFDRB1_1_4thHoseUpgrade";
            this.pnlCWFDRB1_1_4thHoseUpgrade.Size = new System.Drawing.Size(138, 31);
            this.pnlCWFDRB1_1_4thHoseUpgrade.TabIndex = 53;
            // 
            // rdCWFDRB1_1_4thHoseUpgradeNo
            // 
            this.rdCWFDRB1_1_4thHoseUpgradeNo.AutoSize = true;
            this.rdCWFDRB1_1_4thHoseUpgradeNo.Location = new System.Drawing.Point(89, 6);
            this.rdCWFDRB1_1_4thHoseUpgradeNo.Name = "rdCWFDRB1_1_4thHoseUpgradeNo";
            this.rdCWFDRB1_1_4thHoseUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRB1_1_4thHoseUpgradeNo.TabIndex = 1;
            this.rdCWFDRB1_1_4thHoseUpgradeNo.TabStop = true;
            this.rdCWFDRB1_1_4thHoseUpgradeNo.Tag = "CWFDoubleRail";
            this.rdCWFDRB1_1_4thHoseUpgradeNo.Text = "NO";
            this.rdCWFDRB1_1_4thHoseUpgradeNo.UseVisualStyleBackColor = true;
            this.rdCWFDRB1_1_4thHoseUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRB1_1_4thHoseUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRB1_1_4thHoseUpgradeYes
            // 
            this.rdCWFDRB1_1_4thHoseUpgradeYes.AutoSize = true;
            this.rdCWFDRB1_1_4thHoseUpgradeYes.Location = new System.Drawing.Point(22, 6);
            this.rdCWFDRB1_1_4thHoseUpgradeYes.Name = "rdCWFDRB1_1_4thHoseUpgradeYes";
            this.rdCWFDRB1_1_4thHoseUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRB1_1_4thHoseUpgradeYes.TabIndex = 0;
            this.rdCWFDRB1_1_4thHoseUpgradeYes.TabStop = true;
            this.rdCWFDRB1_1_4thHoseUpgradeYes.Tag = "CWFDoubleRail";
            this.rdCWFDRB1_1_4thHoseUpgradeYes.Text = "YES";
            this.rdCWFDRB1_1_4thHoseUpgradeYes.UseVisualStyleBackColor = true;
            this.rdCWFDRB1_1_4thHoseUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(656, 27);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(113, 13);
            this.label159.TabIndex = 52;
            this.label159.Text = "1 1/4\" Hose Upgrade:";
            // 
            // ddlCWFDRBInjectorType
            // 
            this.ddlCWFDRBInjectorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCWFDRBInjectorType.FormattingEnabled = true;
            this.ddlCWFDRBInjectorType.Location = new System.Drawing.Point(473, 103);
            this.ddlCWFDRBInjectorType.Name = "ddlCWFDRBInjectorType";
            this.ddlCWFDRBInjectorType.Size = new System.Drawing.Size(164, 21);
            this.ddlCWFDRBInjectorType.TabIndex = 51;
            this.ddlCWFDRBInjectorType.Tag = "CWFDoubleRail";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(396, 106);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(72, 13);
            this.label153.TabIndex = 50;
            this.label153.Text = "Injector Type:";
            // 
            // pnlCWFDRBAutoInjector
            // 
            this.pnlCWFDRBAutoInjector.Controls.Add(this.rdCWFDRBAutoInjectorNo);
            this.pnlCWFDRBAutoInjector.Controls.Add(this.rdCWFDRBAutoInjectorYes);
            this.pnlCWFDRBAutoInjector.Location = new System.Drawing.Point(474, 134);
            this.pnlCWFDRBAutoInjector.Name = "pnlCWFDRBAutoInjector";
            this.pnlCWFDRBAutoInjector.Size = new System.Drawing.Size(164, 31);
            this.pnlCWFDRBAutoInjector.TabIndex = 49;
            // 
            // rdCWFDRBAutoInjectorNo
            // 
            this.rdCWFDRBAutoInjectorNo.AutoSize = true;
            this.rdCWFDRBAutoInjectorNo.Location = new System.Drawing.Point(88, 7);
            this.rdCWFDRBAutoInjectorNo.Name = "rdCWFDRBAutoInjectorNo";
            this.rdCWFDRBAutoInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBAutoInjectorNo.TabIndex = 1;
            this.rdCWFDRBAutoInjectorNo.TabStop = true;
            this.rdCWFDRBAutoInjectorNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBAutoInjectorNo.Text = "NO";
            this.rdCWFDRBAutoInjectorNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBAutoInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBAutoInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBAutoInjectorYes
            // 
            this.rdCWFDRBAutoInjectorYes.AutoSize = true;
            this.rdCWFDRBAutoInjectorYes.Location = new System.Drawing.Point(21, 7);
            this.rdCWFDRBAutoInjectorYes.Name = "rdCWFDRBAutoInjectorYes";
            this.rdCWFDRBAutoInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBAutoInjectorYes.TabIndex = 0;
            this.rdCWFDRBAutoInjectorYes.TabStop = true;
            this.rdCWFDRBAutoInjectorYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBAutoInjectorYes.Text = "YES";
            this.rdCWFDRBAutoInjectorYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBAutoInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(400, 143);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(70, 13);
            this.label154.TabIndex = 48;
            this.label154.Text = "Auto Injector:";
            // 
            // pnlCWFDRBWaterSolenoidControl
            // 
            this.pnlCWFDRBWaterSolenoidControl.Controls.Add(this.rdCWFDRBWaterSolenoidControlNo);
            this.pnlCWFDRBWaterSolenoidControl.Controls.Add(this.rdCWFDRBWaterSolenoidControlYes);
            this.pnlCWFDRBWaterSolenoidControl.Location = new System.Drawing.Point(1072, 14);
            this.pnlCWFDRBWaterSolenoidControl.Name = "pnlCWFDRBWaterSolenoidControl";
            this.pnlCWFDRBWaterSolenoidControl.Size = new System.Drawing.Size(139, 31);
            this.pnlCWFDRBWaterSolenoidControl.TabIndex = 47;
            // 
            // rdCWFDRBWaterSolenoidControlNo
            // 
            this.rdCWFDRBWaterSolenoidControlNo.AutoSize = true;
            this.rdCWFDRBWaterSolenoidControlNo.Location = new System.Drawing.Point(79, 7);
            this.rdCWFDRBWaterSolenoidControlNo.Name = "rdCWFDRBWaterSolenoidControlNo";
            this.rdCWFDRBWaterSolenoidControlNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBWaterSolenoidControlNo.TabIndex = 1;
            this.rdCWFDRBWaterSolenoidControlNo.TabStop = true;
            this.rdCWFDRBWaterSolenoidControlNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBWaterSolenoidControlNo.Text = "NO";
            this.rdCWFDRBWaterSolenoidControlNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBWaterSolenoidControlNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBWaterSolenoidControlNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBWaterSolenoidControlYes
            // 
            this.rdCWFDRBWaterSolenoidControlYes.AutoSize = true;
            this.rdCWFDRBWaterSolenoidControlYes.Location = new System.Drawing.Point(17, 7);
            this.rdCWFDRBWaterSolenoidControlYes.Name = "rdCWFDRBWaterSolenoidControlYes";
            this.rdCWFDRBWaterSolenoidControlYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBWaterSolenoidControlYes.TabIndex = 0;
            this.rdCWFDRBWaterSolenoidControlYes.TabStop = true;
            this.rdCWFDRBWaterSolenoidControlYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBWaterSolenoidControlYes.Text = "YES";
            this.rdCWFDRBWaterSolenoidControlYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBWaterSolenoidControlYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(948, 23);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(119, 13);
            this.label146.TabIndex = 46;
            this.label146.Text = "Water Solenoid Control:";
            // 
            // ddlCWFDRBFeed
            // 
            this.ddlCWFDRBFeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCWFDRBFeed.FormattingEnabled = true;
            this.ddlCWFDRBFeed.Location = new System.Drawing.Point(775, 187);
            this.ddlCWFDRBFeed.Name = "ddlCWFDRBFeed";
            this.ddlCWFDRBFeed.Size = new System.Drawing.Size(138, 21);
            this.ddlCWFDRBFeed.TabIndex = 45;
            this.ddlCWFDRBFeed.Tag = "CWFDoubleRail";
            this.ddlCWFDRBFeed.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(734, 189);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(34, 13);
            this.label137.TabIndex = 44;
            this.label137.Text = "Feed:";
            // 
            // pnlCWFDRBEV
            // 
            this.pnlCWFDRBEV.Controls.Add(this.rdCWFDRBEVNo);
            this.pnlCWFDRBEV.Controls.Add(this.rdCWFDRBEVYes);
            this.pnlCWFDRBEV.Location = new System.Drawing.Point(473, 175);
            this.pnlCWFDRBEV.Name = "pnlCWFDRBEV";
            this.pnlCWFDRBEV.Size = new System.Drawing.Size(165, 31);
            this.pnlCWFDRBEV.TabIndex = 43;
            // 
            // rdCWFDRBEVNo
            // 
            this.rdCWFDRBEVNo.AutoSize = true;
            this.rdCWFDRBEVNo.Location = new System.Drawing.Point(89, 7);
            this.rdCWFDRBEVNo.Name = "rdCWFDRBEVNo";
            this.rdCWFDRBEVNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBEVNo.TabIndex = 1;
            this.rdCWFDRBEVNo.TabStop = true;
            this.rdCWFDRBEVNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBEVNo.Text = "NO";
            this.rdCWFDRBEVNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBEVNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBEVNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBEVYes
            // 
            this.rdCWFDRBEVYes.AutoSize = true;
            this.rdCWFDRBEVYes.Location = new System.Drawing.Point(22, 7);
            this.rdCWFDRBEVYes.Name = "rdCWFDRBEVYes";
            this.rdCWFDRBEVYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBEVYes.TabIndex = 0;
            this.rdCWFDRBEVYes.TabStop = true;
            this.rdCWFDRBEVYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBEVYes.Text = "YES";
            this.rdCWFDRBEVYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBEVYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label130
            // 
            this.label130.Location = new System.Drawing.Point(385, 175);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(82, 34);
            this.label130.TabIndex = 42;
            this.label130.Text = "EV, VPD, Third Party Control:";
            // 
            // txtCWFDRBTip3
            // 
            this.txtCWFDRBTip3.Location = new System.Drawing.Point(775, 156);
            this.txtCWFDRBTip3.Name = "txtCWFDRBTip3";
            this.txtCWFDRBTip3.Size = new System.Drawing.Size(138, 20);
            this.txtCWFDRBTip3.TabIndex = 41;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(734, 158);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(34, 13);
            this.label118.TabIndex = 40;
            this.label118.Text = "Tip 3:";
            // 
            // txtCWFDRBTip2
            // 
            this.txtCWFDRBTip2.Location = new System.Drawing.Point(775, 131);
            this.txtCWFDRBTip2.Name = "txtCWFDRBTip2";
            this.txtCWFDRBTip2.Size = new System.Drawing.Size(138, 20);
            this.txtCWFDRBTip2.TabIndex = 39;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(734, 133);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(34, 13);
            this.label119.TabIndex = 38;
            this.label119.Text = "Tip 2:";
            // 
            // txtCWFDRBTip1
            // 
            this.txtCWFDRBTip1.Location = new System.Drawing.Point(775, 106);
            this.txtCWFDRBTip1.Name = "txtCWFDRBTip1";
            this.txtCWFDRBTip1.Size = new System.Drawing.Size(138, 20);
            this.txtCWFDRBTip1.TabIndex = 37;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(734, 108);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(34, 13);
            this.label120.TabIndex = 36;
            this.label120.Text = "Tip 1:";
            // 
            // txtCWFDRBNumberOfRows
            // 
            this.txtCWFDRBNumberOfRows.Location = new System.Drawing.Point(168, 80);
            this.txtCWFDRBNumberOfRows.Mask = "G";
            this.txtCWFDRBNumberOfRows.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtCWFDRBNumberOfRows.Name = "txtCWFDRBNumberOfRows";
            this.txtCWFDRBNumberOfRows.Size = new System.Drawing.Size(160, 20);
            this.txtCWFDRBNumberOfRows.TabIndex = 25;
            this.txtCWFDRBNumberOfRows.TabStop = false;
            this.txtCWFDRBNumberOfRows.Tag = "SingleRail";
            this.txtCWFDRBNumberOfRows.Text = "0";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(70, 83);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(91, 13);
            this.label107.TabIndex = 24;
            this.label107.Text = "Number Of Rows:";
            // 
            // pnlCWFDRBWirelessWalkSwitch
            // 
            this.pnlCWFDRBWirelessWalkSwitch.Controls.Add(this.rdCWFDRBWirelessWalkSwitchNo);
            this.pnlCWFDRBWirelessWalkSwitch.Controls.Add(this.rdCWFDRBWirelessWalkSwitchYes);
            this.pnlCWFDRBWirelessWalkSwitch.Location = new System.Drawing.Point(775, 219);
            this.pnlCWFDRBWirelessWalkSwitch.Name = "pnlCWFDRBWirelessWalkSwitch";
            this.pnlCWFDRBWirelessWalkSwitch.Size = new System.Drawing.Size(137, 31);
            this.pnlCWFDRBWirelessWalkSwitch.TabIndex = 19;
            // 
            // rdCWFDRBWirelessWalkSwitchNo
            // 
            this.rdCWFDRBWirelessWalkSwitchNo.AutoSize = true;
            this.rdCWFDRBWirelessWalkSwitchNo.Location = new System.Drawing.Point(79, 7);
            this.rdCWFDRBWirelessWalkSwitchNo.Name = "rdCWFDRBWirelessWalkSwitchNo";
            this.rdCWFDRBWirelessWalkSwitchNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBWirelessWalkSwitchNo.TabIndex = 1;
            this.rdCWFDRBWirelessWalkSwitchNo.TabStop = true;
            this.rdCWFDRBWirelessWalkSwitchNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBWirelessWalkSwitchNo.Text = "NO";
            this.rdCWFDRBWirelessWalkSwitchNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBWirelessWalkSwitchNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBWirelessWalkSwitchNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBWirelessWalkSwitchYes
            // 
            this.rdCWFDRBWirelessWalkSwitchYes.AutoSize = true;
            this.rdCWFDRBWirelessWalkSwitchYes.Location = new System.Drawing.Point(17, 7);
            this.rdCWFDRBWirelessWalkSwitchYes.Name = "rdCWFDRBWirelessWalkSwitchYes";
            this.rdCWFDRBWirelessWalkSwitchYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBWirelessWalkSwitchYes.TabIndex = 0;
            this.rdCWFDRBWirelessWalkSwitchYes.TabStop = true;
            this.rdCWFDRBWirelessWalkSwitchYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBWirelessWalkSwitchYes.Text = "YES";
            this.rdCWFDRBWirelessWalkSwitchYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBWirelessWalkSwitchYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(658, 227);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(113, 13);
            this.label65.TabIndex = 18;
            this.label65.Text = "Wireless Walk Switch:";
            // 
            // txtCWFDRBRailDropDownAssy
            // 
            this.txtCWFDRBRailDropDownAssy.Location = new System.Drawing.Point(167, 214);
            this.txtCWFDRBRailDropDownAssy.Mask = "F2";
            this.txtCWFDRBRailDropDownAssy.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtCWFDRBRailDropDownAssy.Name = "txtCWFDRBRailDropDownAssy";
            this.txtCWFDRBRailDropDownAssy.Size = new System.Drawing.Size(160, 20);
            this.txtCWFDRBRailDropDownAssy.TabIndex = 15;
            this.txtCWFDRBRailDropDownAssy.TabStop = false;
            this.txtCWFDRBRailDropDownAssy.Tag = "CWFDoubleRail";
            this.txtCWFDRBRailDropDownAssy.Text = "0.00";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(28, 217);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(133, 13);
            this.label66.TabIndex = 14;
            this.label66.Text = "Rail Drop Down Assy. (IN):";
            // 
            // pnlCWFDRBTripleTurretBody
            // 
            this.pnlCWFDRBTripleTurretBody.Controls.Add(this.rdCWFDRBTripleTurretBodyNo);
            this.pnlCWFDRBTripleTurretBody.Controls.Add(this.rdCWFDRBTripleTurretBodyYes);
            this.pnlCWFDRBTripleTurretBody.Location = new System.Drawing.Point(775, 59);
            this.pnlCWFDRBTripleTurretBody.Name = "pnlCWFDRBTripleTurretBody";
            this.pnlCWFDRBTripleTurretBody.Size = new System.Drawing.Size(138, 31);
            this.pnlCWFDRBTripleTurretBody.TabIndex = 7;
            // 
            // rdCWFDRBTripleTurretBodyNo
            // 
            this.rdCWFDRBTripleTurretBodyNo.AutoSize = true;
            this.rdCWFDRBTripleTurretBodyNo.Location = new System.Drawing.Point(79, 7);
            this.rdCWFDRBTripleTurretBodyNo.Name = "rdCWFDRBTripleTurretBodyNo";
            this.rdCWFDRBTripleTurretBodyNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBTripleTurretBodyNo.TabIndex = 1;
            this.rdCWFDRBTripleTurretBodyNo.TabStop = true;
            this.rdCWFDRBTripleTurretBodyNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBTripleTurretBodyNo.Text = "NO";
            this.rdCWFDRBTripleTurretBodyNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBTripleTurretBodyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBTripleTurretBodyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBTripleTurretBodyYes
            // 
            this.rdCWFDRBTripleTurretBodyYes.AutoSize = true;
            this.rdCWFDRBTripleTurretBodyYes.Location = new System.Drawing.Point(18, 7);
            this.rdCWFDRBTripleTurretBodyYes.Name = "rdCWFDRBTripleTurretBodyYes";
            this.rdCWFDRBTripleTurretBodyYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBTripleTurretBodyYes.TabIndex = 0;
            this.rdCWFDRBTripleTurretBodyYes.TabStop = true;
            this.rdCWFDRBTripleTurretBodyYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBTripleTurretBodyYes.Text = "YES";
            this.rdCWFDRBTripleTurretBodyYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBTripleTurretBodyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(672, 66);
            this.label68.MaximumSize = new System.Drawing.Size(150, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(94, 13);
            this.label68.TabIndex = 6;
            this.label68.Text = "Triple Turret Body:";
            // 
            // txtCWFDRBQuantityOfSystems
            // 
            this.txtCWFDRBQuantityOfSystems.Location = new System.Drawing.Point(168, 20);
            this.txtCWFDRBQuantityOfSystems.Mask = "G";
            this.txtCWFDRBQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtCWFDRBQuantityOfSystems.Name = "txtCWFDRBQuantityOfSystems";
            this.txtCWFDRBQuantityOfSystems.Size = new System.Drawing.Size(160, 20);
            this.txtCWFDRBQuantityOfSystems.TabIndex = 1;
            this.txtCWFDRBQuantityOfSystems.TabStop = false;
            this.txtCWFDRBQuantityOfSystems.Tag = "CWFDoubleRail";
            this.txtCWFDRBQuantityOfSystems.Text = "0";
            this.txtCWFDRBQuantityOfSystems.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // pnlCWFDRBMountedInjector
            // 
            this.pnlCWFDRBMountedInjector.Controls.Add(this.rdCWFDRBMountedInjectorNo);
            this.pnlCWFDRBMountedInjector.Controls.Add(this.rdCWFDRBMountedInjectorYes);
            this.pnlCWFDRBMountedInjector.Location = new System.Drawing.Point(475, 57);
            this.pnlCWFDRBMountedInjector.Name = "pnlCWFDRBMountedInjector";
            this.pnlCWFDRBMountedInjector.Size = new System.Drawing.Size(162, 31);
            this.pnlCWFDRBMountedInjector.TabIndex = 13;
            // 
            // rdCWFDRBMountedInjectorNo
            // 
            this.rdCWFDRBMountedInjectorNo.AutoSize = true;
            this.rdCWFDRBMountedInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdCWFDRBMountedInjectorNo.Name = "rdCWFDRBMountedInjectorNo";
            this.rdCWFDRBMountedInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBMountedInjectorNo.TabIndex = 1;
            this.rdCWFDRBMountedInjectorNo.TabStop = true;
            this.rdCWFDRBMountedInjectorNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBMountedInjectorNo.Text = "NO";
            this.rdCWFDRBMountedInjectorNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBMountedInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBMountedInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBMountedInjectorYes
            // 
            this.rdCWFDRBMountedInjectorYes.AutoSize = true;
            this.rdCWFDRBMountedInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdCWFDRBMountedInjectorYes.Name = "rdCWFDRBMountedInjectorYes";
            this.rdCWFDRBMountedInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBMountedInjectorYes.TabIndex = 0;
            this.rdCWFDRBMountedInjectorYes.TabStop = true;
            this.rdCWFDRBMountedInjectorYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBMountedInjectorYes.Text = "YES";
            this.rdCWFDRBMountedInjectorYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBMountedInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(424, 66);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(45, 13);
            this.label69.TabIndex = 12;
            this.label69.Text = "Injector:";
            // 
            // pnlCWFDRBPressureRegulator
            // 
            this.pnlCWFDRBPressureRegulator.Controls.Add(this.radioButton11);
            this.pnlCWFDRBPressureRegulator.Controls.Add(this.rdCWFDRBPressureRegulatorNo);
            this.pnlCWFDRBPressureRegulator.Controls.Add(this.rdCWFDRBPressureRegulatorYes);
            this.pnlCWFDRBPressureRegulator.Location = new System.Drawing.Point(475, 18);
            this.pnlCWFDRBPressureRegulator.Name = "pnlCWFDRBPressureRegulator";
            this.pnlCWFDRBPressureRegulator.Size = new System.Drawing.Size(163, 31);
            this.pnlCWFDRBPressureRegulator.TabIndex = 11;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(-290, 41);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(41, 17);
            this.radioButton11.TabIndex = 46;
            this.radioButton11.TabStop = true;
            this.radioButton11.Tag = "Echo";
            this.radioButton11.Text = "NO";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // rdCWFDRBPressureRegulatorNo
            // 
            this.rdCWFDRBPressureRegulatorNo.AutoSize = true;
            this.rdCWFDRBPressureRegulatorNo.Location = new System.Drawing.Point(87, 7);
            this.rdCWFDRBPressureRegulatorNo.Name = "rdCWFDRBPressureRegulatorNo";
            this.rdCWFDRBPressureRegulatorNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBPressureRegulatorNo.TabIndex = 1;
            this.rdCWFDRBPressureRegulatorNo.TabStop = true;
            this.rdCWFDRBPressureRegulatorNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBPressureRegulatorNo.Text = "NO";
            this.rdCWFDRBPressureRegulatorNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBPressureRegulatorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBPressureRegulatorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBPressureRegulatorYes
            // 
            this.rdCWFDRBPressureRegulatorYes.AutoSize = true;
            this.rdCWFDRBPressureRegulatorYes.Location = new System.Drawing.Point(20, 7);
            this.rdCWFDRBPressureRegulatorYes.Name = "rdCWFDRBPressureRegulatorYes";
            this.rdCWFDRBPressureRegulatorYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBPressureRegulatorYes.TabIndex = 0;
            this.rdCWFDRBPressureRegulatorYes.TabStop = true;
            this.rdCWFDRBPressureRegulatorYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBPressureRegulatorYes.Text = "YES";
            this.rdCWFDRBPressureRegulatorYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBPressureRegulatorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(366, 25);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(100, 13);
            this.label70.TabIndex = 10;
            this.label70.Text = "Pressure Regulator:";
            // 
            // txtCWFDRBSpraybodySpacing
            // 
            this.txtCWFDRBSpraybodySpacing.Location = new System.Drawing.Point(168, 137);
            this.txtCWFDRBSpraybodySpacing.Mask = "F2";
            this.txtCWFDRBSpraybodySpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtCWFDRBSpraybodySpacing.Name = "txtCWFDRBSpraybodySpacing";
            this.txtCWFDRBSpraybodySpacing.Size = new System.Drawing.Size(160, 20);
            this.txtCWFDRBSpraybodySpacing.TabIndex = 17;
            this.txtCWFDRBSpraybodySpacing.TabStop = false;
            this.txtCWFDRBSpraybodySpacing.Tag = "CWFDoubleRail";
            this.txtCWFDRBSpraybodySpacing.Text = "0.00";
            this.txtCWFDRBSpraybodySpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(38, 141);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(122, 13);
            this.label71.TabIndex = 16;
            this.label71.Text = "Spraybody Spacing (IN):";
            // 
            // pnlCWFDRBTripleWaterLocklineUpgrade
            // 
            this.pnlCWFDRBTripleWaterLocklineUpgrade.Controls.Add(this.rdCWFDRBTripleWaterLocklineUpgradeNo);
            this.pnlCWFDRBTripleWaterLocklineUpgrade.Controls.Add(this.rdCWFDRBTripleWaterLocklineUpgradeYes);
            this.pnlCWFDRBTripleWaterLocklineUpgrade.Location = new System.Drawing.Point(167, 166);
            this.pnlCWFDRBTripleWaterLocklineUpgrade.Name = "pnlCWFDRBTripleWaterLocklineUpgrade";
            this.pnlCWFDRBTripleWaterLocklineUpgrade.Size = new System.Drawing.Size(160, 31);
            this.pnlCWFDRBTripleWaterLocklineUpgrade.TabIndex = 5;
            // 
            // rdCWFDRBTripleWaterLocklineUpgradeNo
            // 
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.AutoSize = true;
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.Location = new System.Drawing.Point(98, 7);
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.Name = "rdCWFDRBTripleWaterLocklineUpgradeNo";
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.TabIndex = 1;
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.TabStop = true;
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.Tag = "CWFDoubleRail";
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.Text = "NO";
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.UseVisualStyleBackColor = true;
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCWFDRBTripleWaterLocklineUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCWFDRBTripleWaterLocklineUpgradeYes
            // 
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.AutoSize = true;
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.Location = new System.Drawing.Point(26, 7);
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.Name = "rdCWFDRBTripleWaterLocklineUpgradeYes";
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.TabIndex = 0;
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.TabStop = true;
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.Tag = "CWFDoubleRail";
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.Text = "YES";
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.UseVisualStyleBackColor = true;
            this.rdCWFDRBTripleWaterLocklineUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(30, 171);
            this.label73.MaximumSize = new System.Drawing.Size(150, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(136, 26);
            this.label73.TabIndex = 4;
            this.label73.Text = "Step/Stop/Water Lockline Upgrade:";
            // 
            // ddlCWFDRBControllerType
            // 
            this.ddlCWFDRBControllerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCWFDRBControllerType.FormattingEnabled = true;
            this.ddlCWFDRBControllerType.Location = new System.Drawing.Point(168, 106);
            this.ddlCWFDRBControllerType.Name = "ddlCWFDRBControllerType";
            this.ddlCWFDRBControllerType.Size = new System.Drawing.Size(160, 21);
            this.ddlCWFDRBControllerType.TabIndex = 9;
            this.ddlCWFDRBControllerType.Tag = "CWFDoubleRail";
            this.ddlCWFDRBControllerType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(78, 108);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(81, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "Controller Type:";
            // 
            // ddlCWFDRBSingleDualWater
            // 
            this.ddlCWFDRBSingleDualWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCWFDRBSingleDualWater.FormattingEnabled = true;
            this.ddlCWFDRBSingleDualWater.Location = new System.Drawing.Point(168, 51);
            this.ddlCWFDRBSingleDualWater.Name = "ddlCWFDRBSingleDualWater";
            this.ddlCWFDRBSingleDualWater.Size = new System.Drawing.Size(160, 21);
            this.ddlCWFDRBSingleDualWater.TabIndex = 3;
            this.ddlCWFDRBSingleDualWater.Tag = "CWFDoubleRail";
            this.ddlCWFDRBSingleDualWater.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(67, 54);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(98, 13);
            this.label76.TabIndex = 2;
            this.label76.Text = "Single/Dual Water:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(55, 22);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(105, 13);
            this.label77.TabIndex = 0;
            this.label77.Text = "Quantity Of Systems:";
            // 
            // tpControlTowerBoom
            // 
            this.tpControlTowerBoom.Controls.Add(this.ddlCTEVWireGauge);
            this.tpControlTowerBoom.Controls.Add(this.label216);
            this.tpControlTowerBoom.Controls.Add(this.ddlCTInjectorType);
            this.tpControlTowerBoom.Controls.Add(this.label155);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTAutoInjector);
            this.tpControlTowerBoom.Controls.Add(this.label156);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTWaterSolenoidControl);
            this.tpControlTowerBoom.Controls.Add(this.label147);
            this.tpControlTowerBoom.Controls.Add(this.ddlCTFeed);
            this.tpControlTowerBoom.Controls.Add(this.label138);
            this.tpControlTowerBoom.Controls.Add(this.ddlCTCarrySteel);
            this.tpControlTowerBoom.Controls.Add(this.label72);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTEV);
            this.tpControlTowerBoom.Controls.Add(this.label131);
            this.tpControlTowerBoom.Controls.Add(this.txtCTTip3);
            this.tpControlTowerBoom.Controls.Add(this.label121);
            this.tpControlTowerBoom.Controls.Add(this.txtCTTip2);
            this.tpControlTowerBoom.Controls.Add(this.label122);
            this.tpControlTowerBoom.Controls.Add(this.txtCTTip1);
            this.tpControlTowerBoom.Controls.Add(this.label123);
            this.tpControlTowerBoom.Controls.Add(this.txtCTNumberOfRows);
            this.tpControlTowerBoom.Controls.Add(this.label110);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTWaterFeedSystem);
            this.tpControlTowerBoom.Controls.Add(this.label92);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTGallonTank);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTConveyor);
            this.tpControlTowerBoom.Controls.Add(this.label89);
            this.tpControlTowerBoom.Controls.Add(this.label87);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTDualMotorNoAxle);
            this.tpControlTowerBoom.Controls.Add(this.label90);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTDiamondSweep);
            this.tpControlTowerBoom.Controls.Add(this.label88);
            this.tpControlTowerBoom.Controls.Add(this.pnlCT1_1_4thHoseUpgrade);
            this.tpControlTowerBoom.Controls.Add(this.label86);
            this.tpControlTowerBoom.Controls.Add(this.txtCTHoseLoopHeight);
            this.tpControlTowerBoom.Controls.Add(this.label85);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTWirelessWalkSwitch);
            this.tpControlTowerBoom.Controls.Add(this.label67);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTTripleTurretBody);
            this.tpControlTowerBoom.Controls.Add(this.label75);
            this.tpControlTowerBoom.Controls.Add(this.txtCTQuantityOfSystems);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTMountedInjector);
            this.tpControlTowerBoom.Controls.Add(this.label78);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTPressureRegulator);
            this.tpControlTowerBoom.Controls.Add(this.label79);
            this.tpControlTowerBoom.Controls.Add(this.txtCTSpraybodySpacing);
            this.tpControlTowerBoom.Controls.Add(this.label80);
            this.tpControlTowerBoom.Controls.Add(this.pnlCTTripleWaterLocklineUpgrade);
            this.tpControlTowerBoom.Controls.Add(this.label81);
            this.tpControlTowerBoom.Controls.Add(this.ddlCTControllerType);
            this.tpControlTowerBoom.Controls.Add(this.label82);
            this.tpControlTowerBoom.Controls.Add(this.ddlCTSingleDualWater);
            this.tpControlTowerBoom.Controls.Add(this.label83);
            this.tpControlTowerBoom.Controls.Add(this.label84);
            this.tpControlTowerBoom.Location = new System.Drawing.Point(4, 22);
            this.tpControlTowerBoom.Margin = new System.Windows.Forms.Padding(2);
            this.tpControlTowerBoom.Name = "tpControlTowerBoom";
            this.tpControlTowerBoom.Padding = new System.Windows.Forms.Padding(2);
            this.tpControlTowerBoom.Size = new System.Drawing.Size(1368, 267);
            this.tpControlTowerBoom.TabIndex = 4;
            this.tpControlTowerBoom.Tag = "Tower";
            this.tpControlTowerBoom.Text = "Control Tower";
            this.tpControlTowerBoom.UseVisualStyleBackColor = true;
            // 
            // ddlCTEVWireGauge
            // 
            this.ddlCTEVWireGauge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCTEVWireGauge.FormattingEnabled = true;
            this.ddlCTEVWireGauge.Location = new System.Drawing.Point(446, 223);
            this.ddlCTEVWireGauge.Name = "ddlCTEVWireGauge";
            this.ddlCTEVWireGauge.Size = new System.Drawing.Size(139, 21);
            this.ddlCTEVWireGauge.TabIndex = 61;
            this.ddlCTEVWireGauge.Tag = "SingleRail";
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(351, 226);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(86, 13);
            this.label216.TabIndex = 60;
            this.label216.Text = "EV Wire/Gauge:";
            // 
            // ddlCTInjectorType
            // 
            this.ddlCTInjectorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCTInjectorType.FormattingEnabled = true;
            this.ddlCTInjectorType.Location = new System.Drawing.Point(445, 98);
            this.ddlCTInjectorType.Name = "ddlCTInjectorType";
            this.ddlCTInjectorType.Size = new System.Drawing.Size(140, 21);
            this.ddlCTInjectorType.TabIndex = 59;
            this.ddlCTInjectorType.Tag = "Tower";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(364, 100);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(72, 13);
            this.label155.TabIndex = 58;
            this.label155.Text = "Injector Type:";
            // 
            // pnlCTAutoInjector
            // 
            this.pnlCTAutoInjector.Controls.Add(this.rdCTAutoInjectorNo);
            this.pnlCTAutoInjector.Controls.Add(this.rdCTAutoInjectorYes);
            this.pnlCTAutoInjector.Location = new System.Drawing.Point(446, 128);
            this.pnlCTAutoInjector.Name = "pnlCTAutoInjector";
            this.pnlCTAutoInjector.Size = new System.Drawing.Size(138, 31);
            this.pnlCTAutoInjector.TabIndex = 57;
            // 
            // rdCTAutoInjectorNo
            // 
            this.rdCTAutoInjectorNo.AutoSize = true;
            this.rdCTAutoInjectorNo.Location = new System.Drawing.Point(86, 7);
            this.rdCTAutoInjectorNo.Name = "rdCTAutoInjectorNo";
            this.rdCTAutoInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTAutoInjectorNo.TabIndex = 1;
            this.rdCTAutoInjectorNo.TabStop = true;
            this.rdCTAutoInjectorNo.Tag = "Tower";
            this.rdCTAutoInjectorNo.Text = "NO";
            this.rdCTAutoInjectorNo.UseVisualStyleBackColor = true;
            this.rdCTAutoInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTAutoInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTAutoInjectorYes
            // 
            this.rdCTAutoInjectorYes.AutoSize = true;
            this.rdCTAutoInjectorYes.Location = new System.Drawing.Point(19, 7);
            this.rdCTAutoInjectorYes.Name = "rdCTAutoInjectorYes";
            this.rdCTAutoInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTAutoInjectorYes.TabIndex = 0;
            this.rdCTAutoInjectorYes.TabStop = true;
            this.rdCTAutoInjectorYes.Tag = "Tower";
            this.rdCTAutoInjectorYes.Text = "YES";
            this.rdCTAutoInjectorYes.UseVisualStyleBackColor = true;
            this.rdCTAutoInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(366, 137);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(70, 13);
            this.label156.TabIndex = 56;
            this.label156.Text = "Auto Injector:";
            // 
            // pnlCTWaterSolenoidControl
            // 
            this.pnlCTWaterSolenoidControl.Controls.Add(this.rdCTWaterSolenoidControlNo);
            this.pnlCTWaterSolenoidControl.Controls.Add(this.rdCTWaterSolenoidControlYes);
            this.pnlCTWaterSolenoidControl.Location = new System.Drawing.Point(1018, 10);
            this.pnlCTWaterSolenoidControl.Name = "pnlCTWaterSolenoidControl";
            this.pnlCTWaterSolenoidControl.Size = new System.Drawing.Size(131, 31);
            this.pnlCTWaterSolenoidControl.TabIndex = 55;
            // 
            // rdCTWaterSolenoidControlNo
            // 
            this.rdCTWaterSolenoidControlNo.AutoSize = true;
            this.rdCTWaterSolenoidControlNo.Location = new System.Drawing.Point(84, 7);
            this.rdCTWaterSolenoidControlNo.Name = "rdCTWaterSolenoidControlNo";
            this.rdCTWaterSolenoidControlNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTWaterSolenoidControlNo.TabIndex = 1;
            this.rdCTWaterSolenoidControlNo.TabStop = true;
            this.rdCTWaterSolenoidControlNo.Tag = "Tower";
            this.rdCTWaterSolenoidControlNo.Text = "NO";
            this.rdCTWaterSolenoidControlNo.UseVisualStyleBackColor = true;
            this.rdCTWaterSolenoidControlNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTWaterSolenoidControlNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTWaterSolenoidControlYes
            // 
            this.rdCTWaterSolenoidControlYes.AutoSize = true;
            this.rdCTWaterSolenoidControlYes.Location = new System.Drawing.Point(17, 7);
            this.rdCTWaterSolenoidControlYes.Name = "rdCTWaterSolenoidControlYes";
            this.rdCTWaterSolenoidControlYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTWaterSolenoidControlYes.TabIndex = 0;
            this.rdCTWaterSolenoidControlYes.TabStop = true;
            this.rdCTWaterSolenoidControlYes.Tag = "Tower";
            this.rdCTWaterSolenoidControlYes.Text = "YES";
            this.rdCTWaterSolenoidControlYes.UseVisualStyleBackColor = true;
            this.rdCTWaterSolenoidControlYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(894, 19);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(119, 13);
            this.label147.TabIndex = 54;
            this.label147.Text = "Water Solenoid Control:";
            // 
            // ddlCTFeed
            // 
            this.ddlCTFeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCTFeed.FormattingEnabled = true;
            this.ddlCTFeed.Location = new System.Drawing.Point(713, 211);
            this.ddlCTFeed.Name = "ddlCTFeed";
            this.ddlCTFeed.Size = new System.Drawing.Size(140, 21);
            this.ddlCTFeed.TabIndex = 53;
            this.ddlCTFeed.Tag = "Tower";
            this.ddlCTFeed.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(669, 213);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(34, 13);
            this.label138.TabIndex = 52;
            this.label138.Text = "Feed:";
            // 
            // ddlCTCarrySteel
            // 
            this.ddlCTCarrySteel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCTCarrySteel.FormattingEnabled = true;
            this.ddlCTCarrySteel.Location = new System.Drawing.Point(713, 180);
            this.ddlCTCarrySteel.Name = "ddlCTCarrySteel";
            this.ddlCTCarrySteel.Size = new System.Drawing.Size(140, 21);
            this.ddlCTCarrySteel.TabIndex = 51;
            this.ddlCTCarrySteel.Tag = "Tower";
            this.ddlCTCarrySteel.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(641, 182);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(61, 13);
            this.label72.TabIndex = 50;
            this.label72.Text = "Carry Steel:";
            // 
            // pnlCTEV
            // 
            this.pnlCTEV.Controls.Add(this.rdCTEVNo);
            this.pnlCTEV.Controls.Add(this.rdCTEVYes);
            this.pnlCTEV.Location = new System.Drawing.Point(446, 173);
            this.pnlCTEV.Name = "pnlCTEV";
            this.pnlCTEV.Size = new System.Drawing.Size(138, 31);
            this.pnlCTEV.TabIndex = 49;
            // 
            // rdCTEVNo
            // 
            this.rdCTEVNo.AutoSize = true;
            this.rdCTEVNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTEVNo.Name = "rdCTEVNo";
            this.rdCTEVNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTEVNo.TabIndex = 1;
            this.rdCTEVNo.TabStop = true;
            this.rdCTEVNo.Tag = "Tower";
            this.rdCTEVNo.Text = "NO";
            this.rdCTEVNo.UseVisualStyleBackColor = true;
            this.rdCTEVNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTEVNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTEVYes
            // 
            this.rdCTEVYes.AutoSize = true;
            this.rdCTEVYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTEVYes.Name = "rdCTEVYes";
            this.rdCTEVYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTEVYes.TabIndex = 0;
            this.rdCTEVYes.TabStop = true;
            this.rdCTEVYes.Tag = "Tower";
            this.rdCTEVYes.Text = "YES";
            this.rdCTEVYes.UseVisualStyleBackColor = true;
            this.rdCTEVYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label131
            // 
            this.label131.Location = new System.Drawing.Point(359, 175);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(78, 34);
            this.label131.TabIndex = 48;
            this.label131.Text = "EV, VPD, Third Party Control:";
            // 
            // txtCTTip3
            // 
            this.txtCTTip3.Location = new System.Drawing.Point(713, 149);
            this.txtCTTip3.Name = "txtCTTip3";
            this.txtCTTip3.Size = new System.Drawing.Size(140, 20);
            this.txtCTTip3.TabIndex = 47;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(669, 151);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(34, 13);
            this.label121.TabIndex = 46;
            this.label121.Text = "Tip 3:";
            // 
            // txtCTTip2
            // 
            this.txtCTTip2.Location = new System.Drawing.Point(713, 124);
            this.txtCTTip2.Name = "txtCTTip2";
            this.txtCTTip2.Size = new System.Drawing.Size(140, 20);
            this.txtCTTip2.TabIndex = 45;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(669, 126);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(34, 13);
            this.label122.TabIndex = 44;
            this.label122.Text = "Tip 2:";
            // 
            // txtCTTip1
            // 
            this.txtCTTip1.Location = new System.Drawing.Point(713, 98);
            this.txtCTTip1.Name = "txtCTTip1";
            this.txtCTTip1.Size = new System.Drawing.Size(140, 20);
            this.txtCTTip1.TabIndex = 43;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(669, 101);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(34, 13);
            this.label123.TabIndex = 42;
            this.label123.Text = "Tip 1:";
            // 
            // txtCTNumberOfRows
            // 
            this.txtCTNumberOfRows.Location = new System.Drawing.Point(152, 76);
            this.txtCTNumberOfRows.Mask = "G";
            this.txtCTNumberOfRows.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtCTNumberOfRows.Name = "txtCTNumberOfRows";
            this.txtCTNumberOfRows.Size = new System.Drawing.Size(147, 20);
            this.txtCTNumberOfRows.TabIndex = 35;
            this.txtCTNumberOfRows.TabStop = false;
            this.txtCTNumberOfRows.Tag = "SingleRail";
            this.txtCTNumberOfRows.Text = "0";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(57, 80);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(91, 13);
            this.label110.TabIndex = 34;
            this.label110.Text = "Number Of Rows:";
            // 
            // pnlCTWaterFeedSystem
            // 
            this.pnlCTWaterFeedSystem.Controls.Add(this.radioButton29);
            this.pnlCTWaterFeedSystem.Controls.Add(this.rdCTWaterFeedSystemNo);
            this.pnlCTWaterFeedSystem.Controls.Add(this.rdCTWaterFeedSystemYes);
            this.pnlCTWaterFeedSystem.Location = new System.Drawing.Point(1018, 124);
            this.pnlCTWaterFeedSystem.Name = "pnlCTWaterFeedSystem";
            this.pnlCTWaterFeedSystem.Size = new System.Drawing.Size(131, 31);
            this.pnlCTWaterFeedSystem.TabIndex = 33;
            // 
            // radioButton29
            // 
            this.radioButton29.AutoSize = true;
            this.radioButton29.Location = new System.Drawing.Point(-290, 41);
            this.radioButton29.Name = "radioButton29";
            this.radioButton29.Size = new System.Drawing.Size(41, 17);
            this.radioButton29.TabIndex = 46;
            this.radioButton29.TabStop = true;
            this.radioButton29.Tag = "Echo";
            this.radioButton29.Text = "NO";
            this.radioButton29.UseVisualStyleBackColor = true;
            // 
            // rdCTWaterFeedSystemNo
            // 
            this.rdCTWaterFeedSystemNo.AutoSize = true;
            this.rdCTWaterFeedSystemNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTWaterFeedSystemNo.Name = "rdCTWaterFeedSystemNo";
            this.rdCTWaterFeedSystemNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTWaterFeedSystemNo.TabIndex = 1;
            this.rdCTWaterFeedSystemNo.TabStop = true;
            this.rdCTWaterFeedSystemNo.Tag = "Tower";
            this.rdCTWaterFeedSystemNo.Text = "NO";
            this.rdCTWaterFeedSystemNo.UseVisualStyleBackColor = true;
            this.rdCTWaterFeedSystemNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTWaterFeedSystemNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTWaterFeedSystemYes
            // 
            this.rdCTWaterFeedSystemYes.AutoSize = true;
            this.rdCTWaterFeedSystemYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTWaterFeedSystemYes.Name = "rdCTWaterFeedSystemYes";
            this.rdCTWaterFeedSystemYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTWaterFeedSystemYes.TabIndex = 0;
            this.rdCTWaterFeedSystemYes.TabStop = true;
            this.rdCTWaterFeedSystemYes.Tag = "Tower";
            this.rdCTWaterFeedSystemYes.Text = "YES";
            this.rdCTWaterFeedSystemYes.UseVisualStyleBackColor = true;
            this.rdCTWaterFeedSystemYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label92
            // 
            this.label92.Location = new System.Drawing.Point(922, 124);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(90, 29);
            this.label92.TabIndex = 32;
            this.label92.Text = "Center Water Feed System:";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlCTGallonTank
            // 
            this.pnlCTGallonTank.Controls.Add(this.rdCTGallonTankNo);
            this.pnlCTGallonTank.Controls.Add(this.rdCTGallonTankYes);
            this.pnlCTGallonTank.Location = new System.Drawing.Point(1018, 86);
            this.pnlCTGallonTank.Name = "pnlCTGallonTank";
            this.pnlCTGallonTank.Size = new System.Drawing.Size(131, 31);
            this.pnlCTGallonTank.TabIndex = 31;
            // 
            // rdCTGallonTankNo
            // 
            this.rdCTGallonTankNo.AutoSize = true;
            this.rdCTGallonTankNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTGallonTankNo.Name = "rdCTGallonTankNo";
            this.rdCTGallonTankNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTGallonTankNo.TabIndex = 1;
            this.rdCTGallonTankNo.TabStop = true;
            this.rdCTGallonTankNo.Tag = "Tower";
            this.rdCTGallonTankNo.Text = "NO";
            this.rdCTGallonTankNo.UseVisualStyleBackColor = true;
            this.rdCTGallonTankNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTGallonTankNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTGallonTankYes
            // 
            this.rdCTGallonTankYes.AutoSize = true;
            this.rdCTGallonTankYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTGallonTankYes.Name = "rdCTGallonTankYes";
            this.rdCTGallonTankYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTGallonTankYes.TabIndex = 0;
            this.rdCTGallonTankYes.TabStop = true;
            this.rdCTGallonTankYes.Tag = "Tower";
            this.rdCTGallonTankYes.Text = "YES";
            this.rdCTGallonTankYes.UseVisualStyleBackColor = true;
            this.rdCTGallonTankYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // pnlCTConveyor
            // 
            this.pnlCTConveyor.Controls.Add(this.rdCTConveyorNo);
            this.pnlCTConveyor.Controls.Add(this.rdCTConveyorYes);
            this.pnlCTConveyor.Location = new System.Drawing.Point(1018, 197);
            this.pnlCTConveyor.Name = "pnlCTConveyor";
            this.pnlCTConveyor.Size = new System.Drawing.Size(131, 31);
            this.pnlCTConveyor.TabIndex = 27;
            // 
            // rdCTConveyorNo
            // 
            this.rdCTConveyorNo.AutoSize = true;
            this.rdCTConveyorNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTConveyorNo.Name = "rdCTConveyorNo";
            this.rdCTConveyorNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTConveyorNo.TabIndex = 1;
            this.rdCTConveyorNo.TabStop = true;
            this.rdCTConveyorNo.Tag = "Tower";
            this.rdCTConveyorNo.Text = "NO";
            this.rdCTConveyorNo.UseVisualStyleBackColor = true;
            this.rdCTConveyorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTConveyorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTConveyorYes
            // 
            this.rdCTConveyorYes.AutoSize = true;
            this.rdCTConveyorYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTConveyorYes.Name = "rdCTConveyorYes";
            this.rdCTConveyorYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTConveyorYes.TabIndex = 0;
            this.rdCTConveyorYes.TabStop = true;
            this.rdCTConveyorYes.Tag = "Tower";
            this.rdCTConveyorYes.Text = "YES";
            this.rdCTConveyorYes.UseVisualStyleBackColor = true;
            this.rdCTConveyorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label89
            // 
            this.label89.Location = new System.Drawing.Point(922, 84);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(90, 36);
            this.label89.TabIndex = 30;
            this.label89.Text = "25 Gallon Reservoir Tank:";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(958, 206);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(55, 13);
            this.label87.TabIndex = 26;
            this.label87.Text = "Conveyor:";
            // 
            // pnlCTDualMotorNoAxle
            // 
            this.pnlCTDualMotorNoAxle.Controls.Add(this.radioButton24);
            this.pnlCTDualMotorNoAxle.Controls.Add(this.rdCTDualMotorNoAxleNo);
            this.pnlCTDualMotorNoAxle.Controls.Add(this.rdCTDualMotorNoAxleYes);
            this.pnlCTDualMotorNoAxle.Location = new System.Drawing.Point(1018, 47);
            this.pnlCTDualMotorNoAxle.Name = "pnlCTDualMotorNoAxle";
            this.pnlCTDualMotorNoAxle.Size = new System.Drawing.Size(131, 31);
            this.pnlCTDualMotorNoAxle.TabIndex = 29;
            // 
            // radioButton24
            // 
            this.radioButton24.AutoSize = true;
            this.radioButton24.Location = new System.Drawing.Point(-290, 41);
            this.radioButton24.Name = "radioButton24";
            this.radioButton24.Size = new System.Drawing.Size(41, 17);
            this.radioButton24.TabIndex = 46;
            this.radioButton24.TabStop = true;
            this.radioButton24.Tag = "Echo";
            this.radioButton24.Text = "NO";
            this.radioButton24.UseVisualStyleBackColor = true;
            // 
            // rdCTDualMotorNoAxleNo
            // 
            this.rdCTDualMotorNoAxleNo.AutoSize = true;
            this.rdCTDualMotorNoAxleNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTDualMotorNoAxleNo.Name = "rdCTDualMotorNoAxleNo";
            this.rdCTDualMotorNoAxleNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTDualMotorNoAxleNo.TabIndex = 1;
            this.rdCTDualMotorNoAxleNo.TabStop = true;
            this.rdCTDualMotorNoAxleNo.Tag = "Tower";
            this.rdCTDualMotorNoAxleNo.Text = "NO";
            this.rdCTDualMotorNoAxleNo.UseVisualStyleBackColor = true;
            this.rdCTDualMotorNoAxleNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTDualMotorNoAxleNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTDualMotorNoAxleYes
            // 
            this.rdCTDualMotorNoAxleYes.AutoSize = true;
            this.rdCTDualMotorNoAxleYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTDualMotorNoAxleYes.Name = "rdCTDualMotorNoAxleYes";
            this.rdCTDualMotorNoAxleYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTDualMotorNoAxleYes.TabIndex = 0;
            this.rdCTDualMotorNoAxleYes.TabStop = true;
            this.rdCTDualMotorNoAxleYes.Tag = "Tower";
            this.rdCTDualMotorNoAxleYes.Text = "YES";
            this.rdCTDualMotorNoAxleYes.UseVisualStyleBackColor = true;
            this.rdCTDualMotorNoAxleYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(907, 56);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(110, 13);
            this.label90.TabIndex = 28;
            this.label90.Text = "Dual Motor / No Axle:";
            // 
            // pnlCTDiamondSweep
            // 
            this.pnlCTDiamondSweep.Controls.Add(this.radioButton19);
            this.pnlCTDiamondSweep.Controls.Add(this.rdCTDiamondSweepNo);
            this.pnlCTDiamondSweep.Controls.Add(this.rdCTDiamondSweepYes);
            this.pnlCTDiamondSweep.Location = new System.Drawing.Point(152, 235);
            this.pnlCTDiamondSweep.Name = "pnlCTDiamondSweep";
            this.pnlCTDiamondSweep.Size = new System.Drawing.Size(147, 31);
            this.pnlCTDiamondSweep.TabIndex = 25;
            // 
            // radioButton19
            // 
            this.radioButton19.AutoSize = true;
            this.radioButton19.Location = new System.Drawing.Point(-290, 41);
            this.radioButton19.Name = "radioButton19";
            this.radioButton19.Size = new System.Drawing.Size(41, 17);
            this.radioButton19.TabIndex = 46;
            this.radioButton19.TabStop = true;
            this.radioButton19.Tag = "Echo";
            this.radioButton19.Text = "NO";
            this.radioButton19.UseVisualStyleBackColor = true;
            // 
            // rdCTDiamondSweepNo
            // 
            this.rdCTDiamondSweepNo.AutoSize = true;
            this.rdCTDiamondSweepNo.Location = new System.Drawing.Point(98, 7);
            this.rdCTDiamondSweepNo.Name = "rdCTDiamondSweepNo";
            this.rdCTDiamondSweepNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTDiamondSweepNo.TabIndex = 1;
            this.rdCTDiamondSweepNo.TabStop = true;
            this.rdCTDiamondSweepNo.Tag = "Tower";
            this.rdCTDiamondSweepNo.Text = "NO";
            this.rdCTDiamondSweepNo.UseVisualStyleBackColor = true;
            this.rdCTDiamondSweepNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTDiamondSweepNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTDiamondSweepYes
            // 
            this.rdCTDiamondSweepYes.AutoSize = true;
            this.rdCTDiamondSweepYes.Location = new System.Drawing.Point(26, 7);
            this.rdCTDiamondSweepYes.Name = "rdCTDiamondSweepYes";
            this.rdCTDiamondSweepYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTDiamondSweepYes.TabIndex = 0;
            this.rdCTDiamondSweepYes.TabStop = true;
            this.rdCTDiamondSweepYes.Tag = "Tower";
            this.rdCTDiamondSweepYes.Text = "YES";
            this.rdCTDiamondSweepYes.UseVisualStyleBackColor = true;
            this.rdCTDiamondSweepYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(58, 241);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(88, 13);
            this.label88.TabIndex = 24;
            this.label88.Text = "Diamond Sweep:";
            // 
            // pnlCT1_1_4thHoseUpgrade
            // 
            this.pnlCT1_1_4thHoseUpgrade.Controls.Add(this.rdCT1_1_4thHoseUpgradeNo);
            this.pnlCT1_1_4thHoseUpgrade.Controls.Add(this.rdCT1_1_4thHoseUpgradeYes);
            this.pnlCT1_1_4thHoseUpgrade.Location = new System.Drawing.Point(713, 11);
            this.pnlCT1_1_4thHoseUpgrade.Name = "pnlCT1_1_4thHoseUpgrade";
            this.pnlCT1_1_4thHoseUpgrade.Size = new System.Drawing.Size(140, 31);
            this.pnlCT1_1_4thHoseUpgrade.TabIndex = 23;
            // 
            // rdCT1_1_4thHoseUpgradeNo
            // 
            this.rdCT1_1_4thHoseUpgradeNo.AutoSize = true;
            this.rdCT1_1_4thHoseUpgradeNo.Location = new System.Drawing.Point(87, 7);
            this.rdCT1_1_4thHoseUpgradeNo.Name = "rdCT1_1_4thHoseUpgradeNo";
            this.rdCT1_1_4thHoseUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdCT1_1_4thHoseUpgradeNo.TabIndex = 1;
            this.rdCT1_1_4thHoseUpgradeNo.TabStop = true;
            this.rdCT1_1_4thHoseUpgradeNo.Tag = "Tower";
            this.rdCT1_1_4thHoseUpgradeNo.Text = "NO";
            this.rdCT1_1_4thHoseUpgradeNo.UseVisualStyleBackColor = true;
            this.rdCT1_1_4thHoseUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCT1_1_4thHoseUpgradeNo.Click += new System.EventHandler(this.HoseUpgradeNo_Click);
            this.rdCT1_1_4thHoseUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCT1_1_4thHoseUpgradeYes
            // 
            this.rdCT1_1_4thHoseUpgradeYes.AutoSize = true;
            this.rdCT1_1_4thHoseUpgradeYes.Location = new System.Drawing.Point(20, 7);
            this.rdCT1_1_4thHoseUpgradeYes.Name = "rdCT1_1_4thHoseUpgradeYes";
            this.rdCT1_1_4thHoseUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdCT1_1_4thHoseUpgradeYes.TabIndex = 0;
            this.rdCT1_1_4thHoseUpgradeYes.TabStop = true;
            this.rdCT1_1_4thHoseUpgradeYes.Tag = "Tower";
            this.rdCT1_1_4thHoseUpgradeYes.Text = "YES";
            this.rdCT1_1_4thHoseUpgradeYes.UseVisualStyleBackColor = true;
            this.rdCT1_1_4thHoseUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCT1_1_4thHoseUpgradeYes.Click += new System.EventHandler(this.HoseUpgradeYes_Click);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(598, 20);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(113, 13);
            this.label86.TabIndex = 22;
            this.label86.Text = "1 1/4\" Hose Upgrade:";
            // 
            // txtCTHoseLoopHeight
            // 
            this.txtCTHoseLoopHeight.Location = new System.Drawing.Point(152, 170);
            this.txtCTHoseLoopHeight.Mask = "F2";
            this.txtCTHoseLoopHeight.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtCTHoseLoopHeight.Name = "txtCTHoseLoopHeight";
            this.txtCTHoseLoopHeight.Size = new System.Drawing.Size(147, 20);
            this.txtCTHoseLoopHeight.TabIndex = 11;
            this.txtCTHoseLoopHeight.TabStop = false;
            this.txtCTHoseLoopHeight.Tag = "Tower";
            this.txtCTHoseLoopHeight.Text = "0.00";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(28, 173);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(116, 13);
            this.label85.TabIndex = 10;
            this.label85.Text = "Hose Loop Height (IN):";
            // 
            // pnlCTWirelessWalkSwitch
            // 
            this.pnlCTWirelessWalkSwitch.Controls.Add(this.rdCTWirelessWalkSwitchNo);
            this.pnlCTWirelessWalkSwitch.Controls.Add(this.rdCTWirelessWalkSwitchYes);
            this.pnlCTWirelessWalkSwitch.Location = new System.Drawing.Point(1018, 163);
            this.pnlCTWirelessWalkSwitch.Name = "pnlCTWirelessWalkSwitch";
            this.pnlCTWirelessWalkSwitch.Size = new System.Drawing.Size(131, 31);
            this.pnlCTWirelessWalkSwitch.TabIndex = 21;
            // 
            // rdCTWirelessWalkSwitchNo
            // 
            this.rdCTWirelessWalkSwitchNo.AutoSize = true;
            this.rdCTWirelessWalkSwitchNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTWirelessWalkSwitchNo.Name = "rdCTWirelessWalkSwitchNo";
            this.rdCTWirelessWalkSwitchNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTWirelessWalkSwitchNo.TabIndex = 1;
            this.rdCTWirelessWalkSwitchNo.TabStop = true;
            this.rdCTWirelessWalkSwitchNo.Tag = "Tower";
            this.rdCTWirelessWalkSwitchNo.Text = "NO";
            this.rdCTWirelessWalkSwitchNo.UseVisualStyleBackColor = true;
            this.rdCTWirelessWalkSwitchNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTWirelessWalkSwitchNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTWirelessWalkSwitchYes
            // 
            this.rdCTWirelessWalkSwitchYes.AutoSize = true;
            this.rdCTWirelessWalkSwitchYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTWirelessWalkSwitchYes.Name = "rdCTWirelessWalkSwitchYes";
            this.rdCTWirelessWalkSwitchYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTWirelessWalkSwitchYes.TabIndex = 0;
            this.rdCTWirelessWalkSwitchYes.TabStop = true;
            this.rdCTWirelessWalkSwitchYes.Tag = "Tower";
            this.rdCTWirelessWalkSwitchYes.Text = "YES";
            this.rdCTWirelessWalkSwitchYes.UseVisualStyleBackColor = true;
            this.rdCTWirelessWalkSwitchYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(898, 172);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(113, 13);
            this.label67.TabIndex = 20;
            this.label67.Text = "Wireless Walk Switch:";
            // 
            // pnlCTTripleTurretBody
            // 
            this.pnlCTTripleTurretBody.Controls.Add(this.rdCTTripleTurretBodyNo);
            this.pnlCTTripleTurretBody.Controls.Add(this.rdCTTripleTurretBodyYes);
            this.pnlCTTripleTurretBody.Location = new System.Drawing.Point(713, 51);
            this.pnlCTTripleTurretBody.Name = "pnlCTTripleTurretBody";
            this.pnlCTTripleTurretBody.Size = new System.Drawing.Size(139, 31);
            this.pnlCTTripleTurretBody.TabIndex = 7;
            // 
            // rdCTTripleTurretBodyNo
            // 
            this.rdCTTripleTurretBodyNo.AutoSize = true;
            this.rdCTTripleTurretBodyNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTTripleTurretBodyNo.Name = "rdCTTripleTurretBodyNo";
            this.rdCTTripleTurretBodyNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTTripleTurretBodyNo.TabIndex = 1;
            this.rdCTTripleTurretBodyNo.TabStop = true;
            this.rdCTTripleTurretBodyNo.Tag = "Tower";
            this.rdCTTripleTurretBodyNo.Text = "NO";
            this.rdCTTripleTurretBodyNo.UseVisualStyleBackColor = true;
            this.rdCTTripleTurretBodyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTTripleTurretBodyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTTripleTurretBodyYes
            // 
            this.rdCTTripleTurretBodyYes.AutoSize = true;
            this.rdCTTripleTurretBodyYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTTripleTurretBodyYes.Name = "rdCTTripleTurretBodyYes";
            this.rdCTTripleTurretBodyYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTTripleTurretBodyYes.TabIndex = 0;
            this.rdCTTripleTurretBodyYes.TabStop = true;
            this.rdCTTripleTurretBodyYes.Tag = "Tower";
            this.rdCTTripleTurretBodyYes.Text = "YES";
            this.rdCTTripleTurretBodyYes.UseVisualStyleBackColor = true;
            this.rdCTTripleTurretBodyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(607, 59);
            this.label75.MaximumSize = new System.Drawing.Size(150, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(94, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "Triple Turret Body:";
            // 
            // txtCTQuantityOfSystems
            // 
            this.txtCTQuantityOfSystems.Location = new System.Drawing.Point(152, 15);
            this.txtCTQuantityOfSystems.Mask = "G";
            this.txtCTQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtCTQuantityOfSystems.Name = "txtCTQuantityOfSystems";
            this.txtCTQuantityOfSystems.Size = new System.Drawing.Size(147, 20);
            this.txtCTQuantityOfSystems.TabIndex = 1;
            this.txtCTQuantityOfSystems.TabStop = false;
            this.txtCTQuantityOfSystems.Tag = "Tower";
            this.txtCTQuantityOfSystems.Text = "0";
            this.txtCTQuantityOfSystems.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // pnlCTMountedInjector
            // 
            this.pnlCTMountedInjector.Controls.Add(this.rdCTMountedInjectorNo);
            this.pnlCTMountedInjector.Controls.Add(this.rdCTMountedInjectorYes);
            this.pnlCTMountedInjector.Location = new System.Drawing.Point(445, 50);
            this.pnlCTMountedInjector.Name = "pnlCTMountedInjector";
            this.pnlCTMountedInjector.Size = new System.Drawing.Size(139, 31);
            this.pnlCTMountedInjector.TabIndex = 15;
            // 
            // rdCTMountedInjectorNo
            // 
            this.rdCTMountedInjectorNo.AutoSize = true;
            this.rdCTMountedInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTMountedInjectorNo.Name = "rdCTMountedInjectorNo";
            this.rdCTMountedInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTMountedInjectorNo.TabIndex = 1;
            this.rdCTMountedInjectorNo.TabStop = true;
            this.rdCTMountedInjectorNo.Tag = "Tower";
            this.rdCTMountedInjectorNo.Text = "NO";
            this.rdCTMountedInjectorNo.UseVisualStyleBackColor = true;
            this.rdCTMountedInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTMountedInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTMountedInjectorYes
            // 
            this.rdCTMountedInjectorYes.AutoSize = true;
            this.rdCTMountedInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTMountedInjectorYes.Name = "rdCTMountedInjectorYes";
            this.rdCTMountedInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTMountedInjectorYes.TabIndex = 0;
            this.rdCTMountedInjectorYes.TabStop = true;
            this.rdCTMountedInjectorYes.Tag = "Tower";
            this.rdCTMountedInjectorYes.Text = "YES";
            this.rdCTMountedInjectorYes.UseVisualStyleBackColor = true;
            this.rdCTMountedInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(391, 56);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(45, 13);
            this.label78.TabIndex = 14;
            this.label78.Text = "Injector:";
            // 
            // pnlCTPressureRegulator
            // 
            this.pnlCTPressureRegulator.Controls.Add(this.radioButton9);
            this.pnlCTPressureRegulator.Controls.Add(this.rdCTPressureRegulatorNo);
            this.pnlCTPressureRegulator.Controls.Add(this.rdCTPressureRegulatorYes);
            this.pnlCTPressureRegulator.Location = new System.Drawing.Point(445, 11);
            this.pnlCTPressureRegulator.Name = "pnlCTPressureRegulator";
            this.pnlCTPressureRegulator.Size = new System.Drawing.Size(139, 31);
            this.pnlCTPressureRegulator.TabIndex = 13;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(-290, 41);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(41, 17);
            this.radioButton9.TabIndex = 46;
            this.radioButton9.TabStop = true;
            this.radioButton9.Tag = "Echo";
            this.radioButton9.Text = "NO";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // rdCTPressureRegulatorNo
            // 
            this.rdCTPressureRegulatorNo.AutoSize = true;
            this.rdCTPressureRegulatorNo.Location = new System.Drawing.Point(87, 7);
            this.rdCTPressureRegulatorNo.Name = "rdCTPressureRegulatorNo";
            this.rdCTPressureRegulatorNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTPressureRegulatorNo.TabIndex = 1;
            this.rdCTPressureRegulatorNo.TabStop = true;
            this.rdCTPressureRegulatorNo.Tag = "Tower";
            this.rdCTPressureRegulatorNo.Text = "NO";
            this.rdCTPressureRegulatorNo.UseVisualStyleBackColor = true;
            this.rdCTPressureRegulatorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTPressureRegulatorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTPressureRegulatorYes
            // 
            this.rdCTPressureRegulatorYes.AutoSize = true;
            this.rdCTPressureRegulatorYes.Location = new System.Drawing.Point(20, 7);
            this.rdCTPressureRegulatorYes.Name = "rdCTPressureRegulatorYes";
            this.rdCTPressureRegulatorYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTPressureRegulatorYes.TabIndex = 0;
            this.rdCTPressureRegulatorYes.TabStop = true;
            this.rdCTPressureRegulatorYes.Tag = "Tower";
            this.rdCTPressureRegulatorYes.Text = "YES";
            this.rdCTPressureRegulatorYes.UseVisualStyleBackColor = true;
            this.rdCTPressureRegulatorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(335, 19);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(100, 13);
            this.label79.TabIndex = 12;
            this.label79.Text = "Pressure Regulator:";
            // 
            // txtCTSpraybodySpacing
            // 
            this.txtCTSpraybodySpacing.Location = new System.Drawing.Point(152, 136);
            this.txtCTSpraybodySpacing.Mask = "F2";
            this.txtCTSpraybodySpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtCTSpraybodySpacing.Name = "txtCTSpraybodySpacing";
            this.txtCTSpraybodySpacing.Size = new System.Drawing.Size(147, 20);
            this.txtCTSpraybodySpacing.TabIndex = 19;
            this.txtCTSpraybodySpacing.TabStop = false;
            this.txtCTSpraybodySpacing.Tag = "Tower";
            this.txtCTSpraybodySpacing.Text = "0.00";
            this.txtCTSpraybodySpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(22, 140);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(122, 13);
            this.label80.TabIndex = 18;
            this.label80.Text = "Spraybody Spacing (IN):";
            // 
            // pnlCTTripleWaterLocklineUpgrade
            // 
            this.pnlCTTripleWaterLocklineUpgrade.Controls.Add(this.rdCTTripleWaterLocklineUpgradeNo);
            this.pnlCTTripleWaterLocklineUpgrade.Controls.Add(this.rdCTTripleWaterLocklineUpgradeYes);
            this.pnlCTTripleWaterLocklineUpgrade.Location = new System.Drawing.Point(152, 199);
            this.pnlCTTripleWaterLocklineUpgrade.Name = "pnlCTTripleWaterLocklineUpgrade";
            this.pnlCTTripleWaterLocklineUpgrade.Size = new System.Drawing.Size(147, 31);
            this.pnlCTTripleWaterLocklineUpgrade.TabIndex = 5;
            // 
            // rdCTTripleWaterLocklineUpgradeNo
            // 
            this.rdCTTripleWaterLocklineUpgradeNo.AutoSize = true;
            this.rdCTTripleWaterLocklineUpgradeNo.Location = new System.Drawing.Point(98, 7);
            this.rdCTTripleWaterLocklineUpgradeNo.Name = "rdCTTripleWaterLocklineUpgradeNo";
            this.rdCTTripleWaterLocklineUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdCTTripleWaterLocklineUpgradeNo.TabIndex = 1;
            this.rdCTTripleWaterLocklineUpgradeNo.TabStop = true;
            this.rdCTTripleWaterLocklineUpgradeNo.Tag = "Tower";
            this.rdCTTripleWaterLocklineUpgradeNo.Text = "NO";
            this.rdCTTripleWaterLocklineUpgradeNo.UseVisualStyleBackColor = true;
            this.rdCTTripleWaterLocklineUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdCTTripleWaterLocklineUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdCTTripleWaterLocklineUpgradeYes
            // 
            this.rdCTTripleWaterLocklineUpgradeYes.AutoSize = true;
            this.rdCTTripleWaterLocklineUpgradeYes.Location = new System.Drawing.Point(26, 7);
            this.rdCTTripleWaterLocklineUpgradeYes.Name = "rdCTTripleWaterLocklineUpgradeYes";
            this.rdCTTripleWaterLocklineUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdCTTripleWaterLocklineUpgradeYes.TabIndex = 0;
            this.rdCTTripleWaterLocklineUpgradeYes.TabStop = true;
            this.rdCTTripleWaterLocklineUpgradeYes.Tag = "Tower";
            this.rdCTTripleWaterLocklineUpgradeYes.Text = "YES";
            this.rdCTTripleWaterLocklineUpgradeYes.UseVisualStyleBackColor = true;
            this.rdCTTripleWaterLocklineUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(17, 204);
            this.label81.MaximumSize = new System.Drawing.Size(150, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(136, 26);
            this.label81.TabIndex = 4;
            this.label81.Text = "Step/Stop/Water Lockline Upgrade:";
            // 
            // ddlCTControllerType
            // 
            this.ddlCTControllerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCTControllerType.FormattingEnabled = true;
            this.ddlCTControllerType.Location = new System.Drawing.Point(152, 106);
            this.ddlCTControllerType.Name = "ddlCTControllerType";
            this.ddlCTControllerType.Size = new System.Drawing.Size(149, 21);
            this.ddlCTControllerType.TabIndex = 9;
            this.ddlCTControllerType.Tag = "Tower";
            this.ddlCTControllerType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(62, 108);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(81, 13);
            this.label82.TabIndex = 8;
            this.label82.Text = "Controller Type:";
            // 
            // ddlCTSingleDualWater
            // 
            this.ddlCTSingleDualWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCTSingleDualWater.FormattingEnabled = true;
            this.ddlCTSingleDualWater.Location = new System.Drawing.Point(152, 47);
            this.ddlCTSingleDualWater.Name = "ddlCTSingleDualWater";
            this.ddlCTSingleDualWater.Size = new System.Drawing.Size(147, 21);
            this.ddlCTSingleDualWater.TabIndex = 3;
            this.ddlCTSingleDualWater.Tag = "Tower";
            this.ddlCTSingleDualWater.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(52, 51);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(98, 13);
            this.label83.TabIndex = 2;
            this.label83.Text = "Single/Dual Water:";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(40, 18);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(105, 13);
            this.label84.TabIndex = 0;
            this.label84.Text = "Quantity Of Systems:";
            // 
            // tpNavigatorBoom
            // 
            this.tpNavigatorBoom.Controls.Add(this.ddlNavEVWireGauge);
            this.tpNavigatorBoom.Controls.Add(this.label217);
            this.tpNavigatorBoom.Controls.Add(this.ddlNavInjectorType);
            this.tpNavigatorBoom.Controls.Add(this.label157);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavAutoInjector);
            this.tpNavigatorBoom.Controls.Add(this.label158);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavWaterSolenoidControl);
            this.tpNavigatorBoom.Controls.Add(this.label148);
            this.tpNavigatorBoom.Controls.Add(this.ddlNavFeed);
            this.tpNavigatorBoom.Controls.Add(this.label139);
            this.tpNavigatorBoom.Controls.Add(this.ddlNavCarrySteel);
            this.tpNavigatorBoom.Controls.Add(this.label133);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavEV);
            this.tpNavigatorBoom.Controls.Add(this.label132);
            this.tpNavigatorBoom.Controls.Add(this.txtNavTip3);
            this.tpNavigatorBoom.Controls.Add(this.label124);
            this.tpNavigatorBoom.Controls.Add(this.txtNavTip2);
            this.tpNavigatorBoom.Controls.Add(this.label125);
            this.tpNavigatorBoom.Controls.Add(this.txtNavTip1);
            this.tpNavigatorBoom.Controls.Add(this.label126);
            this.tpNavigatorBoom.Controls.Add(this.txtNavNumberOfRows);
            this.tpNavigatorBoom.Controls.Add(this.label111);
            this.tpNavigatorBoom.Controls.Add(this.ddlNavControllerType);
            this.tpNavigatorBoom.Controls.Add(this.label94);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavWaterFeedSystem);
            this.tpNavigatorBoom.Controls.Add(this.label91);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavGallonTank);
            this.tpNavigatorBoom.Controls.Add(this.label93);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavSweep90);
            this.tpNavigatorBoom.Controls.Add(this.label95);
            this.tpNavigatorBoom.Controls.Add(this.pnlNav1_1_4thHoseUpgrade);
            this.tpNavigatorBoom.Controls.Add(this.label97);
            this.tpNavigatorBoom.Controls.Add(this.txtNavHoseLoopHeight);
            this.tpNavigatorBoom.Controls.Add(this.label98);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavWirelessWalkSwitch);
            this.tpNavigatorBoom.Controls.Add(this.label99);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavTripleTurretBody);
            this.tpNavigatorBoom.Controls.Add(this.label102);
            this.tpNavigatorBoom.Controls.Add(this.txtNavQuantityOfSystems);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavMountedInjector);
            this.tpNavigatorBoom.Controls.Add(this.label103);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavPressureRegulator);
            this.tpNavigatorBoom.Controls.Add(this.label104);
            this.tpNavigatorBoom.Controls.Add(this.txtNavSpraybodySpacing);
            this.tpNavigatorBoom.Controls.Add(this.label105);
            this.tpNavigatorBoom.Controls.Add(this.pnlNavTripleWaterLocklineUpgrade);
            this.tpNavigatorBoom.Controls.Add(this.label106);
            this.tpNavigatorBoom.Controls.Add(this.ddlNavSingleDualWater);
            this.tpNavigatorBoom.Controls.Add(this.label108);
            this.tpNavigatorBoom.Controls.Add(this.label109);
            this.tpNavigatorBoom.Location = new System.Drawing.Point(4, 22);
            this.tpNavigatorBoom.Margin = new System.Windows.Forms.Padding(2);
            this.tpNavigatorBoom.Name = "tpNavigatorBoom";
            this.tpNavigatorBoom.Size = new System.Drawing.Size(1368, 267);
            this.tpNavigatorBoom.TabIndex = 5;
            this.tpNavigatorBoom.Tag = "Navigator";
            this.tpNavigatorBoom.Text = "Navigator";
            this.tpNavigatorBoom.UseVisualStyleBackColor = true;
            // 
            // ddlNavEVWireGauge
            // 
            this.ddlNavEVWireGauge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlNavEVWireGauge.FormattingEnabled = true;
            this.ddlNavEVWireGauge.Location = new System.Drawing.Point(455, 222);
            this.ddlNavEVWireGauge.Name = "ddlNavEVWireGauge";
            this.ddlNavEVWireGauge.Size = new System.Drawing.Size(140, 21);
            this.ddlNavEVWireGauge.TabIndex = 67;
            this.ddlNavEVWireGauge.Tag = "SingleRail";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(360, 225);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(86, 13);
            this.label217.TabIndex = 66;
            this.label217.Text = "EV Wire/Gauge:";
            // 
            // ddlNavInjectorType
            // 
            this.ddlNavInjectorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlNavInjectorType.FormattingEnabled = true;
            this.ddlNavInjectorType.Location = new System.Drawing.Point(455, 108);
            this.ddlNavInjectorType.Name = "ddlNavInjectorType";
            this.ddlNavInjectorType.Size = new System.Drawing.Size(140, 21);
            this.ddlNavInjectorType.TabIndex = 65;
            this.ddlNavInjectorType.Tag = "Navigator";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(375, 110);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(72, 13);
            this.label157.TabIndex = 64;
            this.label157.Text = "Injector Type:";
            // 
            // pnlNavAutoInjector
            // 
            this.pnlNavAutoInjector.Controls.Add(this.rdNavAutoInjectorNo);
            this.pnlNavAutoInjector.Controls.Add(this.rdNavAutoInjectorYes);
            this.pnlNavAutoInjector.Location = new System.Drawing.Point(455, 140);
            this.pnlNavAutoInjector.Name = "pnlNavAutoInjector";
            this.pnlNavAutoInjector.Size = new System.Drawing.Size(139, 31);
            this.pnlNavAutoInjector.TabIndex = 63;
            // 
            // rdNavAutoInjectorNo
            // 
            this.rdNavAutoInjectorNo.AutoSize = true;
            this.rdNavAutoInjectorNo.Location = new System.Drawing.Point(86, 7);
            this.rdNavAutoInjectorNo.Name = "rdNavAutoInjectorNo";
            this.rdNavAutoInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavAutoInjectorNo.TabIndex = 1;
            this.rdNavAutoInjectorNo.TabStop = true;
            this.rdNavAutoInjectorNo.Tag = "Navigator";
            this.rdNavAutoInjectorNo.Text = "NO";
            this.rdNavAutoInjectorNo.UseVisualStyleBackColor = true;
            this.rdNavAutoInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavAutoInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavAutoInjectorYes
            // 
            this.rdNavAutoInjectorYes.AutoSize = true;
            this.rdNavAutoInjectorYes.Location = new System.Drawing.Point(19, 7);
            this.rdNavAutoInjectorYes.Name = "rdNavAutoInjectorYes";
            this.rdNavAutoInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavAutoInjectorYes.TabIndex = 0;
            this.rdNavAutoInjectorYes.TabStop = true;
            this.rdNavAutoInjectorYes.Tag = "Navigator";
            this.rdNavAutoInjectorYes.Text = "YES";
            this.rdNavAutoInjectorYes.UseVisualStyleBackColor = true;
            this.rdNavAutoInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(377, 148);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(70, 13);
            this.label158.TabIndex = 62;
            this.label158.Text = "Auto Injector:";
            // 
            // pnlNavWaterSolenoidControl
            // 
            this.pnlNavWaterSolenoidControl.Controls.Add(this.rdNavWaterSolenoidControlNo);
            this.pnlNavWaterSolenoidControl.Controls.Add(this.rdNavWaterSolenoidControlYes);
            this.pnlNavWaterSolenoidControl.Location = new System.Drawing.Point(1024, 24);
            this.pnlNavWaterSolenoidControl.Name = "pnlNavWaterSolenoidControl";
            this.pnlNavWaterSolenoidControl.Size = new System.Drawing.Size(131, 31);
            this.pnlNavWaterSolenoidControl.TabIndex = 61;
            // 
            // rdNavWaterSolenoidControlNo
            // 
            this.rdNavWaterSolenoidControlNo.AutoSize = true;
            this.rdNavWaterSolenoidControlNo.Location = new System.Drawing.Point(84, 7);
            this.rdNavWaterSolenoidControlNo.Name = "rdNavWaterSolenoidControlNo";
            this.rdNavWaterSolenoidControlNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavWaterSolenoidControlNo.TabIndex = 1;
            this.rdNavWaterSolenoidControlNo.TabStop = true;
            this.rdNavWaterSolenoidControlNo.Tag = "Navigator";
            this.rdNavWaterSolenoidControlNo.Text = "NO";
            this.rdNavWaterSolenoidControlNo.UseVisualStyleBackColor = true;
            this.rdNavWaterSolenoidControlNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavWaterSolenoidControlNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavWaterSolenoidControlYes
            // 
            this.rdNavWaterSolenoidControlYes.AutoSize = true;
            this.rdNavWaterSolenoidControlYes.Location = new System.Drawing.Point(17, 7);
            this.rdNavWaterSolenoidControlYes.Name = "rdNavWaterSolenoidControlYes";
            this.rdNavWaterSolenoidControlYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavWaterSolenoidControlYes.TabIndex = 0;
            this.rdNavWaterSolenoidControlYes.TabStop = true;
            this.rdNavWaterSolenoidControlYes.Tag = "Navigator";
            this.rdNavWaterSolenoidControlYes.Text = "YES";
            this.rdNavWaterSolenoidControlYes.UseVisualStyleBackColor = true;
            this.rdNavWaterSolenoidControlYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(900, 32);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(119, 13);
            this.label148.TabIndex = 60;
            this.label148.Text = "Water Solenoid Control:";
            // 
            // ddlNavFeed
            // 
            this.ddlNavFeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlNavFeed.FormattingEnabled = true;
            this.ddlNavFeed.Location = new System.Drawing.Point(727, 223);
            this.ddlNavFeed.Name = "ddlNavFeed";
            this.ddlNavFeed.Size = new System.Drawing.Size(140, 21);
            this.ddlNavFeed.TabIndex = 59;
            this.ddlNavFeed.Tag = "Navigator";
            this.ddlNavFeed.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(685, 225);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(34, 13);
            this.label139.TabIndex = 58;
            this.label139.Text = "Feed:";
            // 
            // ddlNavCarrySteel
            // 
            this.ddlNavCarrySteel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlNavCarrySteel.FormattingEnabled = true;
            this.ddlNavCarrySteel.Location = new System.Drawing.Point(727, 193);
            this.ddlNavCarrySteel.Name = "ddlNavCarrySteel";
            this.ddlNavCarrySteel.Size = new System.Drawing.Size(140, 21);
            this.ddlNavCarrySteel.TabIndex = 57;
            this.ddlNavCarrySteel.Tag = "Navigator";
            this.ddlNavCarrySteel.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(657, 195);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(61, 13);
            this.label133.TabIndex = 56;
            this.label133.Text = "Carry Steel:";
            // 
            // pnlNavEV
            // 
            this.pnlNavEV.Controls.Add(this.rdNavEVNo);
            this.pnlNavEV.Controls.Add(this.rdNavEVYes);
            this.pnlNavEV.Location = new System.Drawing.Point(455, 177);
            this.pnlNavEV.Name = "pnlNavEV";
            this.pnlNavEV.Size = new System.Drawing.Size(139, 31);
            this.pnlNavEV.TabIndex = 55;
            // 
            // rdNavEVNo
            // 
            this.rdNavEVNo.AutoSize = true;
            this.rdNavEVNo.Location = new System.Drawing.Point(86, 7);
            this.rdNavEVNo.Name = "rdNavEVNo";
            this.rdNavEVNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavEVNo.TabIndex = 1;
            this.rdNavEVNo.TabStop = true;
            this.rdNavEVNo.Tag = "Navigator";
            this.rdNavEVNo.Text = "NO";
            this.rdNavEVNo.UseVisualStyleBackColor = true;
            this.rdNavEVNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavEVNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavEVYes
            // 
            this.rdNavEVYes.AutoSize = true;
            this.rdNavEVYes.Location = new System.Drawing.Point(20, 7);
            this.rdNavEVYes.Name = "rdNavEVYes";
            this.rdNavEVYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavEVYes.TabIndex = 0;
            this.rdNavEVYes.TabStop = true;
            this.rdNavEVYes.Tag = "Navigator";
            this.rdNavEVYes.Text = "YES";
            this.rdNavEVYes.UseVisualStyleBackColor = true;
            this.rdNavEVYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label132
            // 
            this.label132.Location = new System.Drawing.Point(364, 177);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(85, 34);
            this.label132.TabIndex = 54;
            this.label132.Text = "EV, VPD, Third Party Control:";
            // 
            // txtNavTip3
            // 
            this.txtNavTip3.Location = new System.Drawing.Point(727, 160);
            this.txtNavTip3.Name = "txtNavTip3";
            this.txtNavTip3.Size = new System.Drawing.Size(140, 20);
            this.txtNavTip3.TabIndex = 53;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(685, 163);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(34, 13);
            this.label124.TabIndex = 52;
            this.label124.Text = "Tip 3:";
            // 
            // txtNavTip2
            // 
            this.txtNavTip2.Location = new System.Drawing.Point(727, 135);
            this.txtNavTip2.Name = "txtNavTip2";
            this.txtNavTip2.Size = new System.Drawing.Size(140, 20);
            this.txtNavTip2.TabIndex = 51;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(685, 137);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(34, 13);
            this.label125.TabIndex = 50;
            this.label125.Text = "Tip 2:";
            // 
            // txtNavTip1
            // 
            this.txtNavTip1.Location = new System.Drawing.Point(727, 110);
            this.txtNavTip1.Name = "txtNavTip1";
            this.txtNavTip1.Size = new System.Drawing.Size(140, 20);
            this.txtNavTip1.TabIndex = 49;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(685, 112);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(34, 13);
            this.label126.TabIndex = 48;
            this.label126.Text = "Tip 1:";
            // 
            // txtNavNumberOfRows
            // 
            this.txtNavNumberOfRows.Location = new System.Drawing.Point(173, 93);
            this.txtNavNumberOfRows.Mask = "G";
            this.txtNavNumberOfRows.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtNavNumberOfRows.Name = "txtNavNumberOfRows";
            this.txtNavNumberOfRows.Size = new System.Drawing.Size(146, 20);
            this.txtNavNumberOfRows.TabIndex = 29;
            this.txtNavNumberOfRows.TabStop = false;
            this.txtNavNumberOfRows.Tag = "SingleRail";
            this.txtNavNumberOfRows.Text = "0";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(76, 97);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(91, 13);
            this.label111.TabIndex = 28;
            this.label111.Text = "Number Of Rows:";
            // 
            // ddlNavControllerType
            // 
            this.ddlNavControllerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlNavControllerType.FormattingEnabled = true;
            this.ddlNavControllerType.Location = new System.Drawing.Point(173, 122);
            this.ddlNavControllerType.Name = "ddlNavControllerType";
            this.ddlNavControllerType.Size = new System.Drawing.Size(147, 21);
            this.ddlNavControllerType.TabIndex = 27;
            this.ddlNavControllerType.Tag = "Navigator";
            this.ddlNavControllerType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(83, 128);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(81, 13);
            this.label94.TabIndex = 26;
            this.label94.Text = "Controller Type:";
            // 
            // pnlNavWaterFeedSystem
            // 
            this.pnlNavWaterFeedSystem.Controls.Add(this.radioButton2);
            this.pnlNavWaterFeedSystem.Controls.Add(this.rdNavWaterFeedSystemNo);
            this.pnlNavWaterFeedSystem.Controls.Add(this.rdNavWaterFeedSystemYes);
            this.pnlNavWaterFeedSystem.Location = new System.Drawing.Point(1024, 110);
            this.pnlNavWaterFeedSystem.Name = "pnlNavWaterFeedSystem";
            this.pnlNavWaterFeedSystem.Size = new System.Drawing.Size(131, 31);
            this.pnlNavWaterFeedSystem.TabIndex = 0;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(-290, 41);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(41, 17);
            this.radioButton2.TabIndex = 0;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "Echo";
            this.radioButton2.Text = "NO";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // rdNavWaterFeedSystemNo
            // 
            this.rdNavWaterFeedSystemNo.AutoSize = true;
            this.rdNavWaterFeedSystemNo.Location = new System.Drawing.Point(84, 7);
            this.rdNavWaterFeedSystemNo.Name = "rdNavWaterFeedSystemNo";
            this.rdNavWaterFeedSystemNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavWaterFeedSystemNo.TabIndex = 1;
            this.rdNavWaterFeedSystemNo.TabStop = true;
            this.rdNavWaterFeedSystemNo.Tag = "Navigator";
            this.rdNavWaterFeedSystemNo.Text = "NO";
            this.rdNavWaterFeedSystemNo.UseVisualStyleBackColor = true;
            this.rdNavWaterFeedSystemNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavWaterFeedSystemNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavWaterFeedSystemYes
            // 
            this.rdNavWaterFeedSystemYes.AutoSize = true;
            this.rdNavWaterFeedSystemYes.Location = new System.Drawing.Point(20, 7);
            this.rdNavWaterFeedSystemYes.Name = "rdNavWaterFeedSystemYes";
            this.rdNavWaterFeedSystemYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavWaterFeedSystemYes.TabIndex = 0;
            this.rdNavWaterFeedSystemYes.TabStop = true;
            this.rdNavWaterFeedSystemYes.Tag = "Navigator";
            this.rdNavWaterFeedSystemYes.Text = "YES";
            this.rdNavWaterFeedSystemYes.UseVisualStyleBackColor = true;
            this.rdNavWaterFeedSystemYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label91
            // 
            this.label91.Location = new System.Drawing.Point(926, 111);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(90, 29);
            this.label91.TabIndex = 25;
            this.label91.Text = "Center Water Feed System:";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlNavGallonTank
            // 
            this.pnlNavGallonTank.Controls.Add(this.rdNavGallonTankNo);
            this.pnlNavGallonTank.Controls.Add(this.rdNavGallonTankYes);
            this.pnlNavGallonTank.Location = new System.Drawing.Point(1024, 63);
            this.pnlNavGallonTank.Name = "pnlNavGallonTank";
            this.pnlNavGallonTank.Size = new System.Drawing.Size(131, 31);
            this.pnlNavGallonTank.TabIndex = 24;
            // 
            // rdNavGallonTankNo
            // 
            this.rdNavGallonTankNo.AutoSize = true;
            this.rdNavGallonTankNo.Location = new System.Drawing.Point(84, 7);
            this.rdNavGallonTankNo.Name = "rdNavGallonTankNo";
            this.rdNavGallonTankNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavGallonTankNo.TabIndex = 1;
            this.rdNavGallonTankNo.TabStop = true;
            this.rdNavGallonTankNo.Tag = "Navigator";
            this.rdNavGallonTankNo.Text = "NO";
            this.rdNavGallonTankNo.UseVisualStyleBackColor = true;
            this.rdNavGallonTankNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavGallonTankNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavGallonTankYes
            // 
            this.rdNavGallonTankYes.AutoSize = true;
            this.rdNavGallonTankYes.Location = new System.Drawing.Point(20, 7);
            this.rdNavGallonTankYes.Name = "rdNavGallonTankYes";
            this.rdNavGallonTankYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavGallonTankYes.TabIndex = 0;
            this.rdNavGallonTankYes.TabStop = true;
            this.rdNavGallonTankYes.Tag = "Navigator";
            this.rdNavGallonTankYes.Text = "YES";
            this.rdNavGallonTankYes.UseVisualStyleBackColor = true;
            this.rdNavGallonTankYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label93
            // 
            this.label93.Location = new System.Drawing.Point(928, 61);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(90, 36);
            this.label93.TabIndex = 23;
            this.label93.Text = "25 Gallon Reservoir Tank:";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlNavSweep90
            // 
            this.pnlNavSweep90.Controls.Add(this.radioButton12);
            this.pnlNavSweep90.Controls.Add(this.rdNavSweep90No);
            this.pnlNavSweep90.Controls.Add(this.rdNavSweep90Yes);
            this.pnlNavSweep90.Location = new System.Drawing.Point(1024, 199);
            this.pnlNavSweep90.Name = "pnlNavSweep90";
            this.pnlNavSweep90.Size = new System.Drawing.Size(131, 31);
            this.pnlNavSweep90.TabIndex = 22;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Location = new System.Drawing.Point(-290, 41);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(41, 17);
            this.radioButton12.TabIndex = 0;
            this.radioButton12.TabStop = true;
            this.radioButton12.Tag = "Echo";
            this.radioButton12.Text = "NO";
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // rdNavSweep90No
            // 
            this.rdNavSweep90No.AutoSize = true;
            this.rdNavSweep90No.Location = new System.Drawing.Point(84, 7);
            this.rdNavSweep90No.Name = "rdNavSweep90No";
            this.rdNavSweep90No.Size = new System.Drawing.Size(41, 17);
            this.rdNavSweep90No.TabIndex = 1;
            this.rdNavSweep90No.TabStop = true;
            this.rdNavSweep90No.Tag = "Navigator";
            this.rdNavSweep90No.Text = "NO";
            this.rdNavSweep90No.UseVisualStyleBackColor = true;
            this.rdNavSweep90No.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavSweep90No.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavSweep90Yes
            // 
            this.rdNavSweep90Yes.AutoSize = true;
            this.rdNavSweep90Yes.Location = new System.Drawing.Point(20, 7);
            this.rdNavSweep90Yes.Name = "rdNavSweep90Yes";
            this.rdNavSweep90Yes.Size = new System.Drawing.Size(46, 17);
            this.rdNavSweep90Yes.TabIndex = 0;
            this.rdNavSweep90Yes.TabStop = true;
            this.rdNavSweep90Yes.Tag = "Navigator";
            this.rdNavSweep90Yes.Text = "YES";
            this.rdNavSweep90Yes.UseVisualStyleBackColor = true;
            this.rdNavSweep90Yes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(963, 208);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(58, 13);
            this.label95.TabIndex = 20;
            this.label95.Text = "Sweep 90:";
            // 
            // pnlNav1_1_4thHoseUpgrade
            // 
            this.pnlNav1_1_4thHoseUpgrade.Controls.Add(this.rdNav1_1_4thHoseUpgradeNo);
            this.pnlNav1_1_4thHoseUpgrade.Controls.Add(this.rdNav1_1_4thHoseUpgradeYes);
            this.pnlNav1_1_4thHoseUpgrade.Location = new System.Drawing.Point(728, 26);
            this.pnlNav1_1_4thHoseUpgrade.Name = "pnlNav1_1_4thHoseUpgrade";
            this.pnlNav1_1_4thHoseUpgrade.Size = new System.Drawing.Size(139, 31);
            this.pnlNav1_1_4thHoseUpgrade.TabIndex = 19;
            // 
            // rdNav1_1_4thHoseUpgradeNo
            // 
            this.rdNav1_1_4thHoseUpgradeNo.AutoSize = true;
            this.rdNav1_1_4thHoseUpgradeNo.Location = new System.Drawing.Point(86, 7);
            this.rdNav1_1_4thHoseUpgradeNo.Name = "rdNav1_1_4thHoseUpgradeNo";
            this.rdNav1_1_4thHoseUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdNav1_1_4thHoseUpgradeNo.TabIndex = 1;
            this.rdNav1_1_4thHoseUpgradeNo.TabStop = true;
            this.rdNav1_1_4thHoseUpgradeNo.Tag = "Navigator";
            this.rdNav1_1_4thHoseUpgradeNo.Text = "NO";
            this.rdNav1_1_4thHoseUpgradeNo.UseVisualStyleBackColor = true;
            this.rdNav1_1_4thHoseUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNav1_1_4thHoseUpgradeNo.Click += new System.EventHandler(this.HoseUpgradeNo_Click);
            this.rdNav1_1_4thHoseUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNav1_1_4thHoseUpgradeYes
            // 
            this.rdNav1_1_4thHoseUpgradeYes.AutoSize = true;
            this.rdNav1_1_4thHoseUpgradeYes.Location = new System.Drawing.Point(20, 7);
            this.rdNav1_1_4thHoseUpgradeYes.Name = "rdNav1_1_4thHoseUpgradeYes";
            this.rdNav1_1_4thHoseUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdNav1_1_4thHoseUpgradeYes.TabIndex = 0;
            this.rdNav1_1_4thHoseUpgradeYes.TabStop = true;
            this.rdNav1_1_4thHoseUpgradeYes.Tag = "Navigator";
            this.rdNav1_1_4thHoseUpgradeYes.Text = "YES";
            this.rdNav1_1_4thHoseUpgradeYes.UseVisualStyleBackColor = true;
            this.rdNav1_1_4thHoseUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNav1_1_4thHoseUpgradeYes.Click += new System.EventHandler(this.HoseUpgradeYes_Click);
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(611, 35);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(113, 13);
            this.label97.TabIndex = 18;
            this.label97.Text = "1 1/4\" Hose Upgrade:";
            // 
            // txtNavHoseLoopHeight
            // 
            this.txtNavHoseLoopHeight.Location = new System.Drawing.Point(173, 177);
            this.txtNavHoseLoopHeight.Mask = "F2";
            this.txtNavHoseLoopHeight.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtNavHoseLoopHeight.Name = "txtNavHoseLoopHeight";
            this.txtNavHoseLoopHeight.Size = new System.Drawing.Size(148, 20);
            this.txtNavHoseLoopHeight.TabIndex = 9;
            this.txtNavHoseLoopHeight.TabStop = false;
            this.txtNavHoseLoopHeight.Tag = "Navigator";
            this.txtNavHoseLoopHeight.Text = "0.00";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(52, 180);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(116, 13);
            this.label98.TabIndex = 8;
            this.label98.Text = "Hose Loop Height (IN):";
            // 
            // pnlNavWirelessWalkSwitch
            // 
            this.pnlNavWirelessWalkSwitch.Controls.Add(this.rdNavWirelessWalkSwitchNo);
            this.pnlNavWirelessWalkSwitch.Controls.Add(this.rdNavWirelessWalkSwitchYes);
            this.pnlNavWirelessWalkSwitch.Location = new System.Drawing.Point(1024, 154);
            this.pnlNavWirelessWalkSwitch.Name = "pnlNavWirelessWalkSwitch";
            this.pnlNavWirelessWalkSwitch.Size = new System.Drawing.Size(131, 31);
            this.pnlNavWirelessWalkSwitch.TabIndex = 17;
            // 
            // rdNavWirelessWalkSwitchNo
            // 
            this.rdNavWirelessWalkSwitchNo.AutoSize = true;
            this.rdNavWirelessWalkSwitchNo.Location = new System.Drawing.Point(84, 7);
            this.rdNavWirelessWalkSwitchNo.Name = "rdNavWirelessWalkSwitchNo";
            this.rdNavWirelessWalkSwitchNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavWirelessWalkSwitchNo.TabIndex = 1;
            this.rdNavWirelessWalkSwitchNo.TabStop = true;
            this.rdNavWirelessWalkSwitchNo.Tag = "Navigator";
            this.rdNavWirelessWalkSwitchNo.Text = "NO";
            this.rdNavWirelessWalkSwitchNo.UseVisualStyleBackColor = true;
            this.rdNavWirelessWalkSwitchNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavWirelessWalkSwitchNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavWirelessWalkSwitchYes
            // 
            this.rdNavWirelessWalkSwitchYes.AutoSize = true;
            this.rdNavWirelessWalkSwitchYes.Location = new System.Drawing.Point(20, 7);
            this.rdNavWirelessWalkSwitchYes.Name = "rdNavWirelessWalkSwitchYes";
            this.rdNavWirelessWalkSwitchYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavWirelessWalkSwitchYes.TabIndex = 0;
            this.rdNavWirelessWalkSwitchYes.TabStop = true;
            this.rdNavWirelessWalkSwitchYes.Tag = "Navigator";
            this.rdNavWirelessWalkSwitchYes.Text = "YES";
            this.rdNavWirelessWalkSwitchYes.UseVisualStyleBackColor = true;
            this.rdNavWirelessWalkSwitchYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(908, 163);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(113, 13);
            this.label99.TabIndex = 16;
            this.label99.Text = "Wireless Walk Switch:";
            // 
            // pnlNavTripleTurretBody
            // 
            this.pnlNavTripleTurretBody.Controls.Add(this.rdNavTripleTurretBodyNo);
            this.pnlNavTripleTurretBody.Controls.Add(this.rdNavTripleTurretBodyYes);
            this.pnlNavTripleTurretBody.Location = new System.Drawing.Point(727, 63);
            this.pnlNavTripleTurretBody.Name = "pnlNavTripleTurretBody";
            this.pnlNavTripleTurretBody.Size = new System.Drawing.Size(139, 31);
            this.pnlNavTripleTurretBody.TabIndex = 7;
            // 
            // rdNavTripleTurretBodyNo
            // 
            this.rdNavTripleTurretBodyNo.AutoSize = true;
            this.rdNavTripleTurretBodyNo.Location = new System.Drawing.Point(87, 7);
            this.rdNavTripleTurretBodyNo.Name = "rdNavTripleTurretBodyNo";
            this.rdNavTripleTurretBodyNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavTripleTurretBodyNo.TabIndex = 1;
            this.rdNavTripleTurretBodyNo.TabStop = true;
            this.rdNavTripleTurretBodyNo.Tag = "Navigator";
            this.rdNavTripleTurretBodyNo.Text = "NO";
            this.rdNavTripleTurretBodyNo.UseVisualStyleBackColor = true;
            this.rdNavTripleTurretBodyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavTripleTurretBodyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavTripleTurretBodyYes
            // 
            this.rdNavTripleTurretBodyYes.AutoSize = true;
            this.rdNavTripleTurretBodyYes.Location = new System.Drawing.Point(20, 7);
            this.rdNavTripleTurretBodyYes.Name = "rdNavTripleTurretBodyYes";
            this.rdNavTripleTurretBodyYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavTripleTurretBodyYes.TabIndex = 0;
            this.rdNavTripleTurretBodyYes.TabStop = true;
            this.rdNavTripleTurretBodyYes.Tag = "Navigator";
            this.rdNavTripleTurretBodyYes.Text = "YES";
            this.rdNavTripleTurretBodyYes.UseVisualStyleBackColor = true;
            this.rdNavTripleTurretBodyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(623, 70);
            this.label102.MaximumSize = new System.Drawing.Size(150, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(94, 13);
            this.label102.TabIndex = 6;
            this.label102.Text = "Triple Turret Body:";
            // 
            // txtNavQuantityOfSystems
            // 
            this.txtNavQuantityOfSystems.Location = new System.Drawing.Point(173, 32);
            this.txtNavQuantityOfSystems.Mask = "G";
            this.txtNavQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtNavQuantityOfSystems.Name = "txtNavQuantityOfSystems";
            this.txtNavQuantityOfSystems.Size = new System.Drawing.Size(147, 20);
            this.txtNavQuantityOfSystems.TabIndex = 1;
            this.txtNavQuantityOfSystems.TabStop = false;
            this.txtNavQuantityOfSystems.Tag = "Navigator";
            this.txtNavQuantityOfSystems.Text = "0";
            this.txtNavQuantityOfSystems.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // pnlNavMountedInjector
            // 
            this.pnlNavMountedInjector.Controls.Add(this.rdNavMountedInjectorNo);
            this.pnlNavMountedInjector.Controls.Add(this.rdNavMountedInjectorYes);
            this.pnlNavMountedInjector.Location = new System.Drawing.Point(455, 63);
            this.pnlNavMountedInjector.Name = "pnlNavMountedInjector";
            this.pnlNavMountedInjector.Size = new System.Drawing.Size(139, 31);
            this.pnlNavMountedInjector.TabIndex = 13;
            // 
            // rdNavMountedInjectorNo
            // 
            this.rdNavMountedInjectorNo.AutoSize = true;
            this.rdNavMountedInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdNavMountedInjectorNo.Name = "rdNavMountedInjectorNo";
            this.rdNavMountedInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavMountedInjectorNo.TabIndex = 1;
            this.rdNavMountedInjectorNo.TabStop = true;
            this.rdNavMountedInjectorNo.Tag = "Navigator";
            this.rdNavMountedInjectorNo.Text = "NO";
            this.rdNavMountedInjectorNo.UseVisualStyleBackColor = true;
            this.rdNavMountedInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavMountedInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavMountedInjectorYes
            // 
            this.rdNavMountedInjectorYes.AutoSize = true;
            this.rdNavMountedInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdNavMountedInjectorYes.Name = "rdNavMountedInjectorYes";
            this.rdNavMountedInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavMountedInjectorYes.TabIndex = 0;
            this.rdNavMountedInjectorYes.TabStop = true;
            this.rdNavMountedInjectorYes.Tag = "Navigator";
            this.rdNavMountedInjectorYes.Text = "YES";
            this.rdNavMountedInjectorYes.UseVisualStyleBackColor = true;
            this.rdNavMountedInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(359, 67);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(90, 13);
            this.label103.TabIndex = 12;
            this.label103.Text = "Mounted Injector:";
            // 
            // pnlNavPressureRegulator
            // 
            this.pnlNavPressureRegulator.Controls.Add(this.radioButton28);
            this.pnlNavPressureRegulator.Controls.Add(this.rdNavPressureRegulatorNo);
            this.pnlNavPressureRegulator.Controls.Add(this.rdNavPressureRegulatorYes);
            this.pnlNavPressureRegulator.Location = new System.Drawing.Point(455, 26);
            this.pnlNavPressureRegulator.Name = "pnlNavPressureRegulator";
            this.pnlNavPressureRegulator.Size = new System.Drawing.Size(139, 31);
            this.pnlNavPressureRegulator.TabIndex = 11;
            // 
            // radioButton28
            // 
            this.radioButton28.AutoSize = true;
            this.radioButton28.Location = new System.Drawing.Point(-290, 41);
            this.radioButton28.Name = "radioButton28";
            this.radioButton28.Size = new System.Drawing.Size(41, 17);
            this.radioButton28.TabIndex = 0;
            this.radioButton28.TabStop = true;
            this.radioButton28.Tag = "Echo";
            this.radioButton28.Text = "NO";
            this.radioButton28.UseVisualStyleBackColor = true;
            // 
            // rdNavPressureRegulatorNo
            // 
            this.rdNavPressureRegulatorNo.AutoSize = true;
            this.rdNavPressureRegulatorNo.Location = new System.Drawing.Point(87, 7);
            this.rdNavPressureRegulatorNo.Name = "rdNavPressureRegulatorNo";
            this.rdNavPressureRegulatorNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavPressureRegulatorNo.TabIndex = 1;
            this.rdNavPressureRegulatorNo.TabStop = true;
            this.rdNavPressureRegulatorNo.Tag = "Navigator";
            this.rdNavPressureRegulatorNo.Text = "NO";
            this.rdNavPressureRegulatorNo.UseVisualStyleBackColor = true;
            this.rdNavPressureRegulatorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavPressureRegulatorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavPressureRegulatorYes
            // 
            this.rdNavPressureRegulatorYes.AutoSize = true;
            this.rdNavPressureRegulatorYes.Location = new System.Drawing.Point(20, 7);
            this.rdNavPressureRegulatorYes.Name = "rdNavPressureRegulatorYes";
            this.rdNavPressureRegulatorYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavPressureRegulatorYes.TabIndex = 0;
            this.rdNavPressureRegulatorYes.TabStop = true;
            this.rdNavPressureRegulatorYes.Tag = "Navigator";
            this.rdNavPressureRegulatorYes.Text = "YES";
            this.rdNavPressureRegulatorYes.UseVisualStyleBackColor = true;
            this.rdNavPressureRegulatorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(346, 33);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(100, 13);
            this.label104.TabIndex = 10;
            this.label104.Text = "Pressure Regulator:";
            // 
            // txtNavSpraybodySpacing
            // 
            this.txtNavSpraybodySpacing.Location = new System.Drawing.Point(173, 151);
            this.txtNavSpraybodySpacing.Mask = "F2";
            this.txtNavSpraybodySpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtNavSpraybodySpacing.Name = "txtNavSpraybodySpacing";
            this.txtNavSpraybodySpacing.Size = new System.Drawing.Size(148, 20);
            this.txtNavSpraybodySpacing.TabIndex = 15;
            this.txtNavSpraybodySpacing.TabStop = false;
            this.txtNavSpraybodySpacing.Tag = "Navigator";
            this.txtNavSpraybodySpacing.Text = "0.00";
            this.txtNavSpraybodySpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(44, 154);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(122, 13);
            this.label105.TabIndex = 14;
            this.label105.Text = "Spraybody Spacing (IN):";
            // 
            // pnlNavTripleWaterLocklineUpgrade
            // 
            this.pnlNavTripleWaterLocklineUpgrade.Controls.Add(this.rdNavTripleWaterLocklineUpgradeNo);
            this.pnlNavTripleWaterLocklineUpgrade.Controls.Add(this.rdNavTripleWaterLocklineUpgradeYes);
            this.pnlNavTripleWaterLocklineUpgrade.Location = new System.Drawing.Point(172, 206);
            this.pnlNavTripleWaterLocklineUpgrade.Name = "pnlNavTripleWaterLocklineUpgrade";
            this.pnlNavTripleWaterLocklineUpgrade.Size = new System.Drawing.Size(147, 31);
            this.pnlNavTripleWaterLocklineUpgrade.TabIndex = 5;
            // 
            // rdNavTripleWaterLocklineUpgradeNo
            // 
            this.rdNavTripleWaterLocklineUpgradeNo.AutoSize = true;
            this.rdNavTripleWaterLocklineUpgradeNo.Location = new System.Drawing.Point(98, 7);
            this.rdNavTripleWaterLocklineUpgradeNo.Name = "rdNavTripleWaterLocklineUpgradeNo";
            this.rdNavTripleWaterLocklineUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdNavTripleWaterLocklineUpgradeNo.TabIndex = 1;
            this.rdNavTripleWaterLocklineUpgradeNo.TabStop = true;
            this.rdNavTripleWaterLocklineUpgradeNo.Tag = "Navigator";
            this.rdNavTripleWaterLocklineUpgradeNo.Text = "NO";
            this.rdNavTripleWaterLocklineUpgradeNo.UseVisualStyleBackColor = true;
            this.rdNavTripleWaterLocklineUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdNavTripleWaterLocklineUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdNavTripleWaterLocklineUpgradeYes
            // 
            this.rdNavTripleWaterLocklineUpgradeYes.AutoSize = true;
            this.rdNavTripleWaterLocklineUpgradeYes.Location = new System.Drawing.Point(26, 7);
            this.rdNavTripleWaterLocklineUpgradeYes.Name = "rdNavTripleWaterLocklineUpgradeYes";
            this.rdNavTripleWaterLocklineUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdNavTripleWaterLocklineUpgradeYes.TabIndex = 0;
            this.rdNavTripleWaterLocklineUpgradeYes.TabStop = true;
            this.rdNavTripleWaterLocklineUpgradeYes.Tag = "Navigator";
            this.rdNavTripleWaterLocklineUpgradeYes.Text = "YES";
            this.rdNavTripleWaterLocklineUpgradeYes.UseVisualStyleBackColor = true;
            this.rdNavTripleWaterLocklineUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(38, 210);
            this.label106.MaximumSize = new System.Drawing.Size(150, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(136, 26);
            this.label106.TabIndex = 4;
            this.label106.Text = "Step/Stop/Water Lockline Upgrade:";
            // 
            // ddlNavSingleDualWater
            // 
            this.ddlNavSingleDualWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlNavSingleDualWater.FormattingEnabled = true;
            this.ddlNavSingleDualWater.Location = new System.Drawing.Point(173, 63);
            this.ddlNavSingleDualWater.Name = "ddlNavSingleDualWater";
            this.ddlNavSingleDualWater.Size = new System.Drawing.Size(147, 21);
            this.ddlNavSingleDualWater.TabIndex = 3;
            this.ddlNavSingleDualWater.Tag = "Navigator";
            this.ddlNavSingleDualWater.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(73, 67);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(98, 13);
            this.label108.TabIndex = 2;
            this.label108.Text = "Single/Dual Water:";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(61, 34);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(105, 13);
            this.label109.TabIndex = 0;
            this.label109.Text = "Quantity Of Systems:";
            // 
            // tpGroundRunnerBoom
            // 
            this.tpGroundRunnerBoom.Controls.Add(this.ddlGREVWireGauge);
            this.tpGroundRunnerBoom.Controls.Add(this.label218);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRWaterFeedSystem);
            this.tpGroundRunnerBoom.Controls.Add(this.label209);
            this.tpGroundRunnerBoom.Controls.Add(this.ddlGRInjectorType);
            this.tpGroundRunnerBoom.Controls.Add(this.label162);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRAutoInjector);
            this.tpGroundRunnerBoom.Controls.Add(this.label163);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRWaterSolenoidControl);
            this.tpGroundRunnerBoom.Controls.Add(this.label164);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGREV);
            this.tpGroundRunnerBoom.Controls.Add(this.label165);
            this.tpGroundRunnerBoom.Controls.Add(this.txtGRTip3);
            this.tpGroundRunnerBoom.Controls.Add(this.label166);
            this.tpGroundRunnerBoom.Controls.Add(this.txtGRTip2);
            this.tpGroundRunnerBoom.Controls.Add(this.label167);
            this.tpGroundRunnerBoom.Controls.Add(this.txtGRTip1);
            this.tpGroundRunnerBoom.Controls.Add(this.label168);
            this.tpGroundRunnerBoom.Controls.Add(this.txtGRNumberOfRows);
            this.tpGroundRunnerBoom.Controls.Add(this.label169);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRWirelessWalkSwitch);
            this.tpGroundRunnerBoom.Controls.Add(this.label170);
            this.tpGroundRunnerBoom.Controls.Add(this.txtGRRailDropDownAssy);
            this.tpGroundRunnerBoom.Controls.Add(this.label171);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGR1_1_4thHoseUpgrade);
            this.tpGroundRunnerBoom.Controls.Add(this.label172);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRTripleTurretBody);
            this.tpGroundRunnerBoom.Controls.Add(this.label173);
            this.tpGroundRunnerBoom.Controls.Add(this.txtGRQuantityOfSystems);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRMountedInjector);
            this.tpGroundRunnerBoom.Controls.Add(this.label174);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRPressureRegulator);
            this.tpGroundRunnerBoom.Controls.Add(this.label175);
            this.tpGroundRunnerBoom.Controls.Add(this.txtGRSpraybodySpacing);
            this.tpGroundRunnerBoom.Controls.Add(this.label176);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRSweep90);
            this.tpGroundRunnerBoom.Controls.Add(this.label177);
            this.tpGroundRunnerBoom.Controls.Add(this.pnlGRTripleWaterLocklineUpgrade);
            this.tpGroundRunnerBoom.Controls.Add(this.label178);
            this.tpGroundRunnerBoom.Controls.Add(this.ddlGRControllerType);
            this.tpGroundRunnerBoom.Controls.Add(this.label179);
            this.tpGroundRunnerBoom.Controls.Add(this.txtGRHoseLoopHeight);
            this.tpGroundRunnerBoom.Controls.Add(this.label180);
            this.tpGroundRunnerBoom.Controls.Add(this.ddlGRSingleDualWater);
            this.tpGroundRunnerBoom.Controls.Add(this.label181);
            this.tpGroundRunnerBoom.Controls.Add(this.label182);
            this.tpGroundRunnerBoom.Location = new System.Drawing.Point(4, 22);
            this.tpGroundRunnerBoom.Name = "tpGroundRunnerBoom";
            this.tpGroundRunnerBoom.Padding = new System.Windows.Forms.Padding(3);
            this.tpGroundRunnerBoom.Size = new System.Drawing.Size(1368, 267);
            this.tpGroundRunnerBoom.TabIndex = 6;
            this.tpGroundRunnerBoom.Tag = "GroundRunner";
            this.tpGroundRunnerBoom.Text = "Ground Runner";
            this.tpGroundRunnerBoom.UseVisualStyleBackColor = true;
            // 
            // ddlGREVWireGauge
            // 
            this.ddlGREVWireGauge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlGREVWireGauge.FormattingEnabled = true;
            this.ddlGREVWireGauge.Location = new System.Drawing.Point(449, 220);
            this.ddlGREVWireGauge.Name = "ddlGREVWireGauge";
            this.ddlGREVWireGauge.Size = new System.Drawing.Size(150, 21);
            this.ddlGREVWireGauge.TabIndex = 89;
            this.ddlGREVWireGauge.Tag = "SingleRail";
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(354, 223);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(86, 13);
            this.label218.TabIndex = 88;
            this.label218.Text = "EV Wire/Gauge:";
            // 
            // pnlGRWaterFeedSystem
            // 
            this.pnlGRWaterFeedSystem.Controls.Add(this.radioButton3);
            this.pnlGRWaterFeedSystem.Controls.Add(this.rdGRWaterFeedSystemNo);
            this.pnlGRWaterFeedSystem.Controls.Add(this.rdGRWaterFeedSystemYes);
            this.pnlGRWaterFeedSystem.Location = new System.Drawing.Point(1054, 54);
            this.pnlGRWaterFeedSystem.Name = "pnlGRWaterFeedSystem";
            this.pnlGRWaterFeedSystem.Size = new System.Drawing.Size(139, 31);
            this.pnlGRWaterFeedSystem.TabIndex = 87;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(-290, 41);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(41, 17);
            this.radioButton3.TabIndex = 46;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "Echo";
            this.radioButton3.Text = "NO";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // rdGRWaterFeedSystemNo
            // 
            this.rdGRWaterFeedSystemNo.AutoSize = true;
            this.rdGRWaterFeedSystemNo.Location = new System.Drawing.Point(87, 7);
            this.rdGRWaterFeedSystemNo.Name = "rdGRWaterFeedSystemNo";
            this.rdGRWaterFeedSystemNo.Size = new System.Drawing.Size(41, 17);
            this.rdGRWaterFeedSystemNo.TabIndex = 1;
            this.rdGRWaterFeedSystemNo.TabStop = true;
            this.rdGRWaterFeedSystemNo.Tag = "GroundRunner";
            this.rdGRWaterFeedSystemNo.Text = "NO";
            this.rdGRWaterFeedSystemNo.UseVisualStyleBackColor = true;
            this.rdGRWaterFeedSystemNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRWaterFeedSystemNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRWaterFeedSystemYes
            // 
            this.rdGRWaterFeedSystemYes.AutoSize = true;
            this.rdGRWaterFeedSystemYes.Location = new System.Drawing.Point(20, 7);
            this.rdGRWaterFeedSystemYes.Name = "rdGRWaterFeedSystemYes";
            this.rdGRWaterFeedSystemYes.Size = new System.Drawing.Size(46, 17);
            this.rdGRWaterFeedSystemYes.TabIndex = 0;
            this.rdGRWaterFeedSystemYes.TabStop = true;
            this.rdGRWaterFeedSystemYes.Tag = "GroundRunner";
            this.rdGRWaterFeedSystemYes.Text = "YES";
            this.rdGRWaterFeedSystemYes.UseVisualStyleBackColor = true;
            this.rdGRWaterFeedSystemYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label209
            // 
            this.label209.Location = new System.Drawing.Point(973, 54);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(75, 29);
            this.label209.TabIndex = 86;
            this.label209.Text = "Center Water Feed System:";
            this.label209.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ddlGRInjectorType
            // 
            this.ddlGRInjectorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlGRInjectorType.FormattingEnabled = true;
            this.ddlGRInjectorType.Location = new System.Drawing.Point(449, 101);
            this.ddlGRInjectorType.Name = "ddlGRInjectorType";
            this.ddlGRInjectorType.Size = new System.Drawing.Size(149, 21);
            this.ddlGRInjectorType.TabIndex = 85;
            this.ddlGRInjectorType.Tag = "GroundRunner";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(367, 103);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(72, 13);
            this.label162.TabIndex = 84;
            this.label162.Text = "Injector Type:";
            // 
            // pnlGRAutoInjector
            // 
            this.pnlGRAutoInjector.Controls.Add(this.rdGRAutoInjectorNo);
            this.pnlGRAutoInjector.Controls.Add(this.rdGRAutoInjectorYes);
            this.pnlGRAutoInjector.Location = new System.Drawing.Point(450, 132);
            this.pnlGRAutoInjector.Name = "pnlGRAutoInjector";
            this.pnlGRAutoInjector.Size = new System.Drawing.Size(149, 31);
            this.pnlGRAutoInjector.TabIndex = 83;
            // 
            // rdGRAutoInjectorNo
            // 
            this.rdGRAutoInjectorNo.AutoSize = true;
            this.rdGRAutoInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdGRAutoInjectorNo.Name = "rdGRAutoInjectorNo";
            this.rdGRAutoInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdGRAutoInjectorNo.TabIndex = 1;
            this.rdGRAutoInjectorNo.TabStop = true;
            this.rdGRAutoInjectorNo.Tag = "GroundRunner";
            this.rdGRAutoInjectorNo.Text = "NO";
            this.rdGRAutoInjectorNo.UseVisualStyleBackColor = true;
            this.rdGRAutoInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRAutoInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRAutoInjectorYes
            // 
            this.rdGRAutoInjectorYes.AutoSize = true;
            this.rdGRAutoInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdGRAutoInjectorYes.Name = "rdGRAutoInjectorYes";
            this.rdGRAutoInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdGRAutoInjectorYes.TabIndex = 0;
            this.rdGRAutoInjectorYes.TabStop = true;
            this.rdGRAutoInjectorYes.Tag = "GroundRunner";
            this.rdGRAutoInjectorYes.Text = "YES";
            this.rdGRAutoInjectorYes.UseVisualStyleBackColor = true;
            this.rdGRAutoInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(370, 141);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(70, 13);
            this.label163.TabIndex = 82;
            this.label163.Text = "Auto Injector:";
            // 
            // pnlGRWaterSolenoidControl
            // 
            this.pnlGRWaterSolenoidControl.Controls.Add(this.rdGRWaterSolenoidControlNo);
            this.pnlGRWaterSolenoidControl.Controls.Add(this.rdGRWaterSolenoidControlYes);
            this.pnlGRWaterSolenoidControl.Location = new System.Drawing.Point(1054, 15);
            this.pnlGRWaterSolenoidControl.Name = "pnlGRWaterSolenoidControl";
            this.pnlGRWaterSolenoidControl.Size = new System.Drawing.Size(143, 31);
            this.pnlGRWaterSolenoidControl.TabIndex = 81;
            // 
            // rdGRWaterSolenoidControlNo
            // 
            this.rdGRWaterSolenoidControlNo.AutoSize = true;
            this.rdGRWaterSolenoidControlNo.Location = new System.Drawing.Point(80, 7);
            this.rdGRWaterSolenoidControlNo.Name = "rdGRWaterSolenoidControlNo";
            this.rdGRWaterSolenoidControlNo.Size = new System.Drawing.Size(41, 17);
            this.rdGRWaterSolenoidControlNo.TabIndex = 1;
            this.rdGRWaterSolenoidControlNo.TabStop = true;
            this.rdGRWaterSolenoidControlNo.Tag = "GroundRunner";
            this.rdGRWaterSolenoidControlNo.Text = "NO";
            this.rdGRWaterSolenoidControlNo.UseVisualStyleBackColor = true;
            this.rdGRWaterSolenoidControlNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRWaterSolenoidControlNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRWaterSolenoidControlYes
            // 
            this.rdGRWaterSolenoidControlYes.AutoSize = true;
            this.rdGRWaterSolenoidControlYes.Location = new System.Drawing.Point(19, 7);
            this.rdGRWaterSolenoidControlYes.Name = "rdGRWaterSolenoidControlYes";
            this.rdGRWaterSolenoidControlYes.Size = new System.Drawing.Size(46, 17);
            this.rdGRWaterSolenoidControlYes.TabIndex = 0;
            this.rdGRWaterSolenoidControlYes.TabStop = true;
            this.rdGRWaterSolenoidControlYes.Tag = "GroundRunner";
            this.rdGRWaterSolenoidControlYes.Text = "YES";
            this.rdGRWaterSolenoidControlYes.UseVisualStyleBackColor = true;
            this.rdGRWaterSolenoidControlYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(927, 21);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(119, 13);
            this.label164.TabIndex = 80;
            this.label164.Text = "Water Solenoid Control:";
            // 
            // pnlGREV
            // 
            this.pnlGREV.Controls.Add(this.rdGREVNo);
            this.pnlGREV.Controls.Add(this.rdGREVYes);
            this.pnlGREV.Location = new System.Drawing.Point(450, 177);
            this.pnlGREV.Name = "pnlGREV";
            this.pnlGREV.Size = new System.Drawing.Size(149, 31);
            this.pnlGREV.TabIndex = 79;
            // 
            // rdGREVNo
            // 
            this.rdGREVNo.AutoSize = true;
            this.rdGREVNo.Location = new System.Drawing.Point(87, 7);
            this.rdGREVNo.Name = "rdGREVNo";
            this.rdGREVNo.Size = new System.Drawing.Size(41, 17);
            this.rdGREVNo.TabIndex = 1;
            this.rdGREVNo.TabStop = true;
            this.rdGREVNo.Tag = "GroundRunner";
            this.rdGREVNo.Text = "NO";
            this.rdGREVNo.UseVisualStyleBackColor = true;
            this.rdGREVNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGREVNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGREVYes
            // 
            this.rdGREVYes.AutoSize = true;
            this.rdGREVYes.Location = new System.Drawing.Point(19, 7);
            this.rdGREVYes.Name = "rdGREVYes";
            this.rdGREVYes.Size = new System.Drawing.Size(46, 17);
            this.rdGREVYes.TabIndex = 0;
            this.rdGREVYes.TabStop = true;
            this.rdGREVYes.Tag = "GroundRunner";
            this.rdGREVYes.Text = "YES";
            this.rdGREVYes.UseVisualStyleBackColor = true;
            this.rdGREVYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label165
            // 
            this.label165.Location = new System.Drawing.Point(360, 175);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(84, 37);
            this.label165.TabIndex = 78;
            this.label165.Text = "EV, VPD, Third Party Control:";
            // 
            // txtGRTip3
            // 
            this.txtGRTip3.Location = new System.Drawing.Point(750, 144);
            this.txtGRTip3.Name = "txtGRTip3";
            this.txtGRTip3.Size = new System.Drawing.Size(144, 20);
            this.txtGRTip3.TabIndex = 77;
            this.txtGRTip3.Tag = "GroundRunner";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(709, 146);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(34, 13);
            this.label166.TabIndex = 76;
            this.label166.Text = "Tip 3:";
            // 
            // txtGRTip2
            // 
            this.txtGRTip2.Location = new System.Drawing.Point(750, 119);
            this.txtGRTip2.Name = "txtGRTip2";
            this.txtGRTip2.Size = new System.Drawing.Size(144, 20);
            this.txtGRTip2.TabIndex = 75;
            this.txtGRTip2.Tag = "GroundRunner";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(709, 121);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(34, 13);
            this.label167.TabIndex = 74;
            this.label167.Text = "Tip 2:";
            // 
            // txtGRTip1
            // 
            this.txtGRTip1.Location = new System.Drawing.Point(750, 93);
            this.txtGRTip1.Name = "txtGRTip1";
            this.txtGRTip1.Size = new System.Drawing.Size(144, 20);
            this.txtGRTip1.TabIndex = 73;
            this.txtGRTip1.Tag = "GroundRunner";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(709, 96);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(34, 13);
            this.label168.TabIndex = 72;
            this.label168.Text = "Tip 1:";
            // 
            // txtGRNumberOfRows
            // 
            this.txtGRNumberOfRows.Location = new System.Drawing.Point(158, 75);
            this.txtGRNumberOfRows.Mask = "G";
            this.txtGRNumberOfRows.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtGRNumberOfRows.Name = "txtGRNumberOfRows";
            this.txtGRNumberOfRows.Size = new System.Drawing.Size(158, 20);
            this.txtGRNumberOfRows.TabIndex = 71;
            this.txtGRNumberOfRows.TabStop = false;
            this.txtGRNumberOfRows.Tag = "GroundRunner";
            this.txtGRNumberOfRows.Text = "0";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(59, 78);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(91, 13);
            this.label169.TabIndex = 70;
            this.label169.Text = "Number Of Rows:";
            // 
            // pnlGRWirelessWalkSwitch
            // 
            this.pnlGRWirelessWalkSwitch.Controls.Add(this.rdGRWirelessWalkSwitchNo);
            this.pnlGRWirelessWalkSwitch.Controls.Add(this.rdGRWirelessWalkSwitchYes);
            this.pnlGRWirelessWalkSwitch.Location = new System.Drawing.Point(750, 220);
            this.pnlGRWirelessWalkSwitch.Name = "pnlGRWirelessWalkSwitch";
            this.pnlGRWirelessWalkSwitch.Size = new System.Drawing.Size(143, 31);
            this.pnlGRWirelessWalkSwitch.TabIndex = 69;
            // 
            // rdGRWirelessWalkSwitchNo
            // 
            this.rdGRWirelessWalkSwitchNo.AutoSize = true;
            this.rdGRWirelessWalkSwitchNo.Location = new System.Drawing.Point(80, 7);
            this.rdGRWirelessWalkSwitchNo.Name = "rdGRWirelessWalkSwitchNo";
            this.rdGRWirelessWalkSwitchNo.Size = new System.Drawing.Size(41, 17);
            this.rdGRWirelessWalkSwitchNo.TabIndex = 1;
            this.rdGRWirelessWalkSwitchNo.TabStop = true;
            this.rdGRWirelessWalkSwitchNo.Tag = "GroundRunner";
            this.rdGRWirelessWalkSwitchNo.Text = "NO";
            this.rdGRWirelessWalkSwitchNo.UseVisualStyleBackColor = true;
            this.rdGRWirelessWalkSwitchNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRWirelessWalkSwitchNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRWirelessWalkSwitchYes
            // 
            this.rdGRWirelessWalkSwitchYes.AutoSize = true;
            this.rdGRWirelessWalkSwitchYes.Location = new System.Drawing.Point(20, 7);
            this.rdGRWirelessWalkSwitchYes.Name = "rdGRWirelessWalkSwitchYes";
            this.rdGRWirelessWalkSwitchYes.Size = new System.Drawing.Size(46, 17);
            this.rdGRWirelessWalkSwitchYes.TabIndex = 0;
            this.rdGRWirelessWalkSwitchYes.TabStop = true;
            this.rdGRWirelessWalkSwitchYes.Tag = "GroundRunner";
            this.rdGRWirelessWalkSwitchYes.Text = "YES";
            this.rdGRWirelessWalkSwitchYes.UseVisualStyleBackColor = true;
            this.rdGRWirelessWalkSwitchYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label170
            // 
            this.label170.Location = new System.Drawing.Point(646, 219);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(96, 28);
            this.label170.TabIndex = 68;
            this.label170.Text = "Wireless Walk Switch:";
            this.label170.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtGRRailDropDownAssy
            // 
            this.txtGRRailDropDownAssy.Location = new System.Drawing.Point(157, 231);
            this.txtGRRailDropDownAssy.Mask = "F2";
            this.txtGRRailDropDownAssy.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtGRRailDropDownAssy.Name = "txtGRRailDropDownAssy";
            this.txtGRRailDropDownAssy.Size = new System.Drawing.Size(160, 20);
            this.txtGRRailDropDownAssy.TabIndex = 63;
            this.txtGRRailDropDownAssy.TabStop = false;
            this.txtGRRailDropDownAssy.Tag = "GroundRunner";
            this.txtGRRailDropDownAssy.Text = "0.00";
            // 
            // label171
            // 
            this.label171.Location = new System.Drawing.Point(17, 234);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(132, 16);
            this.label171.TabIndex = 62;
            this.label171.Text = "Rail Drop Down Assy. (IN):";
            // 
            // pnlGR1_1_4thHoseUpgrade
            // 
            this.pnlGR1_1_4thHoseUpgrade.Controls.Add(this.rdGR1_1_4thHoseUpgradeNo);
            this.pnlGR1_1_4thHoseUpgrade.Controls.Add(this.rdGR1_1_4thHoseUpgradeYes);
            this.pnlGR1_1_4thHoseUpgrade.Location = new System.Drawing.Point(750, 8);
            this.pnlGR1_1_4thHoseUpgrade.Name = "pnlGR1_1_4thHoseUpgrade";
            this.pnlGR1_1_4thHoseUpgrade.Size = new System.Drawing.Size(143, 31);
            this.pnlGR1_1_4thHoseUpgrade.TabIndex = 65;
            // 
            // rdGR1_1_4thHoseUpgradeNo
            // 
            this.rdGR1_1_4thHoseUpgradeNo.AutoSize = true;
            this.rdGR1_1_4thHoseUpgradeNo.Location = new System.Drawing.Point(87, 7);
            this.rdGR1_1_4thHoseUpgradeNo.Name = "rdGR1_1_4thHoseUpgradeNo";
            this.rdGR1_1_4thHoseUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdGR1_1_4thHoseUpgradeNo.TabIndex = 1;
            this.rdGR1_1_4thHoseUpgradeNo.TabStop = true;
            this.rdGR1_1_4thHoseUpgradeNo.Tag = "GroundRunner";
            this.rdGR1_1_4thHoseUpgradeNo.Text = "NO";
            this.rdGR1_1_4thHoseUpgradeNo.UseVisualStyleBackColor = true;
            this.rdGR1_1_4thHoseUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGR1_1_4thHoseUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGR1_1_4thHoseUpgradeYes
            // 
            this.rdGR1_1_4thHoseUpgradeYes.AutoSize = true;
            this.rdGR1_1_4thHoseUpgradeYes.Location = new System.Drawing.Point(20, 7);
            this.rdGR1_1_4thHoseUpgradeYes.Name = "rdGR1_1_4thHoseUpgradeYes";
            this.rdGR1_1_4thHoseUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdGR1_1_4thHoseUpgradeYes.TabIndex = 0;
            this.rdGR1_1_4thHoseUpgradeYes.TabStop = true;
            this.rdGR1_1_4thHoseUpgradeYes.Tag = "GroundRunner";
            this.rdGR1_1_4thHoseUpgradeYes.Text = "YES";
            this.rdGR1_1_4thHoseUpgradeYes.UseVisualStyleBackColor = true;
            this.rdGR1_1_4thHoseUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label172
            // 
            this.label172.Location = new System.Drawing.Point(646, 11);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(94, 28);
            this.label172.TabIndex = 64;
            this.label172.Text = "1 1/4\" Hose Upgrade:";
            this.label172.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlGRTripleTurretBody
            // 
            this.pnlGRTripleTurretBody.Controls.Add(this.rdGRTripleTurretBodyNo);
            this.pnlGRTripleTurretBody.Controls.Add(this.rdGRTripleTurretBodyYes);
            this.pnlGRTripleTurretBody.Location = new System.Drawing.Point(750, 49);
            this.pnlGRTripleTurretBody.Name = "pnlGRTripleTurretBody";
            this.pnlGRTripleTurretBody.Size = new System.Drawing.Size(143, 31);
            this.pnlGRTripleTurretBody.TabIndex = 53;
            // 
            // rdGRTripleTurretBodyNo
            // 
            this.rdGRTripleTurretBodyNo.AutoSize = true;
            this.rdGRTripleTurretBodyNo.Location = new System.Drawing.Point(87, 7);
            this.rdGRTripleTurretBodyNo.Name = "rdGRTripleTurretBodyNo";
            this.rdGRTripleTurretBodyNo.Size = new System.Drawing.Size(41, 17);
            this.rdGRTripleTurretBodyNo.TabIndex = 1;
            this.rdGRTripleTurretBodyNo.TabStop = true;
            this.rdGRTripleTurretBodyNo.Tag = "GroundRunner";
            this.rdGRTripleTurretBodyNo.Text = "NO";
            this.rdGRTripleTurretBodyNo.UseVisualStyleBackColor = true;
            this.rdGRTripleTurretBodyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRTripleTurretBodyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRTripleTurretBodyYes
            // 
            this.rdGRTripleTurretBodyYes.AutoSize = true;
            this.rdGRTripleTurretBodyYes.Location = new System.Drawing.Point(20, 7);
            this.rdGRTripleTurretBodyYes.Name = "rdGRTripleTurretBodyYes";
            this.rdGRTripleTurretBodyYes.Size = new System.Drawing.Size(46, 17);
            this.rdGRTripleTurretBodyYes.TabIndex = 0;
            this.rdGRTripleTurretBodyYes.TabStop = true;
            this.rdGRTripleTurretBodyYes.Tag = "GroundRunner";
            this.rdGRTripleTurretBodyYes.Text = "YES";
            this.rdGRTripleTurretBodyYes.UseVisualStyleBackColor = true;
            this.rdGRTripleTurretBodyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(650, 58);
            this.label173.MaximumSize = new System.Drawing.Size(150, 0);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(94, 13);
            this.label173.TabIndex = 52;
            this.label173.Text = "Triple Turret Body:";
            // 
            // txtGRQuantityOfSystems
            // 
            this.txtGRQuantityOfSystems.Location = new System.Drawing.Point(156, 14);
            this.txtGRQuantityOfSystems.Mask = "G";
            this.txtGRQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtGRQuantityOfSystems.Name = "txtGRQuantityOfSystems";
            this.txtGRQuantityOfSystems.Size = new System.Drawing.Size(160, 20);
            this.txtGRQuantityOfSystems.TabIndex = 45;
            this.txtGRQuantityOfSystems.TabStop = false;
            this.txtGRQuantityOfSystems.Tag = "GroundRunner";
            this.txtGRQuantityOfSystems.Text = "0";
            this.txtGRQuantityOfSystems.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // pnlGRMountedInjector
            // 
            this.pnlGRMountedInjector.Controls.Add(this.rdGRMountedInjectorNo);
            this.pnlGRMountedInjector.Controls.Add(this.rdGRMountedInjectorYes);
            this.pnlGRMountedInjector.Location = new System.Drawing.Point(450, 54);
            this.pnlGRMountedInjector.Name = "pnlGRMountedInjector";
            this.pnlGRMountedInjector.Size = new System.Drawing.Size(149, 31);
            this.pnlGRMountedInjector.TabIndex = 61;
            // 
            // rdGRMountedInjectorNo
            // 
            this.rdGRMountedInjectorNo.AutoSize = true;
            this.rdGRMountedInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdGRMountedInjectorNo.Name = "rdGRMountedInjectorNo";
            this.rdGRMountedInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdGRMountedInjectorNo.TabIndex = 1;
            this.rdGRMountedInjectorNo.TabStop = true;
            this.rdGRMountedInjectorNo.Tag = "GroundRunner";
            this.rdGRMountedInjectorNo.Text = "NO";
            this.rdGRMountedInjectorNo.UseVisualStyleBackColor = true;
            this.rdGRMountedInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRMountedInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRMountedInjectorYes
            // 
            this.rdGRMountedInjectorYes.AutoSize = true;
            this.rdGRMountedInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdGRMountedInjectorYes.Name = "rdGRMountedInjectorYes";
            this.rdGRMountedInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdGRMountedInjectorYes.TabIndex = 0;
            this.rdGRMountedInjectorYes.TabStop = true;
            this.rdGRMountedInjectorYes.Tag = "GroundRunner";
            this.rdGRMountedInjectorYes.Text = "YES";
            this.rdGRMountedInjectorYes.UseVisualStyleBackColor = true;
            this.rdGRMountedInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(395, 64);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(45, 13);
            this.label174.TabIndex = 60;
            this.label174.Text = "Injector:";
            // 
            // pnlGRPressureRegulator
            // 
            this.pnlGRPressureRegulator.Controls.Add(this.radioButton23);
            this.pnlGRPressureRegulator.Controls.Add(this.rdGRPressureRegulatorNo);
            this.pnlGRPressureRegulator.Controls.Add(this.rdGRPressureRegulatorYes);
            this.pnlGRPressureRegulator.Location = new System.Drawing.Point(450, 15);
            this.pnlGRPressureRegulator.Name = "pnlGRPressureRegulator";
            this.pnlGRPressureRegulator.Size = new System.Drawing.Size(149, 31);
            this.pnlGRPressureRegulator.TabIndex = 59;
            // 
            // radioButton23
            // 
            this.radioButton23.AutoSize = true;
            this.radioButton23.Location = new System.Drawing.Point(-290, 41);
            this.radioButton23.Name = "radioButton23";
            this.radioButton23.Size = new System.Drawing.Size(41, 17);
            this.radioButton23.TabIndex = 46;
            this.radioButton23.TabStop = true;
            this.radioButton23.Tag = "Echo";
            this.radioButton23.Text = "NO";
            this.radioButton23.UseVisualStyleBackColor = true;
            // 
            // rdGRPressureRegulatorNo
            // 
            this.rdGRPressureRegulatorNo.AutoSize = true;
            this.rdGRPressureRegulatorNo.Location = new System.Drawing.Point(87, 7);
            this.rdGRPressureRegulatorNo.Name = "rdGRPressureRegulatorNo";
            this.rdGRPressureRegulatorNo.Size = new System.Drawing.Size(41, 17);
            this.rdGRPressureRegulatorNo.TabIndex = 1;
            this.rdGRPressureRegulatorNo.TabStop = true;
            this.rdGRPressureRegulatorNo.Tag = "GroundRunner";
            this.rdGRPressureRegulatorNo.Text = "NO";
            this.rdGRPressureRegulatorNo.UseVisualStyleBackColor = true;
            this.rdGRPressureRegulatorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRPressureRegulatorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRPressureRegulatorYes
            // 
            this.rdGRPressureRegulatorYes.AutoSize = true;
            this.rdGRPressureRegulatorYes.Location = new System.Drawing.Point(20, 7);
            this.rdGRPressureRegulatorYes.Name = "rdGRPressureRegulatorYes";
            this.rdGRPressureRegulatorYes.Size = new System.Drawing.Size(46, 17);
            this.rdGRPressureRegulatorYes.TabIndex = 0;
            this.rdGRPressureRegulatorYes.TabStop = true;
            this.rdGRPressureRegulatorYes.Tag = "GroundRunner";
            this.rdGRPressureRegulatorYes.Text = "YES";
            this.rdGRPressureRegulatorYes.UseVisualStyleBackColor = true;
            this.rdGRPressureRegulatorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(340, 23);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(100, 13);
            this.label175.TabIndex = 58;
            this.label175.Text = "Pressure Regulator:";
            // 
            // txtGRSpraybodySpacing
            // 
            this.txtGRSpraybodySpacing.Location = new System.Drawing.Point(157, 134);
            this.txtGRSpraybodySpacing.Mask = "F2";
            this.txtGRSpraybodySpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtGRSpraybodySpacing.Name = "txtGRSpraybodySpacing";
            this.txtGRSpraybodySpacing.Size = new System.Drawing.Size(158, 20);
            this.txtGRSpraybodySpacing.TabIndex = 67;
            this.txtGRSpraybodySpacing.TabStop = false;
            this.txtGRSpraybodySpacing.Tag = "GroundRunner";
            this.txtGRSpraybodySpacing.Text = "0.00";
            this.txtGRSpraybodySpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label176
            // 
            this.label176.Location = new System.Drawing.Point(48, 133);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(100, 28);
            this.label176.TabIndex = 66;
            this.label176.Text = "Spraybody Spacing (IN):";
            // 
            // pnlGRSweep90
            // 
            this.pnlGRSweep90.Controls.Add(this.rdGRSweep90No);
            this.pnlGRSweep90.Controls.Add(this.rdGRSweep90Yes);
            this.pnlGRSweep90.Location = new System.Drawing.Point(750, 177);
            this.pnlGRSweep90.Name = "pnlGRSweep90";
            this.pnlGRSweep90.Size = new System.Drawing.Size(143, 31);
            this.pnlGRSweep90.TabIndex = 57;
            // 
            // rdGRSweep90No
            // 
            this.rdGRSweep90No.AutoSize = true;
            this.rdGRSweep90No.Location = new System.Drawing.Point(80, 6);
            this.rdGRSweep90No.Name = "rdGRSweep90No";
            this.rdGRSweep90No.Size = new System.Drawing.Size(41, 17);
            this.rdGRSweep90No.TabIndex = 1;
            this.rdGRSweep90No.TabStop = true;
            this.rdGRSweep90No.Tag = "GroundRunner";
            this.rdGRSweep90No.Text = "NO";
            this.rdGRSweep90No.UseVisualStyleBackColor = true;
            this.rdGRSweep90No.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRSweep90No.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRSweep90Yes
            // 
            this.rdGRSweep90Yes.AutoSize = true;
            this.rdGRSweep90Yes.Location = new System.Drawing.Point(19, 6);
            this.rdGRSweep90Yes.Name = "rdGRSweep90Yes";
            this.rdGRSweep90Yes.Size = new System.Drawing.Size(46, 17);
            this.rdGRSweep90Yes.TabIndex = 0;
            this.rdGRSweep90Yes.TabStop = true;
            this.rdGRSweep90Yes.Tag = "GroundRunner";
            this.rdGRSweep90Yes.Text = "YES";
            this.rdGRSweep90Yes.UseVisualStyleBackColor = true;
            this.rdGRSweep90Yes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(686, 192);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(58, 13);
            this.label177.TabIndex = 56;
            this.label177.Text = "Sweep 90:";
            // 
            // pnlGRTripleWaterLocklineUpgrade
            // 
            this.pnlGRTripleWaterLocklineUpgrade.Controls.Add(this.rdGRTripleWaterLocklineUpgradeNo);
            this.pnlGRTripleWaterLocklineUpgrade.Controls.Add(this.rdGRTripleWaterLocklineUpgradeYes);
            this.pnlGRTripleWaterLocklineUpgrade.Location = new System.Drawing.Point(156, 193);
            this.pnlGRTripleWaterLocklineUpgrade.Name = "pnlGRTripleWaterLocklineUpgrade";
            this.pnlGRTripleWaterLocklineUpgrade.Size = new System.Drawing.Size(160, 31);
            this.pnlGRTripleWaterLocklineUpgrade.TabIndex = 51;
            // 
            // rdGRTripleWaterLocklineUpgradeNo
            // 
            this.rdGRTripleWaterLocklineUpgradeNo.AutoSize = true;
            this.rdGRTripleWaterLocklineUpgradeNo.Location = new System.Drawing.Point(98, 7);
            this.rdGRTripleWaterLocklineUpgradeNo.Name = "rdGRTripleWaterLocklineUpgradeNo";
            this.rdGRTripleWaterLocklineUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdGRTripleWaterLocklineUpgradeNo.TabIndex = 1;
            this.rdGRTripleWaterLocklineUpgradeNo.TabStop = true;
            this.rdGRTripleWaterLocklineUpgradeNo.Tag = "GroundRunner";
            this.rdGRTripleWaterLocklineUpgradeNo.Text = "NO";
            this.rdGRTripleWaterLocklineUpgradeNo.UseVisualStyleBackColor = true;
            this.rdGRTripleWaterLocklineUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdGRTripleWaterLocklineUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdGRTripleWaterLocklineUpgradeYes
            // 
            this.rdGRTripleWaterLocklineUpgradeYes.AutoSize = true;
            this.rdGRTripleWaterLocklineUpgradeYes.Location = new System.Drawing.Point(26, 7);
            this.rdGRTripleWaterLocklineUpgradeYes.Name = "rdGRTripleWaterLocklineUpgradeYes";
            this.rdGRTripleWaterLocklineUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdGRTripleWaterLocklineUpgradeYes.TabIndex = 0;
            this.rdGRTripleWaterLocklineUpgradeYes.TabStop = true;
            this.rdGRTripleWaterLocklineUpgradeYes.Tag = "GroundRunner";
            this.rdGRTripleWaterLocklineUpgradeYes.Text = "YES";
            this.rdGRTripleWaterLocklineUpgradeYes.UseVisualStyleBackColor = true;
            this.rdGRTripleWaterLocklineUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(18, 199);
            this.label178.MaximumSize = new System.Drawing.Size(150, 0);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(136, 26);
            this.label178.TabIndex = 50;
            this.label178.Text = "Step/Stop/Water Lockline Upgrade:";
            // 
            // ddlGRControllerType
            // 
            this.ddlGRControllerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlGRControllerType.FormattingEnabled = true;
            this.ddlGRControllerType.Location = new System.Drawing.Point(156, 105);
            this.ddlGRControllerType.Name = "ddlGRControllerType";
            this.ddlGRControllerType.Size = new System.Drawing.Size(160, 21);
            this.ddlGRControllerType.TabIndex = 55;
            this.ddlGRControllerType.Tag = "GroundRunner";
            this.ddlGRControllerType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(66, 107);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(81, 13);
            this.label179.TabIndex = 54;
            this.label179.Text = "Controller Type:";
            // 
            // txtGRHoseLoopHeight
            // 
            this.txtGRHoseLoopHeight.Location = new System.Drawing.Point(156, 164);
            this.txtGRHoseLoopHeight.Mask = "F2";
            this.txtGRHoseLoopHeight.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtGRHoseLoopHeight.Name = "txtGRHoseLoopHeight";
            this.txtGRHoseLoopHeight.Size = new System.Drawing.Size(160, 20);
            this.txtGRHoseLoopHeight.TabIndex = 47;
            this.txtGRHoseLoopHeight.TabStop = false;
            this.txtGRHoseLoopHeight.Tag = "GroundRunner";
            this.txtGRHoseLoopHeight.Text = "0.00";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(34, 167);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(116, 13);
            this.label180.TabIndex = 46;
            this.label180.Text = "Hose Loop Height (IN):";
            // 
            // ddlGRSingleDualWater
            // 
            this.ddlGRSingleDualWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlGRSingleDualWater.FormattingEnabled = true;
            this.ddlGRSingleDualWater.Location = new System.Drawing.Point(156, 45);
            this.ddlGRSingleDualWater.Name = "ddlGRSingleDualWater";
            this.ddlGRSingleDualWater.Size = new System.Drawing.Size(160, 21);
            this.ddlGRSingleDualWater.TabIndex = 49;
            this.ddlGRSingleDualWater.Tag = "GroundRunner";
            this.ddlGRSingleDualWater.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(56, 49);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(98, 13);
            this.label181.TabIndex = 48;
            this.label181.Text = "Single/Dual Water:";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(44, 16);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(105, 13);
            this.label182.TabIndex = 44;
            this.label182.Text = "Quantity Of Systems:";
            // 
            // tpSkyRailBoom
            // 
            this.tpSkyRailBoom.Controls.Add(this.ddlSREVWireGauge);
            this.tpSkyRailBoom.Controls.Add(this.label219);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRWaterFeedSystem);
            this.tpSkyRailBoom.Controls.Add(this.label210);
            this.tpSkyRailBoom.Controls.Add(this.ddlSRInjectorType);
            this.tpSkyRailBoom.Controls.Add(this.label183);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRAutoInjector);
            this.tpSkyRailBoom.Controls.Add(this.label184);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRWaterSolenoidControl);
            this.tpSkyRailBoom.Controls.Add(this.label185);
            this.tpSkyRailBoom.Controls.Add(this.pnlSREV);
            this.tpSkyRailBoom.Controls.Add(this.label186);
            this.tpSkyRailBoom.Controls.Add(this.txtSRTip3);
            this.tpSkyRailBoom.Controls.Add(this.label187);
            this.tpSkyRailBoom.Controls.Add(this.txtSRTip2);
            this.tpSkyRailBoom.Controls.Add(this.label188);
            this.tpSkyRailBoom.Controls.Add(this.txtSRTip1);
            this.tpSkyRailBoom.Controls.Add(this.label189);
            this.tpSkyRailBoom.Controls.Add(this.txtSRNumberOfRows);
            this.tpSkyRailBoom.Controls.Add(this.label190);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRWirelessWalkSwitch);
            this.tpSkyRailBoom.Controls.Add(this.label191);
            this.tpSkyRailBoom.Controls.Add(this.txtSRRailDropDownAssy);
            this.tpSkyRailBoom.Controls.Add(this.label192);
            this.tpSkyRailBoom.Controls.Add(this.pnlSR1_1_4thHoseUpgrade);
            this.tpSkyRailBoom.Controls.Add(this.label193);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRTripleTurretBody);
            this.tpSkyRailBoom.Controls.Add(this.label194);
            this.tpSkyRailBoom.Controls.Add(this.txtSRQuantityOfSystems);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRMountedInjector);
            this.tpSkyRailBoom.Controls.Add(this.label195);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRPressureRegulator);
            this.tpSkyRailBoom.Controls.Add(this.label196);
            this.tpSkyRailBoom.Controls.Add(this.txtSRSpraybodySpacing);
            this.tpSkyRailBoom.Controls.Add(this.label197);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRSweep90);
            this.tpSkyRailBoom.Controls.Add(this.label198);
            this.tpSkyRailBoom.Controls.Add(this.pnlSRTripleWaterLocklineUpgrade);
            this.tpSkyRailBoom.Controls.Add(this.label199);
            this.tpSkyRailBoom.Controls.Add(this.ddlSRControllerType);
            this.tpSkyRailBoom.Controls.Add(this.label200);
            this.tpSkyRailBoom.Controls.Add(this.txtSRHoseLoopHeight);
            this.tpSkyRailBoom.Controls.Add(this.label201);
            this.tpSkyRailBoom.Controls.Add(this.ddlSRSingleDualWater);
            this.tpSkyRailBoom.Controls.Add(this.label202);
            this.tpSkyRailBoom.Controls.Add(this.label203);
            this.tpSkyRailBoom.Location = new System.Drawing.Point(4, 22);
            this.tpSkyRailBoom.Name = "tpSkyRailBoom";
            this.tpSkyRailBoom.Padding = new System.Windows.Forms.Padding(3);
            this.tpSkyRailBoom.Size = new System.Drawing.Size(1368, 267);
            this.tpSkyRailBoom.TabIndex = 7;
            this.tpSkyRailBoom.Tag = "SkyRail";
            this.tpSkyRailBoom.Text = "Sky Rail";
            this.tpSkyRailBoom.UseVisualStyleBackColor = true;
            // 
            // ddlSREVWireGauge
            // 
            this.ddlSREVWireGauge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSREVWireGauge.FormattingEnabled = true;
            this.ddlSREVWireGauge.Location = new System.Drawing.Point(450, 224);
            this.ddlSREVWireGauge.Name = "ddlSREVWireGauge";
            this.ddlSREVWireGauge.Size = new System.Drawing.Size(148, 21);
            this.ddlSREVWireGauge.TabIndex = 131;
            this.ddlSREVWireGauge.Tag = "SingleRail";
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(355, 227);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(86, 13);
            this.label219.TabIndex = 130;
            this.label219.Text = "EV Wire/Gauge:";
            // 
            // pnlSRWaterFeedSystem
            // 
            this.pnlSRWaterFeedSystem.Controls.Add(this.radioButton7);
            this.pnlSRWaterFeedSystem.Controls.Add(this.rdSRWaterFeedSystemNo);
            this.pnlSRWaterFeedSystem.Controls.Add(this.rdSRWaterFeedSystemYes);
            this.pnlSRWaterFeedSystem.Location = new System.Drawing.Point(1054, 54);
            this.pnlSRWaterFeedSystem.Name = "pnlSRWaterFeedSystem";
            this.pnlSRWaterFeedSystem.Size = new System.Drawing.Size(139, 31);
            this.pnlSRWaterFeedSystem.TabIndex = 129;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(-290, 41);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(41, 17);
            this.radioButton7.TabIndex = 46;
            this.radioButton7.TabStop = true;
            this.radioButton7.Tag = "Echo";
            this.radioButton7.Text = "NO";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // rdSRWaterFeedSystemNo
            // 
            this.rdSRWaterFeedSystemNo.AutoSize = true;
            this.rdSRWaterFeedSystemNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRWaterFeedSystemNo.Name = "rdSRWaterFeedSystemNo";
            this.rdSRWaterFeedSystemNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRWaterFeedSystemNo.TabIndex = 1;
            this.rdSRWaterFeedSystemNo.TabStop = true;
            this.rdSRWaterFeedSystemNo.Tag = "SkyRail";
            this.rdSRWaterFeedSystemNo.Text = "NO";
            this.rdSRWaterFeedSystemNo.UseVisualStyleBackColor = true;
            this.rdSRWaterFeedSystemNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRWaterFeedSystemNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRWaterFeedSystemYes
            // 
            this.rdSRWaterFeedSystemYes.AutoSize = true;
            this.rdSRWaterFeedSystemYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRWaterFeedSystemYes.Name = "rdSRWaterFeedSystemYes";
            this.rdSRWaterFeedSystemYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRWaterFeedSystemYes.TabIndex = 0;
            this.rdSRWaterFeedSystemYes.TabStop = true;
            this.rdSRWaterFeedSystemYes.Tag = "SkyRail";
            this.rdSRWaterFeedSystemYes.Text = "YES";
            this.rdSRWaterFeedSystemYes.UseVisualStyleBackColor = true;
            this.rdSRWaterFeedSystemYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label210
            // 
            this.label210.Location = new System.Drawing.Point(973, 54);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(75, 29);
            this.label210.TabIndex = 128;
            this.label210.Text = "Center Water Feed System:";
            this.label210.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ddlSRInjectorType
            // 
            this.ddlSRInjectorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSRInjectorType.FormattingEnabled = true;
            this.ddlSRInjectorType.Location = new System.Drawing.Point(450, 101);
            this.ddlSRInjectorType.Name = "ddlSRInjectorType";
            this.ddlSRInjectorType.Size = new System.Drawing.Size(147, 21);
            this.ddlSRInjectorType.TabIndex = 127;
            this.ddlSRInjectorType.Tag = "SkyRail";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(368, 103);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(72, 13);
            this.label183.TabIndex = 126;
            this.label183.Text = "Injector Type:";
            // 
            // pnlSRAutoInjector
            // 
            this.pnlSRAutoInjector.Controls.Add(this.rdSRAutoInjectorNo);
            this.pnlSRAutoInjector.Controls.Add(this.rdSRAutoInjectorYes);
            this.pnlSRAutoInjector.Location = new System.Drawing.Point(451, 132);
            this.pnlSRAutoInjector.Name = "pnlSRAutoInjector";
            this.pnlSRAutoInjector.Size = new System.Drawing.Size(147, 31);
            this.pnlSRAutoInjector.TabIndex = 125;
            this.pnlSRAutoInjector.Tag = "SkyRail";
            // 
            // rdSRAutoInjectorNo
            // 
            this.rdSRAutoInjectorNo.AutoSize = true;
            this.rdSRAutoInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRAutoInjectorNo.Name = "rdSRAutoInjectorNo";
            this.rdSRAutoInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRAutoInjectorNo.TabIndex = 1;
            this.rdSRAutoInjectorNo.TabStop = true;
            this.rdSRAutoInjectorNo.Tag = "SkyRail";
            this.rdSRAutoInjectorNo.Text = "NO";
            this.rdSRAutoInjectorNo.UseVisualStyleBackColor = true;
            this.rdSRAutoInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRAutoInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRAutoInjectorYes
            // 
            this.rdSRAutoInjectorYes.AutoSize = true;
            this.rdSRAutoInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRAutoInjectorYes.Name = "rdSRAutoInjectorYes";
            this.rdSRAutoInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRAutoInjectorYes.TabIndex = 0;
            this.rdSRAutoInjectorYes.TabStop = true;
            this.rdSRAutoInjectorYes.Tag = "SkyRail";
            this.rdSRAutoInjectorYes.Text = "YES";
            this.rdSRAutoInjectorYes.UseVisualStyleBackColor = true;
            this.rdSRAutoInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(371, 141);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(70, 13);
            this.label184.TabIndex = 124;
            this.label184.Text = "Auto Injector:";
            // 
            // pnlSRWaterSolenoidControl
            // 
            this.pnlSRWaterSolenoidControl.Controls.Add(this.rdSRWaterSolenoidControlNo);
            this.pnlSRWaterSolenoidControl.Controls.Add(this.rdSRWaterSolenoidControlYes);
            this.pnlSRWaterSolenoidControl.Location = new System.Drawing.Point(1054, 13);
            this.pnlSRWaterSolenoidControl.Name = "pnlSRWaterSolenoidControl";
            this.pnlSRWaterSolenoidControl.Size = new System.Drawing.Size(139, 31);
            this.pnlSRWaterSolenoidControl.TabIndex = 123;
            this.pnlSRWaterSolenoidControl.Tag = "SkyRail";
            // 
            // rdSRWaterSolenoidControlNo
            // 
            this.rdSRWaterSolenoidControlNo.AutoSize = true;
            this.rdSRWaterSolenoidControlNo.Location = new System.Drawing.Point(80, 7);
            this.rdSRWaterSolenoidControlNo.Name = "rdSRWaterSolenoidControlNo";
            this.rdSRWaterSolenoidControlNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRWaterSolenoidControlNo.TabIndex = 1;
            this.rdSRWaterSolenoidControlNo.TabStop = true;
            this.rdSRWaterSolenoidControlNo.Tag = "SkyRail";
            this.rdSRWaterSolenoidControlNo.Text = "NO";
            this.rdSRWaterSolenoidControlNo.UseVisualStyleBackColor = true;
            this.rdSRWaterSolenoidControlNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRWaterSolenoidControlNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRWaterSolenoidControlYes
            // 
            this.rdSRWaterSolenoidControlYes.AutoSize = true;
            this.rdSRWaterSolenoidControlYes.Location = new System.Drawing.Point(19, 7);
            this.rdSRWaterSolenoidControlYes.Name = "rdSRWaterSolenoidControlYes";
            this.rdSRWaterSolenoidControlYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRWaterSolenoidControlYes.TabIndex = 0;
            this.rdSRWaterSolenoidControlYes.TabStop = true;
            this.rdSRWaterSolenoidControlYes.Tag = "SkyRail";
            this.rdSRWaterSolenoidControlYes.Text = "YES";
            this.rdSRWaterSolenoidControlYes.UseVisualStyleBackColor = true;
            this.rdSRWaterSolenoidControlYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(927, 19);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(119, 13);
            this.label185.TabIndex = 122;
            this.label185.Text = "Water Solenoid Control:";
            // 
            // pnlSREV
            // 
            this.pnlSREV.Controls.Add(this.rdSREVNo);
            this.pnlSREV.Controls.Add(this.rdSREVYes);
            this.pnlSREV.Location = new System.Drawing.Point(451, 177);
            this.pnlSREV.Name = "pnlSREV";
            this.pnlSREV.Size = new System.Drawing.Size(147, 31);
            this.pnlSREV.TabIndex = 121;
            this.pnlSREV.Tag = "SkyRail";
            // 
            // rdSREVNo
            // 
            this.rdSREVNo.AutoSize = true;
            this.rdSREVNo.Location = new System.Drawing.Point(87, 7);
            this.rdSREVNo.Name = "rdSREVNo";
            this.rdSREVNo.Size = new System.Drawing.Size(41, 17);
            this.rdSREVNo.TabIndex = 1;
            this.rdSREVNo.TabStop = true;
            this.rdSREVNo.Tag = "SkyRail";
            this.rdSREVNo.Text = "NO";
            this.rdSREVNo.UseVisualStyleBackColor = true;
            this.rdSREVNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSREVNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSREVYes
            // 
            this.rdSREVYes.AutoSize = true;
            this.rdSREVYes.Location = new System.Drawing.Point(19, 7);
            this.rdSREVYes.Name = "rdSREVYes";
            this.rdSREVYes.Size = new System.Drawing.Size(46, 17);
            this.rdSREVYes.TabIndex = 0;
            this.rdSREVYes.TabStop = true;
            this.rdSREVYes.Tag = "SkyRail";
            this.rdSREVYes.Text = "YES";
            this.rdSREVYes.UseVisualStyleBackColor = true;
            this.rdSREVYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label186
            // 
            this.label186.Location = new System.Drawing.Point(361, 175);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(84, 37);
            this.label186.TabIndex = 120;
            this.label186.Text = "EV, VPD, Third Party Control:";
            // 
            // txtSRTip3
            // 
            this.txtSRTip3.Location = new System.Drawing.Point(747, 144);
            this.txtSRTip3.Name = "txtSRTip3";
            this.txtSRTip3.Size = new System.Drawing.Size(147, 20);
            this.txtSRTip3.TabIndex = 119;
            this.txtSRTip3.Tag = "SkyRail";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(706, 146);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(34, 13);
            this.label187.TabIndex = 118;
            this.label187.Text = "Tip 3:";
            // 
            // txtSRTip2
            // 
            this.txtSRTip2.Location = new System.Drawing.Point(747, 119);
            this.txtSRTip2.Name = "txtSRTip2";
            this.txtSRTip2.Size = new System.Drawing.Size(147, 20);
            this.txtSRTip2.TabIndex = 117;
            this.txtSRTip2.Tag = "SkyRail";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(706, 121);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(34, 13);
            this.label188.TabIndex = 116;
            this.label188.Text = "Tip 2:";
            // 
            // txtSRTip1
            // 
            this.txtSRTip1.Location = new System.Drawing.Point(747, 93);
            this.txtSRTip1.Name = "txtSRTip1";
            this.txtSRTip1.Size = new System.Drawing.Size(147, 20);
            this.txtSRTip1.TabIndex = 115;
            this.txtSRTip1.Tag = "SkyRail";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(706, 96);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(34, 13);
            this.label189.TabIndex = 114;
            this.label189.Text = "Tip 1:";
            // 
            // txtSRNumberOfRows
            // 
            this.txtSRNumberOfRows.Location = new System.Drawing.Point(159, 75);
            this.txtSRNumberOfRows.Mask = "G";
            this.txtSRNumberOfRows.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRNumberOfRows.Name = "txtSRNumberOfRows";
            this.txtSRNumberOfRows.Size = new System.Drawing.Size(158, 20);
            this.txtSRNumberOfRows.TabIndex = 113;
            this.txtSRNumberOfRows.TabStop = false;
            this.txtSRNumberOfRows.Tag = "SkyRail";
            this.txtSRNumberOfRows.Text = "0";
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(60, 78);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(91, 13);
            this.label190.TabIndex = 112;
            this.label190.Text = "Number Of Rows:";
            // 
            // pnlSRWirelessWalkSwitch
            // 
            this.pnlSRWirelessWalkSwitch.Controls.Add(this.rdSRWirelessWalkSwitchNo);
            this.pnlSRWirelessWalkSwitch.Controls.Add(this.rdSRWirelessWalkSwitchYes);
            this.pnlSRWirelessWalkSwitch.Location = new System.Drawing.Point(747, 220);
            this.pnlSRWirelessWalkSwitch.Name = "pnlSRWirelessWalkSwitch";
            this.pnlSRWirelessWalkSwitch.Size = new System.Drawing.Size(146, 31);
            this.pnlSRWirelessWalkSwitch.TabIndex = 111;
            this.pnlSRWirelessWalkSwitch.Tag = "SkyRail";
            // 
            // rdSRWirelessWalkSwitchNo
            // 
            this.rdSRWirelessWalkSwitchNo.AutoSize = true;
            this.rdSRWirelessWalkSwitchNo.Location = new System.Drawing.Point(80, 7);
            this.rdSRWirelessWalkSwitchNo.Name = "rdSRWirelessWalkSwitchNo";
            this.rdSRWirelessWalkSwitchNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRWirelessWalkSwitchNo.TabIndex = 1;
            this.rdSRWirelessWalkSwitchNo.TabStop = true;
            this.rdSRWirelessWalkSwitchNo.Tag = "SkyRail";
            this.rdSRWirelessWalkSwitchNo.Text = "NO";
            this.rdSRWirelessWalkSwitchNo.UseVisualStyleBackColor = true;
            this.rdSRWirelessWalkSwitchNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRWirelessWalkSwitchNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRWirelessWalkSwitchYes
            // 
            this.rdSRWirelessWalkSwitchYes.AutoSize = true;
            this.rdSRWirelessWalkSwitchYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRWirelessWalkSwitchYes.Name = "rdSRWirelessWalkSwitchYes";
            this.rdSRWirelessWalkSwitchYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRWirelessWalkSwitchYes.TabIndex = 0;
            this.rdSRWirelessWalkSwitchYes.TabStop = true;
            this.rdSRWirelessWalkSwitchYes.Tag = "SkyRail";
            this.rdSRWirelessWalkSwitchYes.Text = "YES";
            this.rdSRWirelessWalkSwitchYes.UseVisualStyleBackColor = true;
            this.rdSRWirelessWalkSwitchYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label191
            // 
            this.label191.Location = new System.Drawing.Point(643, 219);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(96, 28);
            this.label191.TabIndex = 110;
            this.label191.Text = "Wireless Walk Switch:";
            this.label191.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSRRailDropDownAssy
            // 
            this.txtSRRailDropDownAssy.Location = new System.Drawing.Point(158, 231);
            this.txtSRRailDropDownAssy.Mask = "F2";
            this.txtSRRailDropDownAssy.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRRailDropDownAssy.Name = "txtSRRailDropDownAssy";
            this.txtSRRailDropDownAssy.Size = new System.Drawing.Size(160, 20);
            this.txtSRRailDropDownAssy.TabIndex = 105;
            this.txtSRRailDropDownAssy.TabStop = false;
            this.txtSRRailDropDownAssy.Tag = "SkyRail";
            this.txtSRRailDropDownAssy.Text = "0.00";
            // 
            // label192
            // 
            this.label192.Location = new System.Drawing.Point(16, 234);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(132, 16);
            this.label192.TabIndex = 104;
            this.label192.Text = "Rail Drop Down Assy. (IN):";
            // 
            // pnlSR1_1_4thHoseUpgrade
            // 
            this.pnlSR1_1_4thHoseUpgrade.Controls.Add(this.rdSR1_1_4thHoseUpgradeNo);
            this.pnlSR1_1_4thHoseUpgrade.Controls.Add(this.rdSR1_1_4thHoseUpgradeYes);
            this.pnlSR1_1_4thHoseUpgrade.Location = new System.Drawing.Point(746, 12);
            this.pnlSR1_1_4thHoseUpgrade.Name = "pnlSR1_1_4thHoseUpgrade";
            this.pnlSR1_1_4thHoseUpgrade.Size = new System.Drawing.Size(147, 31);
            this.pnlSR1_1_4thHoseUpgrade.TabIndex = 107;
            this.pnlSR1_1_4thHoseUpgrade.Tag = "SkyRail";
            // 
            // rdSR1_1_4thHoseUpgradeNo
            // 
            this.rdSR1_1_4thHoseUpgradeNo.AutoSize = true;
            this.rdSR1_1_4thHoseUpgradeNo.Location = new System.Drawing.Point(87, 7);
            this.rdSR1_1_4thHoseUpgradeNo.Name = "rdSR1_1_4thHoseUpgradeNo";
            this.rdSR1_1_4thHoseUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdSR1_1_4thHoseUpgradeNo.TabIndex = 1;
            this.rdSR1_1_4thHoseUpgradeNo.TabStop = true;
            this.rdSR1_1_4thHoseUpgradeNo.Tag = "SkyRail";
            this.rdSR1_1_4thHoseUpgradeNo.Text = "NO";
            this.rdSR1_1_4thHoseUpgradeNo.UseVisualStyleBackColor = true;
            this.rdSR1_1_4thHoseUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSR1_1_4thHoseUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSR1_1_4thHoseUpgradeYes
            // 
            this.rdSR1_1_4thHoseUpgradeYes.AutoSize = true;
            this.rdSR1_1_4thHoseUpgradeYes.Location = new System.Drawing.Point(20, 7);
            this.rdSR1_1_4thHoseUpgradeYes.Name = "rdSR1_1_4thHoseUpgradeYes";
            this.rdSR1_1_4thHoseUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdSR1_1_4thHoseUpgradeYes.TabIndex = 0;
            this.rdSR1_1_4thHoseUpgradeYes.TabStop = true;
            this.rdSR1_1_4thHoseUpgradeYes.Tag = "SkyRail";
            this.rdSR1_1_4thHoseUpgradeYes.Text = "YES";
            this.rdSR1_1_4thHoseUpgradeYes.UseVisualStyleBackColor = true;
            this.rdSR1_1_4thHoseUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label193
            // 
            this.label193.Location = new System.Drawing.Point(637, 15);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(94, 28);
            this.label193.TabIndex = 106;
            this.label193.Text = "1 1/4\" Hose Upgrade:";
            this.label193.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlSRTripleTurretBody
            // 
            this.pnlSRTripleTurretBody.Controls.Add(this.rdSRTripleTurretBodyNo);
            this.pnlSRTripleTurretBody.Controls.Add(this.rdSRTripleTurretBodyYes);
            this.pnlSRTripleTurretBody.Location = new System.Drawing.Point(747, 49);
            this.pnlSRTripleTurretBody.Name = "pnlSRTripleTurretBody";
            this.pnlSRTripleTurretBody.Size = new System.Drawing.Size(146, 31);
            this.pnlSRTripleTurretBody.TabIndex = 95;
            this.pnlSRTripleTurretBody.Tag = "SkyRail";
            // 
            // rdSRTripleTurretBodyNo
            // 
            this.rdSRTripleTurretBodyNo.AutoSize = true;
            this.rdSRTripleTurretBodyNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRTripleTurretBodyNo.Name = "rdSRTripleTurretBodyNo";
            this.rdSRTripleTurretBodyNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRTripleTurretBodyNo.TabIndex = 1;
            this.rdSRTripleTurretBodyNo.TabStop = true;
            this.rdSRTripleTurretBodyNo.Tag = "SkyRail";
            this.rdSRTripleTurretBodyNo.Text = "NO";
            this.rdSRTripleTurretBodyNo.UseVisualStyleBackColor = true;
            this.rdSRTripleTurretBodyNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRTripleTurretBodyNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRTripleTurretBodyYes
            // 
            this.rdSRTripleTurretBodyYes.AutoSize = true;
            this.rdSRTripleTurretBodyYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRTripleTurretBodyYes.Name = "rdSRTripleTurretBodyYes";
            this.rdSRTripleTurretBodyYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRTripleTurretBodyYes.TabIndex = 0;
            this.rdSRTripleTurretBodyYes.TabStop = true;
            this.rdSRTripleTurretBodyYes.Tag = "SkyRail";
            this.rdSRTripleTurretBodyYes.Text = "YES";
            this.rdSRTripleTurretBodyYes.UseVisualStyleBackColor = true;
            this.rdSRTripleTurretBodyYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(643, 56);
            this.label194.MaximumSize = new System.Drawing.Size(150, 0);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(94, 13);
            this.label194.TabIndex = 94;
            this.label194.Text = "Triple Turret Body:";
            // 
            // txtSRQuantityOfSystems
            // 
            this.txtSRQuantityOfSystems.Location = new System.Drawing.Point(157, 14);
            this.txtSRQuantityOfSystems.Mask = "G";
            this.txtSRQuantityOfSystems.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRQuantityOfSystems.Name = "txtSRQuantityOfSystems";
            this.txtSRQuantityOfSystems.Size = new System.Drawing.Size(160, 20);
            this.txtSRQuantityOfSystems.TabIndex = 87;
            this.txtSRQuantityOfSystems.TabStop = false;
            this.txtSRQuantityOfSystems.Tag = "SkyRail";
            this.txtSRQuantityOfSystems.Text = "0";
            this.txtSRQuantityOfSystems.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // pnlSRMountedInjector
            // 
            this.pnlSRMountedInjector.Controls.Add(this.rdSRMountedInjectorNo);
            this.pnlSRMountedInjector.Controls.Add(this.rdSRMountedInjectorYes);
            this.pnlSRMountedInjector.Location = new System.Drawing.Point(451, 54);
            this.pnlSRMountedInjector.Name = "pnlSRMountedInjector";
            this.pnlSRMountedInjector.Size = new System.Drawing.Size(147, 31);
            this.pnlSRMountedInjector.TabIndex = 103;
            this.pnlSRMountedInjector.Tag = "SkyRail";
            // 
            // rdSRMountedInjectorNo
            // 
            this.rdSRMountedInjectorNo.AutoSize = true;
            this.rdSRMountedInjectorNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRMountedInjectorNo.Name = "rdSRMountedInjectorNo";
            this.rdSRMountedInjectorNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRMountedInjectorNo.TabIndex = 1;
            this.rdSRMountedInjectorNo.TabStop = true;
            this.rdSRMountedInjectorNo.Tag = "SkyRail";
            this.rdSRMountedInjectorNo.Text = "NO";
            this.rdSRMountedInjectorNo.UseVisualStyleBackColor = true;
            this.rdSRMountedInjectorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRMountedInjectorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRMountedInjectorYes
            // 
            this.rdSRMountedInjectorYes.AutoSize = true;
            this.rdSRMountedInjectorYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRMountedInjectorYes.Name = "rdSRMountedInjectorYes";
            this.rdSRMountedInjectorYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRMountedInjectorYes.TabIndex = 0;
            this.rdSRMountedInjectorYes.TabStop = true;
            this.rdSRMountedInjectorYes.Tag = "SkyRail";
            this.rdSRMountedInjectorYes.Text = "YES";
            this.rdSRMountedInjectorYes.UseVisualStyleBackColor = true;
            this.rdSRMountedInjectorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(396, 64);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(45, 13);
            this.label195.TabIndex = 102;
            this.label195.Text = "Injector:";
            // 
            // pnlSRPressureRegulator
            // 
            this.pnlSRPressureRegulator.Controls.Add(this.radioButton25);
            this.pnlSRPressureRegulator.Controls.Add(this.rdSRPressureRegulatorNo);
            this.pnlSRPressureRegulator.Controls.Add(this.rdSRPressureRegulatorYes);
            this.pnlSRPressureRegulator.Location = new System.Drawing.Point(451, 15);
            this.pnlSRPressureRegulator.Name = "pnlSRPressureRegulator";
            this.pnlSRPressureRegulator.Size = new System.Drawing.Size(147, 31);
            this.pnlSRPressureRegulator.TabIndex = 101;
            this.pnlSRPressureRegulator.Tag = "SkyRail";
            // 
            // radioButton25
            // 
            this.radioButton25.AutoSize = true;
            this.radioButton25.Location = new System.Drawing.Point(-290, 41);
            this.radioButton25.Name = "radioButton25";
            this.radioButton25.Size = new System.Drawing.Size(41, 17);
            this.radioButton25.TabIndex = 46;
            this.radioButton25.TabStop = true;
            this.radioButton25.Tag = "Echo";
            this.radioButton25.Text = "NO";
            this.radioButton25.UseVisualStyleBackColor = true;
            // 
            // rdSRPressureRegulatorNo
            // 
            this.rdSRPressureRegulatorNo.AutoSize = true;
            this.rdSRPressureRegulatorNo.Location = new System.Drawing.Point(87, 7);
            this.rdSRPressureRegulatorNo.Name = "rdSRPressureRegulatorNo";
            this.rdSRPressureRegulatorNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRPressureRegulatorNo.TabIndex = 1;
            this.rdSRPressureRegulatorNo.TabStop = true;
            this.rdSRPressureRegulatorNo.Tag = "SkyRail";
            this.rdSRPressureRegulatorNo.Text = "NO";
            this.rdSRPressureRegulatorNo.UseVisualStyleBackColor = true;
            this.rdSRPressureRegulatorNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRPressureRegulatorNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRPressureRegulatorYes
            // 
            this.rdSRPressureRegulatorYes.AutoSize = true;
            this.rdSRPressureRegulatorYes.Location = new System.Drawing.Point(20, 7);
            this.rdSRPressureRegulatorYes.Name = "rdSRPressureRegulatorYes";
            this.rdSRPressureRegulatorYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRPressureRegulatorYes.TabIndex = 0;
            this.rdSRPressureRegulatorYes.TabStop = true;
            this.rdSRPressureRegulatorYes.Tag = "SkyRail";
            this.rdSRPressureRegulatorYes.Text = "YES";
            this.rdSRPressureRegulatorYes.UseVisualStyleBackColor = true;
            this.rdSRPressureRegulatorYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(341, 23);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(100, 13);
            this.label196.TabIndex = 100;
            this.label196.Text = "Pressure Regulator:";
            // 
            // txtSRSpraybodySpacing
            // 
            this.txtSRSpraybodySpacing.Location = new System.Drawing.Point(158, 134);
            this.txtSRSpraybodySpacing.Mask = "F2";
            this.txtSRSpraybodySpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRSpraybodySpacing.Name = "txtSRSpraybodySpacing";
            this.txtSRSpraybodySpacing.Size = new System.Drawing.Size(158, 20);
            this.txtSRSpraybodySpacing.TabIndex = 109;
            this.txtSRSpraybodySpacing.TabStop = false;
            this.txtSRSpraybodySpacing.Tag = "SkyRail";
            this.txtSRSpraybodySpacing.Text = "0.00";
            this.txtSRSpraybodySpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label197
            // 
            this.label197.Location = new System.Drawing.Point(49, 133);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(100, 28);
            this.label197.TabIndex = 108;
            this.label197.Text = "Spraybody Spacing (IN):";
            // 
            // pnlSRSweep90
            // 
            this.pnlSRSweep90.Controls.Add(this.rdSRSweep90No);
            this.pnlSRSweep90.Controls.Add(this.rdSRSweep90Yes);
            this.pnlSRSweep90.Location = new System.Drawing.Point(747, 177);
            this.pnlSRSweep90.Name = "pnlSRSweep90";
            this.pnlSRSweep90.Size = new System.Drawing.Size(146, 31);
            this.pnlSRSweep90.TabIndex = 99;
            this.pnlSRSweep90.Tag = "SkyRail";
            // 
            // rdSRSweep90No
            // 
            this.rdSRSweep90No.AutoSize = true;
            this.rdSRSweep90No.Location = new System.Drawing.Point(80, 6);
            this.rdSRSweep90No.Name = "rdSRSweep90No";
            this.rdSRSweep90No.Size = new System.Drawing.Size(41, 17);
            this.rdSRSweep90No.TabIndex = 1;
            this.rdSRSweep90No.TabStop = true;
            this.rdSRSweep90No.Tag = "SkyRail";
            this.rdSRSweep90No.Text = "NO";
            this.rdSRSweep90No.UseVisualStyleBackColor = true;
            this.rdSRSweep90No.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRSweep90No.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRSweep90Yes
            // 
            this.rdSRSweep90Yes.AutoSize = true;
            this.rdSRSweep90Yes.Location = new System.Drawing.Point(19, 6);
            this.rdSRSweep90Yes.Name = "rdSRSweep90Yes";
            this.rdSRSweep90Yes.Size = new System.Drawing.Size(46, 17);
            this.rdSRSweep90Yes.TabIndex = 0;
            this.rdSRSweep90Yes.TabStop = true;
            this.rdSRSweep90Yes.Tag = "SkyRail";
            this.rdSRSweep90Yes.Text = "YES";
            this.rdSRSweep90Yes.UseVisualStyleBackColor = true;
            this.rdSRSweep90Yes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(683, 192);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(58, 13);
            this.label198.TabIndex = 98;
            this.label198.Text = "Sweep 90:";
            // 
            // pnlSRTripleWaterLocklineUpgrade
            // 
            this.pnlSRTripleWaterLocklineUpgrade.Controls.Add(this.rdSRTripleWaterLocklineUpgradeNo);
            this.pnlSRTripleWaterLocklineUpgrade.Controls.Add(this.rdSRTripleWaterLocklineUpgradeYes);
            this.pnlSRTripleWaterLocklineUpgrade.Location = new System.Drawing.Point(157, 193);
            this.pnlSRTripleWaterLocklineUpgrade.Name = "pnlSRTripleWaterLocklineUpgrade";
            this.pnlSRTripleWaterLocklineUpgrade.Size = new System.Drawing.Size(160, 31);
            this.pnlSRTripleWaterLocklineUpgrade.TabIndex = 93;
            this.pnlSRTripleWaterLocklineUpgrade.Tag = "SkyRail";
            // 
            // rdSRTripleWaterLocklineUpgradeNo
            // 
            this.rdSRTripleWaterLocklineUpgradeNo.AutoSize = true;
            this.rdSRTripleWaterLocklineUpgradeNo.Location = new System.Drawing.Point(98, 7);
            this.rdSRTripleWaterLocklineUpgradeNo.Name = "rdSRTripleWaterLocklineUpgradeNo";
            this.rdSRTripleWaterLocklineUpgradeNo.Size = new System.Drawing.Size(41, 17);
            this.rdSRTripleWaterLocklineUpgradeNo.TabIndex = 1;
            this.rdSRTripleWaterLocklineUpgradeNo.TabStop = true;
            this.rdSRTripleWaterLocklineUpgradeNo.Tag = "SkyRail";
            this.rdSRTripleWaterLocklineUpgradeNo.Text = "NO";
            this.rdSRTripleWaterLocklineUpgradeNo.UseVisualStyleBackColor = true;
            this.rdSRTripleWaterLocklineUpgradeNo.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdSRTripleWaterLocklineUpgradeNo.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdSRTripleWaterLocklineUpgradeYes
            // 
            this.rdSRTripleWaterLocklineUpgradeYes.AutoSize = true;
            this.rdSRTripleWaterLocklineUpgradeYes.Location = new System.Drawing.Point(26, 7);
            this.rdSRTripleWaterLocklineUpgradeYes.Name = "rdSRTripleWaterLocklineUpgradeYes";
            this.rdSRTripleWaterLocklineUpgradeYes.Size = new System.Drawing.Size(46, 17);
            this.rdSRTripleWaterLocklineUpgradeYes.TabIndex = 0;
            this.rdSRTripleWaterLocklineUpgradeYes.TabStop = true;
            this.rdSRTripleWaterLocklineUpgradeYes.Tag = "SkyRail";
            this.rdSRTripleWaterLocklineUpgradeYes.Text = "YES";
            this.rdSRTripleWaterLocklineUpgradeYes.UseVisualStyleBackColor = true;
            this.rdSRTripleWaterLocklineUpgradeYes.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Location = new System.Drawing.Point(19, 199);
            this.label199.MaximumSize = new System.Drawing.Size(150, 0);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(136, 26);
            this.label199.TabIndex = 92;
            this.label199.Text = "Step/Stop/Water Lockline Upgrade:";
            // 
            // ddlSRControllerType
            // 
            this.ddlSRControllerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSRControllerType.FormattingEnabled = true;
            this.ddlSRControllerType.Location = new System.Drawing.Point(157, 105);
            this.ddlSRControllerType.Name = "ddlSRControllerType";
            this.ddlSRControllerType.Size = new System.Drawing.Size(160, 21);
            this.ddlSRControllerType.TabIndex = 97;
            this.ddlSRControllerType.Tag = "SkyRail";
            this.ddlSRControllerType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(67, 107);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(81, 13);
            this.label200.TabIndex = 96;
            this.label200.Text = "Controller Type:";
            // 
            // txtSRHoseLoopHeight
            // 
            this.txtSRHoseLoopHeight.Location = new System.Drawing.Point(157, 164);
            this.txtSRHoseLoopHeight.Mask = "F2";
            this.txtSRHoseLoopHeight.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSRHoseLoopHeight.Name = "txtSRHoseLoopHeight";
            this.txtSRHoseLoopHeight.Size = new System.Drawing.Size(160, 20);
            this.txtSRHoseLoopHeight.TabIndex = 89;
            this.txtSRHoseLoopHeight.TabStop = false;
            this.txtSRHoseLoopHeight.Tag = "SkyRail";
            this.txtSRHoseLoopHeight.Text = "0.00";
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(35, 167);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(116, 13);
            this.label201.TabIndex = 88;
            this.label201.Text = "Hose Loop Height (IN):";
            // 
            // ddlSRSingleDualWater
            // 
            this.ddlSRSingleDualWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSRSingleDualWater.FormattingEnabled = true;
            this.ddlSRSingleDualWater.Location = new System.Drawing.Point(157, 45);
            this.ddlSRSingleDualWater.Name = "ddlSRSingleDualWater";
            this.ddlSRSingleDualWater.Size = new System.Drawing.Size(160, 21);
            this.ddlSRSingleDualWater.TabIndex = 91;
            this.ddlSRSingleDualWater.Tag = "SkyRail";
            this.ddlSRSingleDualWater.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(57, 49);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(98, 13);
            this.label202.TabIndex = 90;
            this.label202.Text = "Single/Dual Water:";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(45, 16);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(105, 13);
            this.label203.TabIndex = 86;
            this.label203.Text = "Quantity Of Systems:";
            // 
            // txtNumberOfBays
            // 
            this.txtNumberOfBays.Location = new System.Drawing.Point(487, 76);
            this.txtNumberOfBays.Mask = "G";
            this.txtNumberOfBays.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtNumberOfBays.Name = "txtNumberOfBays";
            this.txtNumberOfBays.Size = new System.Drawing.Size(59, 20);
            this.txtNumberOfBays.TabIndex = 27;
            this.txtNumberOfBays.TabStop = false;
            this.txtNumberOfBays.Text = "0";
            this.txtNumberOfBays.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtBayLength
            // 
            this.txtBayLength.Location = new System.Drawing.Point(487, 108);
            this.txtBayLength.Mask = "F2";
            this.txtBayLength.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtBayLength.Name = "txtBayLength";
            this.txtBayLength.Size = new System.Drawing.Size(59, 20);
            this.txtBayLength.TabIndex = 29;
            this.txtBayLength.TabStop = false;
            this.txtBayLength.Text = "0.00";
            this.txtBayLength.TextChanged += new System.EventHandler(this.CalculateHoseSize);
            this.txtBayLength.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtNumberOfBaySidewalls
            // 
            this.txtNumberOfBaySidewalls.Location = new System.Drawing.Point(707, 140);
            this.txtNumberOfBaySidewalls.Mask = "G";
            this.txtNumberOfBaySidewalls.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtNumberOfBaySidewalls.Name = "txtNumberOfBaySidewalls";
            this.txtNumberOfBaySidewalls.Size = new System.Drawing.Size(59, 20);
            this.txtNumberOfBaySidewalls.TabIndex = 33;
            this.txtNumberOfBaySidewalls.TabStop = false;
            this.txtNumberOfBaySidewalls.Text = "0";
            // 
            // txtWalkwayWidth
            // 
            this.txtWalkwayWidth.Location = new System.Drawing.Point(952, 78);
            this.txtWalkwayWidth.Mask = "F2";
            this.txtWalkwayWidth.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtWalkwayWidth.Name = "txtWalkwayWidth";
            this.txtWalkwayWidth.Size = new System.Drawing.Size(59, 20);
            this.txtWalkwayWidth.TabIndex = 45;
            this.txtWalkwayWidth.TabStop = false;
            this.txtWalkwayWidth.Text = "0.00";
            this.txtWalkwayWidth.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtBayWidth
            // 
            this.txtBayWidth.Location = new System.Drawing.Point(487, 142);
            this.txtBayWidth.Mask = "F2";
            this.txtBayWidth.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtBayWidth.Name = "txtBayWidth";
            this.txtBayWidth.Size = new System.Drawing.Size(59, 20);
            this.txtBayWidth.TabIndex = 31;
            this.txtBayWidth.TabStop = false;
            this.txtBayWidth.Text = "0.00";
            this.txtBayWidth.TextChanged += new System.EventHandler(this.CalculateHoseSize);
            this.txtBayWidth.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtBottomCordHeight
            // 
            this.txtBottomCordHeight.Location = new System.Drawing.Point(707, 239);
            this.txtBottomCordHeight.Mask = "F2";
            this.txtBottomCordHeight.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtBottomCordHeight.Name = "txtBottomCordHeight";
            this.txtBottomCordHeight.Size = new System.Drawing.Size(59, 20);
            this.txtBottomCordHeight.TabIndex = 55;
            this.txtBottomCordHeight.TabStop = false;
            this.txtBottomCordHeight.Text = "0.00";
            this.txtBottomCordHeight.TextChanged += new System.EventHandler(this.CalculateRailDropdownAssemblyLengthInInches);
            this.txtBottomCordHeight.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtTrussSpacing
            // 
            this.txtTrussSpacing.Location = new System.Drawing.Point(952, 45);
            this.txtTrussSpacing.Mask = "F2";
            this.txtTrussSpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtTrussSpacing.Name = "txtTrussSpacing";
            this.txtTrussSpacing.Size = new System.Drawing.Size(59, 20);
            this.txtTrussSpacing.TabIndex = 43;
            this.txtTrussSpacing.TabStop = false;
            this.txtTrussSpacing.Tag = "Echo,SingleRail,DoubleRail,CWFDoubleRail,Tower,Navigator,GroundRunner,SkyRail";
            this.txtTrussSpacing.Text = "0.00";
            this.txtTrussSpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtGrowingSurfaceHeight
            // 
            this.txtGrowingSurfaceHeight.Location = new System.Drawing.Point(487, 211);
            this.txtGrowingSurfaceHeight.Mask = "F2";
            this.txtGrowingSurfaceHeight.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtGrowingSurfaceHeight.Name = "txtGrowingSurfaceHeight";
            this.txtGrowingSurfaceHeight.Size = new System.Drawing.Size(59, 20);
            this.txtGrowingSurfaceHeight.TabIndex = 35;
            this.txtGrowingSurfaceHeight.TabStop = false;
            this.txtGrowingSurfaceHeight.Text = "0.00";
            this.txtGrowingSurfaceHeight.TextChanged += new System.EventHandler(this.CalculateRailDropdownAssemblyLengthInInches);
            this.txtGrowingSurfaceHeight.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtPostSpacing
            // 
            this.txtPostSpacing.Location = new System.Drawing.Point(487, 246);
            this.txtPostSpacing.Mask = "F2";
            this.txtPostSpacing.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtPostSpacing.Name = "txtPostSpacing";
            this.txtPostSpacing.Size = new System.Drawing.Size(59, 20);
            this.txtPostSpacing.TabIndex = 37;
            this.txtPostSpacing.TabStop = false;
            this.txtPostSpacing.Tag = "Navigator";
            this.txtPostSpacing.Text = "0.00";
            this.txtPostSpacing.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtSpraybarSize
            // 
            this.txtSpraybarSize.Location = new System.Drawing.Point(707, 110);
            this.txtSpraybarSize.Mask = "F2";
            this.txtSpraybarSize.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtSpraybarSize.Name = "txtSpraybarSize";
            this.txtSpraybarSize.Size = new System.Drawing.Size(59, 20);
            this.txtSpraybarSize.TabIndex = 47;
            this.txtSpraybarSize.TabStop = false;
            this.txtSpraybarSize.Text = "0.00";
            this.txtSpraybarSize.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // txtHoseSize
            // 
            this.txtHoseSize.Location = new System.Drawing.Point(487, 176);
            this.txtHoseSize.Mask = "F2";
            this.txtHoseSize.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtHoseSize.Name = "txtHoseSize";
            this.txtHoseSize.Size = new System.Drawing.Size(61, 20);
            this.txtHoseSize.TabIndex = 49;
            this.txtHoseSize.TabStop = false;
            this.txtHoseSize.Text = "0.00";
            this.txtHoseSize.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // rdSQCordType
            // 
            this.rdSQCordType.AutoSize = true;
            this.rdSQCordType.Checked = true;
            this.rdSQCordType.Location = new System.Drawing.Point(5, 7);
            this.rdSQCordType.Name = "rdSQCordType";
            this.rdSQCordType.Size = new System.Drawing.Size(40, 17);
            this.rdSQCordType.TabIndex = 0;
            this.rdSQCordType.TabStop = true;
            this.rdSQCordType.Tag = "Echo,SingleRail,DoubleRail,CWFDoubleRail,Tower,Navigator,GroundRunner,SkyRail";
            this.rdSQCordType.Text = "SQ";
            this.rdSQCordType.UseVisualStyleBackColor = true;
            this.rdSQCordType.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdRDCordType
            // 
            this.rdRDCordType.AutoSize = true;
            this.rdRDCordType.Location = new System.Drawing.Point(50, 7);
            this.rdRDCordType.Name = "rdRDCordType";
            this.rdRDCordType.Size = new System.Drawing.Size(41, 17);
            this.rdRDCordType.TabIndex = 1;
            this.rdRDCordType.Tag = "Echo,SingleRail,DoubleRail,CWFDoubleRail,Tower,Navigator,GroundRunner,SkyRail";
            this.rdRDCordType.Text = "RD";
            this.rdRDCordType.UseVisualStyleBackColor = true;
            this.rdRDCordType.CheckedChanged += new System.EventHandler(this.ValidateRadioOptionSelected);
            this.rdRDCordType.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // pnlBottomCordType
            // 
            this.pnlBottomCordType.Controls.Add(this.rdRDCordType);
            this.pnlBottomCordType.Controls.Add(this.rdSQCordType);
            this.pnlBottomCordType.Location = new System.Drawing.Point(707, 169);
            this.pnlBottomCordType.Name = "pnlBottomCordType";
            this.pnlBottomCordType.Size = new System.Drawing.Size(92, 31);
            this.pnlBottomCordType.TabIndex = 51;
            // 
            // greaterThanZeroErrorProvider
            // 
            this.greaterThanZeroErrorProvider.BlinkRate = 0;
            this.greaterThanZeroErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.greaterThanZeroErrorProvider.ContainerControl = this;
            // 
            // dropdownValueErrorProvider
            // 
            this.dropdownValueErrorProvider.BlinkRate = 0;
            this.dropdownValueErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.dropdownValueErrorProvider.ContainerControl = this;
            // 
            // radioButtonsErrorProvider
            // 
            this.radioButtonsErrorProvider.BlinkRate = 0;
            this.radioButtonsErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.radioButtonsErrorProvider.ContainerControl = this;
            // 
            // btnCreateQuote
            // 
            this.btnCreateQuote.CausesValidation = false;
            this.btnCreateQuote.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateQuote.Location = new System.Drawing.Point(1228, 85);
            this.btnCreateQuote.Name = "btnCreateQuote";
            this.btnCreateQuote.Size = new System.Drawing.Size(176, 32);
            this.btnCreateQuote.TabIndex = 58;
            this.btnCreateQuote.Text = "Create Quote";
            this.btnCreateQuote.UseVisualStyleBackColor = true;
            this.btnCreateQuote.Click += new System.EventHandler(this.btnCreateQuote_Click);
            // 
            // emptyTextErrorProvider
            // 
            this.emptyTextErrorProvider.BlinkRate = 0;
            this.emptyTextErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.emptyTextErrorProvider.ContainerControl = this;
            // 
            // lstDetailQuotes
            // 
            this.lstDetailQuotes.DisplayMember = "SystemNameForList";
            this.lstDetailQuotes.FormattingEnabled = true;
            this.lstDetailQuotes.HorizontalScrollbar = true;
            this.lstDetailQuotes.Location = new System.Drawing.Point(1034, 85);
            this.lstDetailQuotes.Name = "lstDetailQuotes";
            this.lstDetailQuotes.Size = new System.Drawing.Size(176, 212);
            this.lstDetailQuotes.TabIndex = 62;
            this.lstDetailQuotes.Click += new System.EventHandler(this.lstDetailQuotes_Click);
            this.lstDetailQuotes.SelectedIndexChanged += new System.EventHandler(this.lstDetailQuotes_SelectedIndexChanged);
            this.lstDetailQuotes.DoubleClick += new System.EventHandler(this.lstDetailQuotes_DoubleClick);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(16, 318);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(103, 13);
            this.label37.TabIndex = 16;
            this.label37.Text = "Shipping Addresses:";
            // 
            // ddlShipAddresses
            // 
            this.ddlShipAddresses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlShipAddresses.DropDownWidth = 300;
            this.ddlShipAddresses.FormattingEnabled = true;
            this.ddlShipAddresses.Location = new System.Drawing.Point(137, 315);
            this.ddlShipAddresses.Name = "ddlShipAddresses";
            this.ddlShipAddresses.Size = new System.Drawing.Size(181, 21);
            this.ddlShipAddresses.TabIndex = 17;
            this.ddlShipAddresses.SelectionChangeCommitted += new System.EventHandler(this.ddlShipAddresses_SelectionChangeCommitted);
            // 
            // mainMenu
            // 
            this.mainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnNewQuote,
            this.mnRefreshLookups,
            this.mnSearchQuotes,
            this.mnManageLookups,
            this.mnManageComponents,
            this.mnManageEmails});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1404, 24);
            this.mainMenu.TabIndex = 0;
            // 
            // mnNewQuote
            // 
            this.mnNewQuote.Name = "mnNewQuote";
            this.mnNewQuote.Size = new System.Drawing.Size(73, 20);
            this.mnNewQuote.Text = "New Quote";
            this.mnNewQuote.Click += new System.EventHandler(this.mnNewQuote_Click);
            // 
            // mnRefreshLookups
            // 
            this.mnRefreshLookups.Name = "mnRefreshLookups";
            this.mnRefreshLookups.Size = new System.Drawing.Size(99, 20);
            this.mnRefreshLookups.Text = "Refresh Lookups";
            this.mnRefreshLookups.Visible = false;
            this.mnRefreshLookups.Click += new System.EventHandler(this.mnRefreshLookups_Click);
            // 
            // mnSearchQuotes
            // 
            this.mnSearchQuotes.Name = "mnSearchQuotes";
            this.mnSearchQuotes.Size = new System.Drawing.Size(90, 20);
            this.mnSearchQuotes.Text = "Search Quotes";
            this.mnSearchQuotes.Click += new System.EventHandler(this.mnSearchQuotes_Click);
            // 
            // mnManageLookups
            // 
            this.mnManageLookups.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnCustomers,
            this.mnSalesReps,
            this.mnBaseCosts,
            this.mnDistributors,
            this.mnGreenHouses});
            this.mnManageLookups.Name = "mnManageLookups";
            this.mnManageLookups.Size = new System.Drawing.Size(99, 20);
            this.mnManageLookups.Text = "Manage Lookups";
            // 
            // mnCustomers
            // 
            this.mnCustomers.Name = "mnCustomers";
            this.mnCustomers.Size = new System.Drawing.Size(141, 22);
            this.mnCustomers.Text = "Customers";
            this.mnCustomers.Click += new System.EventHandler(this.mnCustomers_Click);
            // 
            // mnSalesReps
            // 
            this.mnSalesReps.Name = "mnSalesReps";
            this.mnSalesReps.Size = new System.Drawing.Size(141, 22);
            this.mnSalesReps.Text = "Sales Reps";
            this.mnSalesReps.Click += new System.EventHandler(this.mnSalesReps_Click);
            // 
            // mnBaseCosts
            // 
            this.mnBaseCosts.Name = "mnBaseCosts";
            this.mnBaseCosts.Size = new System.Drawing.Size(141, 22);
            this.mnBaseCosts.Text = "Base Costs";
            this.mnBaseCosts.Click += new System.EventHandler(this.mnBaseCosts_Click);
            // 
            // mnDistributors
            // 
            this.mnDistributors.Name = "mnDistributors";
            this.mnDistributors.Size = new System.Drawing.Size(141, 22);
            this.mnDistributors.Text = "Distributors";
            this.mnDistributors.Click += new System.EventHandler(this.mnDistributors_Click);
            // 
            // mnGreenHouses
            // 
            this.mnGreenHouses.Name = "mnGreenHouses";
            this.mnGreenHouses.Size = new System.Drawing.Size(141, 22);
            this.mnGreenHouses.Text = "Green Houses";
            this.mnGreenHouses.Click += new System.EventHandler(this.mnGreenHouses_Click);
            // 
            // mnManageComponents
            // 
            this.mnManageComponents.Name = "mnManageComponents";
            this.mnManageComponents.Size = new System.Drawing.Size(120, 20);
            this.mnManageComponents.Text = "Manage Components";
            this.mnManageComponents.Click += new System.EventHandler(this.mnManageComponents_Click);
            // 
            // mnManageEmails
            // 
            this.mnManageEmails.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnManageCustomerQuoteEmail});
            this.mnManageEmails.Name = "mnManageEmails";
            this.mnManageEmails.Size = new System.Drawing.Size(89, 20);
            this.mnManageEmails.Text = "Manage Emails";
            // 
            // mnManageCustomerQuoteEmail
            // 
            this.mnManageCustomerQuoteEmail.Name = "mnManageCustomerQuoteEmail";
            this.mnManageCustomerQuoteEmail.Size = new System.Drawing.Size(153, 22);
            this.mnManageCustomerQuoteEmail.Text = "Customer Quote";
            this.mnManageCustomerQuoteEmail.Click += new System.EventHandler(this.mnManageCustomerQuoteEmail_Click);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(40, 45);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(79, 13);
            this.label100.TabIndex = 0;
            this.label100.Text = "Quote Number:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(79, 107);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(40, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Status:";
            // 
            // ddlQuoteStatus
            // 
            this.ddlQuoteStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlQuoteStatus.FormattingEnabled = true;
            this.ddlQuoteStatus.Location = new System.Drawing.Point(137, 104);
            this.ddlQuoteStatus.Name = "ddlQuoteStatus";
            this.ddlQuoteStatus.Size = new System.Drawing.Size(181, 21);
            this.ddlQuoteStatus.TabIndex = 3;
            // 
            // btnAddSystem
            // 
            this.btnAddSystem.CausesValidation = false;
            this.btnAddSystem.Enabled = false;
            this.btnAddSystem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSystem.Location = new System.Drawing.Point(1034, 307);
            this.btnAddSystem.Name = "btnAddSystem";
            this.btnAddSystem.Size = new System.Drawing.Size(176, 32);
            this.btnAddSystem.TabIndex = 60;
            this.btnAddSystem.Text = "Add System";
            this.btnAddSystem.UseVisualStyleBackColor = true;
            this.btnAddSystem.Click += new System.EventHandler(this.btnAddSystem_Click);
            // 
            // btnUpdateQuote
            // 
            this.btnUpdateQuote.CausesValidation = false;
            this.btnUpdateQuote.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateQuote.Location = new System.Drawing.Point(1228, 85);
            this.btnUpdateQuote.Name = "btnUpdateQuote";
            this.btnUpdateQuote.Size = new System.Drawing.Size(175, 32);
            this.btnUpdateQuote.TabIndex = 59;
            this.btnUpdateQuote.Text = "Update Quote";
            this.btnUpdateQuote.UseVisualStyleBackColor = true;
            this.btnUpdateQuote.Visible = false;
            this.btnUpdateQuote.Click += new System.EventHandler(this.btnUpdateQuote_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(62, 137);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(57, 13);
            this.label39.TabIndex = 4;
            this.label39.Text = "Distributor:";
            // 
            // ddlDistributors
            // 
            this.ddlDistributors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ddlDistributors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlDistributors.FormattingEnabled = true;
            this.ddlDistributors.Location = new System.Drawing.Point(137, 134);
            this.ddlDistributors.Name = "ddlDistributors";
            this.ddlDistributors.Size = new System.Drawing.Size(181, 21);
            this.ddlDistributors.TabIndex = 5;
            this.ddlDistributors.Tag = "Echo,SingleRail,DoubleRail,CWFDoubleRail,Tower,Navigator,GroundRunner,SkyRail";
            this.ddlDistributors.SelectionChangeCommitted += new System.EventHandler(this.ddlDistributors_SelectionChangeCommitted);
            this.ddlDistributors.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateDropdownSelectedValue);
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(598, 198);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(100, 35);
            this.label40.TabIndex = 52;
            this.label40.Text = "Bottom Cord Size:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBottomCordSize
            // 
            this.txtBottomCordSize.Location = new System.Drawing.Point(707, 206);
            this.txtBottomCordSize.Margin = new System.Windows.Forms.Padding(2);
            this.txtBottomCordSize.Name = "txtBottomCordSize";
            this.txtBottomCordSize.Size = new System.Drawing.Size(59, 20);
            this.txtBottomCordSize.TabIndex = 53;
            // 
            // btnGenerateQuoteReport
            // 
            this.btnGenerateQuoteReport.CausesValidation = false;
            this.btnGenerateQuoteReport.Enabled = false;
            this.btnGenerateQuoteReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateQuoteReport.Location = new System.Drawing.Point(1227, 128);
            this.btnGenerateQuoteReport.Name = "btnGenerateQuoteReport";
            this.btnGenerateQuoteReport.Size = new System.Drawing.Size(176, 32);
            this.btnGenerateQuoteReport.TabIndex = 61;
            this.btnGenerateQuoteReport.Text = "Create Report";
            this.btnGenerateQuoteReport.UseVisualStyleBackColor = true;
            this.btnGenerateQuoteReport.Click += new System.EventHandler(this.btnGenerateQuoteReport_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(387, 352);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(85, 13);
            this.label46.TabIndex = 22;
            this.label46.Text = "Freight Estimate:";
            // 
            // txtFreightEstimate
            // 
            this.txtFreightEstimate.Location = new System.Drawing.Point(487, 349);
            this.txtFreightEstimate.Name = "txtFreightEstimate";
            this.txtFreightEstimate.Size = new System.Drawing.Size(59, 20);
            this.txtFreightEstimate.TabIndex = 23;
            // 
            // txtQuoteNumber
            // 
            this.txtQuoteNumber.Location = new System.Drawing.Point(137, 43);
            this.txtQuoteNumber.Name = "txtQuoteNumber";
            this.txtQuoteNumber.Size = new System.Drawing.Size(181, 20);
            this.txtQuoteNumber.TabIndex = 1;
            // 
            // btnRemoveSystem
            // 
            this.btnRemoveSystem.CausesValidation = false;
            this.btnRemoveSystem.Enabled = false;
            this.btnRemoveSystem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveSystem.Location = new System.Drawing.Point(1034, 352);
            this.btnRemoveSystem.Name = "btnRemoveSystem";
            this.btnRemoveSystem.Size = new System.Drawing.Size(176, 32);
            this.btnRemoveSystem.TabIndex = 63;
            this.btnRemoveSystem.Text = "Remove System";
            this.btnRemoveSystem.UseVisualStyleBackColor = true;
            this.btnRemoveSystem.Click += new System.EventHandler(this.btnRemoveSystem_Click);
            // 
            // txtPostDimensions
            // 
            this.txtPostDimensions.Location = new System.Drawing.Point(487, 276);
            this.txtPostDimensions.Name = "txtPostDimensions";
            this.txtPostDimensions.Size = new System.Drawing.Size(59, 20);
            this.txtPostDimensions.TabIndex = 65;
            // 
            // txtLocation
            // 
            this.txtLocation.Location = new System.Drawing.Point(707, 276);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(92, 20);
            this.txtLocation.TabIndex = 67;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(647, 278);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(51, 13);
            this.label134.TabIndex = 66;
            this.label134.Text = "Location:";
            // 
            // btnSendEmail
            // 
            this.btnSendEmail.CausesValidation = false;
            this.btnSendEmail.Enabled = false;
            this.btnSendEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendEmail.Location = new System.Drawing.Point(1227, 175);
            this.btnSendEmail.Name = "btnSendEmail";
            this.btnSendEmail.Size = new System.Drawing.Size(175, 32);
            this.btnSendEmail.TabIndex = 68;
            this.btnSendEmail.Text = "Send Email";
            this.btnSendEmail.UseVisualStyleBackColor = true;
            this.btnSendEmail.Click += new System.EventHandler(this.btnSendEmail_Click);
            // 
            // btnGenerateSalesOrderForm
            // 
            this.btnGenerateSalesOrderForm.CausesValidation = false;
            this.btnGenerateSalesOrderForm.Enabled = false;
            this.btnGenerateSalesOrderForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateSalesOrderForm.Location = new System.Drawing.Point(1034, 401);
            this.btnGenerateSalesOrderForm.Name = "btnGenerateSalesOrderForm";
            this.btnGenerateSalesOrderForm.Size = new System.Drawing.Size(175, 32);
            this.btnGenerateSalesOrderForm.TabIndex = 69;
            this.btnGenerateSalesOrderForm.Text = "Create Sales Form";
            this.btnGenerateSalesOrderForm.UseVisualStyleBackColor = true;
            this.btnGenerateSalesOrderForm.Click += new System.EventHandler(this.btnGenerateSalesOrderForm_Click);
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(636, 50);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(62, 13);
            this.label140.TabIndex = 70;
            this.label140.Text = "Order Date:";
            // 
            // dtOrderConfirmed
            // 
            this.dtOrderConfirmed.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtOrderConfirmed.Location = new System.Drawing.Point(707, 43);
            this.dtOrderConfirmed.Name = "dtOrderConfirmed";
            this.dtOrderConfirmed.Size = new System.Drawing.Size(91, 20);
            this.dtOrderConfirmed.TabIndex = 71;
            this.dtOrderConfirmed.Value = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            this.dtOrderConfirmed.CloseUp += new System.EventHandler(this.CalculateDueDate);
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.Location = new System.Drawing.Point(1080, 56);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(82, 20);
            this.label141.TabIndex = 72;
            this.label141.Text = "Systems:";
            // 
            // txtClearanceWidth
            // 
            this.txtClearanceWidth.Location = new System.Drawing.Point(952, 113);
            this.txtClearanceWidth.Mask = "F2";
            this.txtClearanceWidth.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtClearanceWidth.Name = "txtClearanceWidth";
            this.txtClearanceWidth.Size = new System.Drawing.Size(59, 20);
            this.txtClearanceWidth.TabIndex = 74;
            this.txtClearanceWidth.TabStop = false;
            this.txtClearanceWidth.Text = "0.00";
            // 
            // label142
            // 
            this.label142.Location = new System.Drawing.Point(832, 105);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(109, 35);
            this.label142.TabIndex = 73;
            this.label142.Text = "Clearance Width (IN):";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtInstallationEstimate
            // 
            this.txtInstallationEstimate.Location = new System.Drawing.Point(487, 378);
            this.txtInstallationEstimate.Name = "txtInstallationEstimate";
            this.txtInstallationEstimate.Size = new System.Drawing.Size(59, 20);
            this.txtInstallationEstimate.TabIndex = 76;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(369, 381);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(103, 13);
            this.label161.TabIndex = 75;
            this.label161.Text = "Installation Estimate:";
            // 
            // txtPurchaseOrder
            // 
            this.txtPurchaseOrder.Location = new System.Drawing.Point(137, 445);
            this.txtPurchaseOrder.Name = "txtPurchaseOrder";
            this.txtPurchaseOrder.Size = new System.Drawing.Size(181, 20);
            this.txtPurchaseOrder.TabIndex = 78;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(35, 448);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(84, 13);
            this.label24.TabIndex = 77;
            this.label24.Text = "Purchase Order:";
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(613, 404);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(85, 13);
            this.label205.TabIndex = 81;
            this.label205.Text = "Customer Notes:";
            // 
            // txtCustomerNote
            // 
            this.txtCustomerNote.Location = new System.Drawing.Point(707, 401);
            this.txtCustomerNote.Multiline = true;
            this.txtCustomerNote.Name = "txtCustomerNote";
            this.txtCustomerNote.Size = new System.Drawing.Size(305, 80);
            this.txtCustomerNote.TabIndex = 82;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label206.Location = new System.Drawing.Point(419, 319);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(94, 20);
            this.label206.TabIndex = 83;
            this.label206.Text = "Estimates:";
            // 
            // txtDiscount
            // 
            this.txtDiscount.Location = new System.Drawing.Point(137, 478);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(55, 20);
            this.txtDiscount.TabIndex = 87;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(67, 481);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(52, 13);
            this.label207.TabIndex = 86;
            this.label207.Text = "Discount:";
            // 
            // txtSteelEstimate
            // 
            this.txtSteelEstimate.Location = new System.Drawing.Point(487, 409);
            this.txtSteelEstimate.Name = "txtSteelEstimate";
            this.txtSteelEstimate.Size = new System.Drawing.Size(59, 20);
            this.txtSteelEstimate.TabIndex = 85;
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(395, 412);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(77, 13);
            this.label208.TabIndex = 84;
            this.label208.Text = "Steel Estimate:";
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(642, 79);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(56, 13);
            this.label204.TabIndex = 88;
            this.label204.Text = "Due Date:";
            // 
            // txtDueDate
            // 
            this.txtDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDueDate.Location = new System.Drawing.Point(707, 76);
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.Size = new System.Drawing.Size(92, 20);
            this.txtDueDate.TabIndex = 89;
            this.txtDueDate.Value = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            // 
            // txtContactName
            // 
            this.txtContactName.Location = new System.Drawing.Point(137, 195);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(181, 20);
            this.txtContactName.TabIndex = 90;
            // 
            // txtContactPhone
            // 
            this.txtContactPhone.Location = new System.Drawing.Point(137, 225);
            this.txtContactPhone.Name = "txtContactPhone";
            this.txtContactPhone.Size = new System.Drawing.Size(181, 20);
            this.txtContactPhone.TabIndex = 91;
            // 
            // txtContactFax
            // 
            this.txtContactFax.Location = new System.Drawing.Point(137, 255);
            this.txtContactFax.Name = "txtContactFax";
            this.txtContactFax.Size = new System.Drawing.Size(181, 20);
            this.txtContactFax.TabIndex = 92;
            // 
            // txtContactEmail
            // 
            this.txtContactEmail.Location = new System.Drawing.Point(137, 285);
            this.txtContactEmail.Name = "txtContactEmail";
            this.txtContactEmail.Size = new System.Drawing.Size(181, 20);
            this.txtContactEmail.TabIndex = 93;
            // 
            // txtShipAddress
            // 
            this.txtShipAddress.Location = new System.Drawing.Point(137, 347);
            this.txtShipAddress.Name = "txtShipAddress";
            this.txtShipAddress.Size = new System.Drawing.Size(181, 20);
            this.txtShipAddress.TabIndex = 95;
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(47, 350);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(72, 13);
            this.label211.TabIndex = 94;
            this.label211.Text = "Ship Address:";
            // 
            // btnAddCustomItems
            // 
            this.btnAddCustomItems.CausesValidation = false;
            this.btnAddCustomItems.Enabled = false;
            this.btnAddCustomItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCustomItems.Location = new System.Drawing.Point(1227, 222);
            this.btnAddCustomItems.Name = "btnAddCustomItems";
            this.btnAddCustomItems.Size = new System.Drawing.Size(176, 32);
            this.btnAddCustomItems.TabIndex = 96;
            this.btnAddCustomItems.Text = "Custom Items";
            this.btnAddCustomItems.UseVisualStyleBackColor = true;
            this.btnAddCustomItems.Click += new System.EventHandler(this.btnAddCustomItems_Click);
            // 
            // btnSteelMaterials
            // 
            this.btnSteelMaterials.CausesValidation = false;
            this.btnSteelMaterials.Enabled = false;
            this.btnSteelMaterials.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSteelMaterials.Location = new System.Drawing.Point(1034, 448);
            this.btnSteelMaterials.Name = "btnSteelMaterials";
            this.btnSteelMaterials.Size = new System.Drawing.Size(176, 32);
            this.btnSteelMaterials.TabIndex = 97;
            this.btnSteelMaterials.Text = "Steel Materials";
            this.btnSteelMaterials.UseVisualStyleBackColor = true;
            this.btnSteelMaterials.Click += new System.EventHandler(this.btnSteelMaterials_Click);
            // 
            // txtLeadTime
            // 
            this.txtLeadTime.Location = new System.Drawing.Point(137, 416);
            this.txtLeadTime.Mask = "G";
            this.txtLeadTime.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtLeadTime.Name = "txtLeadTime";
            this.txtLeadTime.Size = new System.Drawing.Size(181, 20);
            this.txtLeadTime.TabIndex = 98;
            this.txtLeadTime.TabStop = false;
            this.txtLeadTime.Tag = "Echo,SingleRail,DoubleRail,CWFDoubleRail,Tower,Navigator,GroundRunner,SkyRail";
            this.txtLeadTime.Text = "0";
            this.txtLeadTime.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateGreaterThanZero);
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(53, 75);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(66, 13);
            this.label212.TabIndex = 99;
            this.label212.Text = "Quote Total:";
            // 
            // txtQuoteTotal
            // 
            this.txtQuoteTotal.Location = new System.Drawing.Point(137, 72);
            this.txtQuoteTotal.Name = "txtQuoteTotal";
            this.txtQuoteTotal.ReadOnly = true;
            this.txtQuoteTotal.Size = new System.Drawing.Size(181, 20);
            this.txtQuoteTotal.TabIndex = 100;
            // 
            // txtPriceLevel
            // 
            this.txtPriceLevel.Location = new System.Drawing.Point(274, 480);
            this.txtPriceLevel.Mask = "F2";
            this.txtPriceLevel.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtPriceLevel.Name = "txtPriceLevel";
            this.txtPriceLevel.Size = new System.Drawing.Size(44, 20);
            this.txtPriceLevel.TabIndex = 102;
            this.txtPriceLevel.TabStop = false;
            this.txtPriceLevel.Tag = "Navigator";
            this.txtPriceLevel.Text = "0.00";
            // 
            // label220
            // 
            this.label220.Location = new System.Drawing.Point(211, 471);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(57, 35);
            this.label220.TabIndex = 101;
            this.label220.Text = "Price Lvl.:";
            this.label220.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // QuoteRequestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1284, 785);
            this.Controls.Add(this.txtPriceLevel);
            this.Controls.Add(this.label220);
            this.Controls.Add(this.txtQuoteTotal);
            this.Controls.Add(this.label212);
            this.Controls.Add(this.txtLeadTime);
            this.Controls.Add(this.btnSteelMaterials);
            this.Controls.Add(this.btnAddCustomItems);
            this.Controls.Add(this.txtShipAddress);
            this.Controls.Add(this.label211);
            this.Controls.Add(this.txtContactEmail);
            this.Controls.Add(this.txtContactFax);
            this.Controls.Add(this.txtContactPhone);
            this.Controls.Add(this.txtContactName);
            this.Controls.Add(this.label204);
            this.Controls.Add(this.txtDueDate);
            this.Controls.Add(this.txtDiscount);
            this.Controls.Add(this.label207);
            this.Controls.Add(this.txtSteelEstimate);
            this.Controls.Add(this.label208);
            this.Controls.Add(this.label206);
            this.Controls.Add(this.label205);
            this.Controls.Add(this.txtCustomerNote);
            this.Controls.Add(this.txtPurchaseOrder);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txtInstallationEstimate);
            this.Controls.Add(this.label161);
            this.Controls.Add(this.txtClearanceWidth);
            this.Controls.Add(this.label142);
            this.Controls.Add(this.label141);
            this.Controls.Add(this.label140);
            this.Controls.Add(this.dtOrderConfirmed);
            this.Controls.Add(this.btnGenerateSalesOrderForm);
            this.Controls.Add(this.btnSendEmail);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.label134);
            this.Controls.Add(this.txtPostDimensions);
            this.Controls.Add(this.btnRemoveSystem);
            this.Controls.Add(this.txtQuoteNumber);
            this.Controls.Add(this.txtFreightEstimate);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.btnGenerateQuoteReport);
            this.Controls.Add(this.txtBottomCordSize);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.ddlDistributors);
            this.Controls.Add(this.btnUpdateQuote);
            this.Controls.Add(this.btnAddSystem);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.ddlQuoteStatus);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.ddlShipAddresses);
            this.Controls.Add(this.lstDetailQuotes);
            this.Controls.Add(this.btnCreateQuote);
            this.Controls.Add(this.tbBooms);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtAdditionalInformation);
            this.Controls.Add(this.pnlBottomCordType);
            this.Controls.Add(this.txtHoseSize);
            this.Controls.Add(this.txtSpraybarSize);
            this.Controls.Add(this.txtPostSpacing);
            this.Controls.Add(this.txtGrowingSurfaceHeight);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtTrussSpacing);
            this.Controls.Add(this.txtBottomCordHeight);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtBayWidth);
            this.Controls.Add(this.txtWalkwayWidth);
            this.Controls.Add(this.txtNumberOfBaySidewalls);
            this.Controls.Add(this.txtBayLength);
            this.Controls.Add(this.txtNumberOfBays);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ddlGreenhouseType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ddlSalesReps);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddlCustomers);
            this.Controls.Add(this.mainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenu;
            this.Name = "QuoteRequestForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quotation System";
            this.Load += new System.EventHandler(this.QuoteRequestForm_Load);
            this.tbBooms.ResumeLayout(false);
            this.tpEcho.ResumeLayout(false);
            this.tpEcho.PerformLayout();
            this.pnlEchoWaterSolenoidControl.ResumeLayout(false);
            this.pnlEchoWaterSolenoidControl.PerformLayout();
            this.pnlEchoEV.ResumeLayout(false);
            this.pnlEchoEV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEchoQuantityOfSystems)).EndInit();
            this.pnlEchoRedheadBreakerUpgrade.ResumeLayout(false);
            this.pnlEchoRedheadBreakerUpgrade.PerformLayout();
            this.pnlEchoRemotePullChainSwitch.ResumeLayout(false);
            this.pnlEchoRemotePullChainSwitch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEchoExtensionHangerLength)).EndInit();
            this.pnlEchoLockline.ResumeLayout(false);
            this.pnlEchoLockline.PerformLayout();
            this.pnlEchoAMIADFilterAssembly.ResumeLayout(false);
            this.pnlEchoAMIADFilterAssembly.PerformLayout();
            this.pnlEchoCenterBayWaterStation.ResumeLayout(false);
            this.pnlEchoCenterBayWaterStation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEchoBasketSpacing)).EndInit();
            this.tpSingleRailBoom.ResumeLayout(false);
            this.tpSingleRailBoom.PerformLayout();
            this.pnlSRBAutoInjector.ResumeLayout(false);
            this.pnlSRBAutoInjector.PerformLayout();
            this.pnlSRBWaterSolenoidControl.ResumeLayout(false);
            this.pnlSRBWaterSolenoidControl.PerformLayout();
            this.pnlSRBEV.ResumeLayout(false);
            this.pnlSRBEV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRBNumberOfRows)).EndInit();
            this.pnlSRBAMIADFilderAssy.ResumeLayout(false);
            this.pnlSRBAMIADFilderAssy.PerformLayout();
            this.pnlSRBTripleTurretBody.ResumeLayout(false);
            this.pnlSRBTripleTurretBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRBQuantityOfSystems)).EndInit();
            this.pnlSRBMountedInjector.ResumeLayout(false);
            this.pnlSRBMountedInjector.PerformLayout();
            this.pnlSRBPressureRegulator.ResumeLayout(false);
            this.pnlSRBPressureRegulator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRBSpraybodySpacing)).EndInit();
            this.pnlSRBSweep90Degree.ResumeLayout(false);
            this.pnlSRBSweep90Degree.PerformLayout();
            this.pnlSRBTripleWaterLocklineUpgrade.ResumeLayout(false);
            this.pnlSRBTripleWaterLocklineUpgrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRBHoseLoopHeight)).EndInit();
            this.tpDoubleRailBoom.ResumeLayout(false);
            this.tpDoubleRailBoom.PerformLayout();
            this.pnlDRBAutoInjector.ResumeLayout(false);
            this.pnlDRBAutoInjector.PerformLayout();
            this.pnlDRBWaterSolenoidControl.ResumeLayout(false);
            this.pnlDRBWaterSolenoidControl.PerformLayout();
            this.pnlDRBEV.ResumeLayout(false);
            this.pnlDRBEV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBNumberOfRows)).EndInit();
            this.pnlDRBWirelessWalkSwitch.ResumeLayout(false);
            this.pnlDRBWirelessWalkSwitch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBRailDropDownAssy)).EndInit();
            this.pnlDRB1_1_4thHoseUpgrade.ResumeLayout(false);
            this.pnlDRB1_1_4thHoseUpgrade.PerformLayout();
            this.pnlDRBTripleTurretBody.ResumeLayout(false);
            this.pnlDRBTripleTurretBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBQuantityOfSystems)).EndInit();
            this.pnlDRBMountedInjector.ResumeLayout(false);
            this.pnlDRBMountedInjector.PerformLayout();
            this.pnlDRBPressureRegulator.ResumeLayout(false);
            this.pnlDRBPressureRegulator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBSpraybodySpacing)).EndInit();
            this.pnlDRBSweep90.ResumeLayout(false);
            this.pnlDRBSweep90.PerformLayout();
            this.pnlDRBTripleWaterLocklineUpgrade.ResumeLayout(false);
            this.pnlDRBTripleWaterLocklineUpgrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDRBHoseLoopHeight)).EndInit();
            this.tpCWFDoubleRailBoom.ResumeLayout(false);
            this.tpCWFDoubleRailBoom.PerformLayout();
            this.pnlCWFDRBWaterFeedSystem.ResumeLayout(false);
            this.pnlCWFDRBWaterFeedSystem.PerformLayout();
            this.pnlCWFDRB1_1_4thHoseUpgrade.ResumeLayout(false);
            this.pnlCWFDRB1_1_4thHoseUpgrade.PerformLayout();
            this.pnlCWFDRBAutoInjector.ResumeLayout(false);
            this.pnlCWFDRBAutoInjector.PerformLayout();
            this.pnlCWFDRBWaterSolenoidControl.ResumeLayout(false);
            this.pnlCWFDRBWaterSolenoidControl.PerformLayout();
            this.pnlCWFDRBEV.ResumeLayout(false);
            this.pnlCWFDRBEV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCWFDRBNumberOfRows)).EndInit();
            this.pnlCWFDRBWirelessWalkSwitch.ResumeLayout(false);
            this.pnlCWFDRBWirelessWalkSwitch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCWFDRBRailDropDownAssy)).EndInit();
            this.pnlCWFDRBTripleTurretBody.ResumeLayout(false);
            this.pnlCWFDRBTripleTurretBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCWFDRBQuantityOfSystems)).EndInit();
            this.pnlCWFDRBMountedInjector.ResumeLayout(false);
            this.pnlCWFDRBMountedInjector.PerformLayout();
            this.pnlCWFDRBPressureRegulator.ResumeLayout(false);
            this.pnlCWFDRBPressureRegulator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCWFDRBSpraybodySpacing)).EndInit();
            this.pnlCWFDRBTripleWaterLocklineUpgrade.ResumeLayout(false);
            this.pnlCWFDRBTripleWaterLocklineUpgrade.PerformLayout();
            this.tpControlTowerBoom.ResumeLayout(false);
            this.tpControlTowerBoom.PerformLayout();
            this.pnlCTAutoInjector.ResumeLayout(false);
            this.pnlCTAutoInjector.PerformLayout();
            this.pnlCTWaterSolenoidControl.ResumeLayout(false);
            this.pnlCTWaterSolenoidControl.PerformLayout();
            this.pnlCTEV.ResumeLayout(false);
            this.pnlCTEV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTNumberOfRows)).EndInit();
            this.pnlCTWaterFeedSystem.ResumeLayout(false);
            this.pnlCTWaterFeedSystem.PerformLayout();
            this.pnlCTGallonTank.ResumeLayout(false);
            this.pnlCTGallonTank.PerformLayout();
            this.pnlCTConveyor.ResumeLayout(false);
            this.pnlCTConveyor.PerformLayout();
            this.pnlCTDualMotorNoAxle.ResumeLayout(false);
            this.pnlCTDualMotorNoAxle.PerformLayout();
            this.pnlCTDiamondSweep.ResumeLayout(false);
            this.pnlCTDiamondSweep.PerformLayout();
            this.pnlCT1_1_4thHoseUpgrade.ResumeLayout(false);
            this.pnlCT1_1_4thHoseUpgrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTHoseLoopHeight)).EndInit();
            this.pnlCTWirelessWalkSwitch.ResumeLayout(false);
            this.pnlCTWirelessWalkSwitch.PerformLayout();
            this.pnlCTTripleTurretBody.ResumeLayout(false);
            this.pnlCTTripleTurretBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTQuantityOfSystems)).EndInit();
            this.pnlCTMountedInjector.ResumeLayout(false);
            this.pnlCTMountedInjector.PerformLayout();
            this.pnlCTPressureRegulator.ResumeLayout(false);
            this.pnlCTPressureRegulator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTSpraybodySpacing)).EndInit();
            this.pnlCTTripleWaterLocklineUpgrade.ResumeLayout(false);
            this.pnlCTTripleWaterLocklineUpgrade.PerformLayout();
            this.tpNavigatorBoom.ResumeLayout(false);
            this.tpNavigatorBoom.PerformLayout();
            this.pnlNavAutoInjector.ResumeLayout(false);
            this.pnlNavAutoInjector.PerformLayout();
            this.pnlNavWaterSolenoidControl.ResumeLayout(false);
            this.pnlNavWaterSolenoidControl.PerformLayout();
            this.pnlNavEV.ResumeLayout(false);
            this.pnlNavEV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNavNumberOfRows)).EndInit();
            this.pnlNavWaterFeedSystem.ResumeLayout(false);
            this.pnlNavWaterFeedSystem.PerformLayout();
            this.pnlNavGallonTank.ResumeLayout(false);
            this.pnlNavGallonTank.PerformLayout();
            this.pnlNavSweep90.ResumeLayout(false);
            this.pnlNavSweep90.PerformLayout();
            this.pnlNav1_1_4thHoseUpgrade.ResumeLayout(false);
            this.pnlNav1_1_4thHoseUpgrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNavHoseLoopHeight)).EndInit();
            this.pnlNavWirelessWalkSwitch.ResumeLayout(false);
            this.pnlNavWirelessWalkSwitch.PerformLayout();
            this.pnlNavTripleTurretBody.ResumeLayout(false);
            this.pnlNavTripleTurretBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNavQuantityOfSystems)).EndInit();
            this.pnlNavMountedInjector.ResumeLayout(false);
            this.pnlNavMountedInjector.PerformLayout();
            this.pnlNavPressureRegulator.ResumeLayout(false);
            this.pnlNavPressureRegulator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNavSpraybodySpacing)).EndInit();
            this.pnlNavTripleWaterLocklineUpgrade.ResumeLayout(false);
            this.pnlNavTripleWaterLocklineUpgrade.PerformLayout();
            this.tpGroundRunnerBoom.ResumeLayout(false);
            this.tpGroundRunnerBoom.PerformLayout();
            this.pnlGRWaterFeedSystem.ResumeLayout(false);
            this.pnlGRWaterFeedSystem.PerformLayout();
            this.pnlGRAutoInjector.ResumeLayout(false);
            this.pnlGRAutoInjector.PerformLayout();
            this.pnlGRWaterSolenoidControl.ResumeLayout(false);
            this.pnlGRWaterSolenoidControl.PerformLayout();
            this.pnlGREV.ResumeLayout(false);
            this.pnlGREV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRNumberOfRows)).EndInit();
            this.pnlGRWirelessWalkSwitch.ResumeLayout(false);
            this.pnlGRWirelessWalkSwitch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRRailDropDownAssy)).EndInit();
            this.pnlGR1_1_4thHoseUpgrade.ResumeLayout(false);
            this.pnlGR1_1_4thHoseUpgrade.PerformLayout();
            this.pnlGRTripleTurretBody.ResumeLayout(false);
            this.pnlGRTripleTurretBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRQuantityOfSystems)).EndInit();
            this.pnlGRMountedInjector.ResumeLayout(false);
            this.pnlGRMountedInjector.PerformLayout();
            this.pnlGRPressureRegulator.ResumeLayout(false);
            this.pnlGRPressureRegulator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRSpraybodySpacing)).EndInit();
            this.pnlGRSweep90.ResumeLayout(false);
            this.pnlGRSweep90.PerformLayout();
            this.pnlGRTripleWaterLocklineUpgrade.ResumeLayout(false);
            this.pnlGRTripleWaterLocklineUpgrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRHoseLoopHeight)).EndInit();
            this.tpSkyRailBoom.ResumeLayout(false);
            this.tpSkyRailBoom.PerformLayout();
            this.pnlSRWaterFeedSystem.ResumeLayout(false);
            this.pnlSRWaterFeedSystem.PerformLayout();
            this.pnlSRAutoInjector.ResumeLayout(false);
            this.pnlSRAutoInjector.PerformLayout();
            this.pnlSRWaterSolenoidControl.ResumeLayout(false);
            this.pnlSRWaterSolenoidControl.PerformLayout();
            this.pnlSREV.ResumeLayout(false);
            this.pnlSREV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRNumberOfRows)).EndInit();
            this.pnlSRWirelessWalkSwitch.ResumeLayout(false);
            this.pnlSRWirelessWalkSwitch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRRailDropDownAssy)).EndInit();
            this.pnlSR1_1_4thHoseUpgrade.ResumeLayout(false);
            this.pnlSR1_1_4thHoseUpgrade.PerformLayout();
            this.pnlSRTripleTurretBody.ResumeLayout(false);
            this.pnlSRTripleTurretBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRQuantityOfSystems)).EndInit();
            this.pnlSRMountedInjector.ResumeLayout(false);
            this.pnlSRMountedInjector.PerformLayout();
            this.pnlSRPressureRegulator.ResumeLayout(false);
            this.pnlSRPressureRegulator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRSpraybodySpacing)).EndInit();
            this.pnlSRSweep90.ResumeLayout(false);
            this.pnlSRSweep90.PerformLayout();
            this.pnlSRTripleWaterLocklineUpgrade.ResumeLayout(false);
            this.pnlSRTripleWaterLocklineUpgrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSRHoseLoopHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberOfBays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBayLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberOfBaySidewalls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWalkwayWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBayWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBottomCordHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrussSpacing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrowingSurfaceHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostSpacing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpraybarSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoseSize)).EndInit();
            this.pnlBottomCordType.ResumeLayout(false);
            this.pnlBottomCordType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.greaterThanZeroErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropdownValueErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtonsErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptyTextErrorProvider)).EndInit();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtClearanceWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeadTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlCustomers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ddlSalesReps;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ddlGreenhouseType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtAdditionalInformation;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabControl tbBooms;
        private System.Windows.Forms.TabPage tpEcho;
        private System.Windows.Forms.TabPage tpSingleRailBoom;
        private System.Windows.Forms.ComboBox ddlEchoDrumSize;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox ddlEchoControllerType;
        private System.Windows.Forms.Label label28;
        private Telerik.WinControls.UI.RadMaskedEditBox txtEchoBasketSpacing;
        private Telerik.WinControls.UI.RadMaskedEditBox txtNumberOfBays;
        private Telerik.WinControls.UI.RadMaskedEditBox txtBayLength;
        private Telerik.WinControls.UI.RadMaskedEditBox txtNumberOfBaySidewalls;
        private Telerik.WinControls.UI.RadMaskedEditBox txtWalkwayWidth;
        private Telerik.WinControls.UI.RadMaskedEditBox txtBayWidth;
        private Telerik.WinControls.UI.RadMaskedEditBox txtBottomCordHeight;
        private Telerik.WinControls.UI.RadMaskedEditBox txtTrussSpacing;
        private Telerik.WinControls.UI.RadMaskedEditBox txtGrowingSurfaceHeight;
        private Telerik.WinControls.UI.RadMaskedEditBox txtPostSpacing;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSpraybarSize;
        private Telerik.WinControls.UI.RadMaskedEditBox txtHoseSize;
        private System.Windows.Forms.RadioButton rdSQCordType;
        private System.Windows.Forms.RadioButton rdRDCordType;
        private System.Windows.Forms.Panel pnlBottomCordType;
        private System.Windows.Forms.Panel pnlEchoCenterBayWaterStation;
        private System.Windows.Forms.RadioButton rdEchoCenterBayWaterStationNo;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox ddlEchoNumberOfLayers;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel pnlEchoRedheadBreakerUpgrade;
        private System.Windows.Forms.RadioButton rdEchoRedheadBreakerUpgradeNo;
        private System.Windows.Forms.RadioButton rdEchoRedheadBreakerUpgradeYes;
        private System.Windows.Forms.ComboBox ddlEchoPulleyBracketSpacing;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel pnlEchoRemotePullChainSwitch;
        private System.Windows.Forms.RadioButton rdEchoRemotePullChainSwitchNo;
        private System.Windows.Forms.RadioButton rdEchoRemotePullChainSwitchYes;
        private System.Windows.Forms.Label label36;
        private Telerik.WinControls.UI.RadMaskedEditBox txtEchoExtensionHangerLength;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel pnlEchoLockline;
        private System.Windows.Forms.RadioButton rdEchoLocklineNo;
        private System.Windows.Forms.RadioButton rdEchoLocklineYes;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel pnlEchoAMIADFilterAssembly;
        private System.Windows.Forms.RadioButton rdEchoAMIADFilterAssemblyNo;
        private System.Windows.Forms.RadioButton rdEchoAMIADFilterAssemblyYes;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ErrorProvider greaterThanZeroErrorProvider;
        private Telerik.WinControls.UI.RadMaskedEditBox txtEchoQuantityOfSystems;
        private System.Windows.Forms.ErrorProvider dropdownValueErrorProvider;
        private System.Windows.Forms.ErrorProvider radioButtonsErrorProvider;
        private System.Windows.Forms.Button btnCreateQuote;
        private System.Windows.Forms.ErrorProvider emptyTextErrorProvider;
        private System.Windows.Forms.ListBox lstDetailQuotes;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox ddlShipAddresses;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem mnNewQuote;
        private System.Windows.Forms.ToolStripMenuItem mnManageLookups;
        private System.Windows.Forms.ToolStripMenuItem mnCustomers;
        private System.Windows.Forms.ToolStripMenuItem mnSalesReps;
        private System.Windows.Forms.ToolStripMenuItem mnSearchQuotes;
        private System.Windows.Forms.ToolStripMenuItem mnManageComponents;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox ddlQuoteStatus;
        private System.Windows.Forms.Button btnAddSystem;
        private System.Windows.Forms.Button btnUpdateQuote;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox ddlDistributors;
        private System.Windows.Forms.TextBox txtBottomCordSize;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ToolStripMenuItem mnBaseCosts;
        private System.Windows.Forms.Button btnGenerateQuoteReport;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRBQuantityOfSystems;
        private System.Windows.Forms.Panel pnlSRBMountedInjector;
        private System.Windows.Forms.RadioButton rdSRBMountedInjectorNo;
        private System.Windows.Forms.RadioButton rdSRBMountedInjectorYes;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel pnlSRBPressureRegulator;
        private System.Windows.Forms.RadioButton rdSRBPressureRegulatorNo;
        private System.Windows.Forms.RadioButton rdSRBPressureRegulatorYes;
        private System.Windows.Forms.Label label43;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRBSpraybodySpacing;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel pnlSRBSweep90Degree;
        private System.Windows.Forms.RadioButton rdSRBSweep90DegreeNo;
        private System.Windows.Forms.RadioButton rdSRBSweep90DegreeYes;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel pnlSRBTripleWaterLocklineUpgrade;
        private System.Windows.Forms.RadioButton rdSRBTripleWaterLocklineUpgradeNo;
        private System.Windows.Forms.RadioButton rdSRBTripleWaterLocklineUpgradeYes;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox ddlSRBControllerType;
        private System.Windows.Forms.Label label49;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRBHoseLoopHeight;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox ddlSRBSingleDualWater;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Panel pnlSRBTripleTurretBody;
        private System.Windows.Forms.RadioButton rdSRBTripleTurretBodyNo;
        private System.Windows.Forms.RadioButton rdSRBTripleTurretBodyYes;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel pnlSRBAMIADFilderAssy;
        private System.Windows.Forms.RadioButton rdSRBAMIADFilderAssyNo;
        private System.Windows.Forms.RadioButton rdSRBAMIADFilderAssyYes;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtFreightEstimate;
        private System.Windows.Forms.TabPage tpDoubleRailBoom;
        private System.Windows.Forms.Panel pnlDRB1_1_4thHoseUpgrade;
        private System.Windows.Forms.RadioButton rdDRB1_1_4thHoseUpgradeNo;
        private System.Windows.Forms.RadioButton rdDRB1_1_4thHoseUpgradeYes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlDRBTripleTurretBody;
        private System.Windows.Forms.RadioButton rdDRBTripleTurretBodyNo;
        private System.Windows.Forms.RadioButton rdDRBTripleTurretBodyYes;
        private System.Windows.Forms.Label label48;
        private Telerik.WinControls.UI.RadMaskedEditBox txtDRBQuantityOfSystems;
        private System.Windows.Forms.Panel pnlDRBMountedInjector;
        private System.Windows.Forms.RadioButton rdDRBMountedInjectorNo;
        private System.Windows.Forms.RadioButton rdDRBMountedInjectorYes;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel pnlDRBPressureRegulator;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton rdDRBPressureRegulatorNo;
        private System.Windows.Forms.RadioButton rdDRBPressureRegulatorYes;
        private System.Windows.Forms.Label label55;
        private Telerik.WinControls.UI.RadMaskedEditBox txtDRBSpraybodySpacing;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel pnlDRBSweep90;
        private System.Windows.Forms.RadioButton rdDRBSweep90No;
        private System.Windows.Forms.RadioButton rdDRBSweep90Yes;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Panel pnlDRBTripleWaterLocklineUpgrade;
        private System.Windows.Forms.RadioButton rdDRBTripleWaterLocklineUpgradeNo;
        private System.Windows.Forms.RadioButton rdDRBTripleWaterLocklineUpgradeYes;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox ddlDRBControllerType;
        private System.Windows.Forms.Label label59;
        private Telerik.WinControls.UI.RadMaskedEditBox txtDRBHoseLoopHeight;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ComboBox ddlDRBSingleDualWater;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Panel pnlDRBWirelessWalkSwitch;
        private System.Windows.Forms.RadioButton rdDRBWirelessWalkSwitchNo;
        private System.Windows.Forms.RadioButton rdDRBWirelessWalkSwitchYes;
        private System.Windows.Forms.Label label64;
        private Telerik.WinControls.UI.RadMaskedEditBox txtDRBRailDropDownAssy;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox txtQuoteNumber;
        private System.Windows.Forms.ToolStripMenuItem mnDistributors;
        private System.Windows.Forms.TabPage tpCWFDoubleRailBoom;
        private System.Windows.Forms.Panel pnlCWFDRBWirelessWalkSwitch;
        private System.Windows.Forms.RadioButton rdCWFDRBWirelessWalkSwitchNo;
        private System.Windows.Forms.RadioButton rdCWFDRBWirelessWalkSwitchYes;
        private System.Windows.Forms.Label label65;
        private Telerik.WinControls.UI.RadMaskedEditBox txtCWFDRBRailDropDownAssy;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel pnlCWFDRBTripleTurretBody;
        private System.Windows.Forms.RadioButton rdCWFDRBTripleTurretBodyNo;
        private System.Windows.Forms.RadioButton rdCWFDRBTripleTurretBodyYes;
        private System.Windows.Forms.Label label68;
        private Telerik.WinControls.UI.RadMaskedEditBox txtCWFDRBQuantityOfSystems;
        private System.Windows.Forms.Panel pnlCWFDRBMountedInjector;
        private System.Windows.Forms.RadioButton rdCWFDRBMountedInjectorNo;
        private System.Windows.Forms.RadioButton rdCWFDRBMountedInjectorYes;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel pnlCWFDRBPressureRegulator;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton rdCWFDRBPressureRegulatorNo;
        private System.Windows.Forms.RadioButton rdCWFDRBPressureRegulatorYes;
        private System.Windows.Forms.Label label70;
        private Telerik.WinControls.UI.RadMaskedEditBox txtCWFDRBSpraybodySpacing;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Panel pnlCWFDRBTripleWaterLocklineUpgrade;
        private System.Windows.Forms.RadioButton rdCWFDRBTripleWaterLocklineUpgradeNo;
        private System.Windows.Forms.RadioButton rdCWFDRBTripleWaterLocklineUpgradeYes;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.ComboBox ddlCWFDRBControllerType;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.ComboBox ddlCWFDRBSingleDualWater;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TabPage tpControlTowerBoom;
        private System.Windows.Forms.Panel pnlCTWirelessWalkSwitch;
        private System.Windows.Forms.RadioButton rdCTWirelessWalkSwitchNo;
        private System.Windows.Forms.RadioButton rdCTWirelessWalkSwitchYes;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel pnlCTTripleTurretBody;
        private System.Windows.Forms.RadioButton rdCTTripleTurretBodyNo;
        private System.Windows.Forms.RadioButton rdCTTripleTurretBodyYes;
        private System.Windows.Forms.Label label75;
        private Telerik.WinControls.UI.RadMaskedEditBox txtCTQuantityOfSystems;
        private System.Windows.Forms.Panel pnlCTMountedInjector;
        private System.Windows.Forms.RadioButton rdCTMountedInjectorNo;
        private System.Windows.Forms.RadioButton rdCTMountedInjectorYes;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel pnlCTPressureRegulator;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton rdCTPressureRegulatorNo;
        private System.Windows.Forms.RadioButton rdCTPressureRegulatorYes;
        private System.Windows.Forms.Label label79;
        private Telerik.WinControls.UI.RadMaskedEditBox txtCTSpraybodySpacing;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Panel pnlCTTripleWaterLocklineUpgrade;
        private System.Windows.Forms.RadioButton rdCTTripleWaterLocklineUpgradeNo;
        private System.Windows.Forms.RadioButton rdCTTripleWaterLocklineUpgradeYes;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.ComboBox ddlCTControllerType;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.ComboBox ddlCTSingleDualWater;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Panel pnlCT1_1_4thHoseUpgrade;
        private System.Windows.Forms.RadioButton rdCT1_1_4thHoseUpgradeNo;
        private System.Windows.Forms.RadioButton rdCT1_1_4thHoseUpgradeYes;
        private System.Windows.Forms.Label label86;
        private Telerik.WinControls.UI.RadMaskedEditBox txtCTHoseLoopHeight;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Panel pnlCTConveyor;
        private System.Windows.Forms.RadioButton rdCTConveyorNo;
        private System.Windows.Forms.RadioButton rdCTConveyorYes;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Panel pnlCTDiamondSweep;
        private System.Windows.Forms.RadioButton radioButton19;
        private System.Windows.Forms.RadioButton rdCTDiamondSweepNo;
        private System.Windows.Forms.RadioButton rdCTDiamondSweepYes;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Panel pnlCTWaterFeedSystem;
        private System.Windows.Forms.RadioButton radioButton29;
        private System.Windows.Forms.RadioButton rdCTWaterFeedSystemNo;
        private System.Windows.Forms.RadioButton rdCTWaterFeedSystemYes;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Panel pnlCTGallonTank;
        private System.Windows.Forms.RadioButton rdCTGallonTankNo;
        private System.Windows.Forms.RadioButton rdCTGallonTankYes;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Panel pnlCTDualMotorNoAxle;
        private System.Windows.Forms.RadioButton radioButton24;
        private System.Windows.Forms.RadioButton rdCTDualMotorNoAxleNo;
        private System.Windows.Forms.RadioButton rdCTDualMotorNoAxleYes;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TabPage tpNavigatorBoom;
        private System.Windows.Forms.Panel pnlNavWaterFeedSystem;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton rdNavWaterFeedSystemNo;
        private System.Windows.Forms.RadioButton rdNavWaterFeedSystemYes;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Panel pnlNavGallonTank;
        private System.Windows.Forms.RadioButton rdNavGallonTankNo;
        private System.Windows.Forms.RadioButton rdNavGallonTankYes;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Panel pnlNavSweep90;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.RadioButton rdNavSweep90No;
        private System.Windows.Forms.RadioButton rdNavSweep90Yes;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Panel pnlNav1_1_4thHoseUpgrade;
        private System.Windows.Forms.RadioButton rdNav1_1_4thHoseUpgradeNo;
        private System.Windows.Forms.RadioButton rdNav1_1_4thHoseUpgradeYes;
        private System.Windows.Forms.Label label97;
        private Telerik.WinControls.UI.RadMaskedEditBox txtNavHoseLoopHeight;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Panel pnlNavWirelessWalkSwitch;
        private System.Windows.Forms.RadioButton rdNavWirelessWalkSwitchNo;
        private System.Windows.Forms.RadioButton rdNavWirelessWalkSwitchYes;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Panel pnlNavTripleTurretBody;
        private System.Windows.Forms.RadioButton rdNavTripleTurretBodyNo;
        private System.Windows.Forms.RadioButton rdNavTripleTurretBodyYes;
        private System.Windows.Forms.Label label102;
        private Telerik.WinControls.UI.RadMaskedEditBox txtNavQuantityOfSystems;
        private System.Windows.Forms.Panel pnlNavMountedInjector;
        private System.Windows.Forms.RadioButton rdNavMountedInjectorNo;
        private System.Windows.Forms.RadioButton rdNavMountedInjectorYes;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Panel pnlNavPressureRegulator;
        private System.Windows.Forms.RadioButton radioButton28;
        private System.Windows.Forms.RadioButton rdNavPressureRegulatorNo;
        private System.Windows.Forms.RadioButton rdNavPressureRegulatorYes;
        private System.Windows.Forms.Label label104;
        private Telerik.WinControls.UI.RadMaskedEditBox txtNavSpraybodySpacing;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Panel pnlNavTripleWaterLocklineUpgrade;
        private System.Windows.Forms.RadioButton rdNavTripleWaterLocklineUpgradeNo;
        private System.Windows.Forms.RadioButton rdNavTripleWaterLocklineUpgradeYes;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.ComboBox ddlNavSingleDualWater;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.ToolStripMenuItem mnRefreshLookups;
        private System.Windows.Forms.Button btnRemoveSystem;
        private System.Windows.Forms.TextBox txtPostDimensions;
        private System.Windows.Forms.ComboBox ddlNavControllerType;
        private System.Windows.Forms.Label label94;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRBNumberOfRows;
        private System.Windows.Forms.Label label96;
        private Telerik.WinControls.UI.RadMaskedEditBox txtDRBNumberOfRows;
        private System.Windows.Forms.Label label101;
        private Telerik.WinControls.UI.RadMaskedEditBox txtCWFDRBNumberOfRows;
        private System.Windows.Forms.Label label107;
        private Telerik.WinControls.UI.RadMaskedEditBox txtCTNumberOfRows;
        private System.Windows.Forms.Label label110;
        private Telerik.WinControls.UI.RadMaskedEditBox txtNavNumberOfRows;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox txtSRBTip1;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox txtSRBTip3;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox txtSRBTip2;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox txtDRBTip3;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.TextBox txtDRBTip2;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox txtDRBTip1;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox txtCWFDRBTip3;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox txtCWFDRBTip2;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox txtCWFDRBTip1;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TextBox txtCTTip3;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox txtCTTip2;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.TextBox txtCTTip1;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.TextBox txtNavTip3;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.TextBox txtNavTip2;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.TextBox txtNavTip1;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Panel pnlEchoEV;
        private System.Windows.Forms.RadioButton rdEchoEVNo;
        private System.Windows.Forms.RadioButton rdEchoEVYes;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Panel pnlSRBEV;
        private System.Windows.Forms.RadioButton rdSRBEVNo;
        private System.Windows.Forms.RadioButton rdSRBEVYes;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Panel pnlDRBEV;
        private System.Windows.Forms.RadioButton rdDRBEVNo;
        private System.Windows.Forms.RadioButton rdDRBEVYes;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Panel pnlCWFDRBEV;
        private System.Windows.Forms.RadioButton rdCWFDRBEVNo;
        private System.Windows.Forms.RadioButton rdCWFDRBEVYes;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Panel pnlCTEV;
        private System.Windows.Forms.RadioButton rdCTEVNo;
        private System.Windows.Forms.RadioButton rdCTEVYes;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Panel pnlNavEV;
        private System.Windows.Forms.RadioButton rdNavEVNo;
        private System.Windows.Forms.RadioButton rdNavEVYes;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.RadioButton rdEchoCenterBayWaterStationYes;
        private System.Windows.Forms.ComboBox ddlCTCarrySteel;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.ComboBox ddlNavCarrySteel;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Button btnSendEmail;
        private System.Windows.Forms.ComboBox ddlEchoFeed;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.ComboBox ddlSRBFeed;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.ComboBox ddlCWFDRBFeed;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.ComboBox ddlCTFeed;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.ComboBox ddlNavFeed;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Button btnGenerateSalesOrderForm;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.DateTimePicker dtOrderConfirmed;
        private System.Windows.Forms.Label label141;
        private Telerik.WinControls.UI.RadMaskedEditBox txtClearanceWidth;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Panel pnlEchoWaterSolenoidControl;
        private System.Windows.Forms.RadioButton rdEchoWaterSolenoidControlNo;
        private System.Windows.Forms.RadioButton rdEchoWaterSolenoidControlYes;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Panel pnlSRBWaterSolenoidControl;
        private System.Windows.Forms.RadioButton rdSRBWaterSolenoidControlNo;
        private System.Windows.Forms.RadioButton rdSRBWaterSolenoidControlYes;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Panel pnlDRBWaterSolenoidControl;
        private System.Windows.Forms.RadioButton rdDRBWaterSolenoidControlNo;
        private System.Windows.Forms.RadioButton rdDRBWaterSolenoidControlYes;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Panel pnlCWFDRBWaterSolenoidControl;
        private System.Windows.Forms.RadioButton rdCWFDRBWaterSolenoidControlNo;
        private System.Windows.Forms.RadioButton rdCWFDRBWaterSolenoidControlYes;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Panel pnlCTWaterSolenoidControl;
        private System.Windows.Forms.RadioButton rdCTWaterSolenoidControlNo;
        private System.Windows.Forms.RadioButton rdCTWaterSolenoidControlYes;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Panel pnlNavWaterSolenoidControl;
        private System.Windows.Forms.RadioButton rdNavWaterSolenoidControlNo;
        private System.Windows.Forms.RadioButton rdNavWaterSolenoidControlYes;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Panel pnlDRBAutoInjector;
        private System.Windows.Forms.RadioButton rdDRBAutoInjectorNo;
        private System.Windows.Forms.RadioButton rdDRBAutoInjectorYes;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.ComboBox ddlDRBInjectorType;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.ComboBox ddlSRBInjectorType;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Panel pnlSRBAutoInjector;
        private System.Windows.Forms.RadioButton rdSRBAutoInjectorNo;
        private System.Windows.Forms.RadioButton rdSRBAutoInjectorYes;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.ComboBox ddlCWFDRBInjectorType;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Panel pnlCWFDRBAutoInjector;
        private System.Windows.Forms.RadioButton rdCWFDRBAutoInjectorNo;
        private System.Windows.Forms.RadioButton rdCWFDRBAutoInjectorYes;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.ComboBox ddlCTInjectorType;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Panel pnlCTAutoInjector;
        private System.Windows.Forms.RadioButton rdCTAutoInjectorNo;
        private System.Windows.Forms.RadioButton rdCTAutoInjectorYes;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.ComboBox ddlNavInjectorType;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Panel pnlNavAutoInjector;
        private System.Windows.Forms.RadioButton rdNavAutoInjectorNo;
        private System.Windows.Forms.RadioButton rdNavAutoInjectorYes;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Panel pnlCWFDRB1_1_4thHoseUpgrade;
        private System.Windows.Forms.RadioButton rdCWFDRB1_1_4thHoseUpgradeNo;
        private System.Windows.Forms.RadioButton rdCWFDRB1_1_4thHoseUpgradeYes;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Panel pnlCWFDRBWaterFeedSystem;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton rdCWFDRBWaterFeedSystemNo;
        private System.Windows.Forms.RadioButton rdCWFDRBWaterFeedSystemYes;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.TextBox txtInstallationEstimate;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.TextBox txtPurchaseOrder;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tpGroundRunnerBoom;
        private System.Windows.Forms.ComboBox ddlGRInjectorType;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Panel pnlGRAutoInjector;
        private System.Windows.Forms.RadioButton rdGRAutoInjectorNo;
        private System.Windows.Forms.RadioButton rdGRAutoInjectorYes;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Panel pnlGRWaterSolenoidControl;
        private System.Windows.Forms.RadioButton rdGRWaterSolenoidControlNo;
        private System.Windows.Forms.RadioButton rdGRWaterSolenoidControlYes;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Panel pnlGREV;
        private System.Windows.Forms.RadioButton rdGREVNo;
        private System.Windows.Forms.RadioButton rdGREVYes;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.TextBox txtGRTip3;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.TextBox txtGRTip2;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.TextBox txtGRTip1;
        private System.Windows.Forms.Label label168;
        private Telerik.WinControls.UI.RadMaskedEditBox txtGRNumberOfRows;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Panel pnlGRWirelessWalkSwitch;
        private System.Windows.Forms.RadioButton rdGRWirelessWalkSwitchNo;
        private System.Windows.Forms.RadioButton rdGRWirelessWalkSwitchYes;
        private System.Windows.Forms.Label label170;
        private Telerik.WinControls.UI.RadMaskedEditBox txtGRRailDropDownAssy;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Panel pnlGR1_1_4thHoseUpgrade;
        private System.Windows.Forms.RadioButton rdGR1_1_4thHoseUpgradeNo;
        private System.Windows.Forms.RadioButton rdGR1_1_4thHoseUpgradeYes;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Panel pnlGRTripleTurretBody;
        private System.Windows.Forms.RadioButton rdGRTripleTurretBodyNo;
        private System.Windows.Forms.RadioButton rdGRTripleTurretBodyYes;
        private System.Windows.Forms.Label label173;
        private Telerik.WinControls.UI.RadMaskedEditBox txtGRQuantityOfSystems;
        private System.Windows.Forms.Panel pnlGRMountedInjector;
        private System.Windows.Forms.RadioButton rdGRMountedInjectorNo;
        private System.Windows.Forms.RadioButton rdGRMountedInjectorYes;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Panel pnlGRPressureRegulator;
        private System.Windows.Forms.RadioButton radioButton23;
        private System.Windows.Forms.RadioButton rdGRPressureRegulatorNo;
        private System.Windows.Forms.RadioButton rdGRPressureRegulatorYes;
        private System.Windows.Forms.Label label175;
        private Telerik.WinControls.UI.RadMaskedEditBox txtGRSpraybodySpacing;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Panel pnlGRSweep90;
        private System.Windows.Forms.RadioButton rdGRSweep90No;
        private System.Windows.Forms.RadioButton rdGRSweep90Yes;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.Panel pnlGRTripleWaterLocklineUpgrade;
        private System.Windows.Forms.RadioButton rdGRTripleWaterLocklineUpgradeNo;
        private System.Windows.Forms.RadioButton rdGRTripleWaterLocklineUpgradeYes;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.ComboBox ddlGRControllerType;
        private System.Windows.Forms.Label label179;
        private Telerik.WinControls.UI.RadMaskedEditBox txtGRHoseLoopHeight;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.ComboBox ddlGRSingleDualWater;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.TabPage tpSkyRailBoom;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRNumberOfRows;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRRailDropDownAssy;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRQuantityOfSystems;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRSpraybodySpacing;
        private Telerik.WinControls.UI.RadMaskedEditBox txtSRHoseLoopHeight;
        private System.Windows.Forms.ComboBox ddlSRInjectorType;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Panel pnlSRAutoInjector;
        private System.Windows.Forms.RadioButton rdSRAutoInjectorNo;
        private System.Windows.Forms.RadioButton rdSRAutoInjectorYes;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Panel pnlSRWaterSolenoidControl;
        private System.Windows.Forms.RadioButton rdSRWaterSolenoidControlNo;
        private System.Windows.Forms.RadioButton rdSRWaterSolenoidControlYes;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Panel pnlSREV;
        private System.Windows.Forms.RadioButton rdSREVNo;
        private System.Windows.Forms.RadioButton rdSREVYes;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.TextBox txtSRTip3;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.TextBox txtSRTip2;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.TextBox txtSRTip1;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Panel pnlSRWirelessWalkSwitch;
        private System.Windows.Forms.RadioButton rdSRWirelessWalkSwitchNo;
        private System.Windows.Forms.RadioButton rdSRWirelessWalkSwitchYes;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Panel pnlSR1_1_4thHoseUpgrade;
        private System.Windows.Forms.RadioButton rdSR1_1_4thHoseUpgradeNo;
        private System.Windows.Forms.RadioButton rdSR1_1_4thHoseUpgradeYes;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.Panel pnlSRTripleTurretBody;
        private System.Windows.Forms.RadioButton rdSRTripleTurretBodyNo;
        private System.Windows.Forms.RadioButton rdSRTripleTurretBodyYes;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Panel pnlSRMountedInjector;
        private System.Windows.Forms.RadioButton rdSRMountedInjectorNo;
        private System.Windows.Forms.RadioButton rdSRMountedInjectorYes;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Panel pnlSRPressureRegulator;
        private System.Windows.Forms.RadioButton radioButton25;
        private System.Windows.Forms.RadioButton rdSRPressureRegulatorNo;
        private System.Windows.Forms.RadioButton rdSRPressureRegulatorYes;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Panel pnlSRSweep90;
        private System.Windows.Forms.RadioButton rdSRSweep90No;
        private System.Windows.Forms.RadioButton rdSRSweep90Yes;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Panel pnlSRTripleWaterLocklineUpgrade;
        private System.Windows.Forms.RadioButton rdSRTripleWaterLocklineUpgradeNo;
        private System.Windows.Forms.RadioButton rdSRTripleWaterLocklineUpgradeYes;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.ComboBox ddlSRControllerType;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.ComboBox ddlSRSingleDualWater;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.TextBox txtCustomerNote;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.TextBox txtSteelEstimate;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Panel pnlGRWaterFeedSystem;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton rdGRWaterFeedSystemNo;
        private System.Windows.Forms.RadioButton rdGRWaterFeedSystemYes;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Panel pnlSRWaterFeedSystem;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton rdSRWaterFeedSystemNo;
        private System.Windows.Forms.RadioButton rdSRWaterFeedSystemYes;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.DateTimePicker txtDueDate;
        private System.Windows.Forms.ToolStripMenuItem mnGreenHouses;
        private System.Windows.Forms.TextBox txtContactEmail;
        private System.Windows.Forms.TextBox txtContactFax;
        private System.Windows.Forms.TextBox txtContactPhone;
        private System.Windows.Forms.TextBox txtContactName;
        private System.Windows.Forms.TextBox txtShipAddress;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.Button btnAddCustomItems;
        private System.Windows.Forms.ToolStripMenuItem mnManageEmails;
        private System.Windows.Forms.ToolStripMenuItem mnManageCustomerQuoteEmail;
        private System.Windows.Forms.Button btnSteelMaterials;
        private Telerik.WinControls.UI.RadMaskedEditBox txtLeadTime;
        private System.Windows.Forms.TextBox txtQuoteTotal;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.ComboBox ddlSRBEVWireGauge;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.ComboBox ddlDRBEVWireGauge;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.ComboBox ddlCWFDRBEVWireGauge;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.ComboBox ddlCTEVWireGauge;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.ComboBox ddlNavEVWireGauge;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.ComboBox ddlGREVWireGauge;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.ComboBox ddlSREVWireGauge;
        private System.Windows.Forms.Label label219;
        private Telerik.WinControls.UI.RadMaskedEditBox txtPriceLevel;
        private System.Windows.Forms.Label label220;
    }
}

