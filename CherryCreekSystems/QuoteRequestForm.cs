﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Utilities;
using JR.Utils.GUI.Forms;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.DAL;
using Telerik.WinControls.UI;

namespace CherryCreekSystems
{
    public partial class QuoteRequestForm : Form
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public delegate void SaveFinishedAsync(QuoteRequest quote, bool showLoadingForm, string loadingFormMessage);

        public SaveFinishedAsync OnSaveFinished;

        private DataContext _context;

        private QuoteRequestService quoteRequestService;
        private QuoteBoomSystemService quoteBoomSystemService;
        private QuoteStatus initialQuoteStatus;

        #region Selected Boom Type (tab)

        public BoomType SelectedBoomType
        {
            get
            {
                var selectedTabPage = tbBooms.SelectedTab;
                var selectedBoomType = (BoomType)Enum.Parse(typeof(BoomType), selectedTabPage.Tag.ToString());
                return selectedBoomType;
            }
        }

        #endregion Selected Boom Type (tab)

        #region Current Boom

        private Boom currentBoom;

        public Boom CurrentBoom
        {
            get { return currentBoom; }
            set
            {
                currentBoom = value;
                var isValidBoom = currentBoom != null;

                btnRemoveSystem.Enabled = isValidBoom;
                btnSteelMaterials.Enabled = isValidBoom;
                btnGenerateSalesOrderForm.Enabled = isValidBoom;

                if (isValidBoom)
                {
                    ddlGreenhouseType.SelectedValue = currentBoom.GreenHouseTypeId;
                    txtAdditionalInformation.Text = currentBoom.AdditionalInformation;
                    txtCustomerNote.Text = currentBoom.CustomerNote;
                    txtNumberOfBays.Text = currentBoom.NumberOfBays.ToString();
                    txtBayLength.Text = currentBoom.BayLengthInFeet.ToString();
                    txtBayWidth.Text = currentBoom.BayWidthInFeet.ToString();
                    txtNumberOfBaySidewalls.Text = currentBoom.NumberOfBaySideWalls.ToString();
                    txtWalkwayWidth.Text = currentBoom.WalkwayWidthInInches.ToString();
                    txtClearanceWidth.Text = currentBoom.ClearanceWidthInInches.ToString();
                    txtBottomCordHeight.Text = currentBoom.BottomCordHeightInFeet.ToString();
                    txtTrussSpacing.Text = currentBoom.TrussSpacingInFeet.ToString();

                    var cordTypeRadioButton = Controls.Find($"rd{currentBoom.BottomCordType}CordType", true)
                                      .First() as RadioButton;
                    cordTypeRadioButton.Checked = true;

                    txtBottomCordSize.Text = currentBoom.BottomCordSize;
                    txtGrowingSurfaceHeight.Text = currentBoom.GrowingSurfaceHeightInInches.ToString();
                    txtPostSpacing.Text = currentBoom.PostSpacingInFeet.ToString();
                    txtPostDimensions.Text = currentBoom.PostDimensionsInInchesSquared;
                    txtSpraybarSize.Text = currentBoom.SprayBarSize.ToString();
                    txtHoseSize.Text = currentBoom.HoseSize.ToString();
                    txtLocation.Text = currentBoom.Location;

                    if (currentBoom.BoomTypeId == 1)
                    {
                        txtEchoQuantityOfSystems.Text = currentBoom.QuantityOfSystems.ToString();
                        ddlEchoDrumSize.SelectedItem = currentBoom.DrumSize.ToString();
                        txtEchoBasketSpacing.Text = currentBoom.BasketSpacing.ToString();
                        ddlEchoControllerType.SelectedItem = currentBoom.ControllerType;
                        ddlEchoNumberOfLayers.SelectedItem = currentBoom.NumberOfLayers.ToString();
                        ddlEchoFeed.SelectedItem = currentBoom.Feed;

                        var answer = currentBoom.EvVpd.ToStringAnswer();
                        var radioButton = Controls.Find($"rdEchoEV{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.CenterBayWaterStationRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdEchoCenterBayWaterStation{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.AMIADFilterAssemblyRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdEchoAMIADFilterAssembly{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.LocklineRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdEchoLockline{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtEchoExtensionHangerLength.Text = currentBoom.ExtensionHangerLengthInInches.ToString();
                        ddlEchoPulleyBracketSpacing.SelectedItem = currentBoom.PulleyBracketSpacingInFeet.ToString();

                        answer = currentBoom.RemotePullChainSwitchRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdEchoRemotePullChainSwitch{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.DRAMMRedheadBreakerUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdEchoRedheadBreakerUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WaterSolenoidControlRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdEchoWaterSolenoidControl{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        var echoTab = tbBooms.TabPages["tpEcho"];
                        tbBooms.SelectedTab = echoTab;
                    }
                    else if (currentBoom.BoomTypeId == 2)
                    {
                        txtSRBQuantityOfSystems.Text = currentBoom.QuantityOfSystems.ToString();
                        txtSRBHoseLoopHeight.Text = currentBoom.HoseLoopHeightInInches.ToString();
                        ddlSRBSingleDualWater.SelectedItem = currentBoom.SingleOrDualWater;
                        txtSRBNumberOfRows.Text = currentBoom.NumberOfRows.ToString();
                        ddlSRBFeed.SelectedItem = currentBoom.Feed;

                        var answer = currentBoom.EvVpd.ToStringAnswer();
                        var radioButton = Controls.Find($"rdSRBEV{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtSRBTip1.Text = currentBoom.Tip1;
                        txtSRBTip2.Text = currentBoom.Tip2;
                        txtSRBTip3.Text = currentBoom.Tip3;

                        answer = currentBoom.TripleStepLocklineUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRBTripleWaterLocklineUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.TripleTurretBodyRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRBTripleTurretBody{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlSRBControllerType.SelectedItem = currentBoom.ControllerType;

                        answer = currentBoom.Sweep90Requested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRBSweep90Degree{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.PressureRegulatorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRBPressureRegulator{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.MountedInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRBMountedInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtSRBSpraybodySpacing.Text = currentBoom.SprayBodySpacingInInches.ToString();

                        answer = currentBoom.AMIADFilterASSYRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRBAMIADFilderAssy{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WaterSolenoidControlRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRBWaterSolenoidControl{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.AutoInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRBAutoInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlSRBInjectorType.SelectedItem = currentBoom.InjectorType;
                        ddlSRBEVWireGauge.SelectedItem = currentBoom.EVWireGauge;

                        var srTab = tbBooms.TabPages["tpSingleRailBoom"];
                        tbBooms.SelectedTab = srTab;
                    }
                    else if (currentBoom.BoomTypeId == 3)
                    {
                        txtDRBQuantityOfSystems.Text = currentBoom.QuantityOfSystems.ToString();
                        txtDRBHoseLoopHeight.Text = currentBoom.HoseLoopHeightInInches.ToString();
                        ddlDRBSingleDualWater.SelectedItem = currentBoom.SingleOrDualWater;
                        txtDRBNumberOfRows.Text = currentBoom.NumberOfRows.ToString();

                        var answer = currentBoom.EvVpd.ToStringAnswer();
                        var radioButton = Controls.Find($"rdDRBEV{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtDRBTip1.Text = currentBoom.Tip1;
                        txtDRBTip2.Text = currentBoom.Tip2;
                        txtDRBTip3.Text = currentBoom.Tip3;

                        answer = currentBoom.TripleStepLocklineUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRBTripleWaterLocklineUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.TripleTurretBodyRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRBTripleTurretBody{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlDRBControllerType.SelectedItem = currentBoom.ControllerType;

                        answer = currentBoom.Sweep90Requested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRBSweep90{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.PressureRegulatorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRBPressureRegulator{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.MountedInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRBMountedInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtDRBSpraybodySpacing.Text = currentBoom.SprayBodySpacingInInches.ToString();
                        txtDRBRailDropDownAssy.Text = currentBoom.RailDropDownASSYInInches.ToString();

                        answer = currentBoom.HoseUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRB1_1_4thHoseUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WirelessWalkSwitchRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRBWirelessWalkSwitch{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WaterSolenoidControlRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRBWaterSolenoidControl{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.AutoInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdDRBAutoInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlDRBInjectorType.SelectedItem = currentBoom.InjectorType;
                        ddlDRBEVWireGauge.SelectedItem = currentBoom.EVWireGauge;

                        var drTab = tbBooms.TabPages["tpDoubleRailBoom"];
                        tbBooms.SelectedTab = drTab;
                    }
                    else if (currentBoom.BoomTypeId == 4)
                    {
                        txtCWFDRBQuantityOfSystems.Text = currentBoom.QuantityOfSystems.ToString();
                        ddlCWFDRBSingleDualWater.SelectedItem = currentBoom.SingleOrDualWater;
                        txtCWFDRBNumberOfRows.Text = currentBoom.NumberOfRows.ToString();
                        ddlCWFDRBFeed.SelectedItem = currentBoom.Feed;

                        var answer = currentBoom.EvVpd.ToStringAnswer();
                        var radioButton = Controls.Find($"rdCWFDRBEV{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtCWFDRBTip1.Text = currentBoom.Tip1;
                        txtCWFDRBTip2.Text = currentBoom.Tip2;
                        txtCWFDRBTip3.Text = currentBoom.Tip3;

                        answer = currentBoom.TripleStepLocklineUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRBTripleWaterLocklineUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.TripleTurretBodyRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRBTripleTurretBody{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlCWFDRBControllerType.SelectedItem = currentBoom.ControllerType;

                        answer = currentBoom.PressureRegulatorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRBPressureRegulator{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.MountedInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRBMountedInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtCWFDRBSpraybodySpacing.Text = currentBoom.SprayBodySpacingInInches.ToString();
                        txtCWFDRBRailDropDownAssy.Text = currentBoom.RailDropDownASSYInInches.ToString();

                        answer = currentBoom.WirelessWalkSwitchRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRBWirelessWalkSwitch{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WaterSolenoidControlRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRBWaterSolenoidControl{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.AutoInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRBAutoInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.CenterWaterFeedSystemRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRBWaterFeedSystem{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.HoseUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCWFDRB1_1_4thHoseUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlCWFDRBInjectorType.SelectedItem = currentBoom.InjectorType;
                        ddlCWFDRBEVWireGauge.SelectedItem = currentBoom.EVWireGauge;

                        var cwfdrTab = tbBooms.TabPages["tpCWFDoubleRailBoom"];
                        tbBooms.SelectedTab = cwfdrTab;
                    }
                    else if (currentBoom.BoomTypeId == 5)
                    {
                        txtCTQuantityOfSystems.Text = currentBoom.QuantityOfSystems.ToString();
                        txtCTHoseLoopHeight.Text = currentBoom.HoseLoopHeightInInches.ToString();
                        ddlCTSingleDualWater.SelectedItem = currentBoom.SingleOrDualWater;
                        txtCTNumberOfRows.Text = currentBoom.NumberOfRows.ToString();
                        ddlCTFeed.SelectedItem = currentBoom.Feed;

                        var answer = currentBoom.EvVpd.ToStringAnswer();
                        var radioButton = Controls.Find($"rdCTEV{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtCTTip1.Text = currentBoom.Tip1;
                        txtCTTip2.Text = currentBoom.Tip2;
                        txtCTTip3.Text = currentBoom.Tip3;

                        answer = currentBoom.TripleStepLocklineUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTTripleWaterLocklineUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.TripleTurretBodyRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTTripleTurretBody{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlCTControllerType.SelectedItem = currentBoom.ControllerType;

                        answer = currentBoom.PressureRegulatorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTPressureRegulator{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.MountedInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTMountedInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtCTSpraybodySpacing.Text = currentBoom.SprayBodySpacingInInches.ToString();

                        answer = currentBoom.HoseUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCT1_1_4thHoseUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.DiamondSweepRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTDiamondSweep{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.ConveyorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTConveyor{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.DualMotorNoAxleRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTDualMotorNoAxle{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.Gallon25ReservoirTankRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTGallonTank{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.CenterWaterFeedSystemRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTWaterFeedSystem{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.HoseUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCT1_1_4thHoseUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WirelessWalkSwitchRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTWirelessWalkSwitch{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WaterSolenoidControlRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTWaterSolenoidControl{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.AutoInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdCTAutoInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlCTCarrySteel.SelectedItem = currentBoom.CarrySteel;
                        ddlCTInjectorType.SelectedItem = currentBoom.InjectorType;
                        ddlCTEVWireGauge.SelectedItem = currentBoom.EVWireGauge;

                        var ctTab = tbBooms.TabPages["tpControlTowerBoom"];
                        tbBooms.SelectedTab = ctTab;
                    }
                    else if (currentBoom.BoomTypeId == 6)
                    {
                        txtNavQuantityOfSystems.Text = currentBoom.QuantityOfSystems.ToString();
                        txtNavHoseLoopHeight.Text = currentBoom.HoseLoopHeightInInches.ToString();
                        ddlNavSingleDualWater.SelectedItem = currentBoom.SingleOrDualWater;
                        txtNavNumberOfRows.Text = currentBoom.NumberOfRows.ToString();
                        ddlNavFeed.SelectedItem = currentBoom.Feed;

                        var answer = currentBoom.EvVpd.ToStringAnswer();
                        var radioButton = Controls.Find($"rdNavEV{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtNavTip1.Text = currentBoom.Tip1;
                        txtNavTip2.Text = currentBoom.Tip2;
                        txtNavTip3.Text = currentBoom.Tip3;

                        answer = currentBoom.TripleStepLocklineUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavTripleWaterLocklineUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.TripleTurretBodyRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavTripleTurretBody{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlNavControllerType.SelectedItem = currentBoom.ControllerType;

                        answer = currentBoom.PressureRegulatorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavPressureRegulator{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.MountedInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavMountedInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtNavSpraybodySpacing.Text = currentBoom.SprayBodySpacingInInches.ToString();

                        answer = currentBoom.HoseUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNav1_1_4thHoseUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.Gallon25ReservoirTankRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavGallonTank{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.CenterWaterFeedSystemRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavWaterFeedSystem{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.HoseUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNav1_1_4thHoseUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WirelessWalkSwitchRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavWirelessWalkSwitch{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.Sweep90Requested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavSweep90{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WaterSolenoidControlRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavWaterSolenoidControl{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.AutoInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdNavAutoInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlNavCarrySteel.SelectedItem = currentBoom.CarrySteel;
                        ddlNavInjectorType.SelectedItem = currentBoom.InjectorType;
                        ddlNavEVWireGauge.SelectedItem = currentBoom.EVWireGauge;

                        var navTab = tbBooms.TabPages["tpNavigatorBoom"];
                        tbBooms.SelectedTab = navTab;
                    }
                    else if (currentBoom.BoomTypeId == 7)
                    {
                        txtGRQuantityOfSystems.Text = currentBoom.QuantityOfSystems.ToString();
                        txtGRHoseLoopHeight.Text = currentBoom.HoseLoopHeightInInches.ToString();
                        ddlGRSingleDualWater.SelectedItem = currentBoom.SingleOrDualWater;
                        txtGRNumberOfRows.Text = currentBoom.NumberOfRows.ToString();

                        var answer = currentBoom.EvVpd.ToStringAnswer();
                        var radioButton = Controls.Find($"rdGREV{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtGRTip1.Text = currentBoom.Tip1;
                        txtGRTip2.Text = currentBoom.Tip2;
                        txtGRTip3.Text = currentBoom.Tip3;

                        answer = currentBoom.TripleStepLocklineUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRTripleWaterLocklineUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.TripleTurretBodyRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRTripleTurretBody{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlGRControllerType.SelectedItem = currentBoom.ControllerType;

                        answer = currentBoom.Sweep90Requested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRSweep90{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.PressureRegulatorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRPressureRegulator{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.MountedInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRMountedInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtGRSpraybodySpacing.Text = currentBoom.SprayBodySpacingInInches.ToString();
                        txtGRRailDropDownAssy.Text = currentBoom.RailDropDownASSYInInches.ToString();

                        answer = currentBoom.HoseUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGR1_1_4thHoseUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WirelessWalkSwitchRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRWirelessWalkSwitch{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WaterSolenoidControlRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRWaterSolenoidControl{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.AutoInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRAutoInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.CenterWaterFeedSystemRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdGRWaterFeedSystem{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlGRInjectorType.SelectedItem = currentBoom.InjectorType;
                        ddlGREVWireGauge.SelectedItem = currentBoom.EVWireGauge;

                        var grTab = tbBooms.TabPages["tpGroundRunnerBoom"];
                        tbBooms.SelectedTab = grTab;
                    }
                    else if (currentBoom.BoomTypeId == 8)
                    {
                        txtSRQuantityOfSystems.Text = currentBoom.QuantityOfSystems.ToString();
                        txtSRHoseLoopHeight.Text = currentBoom.HoseLoopHeightInInches.ToString();
                        ddlSRSingleDualWater.SelectedItem = currentBoom.SingleOrDualWater;
                        txtSRNumberOfRows.Text = currentBoom.NumberOfRows.ToString();

                        var answer = currentBoom.EvVpd.ToStringAnswer();
                        var radioButton = Controls.Find($"rdSREV{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtSRTip1.Text = currentBoom.Tip1;
                        txtSRTip2.Text = currentBoom.Tip2;
                        txtSRTip3.Text = currentBoom.Tip3;

                        answer = currentBoom.TripleStepLocklineUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRTripleWaterLocklineUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.TripleTurretBodyRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRTripleTurretBody{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlSRControllerType.SelectedItem = currentBoom.ControllerType;

                        answer = currentBoom.Sweep90Requested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRSweep90{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.PressureRegulatorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRPressureRegulator{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.MountedInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRMountedInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        txtSRSpraybodySpacing.Text = currentBoom.SprayBodySpacingInInches.ToString();
                        txtSRRailDropDownAssy.Text = currentBoom.RailDropDownASSYInInches.ToString();

                        answer = currentBoom.HoseUpgradeRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSR1_1_4thHoseUpgrade{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WirelessWalkSwitchRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRWirelessWalkSwitch{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.WaterSolenoidControlRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRWaterSolenoidControl{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.AutoInjectorRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRAutoInjector{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        answer = currentBoom.CenterWaterFeedSystemRequested.ToStringAnswer();
                        radioButton = Controls.Find($"rdSRWaterFeedSystem{answer}", true)
                                       .First() as RadioButton;
                        radioButton.Checked = true;

                        ddlSRInjectorType.SelectedItem = currentBoom.InjectorType;
                        ddlSREVWireGauge.SelectedItem = currentBoom.EVWireGauge;

                        var srTab = tbBooms.TabPages["tpSkyRailBoom"];
                        tbBooms.SelectedTab = srTab;
                    }
                }
            }
        }

        #endregion Current Boom

        private void DisplayCustomerFields(string name, string phone, string fax, string email)
        {
            txtContactName.Text = name;
            txtContactPhone.Text = phone;
            txtContactFax.Text = fax;
            txtContactEmail.Text = email;
        }

        private void DisplayShipAddressField(string fullAddress)
        {
            txtShipAddress.Text = fullAddress;
        }

        #region Current Quote

        private QuoteRequest currentQuoteRequest;

        public QuoteRequest CurrentQuoteRequest
        {
            get { return currentQuoteRequest; }
            set
            {
                currentQuoteRequest = value;
                var quoteRequestExists = currentQuoteRequest != null;
                bool hasSystem = false;
                if (quoteRequestExists)
                {
                    txtQuoteNumber.Text = currentQuoteRequest.QuoteNumberWithRevision;
                    txtQuoteTotal.Text = currentQuoteRequest.GetTotalCost().ToString("C2");
                    ddlQuoteStatus.SelectedValue = currentQuoteRequest.QuoteStatusId;
                    ddlSalesReps.SelectedValue = currentQuoteRequest.SalesRepId;
                    ddlCustomers.SelectedValue = currentQuoteRequest.CustomerId;
                    ddlDistributors.SelectedValue = currentQuoteRequest.DistributorId;

                    DisplayCustomerFields(currentQuoteRequest.ContactName, currentQuoteRequest.Phone,
                        currentQuoteRequest.Fax, currentQuoteRequest.Email);
                    DisplayShipAddressField(currentQuoteRequest.FullAddress);

                    txtSteelEstimate.Text = currentQuoteRequest.SteelEstimate;
                    txtFreightEstimate.Text = currentQuoteRequest.FreightEstimate;
                    txtInstallationEstimate.Text = currentQuoteRequest.InstallationEstimate;
                    txtDiscount.Text = currentQuoteRequest.Discount;
                    txtPriceLevel.Text = currentQuoteRequest.PriceLevel.ToString();
                    txtPurchaseOrder.Text = currentQuoteRequest.PurchaseOrder;
                    txtLeadTime.Text = currentQuoteRequest.LeadTime;
                    dtOrderConfirmed.Value = currentQuoteRequest.OrderDate;
                    txtDueDate.Value = currentQuoteRequest.DueDate;

                    CurrentCustomer = currentQuoteRequest.Customer;
                    ddlShipAddresses.SelectedItem = currentQuoteRequest.ShipAddress;

                    //After setting the dropdown values, override the fields with these values
                    DisplayCustomerFields(currentQuoteRequest.ContactName, currentQuoteRequest.Phone,
    currentQuoteRequest.Fax, currentQuoteRequest.Email);
                    DisplayShipAddressField(currentQuoteRequest.FullAddress);

                    refreshQuoteSystemsList(currentQuoteRequest.Id);

                    var firstSystem = currentQuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.FirstOrDefaultActive();
                    hasSystem = firstSystem != null;
                    if (hasSystem)
                    {
                        lstDetailQuotes.SelectedItem = firstSystem;
                    }
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);
                    ddlQuoteStatus.SelectedValue = initialQuoteStatus.Id;

                    CurrentCustomer = null;
                    refreshQuoteSystemsList(null);

                    initializeQuoteRequestControls();
                    initializeDefaultBoomControls();
                }

                btnAddSystem.Enabled = quoteRequestExists;
                btnAddCustomItems.Enabled = quoteRequestExists;
                btnSendEmail.Enabled = quoteRequestExists;
                ddlQuoteStatus.Enabled = quoteRequestExists;
                txtQuoteNumber.Enabled = quoteRequestExists;

                btnUpdateQuote.Visible = quoteRequestExists;
                btnCreateQuote.Visible = !quoteRequestExists;

                btnUpdateQuote.Enabled = hasSystem;
                btnGenerateQuoteReport.Enabled = hasSystem;
                btnSendEmail.Enabled = hasSystem;
            }
        }

        #endregion Current Quote

        #region Current Customer

        private Customer currentCustomer;

        public Customer CurrentCustomer
        {
            get { return currentCustomer; }
            set
            {
                currentCustomer = value;
                if (currentCustomer != null)
                {
                    DisplayCustomerFields(currentCustomer.ContactName, currentCustomer.Phone,
                       currentCustomer.Fax, currentCustomer.Email);

                    ddlShipAddresses.DataSource = null;
                    if (currentCustomer.ShipAddresses != null && currentCustomer.ShipAddresses.Count > 0)
                    {
                        ddlShipAddresses.DataSource = currentCustomer.ShipAddresses;
                        ddlShipAddresses.DisplayMember = "FullAddress";
                        ddlShipAddresses.ValueMember = "Id";
                        ddlShipAddresses.SelectedIndex = -1;
                    }
                }
            }
        }

        #endregion Current Customer

        private readonly LoadingForm LoadingForm;
        private readonly string DoubleNewLine = Environment.NewLine + Environment.NewLine;
        private readonly bool HideMenus;
        private readonly int? QuoteId;
        private double BayLengthInFeetLimit;
        private double BayWidthInFeetLimit;

        public QuoteRequestForm(int? quoteId = null, bool isMainScreen = true)
        {
            InitializeComponent();

            _context = new DataContext();
            HideMenus = !isMainScreen;

            QuoteId = quoteId;

            LoadingForm = new LoadingForm();
            FlexibleMessageBox.MAX_WIDTH_FACTOR = 1.0;
            FlexibleMessageBox.MAX_HEIGHT_FACTOR = 1.0;

            ddlShipAddresses.DropDown += AdjustWidthComboBox_DropDown;
        }

        //http://www.codeproject.com/Articles/5801/Adjust-combo-box-drop-down-list-width-to-longest-s
        private static void AdjustWidthComboBox_DropDown(object sender, EventArgs e)
        {
            var senderComboBox = (ComboBox)sender;
            var width = senderComboBox.DropDownWidth;
            var font = senderComboBox.Font;

            var vertScrollBarWidth = (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems)
                    ? SystemInformation.VerticalScrollBarWidth : 0;

            var itemsList = senderComboBox.Items.Cast<object>().Select(item => item.ToString());
            using (var g = senderComboBox.CreateGraphics())
            {
                foreach (var s in itemsList)
                {
                    var newWidth = (int)g.MeasureString(s, font).Width + vertScrollBarWidth;

                    if (width < newWidth)
                    {
                        width = newWidth;
                    }
                }
            }

            senderComboBox.DropDownWidth = width;
        }

        private async void QuoteRequestForm_Load(object sender, EventArgs e)
        {
            try
            {
                var statesService = new StatesService();
                var oneState = statesService.Get().FirstOrDefault();
                if (oneState == null) throw new Exception("Could not retrieve record from database");
            }
            catch (Exception ex)
            {
                FlexibleMessageBox.Show("Could not connect to the database, check your internet connection",
                    "Quotation System", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
                return;
            }

            btnCreateQuote.Enabled = false;
            LoadingForm.TopMost = true;

            if (HideMenus)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    LoadingForm.Show("Loading...");
                });
                mnNewQuote.Visible = false;
                mnSearchQuotes.Visible = false;
                mnManageLookups.Visible = false;
                mnManageComponents.Visible = false;
                mnManageEmails.Visible = false;
            }
            else
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    LoadingForm.Show("Loading Quotation System...");
                });
            }

            #region check for connection to SQL Azure before doing anything


            await initializeLookups();

            #endregion

            if (QuoteId.HasValue)
            {
                var quoteRequest = await quoteRequestService.Get()
                                                            .ByQuoteId(QuoteId.Value);
                await quoteRequestService.FillChildObjects(quoteRequests: quoteRequest, customer: true);
                CurrentQuoteRequest = quoteRequest;
            }
            else
            {
                CurrentQuoteRequest = null;
                if (Configuration.EnableTestData)
                {
                    populateTestData();
                }
            }

            btnCreateQuote.Enabled = true;

            LoadingForm.Hide();
            LoadingForm.TopMost = false;
        }

        #region bind dropdown lists

        private async Task initializeLookups()
        {
            var customerService = new CustomerService();
            var salesRepService = new SalesRepService();
            var greenhouseTypeService = new GreenhouseTypeService();
            var quoteStatusService = new QuoteStatusService();
            var specService = new SpecService();
            var distributorService = new DistributorService();
            quoteRequestService = new QuoteRequestService(_context);
            quoteBoomSystemService = new QuoteBoomSystemService(_context);

            var customersQuery = customerService.Get().ToListAsync();
            var salesRepsQuery = salesRepService.Get().ToListAsync();
            var greenhouseTypesQuery = greenhouseTypeService.Get().ToListAsync();
            var quoteStatusQuery = quoteStatusService.Get().ToListAsync();
            var distributorQuery = distributorService.Get().ToListAsync();
            var specsQuery = specService.Get().WithSpecLookups().ToListAsync();

            var quoteStatusesList = await quoteStatusQuery;
            var salesRepsList = await salesRepsQuery;
            var greenhouseTypesList = await greenhouseTypesQuery;
            var distributorsList = await distributorQuery;
            var specsList = await specsQuery;
            var customersList = await customersQuery;
            await customerService.FillChildObjects(shipAddress: true, customers: customersList.ToArray());

            bindCustomerList(customersList);
            bindSalesRepList(salesRepsList);
            bindGreenhouseTypeList(greenhouseTypesList);
            bindQuoteStatusList(quoteStatusesList);
            bindDistributorList(distributorsList);
            bindSpecLookups(specsList);
            populateConstantsFromDatabase(specsList);
            ddlShipAddresses.DataSource = null;
        }

        private void populateConstantsFromDatabase(List<Spec> specsList)
        {
            var bayLenthInFeetLimit = specsList.GetValues("Bay Length In Feet Limit").Any() ? specsList.GetValues("Bay Length In Feet Limit").ToList()[0] : string.Empty;
            var bayWidthInFeetLimit = specsList.GetValues("Bay Width In Feet Limit").Any() ? specsList.GetValues("Bay Width In Feet Limit").ToList()[0] : string.Empty;

            double bayLenthInFeetLimitValue;
            double bayWidthInFeetLimitValue;

            var bayLengthAvailable = double.TryParse(bayLenthInFeetLimit, out bayLenthInFeetLimitValue);
            var bayWidthAvailable = double.TryParse(bayWidthInFeetLimit, out bayWidthInFeetLimitValue);

            //Default values in case something fails from the database
            BayLengthInFeetLimit = bayLengthAvailable ? bayLenthInFeetLimitValue : 300;
            BayWidthInFeetLimit = bayWidthAvailable ? bayWidthInFeetLimitValue : 35;
        }

        private void bindSpecLookups(List<Spec> specsList)
        {
            var specContainer = specsList.GetSpecContainer();

            //Echo specs
            ddlEchoDrumSize.DataSource = specContainer.DrumSize.ToList();
            ddlEchoControllerType.DataSource = specContainer.EchoControllerType.ToList();
            ddlEchoNumberOfLayers.DataSource = specContainer.NumberOfLayers.ToList();
            ddlEchoPulleyBracketSpacing.DataSource = specContainer.PulleyBracketSpacing.ToList();
            ddlEchoFeed.DataSource = specContainer.Feed.ToList();

            //SRB specs
            ddlSRBControllerType.DataSource = specContainer.DRBControllerType.ToList();
            ddlSRBSingleDualWater.DataSource = specContainer.SingleDualWater.ToList();
            ddlSRBFeed.DataSource = specContainer.Feed.ToList();
            ddlSRBEVWireGauge.DataSource = specContainer.EvVpdGaugeWires.WithFirstEmptyValue().ToList();
            ddlSRBInjectorType.DataSource = specContainer.InjectorType.ToList();

            //DRB specs
            ddlDRBControllerType.DataSource = specContainer.DRBControllerType.ToList();
            ddlDRBSingleDualWater.DataSource = specContainer.SingleDualWater.ToList();
            ddlDRBEVWireGauge.DataSource = specContainer.EvVpdGaugeWires.WithFirstEmptyValue().ToList();
            ddlDRBInjectorType.DataSource = specContainer.InjectorType.ToList();

            //CWF-DRB specs
            ddlCWFDRBControllerType.DataSource = specContainer.DRBControllerType.ToList();
            ddlCWFDRBSingleDualWater.DataSource = specContainer.SingleDualWater.ToList();
            ddlCWFDRBFeed.DataSource = specContainer.Feed.ToList();
            ddlCWFDRBEVWireGauge.DataSource = specContainer.EvVpdGaugeWires.WithFirstEmptyValue().ToList();
            ddlCWFDRBInjectorType.DataSource = specContainer.InjectorType.ToList();

            //TB specs
            ddlCTControllerType.DataSource = specContainer.DRBControllerType.ToList();
            ddlCTSingleDualWater.DataSource = specContainer.SingleDualWater.ToList();
            ddlCTFeed.DataSource = specContainer.Feed.ToList();
            ddlCTCarrySteel.DataSource = specContainer.CarrySteel.ToList();
            ddlCTEVWireGauge.DataSource = specContainer.EvVpdGaugeWires.WithFirstEmptyValue().ToList();
            ddlCTInjectorType.DataSource = specContainer.InjectorType.ToList();

            //Navigator specs
            ddlNavControllerType.DataSource = specContainer.DRBControllerType.ToList();
            ddlNavSingleDualWater.DataSource = specContainer.SingleDualWater.ToList();
            ddlNavFeed.DataSource = specContainer.Feed.ToList();
            ddlNavCarrySteel.DataSource = specContainer.CarrySteel.ToList();
            ddlNavEVWireGauge.DataSource = specContainer.EvVpdGaugeWires.WithFirstEmptyValue().ToList();
            ddlNavInjectorType.DataSource = specContainer.InjectorType.ToList();

            //Ground Runner specs
            ddlGRControllerType.DataSource = specContainer.DRBControllerType.ToList();
            ddlGRSingleDualWater.DataSource = specContainer.SingleDualWater.ToList();
            ddlGREVWireGauge.DataSource = specContainer.EvVpdGaugeWires.WithFirstEmptyValue().ToList();
            ddlGRInjectorType.DataSource = specContainer.InjectorType.ToList();

            //Ground Runner specs
            ddlSRControllerType.DataSource = specContainer.DRBControllerType.ToList();
            ddlSRSingleDualWater.DataSource = specContainer.SingleDualWater.ToList();
            ddlSREVWireGauge.DataSource = specContainer.EvVpdGaugeWires.WithFirstEmptyValue().ToList();
            ddlSRInjectorType.DataSource = specContainer.InjectorType.ToList();

            resetEchoDropdownSelections();
            resetSRBDropdownSelections();
            resetDRBDropdownSelections();
            resetCWFDRBDropdownSelections();
            resetCTDropdownSelections();
            resetNavDropdownSelections();
            resetGRDropdownSelections();
            resetSRDropdownSelections();
        }

        private void bindDistributorList(List<Distributor> distributorsList)
        {
            ddlDistributors.DataSource = distributorsList.OrderBy(d => d.Name).ToList();
            ddlDistributors.DisplayMember = "Name";
            ddlDistributors.ValueMember = "Id";
            ddlDistributors.SelectedIndex = -1;
        }

        private void bindQuoteStatusList(List<QuoteStatus> quoteStatusesList)
        {
            ddlQuoteStatus.DataSource = quoteStatusesList;
            ddlQuoteStatus.ValueMember = "Id";
            ddlQuoteStatus.DisplayMember = "Name";

            initialQuoteStatus = quoteStatusesList.FirstOrDefault(s => s.Name.ToLower() == "active");
        }

        private void bindGreenhouseTypeList(List<GreenHouseType> greenhouseTypesList)
        {
            ddlGreenhouseType.DataSource = greenhouseTypesList.OrderBy(g => g.Name).ToList();
            ddlGreenhouseType.DisplayMember = "Name";
            ddlGreenhouseType.ValueMember = "Id";
            ddlGreenhouseType.SelectedIndex = -1;
        }

        private void bindSalesRepList(List<SalesRep> salesRepsList)
        {
            ddlSalesReps.DataSource = salesRepsList.OrderBy(s => s.Name).ToList();
            ddlSalesReps.DisplayMember = "Name";
            ddlSalesReps.ValueMember = "Id";
            ddlSalesReps.SelectedIndex = -1;
        }

        private void bindCustomerList(List<Customer> customers)
        {
            ddlCustomers.DataSource = customers.OrderBy(c => c.CompanyName).ToList();
            ddlCustomers.DisplayMember = "CompanyName";
            ddlCustomers.ValueMember = "Id";
            ddlCustomers.SelectedIndex = -1;
        }

        #endregion bind dropdown lists

        #region test data

        private void populateTestData()
        {
            ddlCustomers.SelectedIndex = 0;
            ddlSalesReps.SelectedIndex = 0;
            ddlDistributors.SelectedIndex = 0;
            ddlGreenhouseType.SelectedIndex = 4;
            ddlEchoControllerType.SelectedIndex = 0;
            ddlEchoDrumSize.SelectedIndex = 0;
            ddlEchoNumberOfLayers.SelectedIndex = 1;
            ddlEchoPulleyBracketSpacing.SelectedIndex = 1;

            //txtLeadTime.Text = "8";
            txtAdditionalInformation.Text = "Special Requests";
            txtNumberOfBays.Text = "2";
            txtBayLength.Text = "500";
            txtBayWidth.Text = "12";
            txtNumberOfBaySidewalls.Text = "0";
            txtWalkwayWidth.Text = "36";
            txtBottomCordHeight.Text = "15";
            txtTrussSpacing.Text = "12";
            rdSQCordType.Checked = true;
            txtBottomCordSize.Text = "2 X 2";
            txtGrowingSurfaceHeight.Text = "36";
            txtPostSpacing.Text = "10";
            txtPostDimensions.Text = "4";
            txtSpraybarSize.Text = "1";
            txtHoseSize.Text = "1";

            //Echo
            txtEchoQuantityOfSystems.Text = "1";
            ddlEchoDrumSize.SelectedIndex = 2;
            txtEchoBasketSpacing.Text = "24";
            ddlEchoControllerType.SelectedIndex = 1;
            ddlEchoNumberOfLayers.SelectedIndex = 1;
            rdEchoCenterBayWaterStationNo.Checked = true;
            rdEchoAMIADFilterAssemblyYes.Checked = true;
            rdEchoLocklineNo.Checked = true;
            txtEchoExtensionHangerLength.Text = "20";
            ddlEchoPulleyBracketSpacing.SelectedIndex = 2;
            rdEchoRemotePullChainSwitchNo.Checked = true;
            rdEchoRedheadBreakerUpgradeYes.Checked = true;
            ddlEchoFeed.SelectedIndex = 0;

            //SRB
            txtSRBQuantityOfSystems.Text = "1";
            txtSRBHoseLoopHeight.Text = "60";
            ddlSRBSingleDualWater.SelectedIndex = 0;
            rdSRBTripleWaterLocklineUpgradeNo.Checked = true;
            rdSRBTripleTurretBodyNo.Checked = true;
            ddlSRBControllerType.SelectedIndex = 0;
            rdSRBSweep90DegreeNo.Checked = true;
            rdSRBPressureRegulatorNo.Checked = true;
            rdSRBMountedInjectorNo.Checked = true;
            txtSRBSpraybodySpacing.Text = "18";
            rdSRBAMIADFilderAssyNo.Checked = true;
            ddlSRBFeed.SelectedIndex = 0;
            ddlSRBInjectorType.SelectedIndex = 0;

            //DRB
            txtDRBQuantityOfSystems.Text = "1";
            txtDRBHoseLoopHeight.Text = "60";
            ddlDRBSingleDualWater.SelectedIndex = 0;
            rdDRBTripleWaterLocklineUpgradeNo.Checked = true;
            rdDRBTripleTurretBodyNo.Checked = true;
            ddlDRBControllerType.SelectedIndex = 0;
            rdDRBSweep90No.Checked = true;
            rdDRBPressureRegulatorNo.Checked = true;
            rdDRBMountedInjectorNo.Checked = true;
            txtDRBRailDropDownAssy.Text = "0";
            rdDRB1_1_4thHoseUpgradeNo.Checked = true;
            txtDRBSpraybodySpacing.Text = "18";
            rdDRBWirelessWalkSwitchNo.Checked = true;
            ddlDRBInjectorType.SelectedIndex = 1;

            //CWF-DRB
            txtCWFDRBQuantityOfSystems.Text = "10";
            ddlCWFDRBSingleDualWater.SelectedIndex = 1;
            rdCWFDRBTripleWaterLocklineUpgradeNo.Checked = true;
            rdCWFDRBTripleTurretBodyNo.Checked = true;
            ddlCWFDRBControllerType.SelectedIndex = 1;
            rdCWFDRBPressureRegulatorNo.Checked = true;
            rdCWFDRBMountedInjectorNo.Checked = true;
            txtCWFDRBRailDropDownAssy.Text = "0";
            txtCWFDRBSpraybodySpacing.Text = "18";
            rdCWFDRBWirelessWalkSwitchNo.Checked = true;
            ddlCWFDRBFeed.SelectedIndex = 0;
            ddlCWFDRBInjectorType.SelectedIndex = 0;

            //TB
            txtCTQuantityOfSystems.Text = "10";
            ddlCTSingleDualWater.SelectedIndex = 1;
            rdCTTripleWaterLocklineUpgradeNo.Checked = true;
            rdCTTripleTurretBodyNo.Checked = true;
            ddlCTControllerType.SelectedIndex = 1;
            txtCTHoseLoopHeight.Text = "60";
            rdCTPressureRegulatorNo.Checked = true;
            rdCTMountedInjectorNo.Checked = true;
            txtCTSpraybodySpacing.Text = "18";
            rdCTWirelessWalkSwitchNo.Checked = true;
            rdCT1_1_4thHoseUpgradeNo.Checked = true;
            rdCTDiamondSweepNo.Checked = true;
            rdCTConveyorNo.Checked = true;
            rdCTDualMotorNoAxleNo.Checked = true;
            rdCTGallonTankNo.Checked = true;
            rdCTWaterFeedSystemNo.Checked = true;
            ddlCTFeed.SelectedIndex = 0;
            ddlCTInjectorType.SelectedIndex = 1;

            //Navigator
            txtNavQuantityOfSystems.Text = "10";
            ddlNavSingleDualWater.SelectedIndex = 0;
            rdNavTripleWaterLocklineUpgradeNo.Checked = true;
            rdNavTripleTurretBodyNo.Checked = true;
            ddlNavControllerType.SelectedIndex = 1;
            txtNavHoseLoopHeight.Text = "60";
            rdNavPressureRegulatorNo.Checked = true;
            rdNavMountedInjectorNo.Checked = true;
            txtNavSpraybodySpacing.Text = "18";
            rdNavWirelessWalkSwitchNo.Checked = true;
            rdNav1_1_4thHoseUpgradeNo.Checked = true;
            rdNavGallonTankNo.Checked = true;
            rdNavWaterFeedSystemNo.Checked = true;
            rdNavSweep90No.Checked = true;
            ddlNavFeed.SelectedIndex = 0;
            ddlNavInjectorType.SelectedItem = 0;
        }

        #endregion test data

        #region populate dropdowns

        private void resetEchoDropdownSelections()
        {
            ddlEchoDrumSize.SelectedIndex = -1;
            ddlEchoControllerType.SelectedIndex = -1;
            ddlEchoNumberOfLayers.SelectedIndex = -1;
            ddlEchoPulleyBracketSpacing.SelectedIndex = -1;
            ddlEchoFeed.SelectedIndex = -1;
        }

        private void resetSRBDropdownSelections()
        {
            ddlSRBControllerType.SelectedIndex = -1;
            ddlSRBSingleDualWater.SelectedIndex = -1;
            ddlSRBFeed.SelectedIndex = -1;
            ddlSRBInjectorType.SelectedIndex = -1;
        }

        private void resetDRBDropdownSelections()
        {
            ddlDRBControllerType.SelectedIndex = -1;
            ddlDRBSingleDualWater.SelectedIndex = -1;
            ddlDRBInjectorType.SelectedIndex = -1;
        }

        private void resetCWFDRBDropdownSelections()
        {
            ddlCWFDRBControllerType.SelectedIndex = -1;
            ddlCWFDRBSingleDualWater.SelectedIndex = -1;
            ddlCWFDRBFeed.SelectedIndex = -1;
            ddlCWFDRBInjectorType.SelectedIndex = -1;
        }

        private void resetCTDropdownSelections()
        {
            ddlCTControllerType.SelectedIndex = -1;
            ddlCTSingleDualWater.SelectedIndex = -1;
            ddlCTFeed.SelectedIndex = -1;
            ddlCTInjectorType.SelectedIndex = -1;
        }

        private void resetNavDropdownSelections()
        {
            ddlNavControllerType.SelectedIndex = -1;
            ddlNavSingleDualWater.SelectedIndex = -1;
            ddlNavFeed.SelectedIndex = -1;
            ddlNavInjectorType.SelectedIndex = -1;
        }

        private void resetGRDropdownSelections()
        {
            ddlGRControllerType.SelectedIndex = -1;
            ddlGRSingleDualWater.SelectedIndex = -1;
            ddlGRInjectorType.SelectedIndex = -1;
        }

        private void resetSRDropdownSelections()
        {
            ddlSRControllerType.SelectedIndex = -1;
            ddlSRSingleDualWater.SelectedIndex = -1;
            ddlSRInjectorType.SelectedIndex = -1;
        }

        //private Task<List<Spec>> getSpecLookups()
        //{
        //    var specsTask = Task.Run(() =>
        //    {
        //        var specs = specService.GetAllIncluding(IncludeProperties: l => l.SpecLookups).ToList();
        //        return specs;
        //    });

        //    return specsTask;
        //}

        //private Task<List<Distributor>> getDistributorList()
        //{
        //    var distributorsTask = Task.Run(() =>
        //    {
        //        var distributorList = distributorService.GetAll().ToList();
        //        return distributorList;
        //    });

        //    return distributorsTask;
        //}

        //private Task<List<QuoteStatus>> getQuoteStatusList()
        //{
        //    var quoteStatusesTask = Task.Run(() =>
        //    {
        //        var quoteStatusList = quoteStatusService.GetAll().ToList();
        //        return quoteStatusList;
        //    });

        //    return quoteStatusesTask;
        //}

        //private Task<List<GreenHouseType>> getGreenhouseTypeList()
        //{
        //    var greenhouseTypesTask = Task.Run(() =>
        //    {
        //        var greenhouseTypeList = greenhouseTypeService.GetAll().ToList();
        //        return greenhouseTypeList;
        //    });

        //    return greenhouseTypesTask;
        //}

        //private Task<List<SalesRep>> getSalesRepList()
        //{
        //    var salesRepsTask = Task.Run(() =>
        //    {
        //        var salesRepList = salesRepService.GetAll().ToList();
        //        return salesRepList;
        //    });

        //    return salesRepsTask;
        //}

        //private Task<List<Customer>> getCustomerList()
        //{
        //    var customersTask = Task.Run(() =>
        //    {
        //        var customerList = customerService.GetAllIncluding(IncludeProperties: c => c.ShipAddresses).ToList();
        //        return customerList;
        //    });

        //    return customersTask;
        //}

        #endregion populate dropdowns

        private async void ddlCustomers_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var selectedCustomer = ddlCustomers.SelectedItem as Customer;
            CurrentCustomer = selectedCustomer;

            var quoteRequestServiceForPriceLevel = new QuoteRequestService();
            var latestPriceLevel = await quoteRequestServiceForPriceLevel.Get().LatestPriceLevelAssigned(CurrentCustomer.Id);
            if (latestPriceLevel != 0)
            {
                var result = MessageBox.Show($"Do you want use the customer latest's price level of {latestPriceLevel}%?", "Quotation System", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                    txtPriceLevel.Text = latestPriceLevel.ToString();
            }
        }

        private void ddlShipAddresses_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var selectedShipAddress = ddlShipAddresses.SelectedItem as ShipAddress;
            if (selectedShipAddress != null)
                DisplayShipAddressField(selectedShipAddress.FullAddress);
        }

        private async void btnCreateQuote_Click(object sender, EventArgs e)
        {
            var quoteRequest = new QuoteRequest();
            var quoteIsValid = ValidateAndPopulateQuote(quoteRequest);

            if (quoteIsValid)
            {
                btnCreateQuote.Enabled = false;
                btnRemoveSystem.Enabled = false;
                btnSteelMaterials.Enabled = false;
                mnNewQuote.Enabled = false;

                var boom = populateNewBoomForQuote(quoteRequest);
                LoadingForm.Show("Creating Quote...");

                quoteRequestService.Add(quoteRequest);
                await _context.SaveChangesAsync();

                var quoteSystemResult = await quoteBoomSystemService.CreateQuoteBoomSystem(quoteRequest, boom);

                quoteRequest.QuoteNumber = await quoteRequestService.CreateQuoteNumber(quoteRequest);

                await _context.SaveChangesAsync();

                var sugarCRMClient = new SugarCRMClient();
                await sugarCRMClient.ExportQuoteToSugarCRM(quoteRequest.Id);

                CurrentQuoteRequest = quoteRequest;

                if (quoteSystemResult.CalculationErrors.Count == 0)
                {
                    refreshQuoteSystemsList(quoteRequest.Id);
                    lstDetailQuotes.SelectedItem = quoteSystemResult.System;
                    showDetailQuoteForm(quoteSystemResult.System, quoteRequest, boom);
                    CurrentBoom = boom;
                }
                else
                {
                    ShowErrors(quoteSystemResult.CalculationErrors);
                }

                LoadingForm.Hide();

                btnSteelMaterials.Enabled = true;
                btnCreateQuote.Enabled = true;
                btnRemoveSystem.Enabled = true;
                mnNewQuote.Enabled = true;
            }
        }

        private void ShowErrors(List<string> CalculationErrors)
        {
            var errorList = string.Join(DoubleNewLine, CalculationErrors);

            BeginInvoke((MethodInvoker)delegate
            {
                FlexibleMessageBox.Show("The following formulas had errors: " + DoubleNewLine + errorList, "Quotation System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            });
        }

        private async void btnUpdateQuote_Click(object sender, EventArgs e)
        {
            var formIsValid = ValidateChildren();

            if (formIsValid)
            {
                populateMainValuesForSaving(CurrentQuoteRequest);
                //User might want to update the quote information but there is no system selected or existing so skip this code
                if (CurrentBoom != null)
                {
                    populateGreenhouseValues(CurrentBoom);

                    if (CurrentBoom.BoomTypeId == 1)
                        populateEchoBoom(CurrentBoom);
                    else if (CurrentBoom.BoomTypeId == 2)
                        populateSRBBoom(CurrentBoom);
                    else if (CurrentBoom.BoomTypeId == 3)
                        populateDRBoom(CurrentBoom);
                    else if (CurrentBoom.BoomTypeId == 4)
                        populateCWFDRBoom(CurrentBoom);
                    else if (CurrentBoom.BoomTypeId == 5)
                        populateTowerBoom(CurrentBoom);
                    else if (CurrentBoom.BoomTypeId == 6)
                        populateNavigatorBoom(CurrentBoom);
                    else if (CurrentBoom.BoomTypeId == 7)
                        populateGRBoom(CurrentBoom);
                    else if (CurrentBoom.BoomTypeId == 8)
                        populateSRBoom(CurrentBoom);
                }

                //Save the user values first, later on if calculations fail, the calculations will NOT be saved
                await _context.SaveChangesAsync();
                List<string> CalculationErrors = new List<string>();
                var activeSystems = CurrentQuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.Where(quoteBoomSystem => quoteBoomSystem.Active);
                foreach (var quoteBoomSystem in activeSystems)
                {
                    quoteBoomSystem.Quantity = quoteBoomSystem.Boom.QuantityOfSystems;
                    CalculationErrors = quoteBoomSystemService.PerformDetailQuoteCalculations(quoteBoomSystem,
                        CurrentQuoteRequest);
                }

                CurrentQuoteRequest.QuoteBoomSystemsData.Update();

                if (CalculationErrors.Count == 0)
                {
                    LoadingForm.Show("Updating Quote...");

                    var updateQuoteTask = _context.SaveChangesAsync();

                    refreshQuoteSystemsList(CurrentQuoteRequest.Id);

                    await updateQuoteTask;

                    var sugarCRMClient = new SugarCRMClient();
                    await sugarCRMClient.ExportQuoteToSugarCRM(CurrentQuoteRequest.Id);

                    refreshMainQuote();

                    //If the quote was opened through the search quote form, then any changes
                    //saved here should be reflected there as well
                    if (OnSaveFinished != null)
                    {
                        OnSaveFinished(CurrentQuoteRequest, showLoadingForm: false, loadingFormMessage: "");
                    }
                }
                else
                {
                    ShowErrors(CalculationErrors);
                }

                LoadingForm.Hide();

                if (CalculationErrors.Count == 0)
                    MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private async void btnAddSystem_Click(object sender, EventArgs e)
        {
            var quoteIsValid = ValidateAndPopulateQuote(CurrentQuoteRequest);

            if (quoteIsValid)
            {
                btnSteelMaterials.Enabled = false;
                btnUpdateQuote.Enabled = false;
                btnAddSystem.Enabled = false;
                btnAddCustomItems.Enabled = false;
                btnRemoveSystem.Enabled = false;
                btnGenerateQuoteReport.Enabled = false;
                btnSendEmail.Enabled = false;
                mnNewQuote.Enabled = false;

                var boom = populateNewBoomForQuote(CurrentQuoteRequest);
                LoadingForm.Show("Adding System...");

                var quoteSystemResult = await quoteBoomSystemService.CreateQuoteBoomSystem(CurrentQuoteRequest, boom);

                if (quoteSystemResult.CalculationErrors.Count == 0)
                {
                    var sugarCRMClient = new SugarCRMClient();
                    await sugarCRMClient.ExportQuoteToSugarCRM(CurrentQuoteRequest.Id);

                    refreshQuoteSystemsList(CurrentQuoteRequest.Id);
                    lstDetailQuotes.SelectedItem = quoteSystemResult.System;
                    showDetailQuoteForm(quoteSystemResult.System, CurrentQuoteRequest, CurrentBoom);
                    CurrentBoom = boom;

                    //If the quote was opened through the search quote form, then any changes
                    //saved here should be reflected there as well
                    if (OnSaveFinished != null)
                    {
                        OnSaveFinished(CurrentQuoteRequest, showLoadingForm: false, loadingFormMessage: "");
                    }
                }
                else
                {
                    ShowErrors(quoteSystemResult.CalculationErrors);
                }

                btnSteelMaterials.Enabled = true;
                btnUpdateQuote.Enabled = true;
                btnAddSystem.Enabled = true;
                btnAddCustomItems.Enabled = true;
                btnRemoveSystem.Enabled = true;
                btnGenerateQuoteReport.Enabled = true;
                btnSteelMaterials.Enabled = true;
                btnSendEmail.Enabled = true;
                mnNewQuote.Enabled = true;

                LoadingForm.Hide();
            }
        }

        private async void btnGenerateQuoteReport_Click(object sender, EventArgs e)
        {
            var fileNameResult = await CreateQuoteReport();
            Process.Start(fileNameResult);
        }

        private Task<string> CreateReportGetFileName()
        {
            var generateReportTask = Task.Run(async () =>
            {
                var reportManager = new ReportManager();
                var fileName = await reportManager.GenerateMainQuoteReportFile(CurrentQuoteRequest.Id);
                return fileName;
            });

            return generateReportTask;
        }

        private async Task<string> CreateQuoteReport()
        {
            LoadingForm.Show("Generating Quote Report...");

            var generateReportTask = CreateReportGetFileName();

            LoadingForm.Hide();

            var fileNameResult = await generateReportTask;
            return fileNameResult;
        }

        private bool ValidateAndPopulateQuote(QuoteRequest quoteRequest)
        {
            var formIsValid = ValidateChildren();
            if (!formIsValid) return false;

            populateMainValuesForSaving(quoteRequest);

            return true;
        }

        private async void btnRemoveSystem_Click(object sender, EventArgs e)
        {
            var selectedQuoteBoomSystem = lstDetailQuotes.SelectedItem as QuoteBoomSystem;

            if (selectedQuoteBoomSystem != null)
            {
                var result = MessageBox.Show($"Do you want to delete the {selectedQuoteBoomSystem.SystemName}?", "Quotation System", MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {
                    await quoteBoomSystemService.RemoveQuoteBoomSystem(CurrentQuoteRequest, selectedQuoteBoomSystem.Id_);
                    refreshQuoteSystemsList(CurrentQuoteRequest.Id);

                    //If the quote was opened through the search quote form, then any changes
                    //saved here should be reflected there as well
                    OnSaveFinished?.Invoke(CurrentQuoteRequest, showLoadingForm: false, loadingFormMessage: "");

                    btnRemoveSystem.Enabled = false;
                    btnGenerateSalesOrderForm.Enabled = false;
                    btnSteelMaterials.Enabled = false;

                    if (lstDetailQuotes.Items.Count == 0)
                    {
                        btnGenerateQuoteReport.Enabled = false;
                        btnSendEmail.Enabled = false;
                        btnUpdateQuote.Enabled = false;
                    }
                }
            }
        }

        private Boom populateNewBoomForQuote(QuoteRequest quoteRequest)
        {
            Boom boom = new Boom();

            switch (SelectedBoomType)
            {
                case BoomType.Echo:
                    populateEchoBoom(boom);
                    boom.BoomTypeId = (int)BoomType.Echo;
                    break;

                case BoomType.SingleRail:
                    populateSRBBoom(boom);
                    boom.BoomTypeId = (int)BoomType.SingleRail;
                    break;

                case BoomType.DoubleRail:
                    populateDRBoom(boom);
                    boom.BoomTypeId = (int)BoomType.DoubleRail;
                    break;

                case BoomType.CWFDoubleRail:
                    populateCWFDRBoom(boom);
                    boom.BoomTypeId = (int)BoomType.CWFDoubleRail;
                    break;

                case BoomType.Tower:
                    populateTowerBoom(boom);
                    boom.BoomTypeId = (int)BoomType.Tower;
                    break;

                case BoomType.Navigator:
                    populateNavigatorBoom(boom);
                    boom.BoomTypeId = (int)BoomType.Navigator;
                    break;

                case BoomType.GroundRunner:
                    populateGRBoom(boom);
                    boom.BoomTypeId = (int)BoomType.GroundRunner;
                    break;

                case BoomType.SkyRail:
                    populateSRBoom(boom);
                    boom.BoomTypeId = (int)BoomType.SkyRail;
                    break;
            }

            populateGreenhouseValues(boom);

            return boom;
        }

        private void populateGreenhouseValues(Boom boom)
        {
            boom.AdditionalInformation = txtAdditionalInformation.Text.Trim();
            boom.CustomerNote = txtCustomerNote.Text.Trim();
            boom.GreenHouseTypeId = Convert.ToInt32(ddlGreenhouseType.SelectedValue);
            boom.NumberOfBays = Convert.ToInt32(txtNumberOfBays.Text);
            boom.BayLengthInFeet = Convert.ToDouble(txtBayLength.Text);
            boom.BayWidthInFeet = Convert.ToDouble(txtBayWidth.Text);
            boom.NumberOfBaySideWalls = Convert.ToInt32(txtNumberOfBaySidewalls.Text);
            boom.WalkwayWidthInInches = Convert.ToDouble(txtWalkwayWidth.Text);
            boom.ClearanceWidthInInches = Convert.ToDouble(txtClearanceWidth.Text);
            boom.BottomCordHeightInFeet = Convert.ToDouble(txtBottomCordHeight.Text);
            boom.TrussSpacingInFeet = Convert.ToDouble(txtTrussSpacing.Text);
            boom.BottomCordType = getSelectedRadioButtonValue(pnlBottomCordType);
            boom.BottomCordSize = txtBottomCordSize.Text;
            boom.GrowingSurfaceHeightInInches = Convert.ToDouble(txtGrowingSurfaceHeight.Text);
            boom.PostSpacingInFeet = Convert.ToDouble(txtPostSpacing.Text);
            boom.PostDimensionsInInchesSquared = txtPostDimensions.Text;
            boom.SprayBarSize = Convert.ToDouble(txtSpraybarSize.Text);
            boom.HoseSize = Convert.ToDouble(txtHoseSize.Text);
            boom.Location = txtLocation.Text;
        }

        #region create booms

        private void populateSRBoom(Boom boom)
        {
            boom.QuantityOfSystems = Convert.ToInt32(txtSRQuantityOfSystems.Text);
            boom.HoseLoopHeightInInches = Convert.ToDouble(txtSRHoseLoopHeight.Text);
            boom.SingleOrDualWater = ddlSRSingleDualWater.SelectedItem.ToString();
            boom.NumberOfRows = Convert.ToInt32(txtSRNumberOfRows.Text);

            string radioButtonValue;

            radioButtonValue = getSelectedRadioButtonValue(pnlSREV);
            var ev = radioButtonValue.ToBoolean();
            boom.EvVpd = ev;

            boom.Tip1 = txtSRTip1.Text;
            boom.Tip2 = txtSRTip2.Text;
            boom.Tip3 = txtSRTip3.Text;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRTripleWaterLocklineUpgrade);
            var tripleWaterLocklineUpgradeRequested = radioButtonValue.ToBoolean();
            boom.TripleStepLocklineUpgradeRequested = tripleWaterLocklineUpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRTripleTurretBody);
            var tripleTurretBodyRequested = radioButtonValue.ToBoolean();
            boom.TripleTurretBodyRequested = tripleTurretBodyRequested;

            boom.ControllerType = ddlSRControllerType.SelectedItem.ToString();

            radioButtonValue = getSelectedRadioButtonValue(pnlSRSweep90);
            var sweep90DesreeRequested = radioButtonValue.ToBoolean();
            boom.Sweep90Requested = sweep90DesreeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRPressureRegulator);
            var pressureRegulatorRequested = radioButtonValue.ToBoolean();
            boom.PressureRegulatorRequested = pressureRegulatorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRMountedInjector);
            var mountedInjectorRequested = radioButtonValue.ToBoolean();
            boom.MountedInjectorRequested = mountedInjectorRequested;

            boom.RailDropDownASSYInInches = Convert.ToDouble(txtSRRailDropDownAssy.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlSR1_1_4thHoseUpgrade);
            var hose_1_1_4UpgradeRequested = radioButtonValue.ToBoolean();
            boom.HoseUpgradeRequested = hose_1_1_4UpgradeRequested;

            boom.SprayBodySpacingInInches = Convert.ToDouble(txtSRSpraybodySpacing.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlSRWirelessWalkSwitch);
            var wirelessWalkSwitchRequested = radioButtonValue.ToBoolean();
            boom.WirelessWalkSwitchRequested = wirelessWalkSwitchRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRWaterSolenoidControl);
            var waterSolenoidControlRequested = radioButtonValue.ToBoolean();
            boom.WaterSolenoidControlRequested = waterSolenoidControlRequested;

            boom.InjectorType = ddlSRInjectorType.SelectedItem.ToSafeString();
            boom.EVWireGauge = ddlSREVWireGauge.SelectedItem.ToSafeString();

            radioButtonValue = getSelectedRadioButtonValue(pnlSRAutoInjector);
            var autoInjectorRequested = radioButtonValue.ToBoolean();
            boom.AutoInjectorRequested = autoInjectorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRWaterFeedSystem);
            var waterFeedSystemRequested = radioButtonValue.ToBoolean();
            boom.CenterWaterFeedSystemRequested = waterFeedSystemRequested;
        }

        private void populateGRBoom(Boom boom)
        {
            boom.QuantityOfSystems = Convert.ToInt32(txtGRQuantityOfSystems.Text);
            boom.HoseLoopHeightInInches = Convert.ToDouble(txtGRHoseLoopHeight.Text);
            boom.SingleOrDualWater = ddlGRSingleDualWater.SelectedItem.ToString();
            boom.NumberOfRows = Convert.ToInt32(txtGRNumberOfRows.Text);

            string radioButtonValue;

            radioButtonValue = getSelectedRadioButtonValue(pnlGREV);
            var ev = radioButtonValue.ToBoolean();
            boom.EvVpd = ev;

            boom.Tip1 = txtGRTip1.Text;
            boom.Tip2 = txtGRTip2.Text;
            boom.Tip3 = txtGRTip3.Text;

            radioButtonValue = getSelectedRadioButtonValue(pnlGRTripleWaterLocklineUpgrade);
            var tripleWaterLocklineUpgradeRequested = radioButtonValue.ToBoolean();
            boom.TripleStepLocklineUpgradeRequested = tripleWaterLocklineUpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlGRTripleTurretBody);
            var tripleTurretBodyRequested = radioButtonValue.ToBoolean();
            boom.TripleTurretBodyRequested = tripleTurretBodyRequested;

            boom.ControllerType = ddlGRControllerType.SelectedItem.ToString();

            radioButtonValue = getSelectedRadioButtonValue(pnlGRSweep90);
            var sweep90DegreeRequested = radioButtonValue.ToBoolean();
            boom.Sweep90Requested = sweep90DegreeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlGRPressureRegulator);
            var pressureRegulatorRequested = radioButtonValue.ToBoolean();
            boom.PressureRegulatorRequested = pressureRegulatorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlGRMountedInjector);
            var mountedInjectorRequested = radioButtonValue.ToBoolean();
            boom.MountedInjectorRequested = mountedInjectorRequested;

            boom.RailDropDownASSYInInches = Convert.ToDouble(txtGRRailDropDownAssy.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlGR1_1_4thHoseUpgrade);
            var hose_1_1_4UpgradeRequested = radioButtonValue.ToBoolean();
            boom.HoseUpgradeRequested = hose_1_1_4UpgradeRequested;

            boom.SprayBodySpacingInInches = Convert.ToDouble(txtGRSpraybodySpacing.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlGRWirelessWalkSwitch);
            var wirelessWalkSwitchRequested = radioButtonValue.ToBoolean();
            boom.WirelessWalkSwitchRequested = wirelessWalkSwitchRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlGRWaterSolenoidControl);
            var waterSolenoidControlRequested = radioButtonValue.ToBoolean();
            boom.WaterSolenoidControlRequested = waterSolenoidControlRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlGRWaterFeedSystem);
            var waterFeedSystemRequested = radioButtonValue.ToBoolean();
            boom.CenterWaterFeedSystemRequested = waterFeedSystemRequested;

            boom.InjectorType = ddlGRInjectorType.SelectedItem.ToSafeString();
            boom.EVWireGauge = ddlGREVWireGauge.SelectedItem.ToSafeString();

            radioButtonValue = getSelectedRadioButtonValue(pnlGRAutoInjector);
            var autoInjectorRequested = radioButtonValue.ToBoolean();
            boom.AutoInjectorRequested = autoInjectorRequested;
        }

        private void populateNavigatorBoom(Boom boom)
        {
            boom.QuantityOfSystems = Convert.ToInt32(txtNavQuantityOfSystems.Text);
            boom.HoseLoopHeightInInches = Convert.ToDouble(txtNavHoseLoopHeight.Text);
            boom.SingleOrDualWater = ddlNavSingleDualWater.SelectedItem.ToString();
            boom.NumberOfRows = Convert.ToInt32(txtNavNumberOfRows.Text);
            boom.Feed = ddlNavFeed.Text;

            string radioButtonValue;

            radioButtonValue = getSelectedRadioButtonValue(pnlNavEV);
            var ev = radioButtonValue.ToBoolean();
            boom.EvVpd = ev;

            boom.Tip1 = txtNavTip1.Text;
            boom.Tip2 = txtNavTip2.Text;
            boom.Tip3 = txtNavTip3.Text;

            boom.ControllerType = ddlNavControllerType.SelectedItem.ToString();

            radioButtonValue = getSelectedRadioButtonValue(pnlNavTripleWaterLocklineUpgrade);
            var tripleWaterLocklineUpgradeRequested = radioButtonValue.ToBoolean();
            boom.TripleStepLocklineUpgradeRequested = tripleWaterLocklineUpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlNavTripleTurretBody);
            var tripleTurretBodyRequested = radioButtonValue.ToBoolean();
            boom.TripleTurretBodyRequested = tripleTurretBodyRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlNavPressureRegulator);
            var pressureRegulatorRequested = radioButtonValue.ToBoolean();
            boom.PressureRegulatorRequested = pressureRegulatorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlNavMountedInjector);
            var mountedInjectorRequested = radioButtonValue.ToBoolean();
            boom.MountedInjectorRequested = mountedInjectorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlNavSweep90);
            var sweep90Requested = radioButtonValue.ToBoolean();
            boom.Sweep90Requested = sweep90Requested;

            boom.SprayBodySpacingInInches = Convert.ToDouble(txtNavSpraybodySpacing.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlNavWirelessWalkSwitch);
            var wirelessWalkSwitchRequested = radioButtonValue.ToBoolean();
            boom.WirelessWalkSwitchRequested = wirelessWalkSwitchRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlNav1_1_4thHoseUpgrade);
            var hose_1_1_4UpgradeRequested = radioButtonValue.ToBoolean();
            boom.HoseUpgradeRequested = hose_1_1_4UpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlNavGallonTank);
            var gallonTankRequested = radioButtonValue.ToBoolean();
            boom.Gallon25ReservoirTankRequested = gallonTankRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlNavWaterFeedSystem);
            var waterFeedSystemRequested = radioButtonValue.ToBoolean();
            boom.CenterWaterFeedSystemRequested = waterFeedSystemRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlNavWaterSolenoidControl);
            var waterSolenoidControlRequested = radioButtonValue.ToBoolean();
            boom.WaterSolenoidControlRequested = waterSolenoidControlRequested;

            boom.CarrySteel = ddlNavCarrySteel.SelectedItem.ToString();
            boom.InjectorType = ddlNavInjectorType.SelectedItem.ToSafeString();
            boom.EVWireGauge = ddlNavEVWireGauge.SelectedItem.ToSafeString();

            radioButtonValue = getSelectedRadioButtonValue(pnlNavAutoInjector);
            var autoInjectorRequested = radioButtonValue.ToBoolean();
            boom.AutoInjectorRequested = autoInjectorRequested;
        }

        private void populateTowerBoom(Boom boom)
        {
            boom.QuantityOfSystems = Convert.ToInt32(txtCTQuantityOfSystems.Text);
            boom.HoseLoopHeightInInches = Convert.ToDouble(txtCTHoseLoopHeight.Text);
            boom.SingleOrDualWater = ddlCTSingleDualWater.SelectedItem.ToString();
            boom.NumberOfRows = Convert.ToInt32(txtCTNumberOfRows.Text);
            boom.Feed = ddlCTFeed.Text;

            string radioButtonValue;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTEV);
            var ev = radioButtonValue.ToBoolean();
            boom.EvVpd = ev;

            boom.Tip1 = txtCTTip1.Text;
            boom.Tip2 = txtCTTip2.Text;
            boom.Tip3 = txtCTTip3.Text;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTTripleWaterLocklineUpgrade);
            var tripleWaterLocklineUpgradeRequested = radioButtonValue.ToBoolean();
            boom.TripleStepLocklineUpgradeRequested = tripleWaterLocklineUpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTTripleTurretBody);
            var tripleTurretBodyRequested = radioButtonValue.ToBoolean();
            boom.TripleTurretBodyRequested = tripleTurretBodyRequested;

            boom.ControllerType = ddlCTControllerType.SelectedItem.ToString();

            radioButtonValue = getSelectedRadioButtonValue(pnlCTPressureRegulator);
            var pressureRegulatorRequested = radioButtonValue.ToBoolean();
            boom.PressureRegulatorRequested = pressureRegulatorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTMountedInjector);
            var mountedInjectorRequested = radioButtonValue.ToBoolean();
            boom.MountedInjectorRequested = mountedInjectorRequested;

            boom.SprayBodySpacingInInches = Convert.ToDouble(txtCTSpraybodySpacing.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlCTWirelessWalkSwitch);
            var wirelessWalkSwitchRequested = radioButtonValue.ToBoolean();
            boom.WirelessWalkSwitchRequested = wirelessWalkSwitchRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCT1_1_4thHoseUpgrade);
            var hose_1_1_4UpgradeRequested = radioButtonValue.ToBoolean();
            boom.HoseUpgradeRequested = hose_1_1_4UpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTDiamondSweep);
            var diamondSweepRequested = radioButtonValue.ToBoolean();
            boom.DiamondSweepRequested = diamondSweepRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTConveyor);
            var conveyorRequested = radioButtonValue.ToBoolean();
            boom.ConveyorRequested = conveyorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTDualMotorNoAxle);
            var dualMotorNoAxleRequested = radioButtonValue.ToBoolean();
            boom.DualMotorNoAxleRequested = dualMotorNoAxleRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTGallonTank);
            var gallonTankRequested = radioButtonValue.ToBoolean();
            boom.Gallon25ReservoirTankRequested = gallonTankRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTWaterFeedSystem);
            var waterFeedSystemRequested = radioButtonValue.ToBoolean();
            boom.CenterWaterFeedSystemRequested = waterFeedSystemRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCTWaterSolenoidControl);
            var waterSolenoidControlRequested = radioButtonValue.ToBoolean();
            boom.WaterSolenoidControlRequested = waterSolenoidControlRequested;

            boom.CarrySteel = ddlCTCarrySteel.SelectedItem.ToString();
            boom.InjectorType = ddlCTInjectorType.SelectedItem.ToSafeString();
            boom.EVWireGauge = ddlCTEVWireGauge.SelectedItem.ToSafeString();

            radioButtonValue = getSelectedRadioButtonValue(pnlCTAutoInjector);
            var autoInjectorRequested = radioButtonValue.ToBoolean();
            boom.AutoInjectorRequested = autoInjectorRequested;
        }

        private void populateCWFDRBoom(Boom boom)
        {
            boom.QuantityOfSystems = Convert.ToInt32(txtCWFDRBQuantityOfSystems.Text);
            boom.SingleOrDualWater = ddlCWFDRBSingleDualWater.SelectedItem.ToString();
            boom.NumberOfRows = Convert.ToInt32(txtCWFDRBNumberOfRows.Text);
            boom.Feed = ddlCWFDRBFeed.Text;

            string radioButtonValue;

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBEV);
            var ev = radioButtonValue.ToBoolean();
            boom.EvVpd = ev;

            boom.Tip1 = txtCWFDRBTip1.Text;
            boom.Tip2 = txtCWFDRBTip2.Text;
            boom.Tip3 = txtCWFDRBTip3.Text;

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBTripleWaterLocklineUpgrade);
            var tripleWaterLocklineUpgradeRequested = radioButtonValue.ToBoolean();
            boom.TripleStepLocklineUpgradeRequested = tripleWaterLocklineUpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBTripleTurretBody);
            var tripleTurretBodyRequested = radioButtonValue.ToBoolean();
            boom.TripleTurretBodyRequested = tripleTurretBodyRequested;

            boom.ControllerType = ddlCWFDRBControllerType.SelectedItem.ToString();

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBPressureRegulator);
            var pressureRegulatorRequested = radioButtonValue.ToBoolean();
            boom.PressureRegulatorRequested = pressureRegulatorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBMountedInjector);
            var mountedInjectorRequested = radioButtonValue.ToBoolean();
            boom.MountedInjectorRequested = mountedInjectorRequested;

            boom.RailDropDownASSYInInches = Convert.ToDouble(txtCWFDRBRailDropDownAssy.Text);

            boom.SprayBodySpacingInInches = Convert.ToDouble(txtCWFDRBSpraybodySpacing.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBWirelessWalkSwitch);
            var wirelessWalkSwitchRequested = radioButtonValue.ToBoolean();
            boom.WirelessWalkSwitchRequested = wirelessWalkSwitchRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBWaterSolenoidControl);
            var waterSolenoidControlRequested = radioButtonValue.ToBoolean();
            boom.WaterSolenoidControlRequested = waterSolenoidControlRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRB1_1_4thHoseUpgrade);
            var hose_1_1_4UpgradeRequested = radioButtonValue.ToBoolean();
            boom.HoseUpgradeRequested = hose_1_1_4UpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBWaterFeedSystem);
            var waterFeedSystemRequested = radioButtonValue.ToBoolean();
            boom.CenterWaterFeedSystemRequested = waterFeedSystemRequested;

            boom.InjectorType = ddlCWFDRBInjectorType.SelectedItem.ToSafeString();
            boom.EVWireGauge = ddlCWFDRBEVWireGauge.SelectedItem.ToSafeString();

            radioButtonValue = getSelectedRadioButtonValue(pnlCWFDRBAutoInjector);
            var autoInjectorRequested = radioButtonValue.ToBoolean();
            boom.AutoInjectorRequested = autoInjectorRequested;
        }

        private void populateDRBoom(Boom boom)
        {
            boom.QuantityOfSystems = Convert.ToInt32(txtDRBQuantityOfSystems.Text);
            boom.HoseLoopHeightInInches = Convert.ToDouble(txtDRBHoseLoopHeight.Text);
            boom.SingleOrDualWater = ddlDRBSingleDualWater.SelectedItem.ToString();
            boom.NumberOfRows = Convert.ToInt32(txtDRBNumberOfRows.Text);

            string radioButtonValue;

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBEV);
            var ev = radioButtonValue.ToBoolean();
            boom.EvVpd = ev;

            boom.Tip1 = txtDRBTip1.Text;
            boom.Tip2 = txtDRBTip2.Text;
            boom.Tip3 = txtDRBTip3.Text;

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBTripleWaterLocklineUpgrade);
            var tripleWaterLocklineUpgradeRequested = radioButtonValue.ToBoolean();
            boom.TripleStepLocklineUpgradeRequested = tripleWaterLocklineUpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBTripleTurretBody);
            var tripleTurretBodyRequested = radioButtonValue.ToBoolean();
            boom.TripleTurretBodyRequested = tripleTurretBodyRequested;

            boom.ControllerType = ddlDRBControllerType.SelectedItem.ToString();

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBSweep90);
            var sweep90DegreeRequested = radioButtonValue.ToBoolean();
            boom.Sweep90Requested = sweep90DegreeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBPressureRegulator);
            var pressureRegulatorRequested = radioButtonValue.ToBoolean();
            boom.PressureRegulatorRequested = pressureRegulatorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBMountedInjector);
            var mountedInjectorRequested = radioButtonValue.ToBoolean();
            boom.MountedInjectorRequested = mountedInjectorRequested;

            boom.RailDropDownASSYInInches = Convert.ToDouble(txtDRBRailDropDownAssy.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlDRB1_1_4thHoseUpgrade);
            var hose_1_1_4UpgradeRequested = radioButtonValue.ToBoolean();
            boom.HoseUpgradeRequested = hose_1_1_4UpgradeRequested;

            boom.SprayBodySpacingInInches = Convert.ToDouble(txtDRBSpraybodySpacing.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBWirelessWalkSwitch);
            var wirelessWalkSwitchRequested = radioButtonValue.ToBoolean();
            boom.WirelessWalkSwitchRequested = wirelessWalkSwitchRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBWaterSolenoidControl);
            var waterSolenoidControlRequested = radioButtonValue.ToBoolean();
            boom.WaterSolenoidControlRequested = waterSolenoidControlRequested;

            boom.InjectorType = ddlDRBInjectorType.SelectedItem.ToSafeString();
            boom.EVWireGauge = ddlDRBEVWireGauge.SelectedItem.ToSafeString();

            radioButtonValue = getSelectedRadioButtonValue(pnlDRBAutoInjector);
            var autoInjectorRequested = radioButtonValue.ToBoolean();
            boom.AutoInjectorRequested = autoInjectorRequested;
        }

        private void populateSRBBoom(Boom boom)
        {
            boom.QuantityOfSystems = Convert.ToInt32(txtSRBQuantityOfSystems.Text);
            boom.HoseLoopHeightInInches = Convert.ToDouble(txtSRBHoseLoopHeight.Text);
            boom.SingleOrDualWater = ddlSRBSingleDualWater.SelectedItem.ToString();
            boom.NumberOfRows = Convert.ToInt32(txtSRBNumberOfRows.Text);
            boom.Feed = ddlSRBFeed.Text;

            string radioButtonValue;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBEV);
            var ev = radioButtonValue.ToBoolean();
            boom.EvVpd = ev;

            boom.Tip1 = txtSRBTip1.Text;
            boom.Tip2 = txtSRBTip2.Text;
            boom.Tip3 = txtSRBTip3.Text;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBTripleWaterLocklineUpgrade);
            var tripleWaterLocklineUpgradeRequested = radioButtonValue.ToBoolean();
            boom.TripleStepLocklineUpgradeRequested = tripleWaterLocklineUpgradeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBTripleTurretBody);
            var tripleTurretBodyRequested = radioButtonValue.ToBoolean();
            boom.TripleTurretBodyRequested = tripleTurretBodyRequested;

            boom.ControllerType = ddlSRBControllerType.SelectedItem.ToString();

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBSweep90Degree);
            var sweep90DegreeRequested = radioButtonValue.ToBoolean();
            boom.Sweep90Requested = sweep90DegreeRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBPressureRegulator);
            var pressureRegulatorRequested = radioButtonValue.ToBoolean();
            boom.PressureRegulatorRequested = pressureRegulatorRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBMountedInjector);
            var mountedInjectorRequested = radioButtonValue.ToBoolean();
            boom.MountedInjectorRequested = mountedInjectorRequested;

            boom.SprayBodySpacingInInches = Convert.ToDouble(txtSRBSpraybodySpacing.Text);

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBAMIADFilderAssy);
            var amiadFilterAssyRequested = radioButtonValue.ToBoolean();
            boom.AMIADFilterASSYRequested = amiadFilterAssyRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBWaterSolenoidControl);
            var waterSolenoidControlRequested = radioButtonValue.ToBoolean();
            boom.WaterSolenoidControlRequested = waterSolenoidControlRequested;

            boom.InjectorType = ddlSRBInjectorType.SelectedItem.ToSafeString();
            boom.EVWireGauge = ddlSRBEVWireGauge.SelectedItem.ToSafeString();

            radioButtonValue = getSelectedRadioButtonValue(pnlSRBAutoInjector);
            var autoInjectorRequested = radioButtonValue.ToBoolean();
            boom.AutoInjectorRequested = autoInjectorRequested;
        }

        private void populateEchoBoom(Boom boom)
        {
            boom.QuantityOfSystems = Convert.ToInt32(txtEchoQuantityOfSystems.Text);
            boom.DrumSize = Convert.ToInt32(ddlEchoDrumSize.SelectedValue);
            boom.BasketSpacing = Convert.ToDouble(txtEchoBasketSpacing.Text);
            boom.ControllerType = ddlEchoControllerType.Text;
            boom.NumberOfLayers = Convert.ToInt32(ddlEchoNumberOfLayers.SelectedValue);
            boom.Feed = ddlEchoFeed.Text;

            string radioButtonValue;

            radioButtonValue = getSelectedRadioButtonValue(pnlEchoEV);
            var ev = radioButtonValue.ToBoolean();
            boom.EvVpd = ev;

            radioButtonValue = getSelectedRadioButtonValue(pnlEchoCenterBayWaterStation);
            var centerBayWaterStationRequested = radioButtonValue.ToBoolean();
            boom.CenterBayWaterStationRequested = centerBayWaterStationRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlEchoAMIADFilterAssembly);
            var AMIADFilterAssemblyRequested = radioButtonValue.ToBoolean();
            boom.AMIADFilterAssemblyRequested = AMIADFilterAssemblyRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlEchoLockline);
            var echoLockLineRequested = radioButtonValue.ToBoolean();
            boom.LocklineRequested = echoLockLineRequested;

            boom.ExtensionHangerLengthInInches = Convert.ToDouble(txtEchoExtensionHangerLength.Text);
            boom.PulleyBracketSpacingInFeet = Convert.ToDouble(ddlEchoPulleyBracketSpacing.SelectedValue);

            radioButtonValue = getSelectedRadioButtonValue(pnlEchoRemotePullChainSwitch);
            var remotePullChainSwitchRequested = radioButtonValue.ToBoolean();
            boom.RemotePullChainSwitchRequested = remotePullChainSwitchRequested;

            radioButtonValue = getSelectedRadioButtonValue(pnlEchoRedheadBreakerUpgrade);
            var redheadBreakerUpgrade = radioButtonValue.ToBoolean();
            boom.DRAMMRedheadBreakerUpgradeRequested = redheadBreakerUpgrade;

            radioButtonValue = getSelectedRadioButtonValue(pnlEchoWaterSolenoidControl);
            var waterSolenoidControlRequested = radioButtonValue.ToBoolean();
            boom.WaterSolenoidControlRequested = waterSolenoidControlRequested;
        }

        #endregion create booms

        private void showDetailQuoteForm(QuoteBoomSystem quoteBoomSystem, QuoteRequest quoteRequest, Boom boom)
        {
            var detailQuoteForm = new DetailQuoteForm(_context, quoteBoomSystem, quoteRequest, boom);
            detailQuoteForm.OnSaveFinished += refreshQuoteSystemsList;
            detailQuoteForm.OnSaveFinished += detailedQuoteSaved;
            detailQuoteForm.CreateComponentControls();
            detailQuoteForm.Show();
        }

        private void detailedQuoteSaved(int? quoteRequest)
        {
            if (OnSaveFinished != null)
            {
                OnSaveFinished(CurrentQuoteRequest, showLoadingForm: false, loadingFormMessage: "");
            }
        }

        private void refreshQuoteSystemsList(int? quoteRequestId)
        {
            //Get current selected item
            QuoteBoomSystem detailQuoteSelectedItem = null;
            if (lstDetailQuotes.Items.Count > 0)
            {
                detailQuoteSelectedItem = lstDetailQuotes.SelectedItem as QuoteBoomSystem;
            }

            //Refresh list
            lstDetailQuotes.Items.Clear();
            if (quoteRequestId.HasValue && CurrentQuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems != null)
            {
                var currentQuoteBoomSystems = CurrentQuoteRequest.QuoteBoomSystemsData.QuoteBoomSystems.Where(s => s.Active).OrderBy(q => q.Order);
                int listNumber = 1;
                foreach (var quoteBoomSystem in currentQuoteBoomSystems)
                {
                    quoteBoomSystem.Number = listNumber;
                    lstDetailQuotes.Items.Add(quoteBoomSystem);
                    listNumber++;
                }

                //Restore selected item
                if (currentQuoteBoomSystems.Contains(detailQuoteSelectedItem))
                {
                    lstDetailQuotes.SelectedItem = detailQuoteSelectedItem;
                }
                else
                {
                    CurrentBoom = null;
                }

                refreshMainQuote();
            }
        }

        private void refreshMainQuote()
        {
            if (CurrentQuoteRequest != null)
            {
                txtQuoteNumber.Text = CurrentQuoteRequest.QuoteNumberWithRevision;
                txtQuoteTotal.Text = CurrentQuoteRequest.GetTotalCost().ToString("C2");
            }
        }

        private void populateMainValuesForSaving(QuoteRequest quoteRequest)
        {
            var selectedCustomerId = ((Customer)ddlCustomers.SelectedItem).Id;
            var selectedShipAddressId = (int?)ddlShipAddresses.SelectedValue;
            var selectedSalesRepId = (int)ddlSalesReps.SelectedValue;
            var selectedQuoteStatusId = (int)ddlQuoteStatus.SelectedValue;
            var selectedDistributorId = (int)ddlDistributors.SelectedValue;

            quoteRequest.ContactName = txtContactName.Text;
            quoteRequest.Phone = txtContactPhone.Text;
            quoteRequest.Fax = txtContactFax.Text;
            quoteRequest.Email = txtContactEmail.Text;
            quoteRequest.FullAddress = txtShipAddress.Text;

            //Check if status changed
            if (selectedQuoteStatusId != quoteRequest.QuoteStatusId)
            {
                quoteRequest.StatusDate = DateTime.Now;
            }

            var containsRevisionNumber = txtQuoteNumber.Text.EndsWith("-" + quoteRequest.Revision);
            var quoteNumber = txtQuoteNumber.Text;
            if (containsRevisionNumber)
            {
                var dashIndex = quoteNumber.LastIndexOf("-");
                quoteNumber = quoteNumber.Substring(0, dashIndex);
            }

            //Update to new status
            quoteRequest.QuoteStatusId = selectedQuoteStatusId;
            quoteRequest.Revision++;

            quoteRequest.CustomerId = selectedCustomerId;
            quoteRequest.ShipAddressId = selectedShipAddressId;
            quoteRequest.SalesRepId = selectedSalesRepId;
            quoteRequest.DistributorId = selectedDistributorId;
            quoteRequest.LeadTime = txtLeadTime.Text;
            quoteRequest.SteelEstimate = txtSteelEstimate.Text;
            quoteRequest.FreightEstimate = txtFreightEstimate.Text;
            quoteRequest.InstallationEstimate = txtInstallationEstimate.Text;
            quoteRequest.Discount = txtDiscount.Text;
            quoteRequest.PriceLevel = Convert.ToDouble(txtPriceLevel.Text);
            quoteRequest.PurchaseOrder = txtPurchaseOrder.Text;
            quoteRequest.OrderDate = dtOrderConfirmed.Value;
            quoteRequest.DueDate = txtDueDate.Value;
            quoteRequest.Active = true;

            quoteRequest.QuoteNumber = quoteNumber;
        }

        private static string getSelectedRadioButtonValue(Panel panel)
        {
            var value = ControlUtilities.GetSelectedRadioButtonValueInPanel(panel);
            return value;
        }

        #region validation

        /// <summary>
        /// Contains code to specificy validation for a field for a specific BOOM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidateGreaterThanZero(object sender, CancelEventArgs e)
        {
            var textBox = sender as RadMaskedEditBox;
            var isValidatedInCurrentBoom = false;
            if (textBox.Tag != null)
            {
                var validationBoomTypesString = textBox.Tag as string;
                var validationBoomTypesList = validationBoomTypesString.Split(',');
                isValidatedInCurrentBoom = validationBoomTypesList.Contains(SelectedBoomType.ToString());
            }

            if (!isValidatedInCurrentBoom)
            {
                greaterThanZeroErrorProvider.SetError(textBox, "");
            }
            else
            {
                ValidationUtilities.ValidateGreaterThanZero(textBox, greaterThanZeroErrorProvider, e);
            }
        }

        private void ValidateDropdownSelectedValue(object sender, CancelEventArgs e)
        {
            var dropDown = sender as ComboBox;
            var isValidatedInCurrentBoom = false;
            if (dropDown.Tag != null)
            {
                var validationBoomTypesString = dropDown.Tag as string;
                var validationBoomTypesList = validationBoomTypesString.Split(',');
                isValidatedInCurrentBoom = validationBoomTypesList.Contains(SelectedBoomType.ToString());
            }

            if (!isValidatedInCurrentBoom)
            {
                dropdownValueErrorProvider.SetError(dropDown, "");
            }
            else
            {
                ValidationUtilities.ValidateDropdownSelectedValue(dropDown, dropdownValueErrorProvider, e);
            }
        }

        /// <summary>
        /// Called when the form is validating all child controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidateRadioOptionSelected(object sender, CancelEventArgs e)
        {
            var radioButton = sender as RadioButton;
            ValidateRadioOptionSelected(radioButton, e);
        }

        /// <summary>
        /// Calls validation when a radiobutton is checked (to clear validation error)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidateRadioOptionSelected(object sender, EventArgs e)
        {
            var radioButton = sender as RadioButton;
            ValidateRadioOptionSelected(radioButton, null);
        }

        private void ValidateRadioOptionSelected(RadioButton sender, CancelEventArgs e)
        {
            var radioButton = sender;
            var isValidatedInCurrentBoom = false;
            if (radioButton.Tag != null)
            {
                var validationBoomTypesString = radioButton.Tag as string;
                var validationBoomTypesList = validationBoomTypesString.Split(',');
                isValidatedInCurrentBoom = validationBoomTypesList.Contains(SelectedBoomType.ToString());
            }

            if (!isValidatedInCurrentBoom)
            {
                var panel = radioButton.Parent as Panel;
                radioButtonsErrorProvider.SetError(panel, "");
            }
            else
            {
                ValidationUtilities.ValidateRadioOptionSelected(radioButton, radioButtonsErrorProvider, e);
            }
        }

        private void ValidateTextboxHasValue(object sender, CancelEventArgs e)
        {
            var textBox = sender as TextBox;
            var isValidatedInCurrentBoom = false;
            if (textBox.Tag != null)
            {
                var validationBoomTypesString = textBox.Tag as string;
                var validationBoomTypesList = validationBoomTypesString.Split(',');
                isValidatedInCurrentBoom = validationBoomTypesList.Contains(SelectedBoomType.ToString());
            }

            if (!isValidatedInCurrentBoom)
            {
                greaterThanZeroErrorProvider.SetError(textBox, "");
            }
            else
            {
                ValidationUtilities.ValidateTextboxHasValue(textBox, emptyTextErrorProvider, e);
            }
        }

        #endregion validation

        #region menu

        private async void mnNewQuote_Click(object sender, EventArgs e)
        {
            CurrentQuoteRequest = null;
            CurrentCustomer = null;
            //Start again with a clean object context whenever possible, to avoid accumulating entities in memory
            mnNewQuote.Enabled = false;
            _context.Dispose();
            _context = new DataContext();
            LoadingForm.Show("Clearing Form...");
            await initializeLookups();

            initializeQuoteRequestControls();
            initializeDefaultBoomControls();

            mnNewQuote.Enabled = true;
            LoadingForm.Hide();
        }

        private void initializeQuoteRequestControls()
        {
            dtOrderConfirmed.Value = DateTime.Now;
            txtDueDate.Value = DateTime.Now.AddDays(56);

            //Default quote request values
            var distributors = (List<Distributor>)ddlDistributors.DataSource;
            ddlDistributors.SelectedIndex = distributors.FindIndex(p => p.Name.ToLower().Contains("cherry creek systems"));
            if (ddlDistributors.SelectedIndex > -1)
            {
                var startingDistributor = ddlDistributors.SelectedItem as Distributor;
                txtPriceLevel.Text = startingDistributor.PriceLevel.ToString();
            }
        }

        //Initialize all controls and set some default values
        private void initializeDefaultBoomControls()
        {
            ControlUtilities.CheckAllNO(this);
            rdCTWaterFeedSystemYes.Checked = true;
            rdNavWaterFeedSystemYes.Checked = true;
            ddlCTCarrySteel.SelectedItem = "CWF System Rails";
            ddlNavCarrySteel.SelectedItem = "CWF System Rails";

            btnRemoveSystem.Enabled = false;
            btnGenerateSalesOrderForm.Enabled = false;
        }

        private async void mnRefreshLookups_Click(object sender, EventArgs e)
        {
            LoadingForm.Show("Refreshing Lookups...");
            await initializeLookups();
            LoadingForm.Hide();
        }

        private async void mnSearchQuotes_Click(object sender, EventArgs e)
        {
            var currentQuoteNumber = CurrentQuoteRequest != null ? CurrentQuoteRequest.QuoteNumber : string.Empty;
            var searchQuoteForm = new SearchQuoteForm(currentQuoteNumber);
            await searchQuoteForm.Initialize();
            searchQuoteForm.ShowDialog();
        }

        private async void mnCustomers_Click(object sender, EventArgs e)
        {
            var customersForm = new CustomersForm();
            await customersForm.Initialize();
            customersForm.OnSaveFinished += bindAndReselectCustomerAndShipAddress;
            customersForm.ShowDialog();
        }

        private async void mnSalesReps_Click(object sender, EventArgs e)
        {
            var salesRepsForm = new SalesRepsForm();
            await salesRepsForm.Initialize();
            salesRepsForm.OnSaveFinished += bindAndReselectSalesRep;
            salesRepsForm.ShowDialog();
        }

        private async void mnDistributors_Click(object sender, EventArgs e)
        {
            var distributorsForm = new DistributorsForm();
            await distributorsForm.Initialize();
            distributorsForm.OnSaveFinished += bindAndReselectDistributor;
            distributorsForm.Show();
        }

        private async void mnGreenHouses_Click(object sender, EventArgs e)
        {
            var greenHousesForm = new GreenHousesForm();
            await greenHousesForm.Initialize();
            greenHousesForm.OnSaveFinished += bindAndReselectGreenHouse;
            greenHousesForm.ShowDialog();
        }

        private async void mnManageComponents_Click(object sender, EventArgs e)
        {
            var componentsForm = new ComponentsForm();
            await componentsForm.Initialize();
            componentsForm.ShowDialog();
        }

        private async void mnBaseCosts_Click(object sender, EventArgs e)
        {
            var baseCostsForm = new BaseCostsForm();
            await baseCostsForm.Initialize();
            baseCostsForm.ShowDialog();
        }

        #endregion menu

        #region refresh changes from other forms

        private async void bindAndReselectCustomerAndShipAddress()
        {
            //Get currently selected customer and ship address
            var currentCustomerSelectedIndex = ddlCustomers.SelectedIndex;
            var currentShipAddressSelectedIndex = ddlShipAddresses.SelectedIndex;

            var customerService = new CustomerService();
            var customersList = await customerService.Get().ToListAsync();
            await customerService.FillChildObjects(shipAddress: true, customers: customersList.ToArray());
            bindCustomerList(customersList);

            ddlCustomers.SelectedIndex = currentCustomerSelectedIndex;
            var updatedCustomer = ddlCustomers.SelectedItem as Customer;

            if (CurrentCustomer != null)
            {
                //This will also refresh the shipping addresses for the customer
                CurrentCustomer = updatedCustomer;
                ddlShipAddresses.SelectedIndex = currentShipAddressSelectedIndex;
            }
        }

        private async void bindAndReselectSalesRep()
        {
            var currentSelectedSalesRepIndex = ddlSalesReps.SelectedIndex;
            var salesReps = await new SalesRepService().Get().ToListAsync();
            bindSalesRepList(salesReps);
            ddlSalesReps.SelectedIndex = currentSelectedSalesRepIndex;
        }

        private async void bindAndReselectDistributor()
        {
            var currentSelectedDistributorIndex = ddlDistributors.SelectedIndex;
            var distributors = await new DistributorService().Get().ToListAsync();// getDistributorList().Result;
            bindDistributorList(distributors);
            ddlDistributors.SelectedIndex = currentSelectedDistributorIndex;
        }

        private async void bindAndReselectGreenHouse()
        {
            var currentSelectedGreenHouseIndex = ddlGreenhouseType.SelectedIndex;
            var greenHouseTypes = await new GreenhouseTypeService().Get().ToListAsync();// getGreenhouseTypeList().Result;
            bindGreenhouseTypeList(greenHouseTypes);
            ddlGreenhouseType.SelectedIndex = currentSelectedGreenHouseIndex;
        }

        private void lstDetailQuotes_Click(object sender, EventArgs e)
        {
            loadBoom();
        }

        private void lstDetailQuotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadBoom();
        }

        private void loadBoom()
        {
            var selectedQuoteBoomSystem = lstDetailQuotes.SelectedItem as QuoteBoomSystem;

            if (selectedQuoteBoomSystem != null)
            {
                CurrentBoom = selectedQuoteBoomSystem.Boom;
                btnGenerateSalesOrderForm.Enabled = true;
            }
        }

        private void lstDetailQuotes_DoubleClick(object sender, EventArgs e)
        {
            var selectedQuoteBoomSystem = lstDetailQuotes.SelectedItem as QuoteBoomSystem;

            if (selectedQuoteBoomSystem != null)
            {
                showDetailQuoteForm(selectedQuoteBoomSystem, CurrentQuoteRequest, selectedQuoteBoomSystem.Boom);
            }
        }

        #endregion refresh changes form other forms

        #region Hose Size calculation

        private void CalculateHoseSize(object sender, EventArgs e)
        {
            //If the house upgrade is selected, hose size will always be 1.25
            //
            //If the hose upgrade is selected then do not try to change the hose size below based on the
            //bay length and width
            switch (SelectedBoomType)
            {
                case BoomType.DoubleRail:
                    if (rdDRB1_1_4thHoseUpgradeYes.Checked)
                    {
                        txtHoseSize.Value = "1.25";
                        return;
                    }
                    break;

                case BoomType.Tower:
                    if (rdCT1_1_4thHoseUpgradeYes.Checked)
                    {
                        txtHoseSize.Value = "1.25";
                        return;
                    }
                    break;

                case BoomType.Navigator:
                    if (rdNav1_1_4thHoseUpgradeYes.Checked)
                    {
                        txtHoseSize.Value = "1.25";
                        return;
                    }
                    break;
            }

            double bayLength;
            double bayWidth;
            double.TryParse(txtBayLength.Text, out bayLength);
            double.TryParse(txtBayWidth.Text, out bayWidth);

            if ((bayLength >= BayLengthInFeetLimit && bayWidth >= BayWidthInFeetLimit)
                || bayLength >= 400 || bayWidth >= 40)
            {
                txtHoseSize.Value = "1.25";
            }
            else
            {
                txtHoseSize.Value = "1.00";
            }
        }

        private void HoseUpgradeYes_Click(object sender, EventArgs e)
        {
            //If the house upgrade is selected, hose size will always be 1.25
            txtHoseSize.Value = "1.25";
        }

        private void HoseUpgradeNo_Click(object sender, EventArgs e)
        {
            CalculateHoseSize(sender, e);
        }

        #endregion Hose Size calculation

        private async void btnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                bool emailAccepted = false;
                var modifiedTemplate = string.Empty;
                var emailTemplateLookup = await new SpecService().GetCustomerQuoteEmail();
                if (emailTemplateLookup?.Value != null)
                {
                    var emailForm = new EmailForm(emailTemplateLookup.Value);
                    emailForm.OnSaveFinished += template =>
                    {
                        modifiedTemplate = template;
                        emailAccepted = true;
                    };
                    emailForm.ShowDialog();
                }

                if (!emailAccepted) return;

                LoadingForm.Show($"Sending email to {CurrentCustomer.CompanyName}...");

                var reportFileName = await CreateReportGetFileName();

                var userEmails = new UserEmails();

                var success = await userEmails.SendQuoteFileNotifyWebsite(CurrentCustomer.Id, CurrentQuoteRequest.Email, reportFileName, CurrentQuoteRequest.Id,
                    CurrentQuoteRequest.QuoteNumber, modifiedTemplate);

                LoadingForm.Hide();

                if (success)
                    MessageBox.Show($"Email has been sent to {CurrentCustomer.CompanyName}", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                else
                    FlexibleMessageBox.Show("There was an error sending email", "Quotation System", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                FlexibleMessageBox.Show("There was an error sending email", "Quotation System", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                log.Error("Send Quote by Email", ex);
            }
            finally
            {
                btnUpdateQuote.Enabled = true;
                btnAddSystem.Enabled = true;
                btnAddCustomItems.Enabled = true;
                btnRemoveSystem.Enabled = true;
                btnGenerateQuoteReport.Enabled = true;
                btnSendEmail.Enabled = true;
                mnNewQuote.Enabled = true;
            }
        }

        private async void btnGenerateSalesOrderForm_Click(object sender, EventArgs e)
        {
            var selectedQuoteBoomSystem = lstDetailQuotes.SelectedItem as QuoteBoomSystem;

            if (selectedQuoteBoomSystem == null) return;

            LoadingForm.Show("Generating Sales Order Form...");

            var reportManager = new ReportManager();
            var fileName = await reportManager.GenerateSalesOrderForm(CurrentQuoteRequest.Id, selectedQuoteBoomSystem.Id_);

            LoadingForm.Hide();

            Process.Start(fileName);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = false;
            base.OnFormClosing(e);
        }

        private void CalculateRailDropdownAssemblyLengthInInches(object sender, EventArgs e)
        {
            var bottomCordHeightInFeet = Convert.ToDouble(txtBottomCordHeight.Value);
            var bottomCordHeightInInches = bottomCordHeightInFeet * 12;
            var growingSurfaceHeightInInches = Convert.ToDouble(txtGrowingSurfaceHeight.Value);

            var railDropdownAssemblyLengthInInches = bottomCordHeightInInches - 104 - growingSurfaceHeightInInches - 18;
            if (railDropdownAssemblyLengthInInches > 0)
            {
                txtDRBRailDropDownAssy.Value = railDropdownAssemblyLengthInInches;
                txtCWFDRBRailDropDownAssy.Value = railDropdownAssemblyLengthInInches;
            }
            else
            {
                txtDRBRailDropDownAssy.Value = 0;
                txtCWFDRBRailDropDownAssy.Value = 0;
            }
        }

        private void btnAddCustomItems_Click(object sender, EventArgs e)
        {
            var customItemsForm = new CustomQuoteItemsForm(CurrentQuoteRequest, _context);
            customItemsForm.ShowDialog();
        }

        private void btnSteelMaterials_Click(object sender, EventArgs e)
        {
            var selectedQuoteBoomSystem = lstDetailQuotes.SelectedItem as QuoteBoomSystem;

            if (selectedQuoteBoomSystem != null)
            {
                var steelMaterialsForm = new SteelMaterialsForm(CurrentQuoteRequest, selectedQuoteBoomSystem, _context);
                steelMaterialsForm.ShowDialog();
            }
        }

        private void CalculateDueDate(object sender, EventArgs e)
        {
            #region invalid cases

            if (string.IsNullOrEmpty(txtLeadTime.Text)) return;

            var leadTime = Regex.Replace(txtLeadTime.Text, @"\s+", " "); //  Remove multiple spaces

            int weeks;
            if (!int.TryParse(leadTime, out weeks)) return;

            #endregion

            var days = weeks * 7; //Convert weeks to days
            var dateValue = dtOrderConfirmed.Value.AddDays(days);
            txtDueDate.Value = dateValue;

            FlexibleMessageBox.Show($"Due Date is {txtDueDate.Value.ToString("MM/dd/yyyy")}", "Quotation System", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private async void mnManageCustomerQuoteEmail_Click(object sender, EventArgs e)
        {
            var emailTemplateLookup = await new SpecService().GetCustomerQuoteEmail();
            if (emailTemplateLookup?.Value != null)
            {
                var emailForm = new EmailForm(emailTemplateLookup.Value, saveData: true);
                emailForm.OnSaveFinished += template =>
                {
                    emailTemplateLookup.Value = template;
                    _context.SaveChanges();
                };
                emailForm.ShowDialog();
            }
        }

        private void ddlDistributors_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var selectedDistributor = ddlDistributors.SelectedItem as Distributor;
            var selectedDistributorPriceLevel = selectedDistributor?.PriceLevel ?? 0;
            if (selectedDistributorPriceLevel != 0)
            {
                var priceLevel = selectedDistributor.PriceLevel.ToString();
                var result = MessageBox.Show($"Do you want use the distributor's price level of {priceLevel}%?", "Quotation System", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                    txtPriceLevel.Text = priceLevel;
            }
        }
    }
}