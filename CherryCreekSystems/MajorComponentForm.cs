﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class MajorComponentForm : Form
    {
        #region public events

        public delegate void SaveFinished(bool isDelete);

        public SaveFinished OnSaveFinished;

        #endregion public events

        #region private variables

        //private QuoteRequest quoteRequest;
        private Boom boom;

        private readonly DataContext _context;

        private readonly MajorComponentService majorComponentService;

        private readonly int BoomTypeId;
        private readonly Category Category;
        private readonly int MajorComponentsCount;
        private readonly int NewMajorComponentOrder;

        #endregion private variables

        private MajorComponent _currentMajorComponent;

        private MajorComponent CurrentMajorComponent
        {
            get { return _currentMajorComponent; }
            set
            {
                _currentMajorComponent = value;
                if (_currentMajorComponent != null)
                {
                    lblId.Text = _currentMajorComponent.ComponentID;
                    lblDetailId.Text = _currentMajorComponent.DetailCalculationID;
                    lblDescriptionId.Text = _currentMajorComponent.DescriptionCalculationID;
                    lblPriceID.Text = _currentMajorComponent.PriceCalculationID;
                    txtMajorComponentName.Text = _currentMajorComponent.Name;
                    txtDetailCalculation.Text = _currentMajorComponent.DetailCalculation;
                    txtDescriptionCalculation.Text = _currentMajorComponent.DescriptionCalculation;
                    txtAddPriceCondition.Text = _currentMajorComponent.AddPriceCondition;
                    txtAdditionalPriceCalculation.Text = _currentMajorComponent.PriceCalculation;
                    txtActivateCondition.Text = _currentMajorComponent.ActivateComponentCondition;
                    ddlOrder.DataSource = Enumerable.Range(1, MajorComponentsCount).ToList();
                    ddlOrder.SelectedItem = _currentMajorComponent.Order;

                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    ddlOrder.DataSource = Enumerable.Range(1, NewMajorComponentOrder).ToList();
                    ddlOrder.SelectedItem = NewMajorComponentOrder;

                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        private Property SelectedBoomSpecField
        {
            get
            {
                var selectedBoomSpecField = lstBoomSpecs.SelectedItem as Property;
                return selectedBoomSpecField;
            }
        }

        public MajorComponentForm(DataContext context, MajorComponent majorComponent, Category category, int boomTypeId, int majorComponentsCount)
        {
            InitializeComponent();

            _context = context;
            BoomTypeId = boomTypeId;
            Category = category;
            MajorComponentsCount = majorComponentsCount;
            NewMajorComponentOrder = majorComponentsCount + 1;
            CurrentMajorComponent = majorComponent;

            majorComponentService = new MajorComponentService(_context);

            bindPropertiesLists(BoomTypeId);
        }

        private void bindPropertiesLists(int boomTypeId)
        {
            boom = BoomTypeFactory.CreateBoomType((BoomType)boomTypeId);

            var boomType = boom.GetType();
            var boomSpecsProperties = ReflectionUtilities.GetBoomProperties(boomType);

            lstBoomSpecs.DataSource = boomSpecsProperties;
            lstBoomSpecs.DisplayMember = "Name";
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            MajorComponent majorComponent = new MajorComponent();
            bool isValidMajorComponent = ValidateAndPopulateMajorComponent(majorComponent);
            if (isValidMajorComponent)
            {
                majorComponentService.Add(majorComponent);
                await _context.SaveChangesAsync();

                majorComponent.ComponentID = "MAC" + majorComponent.Id;
                majorComponent.DetailCalculationID = "MACD" + majorComponent.Id;
                majorComponent.DescriptionCalculationID = "MACT" + majorComponent.Id;
                majorComponent.PriceCalculationID = "MACP" + majorComponent.Id;
                await _context.SaveChangesAsync();

                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidMajorComponent = ValidateAndPopulateMajorComponent(CurrentMajorComponent);
            if (isValidMajorComponent)
            {
                await _context.SaveChangesAsync();
                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            //majorComponentService.Remove(CurrentMajorComponent);
            CurrentMajorComponent.IsActive = false;
            await _context.SaveChangesAsync();
            OnSaveFinished(isDelete: true);
            MessageBox.Show("Major Component has been removed", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            Close();
        }

        private bool ValidateAndPopulateMajorComponent(MajorComponent majorComponent)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(majorComponent);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(MajorComponent majorComponent)
        {
            majorComponent.Name = txtMajorComponentName.Text.Trim();
            majorComponent.Order = Convert.ToInt32(ddlOrder.SelectedItem);
            majorComponent.CategoryId = Category.Id;
            majorComponent.PriceCalculation = txtAdditionalPriceCalculation.Text;
            majorComponent.DetailCalculation = txtDetailCalculation.Text;
            majorComponent.DescriptionCalculation = txtDescriptionCalculation.Text;
            majorComponent.AddPriceCondition = txtAddPriceCondition.Text;
            majorComponent.IsActive = true;

            majorComponent.ActivateComponentCondition = txtActivateCondition.Text;
            if (string.IsNullOrWhiteSpace(majorComponent.ActivateComponentCondition))
                majorComponent.ActivateComponentCondition = "true";
        }

        #region drag and drop events

        private void lstBoomSpecs_MouseDown(object sender, MouseEventArgs e)
        {
            if (SelectedBoomSpecField != null)
            {
                DoDragDrop(SelectedBoomSpecField, DragDropEffects.Copy);
            }
        }

        private void txtDragEnter(object sender, DragEventArgs e)
        {
            if (e.AllowedEffect == DragDropEffects.Copy)
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void txtDragDrop(object sender, DragEventArgs e)
        {
            var textbox = sender as TextBox;
            var selectedField = e.Data.GetData(typeof(Property)) as Property;
            var targetProperty = selectedField.Name.Replace(" ", "");

            var insertIndex = GetCaretIndexFromPoint(textbox, e.X, e.Y);
            textbox.Text = textbox.Text.Insert(insertIndex, targetProperty);
            textbox.SelectionStart = insertIndex;
        }

        #endregion drag and drop events

        #region validation

        private void ValidateTextboxHasValue(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        #endregion validation

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtDragOver(object sender, DragEventArgs e)
        {
            var textBox = sender as TextBox;
            // fake moving the text caret
            textBox.SelectionStart = GetCaretIndexFromPoint(textBox, e.X, e.Y);
            textBox.SelectionLength = 0;
            // don't forget to set focus to the text box to make the caret visible!
            textBox.Focus();
        }

        //http://stackoverflow.com/questions/913735/how-to-move-insert-caret-on-textbox-when-accepting-a-drop
        /// <remarks>
        /// GetCharIndexFromPosition is missing one caret position, as there is one extra caret
        /// position than there are characters (an extra one at the end).
        /// </remarks>
        private static int GetCaretIndexFromPoint(TextBox box, int x, int y)
        {
            Point realPoint = box.PointToClient(new Point(x, y));
            int index = box.GetCharIndexFromPosition(realPoint);
            if (index == box.Text.Length - 1)
            {
                Point caretPoint = box.GetPositionFromCharIndex(index);
                if (realPoint.X > caretPoint.X)
                {
                    index += 1;
                }
            }
            return index;
        }
    }
}