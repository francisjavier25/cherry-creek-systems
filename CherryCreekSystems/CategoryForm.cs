﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class CategoryForm : Form
    {
        public delegate void SaveFinished(bool isDelete);

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly CategoryService categoryService;

        private readonly int BoomSystemId;
        private readonly int CategoriesCount;
        private readonly int NewCategoryOrder;

        private Category _currentCategory;

        public Category CurrentCategory
        {
            get { return _currentCategory; }
            set
            {
                _currentCategory = value;
                if (_currentCategory != null)
                {
                    txtCategoryName.Text = _currentCategory.Name;
                    ddlOrder.DataSource = Enumerable.Range(1, CategoriesCount).ToList();
                    ddlOrder.SelectedItem = _currentCategory.Order;

                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    ddlOrder.DataSource = Enumerable.Range(1, NewCategoryOrder).ToList();
                    ddlOrder.SelectedItem = NewCategoryOrder;

                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public CategoryForm(DataContext context, Category category, int boomTypeId, int categoriesCount)
        {
            InitializeComponent();

            _context = context;
            BoomSystemId = boomTypeId;
            CategoriesCount = categoriesCount;
            NewCategoryOrder = categoriesCount + 1;
            CurrentCategory = category;

            categoryService = new CategoryService(_context);
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            Category category = new Category();
            bool isValidCategory = ValidateAndPopulateCategory(category);
            if (isValidCategory)
            {
                categoryService.Add(category);
                await _context.SaveChangesAsync();

                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidCategory = ValidateAndPopulateCategory(CurrentCategory);
            if (isValidCategory)
            {
                await _context.SaveChangesAsync();
                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            CurrentCategory.IsActive = false;
            //categoryService.Remove(CurrentCategory);
            _context.SaveChangesAsync();
            OnSaveFinished(isDelete: true);
            MessageBox.Show("Category has been removed", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            Close();
        }

        private bool ValidateAndPopulateCategory(Category category)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(category);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(Category category)
        {
            category.Name = txtCategoryName.Text.Trim();
            category.Order = Convert.ToInt32(ddlOrder.SelectedItem);
            category.SystemId = BoomSystemId;
            category.IsActive = true;
        }

        private void ValidateTextboxHasValue(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}