﻿namespace CherryCreekSystems
{
    partial class BoomSystemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label33 = new System.Windows.Forms.Label();
            this.lstBoomSpecs = new System.Windows.Forms.ListBox();
            this.emptyTextErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtCostCalculation = new System.Windows.Forms.TextBox();
            this.txtDetailCalculation = new System.Windows.Forms.TextBox();
            this.lbAdditionalPriceCondition = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtBoomSystemName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDetailId = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblPriceID = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDescriptionId = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescriptionCalculation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.emptyTextErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(1135, 32);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(128, 24);
            this.label33.TabIndex = 92;
            this.label33.Text = "Boom Specs";
            // 
            // lstBoomSpecs
            // 
            this.lstBoomSpecs.FormattingEnabled = true;
            this.lstBoomSpecs.HorizontalScrollbar = true;
            this.lstBoomSpecs.ItemHeight = 16;
            this.lstBoomSpecs.Location = new System.Drawing.Point(1042, 68);
            this.lstBoomSpecs.Name = "lstBoomSpecs";
            this.lstBoomSpecs.Size = new System.Drawing.Size(345, 612);
            this.lstBoomSpecs.TabIndex = 90;
            this.lstBoomSpecs.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstBoomSpecs_MouseDown);
            // 
            // emptyTextErrorProvider
            // 
            this.emptyTextErrorProvider.BlinkRate = 0;
            this.emptyTextErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.emptyTextErrorProvider.ContainerControl = this;
            // 
            // txtCostCalculation
            // 
            this.txtCostCalculation.AllowDrop = true;
            this.txtCostCalculation.Location = new System.Drawing.Point(198, 466);
            this.txtCostCalculation.Multiline = true;
            this.txtCostCalculation.Name = "txtCostCalculation";
            this.txtCostCalculation.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCostCalculation.Size = new System.Drawing.Size(777, 136);
            this.txtCostCalculation.TabIndex = 88;
            this.txtCostCalculation.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtCostCalculation.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtCostCalculation.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // txtDetailCalculation
            // 
            this.txtDetailCalculation.AllowDrop = true;
            this.txtDetailCalculation.Location = new System.Drawing.Point(198, 157);
            this.txtDetailCalculation.Multiline = true;
            this.txtDetailCalculation.Name = "txtDetailCalculation";
            this.txtDetailCalculation.Size = new System.Drawing.Size(777, 98);
            this.txtDetailCalculation.TabIndex = 87;
            this.txtDetailCalculation.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtDetailCalculation.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtDetailCalculation.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // lbAdditionalPriceCondition
            // 
            this.lbAdditionalPriceCondition.AutoSize = true;
            this.lbAdditionalPriceCondition.Location = new System.Drawing.Point(51, 469);
            this.lbAdditionalPriceCondition.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbAdditionalPriceCondition.Name = "lbAdditionalPriceCondition";
            this.lbAdditionalPriceCondition.Size = new System.Drawing.Size(117, 17);
            this.lbAdditionalPriceCondition.TabIndex = 86;
            this.lbAdditionalPriceCondition.Text = "Price Calculation:";
            this.lbAdditionalPriceCondition.UseMnemonic = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 160);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 85;
            this.label2.Text = "Detail Calculation:";
            this.label2.UseMnemonic = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(611, 628);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(197, 52);
            this.btnClose.TabIndex = 82;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(354, 628);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(197, 52);
            this.btnUpdate.TabIndex = 81;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtBoomSystemName
            // 
            this.txtBoomSystemName.AllowDrop = true;
            this.txtBoomSystemName.Location = new System.Drawing.Point(198, 68);
            this.txtBoomSystemName.Name = "txtBoomSystemName";
            this.txtBoomSystemName.Size = new System.Drawing.Size(777, 22);
            this.txtBoomSystemName.TabIndex = 79;
            this.txtBoomSystemName.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtBoomSystemName.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtBoomSystemName.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateTextboxHasValue);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(119, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 78;
            this.label3.Text = "Name:";
            this.label3.UseMnemonic = false;
            // 
            // lblDetailId
            // 
            this.lblDetailId.AutoSize = true;
            this.lblDetailId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailId.Location = new System.Drawing.Point(195, 116);
            this.lblDetailId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDetailId.Name = "lblDetailId";
            this.lblDetailId.Size = new System.Drawing.Size(0, 17);
            this.lblDetailId.TabIndex = 94;
            this.lblDetailId.UseMnemonic = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(103, 116);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 17);
            this.label4.TabIndex = 93;
            this.label4.Text = "Detail ID:";
            this.label4.UseMnemonic = false;
            // 
            // lblPriceID
            // 
            this.lblPriceID.AutoSize = true;
            this.lblPriceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPriceID.Location = new System.Drawing.Point(195, 432);
            this.lblPriceID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPriceID.Name = "lblPriceID";
            this.lblPriceID.Size = new System.Drawing.Size(0, 17);
            this.lblPriceID.TabIndex = 96;
            this.lblPriceID.UseMnemonic = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(107, 432);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 17);
            this.label6.TabIndex = 95;
            this.label6.Text = "Price ID:";
            this.label6.UseMnemonic = false;
            // 
            // lblDescriptionId
            // 
            this.lblDescriptionId.AutoSize = true;
            this.lblDescriptionId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionId.Location = new System.Drawing.Point(195, 272);
            this.lblDescriptionId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescriptionId.Name = "lblDescriptionId";
            this.lblDescriptionId.Size = new System.Drawing.Size(0, 17);
            this.lblDescriptionId.TabIndex = 100;
            this.lblDescriptionId.UseMnemonic = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 272);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 99;
            this.label5.Text = "Description ID:";
            this.label5.UseMnemonic = false;
            // 
            // txtDescriptionCalculation
            // 
            this.txtDescriptionCalculation.AllowDrop = true;
            this.txtDescriptionCalculation.Location = new System.Drawing.Point(198, 313);
            this.txtDescriptionCalculation.Multiline = true;
            this.txtDescriptionCalculation.Name = "txtDescriptionCalculation";
            this.txtDescriptionCalculation.Size = new System.Drawing.Size(777, 99);
            this.txtDescriptionCalculation.TabIndex = 98;
            this.txtDescriptionCalculation.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtDescriptionCalculation.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtDescriptionCalculation.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 316);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 17);
            this.label7.TabIndex = 97;
            this.label7.Text = "Description Calculation:";
            this.label7.UseMnemonic = false;
            // 
            // BoomSystemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1416, 723);
            this.Controls.Add(this.lblDescriptionId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDescriptionCalculation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblPriceID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblDetailId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lstBoomSpecs);
            this.Controls.Add(this.txtCostCalculation);
            this.Controls.Add(this.txtDetailCalculation);
            this.Controls.Add(this.lbAdditionalPriceCondition);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtBoomSystemName);
            this.Controls.Add(this.label3);
            this.Name = "BoomSystemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "BoomSystemForm";
            ((System.ComponentModel.ISupportInitialize)(this.emptyTextErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ListBox lstBoomSpecs;
        private System.Windows.Forms.ErrorProvider emptyTextErrorProvider;
        private System.Windows.Forms.TextBox txtCostCalculation;
        private System.Windows.Forms.TextBox txtDetailCalculation;
        private System.Windows.Forms.Label lbAdditionalPriceCondition;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtBoomSystemName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDetailId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPriceID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblDescriptionId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDescriptionCalculation;
        private System.Windows.Forms.Label label7;
    }
}