﻿using mshtml;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace CherryCreekSystems
{
    public partial class EmailForm : Form
    {
        public delegate void SaveFinished(string template);

        public SaveFinished OnSaveFinished;

        private readonly string _content;

        private readonly bool _saveData;

        private string HtmlContent
        {
            get
            {
                return ((HTMLDocumentClass)axWebBrowser1.Document).IHTMLDocument2_body.innerHTML;
            }
            set
            {
                ((HTMLDocumentClass)axWebBrowser1.Document).IHTMLDocument2_body.innerHTML = value;
            }
        }

        public EmailForm(string content, bool saveData = false)
        {
            InitializeComponent();
            _content = content ?? string.Empty;
            _saveData = saveData;

            if (_saveData)
            {
                btnSave.Text = "Save";
                btnClose.Text = "Close";
            }
            else
            {
                btnSave.Text = "Send Email";
                btnClose.Text = "Cancel";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            object path = Directory.GetCurrentDirectory() + "\\test.htm";
            object missing = System.Reflection.Missing.Value;
            axWebBrowser1.Navigate2(ref path, ref missing, ref missing, ref missing, ref missing);
            var doc = (HTMLDocumentClass)axWebBrowser1.Document;
            doc.designMode = "On";      // This turns the control into an editor
        }

        private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            var doc = (HTMLDocumentClass)axWebBrowser1.Document;
            switch (e.Button.ImageIndex)
            {
                case 10:        // Open
                    openFileDialog1.ShowDialog();
                    break;

                case 0:         // Save
                    saveFileDialog1.ShowDialog();
                    break;

                case 2:     // Cut
                    doc.execCommand("Cut", false);
                    break;

                case 3:     // Copy
                    doc.execCommand("Copy", false);
                    break;

                case 4:     // Paste
                    doc.execCommand("Paste", false);
                    break;

                case 7:     // Bold
                    doc.execCommand("Bold", false);
                    break;

                case 8:     // Italic
                    doc.execCommand("Italic", false);
                    break;

                case 9:     // Underline
                    doc.execCommand("Underline", false);
                    break;

                case 6:     // Undo
                    doc.execCommand("Undo", false);
                    break;

                case 5:     // Redo
                    doc.execCommand("Redo", false);
                    break;
            }

            if (e.Button.Text != "")
            {
                doc.execCommand("FontSize", false, e.Button.Text);
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                object path = openFileDialog1.FileName;
                object missing = System.Reflection.Missing.Value;
                axWebBrowser1.Navigate2(ref path, ref missing, ref missing, ref missing, ref missing);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                var filename = saveFileDialog1.FileName;
                var file = new StreamWriter(filename, false, System.Text.Encoding.Unicode);
                var doc = (HTMLDocumentClass)axWebBrowser1.Document;
                var str = "<body>" + doc.IHTMLDocument2_body.innerHTML + "</body>";
                file.Write(str);
                file.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tabControl1.SelectedIndex == 1)    // Code View
                {
                    var doc = (HTMLDocumentClass)axWebBrowser1.Document;
                    textBox1.Text = "<body>" + doc.IHTMLDocument2_body.innerHTML + "</body>";
                }

                if (tabControl1.SelectedIndex == 0)    // Editor View
                {
                    HtmlContent = textBox1.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void axWebBrowser1_NavigateComplete2(object sender, AxSHDocVw.DWebBrowserEvents2_NavigateComplete2Event e)
        {
            HtmlContent = _content;
        }

        private void axWebBrowser1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            textBox1.Text = HtmlContent;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            OnSaveFinished?.Invoke(HtmlContent);
            if (_saveData)
            {
                MessageBox.Show("Email template has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}