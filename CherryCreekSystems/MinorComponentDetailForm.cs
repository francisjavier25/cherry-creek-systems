﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class MinorComponentDetailForm : Form
    {
        public delegate void SaveFinished(bool isDelete);

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly MinorComponentDetailService minorComponentDetailService;

        private readonly MinorComponent MinorComponent;

        private readonly int MinorComponentDetailsCount;
        private readonly int NewMinorComponentDetailOrder;

        private MinorComponentDetail _currentMinorComponentDetail;

        public MinorComponentDetail CurrentMinorComponentDetail
        {
            get { return _currentMinorComponentDetail; }
            set
            {
                _currentMinorComponentDetail = value;
                if (_currentMinorComponentDetail != null)
                {
                    txtMinorComponentDetailName.Text = _currentMinorComponentDetail.Name;
                    //txtDetailCalculation.Text = _currentMinorComponentDetail.DetailCalculation;
                    //txtAddPriceCondition.Text = _currentMinorComponentDetail.AddPriceCondition;
                    //txtAdditionalPrice.Text = _currentMinorComponentDetail.Price.ToString();
                    ddlOrder.DataSource = Enumerable.Range(1, MinorComponentDetailsCount).ToList();
                    ddlOrder.SelectedItem = _currentMinorComponentDetail.Order;

                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    ddlOrder.DataSource = Enumerable.Range(1, NewMinorComponentDetailOrder).ToList();
                    ddlOrder.SelectedItem = NewMinorComponentDetailOrder;

                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public MinorComponentDetailForm(DataContext context, MinorComponentDetail minorComponentDetail, MinorComponent minorComponent, int minorComponentDetailsCount)
        {
            InitializeComponent();

            _context = context;
            MinorComponent = minorComponent;
            MinorComponentDetailsCount = minorComponentDetailsCount;
            NewMinorComponentDetailOrder = minorComponentDetailsCount + 1;
            CurrentMinorComponentDetail = minorComponentDetail;

            minorComponentDetailService = new MinorComponentDetailService(_context);
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            MinorComponentDetail minorComponentDetail = new MinorComponentDetail();
            bool isValidMinorComponentDetail = ValidateAndPopulateMinorComponentDetail(minorComponentDetail);
            if (isValidMinorComponentDetail)
            {
                minorComponentDetailService.Add(minorComponentDetail);
                await _context.SaveChangesAsync();

                minorComponentDetail.ComponentID = "MID" + minorComponentDetail.Id;
                minorComponentDetail.DetailCalculationID = "MIDD" + minorComponentDetail.Id;
                minorComponentDetail.DescriptionCalculationID = "MIDT" + minorComponentDetail.Id;
                minorComponentDetail.PriceCalculationID = "MIDP" + minorComponentDetail.Id;
                await _context.SaveChangesAsync();

                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidMinorComponentDetail = ValidateAndPopulateMinorComponentDetail(CurrentMinorComponentDetail);
            if (isValidMinorComponentDetail)
            {
                await _context.SaveChangesAsync();
                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            //minorComponentDetailService.Remove(CurrentMinorComponentDetail);
            CurrentMinorComponentDetail.IsActive = false;
            await _context.SaveChangesAsync();
            OnSaveFinished(isDelete: true);
            MessageBox.Show("Minor Component Detail has been removed", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            Close();
        }

        private bool ValidateAndPopulateMinorComponentDetail(MinorComponentDetail minorComponentDetail)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(minorComponentDetail);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(MinorComponentDetail minorComponentDetail)
        {
            minorComponentDetail.Name = txtMinorComponentDetailName.Text.Trim();
            minorComponentDetail.Order = Convert.ToInt32(ddlOrder.SelectedItem);
            minorComponentDetail.MinorComponentId = MinorComponent.Id;
            minorComponentDetail.IsActive = true;
            //minorComponentDetail.Price = Convert.ToDouble(txtAdditionalPrice.Text);
        }

        private void ValidateTextboxHasValue(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}