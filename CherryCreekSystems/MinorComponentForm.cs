﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class MinorComponentForm : Form
    {
        #region public events

        public delegate void SaveFinished(bool isDelete);

        public SaveFinished OnSaveFinished;

        #endregion public events

        #region private variables

        private Boom boom;

        private readonly DataContext _context;

        private readonly MinorComponentService minorComponentService;

        private readonly int BoomTypeId;
        private readonly MajorComponent MajorComponent;
        private readonly int MinorComponentsCount;
        private readonly int NewMinorComponentOrder;

        private MinorComponent _currentMinorComponent;

        #endregion private variables

        private MinorComponent CurrentMinorComponent
        {
            get { return _currentMinorComponent; }
            set
            {
                _currentMinorComponent = value;
                if (_currentMinorComponent != null)
                {
                    lblId.Text = _currentMinorComponent.ComponentID;
                    lblDetailId.Text = _currentMinorComponent.DetailCalculationID;
                    lblDescriptionId.Text = _currentMinorComponent.DescriptionCalculationID;
                    txtMinorComponentName.Text = _currentMinorComponent.Name;
                    txtDetailCalculation.Text = _currentMinorComponent.DetailCalculation;
                    txtDescriptionCalculation.Text = _currentMinorComponent.DescriptionCalculation;
                    txtActivateCondition.Text = _currentMinorComponent.ActivateComponentCondition;
                    //txtAddPriceCondition.Text = _currentMinorComponent.AddPriceCondition;
                    //txtAdditionalPrice.Text = _currentMinorComponent.Price.ToString();
                    ddlOrder.DataSource = Enumerable.Range(1, MinorComponentsCount).ToList();
                    ddlOrder.SelectedItem = _currentMinorComponent.Order;

                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    ddlOrder.DataSource = Enumerable.Range(1, NewMinorComponentOrder).ToList();
                    ddlOrder.SelectedItem = NewMinorComponentOrder;

                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        private Property SelectedBoomSpecField
        {
            get
            {
                var selectedBoomSpecField = lstBoomSpecs.SelectedItem as Property;
                return selectedBoomSpecField;
            }
        }

        public MinorComponentForm(DataContext context, MinorComponent minorComponent, MajorComponent majorComponent, int boomTypeId, int minorComponentsCount)
        {
            InitializeComponent();

            _context = context;
            BoomTypeId = boomTypeId;
            MajorComponent = majorComponent;
            MinorComponentsCount = minorComponentsCount;
            NewMinorComponentOrder = minorComponentsCount + 1;
            CurrentMinorComponent = minorComponent;

            minorComponentService = new MinorComponentService(_context);

            bindPropertiesLists(BoomTypeId);
        }

        private void bindPropertiesLists(int boomTypeId)
        {
            boom = BoomTypeFactory.CreateBoomType((BoomType)boomTypeId);

            var boomType = boom.GetType();
            var boomSpecsProperties = ReflectionUtilities.GetBoomProperties(boomType);

            lstBoomSpecs.DataSource = boomSpecsProperties;
            lstBoomSpecs.DisplayMember = "Name";
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            MinorComponent minorComponent = new MinorComponent();
            bool isValidMinorComponent = ValidateAndPopulateMinorComponent(minorComponent);
            if (isValidMinorComponent)
            {
                minorComponentService.Add(minorComponent);
                await _context.SaveChangesAsync();

                minorComponent.ComponentID = "MIC" + minorComponent.Id;
                minorComponent.DetailCalculationID = "MICD" + minorComponent.Id;
                minorComponent.DescriptionCalculationID = "MICT" + minorComponent.Id;
                minorComponent.PriceCalculationID = "MICP" + minorComponent.Id;
                await _context.SaveChangesAsync();

                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidMinorComponent = ValidateAndPopulateMinorComponent(CurrentMinorComponent);
            if (isValidMinorComponent)
            {
                await _context.SaveChangesAsync();
                OnSaveFinished(isDelete: false);
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            //minorComponentService.Remove(CurrentMinorComponent);
            CurrentMinorComponent.IsActive = false;
            await _context.SaveChangesAsync();
            OnSaveFinished(isDelete: true);
            MessageBox.Show("Minor Component has been removed", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            Close();
        }

        private bool ValidateAndPopulateMinorComponent(MinorComponent minorComponent)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(minorComponent);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(MinorComponent minorComponent)
        {
            minorComponent.Name = txtMinorComponentName.Text.Trim();
            minorComponent.Order = Convert.ToInt32(ddlOrder.SelectedItem);
            minorComponent.MajorComponentId = MajorComponent.Id;
            minorComponent.DetailCalculation = txtDetailCalculation.Text;
            minorComponent.DescriptionCalculation = txtDescriptionCalculation.Text;
            minorComponent.ActivateComponentCondition = txtActivateCondition.Text;
            minorComponent.IsActive = true;

            if (string.IsNullOrWhiteSpace(minorComponent.ActivateComponentCondition))
                minorComponent.ActivateComponentCondition = "true";
        }

        #region drag and drop events

        private void lstBoomSpecs_MouseDown(object sender, MouseEventArgs e)
        {
            if (SelectedBoomSpecField != null)
            {
                DoDragDrop(SelectedBoomSpecField, DragDropEffects.Copy);
            }
        }

        private void txtDragEnter(object sender, DragEventArgs e)
        {
            if (e.AllowedEffect == DragDropEffects.Copy)
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void txtDragDrop(object sender, DragEventArgs e)
        {
            var textbox = sender as TextBox;
            var selectedField = e.Data.GetData(typeof(Property)) as Property;
            var targetProperty = selectedField.Name.Replace(" ", "");

            var insertIndex = GetCaretIndexFromPoint(textbox, e.X, e.Y);
            textbox.Text = textbox.Text.Insert(insertIndex, targetProperty);
            textbox.SelectionStart = insertIndex;
        }

        #endregion drag and drop events

        #region validation

        private void ValidateTextboxHasValue(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        #endregion validation

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtDragOver(object sender, DragEventArgs e)
        {
            var textBox = sender as TextBox;
            // fake moving the text caret
            textBox.SelectionStart = GetCaretIndexFromPoint(textBox, e.X, e.Y);
            textBox.SelectionLength = 0;
            // don't forget to set focus to the text box to make the caret visible!
            textBox.Focus();
        }

        //http://stackoverflow.com/questions/913735/how-to-move-insert-caret-on-textbox-when-accepting-a-drop
        /// <remarks>
        /// GetCharIndexFromPosition is missing one caret position, as there is one extra caret
        /// position than there are characters (an extra one at the end).
        /// </remarks>
        private static int GetCaretIndexFromPoint(TextBox box, int x, int y)
        {
            Point realPoint = box.PointToClient(new Point(x, y));
            int index = box.GetCharIndexFromPosition(realPoint);
            if (index == box.Text.Length - 1)
            {
                Point caretPoint = box.GetPositionFromCharIndex(index);
                if (realPoint.X > caretPoint.X)
                {
                    index += 1;
                }
            }
            return index;
        }
    }
}