﻿namespace CherryCreekSystems
{
    partial class PropertyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtDoubleValue = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pnlValue = new System.Windows.Forms.Panel();
            this.rdFalse = new System.Windows.Forms.RadioButton();
            this.rdTrue = new System.Windows.Forms.RadioButton();
            this.btnSubmitValue = new System.Windows.Forms.Button();
            this.radioButtonsErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.greaterThanZeroErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtIntValue = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtStringValue = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleValue)).BeginInit();
            this.pnlValue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtonsErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greaterThanZeroErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIntValue)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDoubleValue
            // 
            this.txtDoubleValue.Location = new System.Drawing.Point(42, 82);
            this.txtDoubleValue.Margin = new System.Windows.Forms.Padding(4);
            this.txtDoubleValue.Mask = "G";
            this.txtDoubleValue.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtDoubleValue.Name = "txtDoubleValue";
            this.txtDoubleValue.Size = new System.Drawing.Size(213, 24);
            this.txtDoubleValue.TabIndex = 63;
            this.txtDoubleValue.TabStop = false;
            this.txtDoubleValue.Tag = "Echo";
            this.txtDoubleValue.Text = "0";
            this.txtDoubleValue.Visible = false;
            // 
            // pnlValue
            // 
            this.pnlValue.Controls.Add(this.rdFalse);
            this.pnlValue.Controls.Add(this.rdTrue);
            this.pnlValue.Location = new System.Drawing.Point(42, 82);
            this.pnlValue.Margin = new System.Windows.Forms.Padding(4);
            this.pnlValue.Name = "pnlValue";
            this.pnlValue.Size = new System.Drawing.Size(213, 38);
            this.pnlValue.TabIndex = 64;
            this.pnlValue.Visible = false;
            // 
            // rdFalse
            // 
            this.rdFalse.AutoSize = true;
            this.rdFalse.Location = new System.Drawing.Point(113, 9);
            this.rdFalse.Margin = new System.Windows.Forms.Padding(4);
            this.rdFalse.Name = "rdFalse";
            this.rdFalse.Size = new System.Drawing.Size(50, 21);
            this.rdFalse.TabIndex = 46;
            this.rdFalse.TabStop = true;
            this.rdFalse.Tag = "Echo";
            this.rdFalse.Text = "NO";
            this.rdFalse.UseVisualStyleBackColor = true;
            this.rdFalse.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // rdTrue
            // 
            this.rdTrue.AutoSize = true;
            this.rdTrue.Location = new System.Drawing.Point(31, 9);
            this.rdTrue.Margin = new System.Windows.Forms.Padding(4);
            this.rdTrue.Name = "rdTrue";
            this.rdTrue.Size = new System.Drawing.Size(56, 21);
            this.rdTrue.TabIndex = 45;
            this.rdTrue.TabStop = true;
            this.rdTrue.Tag = "Echo";
            this.rdTrue.Text = "YES";
            this.rdTrue.UseVisualStyleBackColor = true;
            this.rdTrue.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateRadioOptionSelected);
            // 
            // btnSubmitValue
            // 
            this.btnSubmitValue.Location = new System.Drawing.Point(82, 169);
            this.btnSubmitValue.Name = "btnSubmitValue";
            this.btnSubmitValue.Size = new System.Drawing.Size(132, 37);
            this.btnSubmitValue.TabIndex = 65;
            this.btnSubmitValue.Text = "Ok";
            this.btnSubmitValue.UseVisualStyleBackColor = true;
            this.btnSubmitValue.Click += new System.EventHandler(this.btnSubmitValue_Click);
            // 
            // radioButtonsErrorProvider
            // 
            this.radioButtonsErrorProvider.BlinkRate = 0;
            this.radioButtonsErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.radioButtonsErrorProvider.ContainerControl = this;
            // 
            // greaterThanZeroErrorProvider
            // 
            this.greaterThanZeroErrorProvider.BlinkRate = 0;
            this.greaterThanZeroErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.greaterThanZeroErrorProvider.ContainerControl = this;
            // 
            // txtIntValue
            // 
            this.txtIntValue.Location = new System.Drawing.Point(42, 82);
            this.txtIntValue.Margin = new System.Windows.Forms.Padding(4);
            this.txtIntValue.Mask = "G";
            this.txtIntValue.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txtIntValue.Name = "txtIntValue";
            this.txtIntValue.Size = new System.Drawing.Size(213, 24);
            this.txtIntValue.TabIndex = 66;
            this.txtIntValue.TabStop = false;
            this.txtIntValue.Text = "0";
            this.txtIntValue.Visible = false;
            // 
            // txtStringValue
            // 
            this.txtStringValue.Location = new System.Drawing.Point(42, 82);
            this.txtStringValue.Name = "txtStringValue";
            this.txtStringValue.Size = new System.Drawing.Size(213, 22);
            this.txtStringValue.TabIndex = 68;
            this.txtStringValue.Visible = false;
            // 
            // PropertyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 254);
            this.Controls.Add(this.txtStringValue);
            this.Controls.Add(this.txtIntValue);
            this.Controls.Add(this.btnSubmitValue);
            this.Controls.Add(this.pnlValue);
            this.Controls.Add(this.txtDoubleValue);
            this.Name = "PropertyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Quotation System";
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleValue)).EndInit();
            this.pnlValue.ResumeLayout(false);
            this.pnlValue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtonsErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greaterThanZeroErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIntValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadMaskedEditBox txtDoubleValue;
        private System.Windows.Forms.Panel pnlValue;
        private System.Windows.Forms.RadioButton rdFalse;
        private System.Windows.Forms.RadioButton rdTrue;
        private System.Windows.Forms.Button btnSubmitValue;
        private System.Windows.Forms.ErrorProvider radioButtonsErrorProvider;
        private System.Windows.Forms.ErrorProvider greaterThanZeroErrorProvider;
        private Telerik.WinControls.UI.RadMaskedEditBox txtIntValue;
        private System.Windows.Forms.TextBox txtStringValue;
    }
}