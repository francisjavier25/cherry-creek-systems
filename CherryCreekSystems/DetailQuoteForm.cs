﻿using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using CherryCreekSystems.BL;
using Telerik.WinControls.UI;

namespace CherryCreekSystems
{
    public partial class DetailQuoteForm : Form
    {
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool BringWindowToTop(IntPtr hWnd);

        public delegate void SaveFinished(int? quoteRequest);

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly QuoteBoomSystem QuoteBoomSystem;

        private readonly QuoteRequest QuoteRequest;

        private readonly Boom Boom;

        //private PaintEventHandler redrawForScrollbar;

        private int scrollbarVerticalPosition;

        private int additionalScrollbarVerticalPosition = 30;

        private LoadingForm LoadingForm;

        #region text constants

        private const string calculatedCostText = "Calculated Cost";
        private const string addedCostText = "Added Cost";
        private const string removeText = "Remove";

        #endregion

        public DetailQuoteForm(DataContext context, QuoteBoomSystem quoteBoomSystem, QuoteRequest quoteRequest, Boom boom)
        {
            InitializeComponent();

            _context = context;
            QuoteRequest = quoteRequest;
            Boom = boom;
            QuoteBoomSystem = quoteBoomSystem.Ordered();
            LoadingForm = new LoadingForm();
        }

        private void DetailQuoteForm_Load(object sender, EventArgs e)
        {
            txtSystemName.Text = QuoteBoomSystem.DescriptionCalculationResult;
            txtSystemCost.Text = QuoteBoomSystem.AssignedPrice.ToString("N2");
            txtSystemDetailCalculationResult.Text = QuoteBoomSystem.DetailCalculationResult;
            txtQuantityOfSystems.Text = QuoteBoomSystem.Quantity.ToString();
            chkOverrideSystemComponent.Checked = QuoteBoomSystem.OverrideComponentData;

            populateCustomerValues();
            CreateComponentControls();
        }

        private void populateCustomerValues()
        {
            //lblQuoteId.Text = QuoteBoomSystem.Id.ToString();
            lblQuoteNumber.Text = QuoteRequest.QuoteNumber;
            lblSystemWidth.Text = Boom.BayWidthInFeet.ToString();
            lblSystemLength.Text = Boom.BayLengthInFeet.ToString();
            lblNumberOfBays.Text = Boom.NumberOfBays.ToString();
        }

        public void CreateComponentControls()
        {
            pnComponents.Controls.Clear();

            const int CategoryXCoordinate = 175;
            var yCoordinate = 0;
            var detailTextBoxWidth = 50;
            var majorComponentDetailTextBoxWidth = 65;
            var buttonWidth = 75;
            var removeOptionalButtonPosition = 1070;
            var categoryFont = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);

            foreach (var quoteCategory in QuoteBoomSystem.QuoteCategories)
            {
                var xCoordinate = CategoryXCoordinate;
                yCoordinate += 30;

                var txtCategoryName = new TextBox
                {
                    Width = 930,
                    Font = categoryFont,
                    Location = new Point(xCoordinate, yCoordinate),
                    Tag = quoteCategory,
                    Name = $"{"txtCategoryName"}{quoteCategory.Id_}",
                    Text = $"{quoteCategory.Name}"
                };
                txtCategoryName.DoubleClick += txtCategoryName_DoubleClick;

                pnComponents.Controls.Add(txtCategoryName);

                yCoordinate += 30;

                var lblOptionPrice = new Label
                {
                    Width = 95,
                    Location = new Point(CategoryXCoordinate + 695, yCoordinate),
                    Text = calculatedCostText
                };

                var lblAdditionalCost = new Label
                {
                    Width = 140,
                    Location = new Point(CategoryXCoordinate + 805, yCoordinate),
                    Text = addedCostText
                };

                pnComponents.Controls.Add(lblOptionPrice);
                pnComponents.Controls.Add(lblAdditionalCost);

                if (quoteCategory.QuoteMajorComponents != null)
                {
                    foreach (var quoteMajorComponent in quoteCategory.QuoteMajorComponents.Where(m => m.IsActive && m.IsVisible))
                    {
                        yCoordinate += 30;

                        var xOverrideCoordinate = CategoryXCoordinate - 145;
                        var chkOverrideQuoteMajorComponentData = new CheckBox
                        {
                            Width = 50,
                            Location = new Point(CategoryXCoordinate - 145, yCoordinate),
                            Tag = quoteMajorComponent,
                            Name = $"{"chkMajorComponentOverrideData"}{quoteMajorComponent.Id_}",
                            Checked = quoteMajorComponent.OverrideComponentData
                        };

                        var xBDCoordinate = CategoryXCoordinate - 55;
                        var chkIncludeInBuildDocumentMajorComponent = new CheckBox
                        {
                            Width = 50,
                            Location = new Point(xBDCoordinate, yCoordinate),
                            Tag = quoteMajorComponent,
                            Name = $"{"chkIncludeInBuildDocumentMajorComponent"}{quoteMajorComponent.Id_}",
                            Checked = quoteMajorComponent.IncludeInBuildDocument
                        };

                        xCoordinate = CategoryXCoordinate;

                        var txtMajorComponentCalculation = new TextBox
                        {
                            Width = majorComponentDetailTextBoxWidth,
                            Location = new Point(xCoordinate, yCoordinate),
                            Tag = quoteMajorComponent,
                            Name = $"{"txtMajorComponentCalculation"}{quoteMajorComponent.Id_}",
                            Text = quoteMajorComponent.DetailCalculationResult
                        };

                        xCoordinate += 75;
                        var txtMajorComponentName = new TextBox
                        {
                            Width = 600,
                            Location = new Point(xCoordinate, yCoordinate),
                            Tag = quoteMajorComponent,
                            Name = $"{"txtMajorComponentName"}{quoteMajorComponent.Id_}",
                            Text = quoteMajorComponent.DescriptionCalculationResult
                        };
                        txtMajorComponentName.DoubleClick += txtMajorComponentName_DoubleClick;

                        xCoordinate = removeOptionalButtonPosition;
                        var btnRemoveMajorComponent = new LinkLabel
                        {
                            Width = buttonWidth,
                            Location = new Point(xCoordinate, yCoordinate),
                            Tag = quoteMajorComponent,
                            Name = $"{"btnRemoveMajorComponent"}{quoteMajorComponent.Id_}",
                            Text = removeText
                        };
                        btnRemoveMajorComponent.Click += btnRemoveComponent_Click;

                        pnComponents.Controls.Add(chkOverrideQuoteMajorComponentData);
                        pnComponents.Controls.Add(chkIncludeInBuildDocumentMajorComponent);
                        pnComponents.Controls.Add(txtMajorComponentCalculation);
                        pnComponents.Controls.Add(txtMajorComponentName);
                        pnComponents.Controls.Add(btnRemoveMajorComponent);

                        var txtOptionPrice = new RadMaskedEditBox
                        {
                            Width = 70,
                            TextAlign = HorizontalAlignment.Right,
                            Location = new Point(CategoryXCoordinate + 700, yCoordinate),
                            Mask = "F2",
                            MaskType = MaskType.Numeric,
                            Tag = quoteMajorComponent,
                            Name = $"{"txtOptionPrice"}{quoteMajorComponent.Id_}",
                            Text = quoteMajorComponent.CalculatedPrice.ToString("N2")
                        };

                        var txtAdditionalCost = new RadMaskedEditBox
                        {
                            Width = 70,
                            TextAlign = HorizontalAlignment.Right,
                            Location = new Point(CategoryXCoordinate + 800, yCoordinate),
                            Mask = "F2",
                            MaskType = MaskType.Numeric,
                            Tag = quoteMajorComponent,
                            Name = $"{"txtAdditionalCost"}{quoteMajorComponent.Id_}",
                            Text = quoteMajorComponent.AssignedPrice.ToString("N2")
                        };

                        pnComponents.Controls.Add(txtOptionPrice);
                        pnComponents.Controls.Add(txtAdditionalCost);

                        if (quoteMajorComponent.QuoteMajorComponentDetails != null)
                        {
                            foreach (var quoteMajorComponentDetail in quoteMajorComponent.QuoteMajorComponentDetails.Where(m => m.IsVisible))
                            {
                                xCoordinate = CategoryXCoordinate + 100;
                                yCoordinate += 25;

                                var chkIncludeInBuildDocumentMajorComponentDetail = new CheckBox
                                {
                                    Width = 50,
                                    Location = new Point(xBDCoordinate, yCoordinate),
                                    Tag = quoteMajorComponentDetail,
                                    Name =
                                        $"{"chkIncludeInBuildDocumentMajorComponentDetail"}{quoteMajorComponentDetail.Id_}",
                                    Checked = quoteMajorComponentDetail.IncludeInBuildDocument
                                };

                                var txtMajorComponentDetailName = new TextBox
                                {
                                    Width = 575,
                                    Location = new Point(xCoordinate, yCoordinate),
                                    Tag = quoteMajorComponentDetail,
                                    Name =
                                        $"{"txtMajorComponentDetailName"}{quoteMajorComponentDetail.Id_}",
                                    Text = $"{quoteMajorComponentDetail.Name}"
                                };

                                xCoordinate = removeOptionalButtonPosition;
                                var btnRemoveMajorComponentDetail = new LinkLabel
                                {
                                    Width = buttonWidth,
                                    Location = new Point(xCoordinate, yCoordinate),
                                    Tag = quoteMajorComponentDetail,
                                    Name =
                                        $"{"btnRemoveMajorComponentDetail"}{quoteMajorComponentDetail.Id_}",
                                    Text = removeText
                                };
                                btnRemoveMajorComponentDetail.Click += btnRemoveComponent_Click;

                                pnComponents.Controls.Add(chkIncludeInBuildDocumentMajorComponentDetail);
                                pnComponents.Controls.Add(txtMajorComponentDetailName);
                                pnComponents.Controls.Add(btnRemoveMajorComponentDetail);
                            }
                        }

                        if (quoteMajorComponent.QuoteMinorComponents != null)
                        {
                            foreach (var quoteMinorComponent in quoteMajorComponent.QuoteMinorComponents.Where(m => m.IsActive && m.IsVisible))
                            {
                                xCoordinate = CategoryXCoordinate + 75;
                                yCoordinate += 25;

                                var chkOverrideQuoteMinorComponentData = new CheckBox
                                {
                                    Width = 50,
                                    Location = new Point(xOverrideCoordinate, yCoordinate),
                                    Tag = quoteMinorComponent,
                                    Name = $"{"chkMinorComponentOverrideData"}{quoteMinorComponent.Id_}",
                                    Checked = quoteMinorComponent.OverrideComponentData
                                };

                                var chkIncludeInBuildDocumentMinorComponent = new CheckBox
                                {
                                    Width = 50,
                                    Location = new Point(xBDCoordinate, yCoordinate),
                                    Tag = quoteMinorComponent,
                                    Name = $"{"chkIncludeInBuildDocumentMinorComponent"}{quoteMinorComponent.Id_}",
                                    Checked = quoteMinorComponent.IncludeInBuildDocument
                                };

                                var txtMinorComponentCalculation = new TextBox
                                {
                                    Width = detailTextBoxWidth,
                                    Location = new Point(xCoordinate, yCoordinate),
                                    Tag = quoteMinorComponent,
                                    Name =
                                        $"{"txtMinorComponentCalculation"}{quoteMinorComponent.Id_}",
                                    Text = quoteMinorComponent.DetailCalculationResult
                                };

                                xCoordinate = CategoryXCoordinate + 130;

                                var txtMinorComponentName = new TextBox
                                {
                                    Width = 545,
                                    Location = new Point(xCoordinate, yCoordinate),
                                    Tag = quoteMinorComponent,
                                    Name = $"{"txtMinorComponentName"}{quoteMinorComponent.Id_}",
                                    Text = quoteMinorComponent.DescriptionCalculationResult
                                };
                                txtMinorComponentName.DoubleClick += txtMinorComponentName_DoubleClick;

                                xCoordinate = removeOptionalButtonPosition;
                                var btnRemoveMinorComponent = new LinkLabel
                                {
                                    Width = buttonWidth,
                                    Location = new Point(xCoordinate, yCoordinate),
                                    Tag = quoteMinorComponent,
                                    Name = $"{"btnRemoveMinorComponent"}{quoteMinorComponent.Id_}",
                                    Text = removeText
                                };
                                btnRemoveMinorComponent.Click += btnRemoveComponent_Click;

                                pnComponents.Controls.Add(chkOverrideQuoteMinorComponentData);
                                pnComponents.Controls.Add(chkIncludeInBuildDocumentMinorComponent);
                                pnComponents.Controls.Add(txtMinorComponentCalculation);
                                pnComponents.Controls.Add(txtMinorComponentName);
                                pnComponents.Controls.Add(btnRemoveMinorComponent);

                                if (quoteMinorComponent.QuoteMinorComponentDetails != null)
                                {
                                    foreach (var quoteMinorComponentDetail in quoteMinorComponent.QuoteMinorComponentDetails.Where(m => m.IsVisible))
                                    {
                                        xCoordinate = CategoryXCoordinate + 150;
                                        yCoordinate += 25;

                                        var chkIncludeInBuildDocumentMinorComponentDetail = new CheckBox
                                        {
                                            Width = 50,
                                            Location = new Point(xBDCoordinate, yCoordinate),
                                            Tag = quoteMinorComponentDetail,
                                            Name =
                                                $"{"chkIncludeInBuildDocumentMinorComponentDetail"}{quoteMinorComponentDetail.Id_}",
                                            Checked = quoteMinorComponentDetail.IncludeInBuildDocument
                                        };

                                        var txtMinorComponentDetailName = new TextBox
                                        {
                                            Width = 525,
                                            Location = new Point(xCoordinate, yCoordinate),
                                            Tag = quoteMinorComponentDetail,
                                            Name =
                                                $"{"txtMinorComponentDetailName"}{quoteMinorComponentDetail.Id_}",
                                            Text = quoteMinorComponentDetail.Name
                                        };

                                        xCoordinate = removeOptionalButtonPosition;
                                        var btnRemoveMinorComponentDetail = new LinkLabel
                                        {
                                            Width = buttonWidth,
                                            Location = new Point(xCoordinate, yCoordinate),
                                            Tag = quoteMinorComponentDetail,
                                            Name =
                                                $"{"btnRemoveMinorComponentDetail"}{quoteMinorComponentDetail.Id_}",
                                            Text = removeText
                                        };
                                        btnRemoveMinorComponentDetail.Click += btnRemoveComponent_Click;

                                        pnComponents.Controls.Add(chkIncludeInBuildDocumentMinorComponentDetail);
                                        pnComponents.Controls.Add(txtMinorComponentDetailName);
                                        pnComponents.Controls.Add(btnRemoveMinorComponentDetail);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            yCoordinate += 50;

            var lblTotalCost = new Label
            {
                Text = "TOTAL PRICE PER SYSTEM:",
                Font = categoryFont,
                Width = 400,
                Location = new Point(CategoryXCoordinate + 205, yCoordinate)
            };

            var lblTotalCostValue = new Label
            {
                Name = "lblTotalCostValue",
                Text = QuoteBoomSystem.GetTotalCost().ToString("C2"),
                Font = categoryFont,
                Width = 300,
                Location = new Point(CategoryXCoordinate + 640, yCoordinate)
            };

            pnComponents.Controls.Add(lblTotalCost);
            pnComponents.Controls.Add(lblTotalCostValue);

            pnComponents.VerticalScroll.Value = scrollbarVerticalPosition;
            pnComponents.PerformLayout();
            pnComponents.Refresh();
        }

        private async void btnRemoveComponent_Click(object sender, EventArgs e)
        {
            var button = sender as LinkLabel;
            var component = button.Tag;

            var namedComponent = component as INamedAndOrderedComponent;
            var result = MessageBox.Show($"Do you want to remove {namedComponent.Name}?", "QuotationvSystem", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                var activeComponent = component as IVisible;
                activeComponent.IsVisible = false;

                await SaveChanges();
                scrollbarVerticalPosition = pnComponents.VerticalScroll.Value + additionalScrollbarVerticalPosition;
                CreateComponentControls();
            }
        }

        #region add components manually

        private async void txtMinorComponentName_DoubleClick(object sender, EventArgs e)
        {
            var minorComponentTextBox = sender as TextBox;
            var minorComponentName = minorComponentTextBox.Text;
            var answer = MessageBox.Show($"Add Minor Component Detail to {minorComponentName}?", "Quotation System", MessageBoxButtons.YesNo);
            if (answer == DialogResult.Yes)
            {
                await saveQuoteBoomSystem();
                var minorComponent = minorComponentTextBox.Tag as QuoteMinorComponent;
                var nextOrder = 1;
                if (minorComponent.QuoteMinorComponentDetails.Any())
                {
                    nextOrder = minorComponent.QuoteMinorComponentDetails.Count + 1;
                }

                var newQuoteMinorComponentDetail = new QuoteMinorComponentDetail
                {
                    Order = nextOrder,
                    IncludeInBuildDocument = true,
                    IsVisible = true
                };
                newQuoteMinorComponentDetail.GenerateID();
                minorComponent.QuoteMinorComponentDetails.Add(newQuoteMinorComponentDetail);

                await SaveChanges();
                scrollbarVerticalPosition = pnComponents.VerticalScroll.Value + additionalScrollbarVerticalPosition;
                CreateComponentControls();
            }
        }

        private async void txtMajorComponentName_DoubleClick(object sender, EventArgs e)
        {
            var majorComponentTextBox = sender as TextBox;
            var majorComponentName = majorComponentTextBox.Text;
            var answer1 = MessageBox.Show($"Add Minor Component to {majorComponentName}?", "Quotation System", MessageBoxButtons.YesNo);
            if (answer1 == DialogResult.Yes)
            {
                await saveQuoteBoomSystem();
                var majorComponent = majorComponentTextBox.Tag as QuoteMajorComponent;
                var nextOrder = 1;
                if (majorComponent.QuoteMinorComponents.Any())
                {
                    nextOrder = majorComponent.QuoteMinorComponents.Count + 1;
                }

                var newQuoteMinorComponent = new QuoteMinorComponent
                {
                    QuoteMinorComponentDetails = new List<QuoteMinorComponentDetail>(),
                    Order = nextOrder,
                    IncludeInBuildDocument = true,
                    IsActive = true,
                    IsVisible = true
                };
                newQuoteMinorComponent.GenerateID();
                majorComponent.QuoteMinorComponents.Add(newQuoteMinorComponent);

                await SaveChanges();
                scrollbarVerticalPosition = pnComponents.VerticalScroll.Value + additionalScrollbarVerticalPosition;
                CreateComponentControls();
            }
            else
            {
                var answer2 = MessageBox.Show($"Add Major Component Detail to {majorComponentName}?", "Quotation System", MessageBoxButtons.YesNo);
                if (answer2 == DialogResult.Yes)
                {
                    await saveQuoteBoomSystem();
                    var majorComponent = majorComponentTextBox.Tag as QuoteMajorComponent;
                    var nextOrder = 1;
                    if (majorComponent.QuoteMajorComponentDetails.Any())
                    {
                        nextOrder = majorComponent.QuoteMajorComponentDetails.Count + 1;
                    }

                    var newQuoteMajorComponentDetail = new QuoteMajorComponentDetail
                    {
                        Order = nextOrder,
                        IncludeInBuildDocument = true,
                        IsVisible = true
                    };
                    newQuoteMajorComponentDetail.GenerateID();
                    majorComponent.QuoteMajorComponentDetails.Add(newQuoteMajorComponentDetail);

                    await SaveChanges();
                    scrollbarVerticalPosition = pnComponents.VerticalScroll.Value + additionalScrollbarVerticalPosition;
                    CreateComponentControls();
                }
            }
        }

        private async void txtCategoryName_DoubleClick(object sender, EventArgs e)
        {
            var categoryTextBox = sender as TextBox;
            var categoryName = categoryTextBox.Text;
            var answer = MessageBox.Show($"Add Major Component to {categoryName}?", "Quotation System", MessageBoxButtons.YesNo);
            if (answer == DialogResult.Yes)
            {
                await saveQuoteBoomSystem();
                var category = categoryTextBox.Tag as QuoteCategory;
                var nextOrder = 1;
                if (category.QuoteMajorComponents.Any())
                {
                    nextOrder = category.QuoteMajorComponents.Count + 1;
                }

                var newQuoteMajorComponent = new QuoteMajorComponent
                {
                    QuoteMajorComponentDetails = new List<QuoteMajorComponentDetail>(),
                    QuoteMinorComponents = new List<QuoteMinorComponent>(),
                    Order = nextOrder,
                    IncludeInBuildDocument = true,
                    IsActive = true,
                    IsVisible = true
                };
                newQuoteMajorComponent.GenerateID();
                category.QuoteMajorComponents.Add(newQuoteMajorComponent);

                await SaveChanges();
                scrollbarVerticalPosition = pnComponents.VerticalScroll.Value + additionalScrollbarVerticalPosition;
                CreateComponentControls();
            }
        }

        #endregion add components manually

        private void refreshTotalCost()
        {
            var totalCostTextBox = pnComponents.Controls
                                   .Find("lblTotalCostValue", false)
                                   .First()
                                   as Label;
            totalCostTextBox.Text = QuoteBoomSystem.GetTotalCost().ToString("C2");
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            await saveQuoteBoomSystem();

            MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
        }

        private async void btnGenerateDetailedReport_Click(object sender, EventArgs e)
        {
            LoadingForm.Show("Generating System Report...");

            var reportManager = new ReportManager();
            var fileName = await reportManager.GenerateDetailQuoteReportFile(QuoteRequest.Id, QuoteBoomSystem.Id_);
            showReport(fileName);
        }

        private async void btnGenerateBuildDocument_Click(object sender, EventArgs e)
        {
            LoadingForm.Show("Generating Build Document...");

            var reportManager = new ReportManager();
            var fileName = await reportManager.GenerateDetailBuildReport(QuoteRequest.Id, QuoteBoomSystem.Id_);
            showReport(fileName);
        }

        private void showReport(string fileName)
        {
            var process = Process.Start(fileName);
            if (process != null)
            {
                var handle = process.MainWindowHandle;
                BringWindowToTop(handle);
            }
            LoadingForm.Hide();
        }

        private Task saveQuoteBoomSystem()
        {
            QuoteBoomSystem.Name = txtSystemName.Text;
            QuoteBoomSystem.AssignedPrice = Convert.ToDouble(txtSystemCost.Text);
            QuoteBoomSystem.DescriptionCalculationResult = txtSystemName.Text;
            QuoteBoomSystem.DetailCalculationResult = txtSystemDetailCalculationResult.Text;
            QuoteBoomSystem.Quantity = Convert.ToInt32(txtQuantityOfSystems.Text);
            Boom.QuantityOfSystems = QuoteBoomSystem.Quantity;
            QuoteBoomSystem.OverrideComponentData = chkOverrideSystemComponent.Checked;

            foreach (var editControl in pnComponents.Controls)
            {
                var textbox = editControl as TextBox;
                var maskedEditBox = editControl as RadMaskedEditBox;
                var checkBox = editControl as CheckBox;

                //Regular detail calculation textboxes
                if (textbox != null)
                {
                    var textboxName = textbox.Name.ToLower();
                    var text = textbox.Text;
                    if (textboxName.Contains("name"))
                    {
                        var component = textbox.Tag as IDescriptionCalculatedComponent;
                        if (component != null)
                        {
                            component.Name = text;
                            component.DescriptionCalculationResult = text;
                        }
                        else
                        {
                            var componentInamed = textbox.Tag as INamedAndOrderedComponent;
                            if (componentInamed != null)
                            {
                                componentInamed.Name = text;
                            }
                        }
                    }
                    else if (textboxName.Contains("calculation"))
                    {
                        var componenDetailCalculation = textbox.Tag as IDetailCalculatedComponent;
                        if (componenDetailCalculation != null)
                        {
                            componenDetailCalculation.DetailCalculationResult = text;
                        }
                    }
                }
                //Masked textboxes for additional component costs
                else if (maskedEditBox != null)
                {
                    var componentAdditionalPriceCalculation = maskedEditBox.Tag as IPriceCalculatedComponent;

                    var textboxName = maskedEditBox.Name.ToLower();
                    var text = maskedEditBox.Text;
                    if (textboxName.Contains("additionalcost"))
                    {
                        componentAdditionalPriceCalculation.AssignedPrice = Convert.ToDouble(text);
                    }
                    else if (textboxName.Contains("optionprice"))
                    {
                        componentAdditionalPriceCalculation.CalculatedPrice = Convert.ToDouble(text);
                    }
                }

                if (checkBox != null)
                {
                    var checkboxName = checkBox.Name.ToLower();
                    if (checkboxName.Contains("includeinbuilddocument"))
                    {
                        var componentIncludeInBuildDocument = checkBox.Tag as IDescriptionCalculatedComponent;
                        componentIncludeInBuildDocument.IncludeInBuildDocument = checkBox.Checked;
                    }
                    else if (checkboxName.Contains("componentoverridedata"))
                    {
                        var componentOverrideAnswer = checkBox.Tag as IDetailCalculatedComponent;
                        componentOverrideAnswer.OverrideComponentData = checkBox.Checked;
                    }
                }
            }

            return SaveChanges();
        }

        private async Task SaveChanges()
        {
            QuoteRequest.Revision++;
            QuoteBoomSystem.Boom.QuantityOfSystems = QuoteBoomSystem.Quantity;
            QuoteRequest.QuoteBoomSystemsData.Update();

            await _context.SaveChangesAsync();

            var sugarCRMClient = new SugarCRMClient();
            await sugarCRMClient.ExportQuoteToSugarCRM(QuoteRequest.Id);

            refreshTotalCost();
            OnSaveFinished(QuoteRequest.Id);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;    // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
    }
}