﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.DAL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CherryCreekSystems
{
    public partial class ShipAddressesForm : Form
    {
        public delegate void SaveFinished();

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly ShipAddressService shipAddressService;
        private readonly StatesService statesService;

        private readonly int CustomerId;

        private ShipAddress _currentShipAddress;

        public ShipAddress CurrentShipAddress
        {
            get { return _currentShipAddress; }
            set
            {
                _currentShipAddress = value;
                if (_currentShipAddress != null)
                {
                    txtAddressName.Text = _currentShipAddress.Name;
                    txtStreet1.Text = _currentShipAddress.Street1;
                    txtStreet2.Text = _currentShipAddress.Street2;
                    txtCity.Text = _currentShipAddress.City;
                    if (_currentShipAddress.StateId.HasValue)
                        ddlStates.SelectedValue = _currentShipAddress.StateId;
                    txtZip.Text = _currentShipAddress.Zip;

                    btnUpdate.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    btnUpdate.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public ShipAddressesForm(int customerId)
        {
            InitializeComponent();

            _context = new DataContext();
            CustomerId = customerId;

            shipAddressService = new ShipAddressService(_context);
            statesService = new StatesService();
        }

        public async Task Initialize()
        {
            await bindStatesList();
            await bindShipAddressesList();
        }

        private async Task bindShipAddressesList()
        {
            var shipAddressesList = await shipAddressService.Get().ByCustomerId(CustomerId).IncludeStates().ToListAsync();

            ddlShipAddresses.DataSource = shipAddressesList;
            ddlShipAddresses.DisplayMember = "FullAddress";
            ddlShipAddresses.SelectedIndex = -1;
        }

        private async Task bindStatesList()
        {
            var statesList = await statesService.Get().ToListAsync();

            ddlStates.DataSource = statesList;
            ddlStates.DisplayMember = "Name";
            ddlStates.ValueMember = "Id";
            ddlStates.SelectedIndex = -1;
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            ShipAddress shipAddress = new ShipAddress();
            bool isValidShipAddress = ValidateAndPopulateShipAddress(shipAddress);
            if (isValidShipAddress)
            {
                shipAddressService.Add(shipAddress);
                await _context.SaveChangesAsync();

                await bindShipAddressesList();
                ddlShipAddresses.SelectedItem = shipAddress;
                OnSaveFinished();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidShipAddress = ValidateAndPopulateShipAddress(CurrentShipAddress);
            if (isValidShipAddress)
            {
                await _context.SaveChangesAsync();

                var currentAddressIndex = ddlShipAddresses.SelectedIndex;
                await bindShipAddressesList();
                ddlShipAddresses.SelectedIndex = currentAddressIndex;
                OnSaveFinished();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private bool ValidateAndPopulateShipAddress(ShipAddress shipAddress)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(shipAddress);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(ShipAddress shipAddress)
        {
            var selectedState = ddlStates.SelectedItem as State;

            shipAddress.Name = txtAddressName.Text.Trim();
            shipAddress.Street1 = txtStreet1.Text.Trim();
            shipAddress.Street2 = txtStreet2.Text.Trim();
            shipAddress.City = txtCity.Text.Trim();
            shipAddress.StateId = selectedState?.Id;
            shipAddress.Zip = txtZip.Text.Trim();
            shipAddress.CustomerId = CustomerId;
        }

        private void ValidateTextboxHasValue(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        private void ValidateDropdownSelectedValue(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateDropdownSelectedValue(sender, dropdownValueErrorProvider, e);
        }

        private void ddlShipAddresses_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedShipAddress = ddlShipAddresses.SelectedValue as ShipAddress;
            CurrentShipAddress = selectedShipAddress;
        }

        private void mnNewShipAddress_Click(object sender, EventArgs e)
        {
            CurrentShipAddress = null;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ShipAddressesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _context.Dispose();
        }
    }
}