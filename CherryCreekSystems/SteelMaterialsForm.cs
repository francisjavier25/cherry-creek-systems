﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using CherryCreekSystems.DAL;
using Telerik.WinControls.UI;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.BL.QueryExtensions;

namespace CherryCreekSystems
{
    public partial class SteelMaterialsForm : Form
    {
        public delegate void SaveFinished();

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private QuoteRequest _quoteRequest;

        private QuoteBoomSystem _system;

        private SteelMaterial _CurrentSteelMaterial;

        public SteelMaterial CurrentSteelMaterial
        {
            get { return _CurrentSteelMaterial; }
            set
            {
                _CurrentSteelMaterial = value;
                if (_CurrentSteelMaterial != null)
                {
                    txtMaterial.Text = _CurrentSteelMaterial.Material;
                    txtQuantity.Text = _CurrentSteelMaterial.Quantity;

                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public SteelMaterialsForm(QuoteRequest quoteRequest, QuoteBoomSystem system, DataContext context)
        {
            InitializeComponent();

            _context = context;

            _system = system;

            bindSteelMaterialsList();
            bindSteelMaterialDefaultsList();
        }

        private void bindSteelMaterialsList()
        {
            var steelMaterials = _system.SteelMaterials
                                          .Where(c => c.Active)
                                          .OrderBy(d => d.Id)
                                          .ToList();

            ddlSteelMaterials.DataSource = steelMaterials;
            ddlSteelMaterials.DisplayMember = "Material";
            ddlSteelMaterials.SelectedIndex = -1;
        }

        private void bindSteelMaterialDefaultsList()
        {
            var steelDefaultMaterialService = new SteelMaterialDefaultService();
            var steelMaterialDefaults = steelDefaultMaterialService.Get().OrderById().SelectMaterial().ToList();

            ddlSteelMaterialDefaults.DataSource = steelMaterialDefaults;
            ddlSteelMaterialDefaults.SelectedIndex = -1;
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            var steelMaterial = new SteelMaterial();
            steelMaterial.GenerateID();
            bool isValidItem = ValidateAndPopulateSteelMaterial(steelMaterial);
            if (isValidItem)
            {
                _system.SteelMaterials.Add(steelMaterial);
                //_quoteRequest.QuoteBoomSystemsData.Update();
                await _context.SaveChangesAsync();

                bindSteelMaterialsList();
                ddlSteelMaterials.SelectedItem = steelMaterial;
                OnSaveFinished?.Invoke();

                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidItem = ValidateAndPopulateSteelMaterial(CurrentSteelMaterial);
            if (isValidItem)
            {
                await _context.SaveChangesAsync();

                var currentItemIndex = ddlSteelMaterials.SelectedIndex;
                bindSteelMaterialsList();
                ddlSteelMaterials.SelectedIndex = currentItemIndex;
                OnSaveFinished?.Invoke();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            CurrentSteelMaterial.Active = false;
            //_quoteRequest.QuoteBoomSystemsData.Update();
            await _context.SaveChangesAsync();

            CurrentSteelMaterial = null;

            bindSteelMaterialsList();

            MessageBox.Show("Item has been removed", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
        }

        private bool ValidateAndPopulateSteelMaterial(SteelMaterial steelMaterial)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(steelMaterial);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(SteelMaterial steelMaterial)
        {
            steelMaterial.Material = txtMaterial.Text.Trim();
            steelMaterial.Quantity = txtQuantity.Text;
            steelMaterial.Active = true;
        }

        private void mnNewItem_Click(object sender, EventArgs e)
        {
            CurrentSteelMaterial = null;
        }

        private void ddlItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = ddlSteelMaterials.SelectedValue as SteelMaterial;
            CurrentSteelMaterial = selectedItem;
        }

        private void ddlSteelMaterialDefaults_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = ddlSteelMaterialDefaults.SelectedValue as string;
            txtMaterial.Text = selectedItem; 
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, errorProvider, e);
        }

        private void txtQuantity_Validating(object sender, CancelEventArgs e)
        {
            var textBox = sender as RadMaskedEditBox;
            ValidationUtilities.ValidateGreaterThanZero(textBox, errorProvider, e);
        }
    }
}