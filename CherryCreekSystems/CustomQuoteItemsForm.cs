﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using CherryCreekSystems.DAL;
using Telerik.WinControls.UI;

namespace CherryCreekSystems
{
    public partial class CustomQuoteItemsForm : Form
    {
        public delegate void SaveFinished();

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private QuoteRequest _quoteRequest;

        private CustomQuoteItem _CurrentCustomQuoteItem;

        public CustomQuoteItem CurrentCustomQuoteItem
        {
            get { return _CurrentCustomQuoteItem; }
            set
            {
                _CurrentCustomQuoteItem = value;
                if (_CurrentCustomQuoteItem != null)
                {
                    txtName.Text = _CurrentCustomQuoteItem.Description;
                    txtPrice.Text = _CurrentCustomQuoteItem.Price.ToString();
                    txtQuantity.Text = _CurrentCustomQuoteItem.Quantity.ToString();

                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;
                    btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    btnAdd.Visible = true;
                }
            }
        }

        public CustomQuoteItemsForm(QuoteRequest quoteRequest, DataContext context)
        {
            InitializeComponent();

            _context = context;

            _quoteRequest = quoteRequest;

            bindCustomQuoteItemsList();
        }

        private void bindCustomQuoteItemsList()
        {
            var customQuoteItems = _quoteRequest.CustomQuoteItems
                                                .Where(c => c.Active)
                                                .OrderBy(d => d.Description)
                                                .ToList();

            ddlCustomQuoteItems.DataSource = customQuoteItems;
            ddlCustomQuoteItems.DisplayMember = "Description";
            ddlCustomQuoteItems.SelectedIndex = -1;
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            var customQuoteItem = new CustomQuoteItem();
            bool isValidItem = ValidateAndPopulateCustomQuoteItem(customQuoteItem);
            if (isValidItem)
            {
                _quoteRequest.CustomQuoteItems.Add(customQuoteItem);
                await _context.SaveChangesAsync();

                bindCustomQuoteItemsList();
                ddlCustomQuoteItems.SelectedItem = customQuoteItem;
                OnSaveFinished?.Invoke();

                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidItem = ValidateAndPopulateCustomQuoteItem(CurrentCustomQuoteItem);
            if (isValidItem)
            {
                await _context.SaveChangesAsync();

                var currentItemIndex = ddlCustomQuoteItems.SelectedIndex;
                bindCustomQuoteItemsList();
                ddlCustomQuoteItems.SelectedIndex = currentItemIndex;
                OnSaveFinished?.Invoke();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            CurrentCustomQuoteItem.Active = false;

            await _context.SaveChangesAsync();

            CurrentCustomQuoteItem = null;

            bindCustomQuoteItemsList();

            MessageBox.Show("Item has been removed", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
        }

        private bool ValidateAndPopulateCustomQuoteItem(CustomQuoteItem customQuoteItem)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(customQuoteItem);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(CustomQuoteItem customQuoteItem)
        {
            customQuoteItem.Description = txtName.Text.Trim();
            customQuoteItem.Price = Convert.ToDouble(txtPrice.Text);
            customQuoteItem.Quantity = Convert.ToInt32(txtQuantity.Text);
            customQuoteItem.Active = true;
        }

        private void mnNewItem_Click(object sender, EventArgs e)
        {
            CurrentCustomQuoteItem = null;
        }

        private void ddlItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = ddlCustomQuoteItems.SelectedValue as CustomQuoteItem;
            CurrentCustomQuoteItem = selectedItem;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, errorProvider, e);
        }

        private void txtQuantity_Validating(object sender, CancelEventArgs e)
        {
            var textBox = sender as RadMaskedEditBox;
            ValidationUtilities.ValidateGreaterThanZero(textBox, errorProvider, e);
        }
    }
}