﻿using log4net;
using log4net.Config;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CherryCreekSystems
{
    internal static class Program
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            XmlConfigurator.Configure();

            Task.Run(() =>
            {
                var appDirectory = AppDomain.CurrentDomain.BaseDirectory;
                var directory = new DirectoryInfo(appDirectory);
                var pdfFiles = directory.GetFiles("*.pdf");
                foreach (var f in pdfFiles)
                {
                    f.Delete();
                }
            });

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new QuoteRequestForm());
        }
    }
}