﻿namespace CherryCreekSystems
{
    partial class MajorComponentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ddlOrder = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtMajorComponentName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbAdditionalPriceCondition = new System.Windows.Forms.Label();
            this.txtDetailCalculation = new System.Windows.Forms.TextBox();
            this.txtAddPriceCondition = new System.Windows.Forms.TextBox();
            this.lstBoomSpecs = new System.Windows.Forms.ListBox();
            this.label33 = new System.Windows.Forms.Label();
            this.emptyTextErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtAdditionalPriceCalculation = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDetailId = new System.Windows.Forms.Label();
            this.lblPriceID = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDescriptionId = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescriptionCalculation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtActivateCondition = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.emptyTextErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // ddlOrder
            // 
            this.ddlOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlOrder.FormattingEnabled = true;
            this.ddlOrder.Location = new System.Drawing.Point(216, 704);
            this.ddlOrder.Name = "ddlOrder";
            this.ddlOrder.Size = new System.Drawing.Size(805, 24);
            this.ddlOrder.TabIndex = 63;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(149, 707);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 62;
            this.label1.Text = "Order:";
            this.label1.UseMnemonic = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(695, 759);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(113, 52);
            this.btnClose.TabIndex = 61;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(431, 759);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(113, 52);
            this.btnUpdate.TabIndex = 60;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(431, 759);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(113, 52);
            this.btnAdd.TabIndex = 59;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtMajorComponentName
            // 
            this.txtMajorComponentName.Location = new System.Drawing.Point(217, 69);
            this.txtMajorComponentName.Name = "txtMajorComponentName";
            this.txtMajorComponentName.Size = new System.Drawing.Size(805, 22);
            this.txtMajorComponentName.TabIndex = 58;
            this.txtMajorComponentName.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateTextboxHasValue);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(149, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 57;
            this.label3.Text = "Name:";
            this.label3.UseMnemonic = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 153);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 64;
            this.label2.Text = "Detail Calculation:";
            this.label2.UseMnemonic = false;
            // 
            // lbAdditionalPriceCondition
            // 
            this.lbAdditionalPriceCondition.AutoSize = true;
            this.lbAdditionalPriceCondition.Location = new System.Drawing.Point(26, 383);
            this.lbAdditionalPriceCondition.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbAdditionalPriceCondition.Name = "lbAdditionalPriceCondition";
            this.lbAdditionalPriceCondition.Size = new System.Drawing.Size(173, 17);
            this.lbAdditionalPriceCondition.TabIndex = 66;
            this.lbAdditionalPriceCondition.Text = "Additional Price Condition:";
            this.lbAdditionalPriceCondition.UseMnemonic = false;
            // 
            // txtDetailCalculation
            // 
            this.txtDetailCalculation.AllowDrop = true;
            this.txtDetailCalculation.Location = new System.Drawing.Point(217, 150);
            this.txtDetailCalculation.Multiline = true;
            this.txtDetailCalculation.Name = "txtDetailCalculation";
            this.txtDetailCalculation.Size = new System.Drawing.Size(805, 72);
            this.txtDetailCalculation.TabIndex = 67;
            this.txtDetailCalculation.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtDetailCalculation.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtDetailCalculation.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // txtAddPriceCondition
            // 
            this.txtAddPriceCondition.AllowDrop = true;
            this.txtAddPriceCondition.Location = new System.Drawing.Point(217, 380);
            this.txtAddPriceCondition.Multiline = true;
            this.txtAddPriceCondition.Name = "txtAddPriceCondition";
            this.txtAddPriceCondition.Size = new System.Drawing.Size(805, 72);
            this.txtAddPriceCondition.TabIndex = 68;
            this.txtAddPriceCondition.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtAddPriceCondition.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtAddPriceCondition.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // lstBoomSpecs
            // 
            this.lstBoomSpecs.FormattingEnabled = true;
            this.lstBoomSpecs.ItemHeight = 16;
            this.lstBoomSpecs.Location = new System.Drawing.Point(1085, 61);
            this.lstBoomSpecs.Name = "lstBoomSpecs";
            this.lstBoomSpecs.Size = new System.Drawing.Size(345, 756);
            this.lstBoomSpecs.TabIndex = 70;
            this.lstBoomSpecs.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstBoomSpecs_MouseDown);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(1197, 28);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(128, 24);
            this.label33.TabIndex = 72;
            this.label33.Text = "Boom Specs";
            // 
            // emptyTextErrorProvider
            // 
            this.emptyTextErrorProvider.BlinkRate = 0;
            this.emptyTextErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.emptyTextErrorProvider.ContainerControl = this;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(563, 759);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(113, 52);
            this.btnDelete.TabIndex = 77;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtAdditionalPriceCalculation
            // 
            this.txtAdditionalPriceCalculation.AllowDrop = true;
            this.txtAdditionalPriceCalculation.Location = new System.Drawing.Point(216, 507);
            this.txtAdditionalPriceCalculation.Multiline = true;
            this.txtAdditionalPriceCalculation.Name = "txtAdditionalPriceCalculation";
            this.txtAdditionalPriceCalculation.Size = new System.Drawing.Size(806, 72);
            this.txtAdditionalPriceCalculation.TabIndex = 78;
            this.txtAdditionalPriceCalculation.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtAdditionalPriceCalculation.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtAdditionalPriceCalculation.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(133, 118);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 17);
            this.label4.TabIndex = 80;
            this.label4.Text = "Detail ID:";
            this.label4.UseMnemonic = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(137, 473);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 17);
            this.label6.TabIndex = 82;
            this.label6.Text = "Price ID:";
            this.label6.UseMnemonic = false;
            // 
            // lblDetailId
            // 
            this.lblDetailId.AutoSize = true;
            this.lblDetailId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailId.Location = new System.Drawing.Point(214, 118);
            this.lblDetailId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDetailId.Name = "lblDetailId";
            this.lblDetailId.Size = new System.Drawing.Size(0, 17);
            this.lblDetailId.TabIndex = 83;
            this.lblDetailId.UseMnemonic = false;
            // 
            // lblPriceID
            // 
            this.lblPriceID.AutoSize = true;
            this.lblPriceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPriceID.Location = new System.Drawing.Point(214, 473);
            this.lblPriceID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPriceID.Name = "lblPriceID";
            this.lblPriceID.Size = new System.Drawing.Size(0, 17);
            this.lblPriceID.TabIndex = 84;
            this.lblPriceID.UseMnemonic = false;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(214, 35);
            this.lblId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(0, 17);
            this.lblId.TabIndex = 86;
            this.lblId.UseMnemonic = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(173, 35);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 17);
            this.label8.TabIndex = 85;
            this.label8.Text = "ID:";
            this.label8.UseMnemonic = false;
            // 
            // lblDescriptionId
            // 
            this.lblDescriptionId.AutoSize = true;
            this.lblDescriptionId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionId.Location = new System.Drawing.Point(214, 238);
            this.lblDescriptionId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescriptionId.Name = "lblDescriptionId";
            this.lblDescriptionId.Size = new System.Drawing.Size(0, 17);
            this.lblDescriptionId.TabIndex = 104;
            this.lblDescriptionId.UseMnemonic = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(87, 238);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 103;
            this.label5.Text = "Description ID:";
            this.label5.UseMnemonic = false;
            // 
            // txtDescriptionCalculation
            // 
            this.txtDescriptionCalculation.AllowDrop = true;
            this.txtDescriptionCalculation.Location = new System.Drawing.Point(217, 279);
            this.txtDescriptionCalculation.Multiline = true;
            this.txtDescriptionCalculation.Name = "txtDescriptionCalculation";
            this.txtDescriptionCalculation.Size = new System.Drawing.Size(804, 72);
            this.txtDescriptionCalculation.TabIndex = 102;
            this.txtDescriptionCalculation.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtDescriptionCalculation.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtDescriptionCalculation.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 282);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 17);
            this.label7.TabIndex = 101;
            this.label7.Text = "Description Calculation:";
            this.label7.UseMnemonic = false;
            // 
            // txtActivateCondition
            // 
            this.txtActivateCondition.AllowDrop = true;
            this.txtActivateCondition.Location = new System.Drawing.Point(215, 606);
            this.txtActivateCondition.Multiline = true;
            this.txtActivateCondition.Name = "txtActivateCondition";
            this.txtActivateCondition.Size = new System.Drawing.Size(806, 72);
            this.txtActivateCondition.TabIndex = 106;
            this.txtActivateCondition.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.txtActivateCondition.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            this.txtActivateCondition.DragOver += new System.Windows.Forms.DragEventHandler(this.txtDragOver);
            // 
            // label9
            // 
            this.label9.AllowDrop = true;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(73, 606);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 17);
            this.label9.TabIndex = 105;
            this.label9.Text = "Activate Condition:";
            this.label9.UseMnemonic = false;
            this.label9.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtDragDrop);
            this.label9.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtDragEnter);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(81, 510);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 17);
            this.label10.TabIndex = 107;
            this.label10.Text = "Price Calculation:";
            this.label10.UseMnemonic = false;
            // 
            // MajorComponentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1456, 874);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtActivateCondition);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblDescriptionId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDescriptionCalculation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblPriceID);
            this.Controls.Add(this.lblDetailId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAdditionalPriceCalculation);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lstBoomSpecs);
            this.Controls.Add(this.txtAddPriceCondition);
            this.Controls.Add(this.txtDetailCalculation);
            this.Controls.Add(this.lbAdditionalPriceCondition);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ddlOrder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtMajorComponentName);
            this.Controls.Add(this.label3);
            this.Name = "MajorComponentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Quotation System";
            ((System.ComponentModel.ISupportInitialize)(this.emptyTextErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtMajorComponentName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbAdditionalPriceCondition;
        private System.Windows.Forms.TextBox txtDetailCalculation;
        private System.Windows.Forms.TextBox txtAddPriceCondition;
        private System.Windows.Forms.ListBox lstBoomSpecs;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ErrorProvider emptyTextErrorProvider;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TextBox txtAdditionalPriceCalculation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPriceID;
        private System.Windows.Forms.Label lblDetailId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblDescriptionId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDescriptionCalculation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtActivateCondition;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}