﻿namespace CherryCreekSystems
{
    partial class BaseCostsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ddlBoomSystems = new System.Windows.Forms.ComboBox();
            this.gvBaseCosts = new System.Windows.Forms.DataGridView();
            this.X = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvBaseCosts)).BeginInit();
            this.SuspendLayout();
            // 
            // ddlBoomSystems
            // 
            this.ddlBoomSystems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlBoomSystems.FormattingEnabled = true;
            this.ddlBoomSystems.Location = new System.Drawing.Point(208, 62);
            this.ddlBoomSystems.Name = "ddlBoomSystems";
            this.ddlBoomSystems.Size = new System.Drawing.Size(308, 24);
            this.ddlBoomSystems.TabIndex = 0;
            this.ddlBoomSystems.SelectionChangeCommitted += new System.EventHandler(this.ddlBoomSystems_SelectionChangeCommitted);
            // 
            // gvBaseCosts
            // 
            this.gvBaseCosts.AllowUserToResizeColumns = false;
            this.gvBaseCosts.AllowUserToResizeRows = false;
            this.gvBaseCosts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvBaseCosts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.X,
            this.Y});
            this.gvBaseCosts.Enabled = false;
            this.gvBaseCosts.Location = new System.Drawing.Point(178, 166);
            this.gvBaseCosts.Name = "gvBaseCosts";
            this.gvBaseCosts.RowTemplate.Height = 24;
            this.gvBaseCosts.Size = new System.Drawing.Size(369, 269);
            this.gvBaseCosts.TabIndex = 1;
            this.gvBaseCosts.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvBaseCosts_CellEndEdit);
            this.gvBaseCosts.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.gvBaseCosts_CellValidating);
            this.gvBaseCosts.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.gvBaseCosts_UserAddedRow);
            this.gvBaseCosts.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.gvBaseCosts_UserDeletingRow);
            // 
            // X
            // 
            this.X.DataPropertyName = "XValue";
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = "0";
            this.X.DefaultCellStyle = dataGridViewCellStyle1;
            this.X.HeaderText = "Size";
            this.X.Name = "X";
            this.X.Width = 150;
            // 
            // Y
            // 
            this.Y.DataPropertyName = "YValue";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = "0";
            this.Y.DefaultCellStyle = dataGridViewCellStyle2;
            this.Y.HeaderText = "Base Cost";
            this.Y.Name = "Y";
            this.Y.Width = 150;
            // 
            // BaseCostsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 516);
            this.Controls.Add(this.gvBaseCosts);
            this.Controls.Add(this.ddlBoomSystems);
            this.Name = "BaseCostsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Quotation System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BaseCostsForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gvBaseCosts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlBoomSystems;
        private System.Windows.Forms.DataGridView gvBaseCosts;
        private System.Windows.Forms.DataGridViewTextBoxColumn X;
        private System.Windows.Forms.DataGridViewTextBoxColumn Y;
    }
}