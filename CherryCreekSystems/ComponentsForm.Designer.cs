﻿namespace CherryCreekSystems
{
    partial class ComponentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label24 = new System.Windows.Forms.Label();
            this.lstBooms = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lstCategories = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lstMajorComponents = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lstMinorComponents = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lstMinorComponentDetails = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lstMajorComponentDetails = new System.Windows.Forms.ListBox();
            this.btnAddCategory = new System.Windows.Forms.LinkLabel();
            this.btnAddMajorComponent = new System.Windows.Forms.LinkLabel();
            this.btnAddMinorComponent = new System.Windows.Forms.LinkLabel();
            this.btnAddMajorComponentDetail = new System.Windows.Forms.LinkLabel();
            this.btnAddMinorComponentDetail = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(647, 46);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(225, 25);
            this.label24.TabIndex = 51;
            this.label24.Text = "Manage Components:";
            // 
            // lstBooms
            // 
            this.lstBooms.FormattingEnabled = true;
            this.lstBooms.HorizontalScrollbar = true;
            this.lstBooms.ItemHeight = 16;
            this.lstBooms.Location = new System.Drawing.Point(35, 200);
            this.lstBooms.Name = "lstBooms";
            this.lstBooms.Size = new System.Drawing.Size(277, 500);
            this.lstBooms.TabIndex = 52;
            this.lstBooms.Click += new System.EventHandler(this.lstBooms_Click);
            this.lstBooms.DoubleClick += new System.EventHandler(this.lstBooms_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(149, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 24);
            this.label1.TabIndex = 53;
            this.label1.Text = "Booms";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(382, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 24);
            this.label2.TabIndex = 55;
            this.label2.Text = "Categories";
            // 
            // lstCategories
            // 
            this.lstCategories.FormattingEnabled = true;
            this.lstCategories.HorizontalScrollbar = true;
            this.lstCategories.ItemHeight = 16;
            this.lstCategories.Location = new System.Drawing.Point(349, 200);
            this.lstCategories.Name = "lstCategories";
            this.lstCategories.Size = new System.Drawing.Size(176, 500);
            this.lstCategories.TabIndex = 54;
            this.lstCategories.Click += new System.EventHandler(this.lstCategories_Click);
            this.lstCategories.SelectedIndexChanged += new System.EventHandler(this.lstCategories_SelectedIndexChanged);
            this.lstCategories.DoubleClick += new System.EventHandler(this.lstCategories_DoubleClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(702, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 24);
            this.label3.TabIndex = 57;
            this.label3.Text = "Major Components";
            // 
            // lstMajorComponents
            // 
            this.lstMajorComponents.FormattingEnabled = true;
            this.lstMajorComponents.HorizontalScrollbar = true;
            this.lstMajorComponents.ItemHeight = 16;
            this.lstMajorComponents.Location = new System.Drawing.Point(573, 200);
            this.lstMajorComponents.Name = "lstMajorComponents";
            this.lstMajorComponents.Size = new System.Drawing.Size(435, 308);
            this.lstMajorComponents.TabIndex = 56;
            this.lstMajorComponents.Click += new System.EventHandler(this.lstMajorComponents_Click);
            this.lstMajorComponents.SelectedIndexChanged += new System.EventHandler(this.lstMajorComponents_SelectedIndexChanged);
            this.lstMajorComponents.DoubleClick += new System.EventHandler(this.lstMajorComponents_DoubleClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1145, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 24);
            this.label4.TabIndex = 59;
            this.label4.Text = "Minor Components";
            // 
            // lstMinorComponents
            // 
            this.lstMinorComponents.FormattingEnabled = true;
            this.lstMinorComponents.HorizontalScrollbar = true;
            this.lstMinorComponents.ItemHeight = 16;
            this.lstMinorComponents.Location = new System.Drawing.Point(1039, 200);
            this.lstMinorComponents.Name = "lstMinorComponents";
            this.lstMinorComponents.Size = new System.Drawing.Size(437, 308);
            this.lstMinorComponents.TabIndex = 58;
            this.lstMinorComponents.Click += new System.EventHandler(this.lstMinorComponents_Click);
            this.lstMinorComponents.SelectedIndexChanged += new System.EventHandler(this.lstMinorComponents_SelectedIndexChanged);
            this.lstMinorComponents.DoubleClick += new System.EventHandler(this.lstMinorComponents_DoubleClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1129, 532);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(245, 24);
            this.label5.TabIndex = 63;
            this.label5.Text = "Minor Component Details";
            // 
            // lstMinorComponentDetails
            // 
            this.lstMinorComponentDetails.FormattingEnabled = true;
            this.lstMinorComponentDetails.HorizontalScrollbar = true;
            this.lstMinorComponentDetails.ItemHeight = 16;
            this.lstMinorComponentDetails.Location = new System.Drawing.Point(1052, 600);
            this.lstMinorComponentDetails.Name = "lstMinorComponentDetails";
            this.lstMinorComponentDetails.Size = new System.Drawing.Size(424, 100);
            this.lstMinorComponentDetails.TabIndex = 62;
            this.lstMinorComponentDetails.DoubleClick += new System.EventHandler(this.lstMinorComponentDetails_DoubleClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(650, 532);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 24);
            this.label6.TabIndex = 61;
            this.label6.Text = "Major Component Details";
            // 
            // lstMajorComponentDetails
            // 
            this.lstMajorComponentDetails.FormattingEnabled = true;
            this.lstMajorComponentDetails.HorizontalScrollbar = true;
            this.lstMajorComponentDetails.ItemHeight = 16;
            this.lstMajorComponentDetails.Location = new System.Drawing.Point(573, 599);
            this.lstMajorComponentDetails.Name = "lstMajorComponentDetails";
            this.lstMajorComponentDetails.Size = new System.Drawing.Size(435, 100);
            this.lstMajorComponentDetails.TabIndex = 60;
            this.lstMajorComponentDetails.DoubleClick += new System.EventHandler(this.lstMajorComponentDetails_DoubleClick);
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.AutoSize = true;
            this.btnAddCategory.Location = new System.Drawing.Point(346, 178);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(64, 17);
            this.btnAddCategory.TabIndex = 65;
            this.btnAddCategory.TabStop = true;
            this.btnAddCategory.Text = "Add New";
            this.btnAddCategory.Visible = false;
            this.btnAddCategory.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAddCategory_LinkClicked);
            // 
            // btnAddMajorComponent
            // 
            this.btnAddMajorComponent.AutoSize = true;
            this.btnAddMajorComponent.Location = new System.Drawing.Point(570, 178);
            this.btnAddMajorComponent.Name = "btnAddMajorComponent";
            this.btnAddMajorComponent.Size = new System.Drawing.Size(64, 17);
            this.btnAddMajorComponent.TabIndex = 66;
            this.btnAddMajorComponent.TabStop = true;
            this.btnAddMajorComponent.Text = "Add New";
            this.btnAddMajorComponent.Visible = false;
            this.btnAddMajorComponent.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAddMajorComponent_LinkClicked);
            // 
            // btnAddMinorComponent
            // 
            this.btnAddMinorComponent.AutoSize = true;
            this.btnAddMinorComponent.Location = new System.Drawing.Point(1036, 178);
            this.btnAddMinorComponent.Name = "btnAddMinorComponent";
            this.btnAddMinorComponent.Size = new System.Drawing.Size(64, 17);
            this.btnAddMinorComponent.TabIndex = 67;
            this.btnAddMinorComponent.TabStop = true;
            this.btnAddMinorComponent.Text = "Add New";
            this.btnAddMinorComponent.Visible = false;
            this.btnAddMinorComponent.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAddMinorComponent_LinkClicked);
            // 
            // btnAddMajorComponentDetail
            // 
            this.btnAddMajorComponentDetail.AutoSize = true;
            this.btnAddMajorComponentDetail.Location = new System.Drawing.Point(570, 573);
            this.btnAddMajorComponentDetail.Name = "btnAddMajorComponentDetail";
            this.btnAddMajorComponentDetail.Size = new System.Drawing.Size(64, 17);
            this.btnAddMajorComponentDetail.TabIndex = 68;
            this.btnAddMajorComponentDetail.TabStop = true;
            this.btnAddMajorComponentDetail.Text = "Add New";
            this.btnAddMajorComponentDetail.Visible = false;
            this.btnAddMajorComponentDetail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAddMajorComponentDetail_LinkClicked);
            // 
            // btnAddMinorComponentDetail
            // 
            this.btnAddMinorComponentDetail.AutoSize = true;
            this.btnAddMinorComponentDetail.Location = new System.Drawing.Point(1049, 573);
            this.btnAddMinorComponentDetail.Name = "btnAddMinorComponentDetail";
            this.btnAddMinorComponentDetail.Size = new System.Drawing.Size(64, 17);
            this.btnAddMinorComponentDetail.TabIndex = 69;
            this.btnAddMinorComponentDetail.TabStop = true;
            this.btnAddMinorComponentDetail.Text = "Add New";
            this.btnAddMinorComponentDetail.Visible = false;
            this.btnAddMinorComponentDetail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAddMinorComponentDetail_LinkClicked);
            // 
            // ComponentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1518, 763);
            this.Controls.Add(this.btnAddMinorComponentDetail);
            this.Controls.Add(this.btnAddMajorComponentDetail);
            this.Controls.Add(this.btnAddMinorComponent);
            this.Controls.Add(this.btnAddMajorComponent);
            this.Controls.Add(this.btnAddCategory);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lstMinorComponentDetails);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lstMajorComponentDetails);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lstMinorComponents);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lstMajorComponents);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstCategories);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstBooms);
            this.Controls.Add(this.label24);
            this.Name = "ComponentsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Quotation System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ComponentsForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ListBox lstBooms;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lstCategories;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstMajorComponents;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lstMinorComponents;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox lstMinorComponentDetails;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox lstMajorComponentDetails;
        private System.Windows.Forms.LinkLabel btnAddCategory;
        private System.Windows.Forms.LinkLabel btnAddMajorComponent;
        private System.Windows.Forms.LinkLabel btnAddMinorComponent;
        private System.Windows.Forms.LinkLabel btnAddMajorComponentDetail;
        private System.Windows.Forms.LinkLabel btnAddMinorComponentDetail;

    }
}