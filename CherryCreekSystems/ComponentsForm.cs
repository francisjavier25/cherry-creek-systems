﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain.BoomComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class ComponentsForm : Form
    {
        private readonly DataContext _context;

        private readonly BoomSystemService boomSystemService;

        #region selected components

        public BoomSystem SelectedBoomSystem
        {
            get
            {
                var selectedBoomSystem = lstBooms.SelectedItem as BoomSystem;
                return selectedBoomSystem;
            }
        }

        public Category SelectedCategory
        {
            get
            {
                var selectedCategory = lstCategories.SelectedItem as Category;
                return selectedCategory;
            }
        }

        public MajorComponent SelectedMajorComponent
        {
            get
            {
                var selectedMajorComponent = lstMajorComponents.SelectedItem as MajorComponent;
                return selectedMajorComponent;
            }
        }

        public MinorComponent SelectedMinorComponent
        {
            get
            {
                var selectedMinorComponent = lstMinorComponents.SelectedItem as MinorComponent;
                return selectedMinorComponent;
            }
        }

        public MajorComponentDetail SelectedMajorComponentDetail
        {
            get
            {
                var selectedMajorComponentDetail = lstMajorComponentDetails.SelectedItem as MajorComponentDetail;
                return selectedMajorComponentDetail;
            }
        }

        public MinorComponentDetail SelectedMinorComponentDetail
        {
            get
            {
                var selectedMinorComponentDetail = lstMinorComponentDetails.SelectedItem as MinorComponentDetail;
                return selectedMinorComponentDetail;
            }
        }

        #endregion selected components

        #region component counts

        public int CategoriesCount
        {
            get
            {
                var categoriesCount = lstCategories.Items.Count;
                return categoriesCount;
            }
        }

        public int MajorComponentsCount
        {
            get
            {
                var majorComponentsCount = lstMajorComponents.Items.Count;
                return majorComponentsCount;
            }
        }

        public int MinorComponentsCount
        {
            get
            {
                var minorComponentsCount = lstMinorComponents.Items.Count;
                return minorComponentsCount;
            }
        }

        public int MajorComponentsDetailsCount
        {
            get
            {
                var majorComponentsDetailsCount = lstMajorComponentDetails.Items.Count;
                return majorComponentsDetailsCount;
            }
        }

        public int MinorComponentsDetailsCount
        {
            get
            {
                var minorComponentsDetailsCount = lstMinorComponentDetails.Items.Count;
                return minorComponentsDetailsCount;
            }
        }

        #endregion component counts

        public ComponentsForm()
        {
            InitializeComponent();

            _context = new DataContext();

            boomSystemService = new BoomSystemService(_context);
        }

        public async Task Initialize()
        {
            var booms = await boomSystemService.GetAllLoaded();

            assignDisplayMember(lstBooms);
            lstBooms.DataSource = booms;
            lstBooms.SelectedIndex = -1;
        }

        #region cascade selecting components

        private void lstBooms_Click(object sender, EventArgs e)
        {
            if (SelectedBoomSystem != null)
            {
                var categories = SelectedBoomSystem.Categories
                                                   .IsActive()
                                                   .OrderBy(c => c.Order)
                                                   .ToList<INamedAndOrderedComponent>();

                bindComponents(lstCategories, categories);

                lstMajorComponents.DataSource = null;
                lstMajorComponentDetails.DataSource = null;
                lstMinorComponents.DataSource = null;
                lstMinorComponentDetails.DataSource = null;

                btnAddCategory.Visible = true;
                btnAddMajorComponent.Visible = false;
                btnAddMajorComponentDetail.Visible = false;
                btnAddMinorComponent.Visible = false;
                btnAddMinorComponentDetail.Visible = false;
            }
        }

        private void lstCategories_Click(object sender, EventArgs e)
        {
            categorySelected();
        }

        private void lstCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            categorySelected();
        }

        private void categorySelected()
        {
            if (SelectedCategory != null)
            {
                if (SelectedCategory.MajorComponents != null)
                {
                    var majorComponents = SelectedCategory.MajorComponents
                                                    .IsActive()
                                                    .OrderBy(m => m.Order)
                                                    .ToList<INamedAndOrderedComponent>();
                    bindComponents(lstMajorComponents, majorComponents);
                }
                else
                {
                    lstMajorComponents.DataSource = null;
                }

                btnAddMajorComponent.Visible = true;
                btnAddMajorComponentDetail.Visible = false;
                btnAddMinorComponent.Visible = false;
                btnAddMinorComponentDetail.Visible = false;

                lstMajorComponentDetails.DataSource = null;
                lstMinorComponents.DataSource = null;
                lstMinorComponentDetails.DataSource = null;
            }
            else
            {
                lstMajorComponents.DataSource = null;

                btnAddMajorComponent.Visible = false;
            }
        }

        private void lstMajorComponents_Click(object sender, EventArgs e)
        {
            majorComponentSelected();
        }

        private void lstMajorComponents_SelectedIndexChanged(object sender, EventArgs e)
        {
            majorComponentSelected();
        }

        private void majorComponentSelected()
        {
            if (SelectedMajorComponent != null)
            {
                if (SelectedMajorComponent.MinorComponents != null)
                {
                    var minorComponents = SelectedMajorComponent.MinorComponents
                                            .IsActive()
                                            .OrderBy(m => m.Order)
                                            .ToList<INamedAndOrderedComponent>();
                    bindComponents(lstMinorComponents, minorComponents);
                }
                else
                {
                    lstMinorComponents.DataSource = null;
                }

                if (SelectedMajorComponent.MajorComponentDetails != null)
                {
                    var majorComponentDetails = SelectedMajorComponent.MajorComponentDetails
                                                  .IsActive()
                                                  .OrderBy(m => m.Order)
                                                  .ToList<INamedAndOrderedComponent>();
                    bindComponents(lstMajorComponentDetails, majorComponentDetails);
                }
                else
                {
                    lstMajorComponentDetails.DataSource = null;
                }

                lstMinorComponentDetails.DataSource = null;

                btnAddMajorComponentDetail.Visible = true;
                btnAddMinorComponent.Visible = true;
                btnAddMinorComponentDetail.Visible = false;
            }
            else
            {
                lstMinorComponents.DataSource = null;
                lstMajorComponentDetails.DataSource = null;

                btnAddMajorComponentDetail.Visible = false;
                btnAddMinorComponent.Visible = false;
            }
        }

        private void lstMinorComponents_Click(object sender, EventArgs e)
        {
            minorComponentSelected();
        }

        private void lstMinorComponents_SelectedIndexChanged(object sender, EventArgs e)
        {
            minorComponentSelected();
        }

        private void minorComponentSelected()
        {
            if (SelectedMinorComponent != null)
            {
                if (SelectedMinorComponent.MinorComponentDetails != null)
                {
                    var minorComponentDetails = SelectedMinorComponent.MinorComponentDetails
                                                      .IsActive()
                                                      .OrderBy(m => m.Order)
                                                      .ToList<INamedAndOrderedComponent>();
                    bindComponents(lstMinorComponentDetails, minorComponentDetails);
                }
                else
                {
                    lstMinorComponentDetails.DataSource = null;
                }

                btnAddMinorComponentDetail.Visible = true;
            }
            else
            {
                lstMinorComponentDetails.DataSource = null;

                btnAddMinorComponentDetail.Visible = false;
            }
        }

        private void bindComponents(ListBox listBox, List<INamedAndOrderedComponent> components)
        {
            assignDisplayMember(listBox);
            listBox.DataSource = components;
            listBox.SelectedIndex = -1;
        }

        private void assignDisplayMember(ListBox listBox)
        {
            listBox.DisplayMember = "Display";
        }

        #endregion cascade selecting components

        #region add components

        private void lstBooms_DoubleClick(object sender, EventArgs e)
        {
            if (SelectedBoomSystem != null)
            {
                BoomSystemForm boomSystemForm = new BoomSystemForm(_context, SelectedBoomSystem);
                boomSystemForm.OnSaveFinished += refreshBoomSystems;
                boomSystemForm.ShowDialog();
            }
        }

        private void btnAddCategory_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CategoryForm categoryForm = new CategoryForm(_context, null, SelectedBoomSystem.Id, CategoriesCount);
            categoryForm.OnSaveFinished += refreshCategories;
            categoryForm.ShowDialog();
        }

        private void lstCategories_DoubleClick(object sender, EventArgs e)
        {
            if (SelectedCategory != null)
            {
                CategoryForm categoryForm = new CategoryForm(_context, SelectedCategory, SelectedBoomSystem.Id, CategoriesCount);
                categoryForm.OnSaveFinished += refreshCategories;
                categoryForm.ShowDialog();
            }
        }

        private void btnAddMajorComponent_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MajorComponentForm majorComponentForm = new MajorComponentForm(_context, null, SelectedCategory, SelectedBoomSystem.BoomTypeId, MajorComponentsCount);
            majorComponentForm.OnSaveFinished += refreshMajorComponents;
            majorComponentForm.ShowDialog();
        }

        private void lstMajorComponents_DoubleClick(object sender, EventArgs e)
        {
            if (SelectedMajorComponent != null)
            {
                MajorComponentForm majorComponentForm = new MajorComponentForm(_context, SelectedMajorComponent, SelectedCategory, SelectedBoomSystem.BoomTypeId, MajorComponentsCount);
                majorComponentForm.OnSaveFinished += refreshMajorComponents;
                majorComponentForm.ShowDialog();
            }
        }

        private void btnAddMinorComponent_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MinorComponentForm minorComponentForm = new MinorComponentForm(_context, null, SelectedMajorComponent, SelectedBoomSystem.BoomTypeId, MinorComponentsCount);
            minorComponentForm.OnSaveFinished += refreshMinorComponents;
            minorComponentForm.ShowDialog();
        }

        private void lstMinorComponents_DoubleClick(object sender, EventArgs e)
        {
            if (SelectedMinorComponent != null)
            {
                MinorComponentForm minorComponentForm = new MinorComponentForm(_context, SelectedMinorComponent, SelectedMajorComponent, SelectedBoomSystem.BoomTypeId, MinorComponentsCount);
                minorComponentForm.OnSaveFinished += refreshMinorComponents;
                minorComponentForm.ShowDialog();
            }
        }

        private void btnAddMajorComponentDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MajorComponentDetailForm majorComponentDetailForm = new MajorComponentDetailForm(_context, null, SelectedMajorComponent, MajorComponentsDetailsCount);
            majorComponentDetailForm.OnSaveFinished += refreshMajorComponentDetails;
            majorComponentDetailForm.ShowDialog();
        }

        private void lstMajorComponentDetails_DoubleClick(object sender, EventArgs e)
        {
            if (SelectedMajorComponentDetail != null)
            {
                MajorComponentDetailForm majorComponentDetailForm = new MajorComponentDetailForm(_context, SelectedMajorComponentDetail, SelectedMajorComponent, MajorComponentsDetailsCount);
                majorComponentDetailForm.OnSaveFinished += refreshMajorComponentDetails;
                majorComponentDetailForm.ShowDialog();
            }
        }

        private void btnAddMinorComponentDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MinorComponentDetailForm minorComponentDetailForm = new MinorComponentDetailForm(_context, null, SelectedMinorComponent, MinorComponentsDetailsCount);
            minorComponentDetailForm.OnSaveFinished += refreshMinorComponentDetails;
            minorComponentDetailForm.ShowDialog();
        }

        private void lstMinorComponentDetails_DoubleClick(object sender, EventArgs e)
        {
            if (SelectedMinorComponentDetail != null)
            {
                MinorComponentDetailForm minorComponentDetailForm = new MinorComponentDetailForm(_context, SelectedMinorComponentDetail, SelectedMinorComponent, MinorComponentsDetailsCount);
                minorComponentDetailForm.OnSaveFinished += refreshMinorComponentDetails;
                minorComponentDetailForm.ShowDialog();
            }
        }

        #endregion add components

        #region refresh components

        private async void refreshBoomSystems()
        {
            var booms = await boomSystemService.GetAllLoaded();

            var selectedIndex = lstCategories.SelectedIndex;
            lstBooms.DataSource = booms;
            lstBooms.SelectedIndex = selectedIndex;
        }

        private void refreshCategories(bool isDelete)
        {
            var categories = SelectedBoomSystem.Categories.IsActive().OrderAsc();
            //categoryService.GetWhere(c => c.SystemId == SelectedBoomSystem.Id)
            //                                .OrderBy(c => c.Order)
            //                                .ToList();

            var selectedIndex = lstCategories.SelectedIndex;
            assignDisplayMember(lstCategories);
            lstCategories.DataSource = categories;
            lstCategories.SelectedIndex = !isDelete ? selectedIndex : -1;
            if (lstCategories.Items.Count == 0)
            {
                categorySelected();
            }
        }

        private void refreshMajorComponents(bool isDelete)
        {
            var majorComponents = SelectedCategory.MajorComponents.IsActive().OrderAsc();
            //majorComponentService.GetWhere(c => c.CategoryId == SelectedCategory.Id)
            //                                .OrderBy(c => c.Order)
            //                                .ToList();

            var selectedIndex = lstMajorComponents.SelectedIndex;
            assignDisplayMember(lstMajorComponents);
            lstMajorComponents.DataSource = majorComponents;
            lstMajorComponents.SelectedIndex = !isDelete ? selectedIndex : -1;
            if (lstMajorComponents.Items.Count == 0)
            {
                majorComponentSelected();
            }
        }

        private void refreshMinorComponents(bool isDelete)
        {
            var minorComponents = SelectedMajorComponent.MinorComponents.IsActive().OrderAsc();
            //minorComponentService.GetWhere(c => c.MajorComponentId == SelectedMajorComponent.Id)
            //                                .OrderBy(c => c.Order)
            //                                .ToList();

            var selectedIndex = lstMinorComponents.SelectedIndex;
            assignDisplayMember(lstMinorComponents);
            lstMinorComponents.DataSource = minorComponents;
            lstMinorComponents.SelectedIndex = !isDelete ? selectedIndex : -1;
            if (lstMinorComponents.Items.Count == 0)
            {
                minorComponentSelected();
            }
        }

        private void refreshMajorComponentDetails(bool isDelete)
        {
            var majorComponentDetails = SelectedMajorComponent.MajorComponentDetails.IsActive().OrderAsc();
            //majorComponentDetailService.GetWhere(c => c.MajorComponentId == SelectedMajorComponent.Id)
            //                                .OrderBy(c => c.Order)
            //                                .ToList();

            var selectedIndex = lstMajorComponentDetails.SelectedIndex;
            assignDisplayMember(lstMajorComponentDetails);
            lstMajorComponentDetails.DataSource = majorComponentDetails;
            lstMajorComponentDetails.SelectedIndex = !isDelete ? selectedIndex : -1;
        }

        private void refreshMinorComponentDetails(bool isDelete)
        {
            var minorComponentDetails = SelectedMinorComponent.MinorComponentDetails.IsActive().OrderAsc();
            //minorComponentDetailService.GetWhere(c => c.MinorComponentId == SelectedMinorComponent.Id)
            //                            .OrderBy(c => c.Order)
            //                            .ToList();

            var selectedIndex = lstMinorComponentDetails.SelectedIndex;
            assignDisplayMember(lstMinorComponentDetails);
            lstMinorComponentDetails.DataSource = minorComponentDetails;
            lstMinorComponentDetails.SelectedIndex = !isDelete ? selectedIndex : -1;
        }

        #endregion refresh components

        private void ComponentsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _context.Dispose();
        }
    }
}