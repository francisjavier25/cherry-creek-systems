﻿namespace CherryCreekSystems
{
    partial class SearchQuoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label100 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlCustomers = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ddlSalesReps = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gvQuotes = new System.Windows.Forms.DataGridView();
            this.QuoteNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Salesrep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCreated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuoteStatus = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TotalCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Copy = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Delete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.btnClose = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.ddlQuoteStatus = new System.Windows.Forms.ComboBox();
            this.dtStart = new System.Windows.Forms.DateTimePicker();
            this.dtEnd = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtQuoteNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ddlBoomTypes = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.gvQuotes)).BeginInit();
            this.SuspendLayout();
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(443, 80);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(79, 13);
            this.label100.TabIndex = 1;
            this.label100.Text = "Quote Number:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(468, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Customer:";
            // 
            // ddlCustomers
            // 
            this.ddlCustomers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCustomers.FormattingEnabled = true;
            this.ddlCustomers.Location = new System.Drawing.Point(543, 115);
            this.ddlCustomers.Name = "ddlCustomers";
            this.ddlCustomers.Size = new System.Drawing.Size(250, 21);
            this.ddlCustomers.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(463, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Sales Rep:";
            // 
            // ddlSalesReps
            // 
            this.ddlSalesReps.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSalesReps.FormattingEnabled = true;
            this.ddlSalesReps.Location = new System.Drawing.Point(543, 154);
            this.ddlSalesReps.Name = "ddlSalesReps";
            this.ddlSalesReps.Size = new System.Drawing.Size(250, 21);
            this.ddlSalesReps.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(602, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Search Quotes";
            // 
            // gvQuotes
            // 
            this.gvQuotes.AllowUserToAddRows = false;
            this.gvQuotes.AllowUserToDeleteRows = false;
            this.gvQuotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvQuotes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.QuoteNumber,
            this.Customer,
            this.Salesrep,
            this.DateCreated,
            this.QuoteStatus,
            this.TotalCost,
            this.Copy,
            this.Delete});
            this.gvQuotes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.gvQuotes.Location = new System.Drawing.Point(31, 418);
            this.gvQuotes.Name = "gvQuotes";
            this.gvQuotes.Size = new System.Drawing.Size(1273, 208);
            this.gvQuotes.TabIndex = 14;
            this.gvQuotes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvQuotes_CellContentClick);
            this.gvQuotes.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvQuotes_DoubleCellClick);
            this.gvQuotes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvQuotes_CellValueChanged);
            this.gvQuotes.CurrentCellDirtyStateChanged += new System.EventHandler(this.gvQuotes_CurrentCellDirtyStateChanged);
            this.gvQuotes.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.gvQuotes_EditingControlShowing);
            // 
            // QuoteNumber
            // 
            this.QuoteNumber.DataPropertyName = "QuoteNumber";
            this.QuoteNumber.HeaderText = "Quote Number";
            this.QuoteNumber.Name = "QuoteNumber";
            this.QuoteNumber.ReadOnly = true;
            this.QuoteNumber.Width = 300;
            // 
            // Customer
            // 
            this.Customer.DataPropertyName = "CustomerName";
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            this.Customer.Width = 200;
            // 
            // Salesrep
            // 
            this.Salesrep.DataPropertyName = "SalesRepName";
            this.Salesrep.HeaderText = "Sales Rep";
            this.Salesrep.Name = "Salesrep";
            this.Salesrep.ReadOnly = true;
            this.Salesrep.Width = 200;
            // 
            // DateCreated
            // 
            this.DateCreated.DataPropertyName = "DateCreated";
            this.DateCreated.HeaderText = "Date";
            this.DateCreated.Name = "DateCreated";
            this.DateCreated.ReadOnly = true;
            this.DateCreated.Width = 130;
            // 
            // QuoteStatus
            // 
            this.QuoteStatus.AutoComplete = false;
            this.QuoteStatus.DataPropertyName = "QuoteStatus";
            this.QuoteStatus.HeaderText = "Status";
            this.QuoteStatus.Name = "QuoteStatus";
            this.QuoteStatus.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.QuoteStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.QuoteStatus.Width = 130;
            // 
            // TotalCost
            // 
            this.TotalCost.DataPropertyName = "TotalCost";
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = "0";
            this.TotalCost.DefaultCellStyle = dataGridViewCellStyle1;
            this.TotalCost.HeaderText = "TotalCost";
            this.TotalCost.Name = "TotalCost";
            this.TotalCost.ReadOnly = true;
            this.TotalCost.Width = 130;
            // 
            // Copy
            // 
            this.Copy.HeaderText = "";
            this.Copy.Name = "Copy";
            this.Copy.Text = "Copy";
            this.Copy.UseColumnTextForLinkValue = true;
            this.Copy.Width = 65;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "";
            this.Delete.Name = "Delete";
            this.Delete.Text = "Delete";
            this.Delete.UseColumnTextForLinkValue = true;
            this.Delete.Width = 75;
            // 
            // btnClose
            // 
            this.btnClose.CausesValidation = false;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(592, 671);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(150, 42);
            this.btnClose.TabIndex = 69;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(482, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Status:";
            // 
            // ddlQuoteStatus
            // 
            this.ddlQuoteStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlQuoteStatus.FormattingEnabled = true;
            this.ddlQuoteStatus.Location = new System.Drawing.Point(543, 193);
            this.ddlQuoteStatus.Name = "ddlQuoteStatus";
            this.ddlQuoteStatus.Size = new System.Drawing.Size(250, 21);
            this.ddlQuoteStatus.TabIndex = 8;
            // 
            // dtStart
            // 
            this.dtStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtStart.Location = new System.Drawing.Point(543, 286);
            this.dtStart.Name = "dtStart";
            this.dtStart.Size = new System.Drawing.Size(84, 20);
            this.dtStart.TabIndex = 10;
            this.dtStart.Value = new System.DateTime(2014, 12, 19, 20, 20, 38, 123);
            // 
            // dtEnd
            // 
            this.dtEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEnd.Location = new System.Drawing.Point(709, 286);
            this.dtEnd.Name = "dtEnd";
            this.dtEnd.Size = new System.Drawing.Size(84, 20);
            this.dtEnd.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(654, 290);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "to";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(489, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Date:";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(592, 343);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(150, 42);
            this.btnSearch.TabIndex = 13;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.PerformSearch);
            // 
            // txtQuoteNumber
            // 
            this.txtQuoteNumber.Location = new System.Drawing.Point(542, 77);
            this.txtQuoteNumber.Margin = new System.Windows.Forms.Padding(2);
            this.txtQuoteNumber.Name = "txtQuoteNumber";
            this.txtQuoteNumber.Size = new System.Drawing.Size(251, 20);
            this.txtQuoteNumber.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(458, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 70;
            this.label7.Text = "Boom Type:";
            // 
            // ddlBoomTypes
            // 
            this.ddlBoomTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlBoomTypes.FormattingEnabled = true;
            this.ddlBoomTypes.Location = new System.Drawing.Point(543, 232);
            this.ddlBoomTypes.Name = "ddlBoomTypes";
            this.ddlBoomTypes.Size = new System.Drawing.Size(250, 21);
            this.ddlBoomTypes.TabIndex = 71;
            // 
            // SearchQuoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1334, 752);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ddlBoomTypes);
            this.Controls.Add(this.txtQuoteNumber);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtEnd);
            this.Controls.Add(this.dtStart);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ddlQuoteStatus);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gvQuotes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ddlSalesReps);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddlCustomers);
            this.Name = "SearchQuoteForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Search Quotes";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SearchQuoteForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gvQuotes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlCustomers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ddlSalesReps;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gvQuotes;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox ddlQuoteStatus;
        private System.Windows.Forms.DateTimePicker dtStart;
        private System.Windows.Forms.DateTimePicker dtEnd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtQuoteNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuoteNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Salesrep;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCreated;
        private System.Windows.Forms.DataGridViewComboBoxColumn QuoteStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalCost;
        private System.Windows.Forms.DataGridViewLinkColumn Copy;
        private System.Windows.Forms.DataGridViewLinkColumn Delete;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ddlBoomTypes;
    }
}