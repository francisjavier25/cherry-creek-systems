﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.BL.Services;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class CustomersForm : Form
    {
        public delegate void SaveFinished();

        public SaveFinished OnSaveFinished;

        private readonly DataContext _context;

        private readonly CustomerService customerService;

        private Customer _currentCustomer;

        public Customer CurrentCustomer
        {
            get { return _currentCustomer; }
            set
            {
                _currentCustomer = value;
                if (_currentCustomer != null)
                {
                    txtCompanyName.Text = _currentCustomer.CompanyName;
                    txtContactName.Text = _currentCustomer.ContactName;
                    txtPhone.Text = _currentCustomer.Phone;
                    txtEmail.Text = _currentCustomer.Email;
                    txtFax.Text = _currentCustomer.Fax;

                    btnUpdate.Enabled = true;
                    btnManageShippingAddresses.Enabled = true;
                    //btnAdd.Visible = false;
                }
                else
                {
                    ControlUtilities.ClearInputValuesAndErrors(this);

                    btnUpdate.Enabled = false;
                    btnManageShippingAddresses.Enabled = false;
                    //btnAdd.Visible = true;
                }
            }
        }

        public CustomersForm()
        {
            InitializeComponent();

            _context = new DataContext();

            customerService = new CustomerService(_context);
        }

        public async Task Initialize()
        {
            var customerList = await customerService.Get().OrderedByCompanyName().ToListAsync();

            ddlCustomers.DataSource = customerList;
            ddlCustomers.DisplayMember = "CompanyName";
            ddlCustomers.SelectedIndex = -1;
        }

        //private async void btnAdd_Click(object sender, EventArgs e)
        //{
        //    Customer customer = new Customer();
        //    bool isValidCustomer = ValidateAndPopulateCustomer(customer);
        //    if (isValidCustomer)
        //    {
        //        var userCreator = new UserCreator(_context);
        //        var user = await userCreator.CreateCustomerUser(customer);

        //        if (user != null)
        //        {
        //            await Initialize();
        //            ddlCustomers.SelectedItem = customer;
        //            OnSaveFinished();
        //            MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
        //        }
        //        else
        //        {
        //            MessageBox.Show("There was an error creating the user", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
        //        }
        //    }
        //}

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidCustomer = ValidateAndPopulateCustomer(CurrentCustomer);
            if (isValidCustomer)
            {
                await _context.SaveChangesAsync();

                var currentCustomerIndex = ddlCustomers.SelectedIndex;
                await Initialize();
                ddlCustomers.SelectedIndex = currentCustomerIndex;
                OnSaveFinished();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
            }
        }

        private bool ValidateAndPopulateCustomer(Customer customer)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(customer);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(Customer customer)
        {
            customer.CompanyName = txtCompanyName.Text.Trim();
            customer.ContactName = txtContactName.Text.Trim();
            customer.Phone = txtPhone.Text.Trim();
            customer.Email = txtEmail.Text.Trim();
            customer.Fax = txtFax.Text.Trim();
        }

        //private void mnNewCustomer_Click(object sender, EventArgs e)
        //{
        //    CurrentCustomer = null;
        //}

        private void ddlCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedCustomer = ddlCustomers.SelectedValue as Customer;
            CurrentCustomer = selectedCustomer;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtCompanyName_Validating(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        private async void btnManageShippingAddresses_Click(object sender, EventArgs e)
        {
            ShipAddressesForm shipAddressesForm = new ShipAddressesForm(CurrentCustomer.Id);
            await shipAddressesForm.Initialize();
            shipAddressesForm.OnSaveFinished += SendSaveFinishedNotification;
            shipAddressesForm.ShowDialog();
        }

        private void SendSaveFinishedNotification()
        {
            OnSaveFinished();
        }

        private void CustomersForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _context.Dispose();
        }
    }
}