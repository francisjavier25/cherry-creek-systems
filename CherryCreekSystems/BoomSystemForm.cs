﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.Utils;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Utilities;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class BoomSystemForm : Form
    {
        #region public events

        public delegate void SaveFinished();

        public SaveFinished OnSaveFinished;

        #endregion public events

        #region private variables

        private QuoteRequest quoteRequest;
        private Boom boom;

        private readonly DataContext _context;

        //private readonly BoomSystemService BoomSystemService;

        private readonly int BoomTypeId;

        #endregion private variables

        private BoomSystem _currentBoomSystem;

        public BoomSystem CurrentBoomSystem
        {
            get { return _currentBoomSystem; }
            set
            {
                _currentBoomSystem = value;
                if (_currentBoomSystem != null)
                {
                    lblDetailId.Text = _currentBoomSystem.DetailCalculationID;
                    lblPriceID.Text = _currentBoomSystem.PriceCalculationID;
                    lblDescriptionId.Text = _currentBoomSystem.DescriptionCalculationID;
                    txtBoomSystemName.Text = _currentBoomSystem.Name;
                    txtDetailCalculation.Text = _currentBoomSystem.DetailCalculation;
                    txtCostCalculation.Text = _currentBoomSystem.PriceCalculation;
                    txtDescriptionCalculation.Text = _currentBoomSystem.DescriptionCalculation;
                }
            }
        }

        public Property SelectedBoomSpecField
        {
            get
            {
                var selectedBoomSpecField = lstBoomSpecs.SelectedItem as Property;
                return selectedBoomSpecField;
            }
        }

        public BoomSystemForm(DataContext context, BoomSystem BoomSystem)
        {
            InitializeComponent();

            _context = context;
            BoomTypeId = BoomSystem.BoomTypeId;
            CurrentBoomSystem = BoomSystem;

            //BoomSystemService = new BoomSystemService(UOW);

            bindPropertiesLists(BoomTypeId);
        }

        private void bindPropertiesLists(int boomTypeId)
        {
            quoteRequest = new QuoteRequest();

            boom = BoomTypeFactory.CreateBoomType((BoomType)boomTypeId);

            var boomType = boom.GetType();
            var boomSpecsProperties = ReflectionUtilities.GetBoomProperties(boomType);

            lstBoomSpecs.DataSource = boomSpecsProperties;
            lstBoomSpecs.DisplayMember = "Name";
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            bool isValidBoomSystem = ValidateAndPopulateBoomSystem(CurrentBoomSystem);
            if (isValidBoomSystem)
            {
                await _context.SaveChangesAsync();
                OnSaveFinished();
                MessageBox.Show("Data has been saved", "Cherry Creek Systems Quotation", MessageBoxButtons.OK);
                Close();
            }
        }

        private bool ValidateAndPopulateBoomSystem(BoomSystem BoomSystem)
        {
            var formIsValid = ValidateChildren();
            if (formIsValid)
            {
                populateMainValuesForSaving(BoomSystem);

                return true;
            }

            return false;
        }

        private void populateMainValuesForSaving(BoomSystem BoomSystem)
        {
            BoomSystem.Name = txtBoomSystemName.Text.Trim();
            BoomSystem.DetailCalculation = txtDetailCalculation.Text;
            BoomSystem.PriceCalculation = txtCostCalculation.Text;
            BoomSystem.DescriptionCalculation = txtDescriptionCalculation.Text;
        }

        #region drag and drop events

        private void lstBoomSpecs_MouseDown(object sender, MouseEventArgs e)
        {
            if (SelectedBoomSpecField != null)
            {
                DoDragDrop(SelectedBoomSpecField, DragDropEffects.Copy);
            }
        }

        private void txtDragEnter(object sender, DragEventArgs e)
        {
            if (e.AllowedEffect == DragDropEffects.Copy)
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void txtDragDrop(object sender, DragEventArgs e)
        {
            var textbox = sender as TextBox;
            var selectedField = e.Data.GetData(typeof(Property)) as Property;
            var targetProperty = selectedField.Name.Replace(" ", "");

            var insertIndex = GetCaretIndexFromPoint(textbox, e.X, e.Y);
            textbox.Text = textbox.Text.Insert(insertIndex, targetProperty);
            textbox.SelectionStart = insertIndex;
        }

        #endregion drag and drop events

        #region validation

        private void ValidateTextboxHasValue(object sender, CancelEventArgs e)
        {
            ValidationUtilities.ValidateTextboxHasValue(sender, emptyTextErrorProvider, e);
        }

        #endregion validation

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtDragOver(object sender, DragEventArgs e)
        {
            var textBox = sender as TextBox;
            // fake moving the text caret
            textBox.SelectionStart = GetCaretIndexFromPoint(textBox, e.X, e.Y);
            textBox.SelectionLength = 0;
            // don't forget to set focus to the text box to make the caret visible!
            textBox.Focus();
        }

        //http://stackoverflow.com/questions/913735/how-to-move-insert-caret-on-textbox-when-accepting-a-drop
        /// <remarks>
        /// GetCharIndexFromPosition is missing one caret position, as there is one extra caret
        /// position than there are characters (an extra one at the end).
        /// </remarks>
        private int GetCaretIndexFromPoint(TextBox box, int x, int y)
        {
            Point realPoint = box.PointToClient(new Point(x, y));
            int index = box.GetCharIndexFromPosition(realPoint);
            if (index == box.Text.Length - 1)
            {
                Point caretPoint = box.GetPositionFromCharIndex(index);
                if (realPoint.X > caretPoint.X)
                {
                    index += 1;
                }
            }
            return index;
        }
    }
}