﻿using CherryCreekSystems.BL;
using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems
{
    public partial class BaseCostsForm : Form
    {
        private readonly DataContext _context;

        private readonly BaseCostService BaseCostService;
        private readonly BoomSystemService BoomSystemService;

        public BindingList<BaseCost> BaseCosts
        {
            get
            {
                var baseCosts = gvBaseCosts.DataSource as BindingList<BaseCost>;
                if (baseCosts != null)
                {
                    return baseCosts;
                }

                return null;
            }
        }

        public BaseCostsForm()
        {
            InitializeComponent();

            _context = new DataContext();

            BaseCostService = new BaseCostService(_context);
            BoomSystemService = new BoomSystemService(_context);

            gvBaseCosts.DataSource = null;
        }

        public async Task Initialize()
        {
            var boomSystems = await BoomSystemService.Get().ToListAsync();
            ddlBoomSystems.DataSource = boomSystems;
            ddlBoomSystems.DisplayMember = "Name";
            ddlBoomSystems.SelectedIndex = -1;
        }

        private void ddlBoomSystems_SelectionChangeCommitted(object sender, EventArgs e)
        {
            refreshBaseCosts();
        }

        private async void refreshBaseCosts()
        {
            var boomSystem = ddlBoomSystems.SelectedItem as BoomSystem;
            var baseCosts = await BaseCostService.Get().ByBoomSystemId(boomSystem.Id).ToListAsync();

            gvBaseCosts.AutoGenerateColumns = false;
            gvBaseCosts.DataSource = new BindingList<BaseCost>(baseCosts);
            gvBaseCosts.Enabled = true;
        }

        private void gvBaseCosts_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            var output = 0.0;

            if (!double.TryParse(e.FormattedValue.ToString(), out output))
            {
                gvBaseCosts.Rows[e.RowIndex].ErrorText =
                    "Please enter a valid number";
                e.Cancel = true;
            }
        }

        private async void gvBaseCosts_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            gvBaseCosts.Rows[e.RowIndex].ErrorText = String.Empty;
            var modifiedBaseCost = BaseCosts[e.RowIndex];
            modifiedBaseCost.XValue = Math.Round(modifiedBaseCost.XValue, 2);
            modifiedBaseCost.YValue = Math.Round(modifiedBaseCost.YValue, 2);
            await _context.SaveChangesAsync();
        }

        private async void gvBaseCosts_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            if (BaseCosts != null)
            {
                var lastIndex = BaseCosts.Count - 1;
                var addedBaseCost = BaseCosts[lastIndex];
                var boomSystem = ddlBoomSystems.SelectedItem as BoomSystem;

                addedBaseCost.BoomSystemId = boomSystem.Id;
                BaseCostService.Add(addedBaseCost);
                await _context.SaveChangesAsync();
            }
        }

        private async void gvBaseCosts_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (BaseCosts != null)
            {
                var deletedBaseCost = BaseCosts[e.Row.Index];
                BaseCostService.Remove(deletedBaseCost);
                await _context.SaveChangesAsync();
            }
        }

        private void BaseCostsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _context.Dispose();
        }
    }
}