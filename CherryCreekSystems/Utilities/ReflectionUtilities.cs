﻿using CherryCreekSystems.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CherryCreekSystems.Utilities
{
    public static class ReflectionUtilities
    {
        private static List<Property> GetProperties<T>()
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            var properties = typeof(T).GetProperties()
                                      .Where(p => p.IsDefined(typeof(CalculationEnabled), true))
                                      .Select(p => new Property
                                      {
                                          Name = r.Replace(p.Name, " ")
                                      })
                                      .OrderBy(p => p.Name)
                                      .ToList();

            return properties;
        }

        public static List<Property> GetBoomProperties(Type boomType)
        {
            var boomSpecsProperties = new List<Property>();

            if (boomType == typeof(Echo))
                boomSpecsProperties = GetProperties<Echo>();
            else if (boomType == typeof(SingleRail))
                boomSpecsProperties = GetProperties<SingleRail>();
            else if (boomType == typeof(DoubleRail))
                boomSpecsProperties = GetProperties<DoubleRail>();
            else if (boomType == typeof(CenterWaterFeedDoubleRail))
                boomSpecsProperties = GetProperties<CenterWaterFeedDoubleRail>();
            else if (boomType == typeof(Tower))
                boomSpecsProperties = GetProperties<Tower>();
            else if (boomType == typeof(Navigator))
                boomSpecsProperties = GetProperties<Navigator>();
            else if (boomType == typeof(GroundRunner))
                boomSpecsProperties = GetProperties<GroundRunner>();
            else if (boomType == typeof(SkyRail))
                boomSpecsProperties = GetProperties<SkyRail>();

            return boomSpecsProperties;
        }
    }
}