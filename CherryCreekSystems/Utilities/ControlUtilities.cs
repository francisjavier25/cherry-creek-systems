﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace CherryCreekSystems.Utilities
{
    public static class ControlUtilities
    {
        //Functions for clearing specific types of controls

        private static readonly Action<Control> clearTextValues = c => c.Text = string.Empty;
        private static readonly Action<ComboBox> clearDropdownValues = d => d.SelectedIndex = -1;
        private static readonly Action<RadioButton> clearRadioButtons = r => r.Checked = false;
        private static readonly Action<ListBox> clearListboxValues = l => l.Items.Clear();
        private static readonly Action<Control, ErrorProvider> clearErrors = (c, e) => e.SetError(c, "");

        public static void ClearInputValuesAndErrors(Form container)
        {
            var controls = GetAll(container);
            var errorControls = GetErrorProviders(container);
            ClearValues(controls);
            ClearErrors(controls, errorControls);
        }

        /// <summary>
        /// Find the one selected radio button, by looking at all radio buttons under the parent Panel
        /// </summary>
        /// <param name="panel"></param>
        /// <returns></returns>
        public static string GetSelectedRadioButtonValueInPanel(Panel panel)
        {
            var selectedRadioValue = string.Empty;
            foreach (Control control in panel.Controls)
            {
                var radio = control as RadioButton;
                if (radio != null && radio.Checked)
                {
                    selectedRadioValue = radio.Text;
                }
            }

            return selectedRadioValue;
        }

        private static List<ErrorProvider> GetErrorProviders(Form container)
        {
            return (from field in container.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    where typeof(Component).IsAssignableFrom(field.FieldType)
                    let component = (Component)field.GetValue(container)
                    where component is ErrorProvider
                    select component)
                    .Cast<ErrorProvider>()
                    .ToList();
        }

        private static List<Control> GetAll(Control control)
        {
            var childControls = control.Controls.Cast<Control>();

            return childControls.SelectMany(GetAll).Concat(childControls).ToList();
        }

        /// <summary>
        /// Sets all of the radiobuttons that have a value of NO as checked
        /// </summary>
        /// <param name="control"></param>
        public static void CheckAllNO(Form container)
        {
            var controls = GetAll(container);

            var radioButtons = controls.OfType<RadioButton>().ToList();

            foreach (var rb in radioButtons)
            {
                if (rb.Text.ToLower() == "no")
                {
                    rb.Checked = true;
                }
            }
        }

        private static void ClearValues(List<Control> controls)
        {
            var textBoxes = controls.OfType<TextBox>().ToList();
            var labels = controls.OfType<Label>().Where(c => c.Name.ToLower().Contains("lbl")).ToList();
            var numberTextBoxes = controls.OfType<RadMaskedEditBox>().ToList();
            var dropDowns = controls.OfType<ComboBox>().ToList();
            var radioButtons = controls.OfType<RadioButton>().ToList();
            var listBoxes = controls.OfType<ListBox>().ToList();

            textBoxes.ForEach(clearTextValues);
            labels.ForEach(clearTextValues);
            numberTextBoxes.ForEach(clearTextValues);
            dropDowns.ForEach(clearDropdownValues);
            radioButtons.ForEach(clearRadioButtons);
            listBoxes.ForEach(clearListboxValues);
        }

        private static void ClearErrors(List<Control> controls, List<ErrorProvider> errorControls)
        {
            foreach (var c in controls)
            {
                foreach (var e in errorControls)
                {
                    clearErrors(c, e);
                }
            }
        }
    }
}