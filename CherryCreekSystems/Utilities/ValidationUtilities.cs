﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace CherryCreekSystems.Utilities
{
    public static class ValidationUtilities
    {
        public static void ValidateTextboxHasValue(object sender, ErrorProvider emptyTextErrorProvider, CancelEventArgs e)
        {
            var textbox = sender as TextBox;
            var value = textbox.Text;
            if (string.IsNullOrWhiteSpace(value))
            {
                emptyTextErrorProvider.SetError(textbox, "Please enter a value");
                e.Cancel = true;
            }
            else
            {
                emptyTextErrorProvider.SetError(textbox, "");
            }
        }

        public static void ValidateRadioOptionSelected(object sender, ErrorProvider radioButtonsErrorProvider, CancelEventArgs e)
        {
            var radioIsChecked = false;

            var radioButton = sender as RadioButton;
            var panel = radioButton.Parent as Panel;
            foreach (Control control in panel.Controls)
            {
                var radio = control as RadioButton;
                if (radio != null && radio.Checked)
                {
                    radioIsChecked = true;
                    break;
                }
            }

            if (!radioIsChecked)
            {
                radioButtonsErrorProvider.SetError(panel, "Please select an option");
                if (e != null)
                    e.Cancel = true;
            }
            else
            {
                radioButtonsErrorProvider.SetError(panel, "");
            }
        }

        public static void ValidateDropdownSelectedValue(object sender, ErrorProvider dropdownValueErrorProvider, CancelEventArgs e)
        {
            var dropdown = sender as ComboBox;
            var value = dropdown.Text;
            if (string.IsNullOrWhiteSpace(value))
            {
                dropdownValueErrorProvider.SetError(dropdown, "Please select a value");
                e.Cancel = true;
            }
            else
            {
                dropdownValueErrorProvider.SetError(dropdown, "");
            }
        }

        public static void ValidateGreaterThanZero(object sender, ErrorProvider greaterThanZeroErrorProvider, CancelEventArgs e)
        {
            var textBox = sender as RadMaskedEditBox;
            var value = Convert.ToDouble(textBox.Text);
            if (value <= 0)
            {
                greaterThanZeroErrorProvider.SetError(textBox, "Please enter a value");
                e.Cancel = true;
            }
            else
            {
                greaterThanZeroErrorProvider.SetError(textBox, "");
            }
        }
    }
}