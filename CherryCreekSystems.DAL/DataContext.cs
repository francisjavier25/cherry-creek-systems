﻿using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using CherryCreekSystems.Domain.SugarCRMLogs;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CherryCreekSystems.DAL
{
    public class DataContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>
    {
        public static DataContext Create()
        {
            return new DataContext();
        }

        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public DataContext()
        : base("CherryCreekSystems")
        {
            var ensureDLLIsCopied =
                System.Data.Entity.SqlServer.SqlProviderServices.Instance;

            Configuration.LazyLoadingEnabled = false;
            //Database.SetInitializer<DataContext>(null);//Temporary
        }

        public IQueryable<DistributorUser> GetDistributorUsers()
        {
            var users = Users.Join(ApplicationUserRoles, u => u.Id, ur => ur.UserId,
              (u, ur) => new
              {
                  u,
                  ur
              })
              .Join(Roles, ur => ur.ur.RoleId, r => r.Id, (ur, r) => new
              {
                  ur.u,
                  ur.ur,
                  r
              })
              .Join(Distributors, u => u.ur.ApplicationUserId, d => d.Id, (u, d) => new DistributorUser()
              {
                  Name = d.Name,
                  UserName = u.u.UserName,
                  ApplicationUserId = u.ur.ApplicationUserId,
                  Role = u.r.Name
              })
              .Where(d => d.Role.ToLower() == "distributor");

            return users;
        }

        public IQueryable<SalesRepUser> GetSalesRepUsers()
        {
            var users = Users.Join(ApplicationUserRoles, u => u.Id, ur => ur.UserId,
              (u, ur) => new
              {
                  u,
                  ur
              })
              .Join(Roles, ur => ur.ur.RoleId, r => r.Id, (ur, r) => new
              {
                  ur.u,
                  ur.ur,
                  r
              })
              .Join(SalesReps, u => u.ur.ApplicationUserId, d => d.Id, (u, d) => new SalesRepUser
              {
                  Name = d.Name,
                  UserName = u.u.UserName,
                  ApplicationUserId = u.ur.ApplicationUserId,
                  Role = u.r.Name
              })
              .Where(d => d.Role.ToLower() == "salesrep");

            return users;
        }

        public IQueryable<CustomerUser> GetCustomerUsers()
        {
            var users = Users.Join(ApplicationUserRoles, u => u.Id, ur => ur.UserId,
              (u, ur) => new
              {
                  u,
                  ur
              })
              .Join(Roles, ur => ur.ur.RoleId, r => r.Id, (ur, r) => new
              {
                  ur.u,
                  ur.ur,
                  r
              })
              .Join(Customers, u => u.ur.ApplicationUserId, d => d.Id, (u, d) => new CustomerUser()
              {
                  CompanyName = d.CompanyName,
                  ContactName = d.ContactName,
                  Email = d.Email,
                  Phone = d.Phone,
                  Fax = d.Fax,
                  UserName = u.u.UserName,
                  ApplicationUserId = u.ur.ApplicationUserId,
                  Role = u.r.Name
              })
              .Where(d => d.Role.ToLower() == "customer");

            return users;
        }

        public DbSet<CustomQuoteItem> CustomeQuoteItems { get; set; }

        public DbSet<EmailLog> EmailLogs { get; set; }

        public DbSet<CustomerCheckList> CustomerCheckLists { get; set; }

        public DbSet<ApplicationUserRole> ApplicationUserRoles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<CustomerLog> CustomerLog { get; set; }

        public DbSet<ShipAddress> ShipAddresses { get; set; }

        public DbSet<SalesRep> SalesReps { get; set; }

        public DbSet<GreenHouseType> GreenHouseTypes { get; set; }

        public DbSet<QuoteRequest> QuoteRequests { get; set; }

        public DbSet<QuoteRequestBoom> QuoteRequestBooms { get; set; }

        public DbSet<BoomSystem> BoomSystems { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<MajorComponent> MajorComponents { get; set; }

        public DbSet<MinorComponent> MinorComponents { get; set; }

        public DbSet<MajorComponentDetail> MajorComponentDetails { get; set; }

        public DbSet<MinorComponentDetail> MinorComponentDetails { get; set; }

        public DbSet<QuoteStatus> QuoteStatuses { get; set; }

        public DbSet<Spec> Specs { get; set; }

        public DbSet<SpecLookup> SpecLookups { get; set; }

        public DbSet<State> States { get; set; }

        public DbSet<Distributor> Distributors { get; set; }

        public DbSet<BaseCost> BaseCosts { get; set; }

        public DbSet<ApplicationLog> ApplicationLogs { get; set; }

        public DbSet<SteelEstimate> SteelEstimates { get; set; }

        public DbSet<SteelMaterialDefault> SteelMaterialDefaults { get; set; }

        public DbSet<StatusThreshold> StatusTresholds { get; set; }

        public DbSet<SteelData> SteelData { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.ComplexType<QuoteBoomSystemData>()
                  .Property(p => p.Serialized)
                  .HasColumnName("QuoteBoomSystemData");
            modelBuilder.ComplexType<QuoteBoomSystemData>().Ignore(p => p.QuoteBoomSystems);
            modelBuilder.ComplexType<QuoteBoomSystemData>().Ignore(p => p.MarkDirty);

            modelBuilder.Ignore<DistributorUser>();
            modelBuilder.Entity<ApplicationUser>().Ignore(p => p.ApplicationRoles);
            //modelBuilder.Entity<QuoteRequest>().Ignore(p => p.BoomType);
            modelBuilder.Entity<QuoteRequest>().Ignore(p => p.QuoteNumberWithRevision);
            modelBuilder.Entity<QuoteRequest>().Ignore(p => p.FreightEstimateValue);
            modelBuilder.Entity<QuoteRequest>().Ignore(p => p.InstallationEstimateValue);
            //modelBuilder.Entity<CustomerCheckList>().Ignore(p => p.SignatureImageBase64);
            //modelBuilder.Entity<CustomerCheckList>().Ignore(p => p.BoomSketchImageBase64);
            //modelBuilder.Entity<QuoteRequest>().Ignore(p => p.ExpectedShipmentDate);


            modelBuilder.Entity<QuoteRequest>()
            .HasOptional(q => q.ShipAddress)
            .WithMany()
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShipAddress>().Ignore(s => s.FullAddress);
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (Exception ex)
            {
                log.Error("DbContext Save Error", ex);
                throw;
            }
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(ApplicationUserStore store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new ApplicationUserStore(context.Get<DataContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = false
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 10;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>, IUserStore<ApplicationUser>
    {
        public ApplicationUserStore(DataContext context)
            : base(context)
        {
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }

    public class ApplicationRoleStore : RoleStore<ApplicationRole, String, ApplicationUserRole>
    {
        public ApplicationRoleStore(DataContext context)
            : base(context)
        {
        }
    }

    public class ApplicationRoleManager : RoleManager<ApplicationRole, string>
    {
        public ApplicationRoleManager(ApplicationRoleStore roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            return new ApplicationRoleManager(new ApplicationRoleStore(context.Get<DataContext>()));
        }
    }

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser<string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>, IUser, IUser<string>
    {
        public ICollection<Role> ApplicationRoles { get; set; }

        public ApplicationUser()
        {
            Id = Guid.NewGuid().ToString();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationRole : IdentityRole<string, ApplicationUserRole>, IRole<string>
    {
        public ApplicationRole()
        {
            Id = Guid.NewGuid().ToString();
        }

        public ApplicationRole(string name)
            : this()
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
        }
    }

    public class ApplicationUserRole : IdentityUserRole
    {
        public int? ApplicationUserId { get; set; }
    }

    public class Role
    {
        public int? ApplicationUserId { get; set; }
        public string RoleName { get; set; }
    }

    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}