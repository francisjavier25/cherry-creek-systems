using CherryCreekSystems.Domain;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text.RegularExpressions;

namespace CherryCreekSystems.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DataContext context)
        {
            //    AddStates(context);
            //    context.SaveChanges();
            //    AddSalesReps(context);
            //    AddGreenHouseTypes(context);
            //    AddCustomers(context);
            //AddSystemsAndComponents(context);
            //    AddQuoteStatuses(context);
                AddSpecLookups(context);
            //    AddDistributors(context);
            //AddSteelEstimates(context);
            AddSteelMaterialDefaults(context);
            //AddStatusThresholds(context);
            //    AddSteelData(context);
            //context.SaveChanges();
            //AddBaseCosts(context);

            //context.SaveChanges();

            #region update description fields for new systems

            //foreach (var system in context.BoomSystems.Where(s => s.BoomTypeId >= 8).ToList())
            //{
            //    if (string.IsNullOrWhiteSpace(system.DescriptionCalculation))
            //    {
            //        system.DescriptionCalculation = string.Format(@"""{0}""", system.Name.Replace(@"""", @""""""));
            //    }

            //    foreach (var category in system.Categories)
            //    {
            //        if (category.MajorComponents != null)
            //        {
            //            foreach (var majorComponent in category.MajorComponents)
            //            {
            //                var majorComponentDescriptionCalculationId = Regex.Replace(majorComponent.ComponentID,
            //                    "[^0-9.]", "");
            //                majorComponent.DescriptionCalculationID = "MACT" + majorComponentDescriptionCalculationId;
            //                if (string.IsNullOrWhiteSpace(majorComponent.DescriptionCalculation))
            //                {
            //                    majorComponent.DescriptionCalculation = string.Format(@"""{0}""",
            //                        majorComponent.Name.Replace(@"""", @""""""));
            //                }

            //                if (string.IsNullOrWhiteSpace(majorComponent.ActivateComponentCondition))
            //                {
            //                    majorComponent.ActivateComponentCondition = "true";
            //                }

            //                if (majorComponent.MinorComponents != null)
            //                {
            //                    foreach (var minorComponent in majorComponent.MinorComponents)
            //                    {
            //                        var minorComponentDescriptionCalculationId =
            //                            Regex.Replace(minorComponent.ComponentID, "[^0-9.]", "");
            //                        minorComponent.DescriptionCalculationID = "MICT" +
            //                                                                  minorComponentDescriptionCalculationId;
            //                        if (string.IsNullOrWhiteSpace(minorComponent.DescriptionCalculation))
            //                        {
            //                            minorComponent.DescriptionCalculation = string.Format(@"""{0}""",
            //                                minorComponent.Name.Replace(@"""", @""""""));
            //                        }

            //                        if (string.IsNullOrWhiteSpace(minorComponent.ActivateComponentCondition))
            //                        {
            //                            minorComponent.ActivateComponentCondition = "true";
            //                        }

            //                        if (minorComponent.MinorComponentDetails != null)
            //                        {
            //                            foreach (var minorComponentDetail in minorComponent.MinorComponentDetails)
            //                            {
            //                                if (string.IsNullOrWhiteSpace(minorComponentDetail.DescriptionCalculation))
            //                                {
            //                                    minorComponentDetail.DescriptionCalculation = string.Format(@"""{0}""",
            //                                        minorComponentDetail.Name.Replace(@"""", @""""""));
            //                                }

            //                                minorComponentDetail.ComponentID = "MID" + minorComponentDetail.Id;
            //                                minorComponentDetail.DescriptionCalculationID = "MIDT" +
            //                                                                                minorComponentDetail.Id;
            //                                minorComponentDetail.DetailCalculationID = "MIDD" + minorComponentDetail.Id;
            //                                minorComponentDetail.PriceCalculationID = "MIDP" + minorComponentDetail.Id;
            //                            }
            //                        }
            //                    }
            //                }

            //                if (majorComponent.MajorComponentDetails != null)
            //                {
            //                    foreach (var majorComponentDetail in majorComponent.MajorComponentDetails)
            //                    {
            //                        if (string.IsNullOrWhiteSpace(majorComponentDetail.DescriptionCalculation))
            //                        {
            //                            majorComponentDetail.DescriptionCalculation = string.Format(@"""{0}""",
            //                                majorComponentDetail.Name.Replace(@"""", @""""""));
            //                        }

            //                        majorComponentDetail.ComponentID = "MAD" + majorComponentDetail.Id;
            //                        majorComponentDetail.DescriptionCalculationID = "MADT" + majorComponentDetail.Id;
            //                        majorComponentDetail.DetailCalculationID = "MADD" + majorComponentDetail.Id;
            //                        majorComponentDetail.PriceCalculationID = "MADP" + majorComponentDetail.Id;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}

            //context.SaveChanges();

            #endregion update description fields for new systems

            var roleManager = new ApplicationRoleManager(new ApplicationRoleStore(context));

            // Create Admin Role
            string[] roles = { "administrator", "distributor", "customer", "salesrep" };

            // Check to see if Role Exists, if not create it
            foreach (var role in roles)
            {
                if (!roleManager.RoleExists(role))
                {
                    ApplicationRole appRole = new ApplicationRole(role);
                    context.Roles.Add(appRole);
                }
            }

            context.SaveChanges();
            var dbRoles = roleManager.Roles.ToArray();

            var userManager = new ApplicationUserManager(new ApplicationUserStore(context));

            if (userManager.FindByName("admin") == null)
            {
                var user = new ApplicationUser { UserName = "admin" };
                userManager.Create(user, "@dmin123");

                var adminUserRole = new ApplicationUserRole
                {
                    UserId = user.Id,
                    ApplicationUserId = null,
                    RoleId =
                        dbRoles.First(r => r.Name.Equals("administrator", StringComparison.OrdinalIgnoreCase)).Id
                };
                context.ApplicationUserRoles.Add(adminUserRole);
            }

            if (userManager.FindByName("admin2") == null)
            {
                var user = new ApplicationUser { UserName = "admin2" };
                userManager.Create(user, "@dmin123");

                var adminUserRole = new ApplicationUserRole
                {
                    UserId = user.Id,
                    ApplicationUserId = null,
                    RoleId =
                        dbRoles.First(r => r.Name.Equals("administrator", StringComparison.OrdinalIgnoreCase)).Id
                };
                context.ApplicationUserRoles.Add(adminUserRole);
            }

            var distributors = context.Distributors.ToList();

            foreach (var dist in distributors)
            {
                var distName = Regex.Replace(dist.Name, "[^0-9a-zA-Z]+", "").Replace(" ", "");
                var distributorExists = userManager.FindByName(distName) != null;

                if (distributorExists) continue;

                var distUser = new ApplicationUser { UserName = distName };
                userManager.Create(distUser, "password123");

                var distributorUserRole = new ApplicationUserRole
                {
                    UserId = distUser.Id,
                    ApplicationUserId = dist.Id,
                    RoleId = dbRoles.First(r => r.Name.Equals("distributor", StringComparison.OrdinalIgnoreCase)).Id
                };
                context.ApplicationUserRoles.Add(distributorUserRole);
            }

            var customers = context.Customers.ToList();

            foreach (var cust in customers)
            {
                var custName = Regex.Replace(cust.CompanyName, "[^0-9a-zA-Z]+", "").Replace(" ", "");
                var customerExists = userManager.FindByName(custName) != null;

                if (customerExists) continue;

                var custUser = new ApplicationUser { UserName = custName };
                userManager.Create(custUser, "password123");

                var customerUserRole = new ApplicationUserRole
                {
                    UserId = custUser.Id,
                    ApplicationUserId = cust.Id,
                    RoleId = dbRoles.First(r => r.Name.Equals("customer", StringComparison.OrdinalIgnoreCase)).Id
                };
                context.ApplicationUserRoles.Add(customerUserRole);
            }

            var salesReps = context.SalesReps.ToList();

            foreach (var rep in salesReps)
            {
                var repName = Regex.Replace(rep.Name, "[^0-9a-zA-Z]+", "").Replace(" ", "");
                var repExists = userManager.FindByName(repName) != null;

                if (repExists) continue;

                var repUser = new ApplicationUser { UserName = repName };
                userManager.Create(repUser, "password123");

                var repUserRole = new ApplicationUserRole
                {
                    UserId = repUser.Id,
                    ApplicationUserId = rep.Id,
                    RoleId = dbRoles.First(r => r.Name.Equals("salesrep", StringComparison.OrdinalIgnoreCase)).Id
                };
                context.ApplicationUserRoles.Add(repUserRole);
            }

            context.SaveChanges();
        }

        ///// <summary>
        ///// Wrapper for SaveChanges adding the Validation Messages to the generated exception
        ///// </summary>
        ///// <param name="context">The context.</param>
        //private void SaveChanges(DataContext context)
        //{
        //    try
        //    {
        //        context.SaveChanges();
        //    }
        //    catch (DbEntityValidationException ex)
        //    {
        //        StringBuilder sb = new StringBuilder();

        //        foreach (var failure in ex.EntityValidationErrors)
        //        {
        //            sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
        //            foreach (var error in failure.ValidationErrors)
        //            {
        //                sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
        //                sb.AppendLine();
        //            }
        //        }

        //        throw new DbEntityValidationException(
        //            "Entity Validation Failed - errors follow:\n" +
        //            sb.ToString(), ex
        //        ); // Add the original exception as the innerException
        //    }
        //}

        private void AddSteelMaterialDefaults(DataContext context)
        {
            context.SteelMaterialDefaults.AddOrUpdate(s => s.Material,
                new SteelMaterialDefault
                {
                    Material = "2 x 2 x 14ga"
                }, new SteelMaterialDefault
                {
                    Material = "2 x 2 x 11ga"
                }, new SteelMaterialDefault
                {
                    Material = "2 x 2 x 10ga"
                }, new SteelMaterialDefault
                {
                    Material = "2 x 3 x 14ga"
                }, new SteelMaterialDefault
                {
                    Material = "2 � 3 � 11ga"
                }, new SteelMaterialDefault
                {
                    Material = "2 x 4 x 14ga"
                }, new SteelMaterialDefault
                {
                    Material = "2 x 4 x 11ga"
                }, new SteelMaterialDefault
                {
                    Material = "2 x 6 x 14ga"
                }, new SteelMaterialDefault
                {
                    Material = "2 x 6 x 11ga"
                }
              );
        }

        private void AddSteelData(DataContext context)
        {
            context.SteelData.AddOrUpdate(s => s.Size,
                new SteelData
                {
                    Size = @"3/4"" (20mm)",
                    OuterDiameter = @"1.05"" (26.7mm)",
                    PoundsPerFoot = @"1.131 lbs/ft",
                    Schedule = @"40"
                },
                new SteelData
                {
                    Size = @"1"" (25mm)",
                    OuterDiameter = @"1.32"" (33.4mm)",
                    PoundsPerFoot = @"1.679 lbs/ft",
                    Schedule = @"40"
                },
                new SteelData
                {
                    Size = @"1 1/4"" (32mm)",
                    OuterDiameter = @"1.66"" (42.2mm)",
                    PoundsPerFoot = @"2.273 lbs/ft",
                    Schedule = @"40"
                },
                new SteelData
                {
                    Size = @"1 1/2"" (40mm)",
                    OuterDiameter = @"1.90"" (48.3mm)",
                    PoundsPerFoot = @"2.718 lbs/ft",
                    Schedule = @"40"
                },
                new SteelData
                {
                    Size = @"2"" (50mm)",
                    OuterDiameter = @"2.38"" (60.3mm)",
                    PoundsPerFoot = @"3.653 lbs/ft",
                    Schedule = @"40"
                },
                new SteelData
                {
                    Size = @"2 1/2"" (65mm)",
                    OuterDiameter = @"2.88"" (73.0mm)",
                    PoundsPerFoot = @"5.793 lbs/ft",
                    Schedule = @"40"
                });
        }

        private void AddSteelEstimates(DataContext context)
        {
            //context.SteelEstimates.AddOrUpdate(s => s.BoomTypeId,
            //new SteelEstimate
            //{
            //    BoomTypeId = 1,
            //    Material = @"2""x 2""x 14GA Coated Steel Tube:",
            //    QuantityCalculation = @"=(BayLengthInFeet * 2) * SystemCount & "" FT""",
            //    ActivateEstimateCondition = "true"
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 2,
            //    Material = @"1 1/4"" Schedule 40 Gal. Steel Pipe:",
            //    QuantityCalculation = @"=BayLengthInFeet * SystemCount & "" FT""",
            //    ActivateEstimateCondition = "true"
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 3,
            //    Material = @"1 1/4"" Schedule 40 Gal. Steel Pipe:",
            //    QuantityCalculation = @"=( BayLengthInFeet * 2 ) * SystemCount & "" FT""",
            //    ActivateEstimateCondition = "true"
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 4,
            //    Material = @"2""x 2""x 14GA Coated Steel Tube:",
            //    QuantityCalculation = @"=( BayLengthInFeet * 2 ) * SystemCount & "" FT""",
            //    ActivateEstimateCondition = "true"
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 5,
            //    Material = @"2""x 2""x 14GA Coated Steel Tube (Diamond Trolleys):",
            //    QuantityCalculation = @"= BayLengthInFeet * SystemCount & "" FT""",
            //    ActivateEstimateCondition = @"= CarrySteel = ""Diamond Trolley Rails"""
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 5,
            //    Material = @"1"" Schedule 40 Gal. Steel Pipe: (CWF System Rails):",
            //    QuantityCalculation = @"= BayLengthInFeet * SystemCount * 1.5 & "" FT""",
            //    ActivateEstimateCondition = @"= CarrySteel = ""CWF System Rails"""
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 5,
            //    Material = @"2""x 4""x 14GA Coated Steel Tube (Rails):",
            //    QuantityCalculation = @"= BayLengthInFeet * SystemCount * 2 & "" FT""",
            //    ActivateEstimateCondition = "true"
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 6,
            //    Material = @"2""x 2""x 14GA Coated Steel Tube (Diamond Trolleys):",
            //    QuantityCalculation = @"= BayLengthInFeet * SystemCount & "" FT""",
            //    ActivateEstimateCondition = @"= CarrySteel = ""Diamond Trolley Rails"""
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 7,
            //    Material = @"2""x 2""x 14GA Coated Steel Tube:",
            //    QuantityCalculation = @"=( BayLengthInFeet * 2 ) * SystemCount & "" FT""",
            //    ActivateEstimateCondition = "true"
            //},
            //new SteelEstimate
            //{
            //    BoomTypeId = 8,
            //    Material = @"2""x 2""x 14GA Coated Steel Tube:",
            //    QuantityCalculation = @"=( BayLengthInFeet * 2 ) * SystemCount & "" FT""",
            //    ActivateEstimateCondition = "true"
            //});
        }

        private static void AddStatusThresholds(DataContext context)
        {
            context.StatusTresholds.AddOrUpdate(
                new StatusThreshold
                {
                    BoomTypeId = 1,
                    InfoAndDesignThreshold = 0.1,
                    SprayBarApprovalThreshold = 0.1,
                    AssemblyPickListThreshold = 0.1,
                    ShippingPickListThreshold = 0.15,
                    ProductionThreshold = 0.15,
                    SawAndSheerThreshold = 0.5,
                    PunchAndBreakThreshold = 0.5,
                    MachiningThreshold = 0.5,
                    WeldingThreshold = 0.5,
                    GalganizerThreshold = 0.6,
                    AssemblyThreshold = 0.8,
                    ShippingThreshold = 0.9
                },
                new StatusThreshold
                {
                    BoomTypeId = 2,
                    InfoAndDesignThreshold = 0.1,
                    SprayBarApprovalThreshold = 0.1,
                    AssemblyPickListThreshold = 0.1,
                    ShippingPickListThreshold = 0.15,
                    ProductionThreshold = 0.15,
                    SawAndSheerThreshold = 0.5,
                    PunchAndBreakThreshold = 0.5,
                    MachiningThreshold = 0.5,
                    WeldingThreshold = 0.5,
                    GalganizerThreshold = 0.6,
                    AssemblyThreshold = 0.8,
                    ShippingThreshold = 0.9
                },
                new StatusThreshold
                {
                    BoomTypeId = 3,
                    InfoAndDesignThreshold = 0.1,
                    SprayBarApprovalThreshold = 0.1,
                    AssemblyPickListThreshold = 0.1,
                    ShippingPickListThreshold = 0.15,
                    ProductionThreshold = 0.15,
                    SawAndSheerThreshold = 0.5,
                    PunchAndBreakThreshold = 0.5,
                    MachiningThreshold = 0.5,
                    WeldingThreshold = 0.5,
                    GalganizerThreshold = 0.6,
                    AssemblyThreshold = 0.8,
                    ShippingThreshold = 0.9
                },
                new StatusThreshold
                {
                    BoomTypeId = 4,
                    InfoAndDesignThreshold = 0.1,
                    SprayBarApprovalThreshold = 0.1,
                    AssemblyPickListThreshold = 0.1,
                    ShippingPickListThreshold = 0.15,
                    ProductionThreshold = 0.15,
                    SawAndSheerThreshold = 0.5,
                    PunchAndBreakThreshold = 0.5,
                    MachiningThreshold = 0.5,
                    WeldingThreshold = 0.5,
                    GalganizerThreshold = 0.6,
                    AssemblyThreshold = 0.8,
                    ShippingThreshold = 0.9
                },
                new StatusThreshold
                {
                    BoomTypeId = 5,
                    InfoAndDesignThreshold = 0.1,
                    SprayBarApprovalThreshold = 0.1,
                    AssemblyPickListThreshold = 0.1,
                    ShippingPickListThreshold = 0.15,
                    ProductionThreshold = 0.15,
                    SawAndSheerThreshold = 0.5,
                    PunchAndBreakThreshold = 0.5,
                    MachiningThreshold = 0.5,
                    WeldingThreshold = 0.5,
                    GalganizerThreshold = 0.6,
                    AssemblyThreshold = 0.8,
                    ShippingThreshold = 0.9
                },
                new StatusThreshold
                {
                    BoomTypeId = 6,
                    InfoAndDesignThreshold = 0.1,
                    SprayBarApprovalThreshold = 0.1,
                    AssemblyPickListThreshold = 0.1,
                    ShippingPickListThreshold = 0.15,
                    ProductionThreshold = 0.15,
                    SawAndSheerThreshold = 0.5,
                    PunchAndBreakThreshold = 0.5,
                    MachiningThreshold = 0.5,
                    WeldingThreshold = 0.5,
                    GalganizerThreshold = 0.6,
                    AssemblyThreshold = 0.8,
                    ShippingThreshold = 0.9
                },
                 new StatusThreshold
                 {
                     BoomTypeId = 7,
                     InfoAndDesignThreshold = 0.1,
                     SprayBarApprovalThreshold = 0.1,
                     AssemblyPickListThreshold = 0.1,
                     ShippingPickListThreshold = 0.15,
                     ProductionThreshold = 0.15,
                     SawAndSheerThreshold = 0.5,
                     PunchAndBreakThreshold = 0.5,
                     MachiningThreshold = 0.5,
                     WeldingThreshold = 0.5,
                     GalganizerThreshold = 0.6,
                     AssemblyThreshold = 0.8,
                     ShippingThreshold = 0.9
                 },
                  new StatusThreshold
                  {
                      BoomTypeId = 8,
                      InfoAndDesignThreshold = 0.1,
                      SprayBarApprovalThreshold = 0.1,
                      AssemblyPickListThreshold = 0.1,
                      ShippingPickListThreshold = 0.15,
                      ProductionThreshold = 0.15,
                      SawAndSheerThreshold = 0.5,
                      PunchAndBreakThreshold = 0.5,
                      MachiningThreshold = 0.5,
                      WeldingThreshold = 0.5,
                      GalganizerThreshold = 0.6,
                      AssemblyThreshold = 0.8,
                      ShippingThreshold = 0.9
                  }
                );
        }

        private void AddBaseCosts(DataContext context)
        {
            context.BaseCosts.AddOrUpdate(b => new { b.BoomSystemId, b.XValue, b.YValue },
                //new BaseCost { BoomSystemId = 1, XValue = 150, YValue = 2301.10 },
                //new BaseCost { BoomSystemId = 1, XValue = 200, YValue = 2167.83 },
                //new BaseCost { BoomSystemId = 1, XValue = 250, YValue = 2034.56 },
                //new BaseCost { BoomSystemId = 1, XValue = 300, YValue = 1901.29 },
                //new BaseCost { BoomSystemId = 1, XValue = 350, YValue = 1768.03 },
                //new BaseCost { BoomSystemId = 1, XValue = 400, YValue = 1634.76 },
                //new BaseCost { BoomSystemId = 2, XValue = 10, YValue = 2091.85 },
                //new BaseCost { BoomSystemId = 2, XValue = 15, YValue = 2228.80 },
                //new BaseCost { BoomSystemId = 2, XValue = 20, YValue = 2379.55 },
                //new BaseCost { BoomSystemId = 2, XValue = 25, YValue = 2504.05 },
                //new BaseCost { BoomSystemId = 2, XValue = 30, YValue = 2518.93 },
                //new BaseCost { BoomSystemId = 2, XValue = 35, YValue = 2756.80 },
                //new BaseCost { BoomSystemId = 2, XValue = 40, YValue = 2824.98 },
                //new BaseCost { BoomSystemId = 3, XValue = 10, YValue = 2091.85 },
                //new BaseCost { BoomSystemId = 3, XValue = 15, YValue = 2228.80 },
                //new BaseCost { BoomSystemId = 3, XValue = 20, YValue = 2379.55 },
                //new BaseCost { BoomSystemId = 3, XValue = 25, YValue = 2504.05 },
                //new BaseCost { BoomSystemId = 3, XValue = 30, YValue = 2518.93 },
                //new BaseCost { BoomSystemId = 3, XValue = 35, YValue = 2756.80 },
                //new BaseCost { BoomSystemId = 3, XValue = 40, YValue = 2824.98 },
                //new BaseCost { BoomSystemId = 4, XValue = 10, YValue = 2091.85 },
                //new BaseCost { BoomSystemId = 4, XValue = 15, YValue = 2228.80 },
                //new BaseCost { BoomSystemId = 4, XValue = 20, YValue = 2379.55 },
                //new BaseCost { BoomSystemId = 4, XValue = 25, YValue = 2504.05 },
                //new BaseCost { BoomSystemId = 4, XValue = 30, YValue = 2518.93 },
                //new BaseCost { BoomSystemId = 4, XValue = 35, YValue = 2756.80 },
                //new BaseCost { BoomSystemId = 4, XValue = 40, YValue = 2824.98 },
                //new BaseCost { BoomSystemId = 5, XValue = 15, YValue = 153.41 },
                //new BaseCost { BoomSystemId = 5, XValue = 21, YValue = 144.16 },
                //new BaseCost { BoomSystemId = 5, XValue = 25, YValue = 137.99 },
                //new BaseCost { BoomSystemId = 5, XValue = 30, YValue = 130.28 },
                //new BaseCost { BoomSystemId = 5, XValue = 35, YValue = 122.57 },
                //new BaseCost { BoomSystemId = 5, XValue = 40, YValue = 114.86 },
                //new BaseCost { BoomSystemId = 6, XValue = 15, YValue = 153.41 },
                //new BaseCost { BoomSystemId = 6, XValue = 21, YValue = 144.16 },
                //new BaseCost { BoomSystemId = 6, XValue = 25, YValue = 137.99 },
                //new BaseCost { BoomSystemId = 6, XValue = 30, YValue = 130.28 },
                //new BaseCost { BoomSystemId = 6, XValue = 35, YValue = 122.57 },
                //new BaseCost { BoomSystemId = 6, XValue = 40, YValue = 114.86 },

                //Ground Runner
                //new BaseCost { BoomSystemId = 7, XValue = 10, YValue = 2091.85 },
                //new BaseCost { BoomSystemId = 7, XValue = 15, YValue = 2228.80 },
                //new BaseCost { BoomSystemId = 7, XValue = 20, YValue = 2379.55 },
                //new BaseCost { BoomSystemId = 7, XValue = 25, YValue = 2504.05 },
                //new BaseCost { BoomSystemId = 7, XValue = 30, YValue = 2518.93 },
                //new BaseCost { BoomSystemId = 7, XValue = 35, YValue = 2756.80 },
                //new BaseCost { BoomSystemId = 7, XValue = 40, YValue = 2824.98 }

                //Sky Rail
                new BaseCost { BoomSystemId = 8, XValue = 10, YValue = 2091.85 },
                new BaseCost { BoomSystemId = 8, XValue = 15, YValue = 2228.80 },
                new BaseCost { BoomSystemId = 8, XValue = 20, YValue = 2379.55 },
                new BaseCost { BoomSystemId = 8, XValue = 25, YValue = 2504.05 },
                new BaseCost { BoomSystemId = 8, XValue = 30, YValue = 2518.93 },
                new BaseCost { BoomSystemId = 8, XValue = 35, YValue = 2756.80 },
                new BaseCost { BoomSystemId = 8, XValue = 40, YValue = 2824.98 }
                );
        }

        private void AddDistributors(DataContext context)
        {
            context.Distributors.AddOrUpdate(d => d.Name,
                new Distributor { Name = "Cherry Creek Systems, Inc." },
                new Distributor { Name = "BFG Supply" },
                new Distributor { Name = "BWI Companies" },
                new Distributor { Name = "Rough Bro" },
                new Distributor { Name = "Stuppy" },
                new Distributor { Name = "US Global Resources" },
                new Distributor { Name = "VWGHC" },
                new Distributor { Name = "Horticultural Services" },
                new Distributor { Name = "Agratech" },
                new Distributor { Name = "Crop Production Services" },
                new Distributor { Name = "Nexus Greenhouse" }
                );
        }

        private void AddSpecLookups(DataContext context)
        {
            context.Specs.AddOrUpdate(s => s.Name,
                new Spec
                {
                    Name = "Drum Size",
                    SpecLookups = new List<SpecLookup>
                    {
                            new SpecLookup {Value = "20"},
                            new SpecLookup {Value = "30"},
                            new SpecLookup {Value = "40"},
                            new SpecLookup {Value = "50"}
                           }
                },
                new Spec
                {
                    Name = "Echo Controller Type",
                    SpecLookups = new List<SpecLookup>
                    {
                           new SpecLookup {Value = "Standard Enhanced"},
                           new SpecLookup {Value = "Variable Speed Reaction"}
                          }
                },
                new Spec
                {
                    Name = "SRB Controller Type",
                    SpecLookups = new List<SpecLookup>
                      {
                           new SpecLookup {Value = "NIC"}
                          }
                },
                new Spec
                {
                    Name = "DRB Controller Type",
                    SpecLookups = new List<SpecLookup>
                      {
                           new SpecLookup {Value = "NIC"},
                           new SpecLookup {Value = "WIC"}
                          }
                },
                new Spec
                {
                    Name = "Single Dual Water",
                    SpecLookups = new List<SpecLookup>
                    {
                           new SpecLookup {Value = "Single"},
                           new SpecLookup {Value = "Dual"}
                          }
                },
                new Spec
                {
                    Name = "Number Of Layers",
                    SpecLookups = new List<SpecLookup>
                    {
                          new SpecLookup {Value = "1"},
                          new SpecLookup {Value = "2"},
                          new SpecLookup {Value = "3"}
                         }
                },
                new Spec
                {
                    Name = "Pulley Bracket Spacing",
                    SpecLookups = new List<SpecLookup>
                    {
                         new SpecLookup {Value = "4"},
                         new SpecLookup {Value = "5"},
                         new SpecLookup {Value = "6"}
                        }
                },
                new Spec
                {
                    Name = "EV VPD Gauge Wires",
                    SpecLookups = new List<SpecLookup>
                    {
                        new SpecLookup{Value="12/3"},
                        new SpecLookup{Value="12/5"},
                        new SpecLookup{Value="16/3"},
                        new SpecLookup{Value="16/5"},
                        new SpecLookup{Value="14/3 X 22/6"}
                    }
                }
                );
        }

        private void AddQuoteStatuses(DataContext context)
        {
            context.QuoteStatuses.AddOrUpdate(q => q.Name,
                    new QuoteStatus { Name = "Active" },
                    new QuoteStatus { Name = "In Progress" },
                    new QuoteStatus { Name = "Closed" }
            );
        }

        private void AddSystemsAndComponents(DataContext context)
        {
            #region Starter Boom Systems

            //#region Echo System

            //context.BoomSystems.AddOrUpdate(s => s.Name,
            //new BoomSystem
            //{
            //    Name = "ECHO� System",
            //    DetailCalculation = @"=DrumSize & """"""""",
            //    PriceCalculation = "ROUNDUP"
            //                       + "(BaseValue \r\n"
            //                       + "+(MACD9*1.35+(BayLengthInFeet*2*NumberOfLayers*0.2*1.35)) \r\n"
            //                       + "+((BayLengthInFeet/TrussSpacingInFeet*(8*1.35)))*1.25 \r\n"
            //                       + ", -1) \r\n"
            //                       + "*1.15+350 \r\n",
            //    BoomTypeId = 1,
            //    ComponentID = "SYS",
            //    DetailCalculationID = "SYSD",
            //    PriceCalculationID = "SYSP",
            //    DescriptionCalculationID = "SYST",
            //    Categories = new List<Category>
            //    {
            //                new Category
            //                {
            //                    Name="General Parts",
            //                    Order = 1,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            Name="Drive Drum assembly",
            //                            ComponentID = "MAC1",
            //                            DetailCalculationID = "MACD1",
            //                            PriceCalculationID = "MACP1",
            //                            DetailCalculation = @"=DrumSize & """"""""",
            //                            Order = 1,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {  ComponentID = "MIC1",
            //                            DetailCalculationID = "MICD1",
            //                            PriceCalculationID = "MICP1", Name="EZ Beam Frame Design", Order = 1,},
            //                                new MinorComponent
            //                                {ComponentID = "MIC2",
            //                            DetailCalculationID = "MICD2",
            //                            PriceCalculationID = "MICP2", Name="Regal 1/3 HP; 62:1 Gear Ratio; 115VAC Motor", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Idler Drum assembly",
            //                            ComponentID = "MAC2",
            //                            DetailCalculationID = "MACD2",
            //                            PriceCalculationID = "MACP2",
            //                            DetailCalculation = @"=DrumSize & """"""""",
            //                            Order = 2,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC3",
            //                            DetailCalculationID = "MICD3",
            //                            PriceCalculationID = "MICP3",Name="EZ Beam Frame Design", Order = 1},
            //                                new MinorComponent
            //                                { ComponentID = "MIC4",
            //                            DetailCalculationID = "MICD4",
            //                            PriceCalculationID = "MICP4",Name="EZ Tension Frame Assembly - 1\" ACME Hex & Thread", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Enhanced ECHO� Controller - 2A7 'Continuous Run' Program",
            //                            Order = 3,
            //                            ComponentID = "MAC3",
            //                            DetailCalculationID = "MACD3",
            //                            PriceCalculationID = "MACP3",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC5",
            //                            DetailCalculationID = "MICD5",
            //                            PriceCalculationID = "MICP5",Name="Activator Tab Switch Assembly (Layers & End of Cycle)", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Layers of Baskets per System",
            //                            ComponentID = "MAC4",
            //                            DetailCalculation = "=NumberOfLayers",
            //                            DetailCalculationID = "MACD4",
            //                            PriceCalculationID = "MACP4",
            //                            Order = 4,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="3/4\" Plumbing Manifold  * Max Pressure for Manifold = 80psi *",
            //                            DetailCalculation = "=NumberOfLayers",
            //                            ComponentID = "MAC5",
            //                            DetailCalculationID = "MACD5",
            //                            PriceCalculationID = "MACP5",
            //                            Order = 5,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC6",
            //                            DetailCalculationID = "MICD6",
            //                            PriceCalculationID = "MICP6",Name="3/4\" Rainbird Solenoid Valves", Order = 1},
            //                                new MinorComponent
            //                                { ComponentID = "MIC7",
            //                            DetailCalculationID = "MICD7",
            //                            PriceCalculationID = "MICP7",Name="1/4\" Apollo brass ball valve for Flow Control", Order= 2},
            //                                new MinorComponent
            //                                {ComponentID = "MIC8",
            //                            DetailCalculationID = "MICD8",
            //                            PriceCalculationID = "MICP8", Name="3/8\" Copper water line(s), Dramm water breaker(s)", Order = 3},
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="[Inch] for Basket Spacing (per layer; center to center)",
            //                            DetailCalculation = "=BasketSpacing",
            //                            ComponentID = "MAC6",
            //                            DetailCalculationID = "MACD6",
            //                            PriceCalculationID = "MACP6",
            //                            Order = 6
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="EZ Ready Tab Flush Assy. [includes CCS Pulley w/ Bearing]",
            //                            DetailCalculation = "=ROUNDUP(BayLengthInFeet / PulleyBracketSpacingInFeet,0) * 2",
            //                            ComponentID = "MAC7",
            //                            DetailCalculationID = "MACD7",
            //                            PriceCalculationID = "MACP7",
            //                            Order = 7,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="[FT.] �� galvanized cable w/ crimped aluminum cable stops",
            //                            DetailCalculation = "=BayLengthInFeet * 2",
            //                            ComponentID = "MAC8",
            //                            DetailCalculationID = "MACD8",
            //                            PriceCalculationID = "MACP8",
            //                            Order = 8,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Heavy Duty UV resistant Hooks [rated up to 75 lbs]",
            //                            DetailCalculation = "=ROUNDUP( MACD8 * 12 / MACD6 * MACD4, 0 )",
            //                            ComponentID = "MAC9",
            //                            DetailCalculationID = "MACD9",
            //                            PriceCalculationID = "MACP9",
            //                            Order = 9
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Anti-Sway Bars to stabilize Top Layer Hooks",
            //                            DetailCalculation = "=ROUNDUP(IF(MACD4 = 1,0, MACD9 / MACD4), 0)",
            //                            ComponentID = "MAC10",
            //                            DetailCalculationID = "MACD10",
            //                            PriceCalculationID = "MACP10",
            //                            Order = 10,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name=@"Extension Hangers",
            //                            DetailCalculation = "=ROUNDUP(IF(MACD4 = 1, 0, MACD9 / MACD4), 0)",
            //                            DescriptionCalculation = @"=ExtensionHangerLengthInInches & """""""" & "" Extension Hangers""",
            //                            ComponentID = "MAC11",
            //                            DetailCalculationID = "MACD11",
            //                            PriceCalculationID = "MACP11",
            //                            Order = 11
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Heavy Duty Black Tabs for 'Tab Style' System",
            //                            DetailCalculation = @"=ROUNDUP(IF(MACD4 = 2, MACD11, MACD9 / MACD4),0)",
            //                            ComponentID = "MAC12",
            //                            DetailCalculationID = "MACD12",
            //                            PriceCalculationID = "MACP12",
            //                            Order = 12
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Heavy Duty Green Tabs for 'Tab Style' System",
            //                            ComponentID = "MAC13",
            //                            DetailCalculationID = "MACD13",
            //                            PriceCalculationID = "MACP13",
            //                            DetailCalculation = "=ROUNDUP(IF(MACD4 = 1, 0, MACD9 / MACD4),0)",
            //                            Order = 13,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="2X2 Tube Sleeves [not for use with heat pipe] - 6\" Long",
            //                            DetailCalculation = "=(ROUNDUP( BayLengthInFeet / 24, 0 ) - 1) * 2",
            //                            ComponentID = "MAC14",
            //                            DetailCalculationID = "MACD14",
            //                            PriceCalculationID = "MACP14",
            //                            Order = 14,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Aluminum Support Clamp Assembly; for attaching 2x2 to truss",
            //                            ComponentID = "MAC15",
            //                            DetailCalculationID = "MACD15",
            //                            PriceCalculationID = "MACP15",
            //                            DetailCalculation = "=(ROUNDUP( BayLengthInFeet / TrussSpacingInFeet + 1, 0 ) ) * 2",
            //                            Order = 15,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="1/4\" Cable Swage Crimper - for connecting ends of cable",
            //                            ComponentID = "MAC16",
            //                            DetailCalculationID = "MACD16",
            //                            PriceCalculationID = "MACP16",
            //                            Order = 16,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="1/4\" Cable Cutter - for cutting cable to length",
            //                            ComponentID = "MAC17",
            //                            DetailCalculationID = "MACD17",
            //                            PriceCalculationID = "MACP17",
            //                            Order = 17,
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail { Name="* One set will be sent per order - to be returned to CCS after use, or customer will be charged *", Order = 1}
            //                            },
            //                        },
            //                    }
            //                },
            //                new Category
            //                {
            //                    Name="Options and Upgrades",
            //                    Order = 2,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            Name="Variable Speed AC Upgrade - ECHO REACTION Controller",
            //                            Order = 1,
            //                            AddPriceCondition = @"=ControllerType = ""Variable Speed Reaction""",
            //                            PriceCalculation = "=385.00",
            //                            ComponentID = "MAC18",
            //                            DetailCalculationID = "MACD18",
            //                            PriceCalculationID = "MACP18",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="w/ Variable Speed 3/8 HP 3Phase Inverter Duty AC Motor", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Center Bay Water Station",
            //                            Order = 2,
            //                            AddPriceCondition = "=CenterBayWaterStationRequested = true",
            //                            PriceCalculation = "=25.00",
            //                            ComponentID = "MAC19",
            //                            DetailCalculationID = "MACD19",
            //                            PriceCalculationID = "MACP19",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="Includes 2 Slotted Ready Tab (Pulley) Assemblies", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="AMIAD Filter (3/4\")",
            //                            Order = 3,
            //                            AddPriceCondition = "=AMIADFilterAssemblyRequested = true",
            //                            PriceCalculation = "=25.00",
            //                            ComponentID = "MAC20",
            //                            DetailCalculationID = "MACD20",
            //                            PriceCalculationID = "MACP20",
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Lock-Line Tight-fit Upgrade",
            //                            Order = 4,
            //                            AddPriceCondition = "=LocklineRequested = true",
            //                            PriceCalculation = "=20.00",
            //                            ComponentID = "MAC21",
            //                            DetailCalculationID = "MACD21",
            //                            PriceCalculationID = "MACP21",
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Dramm \"Redhead\" Breaker Upgrade",
            //                            Order = 5,
            //                            AddPriceCondition = "=DRAMMRedheadBreakerUpgradeRequested = true",
            //                            PriceCalculation = "=15.00",
            //                            ComponentID = "MAC22",
            //                            DetailCalculationID = "MACD22",
            //                            PriceCalculationID = "MACP22",
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Remote Pull-Chain Switch Assembly",
            //                            Order = 6,
            //                            AddPriceCondition = "=RemotePullChainSwitchRequested = true",
            //                            PriceCalculation = "=25.00",
            //                            ComponentID = "MAC23",
            //                            DetailCalculationID = "MACD23",
            //                            PriceCalculationID = "MACP23",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="(assembly w/ switch, chain & enclosure)", Order = 1}
            //                            }
            //                        },
            //                    }
            //                }
            //            }
            //}
            //);

            //#endregion Echo System

            //#region SRB System

            //context.BoomSystems.AddOrUpdate(s => s.Name,
            //        new BoomSystem
            //        {
            //            Name = "Mono Series - Single Rail Boom",
            //            BoomTypeId = 2,
            //            ComponentID = "SYS",
            //            DetailCalculationID = "SYSD",
            //            PriceCalculationID = "SYSP",
            //            DescriptionCalculationID = "SYST",
            //            PriceCalculation = "ROUNDUP"
            //                               + "( \r\n"
            //                               + "(BaseValue+BayLengthInFeet/10*39.2 \r\n"
            //                               + "+BayWidthInFeet*12/SprayBodySpacingInInches*7*1.1 \r\n"
            //                               + ")*1.1 \r\n"
            //                               + ", 0) *1.15 \r\n",
            //            Categories = new List<Category>
            //            {
            //                new Category
            //                {
            //                    Name="General Parts",
            //                    Order = 1,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            Name="Frame and Motor Housing Unit",
            //                            Order = 1,
            //                            ComponentID = "MAC1",
            //                            DetailCalculationID = "MACD1",
            //                            PriceCalculationID = "MACP1",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC1",
            //                            DetailCalculationID = "MICD1",
            //                            PriceCalculationID = "MICP1",Name="Baldor, 1/4 HP variable speed DC motor w/chain drive", Order = 1,
            //                                  MinorComponentDetails =  new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail { Name="(11:1 gear ratio, 90VDC, 2.5 Amps)", Order=1}}
            //                                    },
            //                                new MinorComponent
            //                                { ComponentID = "MIC2",
            //                            DetailCalculationID = "MICD2",
            //                            PriceCalculationID = "MICP2",Name="2 Wheel drive design", Order = 2},
            //                                new MinorComponent
            //                                { ComponentID = "MIC3",
            //                            DetailCalculationID = "MICD3",
            //                            PriceCalculationID = "MICP3",Name="3rd Wheel Idle assembly (Motion, Speed and Direction)", Order = 3},
            //                                new MinorComponent
            //                                { ComponentID = "MIC4",
            //                            DetailCalculationID = "MICD4",
            //                            PriceCalculationID = "MICP4",Name="Adjustable Telescopic \"T\" spray bar support", Order = 4,
            //                                  MinorComponentDetails = new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail {Name="(with 1/8\" aircraft cable and remote pipe supports)", Order=1}
            //                                  }}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="1\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *",
            //                            Order = 2,
            //                            ComponentID = "MAC2",
            //                            DetailCalculationID = "MACD2",
            //                            PriceCalculationID = "MACP2",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC5",
            //                            DetailCalculationID = "MICD5",
            //                            PriceCalculationID = "MICP5",Name="1\" Rainbird Solenoid Valves", Order = 1},
            //                                new MinorComponent
            //                                {ComponentID = "MIC6",
            //                            DetailCalculationID = "MICD6",
            //                            PriceCalculationID = "MICP6", Name="1\" Brass Ball Valve", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Whip Hose Hand-Watering Assy.",
            //                            Order = 3,
            //                            ComponentID = "MAC3",
            //                            DetailCalculationID = "MACD3",
            //                            PriceCalculationID = "MACP3",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC7",
            //                            DetailCalculationID = "MICD7",
            //                            PriceCalculationID = "MICP7",Name="3/4\" Commercial Grade Hose", Order = 1,
            //                                MinorComponentDetails = new List<MinorComponentDetail>
            //                                {
            //                                    new MinorComponentDetail {Name="24\" Dramm Wand Assy. w/ Brass Ball Valve", Order=1}
            //                                }}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Single Water Bar Setup",
            //                            ComponentID = "MAC4",
            //                            DetailCalculationID = "MACD4",
            //                            PriceCalculationID = "MACP4",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    Name="[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters",
            //                                    ComponentID = "MIC8",
            //                            DetailCalculationID = "MICD8",
            //                            PriceCalculationID = "MICP8",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    Name="[inch] TeeJet Spray Bodies w/ Tips & Caps",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 2,
            //                                    ComponentID = "MIC9",
            //                            DetailCalculationID = "MICD9",
            //                            PriceCalculationID = "MICP9",
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail {Name="XR11008 White (0.8 gpm) Tips - unless otherwise specified by customer", Order = 1}
            //                                    }},
            //                                new MinorComponent
            //                                {
            //                                    Name="[inch] Spray Body/Tip Spacing (average)",
            //                                    ComponentID = "MIC10",
            //                            DetailCalculationID = "MICD10",
            //                            PriceCalculationID = "MICP10",
            //                                    DetailCalculation = "=SprayBodySpacingInInches",
            //                                    Order = 3}
            //                            },
            //                            Order = 4,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Ft. Commercial Rubber Hose & Power Cable",
            //                            DetailCalculation = "=ROUNDUP( BayLengthInFeet * 1.2, 0 )",
            //                            Order = 5,
            //                            ComponentID = "MAC5",
            //                            DetailCalculationID = "MACD5",
            //                            PriceCalculationID = "MACP5",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    Name="[inch] Commercial Grade Rubber Hose; 200psi",
            //                                    DetailCalculation = "=HoseSize",
            //                                    ComponentID = "MIC11",
            //                            DetailCalculationID = "MICD11",
            //                            PriceCalculationID = "MICP11",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {ComponentID = "MIC12",
            //                            DetailCalculationID = "MICD12",
            //                            PriceCalculationID = "MICP12", Name="16/3 300V Water Resistant Electrical Power Cable", Order= 2},
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="CCS Wrap Around Hose Trolleys - 1 5/8\"",
            //                            DetailCalculation = "=ROUNDUP( (BayLengthInFeet * 1.2) / 10, 0 ) + 1",
            //                            Order = 6,
            //                            ComponentID = "MAC6",
            //                            DetailCalculationID = "MACD6",
            //                            PriceCalculationID = "MACP6",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {ComponentID = "MIC13",
            //                            DetailCalculationID = "MICD13",
            //                            PriceCalculationID = "MICP13",
            //                                    Name="[inch] Hose Loop Height",
            //                                    DetailCalculation = "=HoseLoopHeightInInches",
            //                                    Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Brackets for Rail",
            //                            Order = 7,
            //                            ComponentID = "MAC7",
            //                            DetailCalculationID = "MACD7",
            //                            PriceCalculationID = "MACP7",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC14",
            //                            DetailCalculationID = "MICD14",
            //                            PriceCalculationID = "MICP14",
            //                                    Name="EZ Rail Support Brackets",
            //                                    Order = 1,
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet / TrussSpacingInFeet, 0 ) + 1",
            //                                    MinorComponentDetails =  new List<MinorComponentDetail>
            //                                    {
            //                                       new MinorComponentDetail { Name="Mounting brackets & hardware", Order=1}}
            //                                    },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC15",
            //                            DetailCalculationID = "MICD15",
            //                            PriceCalculationID = "MICP15",
            //                                    Name="Rail Sleeves for Support Pipe; 12\" long",
            //                                    Order = 2,
            //                                    DetailCalculation="=ROUNDDOWN( BayLengthInFeet / 21, 0 )"
            //                                }
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Controller Type",
            //                            Order = 8,
            //                            ComponentID = "MAC8",
            //                            DetailCalculationID = "MACD8",
            //                            PriceCalculationID = "MACP8",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC16",
            //                            DetailCalculationID = "MICD16",
            //                            PriceCalculationID = "MICP16",
            //                                    Name="Network Irrigation Controller Upgrade",
            //                                    Order = 1,
            //                                    MinorComponentDetails =  new List<MinorComponentDetail>
            //                                    {
            //                                       new MinorComponentDetail { Name="NIC w/ Magnet Jobbing (A28 chip)", Order=1}}
            //                                    }
            //                            },
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail
            //                                {
            //                                    Name="* Includes: 12 Magnets per system",
            //                                    Order=1
            //                                }
            //                            }
            //                        },
            //                },
            //                },
            //                new Category
            //                {
            //                    Name="Options and Upgrades",
            //                    Order = 2,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            Name="1\" Amiad In-Line Filter Assembly",
            //                            Order = 1,
            //                            ComponentID = "MAC9",
            //                            DetailCalculationID = "MACD9",
            //                            PriceCalculationID = "MACP9",
            //                            AddPriceCondition = "=AMIADFilterASSYRequested = true",
            //                            PriceCalculation = "=60.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Dual Water/Mist Upgrade",
            //                            Order = 2,
            //                            ComponentID = "MAC10",
            //                            DetailCalculationID = "MACD10",
            //                            PriceCalculationID = "MACP10",
            //                            AddPriceCondition = @"=SingleOrDualWater = ""Dual""",
            //                            PriceCalculation = "=ROUNDUP((11 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2) + 12.5 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet XR11002 (0.2 gpm) 'Yellow' Tips", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Step / Stop / Water Spray Bar Upgrade",
            //                            Order = 3,
            //                            ComponentID = "MAC11",
            //                            DetailCalculationID = "MACD11",
            //                            PriceCalculationID = "MACP11",
            //                            AddPriceCondition = "=TripleStepLocklineUpgradeRequested = true",
            //                            PriceCalculation = "=ROUNDUP(22.5 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet Bodies w/ Flexible Lock-Line assembly", Order = 1},
            //                                new MajorComponentDetail {Name="(including individual needle valves for calibrating)", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Triple Turret Spray Body Upgrade",
            //                            Order = 4,
            //                            ComponentID = "MAC12",
            //                            DetailCalculationID = "MACD12",
            //                            PriceCalculationID = "MACP12",
            //                            AddPriceCondition = "=TripleTurretBodyRequested = true",
            //                            PriceCalculation = "=ROUNDUP((20 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 )), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="1\" Pressure Regulator & Gauge",
            //                            Order = 5,
            //                            ComponentID = "MAC13",
            //                            DetailCalculationID = "MACD13",
            //                            PriceCalculationID = "MACP13",
            //                            AddPriceCondition = "=PressureRegulatorRequested = true",
            //                            PriceCalculation = "=100.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            Name="Mounted Injector w/ Custom Manifold",
            //                            Order = 6,
            //                            AddPriceCondition = "=MountedInjectorRequested = true",
            //                            PriceCalculation = "=585.00",
            //                            ComponentID = "MAC14",
            //                            DetailCalculationID = "MACD14",
            //                            PriceCalculationID = "MACP14",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* Customer to determine Injector Size; prices may vary", Order = 1},
            //                                new MajorComponentDetail {Name="** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC15",
            //                            DetailCalculationID = "MACD15",
            //                            PriceCalculationID = "MACP15",
            //                            Name="90 Degree Hose Sweep Assembly",
            //                            Order = 7,
            //                            AddPriceCondition = "=Sweep90Requested = true",
            //                            PriceCalculation = "=165.00"
            //                        },
            //                    }
            //                }
            //                }
            //        });

            //#endregion SRB System

            //#region DRB System

            //context.BoomSystems.AddOrUpdate(s => s.Name,
            //        new BoomSystem
            //        {
            //            Name = "Grower Series - Double Rail Boom",
            //            BoomTypeId = 3,
            //            ComponentID = "SYS",
            //            DetailCalculationID = "SYSD",
            //            PriceCalculationID = "SYSP",
            //            DescriptionCalculationID = "SYST",
            //            PriceCalculation = "ROUNDUP"
            //                               + "(BaseValue + (BayLengthInFeet / 10 * 39.2 + BayWidthInFeet * 12 / SprayBodySpacingInInches * 7) * 1.1 \r\n"
            //                               + ", 0) * 1.175 + 750 \r\n",
            //            Categories = new List<Category>
            //            {
            //                new Category
            //                {
            //                    Name="General Parts",
            //                    Order = 1,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            Name="Frame Unit w/ 24\" Center Walkway spacing",
            //                            Order = 1,
            //                            ComponentID = "MAC1",
            //                            DetailCalculationID = "MACD1",
            //                            PriceCalculationID = "MACP1",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC1",
            //                            DetailCalculationID = "MICD1",
            //                            PriceCalculationID = "MICP1",Name="Baldor, 1/4 HP variable speed DC motor w/chain drive", Order = 1,
            //                                  MinorComponentDetails =  new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail { Name="(11:1 gear ratio, 90VDC, 2.5 Amps)", Order=1}}
            //                                    },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC2",
            //                            DetailCalculationID = "MICD2",
            //                            PriceCalculationID = "MICP2",Name="4 Wheel Drive design w/ 38\" axles", Order = 2,
            //                                MinorComponentDetails = new List<MinorComponentDetail>
            //                                {
            //                                     new MinorComponentDetail {Name="(36\" wheel spacing; center to center)", Order=1}
            //                                }},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC3",
            //                            DetailCalculationID = "MICD3",
            //                            PriceCalculationID = "MICP3",Name="5th Wheel Idle assembly (Motion, Speed and Direction)", Order = 3},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC4",
            //                            DetailCalculationID = "MICD4",
            //                            PriceCalculationID = "MICP4",Name="Adjustable Telescopic spray bar supports", Order = 4,
            //                                  MinorComponentDetails = new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail {Name="(with 1/8\" aircraft cable and remote pipe supports)", Order=1}
            //                                  }}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC2",
            //                            DetailCalculationID = "MACD2",
            //                            PriceCalculationID = "MACP2",
            //                            Name="1\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *",
            //                            Order = 2,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC5",
            //                            DetailCalculationID = "MICD5",
            //                            PriceCalculationID = "MICP5",Name="1\" Rainbird Solenoid Valves", Order = 1},
            //                                new MinorComponent
            //                                { ComponentID = "MIC6",
            //                            DetailCalculationID = "MICD6",
            //                            PriceCalculationID = "MICP6",Name="1\" Brass Ball Valve", Order = 2},
            //                                new MinorComponent
            //                                {ComponentID = "MIC7",
            //                            DetailCalculationID = "MICD7",
            //                            PriceCalculationID = "MICP7", Name="1\" Amiad In-Line Filter", Order = 3}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC3",
            //                            DetailCalculationID = "MACD3",
            //                            PriceCalculationID = "MACP3",
            //                            Name="Whip Hose Hand-Watering Assy.",
            //                            Order = 3,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC8",
            //                            DetailCalculationID = "MICD8",
            //                            PriceCalculationID = "MICP8",Name="3/4\" Commercial Grade Hose", Order = 1,
            //                                MinorComponentDetails = new List<MinorComponentDetail>
            //                                {
            //                                    new MinorComponentDetail {Name="24\" Dramm Wand Assy. w/ Brass Ball Valve", Order=1}
            //                                }}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC4",
            //                            DetailCalculationID = "MACD4",
            //                            PriceCalculationID = "MACP4",
            //                            Name="Single Water Bar Setup",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC9",
            //                            DetailCalculationID = "MICD9",
            //                            PriceCalculationID = "MICP9",
            //                                    Name="[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC10",
            //                            DetailCalculationID = "MICD10",
            //                            PriceCalculationID = "MICP10",
            //                                    Name="[inch] TeeJet Spray Bodies w/ Tips & Caps",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 2,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail {Name="XR11008 White (0.8 gpm) Tips - unless otherwise specified by customer", Order = 1}
            //                                    }},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC11",
            //                            DetailCalculationID = "MICD11",
            //                            PriceCalculationID = "MICP11",
            //                                    Name="[inch] Spray Body/Tip Spacing (average)",
            //                                    DetailCalculation = "=SprayBodySpacingInInches",
            //                                    Order = 3}
            //                            },
            //                            Order = 4,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC5",
            //                            DetailCalculationID = "MACD5",
            //                            PriceCalculationID = "MACP5",
            //                            Name="Ft. Commercial Rubber Hose & Power Cable",
            //                            DetailCalculation = "=ROUNDUP( BayLengthInFeet * 1.2, 0 )",
            //                            Order = 5,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC12",
            //                            DetailCalculationID = "MICD12",
            //                            PriceCalculationID = "MICP12",
            //                                    Name="[inch] Commercial Grade Rubber Hose; 200psi",
            //                                    DetailCalculation = "=HoseSize",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {ComponentID = "MIC13",
            //                            DetailCalculationID = "MICD13",
            //                            PriceCalculationID = "MICP13", Name="16/3 300V Water Resistant Electrical Power Cable", Order= 2},
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC6",
            //                            DetailCalculationID = "MACD6",
            //                            PriceCalculationID = "MACP6",
            //                            Name="CCS Wrap Around Hose Trolleys - 1 5/8\"",
            //                            DetailCalculation = "ROUNDUP( (BayLengthInFeet * 1.2) / 10, 0 ) + 1",
            //                            Order = 6,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC14",
            //                            DetailCalculationID = "MICD14",
            //                            PriceCalculationID = "MICP14",
            //                                    Name="[inch] Hose Loop Height",
            //                                    DetailCalculation = "=HoseLoopHeightInInches",
            //                                    Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC7",
            //                            DetailCalculationID = "MACD7",
            //                            PriceCalculationID = "MACP7",
            //                            Name="Brackets for Rail",
            //                            Order = 7,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC15",
            //                            DetailCalculationID = "MICD15",
            //                            PriceCalculationID = "MICP15",
            //                                    Name="Bull-Horn Rail Support Brackets",
            //                                    Order = 1,
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet / TrussSpacingInFeet, 0 ) + 1",
            //                                  MinorComponentDetails =  new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail { Name="Mounting brackets & hardware", Order=1}}
            //                                    },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC16",
            //                            DetailCalculationID = "MICD16",
            //                            PriceCalculationID = "MICP16",
            //                                    Name="Rail Sleeves for Support Pipe; 12\" long",
            //                                    Order = 2,
            //                                    DetailCalculation="=ROUNDDOWN( BayLengthInFeet / 21, 0 ) * 2"
            //                                }
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC8",
            //                            DetailCalculationID = "MACD8",
            //                            PriceCalculationID = "MACP8",
            //                            Name="Standard Controller Type",
            //                            Order = 8,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC17",
            //                            DetailCalculationID = "MICD17",
            //                            PriceCalculationID = "MICP17",
            //                                    Name="Network Irrigation Controller",
            //                                    Order = 1,
            //                                    MinorComponentDetails =  new List<MinorComponentDetail>
            //                                    {
            //                                       new MinorComponentDetail { Name="NIC w/ Magnet Jobbing (A28 chip)", Order=1}}
            //                                    }
            //                            },
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail
            //                                {
            //                                    Name="* Includes: 12 Magnets per system",
            //                                    Order=1
            //                                }
            //                            }
            //                        },
            //                },
            //                },
            //                new Category
            //                {
            //                    Name="Options and Upgrades",
            //                    Order = 2,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC9",
            //                            DetailCalculationID = "MACD9",
            //                            PriceCalculationID = "MACP9",
            //                            Name="Compass Capture Controller Upgrade",
            //                            Order = 1,
            //                            AddPriceCondition = @"=ControllerType = ""WIC""",
            //                            PriceCalculation = "=350.00",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC18",
            //                            DetailCalculationID = "MICD18",
            //                            PriceCalculationID = "MICP18",Name="WIC w/ Area Capture Program and Step/Stop/Water", Order= 1}
            //                            },
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* Includes: 2 Magnets per system", Order = 1},
            //                                new MajorComponentDetail {Name="* Includes: Whisker Switch for Checkpoints", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC10",
            //                            DetailCalculationID = "MACD10",
            //                            PriceCalculationID = "MACP10",
            //                            Name="Wireless Walk Switch - Easily Move Boom in Bay",
            //                            Order = 2,
            //                            AddPriceCondition = "=WirelessWalkSwitchRequested = true",
            //                            PriceCalculation = "=285.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC11",
            //                            DetailCalculationID = "MACD11",
            //                            PriceCalculationID = "MACP11",
            //                            Name="Dual Water/Mist Upgrade",
            //                            Order = 3,
            //                            AddPriceCondition = @"=SingleOrDualWater = ""Dual""",
            //                            PriceCalculation = "=ROUNDUP((11 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2) + 25 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet XR11002 (0.2 gpm) 'Yellow' Tips", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC12",
            //                            DetailCalculationID = "MACD12",
            //                            PriceCalculationID = "MACP12",
            //                            Name="Step / Stop / Water Spray Bar Upgrade",
            //                            Order = 4,
            //                            AddPriceCondition = "=TripleStepLocklineUpgradeRequested = true",
            //                            PriceCalculation = "=ROUNDUP(22.5 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet Bodies w/ Flexible Lock-Line assembly", Order = 1},
            //                                new MajorComponentDetail {Name="(including individual needle valves for calibrating)", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC13",
            //                            DetailCalculationID = "MACD13",
            //                            PriceCalculationID = "MACP13",
            //                            Name="Triple Turret Spray Body Upgrade",
            //                            Order = 5,
            //                            AddPriceCondition = "=TripleTurretBodyRequested = true",
            //                            PriceCalculation = "=ROUNDUP((20 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 )), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC14",
            //                            DetailCalculationID = "MACD14",
            //                            PriceCalculationID = "MACP14",
            //                            Name="1\" Pressure Regulator & Gauge",
            //                            Order = 6,
            //                            AddPriceCondition = "=PressureRegulatorRequested = true",
            //                            PriceCalculation = "=100.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC15",
            //                            DetailCalculationID = "MACD15",
            //                            PriceCalculationID = "MACP15",
            //                            Name="Mounted Injector w/ Custom Manifold",
            //                            Order = 7,
            //                            AddPriceCondition = "=MountedInjectorRequested = true",
            //                            PriceCalculation = "=595.00",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* Customer to determine Injector Size; prices may vary", Order = 1},
            //                                new MajorComponentDetail {Name="** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC16",
            //                            DetailCalculationID = "MACD16",
            //                            PriceCalculationID = "MACP16",
            //                            Name="1 1/4\" Hose Upgrade (bays over 400')",
            //                            Order = 8,
            //                            AddPriceCondition = "=HoseUpgradeRequested = true",
            //                            PriceCalculation = "=MACD5 * 2.25"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC17",
            //                            DetailCalculationID = "MACD17",
            //                            PriceCalculationID = "MACP17",
            //                            Name="90 Degree Hose Sweep Assembly",
            //                            Order = 9,
            //                            AddPriceCondition = "=Sweep90Requested = true",
            //                            PriceCalculation = "=165.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC18",
            //                            DetailCalculationID = "MACD18",
            //                            PriceCalculationID = "MACP18",
            //                            Name="[inch] Rail Drop Down Assembly",
            //                            Order = 10,
            //                            DetailCalculation = "=RailDropDownASSYInInches",
            //                            AddPriceCondition = "true",
            //                            PriceCalculation = "=(MICD15*2)*(RailDropDownASSYInInches/12)*2"
            //                        }
            //                    }
            //                }
            //                }
            //        });

            //#endregion DRB System

            //#region CWF - DRB System

            //context.BoomSystems.AddOrUpdate(s => s.Name,
            //        new BoomSystem
            //        {
            //            Name = "Grower Series - Center Water Feed DRB",
            //            BoomTypeId = 4,
            //            ComponentID = "SYS",
            //            DetailCalculationID = "SYSD",
            //            PriceCalculationID = "SYSP",
            //            DescriptionCalculationID = "SYST",
            //            PriceCalculation = "ROUNDUP"
            //                               + "(BaseValue+(BayLengthInFeet/10*39.2+BayWidthInFeet*12/SprayBodySpacingInInches*7) \r\n"
            //                               + "*1.1+(500) \r\n"
            //                               + ",0) *1.175+750 \r\n",
            //            Categories = new List<Category>
            //            {
            //                new Category
            //                {
            //                    Name="General Parts",
            //                    Order = 1,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC1",
            //                            DetailCalculationID = "MACD1",
            //                            PriceCalculationID = "MACP1",
            //                            Name="Frame Unit w/ 24\" Center Walkway spacing",
            //                            Order = 1,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {ComponentID = "MIC1",
            //                            DetailCalculationID = "MICD1",
            //                            PriceCalculationID = "MICP1", Name="Baldor, 1/4 HP variable speed DC motor w/chain drive", Order = 1,
            //                                  MinorComponentDetails =  new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail { Name="(11:1 gear ratio, 90VDC, 2.5 Amps)", Order=1}}
            //                                    },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC2",
            //                            DetailCalculationID = "MICD2",
            //                            PriceCalculationID = "MICP2",Name="4 Wheel Drive design w/ 38\" axles", Order = 2,
            //                                MinorComponentDetails = new List<MinorComponentDetail>
            //                                {
            //                                     new MinorComponentDetail {Name="(36\" wheel spacing; center to center)", Order=1}
            //                                }},
            //                                new MinorComponent
            //                                { ComponentID = "MIC3",
            //                            DetailCalculationID = "MICD3",
            //                            PriceCalculationID = "MICP3",Name="5th Wheel Idle assembly (Motion, Speed and Direction)", Order = 3},
            //                                new MinorComponent
            //                                { ComponentID = "MIC4",
            //                            DetailCalculationID = "MICD4",
            //                            PriceCalculationID = "MICP4",
            //                            Name="Adjustable Telescopic spray bar supports", Order = 4,
            //                                  MinorComponentDetails = new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail {Name="(with 1/8\" aircraft cable and remote pipe supports)", Order=1}
            //                                  }}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC2",
            //                            DetailCalculationID = "MACD2",
            //                            PriceCalculationID = "MACP2",
            //                            Name="1\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *",
            //                            Order = 2,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC5",
            //                            DetailCalculationID = "MICD5",
            //                            PriceCalculationID = "MICP5",Name="1\" Rainbird Solenoid Valves", Order = 1},
            //                                new MinorComponent
            //                                { ComponentID = "MIC6",
            //                            DetailCalculationID = "MICD6",
            //                            PriceCalculationID = "MICP6",Name="1\" Brass Ball Valve", Order = 2},
            //                                new MinorComponent
            //                                { ComponentID = "MIC7",
            //                            DetailCalculationID = "MICD7",
            //                            PriceCalculationID = "MICP7",Name="1\" Amiad In-Line Filter", Order = 3}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC3",
            //                            DetailCalculationID = "MACD3",
            //                            PriceCalculationID = "MACP3",
            //                            Name="Whip Hose Hand-Watering Assy.",
            //                            Order = 3,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {ComponentID = "MIC8",
            //                            DetailCalculationID = "MICD8",
            //                            PriceCalculationID = "MICP8", Name="3/4\" Commercial Grade Hose", Order = 1,
            //                                MinorComponentDetails = new List<MinorComponentDetail>
            //                                {
            //                                    new MinorComponentDetail {Name="24\" Dramm Wand Assy. w/ Brass Ball Valve", Order=1}
            //                                }}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC4",
            //                            DetailCalculationID = "MACD4",
            //                            PriceCalculationID = "MACP4",
            //                            Name="Single Water Bar Setup",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC9",
            //                            DetailCalculationID = "MICD9",
            //                            PriceCalculationID = "MICP9",
            //                                    Name="[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC10",
            //                            DetailCalculationID = "MICD10",
            //                            PriceCalculationID = "MICP10",
            //                                    Name="[inch] TeeJet Spray Bodies w/ Tips & Caps",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 2,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail {Name="XR11008 White (0.8 gpm) Tips - unless otherwise specified by customer", Order = 1}
            //                                    }},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC11",
            //                            DetailCalculationID = "MICD11",
            //                            PriceCalculationID = "MICP11",
            //                                    Name="[inch] Spray Body/Tip Spacing (average)",
            //                                    DetailCalculation = "=SprayBodySpacingInInches",
            //                                    Order = 3}
            //                            },
            //                            Order = 4,
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC5",
            //                            DetailCalculationID = "MACD5",
            //                            PriceCalculationID = "MACP5",
            //                            Name="Center Water Feed System",
            //                            Order = 5,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC12",
            //                            DetailCalculationID = "MICD12",
            //                            PriceCalculationID = "MICP12",
            //                                    Name="Ft. Commercial Poly Pipe & Power Cable",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet * 0.5 + 10, 0 )",
            //                                    Order = 1,
            //                                },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC13",
            //                            DetailCalculationID = "MICD13",
            //                            PriceCalculationID = "MICP13",
            //                                    Name="[inch] 125psi UV Resistant Commercial Grade Poly Pipe (UltraBlue)",
            //                                    DetailCalculation = "=HoseSize",
            //                                    Order = 2},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC14",
            //                            DetailCalculationID = "MICD14",
            //                            PriceCalculationID = "MICP14",
            //                                    Name="[feet] 16/3 300V Water Resistant Electrical Power Cable",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet * 0.5 + 20, 0 )",
            //                                    Order= 3,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail { Name="Water Tight Plumbing / Electrical Assy.", Order = 1}
            //                                    }},
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC6",
            //                            DetailCalculationID = "MACD6",
            //                            PriceCalculationID = "MACP6",
            //                            Name="Center Water Feed Cart",
            //                            Order = 6,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC15",
            //                            DetailCalculationID = "MICD15",
            //                            PriceCalculationID = "MICP15",
            //                                    Name="[feet] 1/8\" Galvanized Aircraft Cable",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet * 2 + 10, 0)",
            //                                    Order = 1,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail { Name="1/8\" Aircraft Cable - 2:1 Run - Cable & Pulley Assy.", Order = 1}
            //                                    }
            //                                }
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC7",
            //                            DetailCalculationID = "MACD7",
            //                            PriceCalculationID = "MACP7",
            //                            Name="Brackets for Rail",
            //                            Order = 7,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC16",
            //                            DetailCalculationID = "MICD16",
            //                            PriceCalculationID = "MICP16",
            //                                    Name="Bull-Horn Rail Support Brackets",
            //                                    Order = 1,
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet / TrussSpacingInFeet, 0 ) + 1",
            //                                  MinorComponentDetails =  new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail { Name="Mounting brackets & hardware", Order=1}}
            //                                    },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC17",
            //                            DetailCalculationID = "MICD17",
            //                            PriceCalculationID = "MICP17",
            //                                    Name="Start & End Cable Pulley brackets",
            //                                    Order = 2,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail {Name = "Includes Center Nipple Feed bracket", Order = 1}
            //                                    }
            //                                },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC18",
            //                            DetailCalculationID = "MICD18",
            //                            PriceCalculationID = "MICP18",
            //                                    Name="Rail Sleeves for Support Pipe",
            //                                    Order = 3,
            //                                    DetailCalculation=@"=ROUNDDOWN((BayLengthInFeet/(IF( BottomCordType = ""SQ"",24,IF( BottomCordType = ""RD"",21,""Error"")))-1),0)*2"
            //                                }
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC8",
            //                            DetailCalculationID = "MACD8",
            //                            PriceCalculationID = "MACP8",
            //                            Name="Standard Controller Type",
            //                            Order = 8,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC19",
            //                            DetailCalculationID = "MICD19",
            //                            PriceCalculationID = "MICP19",
            //                                    Name="Network Irrigation Controller",
            //                                    Order = 1,
            //                                    MinorComponentDetails =  new List<MinorComponentDetail>
            //                                    {
            //                                       new MinorComponentDetail { Name="NIC w/ Magnet Jobbing (A28 chip)", Order=1}}
            //                                    }
            //                            },
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail
            //                                {
            //                                    Name="* Includes: 12 Magnets per system",
            //                                    Order=1
            //                                }
            //                            }
            //                        },
            //                },
            //                },
            //                new Category
            //                {
            //                    Name="Options and Upgrades",
            //                    Order = 2,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC9",
            //                            DetailCalculationID = "MACD9",
            //                            PriceCalculationID = "MACP9",
            //                            Name="Compass Capture Controller Upgrade",
            //                            Order = 1,
            //                            AddPriceCondition = @"=ControllerType = ""WIC""",
            //                            PriceCalculation = "350.00",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC20",
            //                            DetailCalculationID = "MICD20",
            //                            PriceCalculationID = "MICP20",Name="WIC w/ Area Capture Program and Step/Stop/Water", Order= 1}
            //                            },
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* Includes: 2 Magnets per system", Order = 1},
            //                                new MajorComponentDetail {Name="* Includes: Whisker Switch for Checkpoints", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC10",
            //                            DetailCalculationID = "MACD10",
            //                            PriceCalculationID = "MACP10",
            //                            Name="Wireless Walk Switch - Easily Move Boom in Bay",
            //                            Order = 2,
            //                            AddPriceCondition = "=WirelessWalkSwitchRequested = true",
            //                            PriceCalculation = "285.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC11",
            //                            DetailCalculationID = "MACD11",
            //                            PriceCalculationID = "MACP11",
            //                            Name="Dual Water/Mist Upgrade",
            //                            Order = 3,
            //                            AddPriceCondition = @"=SingleOrDualWater = ""Dual""",
            //                            PriceCalculation = "=ROUNDUP((11 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2) + 25 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet XR11002 (0.2 gpm) 'Yellow' Tips", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC12",
            //                            DetailCalculationID = "MACD12",
            //                            PriceCalculationID = "MACP12",
            //                            Name="Step / Stop / Water Spray Bar Upgrade",
            //                            Order = 4,
            //                            AddPriceCondition = "=TripleStepLocklineUpgradeRequested = true",
            //                            PriceCalculation = "=ROUNDUP(22.5 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet Bodies w/ Flexible Lock-Line assembly", Order = 1},
            //                                new MajorComponentDetail {Name="(including individual needle valves for calibrating)", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC13",
            //                            DetailCalculationID = "MACD13",
            //                            PriceCalculationID = "MACP13",
            //                            Name="Triple Turret Spray Body Upgrade",
            //                            Order = 5,
            //                            AddPriceCondition = "=TripleTurretBodyRequested = true",
            //                            PriceCalculation = "=ROUNDUP((20 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 )), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC14",
            //                            DetailCalculationID = "MACD14",
            //                            PriceCalculationID = "MACP14",
            //                            Name="1\" Pressure Regulator & Gauge",
            //                            Order = 6,
            //                            AddPriceCondition = "=PressureRegulatorRequested = true",
            //                            PriceCalculation = "100.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC15",
            //                            DetailCalculationID = "MACD15",
            //                            PriceCalculationID = "MACP15",
            //                            Name="Mounted Injector w/ Custom Manifold",
            //                            Order = 7,
            //                            AddPriceCondition = "=MountedInjectorRequested = true",
            //                            PriceCalculation = "595.00",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* Customer to determine Injector Size; prices may vary", Order = 1},
            //                                new MajorComponentDetail {Name="** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC16",
            //                            DetailCalculationID = "MACD16",
            //                            PriceCalculationID = "MACP16",
            //                            Name="[inch] Rail Drop Down Assembly",
            //                            Order = 8,
            //                            DetailCalculation = "=RailDropDownASSYInInches",
            //                            AddPriceCondition = "true",
            //                            PriceCalculation = "=(MICD16*2)*(RailDropDownASSYInInches/12)*2"
            //                        }
            //                    }
            //                }
            //                }
            //        });

            //#endregion CWF - DRB System

            //#region TB System

            //context.BoomSystems.AddOrUpdate(s => s.Name,
            //     new BoomSystem
            //     {
            //         Name = "Pro Series - Control Tower Boom",
            //         BoomTypeId = 5,
            //         ComponentID = "SYS",
            //         DetailCalculationID = "SYSD",
            //         PriceCalculationID = "SYSP",
            //         DescriptionCalculationID = "SYST",
            //         PriceCalculation = "ROUNDUP("
            //                            + "BaseValue * BayWidthInFeet + (BayLengthInFeet / 10 * 39.2) + (BayWidthInFeet * 12 / SprayBodySpacingInInches * 7) \r\n"
            //                            + "+ (ROUNDUP(BayWidthInFeet / 20, 0) * 9.8 * BayWidthInFeet) * 1.1 \r\n"
            //                            + ", 0) * 1.175 + 750 \r\n",
            //         Categories = new List<Category>
            //         {
            //                new Category
            //                {
            //                    Name="General Parts",
            //                    Order = 1,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC1",
            //                            DetailCalculationID = "MACD1",
            //                            PriceCalculationID = "MACP1",
            //                            Name="Frame Unit - Bay Spanning Design - Rails Mounted to Posts",
            //                            Order = 1,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC1",
            //                            DetailCalculationID = "MICD1",
            //                            PriceCalculationID = "MICP1",Name="Baldor Super E 1/2 HP, Variable Speed 3 Phase Inverter Duty AC motor", Order = 1,
            //                                  MinorComponentDetails =  new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail { Name="(20:1 gear ratio, 230VAC output, 5.25 Amps each)", Order=1}}
            //                                    },
            //                                new MinorComponent
            //                                {ComponentID = "MIC2",
            //                            DetailCalculationID = "MICD2",
            //                            PriceCalculationID = "MICP2", Name="6\" Rubber Molded Cast Iron Wheels w/ Guide Bearings", Order = 2},
            //                                new MinorComponent
            //                                {ComponentID = "MIC3",
            //                            DetailCalculationID = "MICD3",
            //                            PriceCalculationID = "MICP3", Name="3/4\" Chain Driven Axle w/ Pillow Block Bearings", Order = 3},
            //                                new MinorComponent
            //                                { ComponentID = "MIC4",
            //                            DetailCalculationID = "MICD4",
            //                            PriceCalculationID = "MICP4",Name="Adjustable Telescopic spray bar support w/ shaft collar lock", Order = 4}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC2",
            //                            DetailCalculationID = "MACD2",
            //                            PriceCalculationID = "MACP2",
            //                            Name="1\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *",
            //                            Order = 2,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                { ComponentID = "MIC5",
            //                            DetailCalculationID = "MICD5",
            //                            PriceCalculationID = "MICP5",Name="1\" Rainbird Solenoid Valves", Order = 1},
            //                                new MinorComponent
            //                                {ComponentID = "MIC6",
            //                            DetailCalculationID = "MICD6",
            //                            PriceCalculationID = "MICP6", Name="1\" Brass Ball Valve", Order = 2},
            //                                new MinorComponent
            //                                { ComponentID = "MIC7",
            //                            DetailCalculationID = "MICD7",
            //                            PriceCalculationID = "MICP7",Name="1\" Amiad In-Line Filter", Order = 3}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC3",
            //                            DetailCalculationID = "MACD3",
            //                            PriceCalculationID = "MACP3",
            //                            Name="Whip Hose Hand-Watering Assy.",
            //                            Order = 3,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {ComponentID = "MIC8",
            //                            DetailCalculationID = "MICD8",
            //                            PriceCalculationID = "MICP8", Name="3/4\" Commercial Grade Hose", Order = 1,
            //                                MinorComponentDetails = new List<MinorComponentDetail>
            //                                {
            //                                    new MinorComponentDetail {Name="24\" Dramm Wand Assy. w/ Brass Ball Valve", Order=1}
            //                                }}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC4",
            //                            DetailCalculationID = "MACD4",
            //                            PriceCalculationID = "MACP4",
            //                            Name="Single Water Bar Setup",
            //                            Order = 4,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC9",
            //                            DetailCalculationID = "MICD9",
            //                            PriceCalculationID = "MICP9",
            //                                    Name="[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC10",
            //                            DetailCalculationID = "MICD10",
            //                            PriceCalculationID = "MICP10",
            //                                    Name="[inch] TeeJet Spray Bodies w/ Tips & Caps",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 2,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail {Name="XR11008 White (0.8 gpm) Tips - unless otherwise specified by customer", Order = 1}
            //                                    }},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC11",
            //                            DetailCalculationID = "MICD11",
            //                            PriceCalculationID = "MICP11",
            //                                    Name="[inch] Spray Body/Tip Spacing (average)",
            //                                    DetailCalculation = "=SprayBodySpacingInInches",
            //                                    Order = 3}
            //                            },
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC5",
            //                            DetailCalculationID = "MACD5",
            //                            PriceCalculationID = "MACP5",
            //                            Name="Ft. Commercial Rubber Hose & Power Cable",
            //                            DetailCalculation = "=ROUNDUP( BayLengthInFeet * 1.2, 0 )",
            //                            ActivateComponentCondition = @"=CenterWaterFeedSystemRequested = false",
            //                            Order = 5,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC12",
            //                            DetailCalculationID = "MICD12",
            //                            PriceCalculationID = "MICP12",
            //                                    Name="[inch] Commercial Grade Rubber Hose; 200psi",
            //                                    DetailCalculation = "=HoseSize",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC13",
            //                            DetailCalculationID = "MICD13",
            //                            PriceCalculationID = "MICP13",
            //                            Name="12/3 300V Water Resistant Electrical Power Cable", Order= 2},
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC24",
            //                            DetailCalculationID = "MACD24",
            //                            PriceCalculationID = "MACP24",
            //                            ActivateComponentCondition = @"=CenterWaterFeedSystemRequested = true",
            //                            Name="Center Water Feed System",
            //                            Order = 5,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                            ComponentID = "MIC20",
            //                            DetailCalculationID = "MICD20",
            //                            PriceCalculationID = "MICP20",
            //                                    Name="Ft. Commercial Poly Pipe & Power Cable",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet * 0.5 + 10, 0 )",
            //                                    Order = 1,
            //                                },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC21",
            //                            DetailCalculationID = "MICD21",
            //                            PriceCalculationID = "MICP21",
            //                                    Name="[inch] 125psi UV Resistant Commercial Grade Poly Pipe (UltraBlue)",
            //                                    DetailCalculation = "=HoseSize",
            //                                    Order = 2},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC22",
            //                            DetailCalculationID = "MICD22",
            //                            PriceCalculationID = "MICP22",
            //                                    Name="[feet] 16/3 300V Water Resistant Electrical Power Cable",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet * 0.5 + 20, 0 )",
            //                                    Order= 3,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail { Name="Water Tight Plumbing / Electrical Assy.", Order = 1}
            //                                    }},
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC6",
            //                            DetailCalculationID = "MACD6",
            //                            PriceCalculationID = "MACP6",
            //                            Name="CCS Diamond Hose Trolleys - 2\" x 2\" Square",
            //                            DetailCalculation = "ROUNDUP( (BayLengthInFeet * 1.2) / 10, 0 ) + 1",
            //                            Order = 6,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC14",
            //                            DetailCalculationID = "MICD14",
            //                            PriceCalculationID = "MICP14",
            //                                    Name="[inch] Hose Loop Height",
            //                                    DetailCalculation = "=HoseLoopHeightInInches",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC15",
            //                            DetailCalculationID = "MICD15",
            //                            PriceCalculationID = "MICP15",
            //                                    Name="CCS Hose Pusher bracket",
            //                                    DetailCalculation = "=1",
            //                                    Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC7",
            //                            DetailCalculationID = "MACD7",
            //                            PriceCalculationID = "MACP7",
            //                            Name="Brackets for Rail",
            //                            Order = 7,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC16",
            //                            DetailCalculationID = "MICD16",
            //                            PriceCalculationID = "MICP16",
            //                                    Name="[pair] Tower Boom Rail Mount Brackets and hardware",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet / PostSpacingInFeet, 0 ) + 1",
            //                                    Order = 1,
            //                                },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC17",
            //                            DetailCalculationID = "MICD17",
            //                            PriceCalculationID = "MICP17",
            //                                    Name="Single & Double sided brackets for 2\" x 4\" Rails",
            //                                    Order = 2,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail {Name = "(Amounts TBD by Greenhouse Design)", Order = 1}
            //                                    }
            //                                }
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC8",
            //                            DetailCalculationID = "MACD8",
            //                            PriceCalculationID = "MACP8",
            //                            Name="Standard Controller Type",
            //                            Order = 8,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC18",
            //                            DetailCalculationID = "MICD18",
            //                            PriceCalculationID = "MICP18",
            //                                    Name="Task Manager Controller",
            //                                    Order = 1,
            //                                    MinorComponentDetails =  new List<MinorComponentDetail>
            //                                    {
            //                                       new MinorComponentDetail { Name="WIC w/ Jobbing Program using Magnets", Order=1}}
            //                                    }
            //                            },
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail
            //                                {
            //                                    Name="* Includes: 12 Magnets per system",
            //                                    Order=1
            //                                }
            //                            }
            //                        },
            //                },
            //                },
            //                new Category
            //                {
            //                    Name="Options and Upgrades",
            //                    Order = 2,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC9",
            //                            DetailCalculationID = "MACD9",
            //                            PriceCalculationID = "MACP9",
            //                            Name="Compass Capture Controller Upgrade",
            //                            Order = 1,
            //                            AddPriceCondition = @"=""ControllerType"" = ""WIC""",
            //                            PriceCalculation = "350.00",
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC19",
            //                            DetailCalculationID = "MICD19",
            //                            PriceCalculationID = "MICP19",
            //                            Name="WIC w/ Area Capture Program and Step/Stop/Water", Order= 1}
            //                            },
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* Includes: 2 Magnets per system", Order = 1},
            //                                new MajorComponentDetail {Name="* Includes: Whisker Switch for Checkpoints", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC10",
            //                            DetailCalculationID = "MACD10",
            //                            PriceCalculationID = "MACP10",
            //                            Name="Wireless Walk Switch - Easily Move Boom in Bay",
            //                            Order = 2,
            //                            AddPriceCondition = "=WirelessWalkSwitchRequested = true",
            //                            PriceCalculation = "285.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC11",
            //                            DetailCalculationID = "MACD11",
            //                            PriceCalculationID = "MACP11",
            //                            Name="Dual Water/Mist Upgrade",
            //                            Order = 3,
            //                            AddPriceCondition = @"=""SingleOrDualWater"" = ""Dual""",
            //                            PriceCalculation = "=ROUNDUP((11 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2) + 25 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet XR11002 (0.2 gpm) 'Yellow' Tips", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC12",
            //                            DetailCalculationID = "MACD12",
            //                            PriceCalculationID = "MACP12",
            //                            Name="Step / Stop / Water Spray Bar Upgrade",
            //                            Order = 4,
            //                            AddPriceCondition = "=TripleStepLocklineUpgradeRequested = true",
            //                            PriceCalculation = "=ROUNDUP(22.5 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet Bodies w/ Flexible Lock-Line assembly", Order = 1},
            //                                new MajorComponentDetail {Name="(including individual needle valves for calibrating)", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC13",
            //                            DetailCalculationID = "MACD13",
            //                            PriceCalculationID = "MACP13",
            //                            Name="Triple Turret Spray Body Upgrade",
            //                            Order = 5,
            //                            AddPriceCondition = "=TripleTurretBodyRequested = true",
            //                            PriceCalculation = "=ROUNDUP((20 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 )), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC14",
            //                            DetailCalculationID = "MACD14",
            //                            PriceCalculationID = "MACP14",
            //                            Name="Mounted Injector w/ Custom Manifold",
            //                            Order = 7,
            //                            AddPriceCondition = "=MountedInjectorRequested = true",
            //                            PriceCalculation = "595.00",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* Customer to determine Injector Size; prices may vary", Order = 1},
            //                                new MajorComponentDetail {Name="** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC15",
            //                            DetailCalculationID = "MACD15",
            //                            PriceCalculationID = "MACP15",
            //                            Name="1\" Pressure Regulator & Gauge",
            //                            Order = 8,
            //                            AddPriceCondition = "=PressureRegulatorRequested = true",
            //                            PriceCalculation = "=100.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC16",
            //                            DetailCalculationID = "MACD16",
            //                            PriceCalculationID = "MACP16",
            //                            Name="1 1/4\" Hose Upgrade (bays over 400')",
            //                            Order = 9,
            //                            AddPriceCondition = "=HoseUpgradeRequested = true",
            //                            PriceCalculation = "=MACD5*2.25"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC17",
            //                            DetailCalculationID = "MACD17",
            //                            PriceCalculationID = "MACP17",
            //                            Name="Diamond Hose Sweep Assembly",
            //                            Order = 10,
            //                            AddPriceCondition = "=DiamondSweepRequested = true",
            //                            PriceCalculation = "=200.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC18",
            //                            DetailCalculationID = "MACD18",
            //                            PriceCalculationID = "MACP18",
            //                            Name="Direct Drive Dual Motor (No axle)",
            //                            Order = 11,
            //                            AddPriceCondition = "=DualMotorNoAxleRequested = true",
            //                            PriceCalculation = "=685.00",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name = "***Includes Heavy Duty Dual Motor Card ***"}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC19",
            //                            DetailCalculationID = "MACD19",
            //                            PriceCalculationID = "MACP19",
            //                            Name="Conveyor - Mounted to Boom",
            //                            Order = 12,
            //                            AddPriceCondition = "=ConveyorRequested = true",
            //                            PriceCalculation = "=( 1200 + 10.5 * BayWidthInFeet ) * 1.75"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC20",
            //                            DetailCalculationID = "MACD20",
            //                            PriceCalculationID = "MACP20",
            //                            Name="Center Water Feed System",
            //                            Order = 13,
            //                            AddPriceCondition = "=CenterWaterFeedSystemRequested = true",
            //                            PriceCalculation = "= 350+(MICD16*25)"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC21",
            //                            DetailCalculationID = "MACD21",
            //                            PriceCalculationID = "MACP21",
            //                            Name="Bull-Horns  (CWF Rail Support Bracket)",
            //                            Order = 14,
            //                            DetailCalculation = "=ROUNDUP(MICD16/2,0)"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC22",
            //                            DetailCalculationID = "MACD22",
            //                            PriceCalculationID = "MACP22",
            //                            Name="J-Hooks (Single Bull-Horn) Rail Support Brackets",
            //                            Order = 15,
            //                            DetailCalculation = "=MACD21-1"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC23",
            //                            DetailCalculationID = "MACD23",
            //                            PriceCalculationID = "MACP23",
            //                            Name="CWF Cart & Hose Pusher",
            //                            Order = 16,
            //                            DetailCalculation = "=1"
            //                        }
            //                    }
            //                }
            //                }
            //     });

            //#endregion TB System

            //#region Navigator System

            //context.BoomSystems.AddOrUpdate(s => s.Name,
            //     new BoomSystem
            //     {
            //         Name = "Pro Series - Navigator Walk Boom",
            //         BoomTypeId = 6,
            //         ComponentID = "SYS",
            //         DetailCalculationID = "SYSD",
            //         PriceCalculationID = "SYSP",
            //         DescriptionCalculationID = "SYST",
            //         PriceCalculation = "ROUNDUP"
            //                            + "(BaseValue * BayWidthInFeet + (BayLengthInFeet / 10 * 39.2) + (BayWidthInFeet * 12 / SprayBodySpacingInInches * 7) \r\n"
            //                            + "+ (ROUNDUP(BayWidthInFeet / 20, 0) * 9.8 * BayWidthInFeet) * 1.5 \r\n"
            //                            + ", 0) * 1.2 + 800 \r\n",
            //         Categories = new List<Category>
            //         {
            //                new Category
            //                {
            //                    Name="General Parts",
            //                    Order = 1,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC1",
            //                            DetailCalculationID = "MACD1",
            //                            PriceCalculationID = "MACP1",
            //                            Name="Walk-about Truss Frame Unit - Bay Spanning Design - Rails Mounted to Posts",
            //                            Order = 1,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {ComponentID = "MIC1",
            //                            DetailCalculationID = "MICD1",
            //                            PriceCalculationID = "MICP1", Name="Baldor Super E 1/2 HP, Variable Speed 3 Phase Inverter Duty AC motor", Order = 1,
            //                                  MinorComponentDetails =  new List<MinorComponentDetail>
            //                                  {
            //                                       new MinorComponentDetail { Name="(20:1 gear ratio, 230VAC output, 5.25 Amps each)", Order=1}}
            //                                    },
            //                                new MinorComponent
            //                                {ComponentID = "MIC2",
            //                            DetailCalculationID = "MICD2",
            //                            PriceCalculationID = "MICP2", Name="6\" Rubber Molded Cast Iron Wheels w/ Guide Bearings", Order = 2},
            //                                new MinorComponent
            //                                {ComponentID = "MIC3",
            //                            DetailCalculationID = "MICD3",
            //                            PriceCalculationID = "MICP3", Name="3/4\" Chain Driven Axle w/ Pillow Block Bearings", Order = 3},
            //                                new MinorComponent
            //                                {ComponentID = "MIC4",
            //                            DetailCalculationID = "MICD4",
            //                            PriceCalculationID = "MICP4", Name="Adjustable Telescopic spray bar support w/ shaft collar lock", Order = 4}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC2",
            //                            DetailCalculationID = "MACD2",
            //                            PriceCalculationID = "MACP2",
            //                            Name="1\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *",
            //                            Order = 2,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {ComponentID = "MIC5",
            //                            DetailCalculationID = "MICD5",
            //                            PriceCalculationID = "MICP5", Name="1\" Rainbird Solenoid Valves", Order = 1},
            //                                new MinorComponent
            //                                {ComponentID = "MIC6",
            //                            DetailCalculationID = "MICD6",
            //                            PriceCalculationID = "MICP6", Name="1\" Brass Ball Valve", Order = 2},
            //                                new MinorComponent
            //                                {ComponentID = "MIC7",
            //                            DetailCalculationID = "MICD7",
            //                            PriceCalculationID = "MICP7", Name="1\" Amiad In-Line Filter", Order = 3}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC3",
            //                            DetailCalculationID = "MACD3",
            //                            PriceCalculationID = "MACP3",
            //                            Name="Whip Hose Hand-Watering Assy.",
            //                            Order = 3,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {ComponentID = "MIC8",
            //                            DetailCalculationID = "MICD8",
            //                            PriceCalculationID = "MICP8", Name="3/4\" Commercial Grade Hose", Order = 1,
            //                                MinorComponentDetails = new List<MinorComponentDetail>
            //                                {
            //                                    new MinorComponentDetail {Name="24\" Dramm Wand Assy. w/ Brass Ball Valve", Order=1}
            //                                }}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC4",
            //                            DetailCalculationID = "MACD4",
            //                            PriceCalculationID = "MACP4",
            //                            Name="Single Water Bar Setup",
            //                            Order = 4,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC9",
            //                            DetailCalculationID = "MICD9",
            //                            PriceCalculationID = "MICP9",
            //                                    Name="[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC10",
            //                            DetailCalculationID = "MICD10",
            //                            PriceCalculationID = "MICP10",
            //                                    Name="[inch] TeeJet Spray Bodies w/ Tips & Caps",
            //                                    DetailCalculation = "=SprayBarSize",
            //                                    Order = 2,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail {Name="XR11008 White (0.8 gpm) Tips - unless otherwise specified by customer", Order = 1}
            //                                    }},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC11",
            //                            DetailCalculationID = "MICD11",
            //                            PriceCalculationID = "MICP11",
            //                                    Name="[inch] Spray Body/Tip Spacing (average)",
            //                                    DetailCalculation = "=SprayBodySpacingInInches",
            //                                    Order = 3}
            //                            },
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC5",
            //                            DetailCalculationID = "MACD5",
            //                            PriceCalculationID = "MACP5",
            //                            Name="Ft. Commercial Rubber Hose & Power Cable",
            //                            DetailCalculation = "=ROUNDUP( BayLengthInFeet * 1.2, 0 )",
            //                            ActivateComponentCondition = @"=CenterWaterFeedSystemRequested = false",
            //                            Order = 5,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC12",
            //                            DetailCalculationID = "MICD12",
            //                            PriceCalculationID = "MICP12",
            //                                    Name="[inch] Commercial Grade Rubber Hose; 200psi",
            //                                    DetailCalculation = "=HoseSize",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC13",
            //                            DetailCalculationID = "MICD13",
            //                            PriceCalculationID = "MICP13", Name="12/3 300V Water Resistant Electrical Power Cable", Order= 2},
            //                            }
            //                        },
            //                            new MajorComponent
            //                            {
            //                            ComponentID = "MAC21",
            //                            DetailCalculationID = "MACD21",
            //                            PriceCalculationID = "MACP21",
            //                            ActivateComponentCondition = @"=CenterWaterFeedSystemRequested = true",
            //                            Name="Center Water Feed System",
            //                            Order = 5,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                            ComponentID = "MIC20",
            //                            DetailCalculationID = "MICD20",
            //                            PriceCalculationID = "MICP20",
            //                                    Name="Ft. Commercial Poly Pipe & Power Cable",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet * 0.5 + 10, 0 )",
            //                                    Order = 1,
            //                                },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC21",
            //                            DetailCalculationID = "MICD21",
            //                            PriceCalculationID = "MICP21",
            //                                    Name="[inch] 125psi UV Resistant Commercial Grade Poly Pipe (UltraBlue)",
            //                                    DetailCalculation = "=HoseSize",
            //                                    Order = 2},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC22",
            //                            DetailCalculationID = "MICD22",
            //                            PriceCalculationID = "MICP22",
            //                                    Name="[feet] 16/3 300V Water Resistant Electrical Power Cable",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet * 0.5 + 20, 0 )",
            //                                    Order= 3,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail { Name="Water Tight Plumbing / Electrical Assy.", Order = 1}
            //                                    }},
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC6",
            //                            DetailCalculationID = "MACD6",
            //                            PriceCalculationID = "MACP6",
            //                            Name="CCS Diamond Hose Trolleys - 2\" x 2\" Square",
            //                            DetailCalculation = "ROUNDUP( (BayLengthInFeet * 1.2) / 10, 0 ) + 1",
            //                            Order = 6,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC14",
            //                            DetailCalculationID = "MICD14",
            //                            PriceCalculationID = "MICP14",
            //                                    Name="[inch] Hose Loop Height",
            //                                    DetailCalculation = "=HoseLoopHeightInInches",
            //                                    Order = 1},
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC15",
            //                            DetailCalculationID = "MICD15",
            //                            PriceCalculationID = "MICP15",
            //                                    Name="CCS Hose Pusher bracket",
            //                                    DetailCalculation = "=1",
            //                                    Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC7",
            //                            DetailCalculationID = "MACD7",
            //                            PriceCalculationID = "MACP7",
            //                            Name="Brackets for Rail",
            //                            Order = 7,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC16",
            //                            DetailCalculationID = "MICD16",
            //                            PriceCalculationID = "MICP16",
            //                                    Name="[pair] Navigator Boom Rail Mount Brackets and hardware",
            //                                    DetailCalculation = "=ROUNDUP( BayLengthInFeet / PostSpacingInFeet, 0 ) + 1",
            //                                    Order = 1,
            //                                },
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC17",
            //                            DetailCalculationID = "MICD17",
            //                            PriceCalculationID = "MICP17",
            //                                    Name="Single & Double sided brackets for 2\" x 4\" Rails",
            //                                    Order = 2,
            //                                    MinorComponentDetails = new List<MinorComponentDetail>
            //                                    {
            //                                        new MinorComponentDetail {Name = "(Amounts TBD by Greenhouse Design)", Order = 1}
            //                                    }
            //                                }
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC8",
            //                            DetailCalculationID = "MACD8",
            //                            PriceCalculationID = "MACP8",
            //                            Name="Controller Type",
            //                            Order = 8,
            //                            MinorComponents = new List<MinorComponent>
            //                            {
            //                                new MinorComponent
            //                                {
            //                                    ComponentID = "MIC18",
            //                            DetailCalculationID = "MICD18",
            //                            PriceCalculationID = "MICP18",
            //                                    Name="Compass Capture Controller with Heavy Duty Dual Motor Board",
            //                                    Order = 1,
            //                                    MinorComponentDetails =  new List<MinorComponentDetail>
            //                                    {
            //                                       new MinorComponentDetail { Name="WIC w/ Area Capture Program and Step/Stop/Water", Order=1}}
            //                                    }
            //                            },
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail
            //                                {
            //                                    Name="* Includes: 2 Magnets per system",
            //                                    Order=1
            //                                },
            //                                new MajorComponentDetail
            //                                {
            //                                    Name="* Includes: Whisker Switch for Checkpoints",
            //                                    Order=2
            //                                }
            //                            }
            //                        },
            //                },
            //                },
            //                new Category
            //                {
            //                    Name="Options and Upgrades",
            //                    Order = 2,
            //                    MajorComponents = new List<MajorComponent>
            //                    {
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC9",
            //                            DetailCalculationID = "MACD9",
            //                            PriceCalculationID = "MACP9",
            //                            Name="Wireless Walk Switch - Easily Move Boom in Bay",
            //                            Order = 2,
            //                            AddPriceCondition = "=WirelessWalkSwitchRequested = true",
            //                            PriceCalculation = "285.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC10",
            //                            DetailCalculationID = "MACD10",
            //                            PriceCalculationID = "MACP10",
            //                            Name="Dual Water/Mist Upgrade",
            //                            Order = 3,
            //                            AddPriceCondition = @"=SingleOrDualWater = ""Dual""",
            //                            PriceCalculation = "=ROUNDUP((11 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2) + 25 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet XR11002 (0.2 gpm) 'Yellow' Tips", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC11",
            //                            DetailCalculationID = "MACD11",
            //                            PriceCalculationID = "MACP11",
            //                            Name="Step / Stop / Water Spray Bar Upgrade",
            //                            Order = 4,
            //                            AddPriceCondition = "=TripleStepLocklineUpgradeRequested = true",
            //                            PriceCalculation = "=ROUNDUP(22.5 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 ), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* TeeJet Bodies w/ Flexible Lock-Line assembly", Order = 1},
            //                                new MajorComponentDetail {Name="(including individual needle valves for calibrating)", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC12",
            //                            DetailCalculationID = "MACD12",
            //                            PriceCalculationID = "MACP12",
            //                            Name="Triple Turret Spray Body Upgrade",
            //                            Order = 5,
            //                            AddPriceCondition = "=TripleTurretBodyRequested = true",
            //                            PriceCalculation = "=ROUNDUP((20 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 )), 0)",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *", Order = 1}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC13",
            //                            DetailCalculationID = "MACD13",
            //                            PriceCalculationID = "MACP13",
            //                            Name="Mounted Injector w/ Custom Manifold",
            //                            Order = 7,
            //                            AddPriceCondition = "=MountedInjectorRequested = true",
            //                            PriceCalculation = "595.00",
            //                            MajorComponentDetails = new List<MajorComponentDetail>
            //                            {
            //                                new MajorComponentDetail {Name="* Customer to determine Injector Size; prices may vary", Order = 1},
            //                                new MajorComponentDetail {Name="** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket", Order = 2}
            //                            }
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC14",
            //                            DetailCalculationID = "MACD14",
            //                            PriceCalculationID = "MACP14",
            //                            Name="1\" Pressure Regulator & Gauge",
            //                            Order = 8,
            //                            AddPriceCondition = "=PressureRegulatorRequested = true",
            //                            PriceCalculation = "=100.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC15",
            //                            DetailCalculationID = "MACD15",
            //                            PriceCalculationID = "MACP15",
            //                            Name="1 1/4\" Hose Upgrade (bays over 400')",
            //                            Order = 9,
            //                            AddPriceCondition = "=HoseUpgradeRequested = true",
            //                            PriceCalculation = "=MACD5*2.25"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC16",
            //                            DetailCalculationID = "MACD16",
            //                            PriceCalculationID = "MACP16",
            //                            Name="Diamond Hose Sweep Assembly",
            //                            Order = 10,
            //                            AddPriceCondition = "=Sweep90Requested = true",
            //                            PriceCalculation = "=200.00"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC17",
            //                            DetailCalculationID = "MACD17",
            //                            PriceCalculationID = "MACP17",
            //                            Name="Center Water Feed System",
            //                            Order = 13,
            //                            AddPriceCondition = "=CenterWaterFeedSystemRequested = true",
            //                            PriceCalculation = "= 350+(MICD16*25)"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC18",
            //                            DetailCalculationID = "MACD18",
            //                            PriceCalculationID = "MACP18",
            //                            Name="Bull-Horns  (CWF Rail Support Bracket)",
            //                            Order = 14,
            //                            DetailCalculation = "=ROUNDUP(MICD16/2,0)"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC19",
            //                            DetailCalculationID = "MACD19",
            //                            PriceCalculationID = "MACP19",
            //                            Name="J-Hooks (Single Bull-Horn) Rail Support Brackets",
            //                            Order = 15,
            //                            DetailCalculation = "=MACD18-1"
            //                        },
            //                        new MajorComponent
            //                        {
            //                            ComponentID = "MAC20",
            //                            DetailCalculationID = "MACD20",
            //                            PriceCalculationID = "MACP20",
            //                            Name="CWF Cart & Hose Pusher",
            //                            Order = 16,
            //                            DetailCalculation = "=1"
            //                        }
            //                    }
            //                }
            //                }
            //     });

            //#endregion Navigator System

            #endregion Starter Boom Systems

            #region Ground Runner System

            //        context.BoomSystems.AddOrUpdate(s => s.Name,
            //             new BoomSystem
            //             {
            //                 AddPriceCondition = "=true",
            //                 BoomTypeId = 7,
            //                 Categories = new List<Category>
            //{
            //    new Category
            //    {
            //        MajorComponents = new List<MajorComponent>
            //        {
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "=IF(HoseSize < 1.25, true, false)",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC1",
            //                DescriptionCalculation = "=\"Frame Unit w/ 24\"\" Center Walkway spacing\"",
            //                DescriptionCalculationID = "MACT1",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD1",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC1",
            //                        DescriptionCalculation = "\"Baldor, 1/4 HP variable speed DC motor w/chain drive\"",
            //                        DescriptionCalculationID = "MICT1",
            //                        DetailCalculationID = "MICD1",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID7",
            //                                DescriptionCalculation = "\"(11:1 gear ratio, 90VDC, 2.5 Amps)\"",
            //                                DescriptionCalculationID = "MIDT7",
            //                                DetailCalculationID = "MIDD7",
            //                                Name = "(11:1 gear ratio, 90VDC, 2.5 Amps)",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP7"
            //                            }
            //                        },
            //                        Name = "Baldor, 1/4 HP variable speed DC motor w/chain drive",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP1"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC2",
            //                        DescriptionCalculation = "=\"4 Wheel Drive design w/ 38\"\" axles\"",
            //                        DescriptionCalculationID = "MICT2",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD2",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID8",
            //                                DescriptionCalculation = "\"(36\" wheel spacing; center to center)\"",
            //                                DescriptionCalculationID = "MIDT8",
            //                                DetailCalculationID = "MIDD8",
            //                                Name = "(36\" wheel spacing; center to center)",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP8"
            //                            }
            //                        },
            //                        Name = "4 Wheel Drive design w/ 38\" axles",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP2"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC3",
            //                        DescriptionCalculation = "\"5th Wheel Idle assembly (Motion, Speed and Direction)\"",
            //                        DescriptionCalculationID = "MICT3",
            //                        DetailCalculationID = "MICD3",
            //                        Name = "5th Wheel Idle assembly (Motion, Speed and Direction)",
            //                        Order = 3,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP3"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC4",
            //                        DescriptionCalculation = "\"Adjustable Telescopic spray bar supports\"",
            //                        DescriptionCalculationID = "MICT4",
            //                        DetailCalculationID = "MICD4",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID9",
            //                                DescriptionCalculation = "\"(with 1/8\" aircraft cable and remote pipe supports)\"",
            //                                DescriptionCalculationID = "MIDT9",
            //                                DetailCalculationID = "MIDD9",
            //                                Name = "(with 1/8\" aircraft cable and remote pipe supports)",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP9"
            //                            }
            //                        },
            //                        Name = "Adjustable Telescopic spray bar supports",
            //                        Order = 4,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP4"
            //                    }
            //                },
            //                Name = "Frame Unit w/ 24\" Center Walkway spacing",
            //                Order = 1,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP1"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "IF(HoseSize >= 1.25, true, false)",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC128",
            //                DescriptionCalculation = "=\"Wide Frame Unit Walkway spacing\"",
            //                DescriptionCalculationID = "MACT128",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD128",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC137",
            //                        DescriptionCalculation = "=\"Baldor, 1/4 HP variable speed DC motor w/chain drive\"",
            //                        DescriptionCalculationID = "MICT137",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD137",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "(11:1 gear ratio, 90VDC, 2.5 Amps)",
            //                                Order = 1,
            //                            }
            //                        },
            //                        Name = "Baldor, 1/4 HP variable speed DC motor w/chain drive",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP137"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC138",
            //                        DescriptionCalculation = "=\"4 Wheel Drive design w/ 38\"\" axles\"",
            //                        DescriptionCalculationID = "MICT138",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD138",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "(36\" wheel spacing; center to center)",
            //                                Order = 1,
            //                            }
            //                        },
            //                        Name = "4 Wheel Drive design w/ 38\" axles",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP138"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC139",
            //                        DescriptionCalculation = "=\"5th Wheel Idle assembly (Motion, Speed and Direction)\"",
            //                        DescriptionCalculationID = "MICT139",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD139",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "(with 1/8\" aircraft cable and remote pipe supports)",
            //                                Order = 1
            //                            }
            //                        },
            //                        Name = "5th Wheel Idle assembly (Motion, Speed and Direction)",
            //                        Order = 3,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP139"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC140",
            //                        DescriptionCalculation = "=\"Adjustable Telescopic spray bar supports\"",
            //                        DescriptionCalculationID = "MICT140",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD140",
            //                        Name = "Adjustable Telescopic spray bar supports",
            //                        Order = 4,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP140"
            //                    }
            //                },
            //                Name = "Wide Frame Unit Walkway spacing",
            //                Order = 1,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP128"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "=IF(ClearanceWidthInInches > 38.5, true, false)",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC135",
            //                DescriptionCalculation = "\"Custom frame unit\"",
            //                DescriptionCalculationID = "MACT135",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD135",
            //                Name = "Custom frame unit",
            //                Order = 1,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP135"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC2",
            //                DescriptionCalculation = "\"1\"\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *\"",
            //                DescriptionCalculationID = "MACT2",
            //                DetailCalculationID = "MACD2",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC5",
            //                        DescriptionCalculation = "\"1\"\" Rainbird Solenoid Valves\"",
            //                        DescriptionCalculationID = "MICT5",
            //                        DetailCalculationID = "MICD5",
            //                        Name = "1\" Rainbird Solenoid Valves",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP5"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC6",
            //                        DescriptionCalculation = "\"1\"\" Brass Ball Valve\"",
            //                        DescriptionCalculationID = "MICT6",
            //                        DetailCalculationID = "MICD6",
            //                        Name = "1\" Brass Ball Valve",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP6"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC7",
            //                        DescriptionCalculation = "\"1\"\" Amiad In-Line Filter\"",
            //                        DescriptionCalculationID = "MICT7",
            //                        DetailCalculationID = "MICD7",
            //                        Name = "1\" Amiad In-Line Filter",
            //                        Order = 3,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP7"
            //                    }
            //                },
            //                Name = "1\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *",
            //                Order = 2,
            //                PriceCalculationID = "MACP2"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC3",
            //                DescriptionCalculation = "\"Whip Hose Hand-Watering Assy.\"",
            //                DescriptionCalculationID = "MACT3",
            //                DetailCalculationID = "MACD3",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC8",
            //                        DescriptionCalculation = "\"3/4\"\" Commercial Grade Hose\"",
            //                        DescriptionCalculationID = "MICT8",
            //                        DetailCalculationID = "MICD8",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID10",
            //                                DescriptionCalculation = "\"24\" Dramm Wand Assy. w/ Brass Ball Valve\"",
            //                                DescriptionCalculationID = "MIDT10",
            //                                DetailCalculationID = "MIDD10",
            //                                Name = "24\" Dramm Wand Assy. w/ Brass Ball Valve",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP10"
            //                            }
            //                        },
            //                        Name = "3/4\" Commercial Grade Hose",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP8"
            //                    }
            //                },
            //                Name = "Whip Hose Hand-Watering Assy.",
            //                Order = 3,
            //                PriceCalculationID = "MACP3"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC4",
            //                DescriptionCalculation = "\"Single Water Bar Setup\"",
            //                DescriptionCalculationID = "MACT4",
            //                DetailCalculationID = "MACD4",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC9",
            //                        DescriptionCalculation = "\"[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters\"",
            //                        DescriptionCalculationID = "MICT9",
            //                        DetailCalculation = "=SprayBarSize",
            //                        DetailCalculationID = "MICD9",
            //                        Name = "[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP9"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC10",
            //                        DescriptionCalculation = "\"[inch] TeeJet Spray Bodies w/ Tips & Caps\"",
            //                        DescriptionCalculationID = "MICT10",
            //                        DetailCalculation = "=SprayBarSize",
            //                        DetailCalculationID = "MICD10",
            //                        Name = "[inch] TeeJet Spray Bodies w/ Tips & Caps",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP10"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC110",
            //                        DescriptionCalculation = "=Tip1 & \" White (0.8 gpm) Tips - unless otherwise specified by customer\"",
            //                        DescriptionCalculationID = "MICT110",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD110",
            //                        Name = "Tips - unless otherwise specified by customer",
            //                        Order = 3,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP110"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC11",
            //                        DescriptionCalculation = "\"[inch] Spray Body/Tip Spacing (average)\"",
            //                        DescriptionCalculationID = "MICT11",
            //                        DetailCalculation = "=SprayBodySpacingInInches",
            //                        DetailCalculationID = "MICD11",
            //                        Name = "[inch] Spray Body/Tip Spacing (average)",
            //                        Order = 4,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP11"
            //                    }
            //                },
            //                Name = "Single Water Bar Setup",
            //                Order = 4,
            //                PriceCalculationID = "MACP4"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC5",
            //                DescriptionCalculation = "\"Ft. Commercial Rubber Hose & Power Cable\"",
            //                DescriptionCalculationID = "MACT5",
            //                DetailCalculation = "=ROUNDUP( BayLengthInFeet * 1.2, 0 )",
            //                DetailCalculationID = "MACD5",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC12",
            //                        DescriptionCalculation = "\"[inch] Commercial Grade Rubber Hose; 200psi\"",
            //                        DescriptionCalculationID = "MICT12",
            //                        DetailCalculation = "=HoseSize",
            //                        DetailCalculationID = "MICD12",
            //                        Name = "[inch] Commercial Grade Rubber Hose; 200psi",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP12"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC13",
            //                        DescriptionCalculation = "IF(EvVpd = false, \"16/3\", \"16/5\") & \" 300V Water Resistant Electrical Power Cable\"",
            //                        DescriptionCalculationID = "MICT13",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD13",
            //                        Name = "300V Water Resistant Electrical Power Cable",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP13"
            //                    }
            //                },
            //                Name = "Ft. Commercial Rubber Hose & Power Cable",
            //                Order = 5,
            //                PriceCalculationID = "MACP5"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC6",
            //                DescriptionCalculation = "\"CCS Wrap Around Hose Trolleys - 1 5/8\"\"\"",
            //                DescriptionCalculationID = "MACT6",
            //                DetailCalculation = "ROUNDUP( (BayLengthInFeet * 1.2) / 10, 0 ) + 1",
            //                DetailCalculationID = "MACD6",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC14",
            //                        DescriptionCalculation = "\"[inch] Hose Loop Height\"",
            //                        DescriptionCalculationID = "MICT14",
            //                        DetailCalculation = "=HoseLoopHeightInInches",
            //                        DetailCalculationID = "MICD14",
            //                        Name = "[inch] Hose Loop Height",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP14"
            //                    }
            //                },
            //                Name = "CCS Wrap Around Hose Trolleys - 1 5/8\"",
            //                Order = 6,
            //                PriceCalculationID = "MACP6"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC7",
            //                DescriptionCalculation = "\"Brackets for Rail\"",
            //                DescriptionCalculationID = "MACT7",
            //                DetailCalculationID = "MACD7",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC15",
            //                        DescriptionCalculation = "\"Bull-Horn Rail Support Brackets\"",
            //                        DescriptionCalculationID = "MICT15",
            //                        DetailCalculation = "=ROUNDUP( BayLengthInFeet / TrussSpacingInFeet, 0 ) + 1",
            //                        DetailCalculationID = "MICD15",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID12",
            //                                DescriptionCalculation = "\"Mounting brackets & hardware\"",
            //                                DescriptionCalculationID = "MIDT12",
            //                                DetailCalculationID = "MIDD12",
            //                                Name = "Mounting brackets & hardware",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP12"
            //                            }
            //                        },
            //                        Name = "Bull-Horn Rail Support Brackets",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP15"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC16",
            //                        DescriptionCalculation = "\"Rail Sleeves for Support Pipe; 12\"\" long\"",
            //                        DescriptionCalculationID = "MICT16",
            //                        DetailCalculation = "=ROUNDDOWN( BayLengthInFeet / 21, 0 ) * 2",
            //                        DetailCalculationID = "MICD16",
            //                        Name = "Rail Sleeves for Support Pipe; 12\" long",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP16"
            //                    }
            //                },
            //                Name = "Brackets for Rail",
            //                Order = 7,
            //                PriceCalculationID = "MACP7"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "=ControllerType = \"NIC\"",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC8",
            //                DescriptionCalculation = "\"Controller Type\"",
            //                DescriptionCalculationID = "MACT8",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD8",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC17",
            //                        DescriptionCalculation = "\"Network Irrigation Controller\"",
            //                        DescriptionCalculationID = "MICT17",
            //                        DetailCalculationID = "MICD17",
            //                        Name = "Network Irrigation Controller",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP17"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC121",
            //                        DescriptionCalculation = "=\"NIC w/ Magnet Jobbing (A28 chip)\"",
            //                        DescriptionCalculationID = "MICT121",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD121",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "* Includes: 12 Magnets per system",
            //                                Order = 1,
            //                            }
            //                        },
            //                        Name = "NIC w/ Magnet Jobbing (A28 chip)",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP121"
            //                    }
            //                },
            //                Name = "NIC Controller Type",
            //                Order = 8,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP8"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "=ControllerType = \"WIC\"",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC123",
            //                DescriptionCalculation = "\"Controller Type\"",
            //                DescriptionCalculationID = "MACT123",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD123",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC122",
            //                        DescriptionCalculation = "=\"Compass Capture Controller Upgrade\"",
            //                        DescriptionCalculationID = "MICT122",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD122",
            //                        Name = "Compass Capture Controller Upgrade",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP122"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC123",
            //                        DescriptionCalculation = "=\"WIC w/ Area Capture Program and Step/Stop/Water\"",
            //                        DescriptionCalculationID = "MICT123",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD123",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "* Includes: 2 Magnets per system",
            //                                Order = 1,
            //                            },
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "* Includes: Whisker Switch for Checkpoints",
            //                                Order = 2,
            //                            }
            //                        },
            //                        Name = "WIC w/ Area Capture Program and Step/Stop/Water",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP123"
            //                    }
            //                },
            //                Name = "WIC Controller Type",
            //                Order = 8,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP123"
            //            }
            //        },
            //        Name = "General Parts",
            //        Order = 1
            //    },
            //    new Category
            //    {
            //        MajorComponents = new List<MajorComponent>
            //        {
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=WirelessWalkSwitchRequested = true",
            //                ComponentID = "MAC10",
            //                DescriptionCalculation = "\"Wireless Walk Switch - Easily Move Boom in Bay\"",
            //                DescriptionCalculationID = "MACT10",
            //                DetailCalculationID = "MACD10",
            //                Name = "Wireless Walk Switch - Easily Move Boom in Bay",
            //                Order = 2,
            //                PriceCalculation = "=285.00",
            //                PriceCalculationID = "MACP10"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=SingleOrDualWater = \"Dual\"",
            //                ComponentID = "MAC11",
            //                DescriptionCalculation = "\"Dual Water/Mist Upgrade\"",
            //                DescriptionCalculationID = "MACT11",
            //                DetailCalculationID = "MACD11",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC111",
            //                        DescriptionCalculation = "=\"* TeeJet \" & Tip2 & \" (0.2 gpm) 'Yellow' Tips\"",
            //                        DescriptionCalculationID = "MICT111",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD111",
            //                        Name = "Tips",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP111"
            //                    }
            //                },
            //                Name = "Dual Water/Mist Upgrade",
            //                Order = 3,
            //                PriceCalculation = "=ROUNDUP((11 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2) + 25 ), 0)",
            //                PriceCalculationID = "MACP11"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=TripleStepLocklineUpgradeRequested = true",
            //                ComponentID = "MAC12",
            //                DescriptionCalculation = "\"Step / Stop / Water Spray Bar Upgrade\"",
            //                DescriptionCalculationID = "MACT12",
            //                DetailCalculationID = "MACD12",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD16",
            //                        DescriptionCalculation = "\"* TeeJet Bodies w/ Flexible Lock-Line assembly\"",
            //                        DescriptionCalculationID = "MADT16",
            //                        DetailCalculationID = "MADD16",
            //                        Name = "* TeeJet Bodies w/ Flexible Lock-Line assembly",
            //                        Order = 1,
            //                        PriceCalculationID = "MADP16"
            //                    },
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD17",
            //                        DescriptionCalculation = "\"(including individual needle valves for calibrating)\"",
            //                        DescriptionCalculationID = "MADT17",
            //                        DetailCalculationID = "MADD17",
            //                        Name = "(including individual needle valves for calibrating)",
            //                        Order = 2,
            //                        PriceCalculationID = "MADP17"
            //                    }
            //                },
            //                Name = "Step / Stop / Water Spray Bar Upgrade",
            //                Order = 4,
            //                PriceCalculation = "=ROUNDUP(22.5 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 ), 0)",
            //                PriceCalculationID = "MACP12"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=TripleTurretBodyRequested = true",
            //                ComponentID = "MAC13",
            //                DescriptionCalculation = "\"Triple Turret Spray Body Upgrade\"",
            //                DescriptionCalculationID = "MACT13",
            //                DetailCalculationID = "MACD13",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD18",
            //                        DescriptionCalculation = "\"Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *\"",
            //                        DescriptionCalculationID = "MADT18",
            //                        DetailCalculationID = "MADD18",
            //                        Name = "Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *",
            //                        Order = 1,
            //                        PriceCalculationID = "MADP18"
            //                    }
            //                },
            //                Name = "Triple Turret Spray Body Upgrade",
            //                Order = 5,
            //                PriceCalculation = "=ROUNDUP((20 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 )), 0)",
            //                PriceCalculationID = "MACP13"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=PressureRegulatorRequested = true",
            //                ComponentID = "MAC14",
            //                DescriptionCalculation = "\"1\"\" Pressure Regulator & Gauge\"",
            //                DescriptionCalculationID = "MACT14",
            //                DetailCalculationID = "MACD14",
            //                Name = "1\" Pressure Regulator & Gauge",
            //                Order = 6,
            //                PriceCalculation = "=100.00",
            //                PriceCalculationID = "MACP14"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "InjectorType = \"Mounted\"",
            //                AddPriceCondition = "true",
            //                ComponentID = "MAC15",
            //                DescriptionCalculation = "\"Mounted Injector w/ Custom Manifold\"",
            //                DescriptionCalculationID = "MACT15",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD15",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD19",
            //                        DescriptionCalculation = "\"* Customer to determine Injector Size; prices may vary\"",
            //                        DescriptionCalculationID = "MADT19",
            //                        DetailCalculationID = "MADD19",
            //                        Name = "* Customer to determine Injector Size; prices may vary",
            //                        Order = 1,
            //                        PriceCalculationID = "MADP19"
            //                    },
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD20",
            //                        DescriptionCalculation = "\"** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket\"",
            //                        DescriptionCalculationID = "MADT20",
            //                        DetailCalculationID = "MADD20",
            //                        Name = "** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket",
            //                        Order = 2,
            //                        PriceCalculationID = "MADP20"
            //                    }
            //                },
            //                Name = "Mounted Injector w/ Custom Manifold",
            //                Order = 7,
            //                PriceCalculation = "=595.00",
            //                PriceCalculationID = "MACP15"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "InjectorType = \"Portable\"",
            //                AddPriceCondition = "true",
            //                ComponentID = "MAC139",
            //                DescriptionCalculation = "\"Portable Injector w/ Custom Manifold\"",
            //                DescriptionCalculationID = "MACT139",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD139",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        Name = "* Customer to determine Injector Size; prices may vary",
            //                        Order = 1
            //                    },
            //                    new MajorComponentDetail
            //                    {
            //                        Name = "** Includes portable injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket",
            //                        Order = 2
            //                    }
            //                },
            //                Name = "Portable Injector w/ Custom Manifold",
            //                Order = 7,
            //                PriceCalculation = "595",
            //                PriceCalculationID = "MACP139"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "InjectorType = \"Manifold Only\"",
            //                AddPriceCondition = "true",
            //                ComponentID = "MAC140",
            //                DescriptionCalculation = "\"Portable Injector Manifold\"",
            //                DescriptionCalculationID = "MACT140",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD140",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        Name = "Customer to determine Injector Size; prices may vary",
            //                        Order = 1,
            //                    },
            //                    new MajorComponentDetail
            //                    {
            //                        Name = "** Includes manifold for Portable Injector, Bucket Support Platform & 5 Gal. Bucket",
            //                        Order = 2
            //                    }
            //                },
            //                Name = "Portable Injector Manifold",
            //                Order = 7,
            //                PriceCalculation = "95",
            //                PriceCalculationID = "MACP140"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=HoseUpgradeRequested = true",
            //                ComponentID = "MAC16",
            //                DescriptionCalculation = "\"1 1/4\"\" Hose Upgrade (bays over 400')\"",
            //                DescriptionCalculationID = "MACT16",
            //                DetailCalculationID = "MACD16",
            //                Name = "1 1/4\" Hose Upgrade (bays over 400')",
            //                Order = 8,
            //                PriceCalculation = "=MACD5 * 2.25",
            //                PriceCalculationID = "MACP16"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=Sweep90Requested = true",
            //                ComponentID = "MAC17",
            //                DescriptionCalculation = "\"90 Degree Hose Sweep Assembly\"",
            //                DescriptionCalculationID = "MACT17",
            //                DetailCalculationID = "MACD17",
            //                Name = "90 Degree Hose Sweep Assembly",
            //                Order = 9,
            //                PriceCalculation = "=165.00",
            //                PriceCalculationID = "MACP17"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "true",
            //                ComponentID = "MAC18",
            //                DescriptionCalculation = "\"[inch] Rail Drop Down Assembly\"",
            //                DescriptionCalculationID = "MACT18",
            //                DetailCalculation = "=RailDropDownASSYInInches",
            //                DetailCalculationID = "MACD18",
            //                Name = "[inch] Rail Drop Down Assembly",
            //                Order = 10,
            //                PriceCalculation = "=(MICD15*2)*(RailDropDownASSYInInches/12)*2",
            //                PriceCalculationID = "MACP18"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "WaterSolenoidControlRequested = true",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC131",
            //                DescriptionCalculation = "\"Feed and fresh water solenoid control\"",
            //                DescriptionCalculationID = "MACT131",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD131",
            //                Name = "Feed and fresh water solenoid control",
            //                Order = 10,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP131"
            //            }
            //        },
            //        Name = "Options and Upgrades",
            //        Order = 2
            //    }
            //},
            //                 ComponentID = "SYS",
            //                 DescriptionCalculation = "\"Ground Runner\"",
            //                 DescriptionCalculationID = "SYST",
            //                 DetailCalculationID = "SYSD",
            //                 Name = "Ground Runner",
            //                 PriceCalculation = "ROUNDUP(BaseValue + (BayLengthInFeet / 10 * 39.2 + BayWidthInFeet * 12 / SprayBodySpacingInInches * 7) * 1.1 \r\n, 0) * 1.175 + 750 \r\n",
            //                 PriceCalculationID = "SYSP"
            //             });

            #endregion Ground Runner System

            #region Sky Rail System

            //        context.BoomSystems.AddOrUpdate(s => s.Name,
            //             new BoomSystem
            //             {
            //                 AddPriceCondition = "=true",
            //                 BoomTypeId = 8,
            //                 Categories = new List<Category>
            //{
            //    new Category
            //    {
            //        MajorComponents = new List<MajorComponent>
            //        {
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "=IF(HoseSize < 1.25, true, false)",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC1",
            //                DescriptionCalculation = "=\"Frame Unit w/ 24\"\" Center Walkway spacing\"",
            //                DescriptionCalculationID = "MACT1",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD1",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC1",
            //                        DescriptionCalculation = "\"Baldor, 1/4 HP variable speed DC motor w/chain drive\"",
            //                        DescriptionCalculationID = "MICT1",
            //                        DetailCalculationID = "MICD1",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID7",
            //                                DescriptionCalculation = "\"(11:1 gear ratio, 90VDC, 2.5 Amps)\"",
            //                                DescriptionCalculationID = "MIDT7",
            //                                DetailCalculationID = "MIDD7",
            //                                Name = "(11:1 gear ratio, 90VDC, 2.5 Amps)",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP7"
            //                            }
            //                        },
            //                        Name = "Baldor, 1/4 HP variable speed DC motor w/chain drive",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP1"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC2",
            //                        DescriptionCalculation = "=\"4 Wheel Drive design w/ 38\"\" axles\"",
            //                        DescriptionCalculationID = "MICT2",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD2",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID8",
            //                                DescriptionCalculation = "\"(36\" wheel spacing; center to center)\"",
            //                                DescriptionCalculationID = "MIDT8",
            //                                DetailCalculationID = "MIDD8",
            //                                Name = "(36\" wheel spacing; center to center)",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP8"
            //                            }
            //                        },
            //                        Name = "4 Wheel Drive design w/ 38\" axles",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP2"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC3",
            //                        DescriptionCalculation = "\"5th Wheel Idle assembly (Motion, Speed and Direction)\"",
            //                        DescriptionCalculationID = "MICT3",
            //                        DetailCalculationID = "MICD3",
            //                        Name = "5th Wheel Idle assembly (Motion, Speed and Direction)",
            //                        Order = 3,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP3"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC4",
            //                        DescriptionCalculation = "\"Adjustable Telescopic spray bar supports\"",
            //                        DescriptionCalculationID = "MICT4",
            //                        DetailCalculationID = "MICD4",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID9",
            //                                DescriptionCalculation = "\"(with 1/8\" aircraft cable and remote pipe supports)\"",
            //                                DescriptionCalculationID = "MIDT9",
            //                                DetailCalculationID = "MIDD9",
            //                                Name = "(with 1/8\" aircraft cable and remote pipe supports)",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP9"
            //                            }
            //                        },
            //                        Name = "Adjustable Telescopic spray bar supports",
            //                        Order = 4,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP4"
            //                    }
            //                },
            //                Name = "Frame Unit w/ 24\" Center Walkway spacing",
            //                Order = 1,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP1"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "IF(HoseSize >= 1.25, true, false)",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC128",
            //                DescriptionCalculation = "=\"Wide Frame Unit Walkway spacing\"",
            //                DescriptionCalculationID = "MACT128",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD128",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC137",
            //                        DescriptionCalculation = "=\"Baldor, 1/4 HP variable speed DC motor w/chain drive\"",
            //                        DescriptionCalculationID = "MICT137",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD137",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "(11:1 gear ratio, 90VDC, 2.5 Amps)",
            //                                Order = 1,
            //                            }
            //                        },
            //                        Name = "Baldor, 1/4 HP variable speed DC motor w/chain drive",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP137"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC138",
            //                        DescriptionCalculation = "=\"4 Wheel Drive design w/ 38\"\" axles\"",
            //                        DescriptionCalculationID = "MICT138",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD138",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "(36\" wheel spacing; center to center)",
            //                                Order = 1,
            //                            }
            //                        },
            //                        Name = "4 Wheel Drive design w/ 38\" axles",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP138"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC139",
            //                        DescriptionCalculation = "=\"5th Wheel Idle assembly (Motion, Speed and Direction)\"",
            //                        DescriptionCalculationID = "MICT139",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD139",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "(with 1/8\" aircraft cable and remote pipe supports)",
            //                                Order = 1
            //                            }
            //                        },
            //                        Name = "5th Wheel Idle assembly (Motion, Speed and Direction)",
            //                        Order = 3,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP139"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC140",
            //                        DescriptionCalculation = "=\"Adjustable Telescopic spray bar supports\"",
            //                        DescriptionCalculationID = "MICT140",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD140",
            //                        Name = "Adjustable Telescopic spray bar supports",
            //                        Order = 4,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP140"
            //                    }
            //                },
            //                Name = "Wide Frame Unit Walkway spacing",
            //                Order = 1,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP128"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "=IF(ClearanceWidthInInches > 38.5, true, false)",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC135",
            //                DescriptionCalculation = "\"Custom frame unit\"",
            //                DescriptionCalculationID = "MACT135",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD135",
            //                Name = "Custom frame unit",
            //                Order = 1,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP135"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC2",
            //                DescriptionCalculation = "\"1\"\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *\"",
            //                DescriptionCalculationID = "MACT2",
            //                DetailCalculationID = "MACD2",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC5",
            //                        DescriptionCalculation = "\"1\"\" Rainbird Solenoid Valves\"",
            //                        DescriptionCalculationID = "MICT5",
            //                        DetailCalculationID = "MICD5",
            //                        Name = "1\" Rainbird Solenoid Valves",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP5"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC6",
            //                        DescriptionCalculation = "\"1\"\" Brass Ball Valve\"",
            //                        DescriptionCalculationID = "MICT6",
            //                        DetailCalculationID = "MICD6",
            //                        Name = "1\" Brass Ball Valve",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP6"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC7",
            //                        DescriptionCalculation = "\"1\"\" Amiad In-Line Filter\"",
            //                        DescriptionCalculationID = "MICT7",
            //                        DetailCalculationID = "MICD7",
            //                        Name = "1\" Amiad In-Line Filter",
            //                        Order = 3,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP7"
            //                    }
            //                },
            //                Name = "1\" Plumbing Manifold - * Max Pressure for Manifold = 80psi *",
            //                Order = 2,
            //                PriceCalculationID = "MACP2"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC3",
            //                DescriptionCalculation = "\"Whip Hose Hand-Watering Assy.\"",
            //                DescriptionCalculationID = "MACT3",
            //                DetailCalculationID = "MACD3",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC8",
            //                        DescriptionCalculation = "\"3/4\"\" Commercial Grade Hose\"",
            //                        DescriptionCalculationID = "MICT8",
            //                        DetailCalculationID = "MICD8",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID10",
            //                                DescriptionCalculation = "\"24\" Dramm Wand Assy. w/ Brass Ball Valve\"",
            //                                DescriptionCalculationID = "MIDT10",
            //                                DetailCalculationID = "MIDD10",
            //                                Name = "24\" Dramm Wand Assy. w/ Brass Ball Valve",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP10"
            //                            }
            //                        },
            //                        Name = "3/4\" Commercial Grade Hose",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP8"
            //                    }
            //                },
            //                Name = "Whip Hose Hand-Watering Assy.",
            //                Order = 3,
            //                PriceCalculationID = "MACP3"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC4",
            //                DescriptionCalculation = "\"Single Water Bar Setup\"",
            //                DescriptionCalculationID = "MACT4",
            //                DetailCalculationID = "MACD4",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC9",
            //                        DescriptionCalculation = "\"[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters\"",
            //                        DescriptionCalculationID = "MICT9",
            //                        DetailCalculation = "=SprayBarSize",
            //                        DetailCalculationID = "MICD9",
            //                        Name = "[inch] Aluminum Water Bar(s) w/ Galvanized End Caps & Adapters",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP9"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC10",
            //                        DescriptionCalculation = "\"[inch] TeeJet Spray Bodies w/ Tips & Caps\"",
            //                        DescriptionCalculationID = "MICT10",
            //                        DetailCalculation = "=SprayBarSize",
            //                        DetailCalculationID = "MICD10",
            //                        Name = "[inch] TeeJet Spray Bodies w/ Tips & Caps",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP10"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC110",
            //                        DescriptionCalculation = "=Tip1 & \" White (0.8 gpm) Tips - unless otherwise specified by customer\"",
            //                        DescriptionCalculationID = "MICT110",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD110",
            //                        Name = "Tips - unless otherwise specified by customer",
            //                        Order = 3,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP110"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC11",
            //                        DescriptionCalculation = "\"[inch] Spray Body/Tip Spacing (average)\"",
            //                        DescriptionCalculationID = "MICT11",
            //                        DetailCalculation = "=SprayBodySpacingInInches",
            //                        DetailCalculationID = "MICD11",
            //                        Name = "[inch] Spray Body/Tip Spacing (average)",
            //                        Order = 4,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP11"
            //                    }
            //                },
            //                Name = "Single Water Bar Setup",
            //                Order = 4,
            //                PriceCalculationID = "MACP4"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC5",
            //                DescriptionCalculation = "\"Ft. Commercial Rubber Hose & Power Cable\"",
            //                DescriptionCalculationID = "MACT5",
            //                DetailCalculation = "=ROUNDUP( BayLengthInFeet * 1.2, 0 )",
            //                DetailCalculationID = "MACD5",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC12",
            //                        DescriptionCalculation = "\"[inch] Commercial Grade Rubber Hose; 200psi\"",
            //                        DescriptionCalculationID = "MICT12",
            //                        DetailCalculation = "=HoseSize",
            //                        DetailCalculationID = "MICD12",
            //                        Name = "[inch] Commercial Grade Rubber Hose; 200psi",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP12"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC13",
            //                        DescriptionCalculation = "IF(EvVpd = false, \"16/3\", \"16/5\") & \" 300V Water Resistant Electrical Power Cable\"",
            //                        DescriptionCalculationID = "MICT13",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD13",
            //                        Name = "300V Water Resistant Electrical Power Cable",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP13"
            //                    }
            //                },
            //                Name = "Ft. Commercial Rubber Hose & Power Cable",
            //                Order = 5,
            //                PriceCalculationID = "MACP5"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC6",
            //                DescriptionCalculation = "\"CCS Wrap Around Hose Trolleys - 1 5/8\"\"\"",
            //                DescriptionCalculationID = "MACT6",
            //                DetailCalculation = "ROUNDUP( (BayLengthInFeet * 1.2) / 10, 0 ) + 1",
            //                DetailCalculationID = "MACD6",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC14",
            //                        DescriptionCalculation = "\"[inch] Hose Loop Height\"",
            //                        DescriptionCalculationID = "MICT14",
            //                        DetailCalculation = "=HoseLoopHeightInInches",
            //                        DetailCalculationID = "MICD14",
            //                        Name = "[inch] Hose Loop Height",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP14"
            //                    }
            //                },
            //                Name = "CCS Wrap Around Hose Trolleys - 1 5/8\"",
            //                Order = 6,
            //                PriceCalculationID = "MACP6"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                ComponentID = "MAC7",
            //                DescriptionCalculation = "\"Brackets for Rail\"",
            //                DescriptionCalculationID = "MACT7",
            //                DetailCalculationID = "MACD7",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC15",
            //                        DescriptionCalculation = "\"Bull-Horn Rail Support Brackets\"",
            //                        DescriptionCalculationID = "MICT15",
            //                        DetailCalculation = "=ROUNDUP( BayLengthInFeet / TrussSpacingInFeet, 0 ) + 1",
            //                        DetailCalculationID = "MICD15",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                ComponentID = "MID12",
            //                                DescriptionCalculation = "\"Mounting brackets & hardware\"",
            //                                DescriptionCalculationID = "MIDT12",
            //                                DetailCalculationID = "MIDD12",
            //                                Name = "Mounting brackets & hardware",
            //                                Order = 1,
            //                                PriceCalculationID = "MIDP12"
            //                            }
            //                        },
            //                        Name = "Bull-Horn Rail Support Brackets",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP15"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC16",
            //                        DescriptionCalculation = "\"Rail Sleeves for Support Pipe; 12\"\" long\"",
            //                        DescriptionCalculationID = "MICT16",
            //                        DetailCalculation = "=ROUNDDOWN( BayLengthInFeet / 21, 0 ) * 2",
            //                        DetailCalculationID = "MICD16",
            //                        Name = "Rail Sleeves for Support Pipe; 12\" long",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP16"
            //                    }
            //                },
            //                Name = "Brackets for Rail",
            //                Order = 7,
            //                PriceCalculationID = "MACP7"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "=ControllerType = \"NIC\"",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC8",
            //                DescriptionCalculation = "\"Controller Type\"",
            //                DescriptionCalculationID = "MACT8",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD8",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC17",
            //                        DescriptionCalculation = "\"Network Irrigation Controller\"",
            //                        DescriptionCalculationID = "MICT17",
            //                        DetailCalculationID = "MICD17",
            //                        Name = "Network Irrigation Controller",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP17"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC121",
            //                        DescriptionCalculation = "=\"NIC w/ Magnet Jobbing (A28 chip)\"",
            //                        DescriptionCalculationID = "MICT121",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD121",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "* Includes: 12 Magnets per system",
            //                                Order = 1,
            //                            }
            //                        },
            //                        Name = "NIC w/ Magnet Jobbing (A28 chip)",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP121"
            //                    }
            //                },
            //                Name = "NIC Controller Type",
            //                Order = 8,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP8"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "=ControllerType = \"WIC\"",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC123",
            //                DescriptionCalculation = "\"Controller Type\"",
            //                DescriptionCalculationID = "MACT123",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD123",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC122",
            //                        DescriptionCalculation = "=\"Compass Capture Controller Upgrade\"",
            //                        DescriptionCalculationID = "MICT122",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD122",
            //                        Name = "Compass Capture Controller Upgrade",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP122"
            //                    },
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC123",
            //                        DescriptionCalculation = "=\"WIC w/ Area Capture Program and Step/Stop/Water\"",
            //                        DescriptionCalculationID = "MICT123",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD123",
            //                        MinorComponentDetails = new List<MinorComponentDetail>
            //                        {
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "* Includes: 2 Magnets per system",
            //                                Order = 1,
            //                            },
            //                            new MinorComponentDetail
            //                            {
            //                                Name = "* Includes: Whisker Switch for Checkpoints",
            //                                Order = 2,
            //                            }
            //                        },
            //                        Name = "WIC w/ Area Capture Program and Step/Stop/Water",
            //                        Order = 2,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP123"
            //                    }
            //                },
            //                Name = "WIC Controller Type",
            //                Order = 8,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP123"
            //            }
            //        },
            //        Name = "General Parts",
            //        Order = 1
            //    },
            //    new Category
            //    {
            //        MajorComponents = new List<MajorComponent>
            //        {
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=WirelessWalkSwitchRequested = true",
            //                ComponentID = "MAC10",
            //                DescriptionCalculation = "\"Wireless Walk Switch - Easily Move Boom in Bay\"",
            //                DescriptionCalculationID = "MACT10",
            //                DetailCalculationID = "MACD10",
            //                Name = "Wireless Walk Switch - Easily Move Boom in Bay",
            //                Order = 2,
            //                PriceCalculation = "=285.00",
            //                PriceCalculationID = "MACP10"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=SingleOrDualWater = \"Dual\"",
            //                ComponentID = "MAC11",
            //                DescriptionCalculation = "\"Dual Water/Mist Upgrade\"",
            //                DescriptionCalculationID = "MACT11",
            //                DetailCalculationID = "MACD11",
            //                MinorComponents = new List<MinorComponent>
            //                {
            //                    new MinorComponent
            //                    {
            //                        ActivateComponentCondition = "true",
            //                        ComponentID = "MIC111",
            //                        DescriptionCalculation = "=\"* TeeJet \" & Tip2 & \" (0.2 gpm) 'Yellow' Tips\"",
            //                        DescriptionCalculationID = "MICT111",
            //                        DetailCalculation = "",
            //                        DetailCalculationID = "MICD111",
            //                        Name = "Tips",
            //                        Order = 1,
            //                        Price = 0,
            //                        PriceCalculationID = "MICP111"
            //                    }
            //                },
            //                Name = "Dual Water/Mist Upgrade",
            //                Order = 3,
            //                PriceCalculation = "=ROUNDUP((11 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2) + 25 ), 0)",
            //                PriceCalculationID = "MACP11"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=TripleStepLocklineUpgradeRequested = true",
            //                ComponentID = "MAC12",
            //                DescriptionCalculation = "\"Step / Stop / Water Spray Bar Upgrade\"",
            //                DescriptionCalculationID = "MACT12",
            //                DetailCalculationID = "MACD12",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD16",
            //                        DescriptionCalculation = "\"* TeeJet Bodies w/ Flexible Lock-Line assembly\"",
            //                        DescriptionCalculationID = "MADT16",
            //                        DetailCalculationID = "MADD16",
            //                        Name = "* TeeJet Bodies w/ Flexible Lock-Line assembly",
            //                        Order = 1,
            //                        PriceCalculationID = "MADP16"
            //                    },
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD17",
            //                        DescriptionCalculation = "\"(including individual needle valves for calibrating)\"",
            //                        DescriptionCalculationID = "MADT17",
            //                        DetailCalculationID = "MADD17",
            //                        Name = "(including individual needle valves for calibrating)",
            //                        Order = 2,
            //                        PriceCalculationID = "MADP17"
            //                    }
            //                },
            //                Name = "Step / Stop / Water Spray Bar Upgrade",
            //                Order = 4,
            //                PriceCalculation = "=ROUNDUP(22.5 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 ), 0)",
            //                PriceCalculationID = "MACP12"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=TripleTurretBodyRequested = true",
            //                ComponentID = "MAC13",
            //                DescriptionCalculation = "\"Triple Turret Spray Body Upgrade\"",
            //                DescriptionCalculationID = "MACT13",
            //                DetailCalculationID = "MACD13",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD18",
            //                        DescriptionCalculation = "\"Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *\"",
            //                        DescriptionCalculationID = "MADT18",
            //                        DetailCalculationID = "MADD18",
            //                        Name = "Includes: TeeJet XR11002 (0.2 gpm) 'Yellow' Tips/ XR11004 (0.4 gpm) 'Red' Tips/ XR11008 (0.8 gpm) 'White' Tips * this can be customized upon request *",
            //                        Order = 1,
            //                        PriceCalculationID = "MADP18"
            //                    }
            //                },
            //                Name = "Triple Turret Spray Body Upgrade",
            //                Order = 5,
            //                PriceCalculation = "=ROUNDUP((20 * (BayWidthInFeet * 12 / SprayBodySpacingInInches + 2 )), 0)",
            //                PriceCalculationID = "MACP13"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=PressureRegulatorRequested = true",
            //                ComponentID = "MAC14",
            //                DescriptionCalculation = "\"1\"\" Pressure Regulator & Gauge\"",
            //                DescriptionCalculationID = "MACT14",
            //                DetailCalculationID = "MACD14",
            //                Name = "1\" Pressure Regulator & Gauge",
            //                Order = 6,
            //                PriceCalculation = "=100.00",
            //                PriceCalculationID = "MACP14"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "InjectorType = \"Mounted\"",
            //                AddPriceCondition = "true",
            //                ComponentID = "MAC15",
            //                DescriptionCalculation = "\"Mounted Injector w/ Custom Manifold\"",
            //                DescriptionCalculationID = "MACT15",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD15",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD19",
            //                        DescriptionCalculation = "\"* Customer to determine Injector Size; prices may vary\"",
            //                        DescriptionCalculationID = "MADT19",
            //                        DetailCalculationID = "MADD19",
            //                        Name = "* Customer to determine Injector Size; prices may vary",
            //                        Order = 1,
            //                        PriceCalculationID = "MADP19"
            //                    },
            //                    new MajorComponentDetail
            //                    {
            //                        ComponentID = "MAD20",
            //                        DescriptionCalculation = "\"** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket\"",
            //                        DescriptionCalculationID = "MADT20",
            //                        DetailCalculationID = "MADD20",
            //                        Name = "** Includes Injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket",
            //                        Order = 2,
            //                        PriceCalculationID = "MADP20"
            //                    }
            //                },
            //                Name = "Mounted Injector w/ Custom Manifold",
            //                Order = 7,
            //                PriceCalculation = "=595.00",
            //                PriceCalculationID = "MACP15"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "InjectorType = \"Portable\"",
            //                AddPriceCondition = "true",
            //                ComponentID = "MAC139",
            //                DescriptionCalculation = "\"Portable Injector w/ Custom Manifold\"",
            //                DescriptionCalculationID = "MACT139",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD139",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        Name = "* Customer to determine Injector Size; prices may vary",
            //                        Order = 1
            //                    },
            //                    new MajorComponentDetail
            //                    {
            //                        Name = "** Includes portable injector mounted into manifold, Bucket Support Platform & 5 Gal. Bucket",
            //                        Order = 2
            //                    }
            //                },
            //                Name = "Portable Injector w/ Custom Manifold",
            //                Order = 7,
            //                PriceCalculation = "595",
            //                PriceCalculationID = "MACP139"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "InjectorType = \"Manifold Only\"",
            //                AddPriceCondition = "true",
            //                ComponentID = "MAC140",
            //                DescriptionCalculation = "\"Portable Injector Manifold\"",
            //                DescriptionCalculationID = "MACT140",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD140",
            //                MajorComponentDetails = new List<MajorComponentDetail>
            //                {
            //                    new MajorComponentDetail
            //                    {
            //                        Name = "Customer to determine Injector Size; prices may vary",
            //                        Order = 1,
            //                    },
            //                    new MajorComponentDetail
            //                    {
            //                        Name = "** Includes manifold for Portable Injector, Bucket Support Platform & 5 Gal. Bucket",
            //                        Order = 2
            //                    }
            //                },
            //                Name = "Portable Injector Manifold",
            //                Order = 7,
            //                PriceCalculation = "95",
            //                PriceCalculationID = "MACP140"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=HoseUpgradeRequested = true",
            //                ComponentID = "MAC16",
            //                DescriptionCalculation = "\"1 1/4\"\" Hose Upgrade (bays over 400')\"",
            //                DescriptionCalculationID = "MACT16",
            //                DetailCalculationID = "MACD16",
            //                Name = "1 1/4\" Hose Upgrade (bays over 400')",
            //                Order = 8,
            //                PriceCalculation = "=MACD5 * 2.25",
            //                PriceCalculationID = "MACP16"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "=Sweep90Requested = true",
            //                ComponentID = "MAC17",
            //                DescriptionCalculation = "\"90 Degree Hose Sweep Assembly\"",
            //                DescriptionCalculationID = "MACT17",
            //                DetailCalculationID = "MACD17",
            //                Name = "90 Degree Hose Sweep Assembly",
            //                Order = 9,
            //                PriceCalculation = "=165.00",
            //                PriceCalculationID = "MACP17"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "true",
            //                AddPriceCondition = "true",
            //                ComponentID = "MAC18",
            //                DescriptionCalculation = "\"[inch] Rail Drop Down Assembly\"",
            //                DescriptionCalculationID = "MACT18",
            //                DetailCalculation = "=RailDropDownASSYInInches",
            //                DetailCalculationID = "MACD18",
            //                Name = "[inch] Rail Drop Down Assembly",
            //                Order = 10,
            //                PriceCalculation = "=(MICD15*2)*(RailDropDownASSYInInches/12)*2",
            //                PriceCalculationID = "MACP18"
            //            },
            //            new MajorComponent
            //            {
            //                ActivateComponentCondition = "WaterSolenoidControlRequested = true",
            //                AddPriceCondition = "",
            //                ComponentID = "MAC131",
            //                DescriptionCalculation = "\"Feed and fresh water solenoid control\"",
            //                DescriptionCalculationID = "MACT131",
            //                DetailCalculation = "",
            //                DetailCalculationID = "MACD131",
            //                Name = "Feed and fresh water solenoid control",
            //                Order = 10,
            //                PriceCalculation = "",
            //                PriceCalculationID = "MACP131"
            //            }
            //        },
            //        Name = "Options and Upgrades",
            //        Order = 2
            //    }
            //},
            //                 ComponentID = "SYS",
            //                 DescriptionCalculation = "\"Sky Rail\"",
            //                 DescriptionCalculationID = "SYST",
            //                 DetailCalculationID = "SYSD",
            //                 Name = "Sky Rail",
            //                 PriceCalculation = "ROUNDUP(BaseValue + (BayLengthInFeet / 10 * 39.2 + BayWidthInFeet * 12 / SprayBodySpacingInInches * 7) * 1.1 \r\n, 0) * 1.175 + 750 \r\n",
            //                 PriceCalculationID = "SYSP"
            //             });

            #endregion Sky Rail System
        }

        private void AddCustomers(DataContext context)
        {
            context.Customers.AddOrUpdate(c => c.CompanyName,
                new Customer
                {
                    CompanyName = "Test Company",
                    ContactName = "Test Contact",
                    Email = "test@test.net",
                    Phone = "555-555-5555",
                    Fax = "777-777-7777",
                    ShipAddresses = new List<ShipAddress>
                    {
                        new ShipAddress
                        {
                            Name = "Warehouse 1",
                            Street1 = "123 Street",
                            City = "My City",
                            StateId = 4,
                            Zip = "80923"
                        },
                        new ShipAddress
                        {
                            Name = "",
                            Street1 = "567 Street",
                            City = "Another City",
                            StateId = 6,
                            Zip = "80921"
                        }
                    }
                });
        }

        private void AddGreenHouseTypes(DataContext context)
        {
            context.GreenHouseTypes.AddOrUpdate(g => g.Name,
                new GreenHouseType { Name = "Generic Hoop" },
                new GreenHouseType { Name = "Gutter Connect" },
                new GreenHouseType { Name = "Nexus" },
                new GreenHouseType { Name = "GGS" },
                new GreenHouseType { Name = "DeCloet" },
                new GreenHouseType { Name = "Rough Bro" },
                new GreenHouseType { Name = "Stuppy" },
                new GreenHouseType { Name = "Westbrook" },
                new GreenHouseType { Name = "Cravo" },
                new GreenHouseType { Name = "Conley" },
                new GreenHouseType { Name = "Atlas" },
                new GreenHouseType { Name = "Prins U.A." },
                new GreenHouseType { Name = "V.W.G. Co." },
                new GreenHouseType { Name = "Kubo" },
                new GreenHouseType { Name = "Crider Americas" },
                new GreenHouseType { Name = "ACME" });
        }

        private void AddSalesReps(DataContext context)
        {
            context.SalesReps.AddOrUpdate(sr => sr.Name,
                new SalesRep { Name = "WALTER DRUMMOND", Phone = "(719) 244-7200" },
                new SalesRep { Name = "ARIE VAN WINGERDEN", Phone = "(719) 491-5440" },
                new SalesRep { Name = "BFG SUPPLY CO.", Phone = "(720) 339-5848" },
                new SalesRep { Name = "CANADA DISTRIBUTION", Phone = string.Empty },
                new SalesRep { Name = "MISC. DISTRIBUTION", Phone = string.Empty });
        }

        private void AddStates(DataContext context)
        {
            State AL = new State { Abbreviation = "AL", Name = "Alabama" };
            State AK = new State { Abbreviation = "AK", Name = "Alaska" };
            State AZ = new State { Abbreviation = "AZ", Name = "Arizona" };
            State AR = new State { Abbreviation = "AR", Name = "Arkansas" };
            State CA = new State { Abbreviation = "CA", Name = "California" };
            State CO = new State { Abbreviation = "CO", Name = "Colorado" };
            State CT = new State { Abbreviation = "CT", Name = "Connecticut" };
            State DE = new State { Abbreviation = "DE", Name = "Delaware" };
            State FL = new State { Abbreviation = "FL", Name = "Florida" };
            State GA = new State { Abbreviation = "GA", Name = "Georgia" };
            State HI = new State { Abbreviation = "HI", Name = "Hawaii" };
            State ID = new State { Abbreviation = "ID", Name = "Idaho" };
            State IL = new State { Abbreviation = "IL", Name = "Illinois" };
            State IN = new State { Abbreviation = "IN", Name = "Indiana" };
            State IA = new State { Abbreviation = "IA", Name = "Iowa" };
            State KS = new State { Abbreviation = "KS", Name = "Kansas" };
            State KY = new State { Abbreviation = "KY", Name = "Kentucky" };
            State LA = new State { Abbreviation = "LA", Name = "Louisiana" };
            State ME = new State { Abbreviation = "ME", Name = "Maine" };
            State MD = new State { Abbreviation = "MD", Name = "Maryland" };
            State MA = new State { Abbreviation = "MA", Name = "Massachussetts" };
            State MI = new State { Abbreviation = "MI", Name = "Michigan" };
            State MN = new State { Abbreviation = "MN", Name = "Minnesota" };
            State MS = new State { Abbreviation = "MS", Name = "Mississippi" };
            State MO = new State { Abbreviation = "MO", Name = "Missouri" };
            State MT = new State { Abbreviation = "MT", Name = "Montana" };
            State NE = new State { Abbreviation = "NE", Name = "Nebraska" };
            State NV = new State { Abbreviation = "NV", Name = "Nevada" };
            State NH = new State { Abbreviation = "NH", Name = "New Hampshire" };
            State NJ = new State { Abbreviation = "NJ", Name = "New Jersey" };
            State NM = new State { Abbreviation = "NM", Name = "New Mexico" };
            State NY = new State { Abbreviation = "NY", Name = "New York" };
            State NC = new State { Abbreviation = "NC", Name = "North Carolina" };
            State ND = new State { Abbreviation = "ND", Name = "North Dakota" };
            State OH = new State { Abbreviation = "OH", Name = "Ohio" };
            State OK = new State { Abbreviation = "OK", Name = "Oklahoma" };
            State OR = new State { Abbreviation = "OR", Name = "Oregon" };
            State PA = new State { Abbreviation = "PA", Name = "Pennsylvania" };
            State RI = new State { Abbreviation = "RI", Name = "Rhode Island" };
            State SC = new State { Abbreviation = "SC", Name = "South Carolina" };
            State SD = new State { Abbreviation = "SD", Name = "South Dakota" };
            State TN = new State { Abbreviation = "TN", Name = "Tennessee" };
            State TX = new State { Abbreviation = "TX", Name = "Texas" };
            State UT = new State { Abbreviation = "UT", Name = "Utah" };
            State VT = new State { Abbreviation = "VT", Name = "Vermont" };
            State VA = new State { Abbreviation = "VA", Name = "Virginia" };
            State WA = new State { Abbreviation = "WA", Name = "Washington" };
            State WV = new State { Abbreviation = "WV", Name = "West Virginia" };
            State WI = new State { Abbreviation = "WI", Name = "Wisconsin" };
            State WY = new State { Abbreviation = "WY", Name = "Wyoming" };
            State BC = new State { Abbreviation = "BC", Name = "British Columbia" };
            State DC = new State { Abbreviation = "DC", Name = "District of Columbia" };

            context.States.Add(AL);
            context.States.Add(AK);
            context.States.Add(AZ);
            context.States.Add(AR);
            context.States.Add(CA);
            context.States.Add(CO);
            context.States.Add(CT);
            context.States.Add(DE);
            context.States.Add(FL);
            context.States.Add(GA);
            context.States.Add(HI);
            context.States.Add(ID);
            context.States.Add(IL);
            context.States.Add(IN);
            context.States.Add(IA);
            context.States.Add(KS);
            context.States.Add(KY);
            context.States.Add(LA);
            context.States.Add(ME);
            context.States.Add(MD);
            context.States.Add(MA);
            context.States.Add(MI);
            context.States.Add(MN);
            context.States.Add(MS);
            context.States.Add(MO);
            context.States.Add(MT);
            context.States.Add(NE);
            context.States.Add(NV);
            context.States.Add(NH);
            context.States.Add(NJ);
            context.States.Add(NM);
            context.States.Add(NY);
            context.States.Add(NC);
            context.States.Add(ND);
            context.States.Add(OH);
            context.States.Add(OK);
            context.States.Add(OR);
            context.States.Add(PA);
            context.States.Add(RI);
            context.States.Add(SC);
            context.States.Add(SD);
            context.States.Add(TN);
            context.States.Add(TX);
            context.States.Add(UT);
            context.States.Add(VT);
            context.States.Add(VA);
            context.States.Add(WA);
            context.States.Add(WV);
            context.States.Add(WI);
            context.States.Add(WY);
            context.States.Add(BC);
            context.States.Add(DC);
        }
    }
}