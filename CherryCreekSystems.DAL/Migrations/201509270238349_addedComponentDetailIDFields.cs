namespace CherryCreekSystems.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedComponentDetailIDFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MajorComponentDetail", "DetailCalculation", c => c.String());
            AddColumn("dbo.MinorComponentDetail", "DetailCalculation", c => c.String());
            AddColumn("dbo.QuoteMajorComponentDetail", "DetailCalculation", c => c.String());
            AddColumn("dbo.QuoteMajorComponentDetail", "DetailCalculationResult", c => c.String());
            AddColumn("dbo.QuoteMinorComponentDetail", "DetailCalculation", c => c.String());
            AddColumn("dbo.QuoteMinorComponentDetail", "DetailCalculationResult", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuoteMinorComponentDetail", "DetailCalculationResult");
            DropColumn("dbo.QuoteMinorComponentDetail", "DetailCalculation");
            DropColumn("dbo.QuoteMajorComponentDetail", "DetailCalculationResult");
            DropColumn("dbo.QuoteMajorComponentDetail", "DetailCalculation");
            DropColumn("dbo.MinorComponentDetail", "DetailCalculation");
            DropColumn("dbo.MajorComponentDetail", "DetailCalculation");
        }
    }
}
