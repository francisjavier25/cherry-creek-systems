﻿using CherryCreekSystems.Domain;
using System.Linq;

namespace CherryCreekSystems.DAL.QueryExtensions
{
    public static class SalesRepExtensions
    {
        public static IQueryable<SalesRep> GetOrdered(this IQueryable<SalesRep> query)
        {
            return query.OrderBy(c => c.Name);
        }

        public static IQueryable<SalesRepUser> BySearch(this IQueryable<SalesRepUser> query, string search)
        {
            search = (search ?? string.Empty).ToLower().Trim();
            return query.Where(d => string.IsNullOrEmpty(search) || d.Name.ToLower().Contains(search)
                                                                 || d.UserName.ToLower().Contains(search)
                                                                 );
        }

        public static IQueryable<SalesRepUser> GetOrdered(this IQueryable<SalesRepUser> query)
        {
            return query.OrderBy(d => d.Name);
        }
    }
}