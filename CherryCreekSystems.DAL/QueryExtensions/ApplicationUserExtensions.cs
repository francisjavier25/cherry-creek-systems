﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.DAL.QueryExtensions
{
    public static class ApplicationUserExtensions
    {
        public static async Task<bool> IsAdmin(this ApplicationUser user)
        {
            await GetRoles(user);

            return user.ApplicationRoles.Any(r => r.RoleName.Equals("administrator", StringComparison.OrdinalIgnoreCase));
        }

        public static async Task<bool> IsDistributor(this ApplicationUser user)
        {
            await GetRoles(user);

            return user.ApplicationRoles.Any(r => r.RoleName.Equals("distributor", StringComparison.OrdinalIgnoreCase));
        }

        public static async Task<bool> IsCustomer(this ApplicationUser user)
        {
            await GetRoles(user);

            return user.ApplicationRoles.Any(r => r.RoleName.Equals("customer", StringComparison.OrdinalIgnoreCase));
        }

        public static async Task<bool> IsSalesRep(this ApplicationUser user)
        {
            await GetRoles(user);

            return user.ApplicationRoles.Any(r => r.RoleName.Equals("salesrep", StringComparison.OrdinalIgnoreCase));
        }

        public static async Task<IEnumerable<int>> DistributorIds(this ApplicationUser user)
        {
            await GetRoles(user);

            return GetIds(user, "distributor");
        }

        public static async Task<IEnumerable<int>> CustomerIds(this ApplicationUser user)
        {
            await GetRoles(user);

            return GetIds(user, "customer");
        }

        public static async Task<IEnumerable<int>> SalesRepIds(this ApplicationUser user)
        {
            await GetRoles(user);

            return GetIds(user, "salesrep");
        }

        private static IEnumerable<int> GetIds(ApplicationUser user, string role)
        {
            return user.ApplicationRoles.Where(r => r.RoleName.Equals(role, StringComparison.OrdinalIgnoreCase) && r.ApplicationUserId.HasValue)
                                        .Select(r => r.ApplicationUserId.Value);
        }

        private static async Task GetRoles(ApplicationUser user)
        {
            if (user.ApplicationRoles == null)
            {
                using (var context = new DataContext())
                {
                    var roles = await context.Roles.ToListAsync();
                    user.ApplicationRoles = user.Roles.Join(roles, ar => ar.RoleId, r => r.Id, (ur, ar) => new Role
                    {
                        RoleName = ar.Name,
                        ApplicationUserId = ur.ApplicationUserId
                    }).ToList();
                }
            }
        }
    }
}