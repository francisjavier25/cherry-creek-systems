﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CherryCreekSystems.Domain;

namespace CherryCreekSystems.DAL.QueryExtensions
{
    public static class QuoteStatusExtensions
    {
        public static IQueryable<QuoteStatus> GetOrdered(this IQueryable<QuoteStatus> query)
        {
            return query.OrderBy(s => s.Name);
        }

        public static IQueryable<QuoteRequest> ByQuoteStatus(this IQueryable<QuoteRequest> query, int? quoteStatusId)
        {
            return query.Where(q => !quoteStatusId.HasValue || q.QuoteStatusId == quoteStatusId);
        }
    }
}
