﻿using CherryCreekSystems.Domain;
using System.Linq;

namespace CherryCreekSystems.DAL.QueryExtensions
{
    public static class DistributorExtensions
    {
        public static IQueryable<Distributor> GetOrdered(this IQueryable<Distributor> query)
        {
            return query.OrderBy(d => d.Name);
        }

        public static IQueryable<DistributorUser> BySearch(this IQueryable<DistributorUser> query, string search)
        {
            search = (search ?? string.Empty).ToLower().Trim();
            return query.Where(d => string.IsNullOrEmpty(search) || d.Name.ToLower().Contains(search)
                                                                 || d.UserName.ToLower().Contains(search)
                                                                 );
        }

        public static IQueryable<DistributorUser> GetOrdered(this IQueryable<DistributorUser> query)
        {
            return query.OrderBy(d => d.Name);
        }
    }
}