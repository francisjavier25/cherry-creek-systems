﻿using CherryCreekSystems.Domain;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.DAL.QueryExtensions
{
    public static class QuoteRequestExtensions
    {
        public static IQueryable<QuoteRequest> GetOrderedByDate(this IQueryable<QuoteRequest> query,
            bool includeCustomer = false, bool includeDistributor = false, bool includeQuoteStatus = false,
            bool includeSalesRep = false)
        {
            if (includeCustomer)
                query = query.Include(q => q.Customer);

            if (includeDistributor)
                query = query.Include(q => q.Distributor);

            if (includeQuoteStatus)
                query = query.Include(q => q.QuoteStatus);

            if (includeSalesRep)
                query = query.Include(q => q.SalesRep);

            query = query.Include(c => c.CustomQuoteItems);

            return query
                   .OrderByDescending(q => q.OrderDate);
        }

        public static Task<QuoteRequest> GetByid(this IQueryable<QuoteRequest> query, int id)
        {
            return query.FirstOrDefaultAsync(q => q.Id == id);
        }

        public static IQueryable<QuoteRequest> ByDistributor(this IQueryable<QuoteRequest> query, int? distributorId)
        {
            return query.Where(q => !distributorId.HasValue || q.DistributorId == distributorId);
        }

        public static IQueryable<QuoteRequest> ByCustomer(this IQueryable<QuoteRequest> query, int? customerId)
        {
            return query.Where(q => !customerId.HasValue || q.CustomerId == customerId);
        }

        public static IQueryable<QuoteRequest> BySalesRep(this IQueryable<QuoteRequest> query, int? salesRepId)
        {
            return query.Where(q => !salesRepId.HasValue || q.SalesRepId == salesRepId);
        }

        public static IQueryable<QuoteRequest> GetActive(this IQueryable<QuoteRequest> query)
        {
            return query.Where(q => q.Active);
        }
    }
}