﻿using CherryCreekSystems.Domain;
using CherryCreekSystems.Domain.BoomComponents;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CherryCreekSystems.DAL.QueryExtensions
{
    public static class QuoteBoomSystemExtensions
    {
        public static IQueryable<QuoteBoomSystem> ListByQuoteId(this IQueryable<QuoteBoomSystem> query, int id)
        {
            return query.Where(q => q.QuoteRequestId == id);
        }

        public static async Task<IEnumerable<ProgressViewModel>> GetProgressViewModel(this IQueryable<QuoteBoomSystem> query, string searchTerm = "")
        {
            searchTerm = (searchTerm ?? string.Empty).Trim().ToLower();
            var statuses = new[] { 1, 2 }; //Active, In Progress
            var result = await query.Where(s => statuses.Contains(s.QuoteRequest.QuoteStatusId)
                                                && s.QuoteRequest.Active && s.Active
                                                && (string.IsNullOrEmpty(searchTerm) ||
                                                    (s.QuoteRequest.Customer.CompanyName.ToLower().Contains(searchTerm) ||
                                                     s.Name.ToLower().Contains(searchTerm) ||
                                                     s.BoomId.ToString() == searchTerm.ToString()
                                                    )
                                                    )
                                           )
                                   .Select(s => new ProgressViewModel
                                   {
                                       CustomerName = s.QuoteRequest.Customer.CompanyName,
                                       QuoteRequestId = s.QuoteRequestId,
                                       BoomId = s.BoomId,
                                       SystemId = s.Id,
                                       BoomType = (BoomType)s.BoomTypeId,
                                       SystemName = s.DetailCalculationResult + " " + s.Name,
                                       Quantity = s.Quantity,

                                       MachiningThreshold = s.Boom.MachiningThreshold,
                                       WeldingThreshold = s.Boom.WeldingThreshold,
                                       ProductionThreshold = s.Boom.ProductionThreshold,
                                       ShippingPickListThreshold = s.Boom.ShippingPickListThreshold,
                                       SawAndSheerThreshold = s.Boom.SawAndSheerThreshold,
                                       AssemblyPickListThreshold = s.Boom.AssemblyPickListThreshold,
                                       PunchAndBreakThreshold = s.Boom.PunchAndBreakThreshold,
                                       AssemblyThreshold = s.Boom.AssemblyThreshold,
                                       SprayBarApprovalThreshold = s.Boom.SprayBarApprovalThreshold,
                                       GalganizerThreshold = s.Boom.GalganizerThreshold,
                                       ShippingThreshold = s.Boom.ShippingThreshold,
                                       InfoAndDesignThreshold = s.Boom.InfoAndDesignThreshold,

                                       MachiningStatus = s.Boom.MachiningStatus,
                                       WeldingStatus = s.Boom.WeldingStatus,
                                       ProductionStatus = s.Boom.ProductionStatus,
                                       ShippingPickListStatus = s.Boom.ShippingPickListStatus,
                                       SawAndSheerStatus = s.Boom.SawAndSheerStatus,
                                       AssemblyPickListStatus = s.Boom.AssemblyPickListStatus,
                                       PunchAndBreakStatus = s.Boom.PunchAndBreakStatus,
                                       AssemblyStatus = s.Boom.AssemblyStatus,
                                       SprayBarApprovalStatus = s.Boom.SprayBarApprovalStatus,
                                       GalganizerStatus = s.Boom.GalganizerStatus,
                                       ShippingStatus = s.Boom.ShippingStatus,
                                       InfoAndDesignStatus = s.Boom.InfoAndDesignStatus,
                                       //ControlType = "request",
                                       OrderDate = s.QuoteRequest.OrderDate,
                                       DueDate = s.QuoteRequest.DueDate
                                   }).ToListAsync();
            return result;
        }

        public static void CalculateSystemsProgress(this IEnumerable<QuoteBoomSystem> list)
        {
            foreach (var system in list)
            {
                var boom = system.Boom;
                if (boom == null) continue;

                var informationAndDesignAverage = boom.InfoAndDesignStatus;
                var engineeringAverage = boom.SprayBarApprovalStatus;
                var productionAverage = (boom.AssemblyPickListStatus + boom.ShippingPickListStatus
                                         + boom.ProductionStatus + boom.SawAndSheerStatus + boom.PunchAndBreakStatus
                                         + boom.MachiningStatus
                                         + boom.WeldingStatus) / 7;
                var galvanizerAverage = boom.GalganizerStatus;
                var assemblyAverage = boom.AssemblyStatus;
                var shippingAverage = boom.ShippingStatus;

                var progressData = new List<ProgressData>
                {
                    new ProgressData {value = informationAndDesignAverage.ToString(), userColor = "#d3d3d3"},
                    new ProgressData {value = engineeringAverage.ToString(), userColor = "#add8e6"},
                    new ProgressData {value = productionAverage.ToString(), userColor = "#ffff00"},
                    new ProgressData {value = galvanizerAverage.ToString(), userColor = "#ff69b4"},
                    new ProgressData {value = assemblyAverage.ToString(), userColor = "#ff0000"},
                    new ProgressData {value = shippingAverage.ToString(), userColor = "#00ff00"}
                };

                system.StatusData = JsonConvert.SerializeObject(progressData);
            }
        }

        public static Task<QuoteBoomSystem> GetByid(this IQueryable<QuoteBoomSystem> query, int id)
        {
            return query.FirstOrDefaultAsync(q => q.Id == id);
        }
    }

    public class ProgressData
    {
        public string value { get; set; }
        public string userColor { get; set; }
    }
}