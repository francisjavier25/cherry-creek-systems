﻿using CherryCreekSystems.Domain;
using System.Linq;

namespace CherryCreekSystems.DAL.QueryExtensions
{
    public static class CustomerExtensions
    {
        public static IQueryable<Customer> GetOrdered(this IQueryable<Customer> query)
        {
            return query.OrderBy(c => c.CompanyName);
        }

        public static IQueryable<Customer> ByDistributorIds(this IQueryable<Customer> query, params int?[] distributorIds)
        {
            return query.Where(c => c.QuoteRequests.Any(q => distributorIds.Contains(q.DistributorId)));
        }

        public static IQueryable<Customer> BySalesRepIds(this IQueryable<Customer> query, params int?[] salesRepIds)
        {
            return query.Where(c => c.QuoteRequests.Any(q => salesRepIds.Contains(q.SalesRepId)));
        }

        public static IQueryable<CustomerUser> BySearch(this IQueryable<CustomerUser> query, string search)
        {
            search = (search ?? string.Empty).ToLower().Trim();
            return query.Where(d => string.IsNullOrEmpty(search) || d.CompanyName.ToLower().Contains(search)
                                                                 || d.ContactName.ToLower().Contains(search)
                                                                 || d.Email.ToLower().Contains(search)
                                                                 || d.Fax.ToLower().Contains(search)
                                                                 || d.Phone.ToLower().Contains(search)
                                                                 || d.UserName.ToLower().Contains(search)
                                                                 );
        }

        public static IQueryable<CustomerUser> GetOrdered(this IQueryable<CustomerUser> query)
        {
            return query.OrderBy(d => d.CompanyName);
        }
    }
}