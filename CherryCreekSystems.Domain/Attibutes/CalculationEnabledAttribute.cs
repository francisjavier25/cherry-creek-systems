﻿using System;

namespace CherryCreekSystems.Domain
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CalculationEnabled : Attribute
    {
    }
}