﻿namespace CherryCreekSystems.Domain
{
    public interface ISimpleEntity
    {
        int Id { get; set; }
        string Name { get; set; }
    }

    public interface ISimpleEntityViewModel
    {
        int? Id { get; set; }
        string Name { get; set; }
    }

    public class SimpleEntityViewModel : ISimpleEntityViewModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }

    public class GreenhouseViewModel : SimpleEntityViewModel { }
    public class SalesRepViewModel : SimpleEntityViewModel { }
    public class DistributorViewModel : SimpleEntityViewModel { }
    public class QuoteStatusViewModel : SimpleEntityViewModel { }
}