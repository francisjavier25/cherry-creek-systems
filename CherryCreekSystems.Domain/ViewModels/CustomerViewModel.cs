﻿using System.Collections.Generic;

namespace CherryCreekSystems.Domain.ViewModels
{
    public class CustomerViewModel
    {
        public int? Id { get; set; } 

        public string CompanyName { get; set; } = string.Empty;

        public string ContactName { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;

        public string Fax { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public IEnumerable<ShipAddressViewModel> ShipAddresses { get; set; }
    }

    public class ShipAddressViewModel
    {
        public int Id { get; set; }

        public string FullAddress { get; set; }
    }
}