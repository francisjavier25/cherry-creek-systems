﻿using System.Collections.Generic;
using CherryCreekSystems.Domain.BoomComponents;

namespace CherryCreekSystems.Domain.ViewModels
{
    public class QuoteViewModel
    {
        public int QuoteId { get; set; }
        public int SelectedQuoteStatusId { get; set; }
        public int? SelectedCustomerId { get; set; }
        public int SelectedDistributorId { get; set; }
        public int? SelectedBoomType { get; set; }
        public int? SelectedSalesRepId { get; set; }
        public int? SelectedGreenhouseTypeId { get; set; }
        public int? SelectedShipAddressId { get; set; }
        public string SelectedControllerType { get; set; }
        public int? SelectedNumberOfLayers { get; set; }
        public double? SelectedPulleyBracketSpacing { get; set; }
        public string SelectedFeed { get; set; }

        public List<QuoteBoomSystem> Systems { get; set; }

        //public List<BoomTypeStructure> BoomTypes { get; set; }
        //public List<QuoteStatusViewModel> Statuses { get; set; }
        //public List<DistributorViewModel> Distributors { get; set; }
        //public List<CustomerViewModel> Customers { get; set; }
        //public List<SalesRepViewModel> SalesReps { get; set; }
        //public List<GreenhouseViewModel> GreenhouseTypes { get; set; }
        //public List<string> DrumSizes { get; set; }
        //public List<string> EchoSelectedControllerTypes { get; set; }
        //public List<string> NumberOfLayers { get; set; }
        //public List<string> PulleyBracketSpacings { get; set; }
        //public List<string> Feeds { get; set; }

        public string QuoteNumber { get; set; }
        public double QuoteTotal { get; set; }
        public string ShipAddress { get; set; }
        public int? LeadTime { get; set; }
        public string PurchaseOrder { get; set; }
        public string Discount { get; set; }
        public double? PriceLevel { get; set; }

        public int? NumberOfBays { get; set; }
        public double? BayLength { get; set; }
        public double? BayWidth { get; set; }
        public double? HoseSize { get; set; }
        public double? GrowingSurfaceHeight { get; set; }
        public double? PostSpacing { get; set; }
        public string PostDimensions { get; set; }

        public string FreightEstimate { get; set; }
        public string InstallationEstimate { get; set; }
        public string SteelEstimate { get; set; }

        public string OrderDate { get; set; }
        public string DueDate { get; set; }
        public double? SprayBarSize { get; set; }
        public int? NumberOfBaySidewalls { get; set; }
        public string BottomCordType { get; set; }
        public string BottomCordSize { get; set; }
        public double? BottomCordHeight { get; set; }
        public string Location { get; set; }
        public string AdditionalInformation { get; set; }
        public string CustomerNotes { get; set; }

        public double? TrussSpacing { get; set; }
        public double? WalkwayWidth { get; set; }
        public double? ClearanceWidth { get; set; }

        public int? QuantityOfSystems { get; set; }
        public int? SelectedDrumSize { get; set; }
        public double? BasketSpacing { get; set; }
        public bool CenterBayWaterStation { get; set; }
        public bool EvVpdThirdPartyControl { get; set; }
        public bool ThreeFourthAMIADFilterAssembly { get; set; }
        public bool LockLine { get; set; }
        public double? ExtensionHangerLength { get; set; }
        public bool RemotePullChainSwitch { get; set; }
        public bool DRAMMRedheadBreakerUpgrade { get; set; }
        public bool WaterSolenoidControl { get; set; }

        public CustomerViewModel Customer { get; set; }
        public string FullAddress { get; set; }
    }
}