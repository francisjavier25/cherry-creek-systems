﻿using CherryCreekSystems.Domain.BoomComponents;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using Newtonsoft.Json;

namespace CherryCreekSystems.Domain
{
    public class QuoteRequest
    {
        public QuoteRequest()
        {
            CustomQuoteItems = new List<CustomQuoteItem>();
            QuoteBoomSystemsData = new QuoteBoomSystemData { QuoteBoomSystems = new List<QuoteBoomSystem>() };
        }

        public int Id { get; set; }

        private int _revision;
        public int Revision
        {
            get
            {
                return _revision;
            }
            set
            {
                if (QuoteStatusId == 1) //Active status
                {
                    _revision++;
                }
            }
        }

        public string QuoteNumber { get; set; }

        public string QuoteNumberWithRevision
        {
            get
            {
                if (Revision != 0)
                {
                    return $"{QuoteNumber}-{Revision}";
                }

                return $"{QuoteNumber}";
            }
        }

        public virtual Customer Customer { get; set; }

        public virtual int CustomerId { get; set; }

        public virtual string ContactName { get; set; }

        public virtual string Phone { get; set; }

        public virtual string Fax { get; set; }

        public virtual string Email { get; set; }

        public virtual string FullAddress { get; set; }

        public virtual ShipAddress ShipAddress { get; set; }

        public virtual int? ShipAddressId { get; set; }

        public virtual SalesRep SalesRep { get; set; }

        public virtual int SalesRepId { get; set; }

        public virtual Distributor Distributor { get; set; }

        public virtual int DistributorId { get; set; }

        #region not mapped to database

        public double FreightEstimateValue
        {
            get
            {
                double value;
                var isValidValue = double.TryParse(FreightEstimate, out value);
                if (isValidValue)
                    return value;

                return 0;
            }
        }

        public double InstallationEstimateValue
        {
            get
            {
                double value;
                var isValidValue = double.TryParse(InstallationEstimate, out value);
                if (isValidValue)
                    return value;

                return 0;
            }
        }

        public double SteelEstimateValue
        {
            get
            {
                double value;
                var isValidValue = double.TryParse(SteelEstimate, out value);
                if (isValidValue)
                    return value;

                return 0;
            }
        }

        public double DiscountValue
        {
            get
            {
                double value;
                var isValidValue = double.TryParse(Discount, out value);
                if (isValidValue)
                    return -Math.Abs(value);

                return 0;
            }
        }

        //public string ExpectedShipmentDate
        //{
        //    get
        //    {
        //        #region invalid cases

        //        if (string.IsNullOrEmpty(LeadTime)) return string.Empty;

        //        var leadTime = Regex.Replace(LeadTime.ToLower(), @"\s+", " "); //  Remove multiple spaces
        //        var periodValues = leadTime.Split(' ');

        //        if (periodValues.Length == 2)
        //        {
        //            var periodValue = periodValues[0];
        //            var period = periodValues[1];

        //            int periodsToAdd;
        //            if (!int.TryParse(periodValue, out periodsToAdd)) return string.Empty;

        //            #endregion

        //            string[] weeks = { "week", "weeks" };
        //            string[] months = { "month", "months" };
        //            string[] days = { "day", "days" };

        //            DateTime dateValue;

        //            //Add weeks from current date
        //            if (weeks.Contains(period))
        //            {
        //                periodsToAdd = periodsToAdd * 7; //Convert weeks to days
        //                dateValue = DateTime.Now.AddDays(periodsToAdd);
        //                return dateValue.ToString("MM/dd/yyyy");
        //            }

        //            //Add months from current date
        //            if (months.Contains(period))
        //            {
        //                dateValue = DateTime.Now.AddMonths(periodsToAdd);
        //                return dateValue.ToString("MM/dd/yyyy");
        //            }

        //            //Add days from current date
        //            if (days.Contains(period))
        //            {
        //                //Add days from current date
        //                dateValue = DateTime.Now.AddDays(periodsToAdd);
        //                return dateValue.ToString("MM/dd/yyyy");
        //            }
        //        }
        //        else if (periodValues.Length == 1) //If only number, default to weeks
        //        {
        //            var periodValue = periodValues[0];
        //            int periodsToAdd;
        //            if (!int.TryParse(periodValue, out periodsToAdd)) return string.Empty;
        //            periodsToAdd = periodsToAdd * 7; //Convert weeks to days

        //            var dateValue = DateTime.Now.AddDays(periodsToAdd);
        //            return dateValue.ToString("MM/dd/yyyy");
        //        }

        //        return string.Empty;
        //    }
        //}

        #endregion not mapped to database

        public bool Active { get; set; }

        public virtual int QuoteStatusId { get; set; }

        public virtual QuoteStatus QuoteStatus { get; set; }

        public virtual DateTime StatusDate { get; set; }

        public virtual DateTime OrderDate { get; set; }

        public virtual DateTime DueDate { get; set; }

        public virtual string LeadTime { get; set; }

        public virtual string FreightEstimate { get; set; }

        public virtual string InstallationEstimate { get; set; }

        public virtual string SteelEstimate { get; set; }

        public virtual string Discount { get; set; }

        //Really a discount percentage that will apply only to the systems
        public virtual double PriceLevel { get; set; }

        public virtual string PurchaseOrder { get; set; }

        public virtual QuoteBoomSystemData QuoteBoomSystemsData { get; set; }

        public virtual List<CustomQuoteItem> CustomQuoteItems { get; set; }
    }

    public class QuoteBoomSystemData
    {
        public List<QuoteBoomSystem> QuoteBoomSystems { get; set; }

        public bool MarkDirty { get; set; }

        public void Update()
        {
            MarkDirty = !MarkDirty;
        }

        public byte[] Serialized /*{ get; set; }*/
        {
            get
            {
                var json = JsonConvert.SerializeObject(QuoteBoomSystems);
                var bytes = Encoding.UTF8.GetBytes(json);

                using (var msi = new MemoryStream(bytes))
                {
                    using (var mso = new MemoryStream())
                    {
                        using (var gs = new GZipStream(mso, CompressionMode.Compress))
                        {
                            msi.CopyTo(gs);
                        }

                        return mso.ToArray();
                    }
                }
            }
            set
            {
                if (value == null) return;

                string json;
                using (var msi = new MemoryStream(value))
                {
                    using (var mso = new MemoryStream())
                    {
                        using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                        {
                            gs.CopyTo(mso);
                        }

                        json = Encoding.UTF8.GetString(mso.ToArray());
                    }
                }

                if (string.IsNullOrEmpty(json)) return;

                var data = JsonConvert.DeserializeObject<List<QuoteBoomSystem>>(json);

                QuoteBoomSystems = data ?? new List<QuoteBoomSystem>();
            }
        }
    }
}