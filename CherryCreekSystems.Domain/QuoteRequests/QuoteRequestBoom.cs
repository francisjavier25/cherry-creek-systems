﻿namespace CherryCreekSystems.Domain
{
    //Used to map quotes to their available booms (for lookups)
    public class QuoteRequestBoom
    {
        public int Id { get; set; }

        public int QuoteRequestId { get; set; }

        public QuoteRequest QuoteRequest { get; set; }

        public int BoomTypeId { get; set; }
    }
}
