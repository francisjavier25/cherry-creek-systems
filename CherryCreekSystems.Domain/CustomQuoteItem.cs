﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CherryCreekSystems.Domain
{
    public class CustomQuoteItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public bool Active { get; set; }

        //Parent object
        public int QuoteRequest_Id { get; set; }
        [ForeignKey("QuoteRequest_Id")]
        public QuoteRequest QuoteRequest { get; set; }
    }
}