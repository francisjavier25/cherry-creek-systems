﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class QuoteMajorComponent : BaseComponent, IDetailCalculatedComponent, IDescriptionCalculatedComponent, IPriceCalculatedComponent, INamedAndOrderedComponent, IActivatedCalculatedComponent, IVisible
    {
        public QuoteMajorComponent()
        {
            QuoteMinorComponents = new List<QuoteMinorComponent>();
            QuoteMajorComponentDetails = new List<QuoteMajorComponentDetail>();
        }

        public string AddPriceCondition { get; set; }

        public string PriceCalculation { get; set; }

        public double CalculatedPrice { get; set; }

        public double AssignedPrice { get; set; }

        //Children
        public List<QuoteMinorComponent> QuoteMinorComponents { get; set; }

        public List<QuoteMajorComponentDetail> QuoteMajorComponentDetails { get; set; }

        public string ComponentID { get; set; }

        public string DetailCalculationID { get; set; }

        public string DescriptionCalculationID { get; set; }

        public string PriceCalculationID { get; set; }

        public string ActivateComponentCondition { get; set; }

        public bool OverrideComponentData { get; set; }

        public bool IncludeInBuildDocument { get; set; }

        //Gets activated when a calculation result determines that the component should (or not) be shown
        public bool IsActive { get; set; }

        //Acts as isDeleted
        public bool IsVisible { get; set; }

        [JsonIgnore]
        public string Display { get { return Name; } }
    }
}