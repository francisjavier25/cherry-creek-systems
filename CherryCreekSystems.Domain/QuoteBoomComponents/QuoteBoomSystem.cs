﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class QuoteBoomSystem : IDetailCalculatedComponent, IDescriptionCalculatedComponent, IPriceCalculatedComponent
    {
        public QuoteBoomSystem()
        {
            QuoteCategories = new List<QuoteCategory>();
            SteelMaterials = new List<SteelMaterial>();
        }

        public void GenerateID()
        {
            Id_ = Guid.NewGuid().ToString();
        }

        public string Id_ { get; set; }

        public string SugarCRMId { get; set; }

        public int Order { get; set; }

        public string Name { get; set; }

        public string DescriptionCalculation { get; set; }

        public string DescriptionCalculationResult { get; set; }

        public string DetailCalculation { get; set; }

        public string DetailCalculationResult { get; set; }

        public string AddPriceCondition { get; set; }

        public string PriceCalculation { get; set; }

        public double CalculatedPrice { get; set; }

        public double AssignedPrice { get; set; }

        public int Quantity { get; set; }

        //This id maps to the BoomType enum
        //public  int BoomTypeId { get; set; }

        [JsonIgnore]
        public string SystemName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(DetailCalculationResult))
                    return $"{DetailCalculationResult} {Name}";

                return Name;
            }
        }

        public int Number { get; set; }

        [JsonIgnore]
        public string SystemNameForList
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(DetailCalculationResult))
                    return $"{Number}-{DetailCalculationResult} {Name}";

                return $"{Number}-{Name}";
            }
        }

        [JsonIgnore]
        public string StatusData { get; set; }

        public bool OverrideSteelMaterials { get; set; }

        public List<SteelMaterial> SteelMaterials { get; set; }

        public Boom Boom { get; set; }

        public List<QuoteCategory> QuoteCategories { get; set; }

        public string ComponentID { get; set; }

        public string DetailCalculationID { get; set; }

        public string DescriptionCalculationID { get; set; }

        public string PriceCalculationID { get; set; }

        public bool OverrideComponentData { get; set; }

        public bool IncludeInBuildDocument { get; set; }

        public bool Active { get; set; }
    }

    public class QuoteSystemResult
    {
        public QuoteBoomSystem System { get; set; }
        public List<string> CalculationErrors { get; set; }
    }
}