﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class QuoteMinorComponent : BaseComponent, IDetailCalculatedComponent, IDescriptionCalculatedComponent, IPriceCalculatedComponent, INamedAndOrderedComponent, IActivatedCalculatedComponent, IVisible
    {
        public QuoteMinorComponent()
        {
            QuoteMinorComponentDetails = new List<QuoteMinorComponentDetail>();
        }

        public string AddPriceCondition { get; set; }

        public string PriceCalculation { get; set; }

        public double CalculatedPrice { get; set; }

        public double AssignedPrice { get; set; }

        //Parent
        //[JsonIgnore]
        //public int QuoteMajorComponentId { get; set; }

        //[JsonIgnore]
        //public QuoteMajorComponent QuoteMajorComponent { get; set; }

        //Children
        public List<QuoteMinorComponentDetail> QuoteMinorComponentDetails { get; set; }

        public string ComponentID { get; set; }

        public string DetailCalculationID { get; set; }

        public string DescriptionCalculationID { get; set; }

        public string PriceCalculationID { get; set; }

        public string ActivateComponentCondition { get; set; }

        public bool OverrideComponentData { get; set; }

        public bool IncludeInBuildDocument { get; set; }

        public bool IsActive { get; set; }

        public bool IsVisible { get; set; }

        [JsonIgnore]
        public string Display { get { return Name; } }
    }
}