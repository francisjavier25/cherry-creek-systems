﻿using System;
using Newtonsoft.Json;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class QuoteMajorComponentDetail : BaseComponent, INamedAndOrderedComponent, IDescriptionCalculatedComponent, IDetailCalculatedComponent, IVisible
    {
        public string ComponentID { get; set; }

        public string DetailCalculationID { get; set; }

        public string DescriptionCalculationID { get; set; }

        public string PriceCalculationID { get; set; }

        public bool OverrideComponentData { get; set; }

        public bool IncludeInBuildDocument { get; set; }

        public bool IsVisible { get; set; }

        [JsonIgnore]
        public string Display { get { return Name; } }
    }
}