﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class QuoteCategory : INamedAndOrderedComponent
    {
        public QuoteCategory()
        {
            QuoteMajorComponents = new List<QuoteMajorComponent>();
        }

        public void GenerateID()
        {
            Id_ = Guid.NewGuid().ToString();
        }

        public string Id_ { get; set; }

        //public  int Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        //Parent
        //[JsonIgnore]
        //public int QuoteBoomSystemId { get; set; }

        //[JsonIgnore]
        //public  QuoteBoomSystem QuoteBoomSystem { get; set; }

        //Children
        public List<QuoteMajorComponent> QuoteMajorComponents { get; set; }

        [JsonIgnore]
        public string Display
        {
            get { return Name; }
        }
    }
}