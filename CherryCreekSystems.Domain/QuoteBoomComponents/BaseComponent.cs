﻿using Newtonsoft.Json;
using System;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class BaseComponent
    {
        public string DescriptionCalculation { get; set; }

        public string DescriptionCalculationResult { get; set; }

        public string DetailCalculation { get; set; }

        public string DetailCalculationResult { get; set; }

        public void GenerateID()
        {
            Id_ = Guid.NewGuid().ToString();
        }

        public string SugarCRMId { get; set; }
        public string Id_ { get; set; }

        public string Name { get; set; }
        //public string PreviousComponentName { get; set; }

        public int Order { get; set; }

        [JsonIgnore]
        public string ComponentName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(DetailCalculationResult))
                    return $"{DetailCalculationResult} {DescriptionCalculationResult}";

                return $"{DescriptionCalculationResult}";
            }
        }
    }
}
