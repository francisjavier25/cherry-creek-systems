﻿using System;

namespace CherryCreekSystems.Domain
{
    public class ApplicationLog
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Class { get; set; }

        public string Method { get; set; }

        public string Exception { get; set; }

        public string Message { get; set; }

        public string Level { get; set; }
    }
}