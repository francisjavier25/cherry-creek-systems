﻿//using CherryCreekSystems.Domain.BoomComponents;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text.RegularExpressions;

//namespace CherryCreekSystems.Domain
//{
//    public class Order
//    {
//        public Order()
//        {
//            CustomOrderItems = new List<CustomOrderItem>();
//            OrderBoomSystems = new List<OrderBoomSystem>();
//        }

//        public virtual int Id { get; set; }

//        public int Revision { get; set; }

//        public string OrderNumber { get; set; }

//        public string OrderNumberWithRevision
//        {
//            get
//            {
//                if (Revision != 0)
//                {
//                    return $"{OrderNumber}-{Revision}";
//                }

//                return $"{OrderNumber}";
//            }
//        }

//        public virtual Customer Customer { get; set; }

//        public virtual int CustomerId { get; set; }

//        public virtual string ContactName { get; set; }

//        public virtual string Phone { get; set; }

//        public virtual string Fax { get; set; }

//        public virtual string Email { get; set; }

//        public virtual string FullAddress { get; set; }

//        public virtual ShipAddress ShipAddress { get; set; }

//        public virtual int? ShipAddressId { get; set; }

//        public virtual SalesRep SalesRep { get; set; }

//        public virtual int SalesRepId { get; set; }

//        public virtual Distributor Distributor { get; set; }

//        public virtual int DistributorId { get; set; }

//        #region not mapped to database

//        public BoomType BoomType { get; set; }

//        public double FreightEstimateValue
//        {
//            get
//            {
//                double value;
//                var isValidValue = double.TryParse(FreightEstimate, out value);
//                if (isValidValue)
//                    return value;

//                return 0;
//            }
//        }

//        public double InstallationEstimateValue
//        {
//            get
//            {
//                double value;
//                var isValidValue = double.TryParse(InstallationEstimate, out value);
//                if (isValidValue)
//                    return value;

//                return 0;
//            }
//        }

//        public double SteelEstimateValue
//        {
//            get
//            {
//                double value;
//                var isValidValue = double.TryParse(SteelEstimate, out value);
//                if (isValidValue)
//                    return value;

//                return 0;
//            }
//        }

//        public double DiscountValue
//        {
//            get
//            {
//                double value;
//                var isValidValue = double.TryParse(Discount, out value);
//                if (isValidValue)
//                    return value;

//                return 0;
//            }
//        }

//        //public string ExpectedShipmentDate
//        //{
//        //    get
//        //    {
//        //        #region invalid cases

//        //        if (string.IsNullOrEmpty(LeadTime)) return string.Empty;

//        //        var leadTime = Regex.Replace(LeadTime.ToLower(), @"\s+", " "); //  Remove multiple spaces
//        //        var periodValues = leadTime.Split(' ');

//        //        if (periodValues.Length == 2)
//        //        {
//        //            var periodValue = periodValues[0];
//        //            var period = periodValues[1];

//        //            int periodsToAdd;
//        //            if (!int.TryParse(periodValue, out periodsToAdd)) return string.Empty;

//        //            #endregion

//        //            string[] weeks = { "week", "weeks" };
//        //            string[] months = { "month", "months" };
//        //            string[] days = { "day", "days" };

//        //            DateTime dateValue;

//        //            //Add weeks from current date
//        //            if (weeks.Contains(period))
//        //            {
//        //                periodsToAdd = periodsToAdd * 7; //Convert weeks to days
//        //                dateValue = DateTime.Now.AddDays(periodsToAdd);
//        //                return dateValue.ToString("MM/dd/yyyy");
//        //            }

//        //            //Add months from current date
//        //            if (months.Contains(period))
//        //            {
//        //                dateValue = DateTime.Now.AddMonths(periodsToAdd);
//        //                return dateValue.ToString("MM/dd/yyyy");
//        //            }

//        //            //Add days from current date
//        //            if (days.Contains(period))
//        //            {
//        //                //Add days from current date
//        //                dateValue = DateTime.Now.AddDays(periodsToAdd);
//        //                return dateValue.ToString("MM/dd/yyyy");
//        //            }
//        //        }
//        //        else if (periodValues.Length == 1) //If only number, default to weeks
//        //        {
//        //            var periodValue = periodValues[0];
//        //            int periodsToAdd;
//        //            if (!int.TryParse(periodValue, out periodsToAdd)) return string.Empty;
//        //            periodsToAdd = periodsToAdd * 7; //Convert weeks to days

//        //            var dateValue = DateTime.Now.AddDays(periodsToAdd);
//        //            return dateValue.ToString("MM/dd/yyyy");
//        //        }

//        //        return string.Empty;
//        //    }
//        //}

//        #endregion not mapped to database

//        public bool Active { get; set; }

//        public virtual int OrderStatusId { get; set; }

//        public virtual OrderStatus OrderStatus { get; set; }

//        public virtual DateTime StatusDate { get; set; }

//        public virtual DateTime OrderDate { get; set; }

//        public virtual DateTime DueDate { get; set; }

//        public virtual string LeadTime { get; set; }

//        public virtual string FreightEstimate { get; set; }

//        public virtual string InstallationEstimate { get; set; }

//        public virtual string SteelEstimate { get; set; }

//        public virtual string Discount { get; set; }

//        public virtual string PurchaseOrder { get; set; }

//        public virtual List<OrderBoomSystem> OrderBoomSystems { get; set; }

//        public virtual List<CustomOrderItem> CustomOrderItems { get; set; }
//    }
//}