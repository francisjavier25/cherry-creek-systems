﻿using System;

namespace CherryCreekSystems.Domain
{
    //This is for the steel estimate calculations
    public class SteelEstimate
    {
        public virtual int Id { get; set; }

        public int BoomTypeId { get; set; }

        public virtual string Material { get; set; }

        public virtual string QuantityCalculation { get; set; }

        public virtual string QuantityCalculationResult { get; set; }

        public virtual string ActivateEstimateCondition { get; set; }
    }

    //This is for the steel estimate overrides by the user
    public class SteelMaterial
    {
        public SteelMaterial() { }

        public void GenerateID()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }

        public string Material { get; set; }

        public string Quantity { get; set; }

        public bool Active { get; set; }
    }

    public class SteelMaterialDefault
    {
        public int Id { get; set; }

        public string Material { get; set; }
    }
}