﻿namespace CherryCreekSystems.Domain
{
    //This is static data for the quote report
    public class SteelData
    {
        public int Id { get; set; }          

        public string Size { get; set; }

        public string OuterDiameter{ get; set; }

        public string PoundsPerFoot { get; set; }

        public string Schedule { get; set; }
    }
}