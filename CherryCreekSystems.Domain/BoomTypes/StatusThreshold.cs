﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CherryCreekSystems.Domain
{
    public class StatusThreshold
    {
        public virtual int Id { get; set; }

        public int BoomTypeId { get; set; }

        public virtual double InfoAndDesignThreshold { get; set; }
        public virtual double SprayBarApprovalThreshold { get; set; }
        public virtual double AssemblyPickListThreshold { get; set; }
        public virtual double ShippingPickListThreshold { get; set; }
        public virtual double ProductionThreshold { get; set; }
        public virtual double SawAndSheerThreshold { get; set; }
        public virtual double PunchAndBreakThreshold { get; set; }
        public virtual double MachiningThreshold { get; set; }
        public virtual double WeldingThreshold { get; set; }
        public virtual double GalganizerThreshold { get; set; }
        public virtual double AssemblyThreshold { get; set; }
        public virtual double ShippingThreshold { get; set; }
    }
}
