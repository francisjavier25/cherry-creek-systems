﻿namespace CherryCreekSystems.Domain
{
    public class Echo : Boom
    {
        [CalculationEnabled]
        public int DrumSize { get; set; }

        [CalculationEnabled]
        public double BasketSpacing { get; set; }

        [CalculationEnabled]
        public string ControllerType { get; set; }

        [CalculationEnabled]
        public int NumberOfLayers { get; set; }

        [CalculationEnabled]
        public bool CenterBayWaterStationRequested { get; set; }

        [CalculationEnabled]
        public bool AMIADFilterAssemblyRequested { get; set; }

        [CalculationEnabled]
        public bool LocklineRequested { get; set; }

        [CalculationEnabled]
        public double ExtensionHangerLengthInInches { get; set; }

        [CalculationEnabled]
        public double PulleyBracketSpacingInFeet { get; set; }

        [CalculationEnabled]
        public bool RemotePullChainSwitchRequested { get; set; }

        [CalculationEnabled]
        public bool DRAMMRedheadBreakerUpgradeRequested { get; set; }

        [CalculationEnabled]
        public bool WaterSolenoidControlRequested { get; set; }

        [CalculationEnabled]
        public bool EvVpd { get; set; }

        [CalculationEnabled]
        public string Feed { get; set; }
    }
}