﻿namespace CherryCreekSystems.Domain
{
    //Lookup class
    public class Boom
    {
        public int Id { get; set; }

        public int BoomTypeId { get; set; }

        public int QuantityOfSystems { get; set; }

        public GreenHouseType GreenHouseType { get; set; }

        public int GreenHouseTypeId { get; set; }

        public int NumberOfBays { get; set; }

        public double BayLengthInFeet { get; set; }

        public double BayWidthInFeet { get; set; }

        public int NumberOfBaySideWalls { get; set; }

        public double WalkwayWidthInInches { get; set; }

        public double ClearanceWidthInInches { get; set; }

        public double BottomCordHeightInFeet { get; set; }

        public double TrussSpacingInFeet { get; set; }

        public string BottomCordType { get; set; }

        public string BottomCordSize { get; set; }

        public double GrowingSurfaceHeightInInches { get; set; }

        public double PostSpacingInFeet { get; set; }

        public string PostDimensionsInInchesSquared { get; set; }

        public double SprayBarSize { get; set; }

        public double HoseSize { get; set; }

        public string Location { get; set; }

        public string AdditionalInformation { get; set; }

        public string CustomerNote { get; set; }

        public int InfoAndDesignStatus { get; set; }
        public int SprayBarApprovalStatus { get; set; }
        public int AssemblyPickListStatus { get; set; }
        public int ShippingPickListStatus { get; set; }
        public int ProductionStatus { get; set; }
        public int SawAndSheerStatus { get; set; }
        public int PunchAndBreakStatus { get; set; }
        public int MachiningStatus { get; set; }
        public int WeldingStatus { get; set; }
        public int GalganizerStatus { get; set; }
        public int AssemblyStatus { get; set; }
        public int ShippingStatus { get; set; }

        public double InfoAndDesignThreshold { get; set; }
        public double SprayBarApprovalThreshold { get; set; }
        public double AssemblyPickListThreshold { get; set; }
        public double ShippingPickListThreshold { get; set; }
        public double ProductionThreshold { get; set; }
        public double SawAndSheerThreshold { get; set; }
        public double PunchAndBreakThreshold { get; set; }
        public double MachiningThreshold { get; set; }
        public double WeldingThreshold { get; set; }
        public double GalganizerThreshold { get; set; }
        public double AssemblyThreshold { get; set; }
        public double ShippingThreshold { get; set; }

        public CustomerCheckList CustomerCheckList { get; set; }
        public int? CustomerCheckListId { get; set; }

        //Echo fields

        public int DrumSize { get; set; }

        public double BasketSpacing { get; set; }

        public string ControllerType { get; set; }

        public int NumberOfLayers { get; set; }

        public bool CenterBayWaterStationRequested { get; set; }

        //3/4"

        public bool AMIADFilterAssemblyRequested { get; set; }

        public bool LocklineRequested { get; set; }

        public double ExtensionHangerLengthInInches { get; set; }

        public double PulleyBracketSpacingInFeet { get; set; }

        public bool RemotePullChainSwitchRequested { get; set; }

        public bool DRAMMRedheadBreakerUpgradeRequested { get; set; }

        public bool WaterSolenoidControlRequested { get; set; }

        public bool EvVpd { get; set; }

        public string EVWireGauge { get; set; }

        public string Feed { get; set; }

        //Single Rail fields

        public double HoseLoopHeightInInches { get; set; }

        public string SingleOrDualWater { get; set; }

        public bool TripleStepLocklineUpgradeRequested { get; set; }

        public bool TripleTurretBodyRequested { get; set; }

        public bool Sweep90Requested { get; set; }

        public bool PressureRegulatorRequested { get; set; }

        public bool MountedInjectorRequested { get; set; }

        public string InjectorType { get; set; }

        public bool AutoInjectorRequested { get; set; }

        public double SprayBodySpacingInInches { get; set; }

        //1"

        public bool AMIADFilterASSYRequested { get; set; }

        public int NumberOfRows { get; set; }

        public string Tip1 { get; set; }

        public string Tip2 { get; set; }

        public string Tip3 { get; set; }

        //Double Rail fields

        public double RailDropDownASSYInInches { get; set; }

        public bool HoseUpgradeRequested { get; set; }

        public bool WirelessWalkSwitchRequested { get; set; }

        //CWF Double Rail fields

        public bool CenterWaterFeedSystemRequested { get; set; }

        //Tower fields

        public bool DiamondSweepRequested { get; set; }

        public bool ConveyorRequested { get; set; }

        public bool DualMotorNoAxleRequested { get; set; }

        public bool Gallon25ReservoirTankRequested { get; set; }

        public string CarrySteel { get; set; }

        //Navigator fields

        //Ground Runner fields

        //Sky Rail fields
    }
}

//namespace CherryCreekSystems.Domain
//{
//    //Lookup class
//    public class Boom
//    {
//        public int Id { get; set; }

//        public int BoomTypeId { get; set; }

//        [CalculationEnabled]
//        public int QuantityOfSystems { get; set; }

//        public GreenHouseType GreenHouseType { get; set; }

//        public int GreenHouseTypeId { get; set; }

//        [CalculationEnabled]
//        public int NumberOfBays { get; set; }

//        [CalculationEnabled]
//        public double BayLengthInFeet { get; set; }

//        [CalculationEnabled]
//        public double BayWidthInFeet { get; set; }

//        [CalculationEnabled]
//        public int NumberOfBaySideWalls { get; set; }

//        [CalculationEnabled]
//        public double WalkwayWidthInInches { get; set; }

//        [CalculationEnabled]
//        public double ClearanceWidthInInches { get; set; }

//        [CalculationEnabled]
//        public double BottomCordHeightInFeet { get; set; }

//        [CalculationEnabled]
//        public double TrussSpacingInFeet { get; set; }

//        [CalculationEnabled]
//        public string BottomCordType { get; set; }

//        [CalculationEnabled]
//        public string BottomCordSize { get; set; }

//        [CalculationEnabled]
//        public double GrowingSurfaceHeightInInches { get; set; }

//        [CalculationEnabled]
//        public double PostSpacingInFeet { get; set; }

//        [CalculationEnabled]
//        public string PostDimensionsInInchesSquared { get; set; }

//        [CalculationEnabled]
//        public double SprayBarSize { get; set; }

//        [CalculationEnabled]
//        public double HoseSize { get; set; }

//        [CalculationEnabled]
//        public string Location { get; set; }

//        public string AdditionalInformation { get; set; }

//        public string CustomerNote { get; set; }

//        public int InfoAndDesignStatus { get; set; }
//        public int SprayBarApprovalStatus { get; set; }
//        public int AssemblyPickListStatus { get; set; }
//        public int ShippingPickListStatus { get; set; }
//        public int ProductionStatus { get; set; }
//        public int SawAndSheerStatus { get; set; }
//        public int PunchAndBreakStatus { get; set; }
//        public int MachiningStatus { get; set; }
//        public int WeldingStatus { get; set; }
//        public int GalganizerStatus { get; set; }
//        public int AssemblyStatus { get; set; }
//        public int ShippingStatus { get; set; }

//        public double InfoAndDesignThreshold { get; set; }
//        public double SprayBarApprovalThreshold { get; set; }
//        public double AssemblyPickListThreshold { get; set; }
//        public double ShippingPickListThreshold { get; set; }
//        public double ProductionThreshold { get; set; }
//        public double SawAndSheerThreshold { get; set; }
//        public double PunchAndBreakThreshold { get; set; }
//        public double MachiningThreshold { get; set; }
//        public double WeldingThreshold { get; set; }
//        public double GalganizerThreshold { get; set; }
//        public double AssemblyThreshold { get; set; }
//        public double ShippingThreshold { get; set; }

//        public CustomerCheckList CustomerCheckList { get; set; }
//        public int? CustomerCheckListId { get; set; }

//        //Echo fields
//        [CalculationEnabled]
//        public int DrumSize { get; set; }

//        [CalculationEnabled]
//        public double BasketSpacing { get; set; }

//        [CalculationEnabled]
//        public string ControllerType { get; set; }

//        [CalculationEnabled]
//        public int NumberOfLayers { get; set; }

//        [CalculationEnabled]
//        public bool CenterBayWaterStationRequested { get; set; }

//        //3/4"
//        [CalculationEnabled]
//        public bool AMIADFilterAssemblyRequested { get; set; }

//        [CalculationEnabled]
//        public bool LocklineRequested { get; set; }

//        [CalculationEnabled]
//        public double ExtensionHangerLengthInInches { get; set; }

//        [CalculationEnabled]
//        public double PulleyBracketSpacingInFeet { get; set; }

//        [CalculationEnabled]
//        public bool RemotePullChainSwitchRequested { get; set; }

//        [CalculationEnabled]
//        public bool DRAMMRedheadBreakerUpgradeRequested { get; set; }

//        [CalculationEnabled]
//        public bool WaterSolenoidControlRequested { get; set; }

//        [CalculationEnabled]
//        public bool EvVpd { get; set; }

//        [CalculationEnabled]
//        public string Feed { get; set; }

//        //Single Rail fields
//        [CalculationEnabled]
//        public double HoseLoopHeightInInches { get; set; }

//        [CalculationEnabled]
//        public string SingleOrDualWater { get; set; }

//        [CalculationEnabled]
//        public bool TripleStepLocklineUpgradeRequested { get; set; }

//        [CalculationEnabled]
//        public bool TripleTurretBodyRequested { get; set; }

//        [CalculationEnabled]
//        public bool Sweep90Requested { get; set; }

//        [CalculationEnabled]
//        public bool PressureRegulatorRequested { get; set; }

//        [CalculationEnabled]
//        public bool MountedInjectorRequested { get; set; }

//        [CalculationEnabled]
//        public string InjectorType { get; set; }

//        [CalculationEnabled]
//        public bool AutoInjectorRequested { get; set; }

//        [CalculationEnabled]
//        public double SprayBodySpacingInInches { get; set; }

//        //1"
//        [CalculationEnabled]
//        public bool AMIADFilterASSYRequested { get; set; }

//        [CalculationEnabled]
//        public int NumberOfRows { get; set; }

//        [CalculationEnabled]
//        public string Tip1 { get; set; }

//        [CalculationEnabled]
//        public string Tip2 { get; set; }

//        [CalculationEnabled]
//        public string Tip3 { get; set; }

//        //Double Rail fields
//        [CalculationEnabled]
//        public double RailDropDownASSYInInches { get; set; }

//        [CalculationEnabled]
//        public bool HoseUpgradeRequested { get; set; }

//        [CalculationEnabled]
//        public bool WirelessWalkSwitchRequested { get; set; }

//        //CWF Double Rail fields
//        [CalculationEnabled]
//        public bool CenterWaterFeedSystemRequested { get; set; }

//        //Tower fields
//        [CalculationEnabled]
//        public bool DiamondSweepRequested { get; set; }

//        [CalculationEnabled]
//        public bool ConveyorRequested { get; set; }

//        [CalculationEnabled]
//        public bool DualMotorNoAxleRequested { get; set; }

//        [CalculationEnabled]
//        public bool Gallon25ReservoirTankRequested { get; set; }

//        [CalculationEnabled]
//        public string CarrySteel { get; set; }

//        //Navigator fields

//        //Ground Runner fields

//        //Sky Rail fields
//    }
//}