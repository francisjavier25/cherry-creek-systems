﻿namespace CherryCreekSystems.Domain
{
    public class SingleRail : Boom
    {
        [CalculationEnabled]
        public double HoseLoopHeightInInches { get; set; }

        [CalculationEnabled]
        public string SingleOrDualWater { get; set; }

        [CalculationEnabled]
        public bool TripleStepLocklineUpgradeRequested { get; set; }

        [CalculationEnabled]
        public bool TripleTurretBodyRequested { get; set; }

        [CalculationEnabled]
        public string ControllerType { get; set; }

        [CalculationEnabled]
        public bool Sweep90Requested { get; set; }

        [CalculationEnabled]
        public bool PressureRegulatorRequested { get; set; }

        [CalculationEnabled]
        public bool MountedInjectorRequested { get; set; }

        [CalculationEnabled]
        public string InjectorType { get; set; }

        [CalculationEnabled]
        public bool AutoInjectorRequested { get; set; }

        [CalculationEnabled]
        public double SprayBodySpacingInInches { get; set; }

        [CalculationEnabled]
        public bool AMIADFilterASSYRequested { get; set; }

        [CalculationEnabled]
        public bool WaterSolenoidControlRequested { get; set; }

        [CalculationEnabled]
        public int NumberOfRows { get; set; }

        [CalculationEnabled]
        public string Tip1 { get; set; }

        [CalculationEnabled]
        public string Tip2 { get; set; }

        [CalculationEnabled]
        public string Tip3 { get; set; }

        [CalculationEnabled]
        public bool EvVpd { get; set; }

        [CalculationEnabled]
        public string EVWireGauge { get; set; }

        [CalculationEnabled]
        public string Feed { get; set; }
    }
}