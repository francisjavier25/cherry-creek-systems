﻿namespace CherryCreekSystems.Domain
{
    public class Tower : Boom
    {
        [CalculationEnabled]
        public  double HoseLoopHeightInInches { get; set; }

        [CalculationEnabled]
        public  string SingleOrDualWater { get; set; }

        [CalculationEnabled]
        public  bool TripleStepLocklineUpgradeRequested { get; set; }

        [CalculationEnabled]
        public  bool TripleTurretBodyRequested { get; set; }

        [CalculationEnabled]
        public  string ControllerType { get; set; }

        [CalculationEnabled]
        public  bool DiamondSweepRequested { get; set; }

        [CalculationEnabled]
        public  bool PressureRegulatorRequested { get; set; }

        [CalculationEnabled]
        public  bool MountedInjectorRequested { get; set; }

        [CalculationEnabled]
        public  string InjectorType { get; set; }

        [CalculationEnabled]
        public  bool AutoInjectorRequested { get; set; }

        [CalculationEnabled]
        public  bool ConveyorRequested { get; set; }

        [CalculationEnabled]
        public  bool DualMotorNoAxleRequested { get; set; }

        [CalculationEnabled]
        public  bool Gallon25ReservoirTankRequested { get; set; }

        [CalculationEnabled]
        public  bool HoseUpgradeRequested { get; set; }

        [CalculationEnabled]
        public  bool CenterWaterFeedSystemRequested { get; set; }

        [CalculationEnabled]
        public  double SprayBodySpacingInInches { get; set; }

        [CalculationEnabled]
        public  bool WirelessWalkSwitchRequested { get; set; }

        [CalculationEnabled]
        public  bool WaterSolenoidControlRequested { get; set; }

        [CalculationEnabled]
        public  int NumberOfRows { get; set; }

        [CalculationEnabled]
        public  string Tip1 { get; set; }

        [CalculationEnabled]
        public  string Tip2 { get; set; }

        [CalculationEnabled]
        public  string Tip3 { get; set; }

        [CalculationEnabled]
        public  bool EvVpd { get; set; }

        [CalculationEnabled]
        public  string CarrySteel { get; set; }

        [CalculationEnabled]
        public  string Feed { get; set; }
    }
}