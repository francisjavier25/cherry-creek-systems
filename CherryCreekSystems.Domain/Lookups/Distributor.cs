﻿namespace CherryCreekSystems.Domain
{
    public class Distributor : ISimpleEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        //Really a discount percentage that will apply only to the systems
        public double PriceLevel { get; set; }
    }
}