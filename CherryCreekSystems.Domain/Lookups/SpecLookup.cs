﻿namespace CherryCreekSystems.Domain
{
    public class SpecLookup
    {
        public virtual int Id { get; set; }

        public virtual string Value { get; set; }

        public virtual int SpecId { get; set; }

        public virtual Spec Spec { get; set; }
    }
}