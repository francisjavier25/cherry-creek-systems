﻿using System.Collections.Generic;

namespace CherryCreekSystems.Domain
{
    public class Customer
    {
        public int Id { get; set; }

        public string CompanyName { get; set; }

        public string ContactName { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public List<ShipAddress> ShipAddresses { get; set; }

        public List<QuoteRequest> QuoteRequests { get; set; }

        public string SugarCRMId { get; set; }
    }
}