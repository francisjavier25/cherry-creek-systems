﻿using System.Collections.Generic;

namespace CherryCreekSystems.Domain
{
    public class Spec
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public ICollection<SpecLookup> SpecLookups { get; set; }
    }
}