﻿using CherryCreekSystems.Domain.BoomComponents;

namespace CherryCreekSystems.Domain
{
    public class BaseCost
    {
        public virtual int Id { get; set; }

        public virtual int BoomSystemId { get; set; }

        public virtual BoomSystem BoomSystem { get; set; }

        public virtual double XValue { get; set; }

        public virtual double YValue { get; set; }
    }
}