﻿namespace CherryCreekSystems.Domain
{
    public class ShipAddress
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Street1 { get; set; }

        public string Street2 { get; set; }

        public string City { get; set; }

        public string Zip { get; set; }

        public bool IsSugarCRMAddress { get; set; }

        #region not mapped

        public string FullAddress
        {
            get
            {
                return GetFullAddress;
            }
        }

        public override string ToString()
        {
            return GetFullAddress;
        }

        private string GetFullAddress
        {
            get
            {
                string address = string.Empty;
                var state = State?.Name ?? string.Empty;
                if (!string.IsNullOrEmpty(Name))
                    address = $"{Name}, ";

                if (!string.IsNullOrEmpty(Street1))
                    address += $"{Street1}, ";

                if (!string.IsNullOrEmpty(Street2))
                    address += $"{Street2}, ";

                if (!string.IsNullOrEmpty(City))
                    address += $"{City}, ";

                if (!string.IsNullOrEmpty(state))
                    address += $"{state}, ";

                if (!string.IsNullOrEmpty(Zip))
                    address += $"{Zip}";

                return address;
            }
        }

        #endregion not mapped

        //Child
        public virtual int? StateId { get; set; }

        public virtual State State { get; set; }

        //Parent
        public virtual int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
    }
}