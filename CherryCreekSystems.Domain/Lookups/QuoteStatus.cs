﻿namespace CherryCreekSystems.Domain
{
    public class QuoteStatus : ISimpleEntity
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
    }
}