﻿namespace CherryCreekSystems.Domain
{
    public class GreenHouseType : ISimpleEntity
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
    }
}