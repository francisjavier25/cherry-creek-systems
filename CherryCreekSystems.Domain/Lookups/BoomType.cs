﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace CherryCreekSystems.Domain
{
    public enum BoomType
    {
        [Description("ECHO")]
        Echo = 1,
        [Description("Single Rail")]
        SingleRail = 2,
        [Description("Double Rail")]
        DoubleRail = 3,
        [Description("CWF Double Rail")]
        CWFDoubleRail = 4,
        [Description("Tower")]
        Tower = 5,
        [Description("Navigator")]
        Navigator = 6,
        [Description("Ground Runner")]
        GroundRunner = 7,
        [Description("Sky Rail")]
        SkyRail = 8
    }

    public class BoomTypeStructure
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static IEnumerable<BoomTypeStructure> GetList()
        {
            return EnumExtensions.GetValues<BoomType>().ToList().Select(e => new BoomTypeStructure { Name = e.GetDescription(), Id = (int)e });
        }
    }

    public static class BoomTypeStructureExtensions
    {
        public static IEnumerable<BoomTypeStructure> WithFirstEmptyValue(this IEnumerable<BoomTypeStructure> query)
        {
            yield return new BoomTypeStructure { Id = 0, Name = string.Empty };

            foreach (var b in query)
            {
                yield return b;
            }
        }
    }

    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr =
                           Attribute.GetCustomAttribute(field,
                             typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }
            return null;
        }

        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
}