﻿namespace CherryCreekSystems.Domain
{
    //Lookup class
    public class SalesRep : ISimpleEntity
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string Phone { get; set; }
    }
}