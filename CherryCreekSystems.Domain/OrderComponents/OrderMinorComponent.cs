﻿//using System.Collections.Generic;

//namespace CherryCreekSystems.Domain.BoomComponents
//{
//    public class OrderMinorComponent : IDetailCalculatedComponent, IDescriptionCalculatedComponent, IPriceCalculatedComponent, INamedAndOrderedComponent, IActivatedCalculatedComponent
//    {
//        public OrderMinorComponent()
//        {
//            OrderMinorComponentDetails = new List<OrderMinorComponentDetail>();
//        }

//        public virtual int Id { get; set; }

//        public virtual string Name { get; set; }

//        public virtual string DescriptionCalculation { get; set; }

//        public virtual string DescriptionCalculationResult { get; set; }

//        public virtual string DetailCalculation { get; set; }

//        public virtual string DetailCalculationResult { get; set; }

//        public virtual string AddPriceCondition { get; set; }

//        public virtual string PriceCalculation { get; set; }

//        public virtual double CalculatedPrice { get; set; }

//        public virtual double AssignedPrice { get; set; }

//        public virtual int Order { get; set; }

//        //Parent
//        public virtual int OrderMajorComponentId { get; set; }

//        public virtual OrderMajorComponent OrderMajorComponent { get; set; }

//        //Children
//        public virtual ICollection<OrderMinorComponentDetail> OrderMinorComponentDetails { get; set; }

//        public string ComponentID { get; set; }

//        public string DetailCalculationID { get; set; }

//        public string DescriptionCalculationID { get; set; }

//        public string PriceCalculationID { get; set; }

//        public string ActivateComponentCondition { get; set; }

//        public bool OverrideComponentData { get; set; }

//        public bool IncludeInBuildDocument { get; set; }

//        public bool IsActive { get; set; }

//        public string Display { get { return Name; } }
//    }
//}