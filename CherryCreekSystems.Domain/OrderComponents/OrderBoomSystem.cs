﻿//using System.Collections.Generic;

//namespace CherryCreekSystems.Domain.BoomComponents
//{
//    public class OrderBoomSystem : IDetailCalculatedComponent, IDescriptionCalculatedComponent, IPriceCalculatedComponent
//    {
//        public OrderBoomSystem()
//        {
//            OrderCategories = new List<OrderCategory>();
//        }

//        public virtual int Id { get; set; }

//        public virtual string Name { get; set; }

//        public virtual string DescriptionCalculation { get; set; }

//        public virtual string DescriptionCalculationResult { get; set; }

//        public virtual string DetailCalculation { get; set; }

//        public virtual string DetailCalculationResult { get; set; }

//        public virtual string AddPriceCondition { get; set; }

//        public virtual string PriceCalculation { get; set; }

//        public virtual double CalculatedPrice { get; set; }

//        public virtual double AssignedPrice { get; set; }

//        public virtual int Quantity { get; set; }

//        //This id maps to the BoomType enum
//        public virtual int BoomTypeId { get; set; }

//        #region not mapped to database

//        public string SystemName
//        {
//            get
//            {
//                if (!string.IsNullOrWhiteSpace(DetailCalculationResult))
//                    return $"{DetailCalculationResult} {Name}";

//                return Name;
//            }
//        }

//        public int Number { get; set; }

//        public string SystemNameForList
//        {
//            get
//            {
//                if (!string.IsNullOrWhiteSpace(DetailCalculationResult))
//                    return $"{Number}-{DetailCalculationResult} {Name}";

//                return $"{Number}-{Name}";
//            }
//        }

//        public string StatusData { get; set; }

//        #endregion not mapped to database

//        public virtual int OrderId { get; set; }

//        public virtual Order Order { get; set; }

//        public virtual int BoomId { get; set; }

//        public virtual Boom Boom { get; set; }

//        public virtual ICollection<OrderCategory> OrderCategories { get; set; }

//        public string ComponentID { get; set; }

//        public string DetailCalculationID { get; set; }

//        public string DescriptionCalculationID { get; set; }

//        public string PriceCalculationID { get; set; }

//        public bool OverrideComponentData { get; set; }

//        public bool IncludeInBuildDocument { get; set; }

//        public bool Active { get; set; }
//    }
//}