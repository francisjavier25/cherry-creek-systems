﻿using System;
using System.IO;

namespace CherryCreekSystems.Domain
{
    public class CustomerCheckList
    {
        public int Id { get; set; }

        //Customer information
        public string Customer { get; set; }

        public string OnsiteContact { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public string QuoteNumber { get; set; }

        //Ship to Address
        public string ShipToAddressName { get; set; }

        public string ShipToAddressStreet { get; set; }
        public string ShipToAddressCity { get; set; }
        public string ShipToAddressState { get; set; }
        public string ShipToAddressZipCode { get; set; }

        //Boom Type and Quantity
        public string RequestedBoom { get; set; }

        public string RequestedBoomQuantity { get; set; }

        //Greenhouse Specifications
        public string BayWidth { get; set; }

        public string BayLength { get; set; }
        public string PostDimensions { get; set; }
        public string TrussHoopDimensionAndType { get; set; }
        public string TrussHeight { get; set; }
        public string BenchHeight { get; set; }
        public string CropHeight { get; set; }
        public string TrussSpacing { get; set; }
        public string RailTypeAndSize { get; set; }
        public string BottomCordOrientation { get; set; }

        //Walkways & Rows
        public string WalkwayWidth { get; set; }

        public string WalkwayLocation { get; set; }
        public string NumberOfRows { get; set; }
        public string CropRowWidth { get; set; }
        public bool? IsWalkwayClearanceRequired { get; set; }
        public string WalkwayClearanceRequirements { get; set; }
        public byte[] BoomSketchImage { get; set; }
        public string BoomSketchImageName { get; set; }
        public byte[] TrussSketchImage { get; set; }
        public string TrussSketchImageName { get; set; }

        public string BoomSketchImageNameExtension
        {
            get
            {
                if (!string.IsNullOrEmpty(BoomSketchImageName))
                {
                    return Path.GetExtension(BoomSketchImageName).Replace(".", "");
                }

                return string.Empty;
            }
        }

        public string TrussSketchImageNameExtension
        {
            get
            {
                if (!string.IsNullOrEmpty(TrussSketchImageName))
                {
                    return Path.GetExtension(TrussSketchImageName).Replace(".", ""); ;
                }

                return string.Empty;
            }
        }

        public string BoomSketchImageBase64
        {
            get
            {
                if (BoomSketchImage != null && BoomSketchImage.Length > 0)
                {
                    var data = Convert.ToBase64String(BoomSketchImage);
                    return data;
                }

                return string.Empty;
            }
        }

        public string TrussSketchImageBase64
        {
            get
            {
                if (TrussSketchImage != null && TrussSketchImage.Length > 0)
                {
                    var data = Convert.ToBase64String(TrussSketchImage);
                    return data;
                }

                return string.Empty;
            }
        }

        //Other data
        public string CenteredOrOffset { get; set; }

        public string SpraybarSize { get; set; }
        public string HoseSize { get; set; }
        public bool? IsTherePreexistingRails { get; set; }
        public string DistanceFromCenterToCenterOfRail { get; set; }
        public string TypeAndSizeOfRail { get; set; }
        public string VFDOrEnvironmenalControls { get; set; }
        public string AdditionalCustomRequests { get; set; }

        //Sign off
        //[Required(ErrorMessage = "Please enter your name")]
        //public string Name { get; set; }
        //public byte[] SignatureImage { get; set; }

        public DateTime Date { get; set; }
    }

    //public class BoomSketchCoordinates
    //{
    //    public int[] XCoordinates { get; set; }
    //    public int[] YCoordinates { get; set; }
    //    public bool[] DragValues { get; set; }
    //}
}