﻿namespace CherryCreekSystems.Domain
{
    public class User
    {
        public int? ApplicationUserId { get; set; }

        public string UserName { get; set; }

        public string Role { get; set; }
    }
}