﻿namespace CherryCreekSystems.Domain
{
    public class CustomerUser : User
    {
        public virtual string CompanyName { get; set; }

        public virtual string ContactName { get; set; }

        public virtual string Phone { get; set; }

        public virtual string Fax { get; set; }

        public virtual string Email { get; set; }
    }
}