﻿namespace CherryCreekSystems.Domain
{
    public class SalesRepUser : User
    {
        public string Name { get; set; }
    }
}