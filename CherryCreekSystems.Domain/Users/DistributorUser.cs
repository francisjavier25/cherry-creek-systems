﻿namespace CherryCreekSystems.Domain
{
    public class DistributorUser : User
    {
        public string Name { get; set; }
    }
}