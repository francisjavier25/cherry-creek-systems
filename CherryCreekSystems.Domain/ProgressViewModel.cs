﻿using System;

namespace CherryCreekSystems.Domain
{
    public class ProgressViewModel
    {
        public int QuoteRequestId { get; set; }
        public int BoomId { get; set; }
        public int SystemId { get; set; }
        public BoomType BoomType { get; set; }
        public string CustomerName { get; set; }
        public string SystemName { get; set; }
        public string ControlType { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DueDate { get; set; }
        public int Quantity { get; set; }

        public double InfoAndDesignThreshold { get; set; }
        public double SprayBarApprovalThreshold { get; set; }
        public double AssemblyPickListThreshold { get; set; }
        public double ShippingPickListThreshold { get; set; }
        public double ProductionThreshold { get; set; }
        public double SawAndSheerThreshold { get; set; }
        public double PunchAndBreakThreshold { get; set; }
        public double MachiningThreshold { get; set; }
        public double WeldingThreshold { get; set; }
        public double GalganizerThreshold { get; set; }
        public double AssemblyThreshold { get; set; }
        public double ShippingThreshold { get; set; }

        public int InfoAndDesignStatus { get; set; }
        public int SprayBarApprovalStatus { get; set; }
        public int AssemblyPickListStatus { get; set; }
        public int ShippingPickListStatus { get; set; }
        public int ProductionStatus { get; set; }
        public int SawAndSheerStatus { get; set; }
        public int PunchAndBreakStatus { get; set; }
        public int MachiningStatus { get; set; }
        public int WeldingStatus { get; set; }
        public int GalganizerStatus { get; set; }
        public int AssemblyStatus { get; set; }
        public int ShippingStatus { get; set; }
    }

    public class StatusViewModel
    {
        public int BoomId { get; set; }
        public int SystemId { get; set; }
        public string CustomerName { get; set; }
        public string SystemName { get; set; }
        public string ControlType { get; set; }
        public int Quantity { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DueDate { get; set; }
        public double InfoAndDesignStatus { get; set; }
        public double SprayBarApprovalStatus { get; set; }
        public double AssemblyPickListStatus { get; set; }
        public double ShippingPickListStatus { get; set; }
        public double ProductionStatus { get; set; }
        public double SawAndSheerStatus { get; set; }
        public double PunchAndBreakStatus { get; set; }
        public double MachiningStatus { get; set; }
        public double WeldingStatus { get; set; }
        public double GalganizerStatus { get; set; }
        public double AssemblyStatus { get; set; }
        public double ShippingStatus { get; set; }
    }
}