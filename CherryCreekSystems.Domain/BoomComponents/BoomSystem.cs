﻿using System.Collections.Generic;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class BoomSystem : IDetailedComponent, IDescriptionComponent, IPricedComponent
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string DetailCalculation { get; set; }

        public virtual string DescriptionCalculation { get; set; }

        public virtual string PriceCalculation { get; set; }

        //This id maps to the BoomType enum
        public virtual int BoomTypeId { get; set; }

        public virtual List<Category> Categories { get; set; }

        public string ComponentID { get; set; }

        public string DetailCalculationID { get; set; }

        public string DescriptionCalculationID { get; set; }

        public string PriceCalculationID { get; set; }

        public string AddPriceCondition { get; set; }

        public string Display
        {
            get { return Name; }
        }
    }
}