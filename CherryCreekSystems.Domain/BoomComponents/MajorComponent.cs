﻿using System.Collections.Generic;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class MajorComponent : IDetailedComponent, IDescriptionComponent, IPricedComponent, INamedAndOrderedComponent, IActivatedCalculatedComponent
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string DescriptionCalculation { get; set; }

        public virtual string DetailCalculation { get; set; }

        public virtual string AddPriceCondition { get; set; }

        public virtual string PriceCalculation { get; set; }

        public virtual int Order { get; set; }

        //Parent
        public virtual int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        //Children
        public virtual List<MinorComponent> MinorComponents { get; set; }

        public virtual List<MajorComponentDetail> MajorComponentDetails { get; set; }

        public string ComponentID { get; set; }

        public string DescriptionCalculationID { get; set; }

        public string DetailCalculationID { get; set; }

        public string PriceCalculationID { get; set; }

        public string Display
        {
            get { return $"{ComponentID} - {Name}"; }
        }

        public string ActivateComponentCondition { get; set; }

        public bool IsActive { get; set; }
    }
}