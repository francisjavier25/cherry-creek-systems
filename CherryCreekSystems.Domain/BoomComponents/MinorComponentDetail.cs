﻿namespace CherryCreekSystems.Domain.BoomComponents
{
    public class MinorComponentDetail : IDescriptionComponent, INamedAndOrderedComponent, IDetailedComponent
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string DescriptionCalculation { get; set; }

        public virtual string DetailCalculation { get; set; }

        public virtual int Order { get; set; }

        //Parent
        public virtual int MinorComponentId { get; set; }

        public virtual MinorComponent MinorComponent { get; set; }

        public string ComponentID { get; set; }

        public string DescriptionCalculationID { get; set; }

        public string DetailCalculationID { get; set; }

        public string PriceCalculationID { get; set; }

        public string Display
        {
            get { return Name; }
        }

        public bool IsActive { get; set; }
    }
}