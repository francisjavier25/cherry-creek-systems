﻿using System.Collections.Generic;

namespace CherryCreekSystems.Domain.BoomComponents
{
    public class Category : INamedAndOrderedComponent
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual int Order { get; set; }

        //Parent
        public int SystemId { get; set; }

        public virtual BoomSystem System { get; set; }

        //Children
        public virtual List<MajorComponent> MajorComponents { get; set; }

        public string Display
        {
            get { return Name; }
        }

        public bool IsActive { get; set; }
    }
}