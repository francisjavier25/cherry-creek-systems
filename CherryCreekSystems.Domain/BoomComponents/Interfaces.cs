﻿namespace CherryCreekSystems.Domain.BoomComponents
{
    public interface IComponent
    {
        string Name { get; set; }

        string ComponentID { get; set; }

        string DescriptionCalculationID { get; set; }

        string DetailCalculationID { get; set; }

        string PriceCalculationID { get; set; }
    }

    public interface IDetailedComponent : IComponent
    {
        string DetailCalculation { get; set; }
    }

    public interface IDetailCalculatedComponent : IDetailedComponent
    {
        string DetailCalculationResult { get; set; }

        bool OverrideComponentData { get; set; }
    }

    public interface IDescriptionComponent : IComponent
    {
        string DescriptionCalculation { get; set; }
    }

    public interface IDescriptionCalculatedComponent : IDescriptionComponent
    {
        string DescriptionCalculationResult { get; set; }

        bool OverrideComponentData { get; set; }

        bool IncludeInBuildDocument { get; set; }
    }

    public interface IPricedComponent : IComponent
    {
        string AddPriceCondition { get; set; }

        string PriceCalculation { get; set; }
    }

    public interface IPriceCalculatedComponent : IPricedComponent
    {
        double AssignedPrice { get; set; }

        double CalculatedPrice { get; set; }

        bool OverrideComponentData { get; set; }
    }

    public interface IActivatedComponent
    {
        string ActivateComponentCondition { get; set; }
    }

    public interface IActivatedCalculatedComponent : IActivatedComponent, IComponent
    {
        bool IsActive { get; set; }
    }

    public interface IVisible
    {
        bool IsVisible { get; set; }
    }

    public interface INamedAndOrderedComponent
    {
        string Name { get; set; }

        string Display { get; }

        int Order { get; set; }
    }
}