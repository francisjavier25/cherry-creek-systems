﻿using System;
using CherryCreekSystems.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using CherryCreekSystems.BL.Utils;
using Newtonsoft.Json;
using CherryCreekSystems.BL.QueryExtensions;
using CherryCreekSystems.Domain;
using System.Threading.Tasks;

namespace CherryCreekSystems.Tests
{
    [TestClass]
    public class UnitTest1
    {
        //[TestMethod]
        //public void TestPDFMErge()
        //{
        //    MergeDocument
        //}

        //[TestMethod]
        //public void TestBoomChecklist()
        //{
        //    QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
        //    //var system = new QuoteBoomSystemService(uow).GetActive(1, true, true).Ordered();
        //    string fileNameResult = reportGenerator.GenerateBoomChecklistForm();
        //    var process = Process.Start(fileNameResult);
        //}

        //[TestMethod]
        //public async void TestQuoteReport()
        //{
        //    UnitOfWork uow = new UnitOfWork();

        //    QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
        //    //var system = new QuoteBoomSystemService(uow).GetActive(1, true, true).Ordered();
        //    var quoteRequest = await new QuoteRequestService(uow).GetWithIncludedEntities(3, true, true, true);

        //    string fileNameResult = reportGenerator.GenerateQuoteReport(quoteRequest, quoteRequest.QuoteBoomSystems, quoteRequest.CustomQuoteItems); ;
        //    var process = Process.Start(fileNameResult);
        //}

        //[TestMethod]
        //public void TestSalesFormReport()
        //{
        //    UnitOfWork uow = new UnitOfWork();

        //    QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
        //    //var system = new QuoteBoomSystemService(uow).GetActive(1, true, true).Ordered();
        //    var quoteRequest = new QuoteRequestService(uow).GetWithIncludedEntities(5, true, includeComponents: true);

        //    string fileNameResult = QuoteReportGenerator.GenerateSalesOrderForm(quoteRequest, quoteRequest.QuoteBoomSystems[0]); ;
        //    var process = Process.Start(fileNameResult);
        //}

        //[TestMethod]
        //public async void TestDetailQuoteReport()
        //{
        //    UnitOfWork uow = new UnitOfWork();

        //    QuoteReportGenerator reportGenerator = new QuoteReportGenerator();
        //    var quote = await new QuoteRequestService(uow).GetWithIncludedEntities(1174, true, true);
        //    quote.QuoteBoomSystems.ForEach(b => b.Ordered());

        //   string fileNameResult = reportGenerator.GenerateDetailQuoteReport(quote.QuoteBoomSystems[0], quote, quote.QuoteBoomSystems[0].Boom);
        //   var process = Process.Start(fileNameResult);
        //}

        //[TestMethod]
        //public void TestJSONData()
        //{
        //    var quoteRequestService = new QuoteRequestService();
        //    var quoteRequestBoomService = new QuoteRequestBoomService();
        //    var quotes = quoteRequestService.Get().WhereActive().ToList();

        //    foreach (var q in quotes)
        //    {
        //        foreach (var s in q.QuoteBoomSystemsData.QuoteBoomSystems.WhereActive())
        //        {
        //            var boom = new QuoteRequestBoom();
        //            boom.QuoteRequestId = q.Id;
        //            boom.BoomTypeId = s.Boom.BoomTypeId;

        //            quoteRequestBoomService.Add(boom);
        //        }
        //    }

        //    quoteRequestBoomService.SaveChanges();
        //}

        [TestMethod]
        public async Task sendquotetest()
        {
            SugarCRMClient client = new SugarCRMClient();
            await client.MigrateQuoteToSuiteCRM(2165, "a04378d6-896a-4540-af9b-6cfda3c73168");
        }

        [TestMethod]
        public async Task testqueue()
        {
            SugarCRMClient client = new SugarCRMClient();
            await client.ExportQuoteToSugarCRM(2120); //Simth-041216-QUOTE-32120
        }

        [TestMethod]
        public async Task newMigrateQuotes()
        {
            SugarCRMClient client = new SugarCRMClient();
            await client.OneTimeMigrateQuotes();
        }

        //[TestMethod]
        //public async Task MigrateAccounts()
        //{
        //    SugarCRMClient client = new SugarCRMClient();
        //    //client.MigrateAccounts();
        //    await client.MigrateQuotes();
        //}


        //[TestMethod]
        //public async void TestRetrieveJSONData()
        //{
        //UnitOfWork uow = new UnitOfWork(lazyLoadingEnabled: false);

        //var quotes = await new QuoteRequestService(uow).GetAllWithIncludedProperties();

        //foreach (var q in quotes)
        //{
        //    var s = q.QuoteBoomSystemsData.QuoteBoomSystems;
        //}

        //uow.Save();
        //}
    }
}