﻿using CherryCreekSystems.BL;
using CherryCreekSystems.BL.Utils;
using Microsoft.Azure.WebJobs;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;

namespace CherryCreekSystems.QuoteBridge
{
    public class Functions
    {
        const int DEQUEUE_COUNT_LIMIT = 5;

        // This function will get triggered/executed when a new message is written
        // on an Azure Queue called queue.
        public static async Task ProcessQueueMessage([QueueTrigger("quotes")] QuoteMessage message, int dequeueCount, TextWriter log)
        {
            try
            {
                var client = new SugarCRMClient();
                var result = await client.MigrateQuoteToSuiteCRM(message.QuoteId, message.SystemId);
                if (!result) throw new Exception($"SuiteCRM Quote Id was blank, quote could not be migrated: {JsonConvert.SerializeObject(message)}");
            }
            catch (Exception ex)
            {
                if (dequeueCount >= DEQUEUE_COUNT_LIMIT)
                {
                    Console.WriteLine($"Reached {DEQUEUE_COUNT_LIMIT} tries, moving to poison queue, sending email");
                    var serializedMessage = JsonConvert.SerializeObject(message);
                    Console.WriteLine(serializedMessage);
                    var emailBody = $"{ex.Message} <br><br> {serializedMessage}";
                    await EmailClient.SendEmail(Configuration.QuotesBridgeErrorNotificationEmails, "quotesbridge@quotationsystem.com", null, null, emailBody, "Quote System to SuiteCRM Bridge Error", null);
                    throw;
                }

                throw; //Prevents Azure from dequeing the message, eventually gets moved to poison queue
            }
        }

        //Use when having to move large amounts of messages from poison queue to regular queue
        //public static void ProcessPoisonQueueMessage([QueueTrigger("quotes-poison")] QuoteMessage poisonMessage, [Queue("quotes")] out QuoteMessage message)
        //{
        //    message = poisonMessage;
        //}
    }

    public class Message
    {
        public int QuoteId { get; set; }
    }
}