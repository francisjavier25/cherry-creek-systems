﻿using ClosedXML.Excel;
using ServiceStack.Text;
using System;
using System.Data.Entity;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using CherryCreekSystems.DAL;

namespace CherryCreekSystems.ProcessStatusFile
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var filePath =
                $"ftp://ftp.zerogravitysoftware.com/CherryCreekSystems/FileUploads/Orders In Process.xlsx";

            using (var request = new WebClient())
            {
                request.Credentials = new NetworkCredential("zerogra1", "Mizaelj17");
                var fileData = request.DownloadData(filePath);

                MemoryStream stream;
                using (stream = new MemoryStream(fileData))
                {
                    UriFixer.FixInvalidUri(stream, UriFixer.FixUri);
                }

                //Stream is closed after fix, so create a new fresh stream
                using (var fixedStream = new MemoryStream(stream.ToArray()))
                {
                    var workbook = new XLWorkbook(fixedStream);
                    var officeWorksheet = workbook.Worksheet(1);

                    var startingRow = 4;
                    var systemIdColumn = "B";

                    var row = officeWorksheet.Row(startingRow);
                    var systemId = row.Cell(systemIdColumn).GetString();
                    int systemIdValue;
                    int.TryParse(systemId, out systemIdValue);

                    var context = new DataContext();
                    //Go from column E to P
                    while (systemIdValue > 0)
                    {
                        var system = context.QuoteBoomSystems
                                            .Include(s => s.Boom)
                                            .FirstOrDefault(s => s.Id == systemIdValue);

                        if (system != null)
                        {
                            var statusColumn = 'E'; //First status column

                            var infoAndDesign = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var sprayBarApproval = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var assemblyPickList = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var shippingPickList = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var production = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var sawAndsheer = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var punchAndBreak = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var machining = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var welding = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var galvanizer = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var assembly = GetNumericValue(row, statusColumn.ToString()); statusColumn++;
                            var shipping = GetNumericValue(row, statusColumn.ToString()); statusColumn++;

                            system.Boom.InfoAndDesignStatus = infoAndDesign;
                            system.Boom.SprayBarApprovalStatus = sprayBarApproval;
                            system.Boom.AssemblyPickListStatus = assemblyPickList;
                            system.Boom.ShippingPickListStatus = shippingPickList;
                            system.Boom.ProductionStatus = production;
                            system.Boom.SawAndSheerStatus = sawAndsheer;
                            system.Boom.PunchAndBreakStatus = punchAndBreak;
                            system.Boom.MachiningStatus = machining;
                            system.Boom.WeldingStatus = welding;
                            system.Boom.GalganizerStatus = galvanizer;
                            system.Boom.AssemblyStatus = assembly;
                            system.Boom.ShippingStatus = shipping;

                            context.SaveChanges();
                        }

                        row = row.RowBelow();
                        systemId = row.Cell(systemIdColumn).GetString();
                        int.TryParse(systemId, out systemIdValue);
                    }
                }
            }
        }

        private static int GetNumericValue(IXLRow row, string column)
        {
            var cell = row.Cell(column).GetString();
            int cellValue;
            int.TryParse(cell, out cellValue);
            return cellValue;
        }
    }

    //http://stackoverflow.com/questions/17888076/openxml-cannot-open-package-because-filemode-or-fileaccess-value-is-not-valid-fo
    public static class UriFixer
    {
        public static Uri FixUri(string brokenUri)
        {
            brokenUri.Dump();
            return new Uri("http://broken-link/");
        }

        public static void FixInvalidUri(Stream fs, Func<string, Uri> invalidUriHandler)
        {
            XNamespace relNs = "http://schemas.openxmlformats.org/package/2006/relationships";
            using (ZipArchive za = new ZipArchive(fs, ZipArchiveMode.Update))
            {
                foreach (var entry in za.Entries.ToList())
                {
                    if (!entry.Name.EndsWith(".rels"))
                        continue;
                    bool replaceEntry = false;
                    XDocument entryXDoc = null;
                    using (var entryStream = entry.Open())
                    {
                        try
                        {
                            entryXDoc = XDocument.Load(entryStream);
                            if (entryXDoc.Root != null && entryXDoc.Root.Name.Namespace == relNs)
                            {
                                var urisToCheck = entryXDoc
                                    .Descendants(relNs + "Relationship")
                                    .Where(
                                        r =>
                                            r.Attribute("TargetMode") != null &&
                                            (string)r.Attribute("TargetMode") == "External");
                                foreach (var rel in urisToCheck)
                                {
                                    var target = (string)rel.Attribute("Target");
                                    if (target != null)
                                    {
                                        try
                                        {
                                            Uri uri = new Uri(target);
                                        }
                                        catch (UriFormatException)
                                        {
                                            Uri newUri = invalidUriHandler(target);
                                            rel.Attribute("Target").Value = newUri.ToString();
                                            replaceEntry = true;
                                        }
                                    }
                                }
                            }
                        }
                        catch (XmlException)
                        {
                            continue;
                        }
                    }
                    if (replaceEntry)
                    {
                        var fullName = entry.FullName;
                        entry.Delete();
                        var newEntry = za.CreateEntry(fullName);
                        using (StreamWriter writer = new StreamWriter(newEntry.Open()))
                        using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                        {
                            entryXDoc.WriteTo(xmlWriter);
                        }
                    }
                }
            }
        }
    }
}